                  !!!!!!!!!!!!!!!!!!!!!!
                      MODULE ee_EE2EEL
                   !!!!!!!!!!!!!!!!!!!!!!

  use functions
  use collier
  implicit none

contains


 FUNCTION EE2EEL(p1, p2, p3, p4, pole, img)
    !! e-(q1) e-(q2) -> e-(q1) e-(q2)
    !! for massive electrons
    real(kind=prec) :: ee2eel
    real(kind=prec) :: p1(4), p2(4), p3(4), p4(4)
    real(kind=prec), optional :: pole, img(2)
    complex(kind=prec) :: sin_cplx, fin_cplx
    complex(kind=prec) :: pole_tr, pole_bx

    if(present(pole)) then
      fin_cplx = ee2eel_tr_cplx(p1,p2,p3,p4,pole_tr)&
               + ee2eel_bx_cplx(p1,p2,p3,p4,pole_bx)
      sin_cplx = pole_tr + pole_bx
      pole = real(sin_cplx)
    else
      fin_cplx = ee2eel_tr_cplx(p1,p2,p3,p4)&
               + ee2eel_bx_cplx(p1,p2,p3,p4)
    end if

    ee2eel = real(fin_cplx)

    if(present(img)) then
      img = (/ aimag(sin_cplx), aimag(fin_cplx) /)
    end if
  END FUNCTION

  FUNCTION EE2EEL_TR_CPLX(p1,p2,p3,p4,pole)
    complex(kind=prec) :: ee2eel_tr_cplx
    real(kind=prec),intent(in) :: p1(4),p2(4),p3(4),p4(4)
    complex(kind=prec), optional :: pole
    complex(kind=prec) :: pole_tt, pole_tu, pole_uu, pole_ut

    if(present(pole)) then
      ee2eel_tr_cplx = 2*(ee2eel_tr_tt_cplx(p1,p2,p3,p4,pole_tt)&
                         +ee2eel_tr_tu_cplx(p1,p2,p3,p4,pole_tu)&
                         +ee2eel_tr_tt_cplx(p1,p2,p4,p3,pole_uu)&
                         +ee2eel_tr_tu_cplx(p1,p2,p4,p3,pole_ut))
      pole = 2*(pole_tt + pole_tu + pole_uu + pole_ut)
    else
      ee2eel_tr_cplx = 2*(ee2eel_tr_tt_cplx(p1,p2,p3,p4)&
                         +ee2eel_tr_tu_cplx(p1,p2,p3,p4)&
                         +ee2eel_tr_tt_cplx(p1,p2,p4,p3)&
                         +ee2eel_tr_tu_cplx(p1,p2,p4,p3))
    end if
  END FUNCTION

  FUNCTION EE2EEL_BX_CPLX(p1,p2,p3,p4,pole)
    complex(kind=prec) :: ee2eel_bx_cplx
    real(kind=prec),intent(in) :: p1(4),p2(4),p3(4),p4(4)
    complex(kind=prec), optional :: pole
    complex(kind=prec) :: pole_tt, pole_tu, pole_uu, pole_ut

    if(present(pole)) then
      ee2eel_bx_cplx = 2*(ee2eel_bx_tt_cplx(p1,p2,p3,p4,pole_tt)&
                         +ee2eel_bx_tu_cplx(p1,p2,p3,p4,pole_tu)&
                         +ee2eel_bx_tt_cplx(p1,p2,p4,p3,pole_uu)&
                         +ee2eel_bx_tu_cplx(p1,p2,p4,p3,pole_ut))
      pole = 2*(pole_tt + pole_tu + pole_uu + pole_ut)
    else
      ee2eel_bx_cplx = 2*(ee2eel_bx_tt_cplx(p1,p2,p3,p4)&
                         +ee2eel_bx_tu_cplx(p1,p2,p3,p4)&
                         +ee2eel_bx_tt_cplx(p1,p2,p4,p3)&
                         +ee2eel_bx_tu_cplx(p1,p2,p4,p3))
    end if
   END FUNCTION

  FUNCTION EE2EEL_TR_TT_CPLX(p1,p2,p3,p4,pole)
    complex(kind=prec) :: ee2eel_tr_tt_cplx
    real(kind=prec),intent(in) :: p1(4),p2(4),p3(4),p4(4)
    complex(kind=prec), optional :: pole
    real(kind=prec) :: ss, tt, m2, m, lgmu
    complex(kind=prec) :: discb,scalarc0ir6
    complex(kind=prec) :: pva,pvb,pvc

    ss = sq(p1+p2); tt = sq(p1-p3)
    m2 = sq(p1); m = sqrt(m2)
    lgmu = log(musq/m2)

    discb = discb_cplx(tt,m)
    scalarc0ir6 = scalarc0ir6_cplx(tt,m)

    pva = (1 + lgmu)*m2
    pvb = 2 + discb + lgmu
    pvc = scalarc0ir6 - (discb*lgmu)/(4*m2 - tt)

    ee2eel_tr_tt_cplx = (4*pva*(24*m2**3 - 6*m2**2*(4*ss + tt) - tt*(2*ss**2 + 2*ss*tt + tt**2) + 2*m2*(&
                    &3*ss**2 + 7*ss*tt + 2*tt**2)) + m2*(-96*m2**3 - 96*lgmu*m2**3 + 96*m2**2*ss + 96&
                    &*lgmu*m2**2*ss - 24*m2*ss**2 - 24*lgmu*m2*ss**2 + 24*m2**2*tt + 24*lgmu*m2**2*tt&
                    & - 40*m2*ss*tt - 48*lgmu*m2*ss*tt + 4*ss**2*tt + 6*lgmu*ss**2*tt - 8*m2*tt**2 - &
                    &12*lgmu*m2*tt**2 + 4*ss*tt**2 + 6*lgmu*ss*tt**2 + 2*tt**3 + 3*lgmu*tt**3 + 2*pvc&
                    &*(8*m2**2 - 6*m2*tt + tt**2)*(8*m2**2 - 8*m2*ss + 2*ss**2 + 2*ss*tt + tt**2) + p&
                    &vb*(-64*m2**3 + 16*m2**2*(4*ss + tt) + 3*tt*(2*ss**2 + 2*ss*tt + tt**2) - 4*m2*(&
                    &4*ss**2 + 10*ss*tt + 3*tt**2))))/(4.*m2*(4*m2 - tt)*tt**2)

    ee2eel_tr_tt_cplx = 64*alpha**3*pi*ee2eel_tr_tt_cplx

    if (present(pole)) then
      pole = -((8*m2**2 - 8*m2*ss + 2*ss**2 + 2*ss*tt + tt**2)*(-4*m2 + tt + discb*(-2*m2 + t&
       &t)))/(2.*tt**2*(-4*m2 + tt))

      pole = 64*alpha**3*pi*pole
    end if
  END FUNCTION

  FUNCTION EE2EEL_TR_TU_CPLX(p1,p2,p3,p4,pole)
    complex(kind=prec) :: ee2eel_tr_tu_cplx
    real(kind=prec),intent(in) :: p1(4),p2(4),p3(4),p4(4)
    complex(kind=prec), optional :: pole
    real(kind=prec) :: ss, tt, m2, m, lgmu
    complex(kind=prec) :: discb,scalarc0ir6
    complex(kind=prec) :: pva,pvb,pvc

    ss = sq(p1+p2); tt = sq(p1-p3)
    m2 = sq(p1); m = sqrt(m2)
    lgmu = log(musq/m2)

    discb = discb_cplx(tt,m)
    scalarc0ir6 = scalarc0ir6_cplx(tt,m)

    pva = (1 + lgmu)*m2
    pvb = 2 + discb + lgmu
    pvc = scalarc0ir6 - (discb*lgmu)/(4*m2 - tt)

    ee2eel_tr_tu_cplx = -(2*pva*(72*m2**3 - 2*ss**2*tt - 2*m2**2*(24*ss + 7*tt) + m2*(6*ss**2 + 12*ss*tt&
                    & - tt**2)) + m2*(-144*m2**3 - 144*lgmu*m2**3 + 96*m2**2*ss + 96*lgmu*m2**2*ss - &
                    &12*m2*ss**2 - 12*lgmu*m2*ss**2 + 44*m2**2*tt + 36*lgmu*m2**2*tt - 24*m2*ss*tt - &
                    &24*lgmu*m2*ss*tt + 2*ss**2*tt + 3*lgmu*ss**2*tt - 2*m2*tt**2 + 2*pvc*(12*m2**2 -&
                    & 8*m2*ss + ss**2)*(8*m2**2 - 6*m2*tt + tt**2) + pvb*(-96*m2**3 + 3*ss**2*tt + 16&
                    &*m2**2*(4*ss + tt) - 2*m2*(4*ss**2 + 8*ss*tt - tt**2))))/(4.*m2*(4*m2 - tt)*tt*(&
                    &-4*m2 + ss + tt))

    ee2eel_tr_tu_cplx = 64*alpha**3*pi*ee2eel_tr_tu_cplx

    if (present(pole)) then
      pole = ((12*m2**2 - 8*m2*ss + ss**2)*(4*m2 + discb*(2*m2 - tt) - tt))/(2.*(4*m2 - tt)*t&
       &t*(-4*m2 + ss + tt))

      pole = 64*alpha**3*pi*pole
    end if
  END FUNCTION

  FUNCTION EE2EEL_BX_TT_CPLX(p1,p2,p3,p4,pole)
    complex(kind=prec) :: ee2eel_bx_tt_cplx
    real(kind=prec),intent(in) :: p1(4),p2(4),p3(4),p4(4)
    complex(kind=prec), optional :: pole
    real(kind=prec) :: ss, tt, m2, m, lgmu
    complex(kind=prec) :: lg,discb_1,discb_2,scalarc0,scalarc0ir6_1,scalarc0ir6_2
    complex(kind=prec) :: pva,pvb_1,pvb_2,pvb_3,pvc_1,pvc_2,pvc_3,pvd_1,pvd_2

    ss = sq(p1+p2); tt = sq(p1-p3)
    m2 = sq(p1); m = sqrt(m2)
    lgmu = log(musq/m2)

    lg = log(cmplx(-(musq/tt)))
    discb_1 = discb_cplx(ss,m)
    discb_2 = discb_cplx(4*m2 - ss - tt,m)
    scalarc0 = scalarc0_cplx(tt,m)
    scalarc0ir6_1 = scalarc0ir6_cplx(ss,m)
    scalarc0ir6_2 = scalarc0ir6_cplx(4*m2 - ss - tt,m)

    pva = (1 + lgmu)*m2
    pvb_1 = 2 + discb_1 + lgmu
    pvb_2 = 2 + discb_2 + lgmu
    pvb_3 = 2 + lg
    pvc_1 = scalarc0ir6_1 - (discb_1*lgmu)/(4*m2 - ss)
    pvc_2 = scalarc0ir6_2 - (discb_2*lgmu)/(ss + tt)
    pvc_3 = scalarc0
    pvd_1 = (-2*discb_1*lg)/(4*m2*tt - ss*tt)
    pvd_2 = (-2*discb_2*lg)/(tt*(ss + tt))

    ee2eel_bx_tt_cplx = (-4*pvc_2*(8*m2 - 2*ss - tt)*(2*m2 - ss - tt) - 4*pvc_1*(2*m2 - ss)*(2*ss + tt) &
                    &+ (8*pvc_3*(4*m2 - 2*ss - tt)*(8*m2**2 - 8*m2*tt + tt**2))/(4*m2 - tt) - 2*pvd_1&
                    &*(2*m2 - ss)*(16*m2**2 - 16*m2*ss + 4*ss**2 + 2*ss*tt + tt**2) - (4*pvb_1*(8*m2*&
                    &*2 - 6*m2*ss + ss*(ss + tt)))/(4*m2 - ss) + (4*pvb_2*(-2*m2*(ss - tt) + ss*(ss +&
                    & tt)))/(ss + tt) + 2*pvd_2*(-2*m2 + ss + tt)*(16*m2**2 + 4*ss**2 + 6*ss*tt + 3*t&
                    &t**2 - 8*m2*(2*ss + tt)) + (16*(pva + m2)*(4*ss**3 + 6*ss**2*tt - tt**3 - 24*m2*&
                    &ss*(ss + tt) + 16*m2**2*(2*ss + tt)))/((4*m2 - ss)*(4*m2 - tt)*(ss + tt)) + (4*p&
                    &vb_3*(-16*m2**2 + 8*m2*ss + tt*(2*ss + tt)))/(4*m2 - tt))/(16.*tt)

    ee2eel_bx_tt_cplx = 64*alpha**3*pi*ee2eel_bx_tt_cplx

    if (present(pole)) then
      pole = ((8*m2**2 - 8*m2*ss + 2*ss**2 + 2*ss*tt + tt**2)*(discb_2*(4*m2 - ss)*(2*m2 - ss&
       & - tt) + discb_1*(2*m2 - ss)*(ss + tt)))/(2.*(4*m2 - ss)*tt**2*(ss + tt))

      pole = 64*alpha**3*pi*pole
    end if
  END FUNCTION

  FUNCTION EE2EEL_BX_TU_CPLX(p1,p2,p3,p4,pole)
    complex(kind=prec) :: ee2eel_bx_tu_cplx
    real(kind=prec),intent(in) :: p1(4),p2(4),p3(4),p4(4)
    complex(kind=prec), optional :: pole
    real(kind=prec) :: ss, tt, m2, m, lgmu
    complex(kind=prec) :: lg,discb_1,discb_2,scalarc0,scalarc0ir6_1,scalarc0ir6_2
    complex(kind=prec) :: pva,pvb_1,pvb_2,pvb_3,pvc_1,pvc_2,pvc_3,pvd_1,pvd_2

    ss = sq(p1+p2); tt = sq(p1-p3)
    m2 = sq(p1); m = sqrt(m2)
    lgmu = log(musq/m2)

    lg = log(cmplx(-(musq/tt)))
    discb_1 = discb_cplx(ss,m)
    discb_2 = discb_cplx(4*m2 - ss - tt,m)
    scalarc0 = scalarc0_cplx(tt,m)
    scalarc0ir6_1 = scalarc0ir6_cplx(ss,m)
    scalarc0ir6_2 = scalarc0ir6_cplx(4*m2 - ss - tt,m)

    pva = (1 + lgmu)*m2
    pvb_1 = 2 + discb_1 + lgmu
    pvb_2 = 2 + discb_2 + lgmu
    pvb_3 = 2 + lg
    pvc_1 = scalarc0ir6_1 - (discb_1*lgmu)/(4*m2 - ss)
    pvc_2 = scalarc0ir6_2 - (discb_2*lgmu)/(ss + tt)
    pvc_3 = scalarc0
    pvd_1 = (-2*discb_1*lg)/(4*m2*tt - ss*tt)
    pvd_2 = (-2*discb_2*lg)/(tt*(ss + tt))

    ee2eel_bx_tu_cplx = ((-8*pvb_1*m2)/(4*m2 - ss) + (4*pvc_1*m2*(4*m2 + ss - tt))/(-4*m2 + ss + tt) + (&
                    &2*pvc_2*(32*m2**2 + 2*ss**2 + 3*ss*tt + tt**2 - 14*m2*(ss + tt)))/(-4*m2 + ss + &
                    &tt) - (2*pvb_2*(ss*(ss + tt) + m2*(-6*ss + 2*tt)))/((ss + tt)*(-4*m2 + ss + tt))&
                    & - (2*pvb_3*(-24*m2**2 + ss*tt + m2*(4*ss + 6*tt)))/((4*m2 - tt)*(-4*m2 + ss + t&
                    &t)) - (2*pvc_3*(96*m2**3 - tt**2*(2*ss + tt) - 16*m2**2*(ss + 5*tt) + 2*m2*tt*(8&
                    &*ss + 9*tt)))/((4*m2 - tt)*(-4*m2 + ss + tt)) + (2*pvd_1*(24*m2**3 - ss**3 - 4*m&
                    &2**2*(7*ss + tt) + m2*(10*ss**2 - ss*tt + tt**2)))/(-4*m2 + ss + tt) - (8*(pva +&
                    & m2)*(2*ss**3 + 6*ss**2*tt + 3*ss*tt**2 + tt**3 + 32*m2**2*(2*ss + tt) - 12*m2*(&
                    &2*ss**2 + 3*ss*tt + tt**2)))/((4*m2 - ss)*(4*m2 - tt)*(ss + tt)*(-4*m2 + ss + tt&
                    &)) + (pvd_2*(48*m2**3 - 2*ss**3 - 4*ss**2*tt - 3*ss*tt**2 - tt**3 - 56*m2**2*(ss&
                    & + tt) + 2*m2*(10*ss**2 + 15*ss*tt + 7*tt**2)))/(-4*m2 + ss + tt))/8.

    ee2eel_bx_tu_cplx = 64*alpha**3*pi*ee2eel_bx_tu_cplx

    if (present(pole)) then
      pole = -((12*m2**2 - 8*m2*ss + ss**2)*(discb_2*(4*m2 - ss)*(2*m2 - ss - tt) + discb_1*(&
       &2*m2 - ss)*(ss + tt)))/(2.*(4*m2 - ss)*tt*(ss + tt)*(-4*m2 + ss + tt))

      pole = 64*alpha**3*pi*pole
    end if
  END FUNCTION

                          !!!!!!!!!!!!!!!!!!!!!!
                          END MODULE ee_EE2EEL
                          !!!!!!!!!!!!!!!!!!!!!!
