                          !!!!!!!!!!!!!!!!!!!!!!
                                MODULE ee_ee2eegl_coll
                          !!!!!!!!!!!!!!!!!!!!!!
    use functions
    use collier
    implicit none

    real(kind=prec) :: pva1(1)
    real(kind=prec) :: pvb1(2),pvb2(4),pvb3(2),pvb4(1),pvb5(4),pvb6(2),pvb7(1),pvb8(1),pvb9(1),pvb10(1),&
                       pvb11(1),pvb12(2),pvb13(2)
    real(kind=prec) :: pvc1(13),pvc2(3),pvc3(3),pvc4(3),pvc5(3),pvc6(7),pvc7(7),pvc8(7),pvc9(7),pvc10(7),&
                       pvc11(7),pvc12(7),pvc13(3),pvc14(3),pvc15(3),pvc16(3),pvc17(7),pvc18(3),pvc19(7),&
                       pvc20(3),pvc21(3),pvc22(7),pvc23(7)
    real(kind=prec) :: pvd1(24),pvd2(11),pvd3(11),pvd4(4),pvd5(4),pvd6(11),pvd7(11),pvd8(11),pvd9(11),&
                       pvd10(11),pvd11(11),pvd12(11)
    real(kind=prec) :: pve1(1),pve2(1)
    real(kind=prec) :: me2
    complex(kind=prec) :: uvdump(24), pv(281)
    complex(kind=prec),parameter :: cz = (0._prec, 0._prec)

contains

  !FUNCTION EE2EEGL_COLL(p1, p2, p3, p4, p5)
  !  !! e-(q1) e-(q2) -> e-(p3) e-(p4) g(p5)
  !  !! for massive electrons
  !  implicit none
  !  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4), p5(4)
  !  real(kind=prec) :: ee2eegl_coll,ss,tt,s25,s35,s15,m,m2!

  !  ss = sq(p1+p2)
  !  tt = sq(p2-p4)
  !  m2 = sq(p1)
  !  m = sqrt(m2)
  !  s15 = s(p1,p5)
  !  s25 = s(p2,p5)
  !  s35 = s(p3,p5)

  !  ee2eegl_coll = 2.**4*Pi**2*alpha**4*(diags(ss,tt,s25,s35,s15,m)&
  !                                    + diags(ss - s15 - s25,tt,-s15 - s25 + s35,-s15,-s35,m)&
  !                                    + diags(ss,tt + s15 - s35,s15,s15 + s25 - s35,s25,m)&
  !                                    + diags(ss - s15 - s25,s15 - s35 + tt,-s35,-s25,-s15 - s25 + s35,m)&
  !                                    + diags(ss,4.*m2 + s25 - ss - tt,s25,s15 + s25 - s35,s15,m)&
  !                                    + diags(ss - s15 - s25,4.*m2 - ss + s25 - tt, -s35,-s15,-s15 - s25 + s35,m)&
  !                                    + diags(ss,4.*m2 - ss + s35 - tt,s15,s35,s25,m)&
  !                                    + diags(ss - s15 - s25,4.*m2 - ss + s35 - tt, -s15-s25+s35,-s25,-s35,m)&
  !                                    + penta(ss,tt,s25,s35,s15,m)&
  !                                    + penta(ss,tt + s15 - s35,s15,s15 + s25 - s35,s25,m)&
  !                                    + penta(ss,4.*m2 + s25 - ss - tt,s25,s15 + s25 - s35,s15,m)&
  !                                    + penta(ss,4.*m2 - ss + s35 - tt,s15,s35,s25,m))

  !END FUNCTION

  FUNCTION QSYM(ss, tt, s15, s25, s35, m)
  real(kind=prec) :: ss,tt,s25,s35,s15,m, qsym

  call inita(ss - s15 - s25, tt, -s35, -s15 - s25 + s35, -s15, m)
  qsym = diags (ss - s15 - s25, tt, -s35, -s15 - s25 + s35, -s15, m)
  call initb(ss, tt, s15, s25, s35, m)
  qsym = diags(ss, tt, s15, s25, s35, m) + qsym + penta(ss, tt, s15, s25, s35, m)
  END FUNCTION QSYM

  SUBROUTINE INITA(ss,tt,s15,s25,s35,m) !contains all the pv's from diags
    real(kind=prec) :: ss,tt,s25,s35,s15,m
    me2 = m**2

    call SetMuIR2_cll(me2)
    call SetMuUV2_cll(me2)

    call A_cll(pv(1:1), uvdump(1:1), cmplx(me2), 0)

    call B_cll(pv(2:3), uvdump(1:2), cz, cmplx(me2), cmplx(me2), 1)
    call B_cll(pv(4:7), uvdump(1:4), cmplx(me2), cz, cmplx(me2), 2)
    call B_cll(pv(8:9), uvdump(1:2), cmplx(me2), cmplx(me2), cz, 1)
    call B_cll(pv(11:14), uvdump(1:4), cmplx(me2 - s15), cz, cmplx(me2), 2)
    call B_cll(pv(15:16), uvdump(1:2), cmplx(me2 - s15), cmplx(me2), cz, 1)
    call B_cll(pv(17:17), uvdump(1:1), cmplx(ss - s15 - s25), cmplx(me2), cmplx(me2), 0)
    call B_cll(pv(19:19), uvdump(1:1), cmplx(4*me2 - ss + s25 - tt), cmplx(me2), cmplx(me2), 0)
    call B_cll(pv(21:21), uvdump(1:1), cmplx(tt), cz, cz, 0)
    call B_cll(pv(22:23), uvdump(1:2), cmplx(tt), cmplx(me2), cmplx(me2), 1)
    call B_cll(pv(24:25), uvdump(1:2), cmplx(s15 - s35 + tt), cmplx(me2), cmplx(me2), 1)

    call C_cll(pv(26:38), uvdump(1:13),&
        cmplx(me2), cz, cmplx(me2 - s15), &
        cz, cmplx(me2), cmplx(me2), 3)
    call C_cll(pv(51:57), uvdump(1:7),&
        cmplx(me2), cmplx(me2), cmplx(tt),&
        cz, cmplx(me2), cz, 2)
    call C_cll(pv(58:64), uvdump(1:7),&
        cmplx(me2), cmplx(me2), cmplx(tt),&
        cmplx(me2), cz, cmplx(me2), 2)
    call C_cll(pv(65:71), uvdump(1:7),&
        cmplx(me2), cmplx(me2), cmplx(s15 - s35 + tt),&
        cmplx(me2), cz, cmplx(me2), 2)
    call C_cll(pv(72:78), uvdump(1:7),&
        cmplx(me2), cmplx(me2 - s15), cz,&
        cmplx(me2), cz, cmplx(me2), 2)
    call C_cll(pv(79:85), uvdump(1:7),&
        cmplx(me2), cmplx(me2 - s15),&
        cmplx(tt), cmplx(me2), cz, cmplx(me2), 2)
    call C_cll(pv(86:92), uvdump(1:7),&
        cmplx(me2), cmplx(ss - s15 - s25),&
        cmplx(me2 - s15), cz, cmplx(me2), cmplx(me2), 2)
    call C_cll(pv(93:99), uvdump(1:7),&
        cmplx(me2), cmplx(4*me2 - ss + s25 - tt), cmplx(me2 - s15),&
        cz, cmplx(me2), cmplx(me2), 2)
    call C_cll(pv(109:111), uvdump(1:3),&
        cmplx(me2 - s15), cmplx(me2),&
        cmplx(tt), cz, cmplx(me2), cz, 1)
    call C_cll(pv(112:118), uvdump(1:7),&
        cmplx(ss - s15 - s25), cmplx(me2),&
        cmplx(me2), cmplx(me2), cmplx(me2), cz, 2)
    call C_cll(pv(122:128), uvdump(1:7),&
        cmplx(4*me2 - ss + s25 - tt), cmplx(me2), cmplx(me2),&
        cmplx(me2), cmplx(me2), cz, 2)
    call C_cll(pv(142:148), uvdump(1:7),&
        cmplx(s15 - s35 + tt), cz, cmplx(tt),&
        cmplx(me2), cmplx(me2), cmplx(me2), 2)

    call D_cll(pv(149:172), uvdump(1:24),&
        cmplx(me2), cmplx(me2), cz, cmplx(tt), cmplx(s15 - s35 + tt), cmplx(me2 - s15), cmplx(me2),&
        cz, cmplx(me2), cmplx(me2), 3)
    call D_cll(pv(225:235), uvdump(1:11),&
        cmplx(me2), cmplx(ss - s15 - s25), cmplx(me2), cmplx(tt), cmplx(me2 - s15), cmplx(me2), cz,&
        cmplx(me2), cmplx(me2), cz, 2)
    call D_cll(pv(236:246), uvdump(1:11),&
        cmplx(me2), cmplx(4*me2 - ss + s25 - tt), cmplx(me2), cmplx(tt), cmplx(me2 - s15), cmplx(me2), cz,&
        cmplx(me2), cmplx(me2), cz, 2)


    pva1 = real(pv(1:1), prec)

    pvb1 = real(pv(2:3), prec)
    pvb2 = real(pv(4:7), prec)
    pvb3 = real(pv(8:9), prec)
    pvb5 = real(pv(11:14), prec)
    pvb6 = real(pv(15:16), prec)
    pvb7 = real(pv(17:17), prec)
    pvb9 = real(pv(19:19), prec)
    pvb11 = real(pv(21:21), prec)
    pvb12 = real(pv(22:23), prec)
    pvb13 = real(pv(24:25), prec)

    pvc1 = real(pv(26:38), prec)
    pvc6 = real(pv(51:57), prec)
    pvc7 = real(pv(58:64), prec)
    pvc8 = real(pv(65:71), prec)
    pvc9 = real(pv(72:78), prec)
    pvc10 = real(pv(79:85), prec)
    pvc11 = real(pv(86:92), prec)
    pvc12 = real(pv(93:99), prec)
    pvc16 = real(pv(109:111), prec)
    pvc17 = real(pv(112:118), prec)
    pvc19 = real(pv(122:128), prec)
    pvc23 = real(pv(142:148), prec)

    pvd1 = real(pv(149:172), prec)
    pvd8 = real(pv(225:235), prec)
    pvd9 = real(pv(236:246), prec)

  END SUBROUTINE INITA

  SUBROUTINE INITB(ss, tt, s15, s25, s35, m) !contains the rest of the pv's needed to compute the pentagons
    real(kind=prec) :: ss,tt,s25,s35,s15,m
    me2 = m**2

    call SetMuIR2_cll(me2)
    call SetMuUV2_cll(me2)

    call inita(ss,tt,s15,s25,s35,m)

    call B_cll(pv(10:10), uvdump(1:1), cmplx(ss), cmplx(me2), cmplx(me2), 0)
    call B_cll(pv(18:18), uvdump(1:1), cmplx(me2 + s35), cmplx(me2), cz, 0)
    call B_cll(pv(20:20), uvdump(1:1), cmplx(4*me2 - ss + s35 - tt), cmplx(me2), cmplx(me2), 0)

    call C_cll(pv(39:41), uvdump(1:3),&
        cmplx(me2), cmplx(me2), cmplx(ss),&
        cmplx(me2), cz, cmplx(me2), 1)
    call C_cll(pv(42:44), uvdump(1:3),&
        cmplx(me2), cmplx(me2), cmplx(ss - s15 - s25),&
        cmplx(me2), cz, cmplx(me2), 1)
    call C_cll(pv(45:47), uvdump(1:3),&
        cmplx(me2), cmplx(me2), cmplx(4*me2 - ss + s25 - tt),&
        cmplx(me2), cz, cmplx(me2), 1)
    call C_cll(pv(48:50), uvdump(1:3),&
        cmplx(me2), cmplx(me2), cmplx(4*me2 - ss + s35 - tt),&
        cmplx(me2), cz, cmplx(me2), 1)
    call C_cll(pv(100:102), uvdump(1:3),&
        cmplx(me2), cmplx(tt), cmplx(me2 + s35),&
        cmplx(me2), cz, cz, 1)
    call C_cll(pv(103:105), uvdump(1:3),&
        cmplx(ss), cmplx(me2), cmplx(me2 + s35),&
        cmplx(me2), cmplx(me2), cz, 1)
    call C_cll(pv(106:108), uvdump(1:3),&
        cmplx(ss), cmplx(ss - s15 - s25),&
        cz, cmplx(me2), cmplx(me2), cmplx(me2), 1)
    call C_cll(pv(119:121), uvdump(1:3),&
        cmplx(me2 + s35), cmplx(me2), cz,&
        cmplx(me2), cz, cmplx(me2), 1)
    call C_cll(pv(129:131), uvdump(1:3),&
        cmplx(4*me2 - ss + s35 - tt), cmplx(me2), cmplx(me2 + s35),&
        cmplx(me2), cmplx(me2), cz, 1)
    call C_cll(pv(132:134), uvdump(1:3),&
        cmplx(4*me2 - ss + s35 - tt), cmplx(4*me2 - ss + s25 - tt), cz,&
        cmplx(me2), cmplx(me2), cmplx(me2), 1)
    call C_cll(pv(135:141), uvdump(1:7),&
        cmplx(tt), cmplx(me2), cmplx(me2 - s15),&
        cz, cz, cmplx(me2), 2)

    call D_cll(pv(173:183), uvdump(1:11),&
        cmplx(me2), cmplx(me2), cmplx(me2), cmplx(me2 - s15), cmplx(tt), cmplx(ss - s15 - s25), cz,&
        cmplx(me2), cz, cmplx(me2), 2)
    call D_cll(pv(184:194), uvdump(1:11),&
        cmplx(me2), cmplx(me2), cmplx(me2), cmplx(me2 - s15), cmplx(tt), cmplx(4*me2 - ss + s25 - tt), cz,&
        cmplx(me2), cz, cmplx(me2), 2)
    call D_cll(pv(195:198), uvdump(1:4),&
        cmplx(me2), cmplx(me2), cmplx(me2), cmplx(me2 + s35), cmplx(ss), cmplx(tt), cmplx(me2),&
        cz, cmplx(me2), cz, 1)
    call D_cll(pv(199:202), uvdump(1:4),&
        cmplx(me2), cmplx(me2), cmplx(me2), cmplx(me2 + s35), cmplx(4*me2 - ss + s35 - tt), cmplx(tt), cmplx(me2),&
        cz, cmplx(me2), cz, 1)
    call D_cll(pv(203:213), uvdump(1:11),&
        cmplx(me2), cmplx(me2), cmplx(ss - s15 - s25), cz, cmplx(ss), cmplx(me2 - s15), cmplx(me2),&
        cz, cmplx(me2), cmplx(me2), 2)
    call D_cll(pv(214:224), uvdump(1:11),&
        cmplx(me2), cmplx(me2), cmplx(4*me2 - ss + s25 - tt), cz, cmplx(4*me2 - ss + s35 - tt), cmplx(me2 - s15), cmplx(me2),&
        cz, cmplx(me2), cmplx(me2), 2)
    call D_cll(pv(247:257), uvdump(1:11),&
        cmplx(me2), cmplx(tt), cmplx(me2), cz, cmplx(me2 + s35), cmplx(me2 - s15), cmplx(me2),&
        cz, cz, cmplx(me2), 2)
    call D_cll(pv(258:268), uvdump(1:11),&
        cmplx(ss), cmplx(me2), cmplx(me2), cz, cmplx(me2 + s35), cmplx(ss - s15 - s25), cmplx(me2),&
        cmplx(me2), cz, cmplx(me2), 2)
    call D_cll(pv(269:279), uvdump(1:11),&
        cmplx(4*me2 - ss + s35 - tt), cmplx(me2), cmplx(me2), cz, cmplx(me2 + s35), cmplx(4*me2 - ss + s25 - tt), cmplx(me2),&
        cmplx(me2), cz, cmplx(me2), 2)

    call E_cll(pv(280:280), uvdump(1:1),&
        cmplx(me2), cmplx(me2), cmplx(me2), cmplx(me2), cz, cmplx(ss), cmplx(tt), cmplx(ss - s15 - s25),&
        cmplx(me2 + s35), cmplx(me2 - s15), cmplx(me2),&
        cz, cmplx(me2), cz, cmplx(me2), 0)
    call E_cll(pv(281:281), uvdump(1:1),&
        cmplx(me2), cmplx(me2), cmplx(me2), cmplx(me2), cz, cmplx(4*me2 - ss + s35 - tt), cmplx(tt),&
        cmplx(4*me2 - ss + s25 - tt), cmplx(me2 + s35), cmplx(me2 - s15),&
        cmplx(me2), cz, cmplx(me2), cz, cmplx(me2), 0)



    pvb4 = real(pv(10:10), prec)
    pvb8 = real(pv(18:18), prec)
    pvb10 = real(pv(20:20), prec)

    pvc2 = real(pv(39:41), prec)
    pvc3 = real(pv(42:44), prec)
    pvc4 = real(pv(45:47), prec)
    pvc5 = real(pv(48:50), prec)
    pvc13 = real(pv(100:102), prec)
    pvc14 = real(pv(103:105), prec)
    pvc15 = real(pv(106:108), prec)
    pvc18 = real(pv(119:121), prec)
    pvc20 = real(pv(129:131), prec)
    pvc21 = real(pv(132:134), prec)
    pvc22 = real(pv(135:141), prec)

    pvd2 = real(pv(173:183), prec)
    pvd3 = real(pv(184:194), prec)
    pvd4 = real(pv(195:198), prec)
    pvd5 = real(pv(199:202), prec)
    pvd6 = real(pv(203:213), prec)
    pvd7 = real(pv(214:224), prec)
    pvd10 = real(pv(247:257), prec)
    pvd11 = real(pv(258:268), prec)
    pvd12 = real(pv(269:279), prec)

    pve1 = real(pv(280:280), prec)
    pve2 = real(pv(281:281), prec)


  END SUBROUTINE INITB

  FUNCTION DIAGS(ss,tt,s15,s25,s35,m)
  real(kind=prec) :: ss,tt,s25,s35,s15,m, diags
  logical :: bub33,tri86,tri18,tri81,box63,box60,box57,ct
  me2 = m**2

  !call SetMuIR2_cll(me2)
  !call SetMuUV2_cll(me2)

  !call pv_evaluate(ss,tt,s25,s35,s15,m)

  bub33 = .true.
  tri86 = .true.
  tri18 = .true.
  tri81 = .true.
  box63 = .true.
  box60 = .true.
  box57 = .true.
  ct = .true.

  diags = 0.

  if (bub33) then

  diags =  diags + ((256*me2*Pi**2*((2*s15*(-32*me2**3 + 2*tt**3 - tt**2*s15 - tt*s15**2 + &
          &2*ss**2*(2*tt + s15) - 2*tt**2*s25 + tt*s15*s25 + 2*tt*s25**2 + 8*me2**2*(4*ss +&
          & 2*tt - 2*s25 - s35) - 2*tt**2*s35 + 2*tt*s15*s35 + tt*s25*s35 - s15*s25*s35 - 2&
          &*s25**2*s35 + s25*s35**2 - ss*(-4*tt**2 + s15**2 + 4*tt*s25 + 2*s15*s25 + 2*tt*s&
          &35 - 2*s25*s35 + s35**2) + 2*me2*(-4*ss**2 - 2*tt**2 + s15**2 + 6*tt*s25 - 2*ss*&
          &(6*tt + s15 - 2*s25 - s35) - 4*s25*s35 + s35**2 + s15*(4*tt + 2*s25 + s35))))/s3&
          &5 + 2*(32*me2**3 + 8*me2**2*(-4*ss + s15 + 4*s25) + 2*me2*(4*ss**2 + 4*ss*tt + 2&
          &*tt**2 - 4*tt*s15 - 3*s15**2 - 8*ss*s25 - 4*tt*s25 + 2*s15*s25 + 4*s25**2) + s15&
          &*(-2*ss**2 + s25*(tt - s35) - tt*(tt + s15 - s35) + ss*(-2*tt + s15 + 2*s25 + s3&
          &5))) - (2*tt*s15*(32*me2**3 - 4*ss**3 + tt**2*s15 - 2*tt*s15**2 - 8*me2**2*(6*ss&
          & - 4*s15 - 5*s25) + 2*tt**2*s25 - tt*s15*s25 + s15**2*s25 - tt*s25**2 + s15*s25*&
          &*2 - 3*tt*s25*s35 + s25**2*s35 + s25*s35**2 + 2*ss**2*(-2*tt + 3*s15 + 4*s25 + s&
          &35) - ss*(2*tt**2 - 4*tt*s15 + 3*s15**2 - 6*tt*s25 + 7*s15*s25 + 4*s25**2 - 2*tt&
          &*s35 + 3*s25*s35 + s35**2) + 2*me2*(12*ss**2 + 2*tt**2 - 2*tt*s15 + 3*s15**2 - 4&
          &*tt*s25 + 10*s15*s25 + 6*s25**2 - 2*tt*s35 + s25*s35 + s35**2 - 2*ss*(-2*tt + 7*&
          &s15 + 9*s25 + s35))))/(s25*(tt + s15 - s35)) + (2*tt*s15*(32*me2**3 - 4*ss**3 - &
          &2*tt**3 + tt**2*s15 + 2*tt*s15**2 + 4*tt**2*s25 - s15**2*s25 - 3*tt*s25**2 - s15&
          &*s25**2 - 8*me2**2*(6*ss + 2*tt - s15 - 3*s25 - s35) + 2*tt**2*s35 - 2*tt*s15*s3&
          &5 - 4*tt*s25*s35 + s15*s25*s35 + 3*s25**2*s35 + 2*ss**2*(-4*tt + s15 + 4*s25 + s&
          &35) - ss*(6*tt**2 + 4*s25**2 + s15*(-2*tt + s25) - 4*tt*s35 + 5*s25*(-2*tt + s35&
          &)) + 2*me2*(12*ss**2 + 2*tt**2 + s15**2 - 6*tt*s25 + 2*s25**2 - 2*tt*s35 + 5*s25&
          &*s35 - s15*(2*tt + s35) - 2*ss*(-6*tt + 2*s15 + 7*s25 + 2*s35))))/((tt + s15 - s&
          &35)*(s15 + s25 - s35)) - (2*tt*s15*(-48*me2**3 + 2*ss**3 + tt**2*s15 + tt*s15**2&
          & - s15**2*s25 + tt*s25**2 - 2*ss**2*(-tt + s15 + 2*s25) + 4*me2**2*(14*ss + 6*tt&
          & - s15 - 9*s25 - 5*s35) - 2*tt*s15*s35 - s15**2*s35 + s15*s25*s35 - s25**2*s35 +&
          & s15*s35**2 + ss*(2*s15**2 + 2*s15*(s25 - s35) + s25*(-2*tt + 2*s25 + s35)) - me&
          &2*(20*ss**2 + 4*tt*s15 + 11*s15**2 - 8*tt*s25 + 5*s15*s25 + 6*s25**2 - 2*tt*s35 &
          &- 15*s15*s35 + 6*s25*s35 + 2*s35**2 - 2*ss*(-8*tt + 6*s15 + 13*s25 + 2*s35))))/(&
          &(4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + (tt*(96*me2**3 - 8*me2**2*(8*ss + s&
          &15 - 8*s25) + 2*me2*(4*ss**2 - 9*s15**2 + 8*ss*(s15 - s25) + 4*s25**2 - 2*s15*(3&
          &*s25 + s35)) + s15*(-2*ss**2 + s15*(tt - 2*s35) + 3*s25*(tt - s35) + ss*(3*s15 +&
          & 2*s25 + 3*s35))))/(4*me2 - ss - tt + s25) + (2*tt*s15*(-48*me2**3 + tt**2*s15 -&
          & tt*s15**2 + ss**2*(2*tt + s15) - tt*s15*s25 + tt*s25**2 + 4*me2**2*(8*ss + 6*tt&
          & - 6*s25 - 3*s35) - tt*s15*s35 + s15**2*s35 + 2*s15*s25*s35 - s25**2*s35 + ss*s2&
          &5*(-2*tt + s35) - ss*s15*(-2*tt + s25 + 3*s35) + me2*(-4*ss**2 - 2*tt*s15 + 2*s1&
          &5**2 + 6*tt*s25 + 6*s15*s25 + 2*tt*s35 + 5*s15*s35 - 8*s25*s35 + ss*(-16*tt - 6*&
          &s15 + 4*s25 + 6*s35))))/(s35*(4*me2 - ss - tt + s35)) - (2*tt*s15*(48*me2**3 - 2&
          &*ss**3 + 4*ss**2*(s15 + s25) + me2**2*(-56*ss + 24*s15 + 44*s25 + 8*s35) + s15*(&
          &-s15**2 + s25**2 + (tt - s35)*s35 + 2*s15*(-tt + s35) + s25*(-tt + s35)) - ss*(s&
          &15**2 + 2*s25**2 + s15*(-2*tt + 5*s25 + s35)) + me2*(20*ss**2 + 10*s15**2 + s15*&
          &(2*tt + 19*s25 - 10*s35) + ss*(-26*s15 - 30*s25 + 2*s35) + 2*(-(tt*s25) + 5*s25*&
          &*2 - s25*s35 + s35**2))))/(s25*(4*me2 - ss - tt + s35)))*pvb6(1))/(tt**2*s15**3)&
          & + (128*Pi**2*(2*(32*me2**4 - 8*me2**3*(4*ss + s15 - 4*s25) + 2*me2**2*(4*ss**2 &
          &+ 2*tt**2 - 4*tt*s15 - 7*s15**2 + 4*ss*(tt + 2*s15 - 2*s25) - 4*tt*s25 - 6*s15*s&
          &25 + 4*s25**2) + s15**2*(tt*s15 + tt*s25 - 2*s15*s25 - 2*s25**2 + ss*(s15 + 2*s2&
          &5 - s35) - tt*s35 + s25*s35) + me2*s15*(-6*ss**2 - 3*tt**2 + 3*tt*s15 - 2*s15**2&
          & + 5*tt*s25 - 8*s15*s25 - 4*s25**2 + tt*s35 - s25*s35 + ss*(-6*tt + 5*s15 + 10*s&
          &25 + s35))) + (tt*(96*me2**4 - 8*me2**3*(8*ss + 7*s15 - 8*s25) - 2*s15**2*s25*(-&
          &ss + s15 + s25) + 2*me2**2*(4*ss**2 - 13*s15**2 + 8*ss*(3*s15 - s25) + 4*s25**2 &
          &- 2*s15*(11*s25 + s35)) + me2*s15*(-6*ss**2 + s25*(3*tt - 4*s25 - 3*s35) + s15*(&
          &tt - 8*s25 - 2*s35) + ss*(3*s15 + 10*s25 + 3*s35))))/(4*me2 - ss - tt + s25) - (&
          &tt*s15*(96*me2**4 - 8*me2**3*(8*ss + 6*tt + 6*s15 - 6*s25 - 3*s35) + 2*me2**2*(4&
          &*ss**2 - 8*s15**2 - 6*tt*s25 + s15*(14*tt - 18*s25 - 11*s35) + 2*ss*(8*tt + 11*s&
          &15 - 2*s25 - 3*s35) - 2*tt*s35 + 8*s25*s35) - 2*me2*(2*s15**3 + ss**2*(2*tt + 3*&
          &s15) + s15**2*(-5*tt + 3*s25) + s25**2*(tt - s35) - ss*(-10*tt*s15 + s15**2 + 2*&
          &tt*s25 + 3*s15*s25 + 6*s15*s35 - s25*s35) + s15*(tt**2 - 4*tt*s25 - 2*tt*s35 + 6&
          &*s25*s35)) + s15*(2*ss**2*tt + (s15 + s25)*(tt*s15 + s25*(tt - s35)) + ss*(s15**&
          &2 + s15*(-2*tt + s25 - s35) + s25*(-2*tt + s35)))))/(s35*(4*me2 - ss - tt + s35)&
          &) + (tt*s15*(64*me2**4 - 16*me2**3*(6*ss + 2*tt + s15 - 3*s25 - s35) + 4*me2**2*&
          &(12*ss**2 + 2*tt**2 - 3*s15**2 - 6*tt*s25 + 2*s25**2 + s15*(2*tt - 6*s25 - 3*s35&
          &) + 2*ss*(6*tt + 4*s15 - 7*s25 - 2*s35) - 2*tt*s35 + 5*s25*s35) - 2*me2*(4*ss**3&
          & - s15**3 + 2*ss**2*(4*tt + 5*s15 - 4*s25 - s35) - (2*tt**2 - 4*tt*s25 + 3*s25**&
          &2)*(-tt + s35) + s15**2*(-6*tt + 2*s25 + s35) + s15*(tt**2 - 6*tt*s25 + 3*s25**2&
          & + 4*s25*s35) + ss*(6*tt**2 - 6*s15**2 + 4*s25**2 + s15*(10*tt - 13*s25 - 4*s35)&
          & - 4*tt*s35 + 5*s25*(-2*tt + s35))) + s15*(4*ss**3 + s15**2*(-tt + 2*s25) - (2*t&
          &t**2 - 4*tt*s25 + 3*s25**2)*(-tt + s35) - 2*ss**2*(-4*tt + s15 + 4*s25 + s35) + &
          &s15*(2*s25**2 + 2*tt*s35 - 3*s25*s35) + ss*(6*tt**2 - s15**2 + 4*s25**2 - 4*tt*s&
          &35 + 5*s25*(-2*tt + s35) + s15*(-2*tt + s25 + s35)))))/((tt + s15 - s35)*(s15 + &
          &s25 - s35)) + (tt*s15*(96*me2**4 - 8*me2**3*(14*ss + 6*tt + 5*s15 - 9*s25 - 5*s3&
          &5) + 2*me2**2*(20*ss**2 + 16*tt*s15 + 3*s15**2 - 8*tt*s25 - 13*s15*s25 + 6*s25**&
          &2 + 2*ss*(8*tt + 8*s15 - 13*s25 - 2*s35) - 2*tt*s35 - 25*s15*s35 + 6*s25*s35 + 2&
          &*s35**2) - 2*me2*(2*ss**3 + 3*s15**3 + 2*ss**2*(tt + 4*s15 - 2*s25) + s15**2*(-2&
          &*tt + 4*s25 - 4*s35) + s25**2*(tt - s35) + s15*(tt**2 - 4*tt*s25 + 3*s25**2 - 3*&
          &tt*s35 + 4*s25*s35 + 2*s35**2) + ss*(-5*s15**2 + s15*(8*tt - 11*s25 - 4*s35) + s&
          &25*(-2*tt + 2*s25 + s35))) + s15*(2*ss**3 - 2*ss**2*(-tt + s15 + 2*s25) + (s15 +&
          & s25)*(tt*s15 + s25*(tt - s35)) + ss*(s15**2 + s15*(-2*tt + 3*s25 - s35) + s25*(&
          &-2*tt + 2*s25 + s35)))))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) - (s15*(64*&
          &me2**4 - 16*me2**3*(4*ss + 2*tt + 2*s15 - 2*s25 - s35) + 4*me2**2*(4*ss**2 + 2*t&
          &t**2 - 3*s15**2 - 6*tt*s25 + 2*ss*(6*tt + 5*s15 - 2*s25 - s35) + 4*s25*s35 - s35&
          &**2 - 3*s15*(2*s25 + s35)) + s15*(4*ss**2*tt + tt*s15**2 + s15*(3*tt*s25 + tt*s3&
          &5 - 3*s25*s35) + (-tt + s35)*(-2*tt**2 + 2*tt*s25 - 2*s25**2 + s25*s35) + ss*(4*&
          &tt**2 + s15**2 - 4*tt*s25 + 2*s15*(-tt + s25) - 2*tt*s35 + 2*s25*s35 - s35**2)) &
          &- 2*me2*(s15**3 + ss**2*(4*tt + 6*s15) + s15**2*(-7*tt + 2*s25) + (-tt + s35)*(-&
          &2*tt**2 + 2*tt*s25 - 2*s25**2 + s25*s35) + s15*(tt**2 - 5*tt*s25 + 2*tt*s35 + 3*&
          &s25*s35 - s35**2) - ss*(-4*tt**2 + 3*s15**2 + 4*tt*s25 + 2*tt*s35 - 2*s25*s35 + &
          &s35**2 + 2*s15*(-6*tt + 3*s25 + s35)))))/s35 - (tt*s15*(64*me2**4 + me2**3*(-96*&
          &ss + 32*s15 + 80*s25) + 4*me2**2*(12*ss**2 + 2*tt**2 - 2*tt*s15 - 7*s15**2 - 4*t&
          &t*s25 + 6*s25**2 - 2*tt*s35 + s25*s35 + s35**2 - 2*ss*(-2*tt + s15 + 9*s25 + s35&
          &)) - 2*me2*(4*ss**3 + 4*s15**3 + ss**2*(4*tt + 6*s15 - 8*s25 - 2*s35) - s25*(-tt&
          & + s35)*(-2*tt + s25 + s35) + s15**2*(10*s25 + s35) + ss*(2*tt**2 - 13*s15**2 - &
          &6*tt*s25 - 11*s15*s25 + 4*s25**2 - 2*tt*s35 - 2*s15*s35 + 3*s25*s35 + s35**2) + &
          &s15*(5*s25**2 + s25*(-3*tt + s35) + (-tt + s35)**2)) + s15*(4*ss**3 - 2*s15**2*s&
          &25 - s25*(-tt + s35)*(-2*tt + s25 + s35) - 2*ss**2*(-2*tt + 3*s15 + 4*s25 + s35)&
          & + s15*(tt*s25 - 2*s25**2 + tt*(-2*tt + s35)) + ss*(2*tt**2 + 2*s15**2 - 6*tt*s2&
          &5 + 4*s25**2 - 2*tt*s35 + 3*s25*s35 + s35**2 + s15*(-4*tt + 7*s25 + s35)))))/(s2&
          &5*(tt + s15 - s35)) - (2*tt*s15*(48*me2**4 + (ss - s15)*s15*(ss - s25)*(ss - s15&
          & - s25) + me2**3*(-56*ss + 44*s25 + 8*s35) + me2**2*(20*ss**2 - 8*s15**2 - 3*s15&
          &*s25 + 10*s25**2 + 2*s15*(tt - 7*s35) + 2*s35**2 - 2*s25*(tt + s35) + 2*ss*(s15 &
          &- 15*s25 + s35)) + me2*(-2*ss**3 + ss**2*(-6*s15 + 4*s25) + ss*(13*s15**2 - 2*s2&
          &5**2 + 2*s15*(tt + 5*s25 - s35)) + s15*(-6*s15**2 - 4*s25**2 + 2*s25*s35 + (tt -&
          & 2*s35)*s35 + s15*(-3*tt - 10*s25 + 4*s35)))))/(s25*(4*me2 - ss - tt + s35)))*pv&
          &b6(2))/(tt**2*s15**3))/(16.*Pi**2)

          !print*,"1",diags

  end if

  if (tri86) then

  diags =  diags + (4*((2*tt*(s25*(-ss + s15 + s25) + me2*(s15 + 4*s25)))/(4*me2 - ss - tt &
          &+ s25) - 2*(tt*s15 + tt*s25 - 2*s15*s25 - 2*s25**2 - 2*me2*(s15 + 2*s25) + ss*(s&
          &15 + 2*s25 - s35) - tt*s35 + s25*s35) + (tt*(tt*s15**2 - 2*s15**2*s25 - tt*s25**&
          &2 - 2*s15*s25**2 - 2*me2*(s15 + s25)*(s15 - s35) + ss*(s15 + s25)*(s15 - s35) - &
          &2*tt*s15*s35 + 3*s15*s25*s35 + s25**2*s35))/((tt + s15 - s35)*(s15 + s25 - s35))&
          & - (tt*(2*s15**2*s25 + 2*me2*(s15 - s35)*(s25 - s35) - s25*(s25 - s35)*(-tt + s3&
          &5) + ss*(s15 - s35)*(-s25 + s35) + s15*(2*s25**2 + s25*(tt - 2*s35) + tt*s35)))/&
          &(s25*(tt + s15 - s35)) + (tt*((s15 + s25)*(tt*s15 + tt*s25 - s25*s35) + ss*(s15*&
          &*2 + s15*s25 - s15*s35 + s25*s35) - 2*me2*(2*s15**2 + 3*s15*s25 - s15*s35 + s25*&
          &s35)))/(s35*(4*me2 - ss - tt + s35)) - (2*tt*(s15*s25*(-ss + s15 + s25) + me2*(2&
          &*s15*s25 + s35**2)))/(s25*(4*me2 - ss - tt + s35)) + (tt*s15**2 - s25*(2*s25 - s&
          &35)*(-tt + s35) + ss*(s15 + 2*s25 - s35)*(s15 + s35) + s15*(3*tt*s25 + tt*s35 - &
          &3*s25*s35) - 2*me2*(s15**2 + (2*s25 - s35)*s35 + 2*s15*(s25 + s35)))/s35 - (tt*(&
          &(s15 + s25)*(tt*s15 + tt*s25 - s25*s35) + ss*(s15**2 + s15*s25 - s15*s35 + s25*s&
          &35) - 2*me2*(s15**2 + s15*(s25 - s35) + s35*(s25 + s35))))/((4*me2 - ss - tt + s&
          &25)*(s15 + s25 - s35))))/(tt**2*s15) + ((128*Pi**2*(2*me2*s15 - (2*tt*(12*me2**2&
          & + (ss - s25)*(ss - s15 - s25) + me2*(-8*ss + 5*s15 + 8*s25)))/(4*me2 - ss - tt &
          &+ s25) - 2*(8*me2**2 + 2*ss**2 + tt**2 - tt*s15 - 2*tt*s25 + 2*s15*s25 + 2*s25**&
          &2 - 2*ss*(-tt + s15 + 2*s25) + me2*(-8*ss + 6*s15 + 8*s25)) + (tt*s15*(2*me2 - s&
          &s - tt + s25))/(tt + s15 - s35) + (tt*s15*(2*me2 - ss + s15 + s25))/(tt + s15 - &
          &s35) + (me2*tt*s15*(s15 - 2*s35))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + &
          &(me2*tt*s15*(s15 + 2*s25))/(s35*(4*me2 - ss - tt + s35)) - (tt*s15*(s25*(-ss + s&
          &15 + s25) + me2*(-s15 + 2*(s25 + s35))))/(s25*(-4*me2 + ss + tt - s35)))*pva1(1)&
          &)/(tt**2*s15**2) + (128*Pi**2*(32*me2**3 - 32*me2**2*(ss - s15 - s25) + 4*me2*(2&
          &*ss**2 + tt**2 - 2*tt*s15 + 3*s15**2 + ss*(2*tt - 6*s15 - 4*s25) - 2*tt*s25 + 6*&
          &s15*s25 + 2*s25**2) + 2*s15*(2*ss**2 + tt**2 - tt*s15 - 2*tt*s25 + 2*s15*s25 + 2&
          &*s25**2 - 2*ss*(-tt + s15 + 2*s25)) - (tt*s15*(me2*(8*s15**2 + s15*(-2*tt + 21*s&
          &25 - 7*s35) + (-2*tt + 13*s25)*(s25 - s35) - 10*ss*(s15 + s25 - s35)) + 12*me2**&
          &2*(s15 + s25 - s35) + (2*ss**2 + tt**2 - 2*tt*s15 + ss*(2*tt - 3*s15 - 4*s25) - &
          &2*tt*s25 + 3*s15*s25 + 2*s25**2)*(s15 + s25 - s35)))/((tt + s15 - s35)*(s15 + s2&
          &5 - s35)) + s15*(-12*me2**2 - 2*ss**2 - tt**2 + tt*s15 + 2*tt*s25 - 2*s15*s25 - &
          &2*s25**2 + 2*ss*(-tt + s15 + 2*s25) + (2*me2*(3*s25**2 + 3*s15*(s25 - s35) + 4*s&
          &s*s35 - 7*s25*s35))/s35) + (tt*s15*(s25*(ss**2 - s15**2 - 2*ss*s25 + s25**2) + 2&
          &*me2**2*(s15 + 2*s25 - 4*s35) + me2*(tt*s15 + 3*tt*s25 + 4*s25**2 + ss*(s15 - 4*&
          &s25 - s35) + 2*s15*s35 + s25*s35 - 3*s35**2)))/(s25*(4*me2 - ss - tt + s35)) - (&
          &tt*s15*(2*me2**2*(7*s15 + 4*s25 - 10*s35) + (ss - s25)*(ss - s15 - s25)*(s15 + s&
          &25 - s35) - me2*(-4*s15**2 + tt*s25 - 9*s25**2 + ss*(7*s15 + 6*s25 - 7*s35) - 2*&
          &tt*s35 + 10*s25*s35 + 3*s35**2 + s15*(tt - 13*s25 + s35))))/((4*me2 - ss - tt + &
          &s25)*(s15 + s25 - s35)) - (tt*s15*(-4*me2**2*s25 + s25*(-2*ss**2 - tt**2 + tt*s1&
          &5 + s15**2 + 2*tt*s25 - s15*s25 - 2*s25**2 + ss*(-2*tt + s15 + 4*s25)) + me2*(6*&
          &ss*s25 + (s15 - 3*s25)*(s25 + s35))))/(s25*(tt + s15 - s35)) - (tt*s15*((ss - s2&
          &5)*(ss - s15 - s25)*s35 + 2*me2**2*(s15 + 4*s25 + 8*s35) + me2*(s15**2 + tt*s25 &
          &- 3*s25**2 + ss*(s15 - 11*s35) - 2*tt*s35 + 14*s25*s35 + s15*(tt - s25 + 5*s35))&
          &))/(s35*(4*me2 - ss - tt + s35)) + (tt*(48*me2**3 - 2*s15*(ss - s25)*(-ss + s15 &
          &+ s25) + 4*me2**2*(-8*ss + 8*s15 + 11*s25 + 3*s35) + me2*(4*ss**2 + 5*s15**2 + s&
          &15*(-3*tt + 10*s25 + 6*s35) + s25*(-9*tt + 4*s25 + 9*s35) - ss*(13*s15 + 8*s25 +&
          & 9*s35))))/(4*me2 - ss - tt + s25))*pvb1(1))/(tt**2*s15**2) - (128*Pi**2*(tt**2*&
          &s15 + tt**2*s25 - 2*tt*s15*s25 - 2*tt*s25**2 + 2*s15*s25**2 + 2*s25**3 + 8*me2**&
          &2*(s15 + 2*s25) + ss**2*(s15 + 2*s25 - s35) - tt**2*s35 + 2*tt*s25*s35 - s25**2*&
          &s35 + me2*(-5*tt*s15 - 4*tt*s25 + 10*s15*s25 + 12*s25**2 - 2*ss*(3*s15 + 6*s25 -&
          & 2*s35) + 4*tt*s35 - 4*s25*s35) + ss*(s15*(2*tt - 3*s25) - 4*s25**2 - 2*tt*s35 +&
          & 2*s25*(tt + s35)))*pvb1(2))/(tt**2*s15*(4*me2 - ss - tt + s25)) + (128*Pi**2*(-&
          &2*(8*me2**3 + me2**2*(-8*ss + 12*s15 + 8*s25) + s15*(2*ss**2 + tt**2 - tt*s15 - &
          &2*tt*s25 + 2*s15*s25 + 2*s25**2 - 2*ss*(-tt + s15 + 2*s25)) + me2*(2*ss**2 + tt*&
          &*2 - 2*tt*s15 + 6*s15**2 - 2*tt*s25 + 10*s15*s25 + 2*s25**2 - 2*ss*(-tt + 5*s15 &
          &+ 2*s25))) - (s15*(tt**3 + tt*s15**2 - 2*tt**2*s25 + 2*tt*s15*s25 + 2*tt*s25**2 &
          &- tt**2*s35 + tt*s15*s35 + 2*tt*s25*s35 - 2*s15*s25*s35 - 2*s25**2*s35 - 2*ss**2&
          &*(-tt + s35) + 2*ss*(-tt + s15 + 2*s25)*(-tt + s35) - 4*me2**2*(-2*tt + s15 + 2*&
          &s35) + me2*(-8*ss*tt + 2*s15**2 + 8*tt*s25 + 6*s25**2 + s15*(9*tt + 6*s25 - 6*s3&
          &5) + 8*ss*s35 - 3*tt*s35 - 14*s25*s35)))/s35 + (tt*s15*(2*me2**2*(-6*tt + 7*s15 &
          &+ 2*s25 + 8*s35) + me2*(-9*tt*s15 + 3*s15**2 - 4*tt*s25 + 5*s15*s25 - 3*s25**2 +&
          & ss*(8*tt - 5*s15 - 11*s35) - 2*tt*s35 + 5*s15*s35 + 14*s25*s35) + (ss - s15 - s&
          &25)*(tt*s15 + tt*s25 - s25*s35 + ss*(-tt + s35))))/(s35*(4*me2 - ss - tt + s35))&
          & + (tt*s15*(-2*ss**3 + ss**2*(-2*tt + 3*s15 + 4*s25) + s15*(tt**2 - tt*s25 + 2*s&
          &15*s25 + 2*s25**2) - ss*(tt**2 - 2*tt*s15 + s15**2 - 2*tt*s25 + 5*s15*s25 + 2*s2&
          &5**2) + 2*me2**2*(-4*ss + 6*s15 + 2*s25 + s35) + me2*(8*ss**2 + 5*s15**2 - tt*s2&
          &5 + 5*s25**2 - 2*ss*(6*s15 + 5*s25) - tt*s35 - 3*s25*s35 + s15*(3*tt + 13*s25 + &
          &s35))))/(s25*(tt + s15 - s35)) + (tt*s15*(-ss**3 + 2*ss**2*(s15 + s25) + 2*s15*s&
          &25*(s15 + s25) - ss*(s15**2 + 4*s15*s25 + s25**2) + 2*me2**2*(-6*ss + 6*s15 + s2&
          &5 + 3*s35) + me2*(8*ss**2 + 3*tt*s15 + 7*s15**2 - tt*s25 + 12*s15*s25 + 2*s25**2&
          & - tt*s35 - 5*s15*s35 - 3*s25*s35 + 3*s35**2 + ss*(-13*s15 - 10*s25 + 3*s35))))/&
          &(s25*(4*me2 - ss - tt + s35)) - (tt*s15*(32*me2**3 - 2*ss**3 - tt**3 + tt*s15**2&
          & + 2*tt**2*s25 - 2*s15**2*s25 - 2*tt*s25**2 - 2*s15*s25**2 + tt**2*s35 - 2*tt*s1&
          &5*s35 - 2*tt*s25*s35 + 3*s15*s25*s35 + 2*s25**2*s35 + ss**2*(-4*tt + s15 + 4*s25&
          & + 2*s35) + me2**2*(-40*ss - 8*tt + 16*s15 + 28*s25 + 10*s35) + ss*(-3*tt**2 + 2&
          &*s15**2 + 6*tt*s25 - 2*s25**2 + s15*(2*tt + s25 - 3*s35) + 2*tt*s35 - 4*s25*s35)&
          & + me2*(16*ss**2 + 4*tt**2 - 5*s15**2 - 15*tt*s25 + 3*s25**2 - s15*(14*tt + s25 &
          &- 7*s35) + 13*s25*s35 - 2*ss*(-8*tt + 5*s15 + 11*s25 + 5*s35))))/((tt + s15 - s3&
          &5)*(s15 + s25 - s35)) - (tt*(24*me2**3 - 2*s15*(ss - s25)*(-ss + s15 + s25) - 4*&
          &me2**2*(4*ss - 7*s15 - 7*s25 - 3*s35) + me2*(2*ss**2 + 6*s15**2 + s15*(-3*tt + 1&
          &0*s25 + 6*s35) + s25*(-9*tt + 2*s25 + 9*s35) - ss*(13*s15 + 4*s25 + 9*s35))))/(4&
          &*me2 - ss - tt + s25) + (tt*s15*(-48*me2**3 + 2*me2**2*(22*ss + 6*tt - 7*s15 - 1&
          &7*s25 - 13*s35) + (ss - s15 - s25)*(ss**2 - tt*s15 + s25*(-tt + s35) - ss*(-tt +&
          & s25 + s35)) + me2*(-12*ss**2 + 8*tt*s25 - s25**2 + s15*(7*tt + s25 - 2*s35) + 3&
          &*tt*s35 - 14*s25*s35 - 3*s35**2 + ss*(-8*tt + 7*s15 + 16*s25 + 11*s35))))/((4*me&
          &2 - ss - tt + s25)*(s15 + s25 - s35)))*pvb3(1))/(tt**2*s15**2) + (128*Pi**2*((tt&
          &*(24*me2**3 + 2*me2*(ss**2 + ss*(4*tt + 2*s15 - s25 - s35) + s15*(-tt + s25 - s3&
          &5)) - 2*me2**2*(8*ss + 6*tt - 2*s25 - 3*s35) + ss*(-(ss*(tt + s15)) + s15*s35)))&
          &/(s35*(4*me2 - ss - tt + s35)) + (16*me2**3 - 2*ss**2*(tt + s15) + 4*me2**2*(-4*&
          &ss - 2*tt + s15 + 2*s25) + 2*ss*(tt + s15)*(-tt + s35) + tt*(tt + s15 - s35)*(-t&
          &t + s35) + 2*me2*(2*ss**2 + tt**2 - tt*s15 - s15**2 - tt*s25 + ss*(6*tt + 3*s15 &
          &- 2*s25 - s35) + s25*s35 - s35**2))/s35 + (tt*(24*me2**3 + ss**2*(-ss + s15 + s2&
          &5) + me2**2*(-28*ss + 10*s15 + 6*s25) + 2*me2*(5*ss**2 + (tt + s15 - s35)*(s15 +&
          & s25 - s35) + ss*(-4*s15 - 3*s25 + s35))))/(s25*(4*me2 - ss - tt + s35)) + (tt*(&
          &-24*me2**3 + ss*(ss**2 - ss*(-tt + s15 + s25) + s15*(s15 + s25 - s35)) + 2*me2**&
          &2*(14*ss + 6*tt - s15 - 5*s25 - 7*s35) - 2*me2*(5*ss**2 + s15**2 - s15*(tt + s35&
          &) + (-tt + s35)*(2*s25 + s35) - ss*(-4*tt + 2*s15 + 3*s25 + 3*s35))))/((4*me2 - &
          &ss - tt + s25)*(s15 + s25 - s35)) - (tt*(16*me2**3 - 2*ss**3 + (-tt + s25)*(-tt &
          &+ s35)**2 + me2**2*(-24*ss - 8*tt + 6*s15 + 8*s25 + 4*s35) + ss**2*(s15 + 2*(-2*&
          &tt + s25 + s35)) - ss*(s15**2 + s15*(s25 - s35) + (-tt + s35)*(-3*tt + 2*s25 + s&
          &35)) + 2*me2*(6*ss**2 + s15**2 + s15*(-2*tt + s25) + (-tt + 2*s25)*(-tt + s35) -&
          & ss*(-6*tt + 3*s15 + 4*s25 + 3*s35))))/((tt + s15 - s35)*(s15 + s25 - s35)) + (t&
          &t*(16*me2**3 - 2*ss**3 - s25*(tt + s15 - s35)*(-tt + s35) + me2**2*(-24*ss + 10*&
          &s15 + 8*s25 + 4*s35) + ss**2*(s15 + 2*(-tt + s25 + s35)) + ss*(s15*s25 - (-tt + &
          &s35)*(-tt + 2*s25 + s35)) + 2*me2*(6*ss**2 + s15*(tt - s25) + (-tt + s35)*(-tt +&
          & 2*s25 + s35) - ss*(-2*tt + 3*s15 + 4*s25 + 3*s35))))/(s25*(tt + s15 - s35)))*pv&
          &b3(2))/(tt**2*s15) + (64*Pi**2*((-2*tt*(24*me2**3 - 4*me2**2*(4*ss + 5*s15 - 4*s&
          &25) + 2*ss*s15*(-ss + s15 + s25) + me2*(2*ss**2 + 16*ss*s15 - 9*s15**2 - 4*ss*s2&
          &5 - 8*s15*s25 + 2*s25**2)))/(4*me2 - ss - tt + s25) - (2*tt*s15*(-24*me2**3 + (s&
          &s - s25)*(ss - s15 - s25)*(s15 + s25) + 2*me2**2*(8*ss - 3*s15 - 8*s25 - 3*s35) &
          &+ me2*(-2*ss**2 + 3*tt*s15 + 2*s15**2 + 2*ss*s25 + 3*tt*s25 + 3*s15*s25 - ss*s35&
          & + tt*s35 + s15*s35 + s25*s35 - 2*s35**2)))/(s25*(4*me2 - ss - tt + s35)) + (2*s&
          &15*(16*me2**3 + tt*s15**2 - 2*tt**2*s25 + tt*s15*s25 + 2*tt*s25**2 - 2*ss**2*s35&
          & + tt**2*s35 + tt*s15*s35 + tt*s25*s35 + s15*s25*s35 - s25*s35**2 - 4*me2**2*(4*&
          &ss - s15 - 2*s25 + s35) + me2*(4*ss**2 + 2*tt**2 + 3*tt*s15 + 2*s15**2 + 6*tt*s2&
          &5 + 4*s15*s25 - 2*ss*(-2*tt + s15 + 2*s25 - 3*s35) - 3*tt*s35 - 6*s15*s35 - 2*s2&
          &5*s35 - 2*s35**2) + ss*(-s15**2 - 4*tt*s25 - 2*s15*(tt + s25 - s35) + 2*s25*s35 &
          &+ s35**2)))/s35 + (tt*s15*(32*me2**3 - tt*s15**2 + 2*tt**2*s25 + 3*tt*s15*s25 + &
          &2*tt*s25**2 - 4*s15*s25**2 - 4*s25**3 - 2*ss**2*(s15 + 2*s25 - 2*s35) - 2*tt*s15&
          &*s35 - 6*tt*s25*s35 - s15*s25*s35 + 2*s25**2*s35 + 2*s25*s35**2 + 4*me2**2*(-8*s&
          &s + 3*s15 + 4*s25 + s35) + ss*(s15**2 + 8*s15*s25 + 8*s25**2 + 4*tt*s35 - s15*s3&
          &5 - 6*s25*s35 - 2*s35**2) + 2*me2*(4*ss**2 + 4*ss*tt + 2*tt**2 - 3*tt*s15 - s15*&
          &*2 - 3*tt*s25 - 4*s15*s25 - 4*s25**2 - 6*ss*s35 - 3*tt*s35 + 2*s15*s35 + 4*s25*s&
          &35 + 2*s35**2)))/(s25*(tt + s15 - s35)) - 4*(8*me2**3 - 4*me2**2*(2*ss + s15 - 2&
          &*s25) + me2*(2*ss**2 + tt**2 - 4*s15**2 + 2*ss*(tt + 3*s15 - 2*s25) - 2*tt*s25 +&
          & 2*s25**2 - 2*s15*(tt + s25)) + s15*(-2*ss**2 + (-tt + s25)*(tt - s35) + ss*(-2*&
          &tt + s15 + 2*s25 + s35))) + (tt*s15*(32*me2**3 - 3*tt*s15**2 + 2*tt**2*s25 - 7*t&
          &t*s15*s25 + 4*s15**2*s25 - 6*tt*s25**2 + 8*s15*s25**2 + 4*s25**3 + 2*ss**2*(s15 &
          &+ 2*s25) - 2*tt**2*s35 + 2*tt*s15*s35 + 4*tt*s25*s35 - 3*s15*s25*s35 - 2*s25**2*&
          &s35 + 4*me2**2*(-8*ss + 7*s15 + 12*s25 + s35) - ss*(s15**2 + 8*s25**2 + 4*tt*s35&
          & - 2*s25*(4*tt + s35) + s15*(-4*tt + 8*s25 + s35)) + 2*me2*(4*ss**2 + 2*tt**2 + &
          &s15**2 - 13*tt*s25 + 12*s25**2 + 4*tt*s35 + 4*s15*(-2*tt + 3*s25 + s35) - 2*ss*(&
          &-2*tt + 4*s15 + 8*s25 + s35))))/((tt + s15 - s35)*(s15 + s25 - s35)) + (tt*s15*(&
          &48*me2**3 + 3*tt*s15**2 + 5*tt*s15*s25 + 2*tt*s25**2 - 2*ss**2*s35 + s15*s25*s35&
          & - 4*me2**2*(8*ss + 3*s15 - 6*s25 + s35) - ss*(4*tt*s15 + s15**2 + 4*tt*s25 + 2*&
          &s15*s25 - 3*s15*s35 - 2*s25*s35) + 2*me2*(2*ss**2 + 8*tt*s15 + 6*tt*s25 - 2*tt*s&
          &35 - 6*s15*s35 - s25*s35 + ss*(5*s15 - 2*s25 + 3*s35))))/(s35*(4*me2 - ss - tt +&
          & s35)) + (tt*s15*(48*me2**3 - 3*tt*s15**2 - 32*me2**2*(ss - s15 - s25) - 5*tt*s1&
          &5*s25 + 4*s15**2*s25 - 2*tt*s25**2 + 6*s15*s25**2 + 2*s25**3 - s15*s25*s35 + 2*s&
          &s**2*(s15 + s25 + s35) - ss*(s15**2 + 2*s25*(-2*tt + 2*s25 + s35) + s15*(-4*tt +&
          & 6*s25 + 3*s35)) + 2*me2*(2*ss**2 - 6*tt*s15 - 6*tt*s25 + 5*s15*s25 + 4*s25**2 +&
          & 3*tt*s35 + 9*s15*s35 + 5*s25*s35 - 2*s35**2 - ss*(6*s15 + 6*s25 + 7*s35))))/((4&
          &*me2 - ss - tt + s25)*(s15 + s25 - s35)))*pvb5(1))/(tt**2*s15**2) + (128*Pi**2*(&
          &2*(4*me2**2 + 2*ss**2 + tt**2 - tt*s15 - 2*me2*(4*ss + tt - 3*s15 - 4*s25) - 2*t&
          &t*s25 + 2*s15*s25 + 2*s25**2 - 2*ss*(-tt + s15 + 2*s25)) + (2*tt*(6*me2**2 + (ss&
          & - s25)*(ss - s15 - s25) + me2*(-6*ss + 4*s15 + 6*s25)))/(4*me2 - ss - tt + s25)&
          & + (tt*(8*me2**3 - 4*me2**2*(4*ss + 2*tt - 3*s15 - 3*s25) + 2*me2*(5*ss**2 + 2*s&
          &15**2 + ss*(5*tt - 6*s15 - 9*s25) + (tt - 2*s25)**2 + 5*s15*s25) - (ss + tt - s2&
          &5)*(2*ss**2 + tt**2 + s15**2 + ss*(2*tt - 3*s15 - 4*s25) - 2*tt*s25 + 3*s15*s25 &
          &+ 2*s25**2)))/((tt + s15 - s35)*(s15 + s25 - s35)) - (tt*(24*me2**3 - 2*ss**3 + &
          &tt**2*s15 + tt*s15**2 + tt**2*s25 - tt*s15*s25 + s15**2*s25 - 2*tt*s25**2 + 3*s1&
          &5*s25**2 + 2*s25**3 + ss**2*(-2*tt + 3*s15 + 6*s25) - ss*(tt**2 - tt*s15 + s15**&
          &2 - 4*tt*s25 + 6*s15*s25 + 6*s25**2) - 4*me2**2*(8*ss - 6*s15 - 9*s25 + s35) + 2&
          &*me2*(7*ss**2 + tt**2 - 3*tt*s15 + s15**2 - 3*tt*s25 + 9*s15*s25 + 8*s25**2 - s2&
          &5*s35 + ss*(3*tt - 9*s15 - 15*s25 + s35))))/(s25*(tt + s15 - s35)) + (tt*(8*me2*&
          &*3 - (ss + tt - s25)*(ss**2 - 3*ss*s15 + 2*s15**2 - 2*ss*s25 + 3*s15*s25 + s25**&
          &2) - 2*me2**2*(8*ss + 4*tt - 9*s15 - 5*s25 + s35) + me2*(8*ss**2 - 4*tt*s15 + 9*&
          &s15**2 - 3*tt*s25 + 17*s15*s25 + 6*s25**2 + 2*tt*s35 - 2*s15*s35 - s25*s35 + ss*&
          &(6*tt - 18*s15 - 14*s25 + s35))))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) - &
          &(tt*(24*me2**3 - tt*(ss**2 - 3*ss*s15 + 2*s15**2 - 2*ss*s25 + 3*s15*s25 + s25**2&
          &) + 2*me2**2*(-8*ss - 4*tt + 2*s15 + 6*s25 + s35) + me2*(2*ss**2 - 6*tt*s15 + s1&
          &5**2 - 5*tt*s25 + s25*s35 - ss*(-6*tt + s15 + 2*s25 + s35))))/(s35*(4*me2 - ss -&
          & tt + s35)) - (16*me2**3 - tt*(2*ss**2 + tt**2 + tt*s15 + 2*s15**2 - 2*tt*s25 + &
          &2*s15*s25 + 2*s25**2 - 2*ss*(-tt + s15 + 2*s25)) + 4*me2**2*(-4*ss - 2*tt + 2*s2&
          &5 + s35) + 2*me2*(2*ss**2 + 2*tt**2 - 2*tt*s15 + s15**2 - 5*tt*s25 + s25*s35 - s&
          &s*(-6*tt + s15 + 2*s25 + s35)))/s35 - (tt*(32*me2**3 + me2**2*(-32*ss + 16*s15 +&
          & 34*s25) - (ss - s25)*(ss**2 - 3*ss*s15 + 2*s15**2 - 2*ss*s25 + 3*s15*s25 + s25*&
          &*2) + me2*(10*ss**2 + tt*s15 + 4*s15**2 - tt*s25 + 13*s15*s25 + 10*s25**2 + 2*tt&
          &*s35 + s25*s35 - ss*(14*s15 + 20*s25 + s35))))/(s25*(4*me2 - ss - tt + s35)))*pv&
          &b5(2))/(tt**2*s15) + (128*Pi**2*(-((4*me2**2 + (ss + tt - s25)*(ss - s15 - s25) &
          &+ me2*(-4*ss + 3*s15 + 4*s25))/(tt*s25*(tt + s15 - s35))) - (2*me2 - ss + s15 + &
          &s25)/(tt*(s15 + s25 - s35)) - (4*me2**2 + (ss + tt - s25)*(ss - s15 - s25) + me2&
          &*(-4*ss + 3*s15 + 4*s25))/(tt*(tt + s15 - s35)*(s15 + s25 - s35)) - (tt + s15)/(&
          &tt*s35) - (me2*(4*me2 - 2*ss + s15 + 2*s25))/(tt*s25*(4*me2 - ss - tt + s35)) - &
          &(2*me2 - ss + s15 + s25)/(s35*(4*me2 - ss - tt + s35)))*((-2*me2 - s15)/6. + 4*p&
          &vb5(3)))/s15 + (128*Pi**2*(me2 - s15)*(-((4*me2**2 + (ss + tt - s25)*(ss - s15 -&
          & s25) + me2*(-4*ss + 3*s15 + 4*s25))/(tt*s25*(tt + s15 - s35))) - (2*me2 - ss + &
          &s15 + s25)/(tt*(s15 + s25 - s35)) - (4*me2**2 + (ss + tt - s25)*(ss - s15 - s25)&
          & + me2*(-4*ss + 3*s15 + 4*s25))/(tt*(tt + s15 - s35)*(s15 + s25 - s35)) - (tt + &
          &s15)/(tt*s35) - (me2*(4*me2 - 2*ss + s15 + 2*s25))/(tt*s25*(4*me2 - ss - tt + s3&
          &5)) - (2*me2 - ss + s15 + s25)/(s35*(4*me2 - ss - tt + s35)))*pvb5(4))/s15 + (12&
          &8*Pi**2*(-2*(32*me2**4 + me2*tt*s15**2 + 16*me2**3*(-2*ss + s15 + 2*s25) + 2*me2&
          &**2*(4*ss**2 + 2*tt**2 - 4*tt*s15 + s15**2 - 4*tt*s25 + 4*s15*s25 + 4*s25**2 - 4&
          &*ss*(-tt + s15 + 2*s25))) - (tt*s15*(4*me2**3*(s15 - 10*s25 - 4*s35) + 2*me2**2*&
          &(3*tt*s25 - 10*s25**2 + ss*(s15 + 12*s25 - s35) - s25*s35 - 2*s35**2 + s15*(tt -&
          & 4*s25 + 2*s35)) + me2*(-2*ss**2*s25 + 2*s15**2*s25 + 3*tt*s25**2 + s15*s25*(tt &
          &+ 4*s25 - 2*s35) - 3*s25**2*s35 + ss*s25*(-s15 + 2*s25 + 3*s35))))/(s25*(4*me2 -&
          & ss - tt + s35)) - (tt*(96*me2**4 + me2*s15*(2*s15**2 + s25*(3*tt - 3*s35) + s15&
          &*(tt + 4*s25 - 2*s35) - 3*ss*(s15 - s35)) + 8*me2**3*(-8*ss + 2*s15 + 11*s25 + 3&
          &*s35) + 2*me2**2*(4*ss**2 - 2*s15**2 + ss*(3*s15 - 8*s25 - 9*s35) + s15*(-3*tt -&
          & 8*s25 + 4*s35) + s25*(-9*tt + 4*s25 + 9*s35))))/(4*me2 - ss - tt + s25) - (tt*s&
          &15*(4*me2**3*(5*s15 + 8*s25 - 2*s35) + 2*me2**2*(s15**2 + tt*s25 + 5*s25**2 + s1&
          &5*(tt + 6*s25 - 2*s35) - 2*tt*s35 - 3*s25*s35 + ss*(-9*s15 - 10*s25 + 9*s35)) + &
          &me2*(-2*s15**3 - 2*tt*s25**2 - 2*s15**2*(3*s25 - 2*s35) + 2*ss**2*(s15 + s25 - s&
          &35) + 3*tt*s25*s35 + 2*s25**2*s35 - 3*s25*s35**2 + s15*(-2*tt*s25 - 4*s25**2 + 8&
          &*s25*s35 + (tt - 2*s35)*s35) + ss*(2*s15**2 - 2*s25**2 - 5*s15*s35 + 3*s35**2)))&
          &)/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + (tt*s15*(4*me2**3*(s15 + 4*s25 -&
          & 4*s35) - 2*me2**2*(-(tt*s15) + s15**2 - tt*s25 + 4*s15*s25 + 3*s25**2 + 2*tt*s3&
          &5 + s25*s35 + 2*s35**2 - ss*(s15 + 5*s35)) + me2*(s25**2*(tt - s35) - 2*ss**2*s3&
          &5 + s25*(3*tt - 3*s35)*s35 + s15*(tt - 2*s35)*s35 + s15**2*(tt + 2*s35) + s15*s2&
          &5*(2*tt + 3*s35) + ss*(s15**2 + s15*(s25 - 2*s35) + 3*s35*(s25 + s35)))))/(s35*(&
          &4*me2 - ss - tt + s35)) - (tt*s15*(-24*me2**3*s25 + 2*me2**2*(10*ss*s25 - 9*s25*&
          &*2 + 2*s25*s35 + s35**2 - 2*s15*(4*s25 + s35)) + me2*(-4*ss**2*s25 - 2*tt**2*s25&
          & + 3*tt*s25**2 + tt*s25*s35 - 3*s25**2*s35 + s25*s35**2 + s15*(3*tt*s25 + tt*s35&
          & - 2*s25*s35) + ss*(-4*tt*s25 + 4*s25**2 + 3*s25*s35 - s35**2 + s15*(3*s25 + s35&
          &)))))/(s25*(tt + s15 - s35)) + (tt*s15*(-8*me2**3*(s15 + s25 - s35) + 2*me2**2*(&
          &-s15**2 - 2*tt*s25 + s25**2 + 6*ss*(s15 + s25 - s35) + 2*tt*s35 + 2*s15*(-tt + s&
          &35)) + me2*(tt*s15**2 - 2*tt**2*s25 + tt*s25**2 - 4*ss**2*(s15 + s25 - s35) + 2*&
          &tt**2*s35 - s25**2*s35 - 2*tt*s35**2 + 2*s25*s35**2 - s15*(2*tt**2 - 2*tt*s25 + &
          &s25*s35) + ss*(s15**2 - 4*tt*s25 + 4*s25**2 + 4*tt*s35 - 3*s25*s35 - 2*s35**2 + &
          &s15*(-4*tt + 5*s25 + s35)))))/((tt + s15 - s35)*(s15 + s25 - s35)) + (s15*(-8*me&
          &2**3*s35 - 2*me2**2*(s15**2 + 6*s25**2 - s35*(8*ss + s35) + 4*s15*(2*s25 + s35))&
          & + me2*(tt*s15**2 + 2*tt*s25**2 - 4*ss**2*s35 - 2*tt**2*s35 + tt*s25*s35 - 2*s25&
          &**2*s35 + 2*tt*s35**2 - s25*s35**2 + s15*(3*tt*s25 + 3*tt*s35 - 3*s25*s35) + ss*&
          &(s15**2 + 2*s15*(s25 + s35) + s35*(-4*tt + 6*s25 + s35)))))/s35)*pvc9(1))/(tt**2&
          &*s15**2) + (128*Pi**2*(8*me2**2 + 4*ss**2 - 4*me2*(4*ss + tt - 2*s15 - 2*s25) + &
          &2*(-tt + s25)*(-tt + s35) - 2*ss*(-2*tt + s15 + 2*s25 + s35) - (2*tt*(ss*s15**2*&
          &(-ss + s15 + s25) + 12*me2**3*(s25 + s35) + me2*s15*(5*ss*s15 + tt*s15 - 2*s15**&
          &2 + 3*tt*s25 + 3*ss*s35 - 2*s15*s35 - 3*s25*s35) + me2**2*(-3*tt*s15 - 9*s15**2 &
          &- 9*tt*s25 - 10*s15*s25 + 3*ss*(s15 - 3*s35) + 2*s15*s35 + 9*s25*s35)))/(s15**2*&
          &(4*me2 - ss - tt + s25)) + (tt*(-96*me2**4 + 8*me2**3*(8*ss + 6*tt + 2*s15 - 5*s&
          &25 - 6*s35) + ss*s15*(ss - s15 - s25)*(tt + s15 - 2*s35) - 2*me2**2*(4*ss**2 - s&
          &15**2 - 12*tt*s25 + 3*s25**2 + 2*ss*(8*tt + 4*s15 - 2*s25 - 7*s35) - 2*tt*s35 + &
          &14*s25*s35 + 4*s35**2 + s15*(-3*tt - 12*s25 + 7*s35)) + me2*(s15**3 + s15**2*(2*&
          &tt + s25 - 3*s35) + 2*ss**2*(2*tt + s15 - s35) - 3*s25*(-tt + s35)*(-tt + s25 + &
          &2*s35) - ss*(4*tt*s25 + s15*(tt + 5*s25 - 6*s35) + 3*tt*s35 - 5*s25*s35 - 6*s35*&
          &*2) + s15*(tt**2 - 4*tt*s25 + 3*s25**2 + 2*tt*s35 + 3*s25*s35 - 4*s35**2))))/(s1&
          &5*s35*(4*me2 - ss - tt + s35)) + (tt*(96*me2**4 - 8*me2**3*(14*ss + 6*tt - 3*s15&
          & - 6*s25 - 7*s35) - ss*s15*(ss - s15 - s25)*(ss + tt + s15 + s25 - 2*s35) + 2*me&
          &2**2*(20*ss**2 - 19*ss*s15 + 6*s15**2 - 14*tt*s25 + s25**2 - s15*(tt + 3*s25 - 2&
          &*s35) - 2*tt*s35 + 14*s25*s35 + 4*s35**2 - 2*ss*(-8*tt + 9*s25 + 9*s35)) + me2*(&
          &-4*ss**3 - 4*s15**3 + ss**2*(-4*tt + 11*s15 + 6*s25 + 5*s35) + s15**2*(-4*tt - 3&
          &*s25 + 6*s35) + ss*(7*tt*s25 - 2*s25**2 + 2*s15*(3*tt + s25 - 5*s35) + 3*tt*s35 &
          &- 5*s25*s35 - 6*s35**2) + s15*(-tt**2 + 3*tt*s25 - s25**2 + tt*s35 - 3*s25*s35 +&
          & s35**2) + 3*s25*(tt**2 - 3*tt*s35 + 2*s35**2))))/(s15*(4*me2 - ss - tt + s25)*(&
          &s15 + s25 - s35)) - (tt*(64*me2**4 + me2**3*(-96*ss + 56*(s15 + s25)) + 2*me2**2&
          &*(24*ss**2 + 4*tt**2 + 7*s15**2 - 8*tt*s25 + 3*s25**2 + s15*(-6*tt + 8*s25 - s35&
          &) - 4*tt*s35 + 4*s25*s35 + 3*s35**2 - 2*ss*(-4*tt + 15*s15 + 13*s25 + 2*s35)) - &
          &s15*(2*ss**3 + s25*(-tt + s25)*(-tt + s35) - ss**2*(-2*tt + 2*s15 + s35) + ss*(t&
          &t**2 + s15**2 + tt*s25 - 2*s25**2 - tt*s35)) - me2*(8*ss**3 - 3*s15**3 + s25*(2*&
          &tt + s25 - 3*s35)*(-tt + s35) + s15**2*(-tt + s25 + s35) - 4*ss**2*(-2*tt + 5*s1&
          &5 + 3*s25 + s35) + s15*(3*s25**2 - s25*s35 + 2*tt*(-tt + s35)) + ss*(4*tt**2 + 1&
          &2*s15**2 - 8*tt*s25 + 4*s25**2 - 4*tt*s35 + 3*s25*s35 + 3*s35**2 + s15*(-10*tt +&
          & 7*s25 + s35)))))/(s15*s25*(tt + s15 - s35)) - (64*me2**4 - 8*me2**3*(8*ss + 4*t&
          &t + s15 - 4*s25 - 3*s35) + 2*me2**2*(8*ss**2 + 4*tt**2 - 5*s15**2 - 12*tt*s25 + &
          &6*s25**2 - 2*s15*(6*tt + 2*s25 - 5*s35) + 4*ss*(6*tt + s15 - 2*s25 - 3*s35) + 8*&
          &s25*s35 - 3*s35**2) + s15*(tt + s15 - 2*s35)*(-2*ss**2 + (-tt + s25)*(tt - s35) &
          &+ ss*(-2*tt + s15 + 2*s25 + s35)) + me2*(-4*s15**3 - 4*ss**2*(2*tt + s15 - s35) &
          &+ 2*s15**2*(tt - 5*s25 + 4*s35) + ss*(-8*tt**2 + 13*s15**2 + 8*tt*s25 + 2*s15*(4&
          &*tt + 5*s25 - 11*s35) + 8*tt*s35 - 10*s25*s35 + s35**2) - (-tt + s35)*(-6*s25**2&
          & + 2*tt*(-2*tt + s35) + s25*(4*tt + s35)) + s15*(-6*s25**2 + 4*tt*(tt - 2*s35) +&
          & 5*s25*(-tt + 3*s35))))/(s15*s35) - (tt*(96*me2**4 - ss*s15*(ss**2 - 2*ss*s15 + &
          &s15**2 - s25**2) + 8*me2**3*(-14*ss + 5*s15 + 7*s25 + s35) + 2*me2**2*(20*ss**2 &
          &- 25*ss*s15 + 9*s15**2 - 2*tt*s25 + s15*(2*tt + s25 - 5*s35) - 4*ss*(5*s25 + s35&
          &)) + me2*(-4*ss**3 + s15**3 + 3*s25**2*(tt - s35) + s15**2*(-2*tt + 3*s35) + ss*&
          &*2*(13*s15 + 6*s25 + 3*s35) - s15*(tt*s25 - 3*tt*s35 + 3*s35**2) - ss*(8*s15**2 &
          &+ s25*(-3*tt + 2*s25) + s15*(-5*tt + s25 + 4*s35)))))/(s15*s25*(4*me2 - ss - tt &
          &+ s35)) + (tt*(64*me2**4 - 8*me2**3*(12*ss + 4*tt - 4*s15 - 5*s25 - 3*s35) + 2*m&
          &e2**2*(24*ss**2 + 4*tt**2 - 14*tt*s25 + 5*s25**2 - 2*tt*s35 + 10*s25*s35 - 2*ss*&
          &(-12*tt + 10*s15 + 11*s25 + 7*s35) + s15*(-10*tt + 4*s25 + 9*s35)) + s15*(-2*ss*&
          &*3 + (-tt + s25)*(tt + 2*s15 + s25 - 2*s35)*(tt - s35) + ss**2*(-4*tt - 2*s15 + &
          &5*s35) + ss*(-3*tt**2 - 3*tt*s15 + s15**2 + tt*s25 + 4*s15*s25 + 2*s25**2 + 6*tt&
          &*s35 - 4*s25*s35 - 2*s35**2)) - me2*(8*ss**3 + 5*s15**3 + s15**2*(-tt + 11*s25 -&
          & 7*s35) - 4*ss**2*(-4*tt + 4*s15 + 3*s25 + 2*s35) + s15*(-6*tt**2 + 7*tt*s25 + 5&
          &*s25**2 + 8*tt*s35 - 14*s25*s35) + (-tt + s35)*(-4*tt**2 + 6*tt*s25 - 5*s25**2 +&
          & 2*tt*s35 - 2*s25*s35) + ss*(12*tt**2 - 7*s15**2 - 16*tt*s25 + 4*s25**2 - 12*tt*&
          &s35 + 13*s25*s35 + 2*s35**2 + s15*(-18*tt + s25 + 21*s35)))))/(s15*(tt + s15 - s&
          &35)*(s15 + s25 - s35)))*pvc9(2))/tt**2 + (128*Pi**2*(2*(tt*s15 + tt*s25 - 2*s15*&
          &s25 - 2*s25**2 - 2*me2*(s15 + 2*s25) + ss*(s15 + 2*s25 - s35) - tt*s35 + s25*s35&
          &) - (tt*(2*s15*s25*(-ss + s15 + s25) + 4*me2**2*(s25 + s35) + me2*(-(tt*s15) + s&
          &15**2 - 3*tt*s25 + 6*s15*s25 + ss*(s15 - 3*s35) + 2*s15*s35 + 3*s25*s35)))/(s15*&
          &(4*me2 - ss - tt + s25)) + (tt*(32*me2**3*s25 + 8*me2**2*s25*(-4*ss + 3*(s15 + s&
          &25)) + s15*s25*(2*ss**2 + tt**2 + s15**2 + s15*(tt - s25) - 2*s25**2 - 2*tt*s35 &
          &+ 2*s25*s35 - ss*(-2*tt + s15 + 2*s35)) + me2*(8*ss**2*s25 + 3*s15*s25*(-2*tt + &
          &3*s25 - s35) - 2*ss*s25*(-4*tt + 8*s15 + 6*s25 + s35) + s15**2*(9*s25 + s35) + 2&
          &*s25*(2*tt**2 - 3*tt*s25 + 2*s25**2 - tt*s35 + s25*s35))))/(s15*s25*(tt + s15 - &
          &s35)) - (tt*(32*me2**3*(s15 + s25 - s35) - 8*me2**2*(4*ss - 2*s15 - 3*s25)*(s15 &
          &+ s25 - s35) + s15*(s15 + s25 - s35)*(2*ss**2 + tt**2 + 2*tt*s15 - 3*s15*s25 - 2&
          &*s25**2 + ss*(2*tt + s15 - 2*s35) - 2*tt*s35 + 2*s25*s35) + me2*(4*tt**2*s25 - 6&
          &*tt*s25**2 + 4*s25**3 + 8*ss**2*(s15 + s25 - s35) - 4*tt**2*s35 + 4*tt*s25*s35 -&
          & 2*s25**2*s35 + 2*tt*s35**2 - 2*s25*s35**2 - s15**2*(4*tt + s25 + s35) - 2*ss*(s&
          &15 + s25 - s35)*(-4*tt + 6*s15 + 6*s25 + s35) + s15*(4*tt**2 - 10*tt*s25 + 3*s25&
          &**2 + 2*tt*s35 + 3*s25*s35))))/(s15*(tt + s15 - s35)*(s15 + s25 - s35)) + (tt*(-&
          &48*me2**3*(s15 + s25 - s35) + s15*(ss + s25)*(-ss + s15 + s25)*(s15 + s25 - s35)&
          & + me2*(-4*ss**2*(s15 + s25 - s35) + s15**2*(tt + 7*s25 - s35) - 2*s25*(s25 - s3&
          &5)*(-3*tt + s25 + 3*s35) + ss*(7*s15**2 + 14*s15*s25 + 6*s25**2 - s15*s35 - 6*s3&
          &5**2) + s15*(7*tt*s25 + 5*s25**2 - 18*s25*s35 + s35**2)) + 2*me2**2*(-8*s15**2 +&
          & 16*ss*(s15 + s25 - s35) + s15*(-27*s25 + s35) + 4*(-4*s25**2 + 3*s25*s35 + s35*&
          &*2))))/(s15*(4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + (tt*(48*me2**3*s25 + s1&
          &5*s25*(ss**2 - 2*ss*s15 + s15**2 - s25**2) + me2**2*(-2*s15**2 + 8*s25*(-4*ss + &
          &4*s25 + s35) + s15*(26*s25 + 8*s35)) + me2*(4*ss**2*s25 + s15**2*(-tt + 4*s25 - &
          &2*s35) + 2*s25**2*(-3*tt + s25 + 3*s35) + s15*(-5*tt*s25 - 2*s25**2 + 3*s25*s35 &
          &+ 3*s35**2) - ss*(s15**2 + s15*(10*s25 - s35) + 6*s25*(s25 + s35)))))/(s15*s25*(&
          &4*me2 - ss - tt + s35)) + (tt*(-48*me2**3*s35 + s15*(ss + s25)*(-ss + s15 + s25)&
          &*s35 + 2*me2**2*(s15**2 + s15*(4*s25 - 7*s35) + 4*(4*ss - 4*s25 - s35)*s35) + me&
          &2*(s15**3 - 4*ss**2*s35 + s15**2*(tt - s25 + s35) - 2*s25*s35*(-3*tt + s25 + 3*s&
          &35) + s15*(tt*s25 - 3*s25**2 + 12*s25*s35 - 4*s35**2) + ss*(s15**2 + 3*s15*s35 +&
          & 6*s35*(s25 + s35)))))/(s15*s35*(4*me2 - ss - tt + s35)) - (32*me2**3*s35 - 8*me&
          &2**2*(4*ss - 2*s15 - 3*s25)*s35 + s15*s35*(2*ss**2 + 2*ss*tt + tt**2 + tt*s15 - &
          &2*s15*s25 - 2*s25**2 - 2*ss*s35 - 2*tt*s35 + 2*s25*s35) + 2*me2*(s15**2*(3*s25 +&
          & s35) + s15*(3*s25**2 - 7*ss*s35 - 3*tt*s35 - s25*s35) + s35*(4*ss**2 + 2*tt**2 &
          &- 3*tt*s25 + 2*s25**2 - tt*s35 + s25*s35 - ss*(-4*tt + 6*s25 + s35))))/(s15*s35)&
          &)*pvc9(3))/tt**2 + (256*Pi**2*((tt*(4*me2**2 - 2*me2*(ss + 2*tt + s15) - s25*(s1&
          &5 + s25 - s35) + ss*(tt + s15 + s25 - s35)))/((tt + s15 - s35)*(s15 + s25 - s35)&
          &) - (tt*(4*me2**2 + tt**2 + ss*(tt - s25) - tt*s25 + s15*s25 + s25**2 - 2*me2*(s&
          &s + tt - s15 - 2*s25 - s35) - tt*s35))/(s25*(tt + s15 - s35)) + (-4*me2**2 - 2*s&
          &s**2 + tt*s15 + tt*s25 - tt*s35 - s25*s35 + 2*me2*(4*ss + 2*tt - s15 - 2*s25 + s&
          &35) + ss*(-2*tt + s15 + 2*s25 + s35))/s35 - (me2*tt*(-(tt*s15) + 2*s15**2 - 3*tt&
          &*s25 + 2*s15*s25 + 2*s25**2 - s25*s35 + ss*(-2*s15 - 2*s25 + s35) + me2*(6*s15 +&
          & 4*(s25 + s35))))/(s15*s25*(4*me2 - ss - tt + s35)) + (tt*(s15*(-(s25*(s15 + s25&
          & - s35)) + ss*(tt + s15 + s25 - s35)) + me2*(-2*tt*s15 + tt*s25 + 2*s15*s25 + 2*&
          &s25**2 - 2*tt*s35 - s15*s35 - s25*s35 + ss*(-2*s15 - 2*s25 + s35)) + me2**2*(6*s&
          &15 + 4*(s25 + s35))))/(s15*(4*me2 - ss - tt + s25)*(s15 + s25 - s35)) - (tt*(me2&
          &*(-5*ss*s15 - 2*tt*s15 + s15**2 + tt*s25 + 4*s15*s25 - 3*ss*s35 - 2*tt*s35 + s15&
          &*s35 + 3*s25*s35) + me2**2*(6*s15 + 4*(s25 + s35)) + s15*(ss**2 + s25*s35 - ss*(&
          &-tt + s25 + s35))))/(s15*s35*(4*me2 - ss - tt + s35)))*pvc9(4))/tt**2 + (128*Pi*&
          &*2*(-(((2*me2 - ss)*tt*(ss*s15*(ss - s15 - s25) - me2*(5*ss*s15 + tt*s15 - 2*s15&
          &**2 + 3*tt*s25 + 3*ss*s35 - 2*s15*s35 - 3*s25*s35) + me2**2*(6*s15 + 4*(s25 + s3&
          &5))))/(s15*s25*(4*me2 - ss - tt + s35))) + (-8*me2**3 + 8*me2**2*(2*ss + tt) + 2&
          &*ss**2*(tt + s15) - 2*ss*(tt + s15)*(-tt + s35) - tt*(tt + s15 - s35)*(-tt + s35&
          &) + 2*me2*(-2*ss**2 - 2*tt**2 + s15**2 + tt*s35 + s35**2 + ss*(-6*tt - 4*s15 + 2&
          &*s35)))/s35 - (tt*(8*me2**3 - 2*ss**3 - 4*me2**2*(5*ss + tt - 2*s15 - 3*s25 - s3&
          &5) - s25*(tt + s15 - s35)*(-tt + s35) + ss**2*(s15 + 2*(-tt + s25 + s35)) + ss*(&
          &s15*s25 - (-tt + s35)*(-tt + 2*s25 + s35)) + 2*me2*(6*ss**2 - s15*s25 + (-tt + s&
          &35)*(-tt + 3*s25 + s35) - ss*(-3*tt + 3*s15 + 5*s25 + 3*s35))))/(s25*(tt + s15 -&
          & s35)) + (tt*(8*me2**3 - 2*ss**3 + (-tt + s25)*(-tt + s35)**2 + 4*me2**2*(-5*ss &
          &- 2*tt + s15 + s25 + s35) + ss**2*(s15 + 2*(-2*tt + s25 + s35)) - ss*(s15**2 + s&
          &15*(s25 - s35) + (-tt + s35)*(-3*tt + 2*s25 + s35)) + 2*me2*(6*ss**2 + s15**2 + &
          &s15*(-tt + s25) + 2*(-tt + s25)*(-tt + s35) - ss*(-7*tt + 2*s15 + 3*s25 + 4*s35)&
          &)))/((tt + s15 - s35)*(s15 + s25 - s35)) - (tt*(-(ss*s15*(ss*(tt + s15) - s15*s3&
          &5)) + 4*me2**3*(3*s15 + 2*(s25 + s35)) - 2*me2**2*(3*s15**2 + s15*(4*tt + 4*s25 &
          &- 3*s35) - (-tt + s35)*(5*s25 + 2*s35) + ss*(5*s15 + 3*s35)) + me2*(2*ss**2*s15 &
          &+ s15**2*(tt + 2*s25 - 4*s35) - s15*(tt + 5*s25 - 2*s35)*(-tt + s35) + 3*s25*(-t&
          &t + s35)**2 + ss*(5*tt*s15 + 5*s15**2 + 3*(tt - s35)*s35))))/(s15*s35*(4*me2 - s&
          &s - tt + s35)) + (tt*(-(ss*s15*(ss**2 - ss*(-tt + s15 + s25) + s15*(s15 + s25 - &
          &s35))) + 4*me2**3*(3*s15 + 2*(s25 + s35)) - 2*me2**2*(s15*(4*tt + 2*s25 - 5*s35)&
          & - (-tt + s35)*(5*s25 + 2*s35) + ss*(8*s15 + 2*s25 + 5*s35)) + me2*(2*s15**3 + 2&
          &*s15**2*(s25 - s35) - s15*(tt + 2*s25 - 2*s35)*(-tt + s35) + 3*s25*(-tt + s35)**&
          &2 + ss**2*(7*s15 + 3*s35) + ss*(-2*s15**2 + s15*(6*tt - 5*s35) - 3*(-tt + s35)*(&
          &s25 + s35)))))/(s15*(4*me2 - ss - tt + s25)*(s15 + s25 - s35)))*pvc9(5))/tt**2 +&
          & (128*Pi**2*((tt**2*s15 + tt*s15**2 + tt**2*s25 + tt*s15*s25 + 4*me2**2*(s15 + 2&
          &*s25 - s35) - 2*ss**2*s35 - 2*tt**2*s35 - 2*tt*s15*s35 - s15*s25*s35 + 2*tt*s35*&
          &*2 - s25*s35**2 + ss*(s15**2 + s15*(tt + 2*s25) + s35*(-3*tt + s35) + 2*s25*(tt &
          &+ s35)) - 2*me2*(s15**2 + 3*tt*s25 + ss*(s15 + 2*s25 - 5*s35) - 2*tt*s35 + s25*s&
          &35 - s35**2 + 2*s15*(tt + s25 + s35)))/s35 - (tt*(-4*me2**2*(s15 + 3*s25) + ss**&
          &2*(-s15 - 4*s25 + s35) + s25*(-tt**2 + s15**2 + 2*tt*s25 + s15*(tt + s25 - 2*s35&
          &) - 2*s25*s35 + s35**2) + 2*me2*(2*tt*s25 - 5*s25**2 + s15*(tt - 5*s25 - s35) + &
          &ss*(2*s15 + 8*s25 - s35) - tt*s35 + s25*s35 + s35**2) + ss*(4*s25**2 + (tt - s35&
          &)*s35 + s25*(-3*tt + s35) + s15*(-tt + 3*s25 + s35))))/(s25*(tt + s15 - s35)) - &
          &(tt*(2*tt**2*s15 + 2*tt**2*s25 - 2*tt*s15*s25 + s15**2*s25 - 2*tt*s25**2 + s15*s&
          &25**2 + ss**2*(3*s15 + 4*s25 - 3*s35) + 4*me2**2*(2*s15 + 3*s25 - s35) - 2*tt**2&
          &*s35 - 2*tt*s15*s35 + s15*s25*s35 + 2*s25**2*s35 + 2*tt*s35**2 - 2*s25*s35**2 - &
          &2*me2*(-2*s15**2 + 4*tt*s25 - 3*s25**2 + ss*(6*s15 + 8*s25 - 5*s35) - 2*tt*s35 +&
          & s25*s35 + s15*(3*tt - 5*s25 + s35)) - ss*(s15**2 + 4*s25**2 - 2*s35*(-2*tt + s3&
          &5) - s25*(5*tt + s35) + s15*(-4*tt + 5*s25 + s35))))/((tt + s15 - s35)*(s15 + s2&
          &5 - s35)) + (tt*(2*ss*s15*(ss - s15 - s25)*s25 + 2*me2**2*(s15**2 + 7*s15*s25 + &
          &2*s25*(s25 + s35)) - me2*(s25*(-4*s15**2 + s15*(tt - 2*s25 - 2*s35) + 3*s25*(tt &
          &- s35)) + ss*(s15**2 + 11*s15*s25 + 3*s25*s35))))/(s15*s25*(4*me2 - ss - tt + s3&
          &5)) - (tt*(-(s15*(-(ss**2*s35) - s15*s25*s35 + ss*s25*(tt + s35) + ss*s15*(s25 +&
          & s35))) + me2**2*(-2*s15**2 + 4*s35*(s25 + s35) + s15*(-8*s25 + 6*s35)) + me2*(s&
          &15**3 + s15**2*(tt + 4*s25 + s35) - 3*s35*(tt*s25 + ss*s35 - s25*s35) + s15*(2*s&
          &s*s25 + 4*tt*s25 - 5*ss*s35 - tt*s35 - 4*s25*s35 + 2*s35**2))))/(s15*s35*(4*me2 &
          &- ss - tt + s35)) - (tt*(2*me2**2*(4*s15**2 + 9*s15*s25 + 2*s25**2 - s15*s35 - 2&
          &*s35**2) + s15*(s15*s25*(s15 + s25 - s35) + ss**2*(s15 + 2*s25 - s35) + ss*(-s15&
          &**2 + s15*(-3*s25 + s35) + s25*(tt - 2*s25 + s35))) + me2*(2*s15**3 + 3*s25*(s25&
          & - s35)*(-tt + s35) + s15**2*(-2*tt + 2*s25 + s35) + s15*(-8*tt*s25 + tt*s35 + 9&
          &*s25*s35 - 2*s35**2) + ss*(-6*s15**2 + 3*s35*(-s25 + s35) + s15*(-11*s25 + 2*s35&
          &)))))/(s15*(4*me2 - ss - tt + s25)*(s15 + s25 - s35)))*pvc9(6))/tt**2 + (128*Pi*&
          &*2*(tt**2*s15 + tt**2*s25 - 2*tt*s15*s25 - 2*tt*s25**2 + 2*s15*s25**2 + 2*s25**3&
          & + 8*me2**2*(s15 + 2*s25) + ss**2*(s15 + 2*s25 - s35) - tt**2*s35 + 2*tt*s25*s35&
          & - s25**2*s35 + me2*(-5*tt*s15 - 4*tt*s25 + 10*s15*s25 + 12*s25**2 - 2*ss*(3*s15&
          & + 6*s25 - 2*s35) + 4*tt*s35 - 4*s25*s35) + ss*(s15*(2*tt - 3*s25) - 4*s25**2 - &
          &2*tt*s35 + 2*s25*(tt + s35)))*pvc9(7))/(tt**2*(4*me2 - ss - tt + s25)))/(16.*Pi*&
          &*2)

          !print*,"2",diags

  end if

  if (tri18) then

  diags =  diags + (4*(4*me2*tt + (2*tt*s15*(-2*me2 + tt + s15 - s35))/s35 + 2*s15*s35 - (2&
          &*s15*(4*me2**2*tt + tt*(ss - s15)*(ss - s15 - s25) + me2*(tt*(-4*ss + 4*s15 + 3*&
          &s25) + s35**2)))/(s25*(4*me2 - ss - tt + s35)) - (s15*(tt*s15*(2*tt + 2*s15 - s3&
          &5) + s25*(2*tt*(tt + s15) - 3*tt*s35 + s35**2) + 2*me2*(2*tt**2 + 2*tt*s15 - 2*t&
          &t*s35 - s15*s35 + s35**2) + ss*(-2*tt**2 + 2*tt*s35 - s35**2 + s15*(-2*tt + s35)&
          &)))/(s25*(tt + s15 - s35)) - (s15*(-8*me2**2*tt - 2*ss**2*tt + (tt*s15 + s25*(tt&
          & - s35))*s35 + ss*(2*tt*(-tt + s25) - s15*s35 + s35**2) + 2*me2*(tt*(4*ss + 2*tt&
          & - s25) - tt*s35 - 2*s35**2 + s15*(tt + s35))))/((4*me2 - ss - tt + s25)*(s15 + &
          &s25 - s35)) + (8*me2**2*tt + 2*me2*(-s15**2 + 2*tt*(-ss + s25) + s15*(tt + s35))&
          & + s15*(tt*s15 + ss*(s15 - s35) + s25*(tt + s35)))/(4*me2 - ss - tt + s25) + (s1&
          &5*(s25*(2*tt*(tt + s15) - 3*tt*s35 + s35**2) + 2*me2*(2*tt**2 + 2*tt*s15 - 2*tt*&
          &s35 - s15*s35 + s35**2) + ss*(-2*tt**2 + 2*tt*s35 - s35**2 + s15*(-2*tt + s35)) &
          &- tt*(2*tt*(tt - s35) + s15*(2*tt + s35))))/((tt + s15 - s35)*(s15 + s25 - s35))&
          & + (s15*(-8*me2**2*tt + (tt*s15 + s25*(tt - s35))*s35 + ss*(-2*tt**2 + s35**2 - &
          &s15*(2*tt + s35)) + 2*me2*(2*tt*(ss + tt) - tt*s35 - s35**2 + s15*(2*tt + s35)))&
          &)/(s35*(4*me2 - ss - tt + s35))))/(tt*s15**2) + ((64*Pi**2*(-(tt*s15) + tt*s25 +&
          & 2*me2*(5*s15 - s35) + 2*s15*s35 - s25*s35 + ss*(-3*s15 + s35))*pva1(1))/(tt*s15&
          &*s35*(4*me2 - ss - tt + s35)) + (64*Pi**2*((-2*s15*(-16*me2**3 - tt**2*s15 + tt*&
          &s15**2 - 2*tt**2*s25 + 2*tt*s15*s25 + 2*tt*s25**2 + 4*me2**2*(4*ss + 5*tt - 2*s2&
          &5 - 3*s35) + 2*ss**2*(tt + s15 - s35) + tt*s15*s35 + 2*tt*s25*s35 - 2*s15*s25*s3&
          &5 - 2*s25**2*s35 + 2*ss*(-tt + 2*s25)*(-tt + s35) - 2*me2*(2*ss**2 + 3*tt**2 - 3&
          &*tt*s15 - s15**2 - 5*tt*s25 + ss*(6*tt + 3*s15 - 2*s25 - 5*s35) - tt*s35 + 2*s15&
          &*s35 + 5*s25*s35)))/s35 + (tt*(-48*me2**3 + 4*me2**2*(8*ss + 5*tt - 3*s15 - 8*s2&
          &5) - 2*me2*(2*ss**2 + 2*s15**2 + ss*(2*tt - 4*s25) + 2*s25*(-tt + s25) + s15*(tt&
          & + 4*s25 - 2*s35)) + s15*(s15*(tt - 2*s25) + ss*(s15 + 2*s25 - s35) + s25*(tt - &
          &2*s25 + s35))))/(4*me2 - ss - tt + s25) + (tt*s15*(48*me2**3 - (-tt + 2*s15 + 2*&
          &s25)*(tt*s15 + s25*(tt - s35)) - 4*me2**2*(8*ss + 11*tt + s15 - 6*s25 - 8*s35) -&
          & 2*ss**2*(tt + s15 - s35) + ss*(-4*s25*(-tt + s35) + tt*(-2*tt + s15 + s35)) + 2&
          &*me2*(2*ss**2 + 5*tt**2 - 8*tt*s25 + 2*ss*(5*tt + 3*s15 - s25 - 5*s35) - 3*tt*s3&
          &5 + 10*s25*s35 + 2*s15*(-2*tt + s25 + s35))))/(s35*(4*me2 - ss - tt + s35)) + (t&
          &t*s15*(48*me2**3 - 2*(ss - s15)*(ss + tt - s25)*(ss - s15 - s25) + 4*me2**2*(-14&
          &*ss - 5*tt + 11*s15 + 11*s25 + s35) + me2*(20*ss**2 + 12*s15**2 + s15*(-11*tt + &
          &20*s25 - 4*s35) + s25*(-9*tt + 10*s25 + s35) - ss*(-14*tt + 27*s15 + 30*s25 + s3&
          &5))))/(s25*(4*me2 - ss - tt + s35)) + 2*(-16*me2**3 + 4*me2**2*(4*ss + 3*tt - 3*&
          &s15 - 4*s25) + s15*(s15*(tt - 2*s25) + ss*(s15 + 2*s25 - s35) + s25*(tt - 2*s25 &
          &+ s35)) + me2*(-4*ss**2 - 4*s15**2 + 4*(tt - s25)*s25 + 4*ss*(-tt + s15 + 2*s25)&
          & + s15*(-8*s25 + 2*s35))) + (tt*s15*(32*me2**3 - 4*ss**3 - 2*tt*s15**2 - 3*tt*s1&
          &5*s25 + 2*s15**2*s25 - tt*s25**2 + 2*s15*s25**2 - 8*me2**2*(6*ss + 3*tt - 5*s15 &
          &- 5*s25 - s35) + s25**2*s35 + ss**2*(-4*tt + 7*s15 + 8*s25 + s35) - ss*(2*s15**2&
          & + s25*(-5*tt + 4*s25 + 2*s35) + s15*(-7*tt + 7*s25 + 2*s35)) + 2*me2*(12*ss**2 &
          &+ 5*s15**2 + 3*s25*(-3*tt + 2*s25 + s35) + s15*(-9*tt + 11*s25 + 2*s35) - ss*(-1&
          &0*tt + 17*s15 + 18*s25 + 3*s35))))/(s25*(tt + s15 - s35)) - (tt*s15*(32*me2**3 -&
          & 4*ss**3 + 3*tt**2*s15 + tt*s15**2 + 3*tt**2*s25 - 2*tt*s15*s25 - 2*s15**2*s25 -&
          & 3*tt*s25**2 - 2*s15*s25**2 - 8*me2**2*(6*ss + 5*tt - s15 - 3*s25 - 3*s35) - 2*t&
          &t*s15*s35 - 3*tt*s25*s35 + 3*s15*s25*s35 + 3*s25**2*s35 + ss**2*(-8*tt + s15 + 8&
          &*s25 + 3*s35) + ss*(-4*tt**2 + s15**2 + 11*tt*s25 - 4*s25**2 + 3*tt*s35 - 6*s25*&
          &s35 - s15*(-4*tt + s25 + s35)) + 2*me2*(12*ss**2 + 6*tt**2 - 2*s15**2 - 13*tt*s2&
          &5 + 2*s25**2 - 4*tt*s35 + 9*s25*s35 + s15*(-7*tt + s25 + 3*s35) - ss*(-18*tt + 3&
          &*s15 + 14*s25 + 9*s35))))/((tt + s15 - s35)*(s15 + s25 - s35)) - (tt*s15*(48*me2&
          &**3 - 2*ss**3 - (-tt + s15 + s25)*(tt*s15 + s25*(tt - s35)) - 4*me2**2*(14*ss + &
          &11*tt - 5*s15 - 9*s25 - 5*s35) + ss**2*(-4*tt + s15 + 4*s25 + s35) + ss*(-s15**2&
          & + s15*(2*tt - 3*s25 + s35) - (-tt + 2*s25)*(-2*tt + s25 + s35)) + me2*(20*ss**2&
          & + 10*tt**2 - 11*tt*s15 + 6*s15**2 - 23*tt*s25 + 10*s15*s25 + 6*s25**2 - 4*tt*s3&
          &5 - 4*s15*s35 + 11*s25*s35 - ss*(-30*tt + 9*s15 + 26*s25 + 11*s35))))/((4*me2 - &
          &ss - tt + s25)*(s15 + s25 - s35)))*pvb12(1))/(tt**2*s15**2) + (64*Pi**2*((-2*(4*&
          &me2**2 - 2*me2*(ss - s25) + s15*s25))/(4*me2 - ss - tt + s25) + (2*(2*me2*(-tt +&
          & s15) + s15*(s15 - s35)))/tt - (2*s15*(2*me2*(-tt + s15) + tt*(tt - s35)))/(tt*s&
          &35) + (2*s15*(4*me2**2*tt + tt*(ss - s15)*(ss - s25) + me2*(-4*ss*tt + 3*tt*s15 &
          &+ s15**2 + 3*tt*s25 - 2*s15*s35 + s35**2)))/(tt*s25*(4*me2 - ss - tt + s35)) - (&
          &s15*(-2*tt**3 + 2*tt*s15**2 + 2*tt**2*s25 + 2*tt*s15*s25 + 2*tt**2*s35 - 3*tt*s1&
          &5*s35 - 3*tt*s25*s35 + s25*s35**2 + ss*(-2*tt**2 + 2*tt*s35 + s15*s35 - s35**2) &
          &+ me2*(4*tt**2 - 4*tt*s35 - 2*s15*s35 + 2*s35**2)))/(tt*(tt + s15 - s35)*(s15 + &
          &s25 - s35)) + (s15*(2*tt**2*s15 + 2*tt**2*s25 + 2*tt*s15*s25 - tt*s15*s35 - 3*tt&
          &*s25*s35 + s25*s35**2 + ss*(-2*tt**2 + 2*tt*s35 + s15*s35 - s35**2) + me2*(4*tt*&
          &*2 - 4*tt*s35 - 2*s15*s35 + 2*s35**2)))/(tt*s25*(tt + s15 - s35)) - (s15*(8*me2*&
          &*2*tt + 2*ss**2*tt + ss*(2*tt*(tt - s25) + s15*s35 - s35**2) + 2*me2*(s15**2 + t&
          &t*(-4*ss - 2*tt + s25) + tt*s35 - 3*s15*s35 + 2*s35**2) + s35*(-(tt*s15) + s25*(&
          &-tt + s35))))/(tt*(4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + (s15*(8*me2**2*tt&
          & + ss*(2*tt + s15 - s35)*(tt + s35) + (-tt + s35)*(-(tt*s15) + s25*(-tt + s35)) &
          &- 2*me2*(2*tt*(ss + tt) - s35**2 + s15*(2*tt + s35))))/(tt*s35*(4*me2 - ss - tt &
          &+ s35)))*pvb12(2))/s15**2 + (64*Pi**2*(8*me2**2*(-3*tt + s15) + 4*me2*(-tt**2 + &
          &s15**2 + s15*(tt - 2*s35)) - 2*tt*s15*s35 + (tt*s15*(-4*me2**2*(5*tt + s15) - 2*&
          &ss**2*(tt + s15 - s35) + (2*tt + s15 - s35)*(tt*s15 + tt*s25 - s25*s35) + me2*(1&
          &0*tt**2 - 5*tt*s15 - 2*s15**2 - 19*tt*s25 - 6*s15*s25 + ss*(14*tt + 9*s15 - 11*s&
          &35) - 2*tt*s35 + 2*s15*s35 + 7*s25*s35) + ss*(-2*tt**2 + s15**2 + 4*tt*s25 + 2*s&
          &15*s25 + 2*tt*s35 - 2*s25*s35 - s35**2)))/((4*me2 - ss - tt + s25)*(s15 + s25 - &
          &s35)) + (tt*s15*(-2*tt**2*s15 + tt*s15**2 - 2*tt**2*s25 + 3*tt*s15*s25 + 2*tt*s2&
          &5**2 + 4*me2**2*(5*tt + 4*s15 - 9*s35) + 2*ss**2*(s15 - 2*s35) + tt*s15*s35 + 3*&
          &tt*s25*s35 - s15*s25*s35 - 2*s25**2*s35 - s25*s35**2 + ss*(2*tt**2 - s15**2 - 4*&
          &tt*s25 - 2*s15*s25 - 2*tt*s35 + 2*s15*s35 + 6*s25*s35 + s35**2) - 2*me2*(5*tt**2&
          & - 3*s15**2 - 9*tt*s25 + ss*(2*tt + 7*s15 - 13*s35) - 2*s15*(tt + 2*s25 - 3*s35)&
          & - 3*tt*s35 + 11*s25*s35 + s35**2)))/(s35*(4*me2 - ss - tt + s35)) + (tt*s15*(2*&
          &tt*(ss - s15)*(ss - s15 - s25) - 4*me2**2*(-5*tt + s15 - 2*s35) + me2*(13*tt*s15&
          & - 2*s15**2 + 5*tt*s25 - 2*s15*s25 - ss*(14*tt + s15 - 3*s35) + 2*s15*s35 - 3*s2&
          &5*s35 + 2*s35**2)))/(s25*(4*me2 - ss - tt + s35)) - (2*s15*(-4*me2**2*(3*tt + s1&
          &5 - 4*s35) - 2*ss**2*(s15 - 2*s35) + ss*(s15**2 + 4*tt*s25 + 2*s15*(s25 - s35) +&
          & 2*tt*s35 - 6*s25*s35 - s35**2) + 2*me2*(2*tt**2 - 2*s15**2 - 4*tt*s25 - s15*(tt&
          & + 2*s25 - 4*s35) + 4*ss*(s15 - 2*s35) - 2*tt*s35 + 6*s25*s35 + s35**2) + (-tt +&
          & s35)*(-tt**2 + 2*s25**2 + s15*(-2*tt + s25) + s25*(-2*tt + s35))))/s35 - (tt*s1&
          &5*(-2*tt**2*s15 - 3*tt*s15**2 - 2*tt**2*s25 - 3*tt*s15*s25 + 2*ss**2*(s15 - s35)&
          & + tt*s15*s35 + 3*tt*s25*s35 + s15*s25*s35 - s25*s35**2 + 4*me2**2*(-6*tt + s15 &
          &+ s35) + ss*(2*tt**2 + 4*tt*s15 - s15**2 - 2*tt*s25 - 2*s15*s25 - 2*tt*s35 + 2*s&
          &25*s35 + s35**2) + 2*me2*(-2*tt**2 - 9*tt*s15 + s15**2 - 4*tt*s25 + 2*s15*s25 + &
          &2*tt*s35 + s25*s35 - s35**2 + ss*(6*tt - 3*s15 + s35))))/(s25*(tt + s15 - s35)) &
          &- (tt*(4*me2**2*(5*tt + s15) + s15*(tt*s15 + ss*(s15 - s35) + s25*(tt + s35)) + &
          &me2*(-4*ss*(tt + s15) + 4*tt*s25 + s15*(-2*tt + 8*s25 + 4*s35))))/(4*me2 - ss - &
          &tt + s25) - (tt*s15*(-2*tt**3 - 4*tt**2*s15 - tt*s15**2 + tt*s15*s25 + 4*me2**2*&
          &(6*tt + 3*s15 - 5*s35) + 2*ss**2*(s15 - s35) + 2*tt**2*s35 + tt*s15*s35 + tt*s25&
          &*s35 + s15*s25*s35 - s25*s35**2 - ss*(2*tt**2 + s15**2 + 2*tt*s25 + 2*s15*(tt + &
          &s25) - 2*s25*s35 - s35**2) + 2*me2*(-4*tt**2 + s15**2 + 8*tt*s25 + 2*s15*(tt + s&
          &25 - s35) + 3*tt*s35 - 5*s25*s35 - s35**2 + ss*(-6*tt - 5*s15 + 7*s35))))/((tt +&
          & s15 - s35)*(s15 + s25 - s35)))*pvb5(1))/(tt**2*s15**2) + (64*Pi**2*(32*me2**3 +&
          & 16*me2**2*(-2*ss + s15 + 2*s25) + 4*me2*(2*ss**2 + s15**2 + 2*s25*(-tt + s25) -&
          & 2*ss*(-tt + s15 + 2*s25) + s15*(-4*tt + 4*s25 - s35)) - 2*s15*(-2*s15*(-tt + s2&
          &5) + ss*(s15 + 2*s25 - s35) + s25*(tt - 2*s25 + s35)) - (2*tt*s15*(24*me2**3 - (&
          &ss - s15)*(ss**2 + ss*(tt - s15 - 2*s25) + s15*(-2*tt + s25) + s25*(-tt + s25)) &
          &+ 2*me2**2*(-14*ss - 2*tt + 8*s15 + 11*s25 + s35) + me2*(10*ss**2 + 6*s15**2 + s&
          &15*(-4*tt + 11*s25 - 5*s35) + s25*(-3*tt + 5*s25 - s35) + ss*(4*tt - 14*s15 - 15&
          &*s25 + s35))))/(s25*(4*me2 - ss - tt + s35)) - (2*tt*(-24*me2**3 + 4*me2**2*(4*s&
          &s + tt - 4*s25) - 2*me2*(ss**2 + ss*(tt + s15 - 2*s25) - tt*s25 + s25**2 - s15*(&
          &tt + s35)) + s15*(s15*(tt - s25) + ss*(s15 + s25 - s35) + s25*(-s25 + s35))))/(4&
          &*me2 - ss - tt + s25) - (tt*s15*(32*me2**3 - 4*ss**3 - 5*tt*s15**2 - 4*tt*s15*s2&
          &5 + 2*s15**2*s25 - tt*s25**2 + 2*s15*s25**2 + s15*s25*s35 + s25**2*s35 - 4*me2**&
          &2*(12*ss - 11*s15 - 10*s25 + s35) + ss**2*(-4*tt + 7*s15 + 8*s25 + s35) + 2*me2*&
          &(12*ss**2 - 8*tt*s15 + 4*s15**2 - 3*tt*s25 + 13*s15*s25 + 6*s25**2 - 2*ss*(-2*tt&
          & + 9*s15 + 9*s25) + s15*s35) - ss*(3*s15**2 + s15*(-9*tt + 9*s25 + s35) + s25*(-&
          &5*tt + 4*s25 + 2*s35))))/(s25*(tt + s15 - s35)) + (tt*s15*(32*me2**3 - 4*ss**3 +&
          & 5*tt**2*s15 + 4*tt*s15**2 + 3*tt**2*s25 - tt*s15*s25 - 2*s15**2*s25 - 3*tt*s25*&
          &*2 - 2*s15*s25**2 - 4*me2**2*(12*ss + 4*tt - 3*s15 - 6*s25 - 3*s35) - 4*tt*s15*s&
          &35 - 3*tt*s25*s35 + 2*s15*s25*s35 + 3*s25**2*s35 + ss**2*(-8*tt + s15 + 8*s25 + &
          &3*s35) + ss*(-4*tt**2 + 2*s15**2 + 11*tt*s25 - 4*s25**2 + s15*(6*tt + s25 - 2*s3&
          &5) + 3*tt*s35 - 6*s25*s35) + 2*me2*(12*ss**2 - s15**2 - 7*tt*s25 + 2*s25**2 - s1&
          &5*(7*tt + s25 - 2*s35) - tt*s35 + 6*s25*s35 - 2*ss*(-6*tt + 2*s15 + 7*s25 + 3*s3&
          &5))))/((tt + s15 - s35)*(s15 + s25 - s35)) + (tt*s15*(48*me2**3 - 4*me2**2*(14*s&
          &s + 8*tt - 2*s15 - 9*s25 - 5*s35) - (ss + tt - s25)*(2*ss**2 - tt*s15 - tt*s25 +&
          & s25*s35 - ss*(-2*tt + s15 + 2*s25 + s35)) + 2*me2*(10*ss**2 + 2*tt**2 - tt*s15 &
          &+ 3*s15**2 - 7*tt*s25 + 2*s15*s25 + 3*s25**2 - 2*tt*s35 - 4*s15*s35 + 4*s25*s35 &
          &- ss*(-12*tt + 5*s15 + 13*s25 + 4*s35))))/((4*me2 - ss - tt + s25)*(s15 + s25 - &
          &s35)) - (2*s15*(16*me2**3 + 3*tt**2*s15 + 2*tt**2*s25 - 2*tt*s15*s25 - 2*tt*s25*&
          &*2 + 2*ss*(-tt + s15 + 2*s25)*(tt - s35) - 2*tt*s15*s35 - 2*tt*s25*s35 + 2*s15*s&
          &25*s35 + 2*s25**2*s35 + 2*ss**2*(-tt + s35) + 4*me2**2*(-4*ss - 2*tt + s15 + 2*s&
          &25 + 3*s35) + 2*me2*(2*ss**2 - 5*tt*s15 - 5*tt*s25 - tt*s35 + 2*s15*s35 + 5*s25*&
          &s35 - ss*(-6*tt + s15 + 2*s25 + 5*s35))))/s35 + (tt*s15*(-48*me2**3 + (-2*tt + s&
          &15 + 2*s25)*(tt*s15 + s25*(tt - s35)) + 4*me2**2*(8*ss + 8*tt - 3*s15 - 6*s25 - &
          &7*s35) - 2*ss**2*(-tt + s35) + ss*(s15**2 + s15*s35 + 2*(-tt + 2*s25)*(-tt + s35&
          &)) - 2*me2*(2*ss**2 + 2*tt**2 - 4*tt*s15 + s15**2 - 7*tt*s25 - 4*tt*s35 + 3*s15*&
          &s35 + 9*s25*s35 - ss*(-10*tt + s15 + 2*s25 + 9*s35))))/(s35*(4*me2 - ss - tt + s&
          &35)))*pvb5(2))/(tt**2*s15**2) + (32*Pi**2*(10*me2*s15 - 3*ss*s15 - tt*s15 + tt*s&
          &25 - 2*me2*s35 + ss*s35 + 2*s15*s35 - s25*s35)*(2*me2 + s15 - 24*pvb5(3)))/(3.*t&
          &t*s15*s35*(4*me2 - ss - tt + s35)) - (64*Pi**2*(me2 - s15)*(-(tt*s15) + tt*s25 +&
          & 2*me2*(5*s15 - s35) + 2*s15*s35 - s25*s35 + ss*(-3*s15 + s35))*pvb5(4))/(tt*s15&
          &*s35*(4*me2 - ss - tt + s35)) + (128*Pi**2*(32*me2**4 - 8*me2**3*(4*ss + 3*tt - &
          &3*s15 - 4*s25) + 4*me2**2*(2*ss**2 + tt**2 + 2*s15**2 - 2*tt*s25 + 4*s15*s25 + 2&
          &*s25**2 - 2*ss*(-tt + s15 + 2*s25) - s15*s35) + (tt*(48*me2**4 - 4*me2**3*(8*ss &
          &+ 3*tt - 3*s15 - 8*s25) + 2*me2*(-(ss*s15*s25) + s15**2*s25 + s15*s25**2) + 2*me&
          &2**2*(2*ss**2 + 2*tt*s15 + s15**2 - 4*ss*s25 + 4*s15*s25 + 2*s25**2 - s15*s35)))&
          &/(4*me2 - ss - tt + s25) + (2*s15*(-16*me2**4 + 4*me2**3*(4*ss + 5*tt - 2*s25 - &
          &3*s35) - 2*me2**2*(2*ss**2 + 4*tt**2 - 3*tt*s15 - s15**2 - 5*tt*s25 + ss*(6*tt +&
          & 3*s15 - 2*s25 - 5*s35) - tt*s35 + 2*s15*s35 + 5*s25*s35) + me2*(tt**3 + tt*s15*&
          &*2 - 2*tt**2*s25 + 2*tt*s15*s25 + 2*tt*s25**2 + 2*ss**2*(tt + s15 - s35) - tt**2&
          &*s35 + tt*s15*s35 + 2*tt*s25*s35 - 2*s15*s25*s35 - 2*s25**2*s35 + 2*ss*(-tt + 2*&
          &s25)*(-tt + s35))))/s35 - 2*me2*(s15**2*(tt - 2*s25) + ss*s15*(s15 + 2*s25 - s35&
          &) + s15*(-2*s25**2 - tt*s35 + s25*(tt + s35))) - (tt*s15*(48*me2**4 - 2*me2*(ss*&
          &*3 - s15**2*s25 - s15*s25**2 - 2*ss**2*(s15 + s25) + ss*(s15**2 + 3*s15*s25 + s2&
          &5**2)) + 4*me2**3*(-14*ss - 3*tt + 11*s15 + 11*s25 + s35) + me2**2*(20*ss**2 + 1&
          &2*s15**2 - 3*tt*s25 + 10*s25**2 + s15*(-3*tt + 20*s25 - 4*s35) + s25*s35 + 2*s35&
          &**2 - ss*(-6*tt + 27*s15 + 30*s25 + s35))))/(s25*(4*me2 - ss - tt + s35)) + (tt*&
          &s15*(32*me2**4 - 8*me2**3*(6*ss + 5*tt - s15 - 3*s25 - 3*s35) + 2*me2**2*(12*ss*&
          &*2 + 8*tt**2 - 2*s15**2 - 13*tt*s25 + 2*s25**2 - 6*tt*s35 + 9*s25*s35 + s35**2 +&
          & s15*(-5*tt + s25 + 2*s35) - ss*(-18*tt + 3*s15 + 14*s25 + 9*s35)) + me2*(-4*ss*&
          &*3 - 2*tt**3 + s15**2*(tt - 2*s25) + 5*tt**2*s25 - 3*tt*s25**2 + 2*tt**2*s35 - 6&
          &*tt*s25*s35 + 3*s25**2*s35 + s25*s35**2 + ss**2*(-8*tt + s15 + 8*s25 + 3*s35) + &
          &s15*(tt**2 - 2*s25**2 - 3*tt*s35 + 3*s25*s35) + ss*(-6*tt**2 + 2*tt*s15 + s15**2&
          & + 11*tt*s25 - s15*s25 - 4*s25**2 + 5*tt*s35 - 6*s25*s35 - s35**2))))/((tt + s15&
          & - s35)*(s15 + s25 - s35)) + (tt*s15*(48*me2**4 - 4*me2**3*(14*ss + 9*tt - 5*s15&
          & - 9*s25 - 5*s35) + me2**2*(20*ss**2 + 6*tt**2 + 6*s15**2 - 21*tt*s25 + 6*s25**2&
          & + s15*(-13*tt + 10*s25 - 6*s35) - 2*tt*s35 + 11*s25*s35 + 4*s35**2 - ss*(-22*tt&
          & + 9*s15 + 26*s25 + 11*s35)) + me2*(-2*ss**3 - tt*s15**2 + tt**2*s25 - tt*s25**2&
          & - 2*tt*s25*s35 + s25**2*s35 + s25*s35**2 + ss**2*(-2*tt + s15 + 4*s25 + s35) + &
          &s15*(tt**2 - tt*s35 + s25*(-2*tt + s35)) - ss*(s15**2 - 3*tt*s25 + 3*s15*s25 + 2&
          &*s25**2 - tt*s35 + 2*s25*s35 + s35**2 - 2*s15*(tt + s35)))))/((4*me2 - ss - tt +&
          & s25)*(s15 + s25 - s35)) + (tt*s15*(-48*me2**4 + 4*me2**3*(8*ss + 9*tt + s15 - 6&
          &*s25 - 8*s35) - 2*me2**2*(2*ss**2 + 3*tt**2 - 8*tt*s25 + 2*ss*(4*tt + 3*s15 - s2&
          &5 - 5*s35) - 2*tt*s35 + 10*s25*s35 + s35**2 + s15*(-6*tt + 2*s25 + s35)) + me2*(&
          &2*tt*s15**2 + 2*ss**2*(tt + s15 - s35) + s15*(-tt**2 + 4*tt*s25 + tt*s35 - 2*s25&
          &*s35) - (-tt + s35)*(-(tt*s25) + 2*s25**2 + s25*s35) + ss*(-4*tt*s25 - tt*s35 + &
          &4*s25*s35 + s35**2 - s15*(3*tt + s35)))))/(s35*(4*me2 - ss - tt + s35)) - (tt*s1&
          &5*(32*me2**4 - 8*me2**3*(6*ss + 3*tt - 5*s15 - 5*s25 - s35) + 2*me2**2*(12*ss**2&
          & + 2*tt**2 + 5*s15**2 - 9*tt*s25 + 6*s25**2 - 2*tt*s35 + 3*s25*s35 + s35**2 + s1&
          &5*(-7*tt + 11*s25 + s35) - ss*(-10*tt + 17*s15 + 18*s25 + 3*s35)) - me2*(4*ss**3&
          & - 2*tt**2*s25 - 2*s15**2*s25 + tt*s25**2 + 3*tt*s25*s35 - s25**2*s35 - s25*s35*&
          &*2 - ss**2*(-4*tt + 7*s15 + 8*s25 + s35) + s15*(tt*s25 - 2*s25**2 + tt*(-2*tt + &
          &s35)) + ss*(2*tt**2 + 2*s15**2 - 5*tt*s25 + 4*s25**2 - 2*tt*s35 + 2*s25*s35 + s3&
          &5**2 + s15*(-5*tt + 7*s25 + s35)))))/(s25*(tt + s15 - s35)))*pvc10(1))/(tt**2*s1&
          &5**2) + (128*Pi**2*(128*me2**4 - 8*me2**3*(16*ss + 9*tt - 5*s15 - 16*s25) + 4*me&
          &2**2*(8*ss**2 + 3*tt**2 - s15**2 - 16*tt*s25 + 8*s25**2 - 16*ss*(-tt + s25) + s1&
          &5*(-10*tt + 4*s25 - 3*s35)) - s15*(-(tt**2*s15) + tt*s15**2 - tt**2*s25 + 3*tt*s&
          &15*s25 + 2*tt*s25**2 - 2*ss**2*(s15 - s35) + 2*tt**2*s35 - 3*tt*s25*s35 + s15*s2&
          &5*s35 + 2*s25**2*s35 + ss*(s15**2 + s15*(-3*tt + 2*s25 - s35) + 3*tt*s35 - 2*s25&
          &*(tt + 2*s35))) - 2*me2*(-s15**3 + 4*ss**2*(tt + s15) + 2*tt*(tt**2 - 2*tt*s25 +&
          & 2*s25**2) - tt*s15*(tt + 2*s35) + s15**2*(2*tt - 2*s25 + 3*s35) + s15*s25*(4*tt&
          & + 6*s35) + 2*ss*(s15**2 + 2*tt*(tt - 2*s25) - s15*(2*s25 + 3*s35))) + (s15*(-12&
          &8*me2**4 - 2*tt**4 - 2*tt**3*s15 - 2*tt**2*s15**2 + 2*tt**3*s25 - 2*tt**2*s15*s2&
          &5 - 2*tt**2*s25**2 + 8*me2**3*(16*ss + 17*tt + s15 - 8*s25 - 4*s35) + 2*tt**3*s3&
          &5 + tt**2*s15*s35 - tt*s15**2*s35 - tt**2*s25*s35 + tt*s15*s25*s35 + 2*tt*s25**2&
          &*s35 - tt*s25*s35**2 + s15*s25*s35**2 - 2*ss**2*(2*tt**2 + 2*tt*s15 + s15**2 - s&
          &15*s35) - 4*me2**2*(8*ss**2 + 12*tt**2 - s15**2 - 18*tt*s25 + 4*ss*(8*tt + 2*s15&
          & - 2*s25 - s35) - 3*tt*s35 + 8*s25*s35 - 2*s35**2 - s15*(7*tt + 4*s25 + 3*s35)) &
          &+ 2*me2*(5*tt**3 + 6*ss**2*(2*tt + s15) - 11*tt**2*s25 + 4*tt*s25**2 - 3*tt**2*s&
          &35 + 7*tt*s25*s35 - 4*s25**2*s35 - tt*s35**2 + 2*s25*s35**2 - s15**2*(2*tt + s35&
          &) + ss*(20*tt**2 + 2*s15**2 - 14*tt*s25 + s15*(7*tt - 4*s25 - 6*s35) - 5*tt*s35 &
          &+ 4*s25*s35 - 2*s35**2) + s15*(2*tt*(-3*tt + s25) + s35**2)) + ss*(s15**2*(-2*tt&
          & + s35) + tt*(-4*tt**2 + 4*tt*s25 + 2*tt*s35 - 2*s25*s35 + s35**2) - s15*(2*tt**&
          &2 - 5*tt*s35 + 2*s25*s35 + s35**2))))/s35 - (tt*s15*(192*me2**4 + tt**2*s15**2 +&
          & 2*tt**2*s15*s25 + tt**2*s25**2 - 4*me2**3*(32*ss + 47*tt + 4*s15 - 24*s25 - 13*&
          &s35) + tt*s15**2*s35 - tt*s25**2*s35 - s15*s25*s35**2 + ss**2*(2*tt**2 + 2*tt*s1&
          &5 + s15**2 - s15*s35) + 2*me2**2*(8*ss**2 + 25*tt**2 + s15**2 - 37*tt*s25 + ss*(&
          &54*tt + 19*s15 - 8*s25 - 13*s35) - 14*tt*s35 + 17*s25*s35 - 2*s15*(3*tt + 5*s25 &
          &+ 8*s35)) + ss*(-(s15**2*s35) + tt*s25*(-2*tt + s35) + s15*(-2*tt**2 - 3*tt*s35 &
          &+ s25*s35 + s35**2)) - me2*(6*ss**2*(2*tt + s15) + 2*tt**2*(tt - 2*s35) - 4*s25*&
          &*2*(-tt + s35) + s15**2*(-tt + 2*s35) + 2*tt*s25*(-6*tt + 7*s35) + ss*(22*tt**2 &
          &+ 12*tt*s15 + 7*s15**2 - 14*tt*s25 - 4*s15*s25 - 10*tt*s35 - 21*s15*s35 + 4*s25*&
          &s35) + s15*(-10*tt**2 + 3*tt*s25 - 8*tt*s35 + 13*s25*s35 + 2*s35**2))))/(s35*(4*&
          &me2 - ss - tt + s35)) - (tt*s15*(192*me2**4 + 2*tt*(ss - s15)*(-ss + s15 + s25)*&
          &*2 - 4*me2**3*(56*ss + 23*tt - 27*s15 - 44*s25 - 8*s35) + me2**2*(80*ss**2 + 4*t&
          &t**2 + 26*s15**2 - 71*tt*s25 + 40*s25**2 + s15*(-69*tt + 68*s25 - 30*s35) - 12*t&
          &t*s35 + 7*s25*s35 + 8*s35**2 - ss*(-90*tt + 91*s15 + 120*s25 + 7*s35)) + me2*(-8&
          &*ss**3 - 6*s15**3 + 3*tt**2*s25 - 13*tt*s25**2 + tt*s25*s35 + 3*s25**2*s35 - 2*t&
          &t*s35**2 + ss**2*(-26*tt + 13*s15 + 16*s25 + 3*s35) + s15**2*(-27*tt + 8*s35) + &
          &s15*(tt**2 - 36*tt*s25 + 4*s25**2 + 6*tt*s35 + 7*s25*s35 - 6*s35**2) + ss*(s15**&
          &2 - 8*s25**2 + s15*(52*tt - 17*s25 - 7*s35) + s25*(39*tt - 6*s35) - tt*(2*tt + s&
          &35)))))/(s25*(4*me2 - ss - tt + s35)) + (tt*s15*(-128*me2**4 - 4*ss**3*tt + 4*me&
          &2**3*(48*ss + 18*tt - 31*s15 - 40*s25 - 3*s35) + ss**2*(s15**2 - s15*(-8*tt + s3&
          &5) + 2*tt*(-2*tt + 4*s25 + s35)) - 2*me2**2*(48*ss**2 + 6*tt**2 + 9*s15**2 - 42*&
          &tt*s25 + 24*s25**2 - 6*tt*s35 + 7*s25*s35 + 4*s35**2 + s15*(-39*tt + 38*s25 + s3&
          &5) - ss*(-50*tt + 55*s15 + 72*s25 + 11*s35)) + tt*(2*s15**3 + s15**2*(tt + 4*s25&
          &) + s25*(-tt + s35)*(-2*tt + s25 + s35) + s15*(2*s25**2 + tt*(2*tt - s35) - s25*&
          &s35)) - ss*(4*tt*s15**2 + s15*(-(s25*(-8*tt + s35)) + 2*tt*(-2*tt + s35)) + tt*(&
          &2*tt**2 + 4*s25**2 - 2*tt*s35 + s35**2 + 3*s25*(-2*tt + s35))) + me2*(16*ss**3 +&
          & 4*tt**3 + 2*s15**3 - 13*tt**2*s25 + 16*tt*s25**2 + s15*(-3*tt**2 + 30*tt*s25 - &
          &4*s25**2) - 4*tt**2*s35 + 11*tt*s25*s35 - 4*s25**2*s35 + 2*tt*s35**2 - 4*s25*s35&
          &**2 - 8*ss**2*(-5*tt + 3*s15 + 4*s25 + s35) + s15**2*(22*tt - 4*s25 + 2*s35) + s&
          &s*(14*tt**2 + 4*s15**2 - 58*tt*s25 + 16*s25**2 - 11*tt*s35 + 12*s25*s35 + 4*s35*&
          &*2 + s15*(-57*tt + 26*s25 + 4*s35)))))/(s25*(tt + s15 - s35)) + (tt*s15*(128*me2&
          &**4 + 4*ss**3*tt + 2*tt**4 + 2*tt**3*s15 + tt**2*s15**2 - 4*tt**3*s25 - 2*tt**2*&
          &s15*s25 + 3*tt**2*s25**2 + 2*tt*s15*s25**2 - 4*me2**3*(48*ss + 34*tt - 7*s15 - 2&
          &4*s25 - 11*s35) - 2*tt**3*s35 + tt*s15**2*s35 + 4*tt**2*s25*s35 - 3*tt*s25**2*s3&
          &5 - s15*s25*s35**2 + ss**2*(s15**2 + 2*tt*(4*tt - 4*s25 - s35) - s15*s35) + 2*me&
          &2**2*(48*ss**2 + 24*tt**2 + s15**2 - 46*tt*s25 + 8*s25**2 + s15*(-16*tt + 2*s25 &
          &- 5*s35) - 15*tt*s35 + 23*s25*s35 - ss*(-82*tt + 15*s15 + 56*s25 + 19*s35)) + ss&
          &*(-(s15**2*(-2*tt + s35)) + s15*(2*tt**2 - 3*tt*s35 + s25*s35 + s35**2) + tt*(6*&
          &tt**2 + 4*s25**2 - 4*tt*s35 + 5*s25*(-2*tt + s35))) + me2*(-16*ss**3 - 10*tt**3 &
          &+ 2*s15**3 + s15**2*(6*tt - 4*s25) + 27*tt**2*s25 - 16*tt*s25**2 + 10*tt**2*s35 &
          &- 25*tt*s25*s35 + 12*s25**2*s35 + 8*ss**2*(-7*tt + s15 + 4*s25 + s35) - s15*(-9*&
          &tt**2 + 4*tt*s25 + 4*s25**2 + 2*tt*s35 - 2*s25*s35 + 2*s35**2) + ss*(-46*tt**2 -&
          & 2*s15**2 + 70*tt*s25 - 16*s25**2 + 21*tt*s35 - 20*s25*s35 + s15*(15*tt - 6*s25 &
          &+ 6*s35)))))/((tt + s15 - s35)*(s15 + s25 - s35)) + (tt*s15*(192*me2**4 + 2*ss**&
          &3*tt + tt**2*s15**2 + 2*tt**2*s15*s25 + tt**2*s25**2 - 4*me2**3*(56*ss + 47*tt -&
          & 7*s15 - 36*s25 - 20*s35) + tt*s15**2*s35 - tt*s25**2*s35 - s15*s25*s35**2 + ss*&
          &*2*(s15**2 + 2*tt*(tt - 2*s25) - s15*(2*tt + s35)) + me2**2*(80*ss**2 + 50*tt**2&
          & + 40*s15**2 - 105*tt*s25 + 24*s25**2 + s15*(-15*tt + 20*s25 - 62*s35) - 40*tt*s&
          &35 + 39*s25*s35 + 8*s35**2 - ss*(-154*tt + 35*s15 + 104*s25 + 31*s35)) + ss*(-(s&
          &15**2*(-2*tt + s35)) + tt*s25*(-2*tt + 2*s25 + s35) + s15*(-2*tt**2 - 3*tt*s35 +&
          & s35**2 + s25*(4*tt + s35))) + me2*(-8*ss**3 - 2*tt**3 + 15*tt**2*s25 - 13*tt*s2&
          &5**2 + 4*tt**2*s35 - 13*tt*s25*s35 + 7*s25**2*s35 - 2*tt*s35**2 + ss**2*(-34*tt &
          &+ 5*s15 + 16*s25 + 3*s35) + s15**2*(-8*tt + 4*s25 + 6*s35) + s15*(7*tt**2 - 11*t&
          &t*s25 + 14*tt*s35 - 6*s25*s35 - 8*s35**2) + ss*(-24*tt**2 - 12*s15**2 + 45*tt*s2&
          &5 - 8*s25**2 + 9*tt*s35 - 10*s25*s35 + s15*(12*tt - 7*s25 + 14*s35)))))/((4*me2 &
          &- ss - tt + s25)*(s15 + s25 - s35)) + (tt*(192*me2**4 - 4*me2**3*(32*ss + 23*tt &
          &+ s15 - 32*s25) - s15*(-ss + s15 + s25)*(tt*s15 + ss*(s15 - s35) + s25*(tt + s35&
          &)) + 2*me2**2*(8*ss**2 - 6*s15**2 + 2*ss*(11*tt + 7*s15 - 8*s25) + 2*(tt**2 - 11&
          &*tt*s25 + 4*s25**2) - s15*(12*tt + 8*s25 + 13*s35)) + me2*(2*s15**3 - 4*ss**2*(t&
          &t + s15) - 4*tt*s25**2 + s15**2*(-5*tt + 2*s25 - 8*s35) + s15*(2*tt**2 - 11*tt*s&
          &25 + 4*tt*s35 - 15*s25*s35) + ss*(-3*s15**2 + 8*tt*s25 + s15*(6*tt + 4*s25 + 15*&
          &s35)))))/(4*me2 - ss - tt + s25))*pvc10(2))/(tt**2*s15**2) + (64*Pi**2*(32*me2**&
          &3*(-tt + s15) + 8*me2**2*(3*s15**2 - 4*ss*(-tt + s15) - 2*tt*(tt + 2*s25) + s15*&
          &(-5*tt + 4*s25)) + 4*me2*(2*s15**3 + 2*ss**2*(-tt + s15) - 2*ss*(-tt + s15)*(-tt&
          & + s15 + 2*s25) + 2*s15*(2*tt**2 - 3*tt*s25 + s25**2) - 2*tt*(tt**2 - tt*s25 + s&
          &25**2) + s15**2*(-tt + 4*s25 - s35)) + 2*s15*(s15**2*(-tt + 2*s25) - ss*(-tt + s&
          &15)*(s15 + 2*s25 - s35) + tt*(-2*s25**2 - 2*tt*s35 + s25*(tt + s35)) + s15*(tt**&
          &2 + 2*s25**2 - s25*(3*tt + s35))) + (tt*s15*(me2**3*(32*tt - 48*s15) + (-tt**2 -&
          & tt*s15 + 2*s15**2 + 2*s15*s25)*(tt*s15 + s25*(tt - s35)) + 4*me2**2*(-6*ss*tt +&
          & tt**2 + 8*ss*s15 + 20*tt*s15 + s15**2 + 6*tt*s25 - 6*s15*s25 - 2*tt*s35 - 7*s15&
          &*s35 - s35**2) + 2*ss**2*(s15**2 - s15*(-2*tt + s35) - tt*(tt + s35)) + ss*(-3*t&
          &t*s15**2 + tt**2*(2*tt - s35) + 2*tt*s25*s35 + tt*s15*(5*tt + s35) + s15*s25*(-6&
          &*tt + 4*s35)) - 2*me2*(5*tt**3 + 2*ss**2*(-tt + s15) - tt**2*s25 - tt**2*s35 + s&
          &25*s35**2 + 2*s15**2*(-4*tt + s25 + s35) + 2*s15*(5*tt**2 - 6*tt*s25 + 5*s25*s35&
          &) + ss*(6*s15**2 + 2*tt*(-2*tt + s25) + s15*(19*tt - 2*s25 - 9*s35) - 3*tt*s35 -&
          & s35**2))))/(s35*(4*me2 - ss - tt + s35)) - (2*s15*(2*tt**4 + tt**3*s15 + tt**2*&
          &s15**2 - tt*s15**3 + 16*me2**3*(-tt + s15) + 3*tt**2*s15*s25 - 2*tt*s15**2*s25 -&
          & 2*tt*s15*s25**2 - 2*tt**3*s35 - tt*s15**2*s35 - tt**2*s25*s35 - 3*tt*s15*s25*s3&
          &5 + 2*s15**2*s25*s35 + 2*s15*s25**2*s35 + tt*s25*s35**2 + 4*me2**2*(4*ss*tt - 4*&
          &ss*s15 - 8*tt*s15 - 2*tt*s25 + 2*s15*s25 + tt*s35 + 3*s15*s35) + 2*ss**2*(-s15**&
          &2 + s15*(-2*tt + s35) + tt*(tt + s35)) + ss*(tt*s15**2 - tt*(-2*tt**2 + 2*s25*s3&
          &5 + s35**2) + 2*s15*(s25*(3*tt - 2*s35) + tt*(-2*tt + s35))) + 2*me2*(-s15**3 + &
          &2*ss**2*(-tt + s15) + tt*s35**2 + tt*s25*(tt + s35) + tt*s15*(6*tt + s35) + s15*&
          &*2*(-5*tt + 2*s35) + s15*s25*(-7*tt + 5*s35) + ss*(3*s15**2 + 2*tt*s25 + s15*(11&
          &*tt - 2*s25 - 5*s35) - 3*tt*(2*tt + s35)))))/s35 + (tt*(16*me2**3*(-2*tt + 3*s15&
          &) - 4*me2**2*(-6*ss*tt + 5*tt**2 + 8*ss*s15 + 6*tt*s15 - 3*s15**2 + 6*tt*s25 - 8&
          &*s15*s25) + 2*me2*(2*s15**3 + 2*ss**2*(-tt + s15) - 2*tt*s25*(tt + s25) + 2*ss*(&
          &s15*(tt - 2*s25) + tt*(tt + 2*s25)) + s15**2*(3*tt + 4*s25 - 2*s35) + s15*(tt**2&
          & - 4*tt*s25 + 2*s25**2 - 2*tt*s35)) - s15*(s15**2*(tt - 2*s25) + tt*s25*(tt + 2*&
          &s25 + s35) + s15*(tt**2 + 3*tt*s25 - 2*s25**2 + s25*s35) + ss*(s15**2 + s15*(tt &
          &+ 2*s25 - s35) - tt*(2*s25 + s35)))))/(4*me2 - ss - tt + s25) + (tt*s15*(4*tt**3&
          &*s15 + 2*tt**2*s15**2 + 2*tt*s15**3 - 32*me2**3*(-tt + s15) + 4*ss**3*(-tt + s15&
          &) + 4*tt**3*s25 + tt**2*s15*s25 + 5*tt*s15**2*s25 - 2*s15**3*s25 - tt**2*s25**2 &
          &+ 3*tt*s15*s25**2 - 2*s15**2*s25**2 - 2*tt**2*s15*s35 - 6*tt**2*s25*s35 + tt*s25&
          &**2*s35 - s15*s25**2*s35 + 2*tt*s25*s35**2 - ss**2*(7*s15**2 + tt*(4*tt - 8*s25 &
          &- 3*s35) + s15*(-9*tt + 8*s25 + s35)) - 4*me2**2*(-4*tt**2 + 10*s15**2 - 12*ss*(&
          &-tt + s15) - 10*tt*s25 + s35**2 + s15*(-16*tt + 10*s25 + s35)) + ss*(2*s15**3 + &
          &s15**2*(-9*tt + 7*s25 + 2*s35) - tt*(4*tt**2 - 7*tt*s25 + 4*s25**2 - 4*tt*s35 + &
          &4*s25*s35 + 2*s35**2) + s15*(tt**2 + 4*s25**2 + 2*s25*(-6*tt + s35))) - 2*me2*(-&
          &4*tt**3 + 5*s15**3 + 12*ss**2*(-tt + s15) + tt**2*s25 - 6*tt*s25**2 + 4*tt**2*s3&
          &5 - 2*tt*s25*s35 - 2*tt*s35**2 + s25*s35**2 + s15**2*(11*s25 + 2*(-6*tt + s35)) &
          &+ s15*(6*s25**2 + 3*s25*(-6*tt + s35) - tt*(3*tt + s35)) - ss*(17*s15**2 - 18*tt&
          &*s25 - 3*tt*s35 + s35**2 + s15*(-25*tt + 18*s25 + 2*s35)))))/(s25*(tt + s15 - s3&
          &5)) + (tt*s15*(me2**3*(32*tt - 48*s15) + 2*(ss - s15)*(ss**2*(-tt + s15) + s15**&
          &2*(-tt + s25) - tt*s25*(tt + s25) + ss*(tt**2 + 2*tt*s15 - s15**2 + 2*tt*s25 - 2&
          &*s15*s25) + s15*(-tt**2 - 2*tt*s25 + s25**2)) + 4*me2**2*(5*tt**2 - 12*s15**2 + &
          &2*ss*(-5*tt + 7*s15) + 8*tt*s25 + 3*tt*s35 - s35**2 + s15*(9*tt - 11*s25 + s35))&
          & + me2*(-12*s15**3 - 4*ss**2*(-4*tt + 5*s15) + s15**2*(19*tt - 20*s25 + 4*s35) -&
          & s15*(-15*tt**2 - 25*tt*s25 + 10*s25**2 + 4*tt*s35 + s25*s35) + tt*(5*tt*s25 + 1&
          &0*s25**2 - 5*s25*s35 + 4*s35**2) + ss*(27*s15**2 + s15*(-39*tt + 30*s25 + s35) +&
          & tt*(-14*tt - 26*s25 + 5*s35)))))/(s25*(4*me2 - ss - tt + s35)) + (tt*s15*(4*tt*&
          &*4 + 3*tt**3*s15 + 2*tt**2*s15**2 + tt*s15**3 + 32*me2**3*(-tt + s15) - 4*ss**3*&
          &(-tt + s15) - 5*tt**3*s25 + tt**2*s15*s25 - 2*s15**3*s25 + 3*tt**2*s25**2 - tt*s&
          &15*s25**2 - 2*s15**2*s25**2 - 4*tt**3*s35 + 2*tt**2*s15*s35 - 2*tt*s15**2*s35 + &
          &5*tt**2*s25*s35 - 6*tt*s15*s25*s35 + 3*s15**2*s25*s35 - 3*tt*s25**2*s35 + 3*s15*&
          &s25**2*s35 + ss**2*(s15**2 + tt*(8*tt - 8*s25 - s35) + s15*(-11*tt + 8*s25 + 3*s&
          &35)) + 4*me2**2*(2*s15**2 - 12*ss*(-tt + s15) - 6*tt*s25 + s35**2 + s15*(-16*tt &
          &+ 6*s25 + 5*s35)) + ss*(s15**3 - s15**2*(-3*tt + s25 + s35) + tt*(8*tt**2 - 9*tt&
          &*s25 + 4*s25**2 - 5*tt*s35 + 4*s25*s35) - 2*s15*(2*s25**2 + 2*tt*(tt - s35) + 3*&
          &s25*(-2*tt + s35))) + 2*me2*(-2*s15**3 + 12*ss**2*(-tt + s15) + tt**2*s25 - 2*tt&
          &*s25**2 + 2*tt**2*s35 - 4*tt*s25*s35 + s25*s35**2 + s15**2*(-5*tt + s25 + 3*s35)&
          & + s15*(7*tt**2 - 12*tt*s25 + 2*s25**2 - 6*tt*s35 + 9*s25*s35) - ss*(8*tt**2 + 3&
          &*s15**2 - 14*tt*s25 - tt*s35 + s35**2 + s15*(-27*tt + 14*s25 + 8*s35)))))/((tt +&
          & s15 - s35)*(s15 + s25 - s35)) + (tt*s15*(-2*ss**3*(-tt + s15) + 16*me2**3*(-2*t&
          &t + 3*s15) - 4*me2**2*(tt**2 - 6*s15**2 + 2*ss*(-5*tt + 7*s15) + 8*tt*s25 + s15*&
          &(14*tt - 9*s25 - 2*s35) + 4*tt*s35 - 2*s35**2) + (s15**2 + s15*(-2*tt + s25) - t&
          &t*(tt + s25))*(-(tt*s15) + s25*(-tt + s35)) + ss**2*(s15**2 + tt*(-4*s25 + s35) &
          &+ s15*(-7*tt + 4*s25 + s35)) + ss*(-s15**3 + s15**2*(3*tt - 3*s25 + s35) - 2*s15&
          &*(2*tt**2 + s25**2 + s25*(-4*tt + s35)) + tt*(tt*s25 + 2*s25**2 + tt*(-2*tt + s3&
          &5))) + me2*(10*tt**3 + 6*s15**3 + 4*ss**2*(-4*tt + 5*s15) - 7*tt**2*s25 - 6*tt*s&
          &25**2 + s15**2*(-17*tt + 10*s25 - 4*s35) - 3*tt*s25*s35 - 4*tt*s35**2 + 2*s25*s3&
          &5**2 + s15*(6*s25**2 + 11*s25*(-3*tt + s35) + tt*(11*tt + 2*s35)) - ss*(9*s15**2&
          & - 2*tt*(tt + 11*s25) + 3*tt*s35 + 2*s35**2 + s15*(-49*tt + 26*s25 + 9*s35)))))/&
          &((4*me2 - ss - tt + s25)*(s15 + s25 - s35)))*pvc10(3))/(tt**2*s15**2) + (128*Pi*&
          &*2*(32*me2**3 + 16*me2**2*(-2*ss + s15 + 2*s25) - 2*s15*(s15*(tt - 2*s25) + tt*s&
          &25 - 2*s25**2 + ss*(s15 + 2*s25 - s35) + tt*s35 + s25*s35) + 4*me2*(2*ss**2 - tt&
          &**2 + s15**2 - 2*tt*s25 + 2*s25**2 - 2*ss*(-tt + s15 + 2*s25) + s15*(-tt + 4*s25&
          & + s35)) - (2*s15*(16*me2**3 + tt**3 + tt**2*s15 - tt*s15**2 - tt*s15*s25 - tt**&
          &2*s35 + tt*s15*s35 + tt*s25*s35 + s15*s25*s35 - s25*s35**2 - 2*ss**2*(tt + s35) &
          &- 4*me2**2*(4*ss + 2*tt - s15 - 2*s25 + s35) + 2*me2*(2*ss**2 - tt**2 + s15**2 -&
          & tt*s25 - ss*(-6*tt + s15 + 2*s25 - 3*s35) + 2*s15*(-tt + s25 - s35) + tt*s35 - &
          &s25*s35 - s35**2) + ss*(-2*tt**2 - s15**2 - 2*s15*s25 + 2*s15*s35 + 2*s25*s35 + &
          &s35**2)))/s35 + (2*tt*s15*(-24*me2**3 + (ss - s15)*(ss + 2*tt - s25)*(ss - s15 -&
          & s25) + me2**2*(28*ss + 8*tt - 20*s15 - 22*s25 - 6*s35) + me2*(-10*ss**2 - 5*s15&
          &**2 + 8*tt*s25 - 5*s25**2 + ss*(-8*tt + 14*s15 + 15*s25 - s35) + s25*s35 + s35**&
          &2 + s15*(7*tt - 9*s25 + s35))))/(s25*(4*me2 - ss - tt + s35)) + (2*tt*(24*me2**3&
          & - 8*me2**2*(2*ss + tt - s15 - 2*s25) + 2*me2*(ss**2 + 2*s15**2 + s25*(-2*tt + s&
          &25) - ss*(-2*tt + s15 + 2*s25) + s15*(-tt + 4*s25 - s35)) - s15*(s15*(tt - s25) &
          &+ ss*(s15 + s25 - s35) + s25*(tt - s25 + s35))))/(4*me2 - ss - tt + s25) - (tt*s&
          &15*(-48*me2**3 + 2*ss**3 + tt**2*s15 + 2*tt*s15**2 + tt**2*s25 + 3*tt*s15*s25 + &
          &tt*s25**2 + 4*me2**2*(14*ss + 10*tt - 6*s15 - 9*s25 - 5*s35) - 3*tt*s15*s35 - 4*&
          &tt*s25*s35 - 2*s15*s25*s35 - s25**2*s35 + 3*s25*s35**2 + ss**2*(6*tt - 3*s15 - 4&
          &*s25 + s35) - 2*me2*(10*ss**2 + 4*tt**2 - tt*s15 + 4*s15**2 + ss*(16*tt - 9*s15 &
          &- 13*s25) - 4*tt*s25 + 8*s15*s25 + 3*s25**2 - 3*tt*s35 - s15*s35 + 2*s25*s35 - 4&
          &*s35**2) + ss*(4*tt**2 + 2*s15**2 - 5*tt*s25 + 2*s25**2 + tt*s35 - 3*s35**2 + s1&
          &5*(-2*tt + 5*s25 + s35))))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) - (tt*s15&
          &*(-32*me2**3 + 4*ss**3 - 2*tt**3 - 3*tt**2*s15 + tt**2*s25 + 5*tt*s15*s25 + 2*s1&
          &5**2*s25 + 3*tt*s25**2 + 2*s15*s25**2 + 4*me2**2*(12*ss + 4*tt - 5*s15 - 6*s25 -&
          & s35) + 2*tt**2*s35 - tt*s15*s35 - 4*tt*s25*s35 - 4*s15*s25*s35 - 3*s25**2*s35 +&
          & 3*s25*s35**2 - ss**2*(-8*tt + 3*s15 + 8*s25 + s35) + ss*(2*tt**2 - 9*tt*s25 + 4&
          &*s25**2 + tt*s35 + 4*s25*s35 - 3*s35**2 + 3*s15*(-2*tt + s25 + s35)) - 2*me2*(12&
          &*ss**2 - 2*tt**2 - s15**2 - 5*tt*s25 + 2*s25**2 + 3*tt*s35 + 4*s25*s35 - 3*s35**&
          &2 + 3*s15*(-3*tt + s25 + s35) - 2*ss*(-6*tt + 4*s15 + 7*s25 + s35))))/((tt + s15&
          & - s35)*(s15 + s25 - s35)) - (tt*s15*(32*me2**3 - 4*ss**3 - 2*tt**2*s15 - 3*tt*s&
          &15**2 - 2*tt**2*s25 - 4*tt*s15*s25 + 2*s15**2*s25 - tt*s25**2 + 2*s15*s25**2 + t&
          &t*s15*s35 + 3*tt*s25*s35 - s15*s25*s35 + s25**2*s35 - s25*s35**2 + 4*me2**2*(-12&
          &*ss + 9*s15 + 10*s25 + s35) + ss**2*(-4*tt + 5*s15 + 8*s25 + 3*s35) - ss*(-2*tt*&
          &*2 - 7*tt*s15 + s15**2 - 7*tt*s25 + 5*s15*s25 + 4*s25**2 + 2*tt*s35 + 4*s15*s35 &
          &+ 4*s25*s35 - s35**2) + 2*me2*(12*ss**2 - 2*tt**2 - 4*tt*s15 + 4*s15**2 - 5*tt*s&
          &25 + 9*s15*s25 + 6*s25**2 + 2*tt*s35 + 4*s15*s35 + 2*s25*s35 - s35**2 - 2*ss*(-2&
          &*tt + 7*s15 + 9*s25 + 2*s35))))/(s25*(tt + s15 - s35)) + (tt*s15*(-48*me2**3 + (&
          &tt*s15 + s25*(tt - s35))*(tt + s15 - 3*s35) + 2*ss**2*(tt + s35) + 4*me2**2*(8*s&
          &s + 10*tt - 3*s15 - 6*s25 + s35) + ss*(4*tt**2 + 3*tt*s15 + s15**2 + 2*s15*s25 +&
          & tt*s35 - 2*s25*s35 - 3*s35**2) - 2*me2*(2*ss**2 + 4*tt**2 + 3*s15**2 + tt*s25 +&
          & 2*s15*(tt + 3*s25 - s35) - 2*tt*s35 - s25*s35 - 3*s35**2 - ss*(s15 + 2*s25 - 3*&
          &(4*tt + s35)))))/(s35*(4*me2 - ss - tt + s35)))*pvc10(4))/(tt**2*s15**2) + (128*&
          &me2*Pi**2*(4*(2*me2 - s15)*(2*me2 - ss - tt + s25)*(2*me2 - ss + s15 + s25) + (t&
          &t*s15*(32*me2**3 - 4*ss**3 + 2*tt**2*s15 - tt*s15**2 + 2*tt**2*s25 - 4*tt*s15*s2&
          &5 - 3*tt*s25**2 + tt*s15*s35 - tt*s25*s35 + s15*s25*s35 + 3*s25**2*s35 - s25*s35&
          &**2 + 4*me2**2*(-12*ss - 8*tt + s15 + 6*s25 + s35) + ss**2*(-8*tt + 3*s15 + 8*s2&
          &5 + s35) - ss*(4*tt**2 - 5*tt*s15 + s15**2 - 11*tt*s25 + 3*s15*s25 + 4*s25**2 + &
          &4*s25*s35 - s35**2) + 2*me2*(12*ss**2 + 4*tt**2 + s15**2 - 9*tt*s25 + 2*s25**2 +&
          & s15*(-3*tt + s25 - s35) - tt*s35 + 4*s25*s35 - s35**2 - 2*ss*(-8*tt + 2*s15 + 7&
          &*s25 + s35))))/((tt + s15 - s35)*(s15 + s25 - s35)) - (2*tt*(-24*me2**3 + 4*me2*&
          &*2*(4*ss + 4*tt + s15 - 4*s25) - 2*me2*(ss**2 + tt**2 - 2*s15**2 + ss*(3*tt + 3*&
          &s15 - 2*s25) - 3*tt*s25 - 3*s15*s25 + s25**2 - 2*s15*s35) + s15*(ss**2 - tt*s25 &
          &+ s25**2 - tt*s35 + 2*s25*s35 + s15*(-tt + s25 + s35) - ss*(-tt + s15 + 2*s25 + &
          &2*s35))))/(4*me2 - ss - tt + s25) - (tt*s15*(32*me2**3 - 4*ss**3 - tt**2*s15 - 2&
          &*tt*s15**2 - tt**2*s25 - 3*tt*s15*s25 - tt*s25**2 + tt*s15*s35 + s25**2*s35 + s2&
          &5*s35**2 + 4*me2**2*(-12*ss - 4*tt + 5*s15 + 10*s25 + s35) + ss**2*(-4*tt + 5*s1&
          &5 + 8*s25 + 3*s35) - ss*(2*s15**2 - 5*tt*s25 + 4*s25**2 - tt*s35 + 4*s25*s35 + s&
          &35**2 + s15*(-4*tt + 5*s25 + s35)) + 2*me2*(12*ss**2 - 4*tt*s15 + 2*s15**2 - 5*t&
          &t*s25 + 7*s15*s25 + 6*s25**2 + 2*s25*s35 + s35**2 - 2*ss*(-4*tt + 5*s15 + 9*s25 &
          &+ 2*s35))))/(s25*(tt + s15 - s35)) + (2*s15*(-16*me2**3 + 4*me2**2*(4*ss + 4*tt &
          &+ s15 - 2*s25 + s35) - (tt + s15 + s35)*(-2*ss**2 + tt*s15 + s25*(tt - s35) + ss&
          &*(-2*tt + s15 + 2*s25 + s35)) + 2*me2*(-2*ss**2 - 2*tt**2 + s15**2 + 3*tt*s25 + &
          &s25*s35 + s35**2 + 2*s15*(s25 + s35) - ss*(6*tt + 3*s15 - 2*s25 + 3*s35))))/s35 &
          &+ (tt*s15*(-48*me2**3 + 2*ss**3 - tt**2*s15 + 4*tt*s15**2 + 2*s15**3 - tt**2*s25&
          & + 7*tt*s15*s25 + 2*s15**2*s25 + 3*tt*s25**2 + 4*me2**2*(14*ss + 8*tt - 4*s15 - &
          &11*s25 - 3*s35) - 2*tt*s15*s35 - 4*s15**2*s35 + tt*s25*s35 - 4*s15*s25*s35 - 3*s&
          &25**2*s35 + 2*s15*s35**2 - ss**2*(-6*tt + s15 + 4*s25 + 3*s35) + ss*(2*tt**2 - 2&
          &*s15**2 - 9*tt*s25 + 2*s25**2 - tt*s35 + 6*s25*s35 + s15*(-8*tt + s25 + 4*s35)) &
          &- 2*me2*(10*ss**2 + 2*tt**2 - 7*tt*s15 + s15**2 - 10*tt*s25 + 5*s15*s25 + 5*s25*&
          &*2 - 2*tt*s35 - s15*s35 + 5*s25*s35 + s35**2 - ss*(-14*tt + 6*s15 + 15*s25 + 5*s&
          &35))))/(s25*(4*me2 - ss - tt + s35)) - (tt*s15*(48*me2**3 - 2*tt**3 + tt**2*s15 &
          &+ 3*tt*s15**2 + 5*tt**2*s25 + 5*tt*s15*s25 + 2*tt**2*s35 + tt*s15*s35 - 2*s15**2&
          &*s35 - 4*tt*s25*s35 - 5*s15*s25*s35 - s25*s35**2 - 2*ss**2*(tt + s15 + s35) - 4*&
          &me2**2*(8*ss + 14*tt + 3*s15 - 6*s25 + s35) + 2*me2*(2*ss**2 + 10*tt**2 - 3*s15*&
          &*2 - 11*tt*s25 + s15*(2*tt - 6*s25 - 6*s35) - 4*tt*s35 - s25*s35 - s35**2 + ss*(&
          &14*tt + 7*s15 - 2*s25 + 3*s35)) + ss*(-6*tt**2 + s15**2 + 3*tt*s35 + s35**2 + 2*&
          &s25*(tt + s35) + s15*(-5*tt + 2*s25 + 6*s35))))/(s35*(4*me2 - ss - tt + s35)) + &
          &(tt*s15*(48*me2**3 - 2*ss**3 - 2*tt**3 - 2*tt*s15**2 + 6*tt**2*s25 + 2*s15**2*s2&
          &5 - 4*tt*s25**2 - 4*me2**2*(14*ss + 14*tt - 9*s25 - 5*s35) + 2*tt**2*s35 + 3*tt*&
          &s15*s35 + 2*s15**2*s35 - 5*tt*s25*s35 + 4*s25**2*s35 - 2*s15*s35**2 - s25*s35**2&
          & + 2*ss**2*(-4*tt + 2*s25 + s35) + ss*(-8*tt**2 - 2*s15**2 + 12*tt*s25 - 2*s25**&
          &2 + 4*tt*s35 + s15*s35 - 6*s25*s35 + s35**2) + 2*me2*(10*ss**2 + 10*tt**2 + tt*s&
          &15 + 4*s15**2 - 16*tt*s25 + 3*s25**2 - 7*tt*s35 - 5*s15*s35 + 8*s25*s35 - ss*(-2&
          &2*tt + s15 + 13*s25 + 6*s35))))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)))*pvc&
          &10(5))/(tt**2*s15**2) + (64*Pi**2*(32*me2**3*(-tt + s15) + 8*me2**2*(4*ss*tt + t&
          &t**2 - 4*ss*s15 - 5*tt*s15 + 4*s15**2 - 4*tt*s25 + 4*s15*s25 - 2*s15*s35) + 2*s1&
          &5*(2*s15**2*(-tt + s25) + 2*ss**2*(s15 - s35) - 2*ss*(-tt + s15 + 2*s25)*(s15 - &
          &s35) - (tt**2 - 2*tt*s25 + 2*s25**2)*s35 + s15*(2*s25**2 + tt*s35 - 2*s25*(tt + &
          &s35))) + 4*me2*(2*s15**3 + 2*ss**2*(-tt + s15) - tt*(tt**2 - 2*tt*s25 + 2*s25**2&
          &) - 2*ss*(3*s15**2 + tt*(tt - 2*s25) + 2*s15*(-tt + s25 - s35)) + s15**2*(-3*tt &
          &+ 6*s25 - 2*s35) + 2*s15*(s25**2 + tt*(tt + s35) - 2*s25*(tt + s35))) + (tt*s15*&
          &(me2**3*(32*tt - 48*s15) + tt**3*s15 + tt*s15**3 + tt**3*s25 - 2*tt**2*s15*s25 +&
          & tt*s15**2*s25 - 2*tt**2*s25**2 - 2*ss**2*(s15**2 + s15*(tt - 2*s35) + tt*(tt - &
          &s35)) - tt**2*s15*s35 - 3*tt*s15**2*s35 - 2*tt**2*s25*s35 - 3*tt*s15*s25*s35 - s&
          &15**2*s25*s35 + 2*tt*s25**2*s35 + tt*s25*s35**2 + 3*s15*s25*s35**2 + 4*me2**2*(-&
          &6*ss*tt - 4*tt**2 + 8*ss*s15 + 9*tt*s15 - 9*s15**2 + 6*tt*s25 - 6*s15*s25 + 7*tt&
          &*s35 + 8*s15*s35 - s35**2) + ss*(s15**3 + 2*s15**2*s25 - tt*(-tt + s35)*(4*s25 +&
          & s35) + s15*(3*tt**2 + 2*tt*s25 + 5*tt*s35 - 4*s25*s35 - 3*s35**2)) - 2*me2*(3*s&
          &15**3 + 2*ss**2*(-tt + s15) + 8*tt**2*s25 + s15**2*(-tt + 6*s25 - 2*s35) + 2*tt*&
          &*2*s35 - 11*tt*s25*s35 - tt*s35**2 + s25*s35**2 + s15*(9*tt**2 + tt*s25 + tt*s35&
          & - 7*s25*s35 - 3*s35**2) - ss*(6*tt**2 + 9*s15**2 - 2*tt*s25 + 2*s15*(-tt + s25 &
          &- 6*s35) - 10*tt*s35 + s35**2))))/(s35*(4*me2 - ss - tt + s35)) - (2*s15*(tt**4 &
          &- tt*s15**3 + 16*me2**3*(-tt + s15) - 2*tt**3*s25 + tt**2*s15*s25 - tt*s15**2*s2&
          &5 + 2*tt**2*s25**2 + 4*me2**2*(3*s15**2 - 4*ss*(-tt + s15) + tt*(3*tt - 2*s25 - &
          &3*s35) + s15*(-2*tt + 2*s25 - 3*s35)) + 2*ss**2*(s15**2 + s15*(tt - 2*s35) + tt*&
          &(tt - s35)) - tt**3*s35 + tt**2*s15*s35 + 2*tt*s15**2*s35 + 2*tt**2*s25*s35 + tt&
          &*s15*s25*s35 + s15**2*s25*s35 - 2*tt*s25**2*s35 - 2*s15*s25*s35**2 + 2*me2*(s15*&
          &*3 + 2*ss**2*(-tt + s15) - ss*(5*s15**2 + s15*(tt + 2*s25 - 7*s35) + tt*(6*tt - &
          &2*s25 - 5*s35)) + tt*(-2*tt + 5*s25)*(tt - s35) + s15**2*(2*s25 - s35) + s15*(3*&
          &tt**2 + tt*s25 + tt*s35 - 3*s25*s35 - 2*s35**2)) + ss*(-s15**3 + 2*tt*(-tt + 2*s&
          &25)*(-tt + s35) + s15**2*(tt - 2*s25 + s35) + s15*(-2*tt*s25 - 3*tt*s35 + 4*s25*&
          &s35 + 2*s35**2))))/s35 + (tt*s15*(2*tt**4 + tt**3*s15 + tt**2*s15**2 + 32*me2**3&
          &*(-tt + s15) - 4*ss**3*(-tt + s15) - 5*tt**3*s25 - 2*tt**2*s15*s25 - 3*tt*s15**2&
          &*s25 - 2*s15**3*s25 + 3*tt**2*s25**2 - tt*s15*s25**2 - 2*s15**2*s25**2 + ss**2*(&
          &5*s15**2 + tt*(8*tt - 8*s25 - 3*s35) + s15*(-5*tt + 8*s25 - s35)) - 2*tt**3*s35 &
          &+ tt**2*s15*s35 + tt*s15**2*s35 + 6*tt**2*s25*s35 + 2*tt*s15*s25*s35 + 4*s15**2*&
          &s25*s35 - 3*tt*s25**2*s35 + 3*s15*s25**2*s35 - tt*s25*s35**2 - 3*s15*s25*s35**2 &
          &+ 4*me2**2*(6*tt**2 + 5*s15**2 - 12*ss*(-tt + s15) - 6*tt*s25 + s15*(-3*tt + 6*s&
          &25 - 4*s35) - 5*tt*s35 + s35**2) + 2*me2*(-4*tt**3 + s15**3 + 12*ss**2*(-tt + s1&
          &5) + 9*tt**2*s25 - 2*tt*s25**2 + 5*tt**2*s35 - 9*tt*s25*s35 - tt*s35**2 + s25*s3&
          &5**2 + s15**2*(-2*tt + 3*s25 + s35) + s15*(tt**2 + 2*s25**2 + tt*s35 - 3*s35**2)&
          & - ss*(10*s15**2 + 14*tt*(tt - s25) + s15*(-8*tt + 14*s25 - 5*s35) - 8*tt*s35 + &
          &s35**2)) + ss*(-3*s15**2*(-2*tt + s25 + s35) + tt*(6*tt**2 - 11*tt*s25 + 4*s25**&
          &2 - 5*tt*s35 + 6*s25*s35 + s35**2) + s15*(6*tt*s25 - 4*s25**2 - 4*tt*s35 - 2*s25&
          &*s35 + 3*s35**2))))/((tt + s15 - s35)*(s15 + s25 - s35)) + (tt*s15*(2*tt**3*s15 &
          &- tt**2*s15**2 + 3*tt*s15**3 - 32*me2**3*(-tt + s15) + 4*ss**3*(-tt + s15) + 2*t&
          &t**3*s25 - 2*tt**2*s15*s25 + 6*tt*s15**2*s25 - 2*s15**3*s25 - tt**2*s25**2 + 3*t&
          &t*s15*s25**2 - 2*s15**2*s25**2 - tt**2*s15*s35 + tt*s15**2*s35 - 3*tt**2*s25*s35&
          & + 2*tt*s15*s25*s35 + s15**2*s25*s35 + tt*s25**2*s35 - s15*s25**2*s35 + tt*s25*s&
          &35**2 - s15*s25*s35**2 - 4*me2**2*(2*tt**2 + 5*s15**2 - 12*ss*(-tt + s15) - 10*t&
          &t*s25 + 5*s15*(-3*tt + 2*s25) - tt*s35 + s35**2) + ss**2*(-3*s15**2 + s15*(15*tt&
          & - 8*s25 - 5*s35) + tt*(-4*tt + 8*s25 + s35)) - 2*me2*(-2*tt**3 + 2*s15**3 + 12*&
          &ss**2*(-tt + s15) + 5*tt**2*s25 - 6*tt*s25**2 + 3*s15**2*(-4*tt + 3*s25) + 2*tt*&
          &*2*s35 - 3*tt*s25*s35 - tt*s35**2 + s25*s35**2 + s15*(4*tt**2 + 6*s25**2 - tt*s3&
          &5 + s35**2 + 2*s25*(-9*tt + s35)) - ss*(8*s15**2 + 6*tt*(tt - 3*s25) + 18*s15*s2&
          &5 - 2*tt*s35 + s35**2 + 5*s15*(-6*tt + s35))) + ss*(s15**3 - tt*(2*tt**2 - 5*tt*&
          &s25 + 4*s25**2 - 2*tt*s35 + 2*s25*s35 + s35**2) + s15**2*(5*s25 + 2*(-5*tt + s35&
          &)) + s15*(7*tt**2 + 4*s25**2 - 4*tt*s35 + s35**2 + 6*s25*(-3*tt + s35)))))/(s25*&
          &(tt + s15 - s35)) - (tt*s15*(me2**3*(32*tt - 48*s15) + tt**3*s15 + tt**2*s15**2 &
          &+ 2*tt*s15**3 + 2*ss**3*(-tt + s15) + tt**3*s25 + 3*tt*s15**2*s25 - tt**2*s25**2&
          & + tt*s15*s25**2 - tt**2*s15*s35 - 3*tt*s15**2*s35 - 2*tt**2*s25*s35 - 4*tt*s15*&
          &s25*s35 - 2*s15**2*s25*s35 + tt*s25**2*s35 - s15*s25**2*s35 + tt*s25*s35**2 + 3*&
          &s15*s25*s35**2 + ss**2*(-5*s15**2 - 4*s15*s25 + 3*s15*(tt + s35) + tt*(-2*tt + 4&
          &*s25 + s35)) + 4*me2**2*(-4*tt**2 - 9*s15**2 + 2*ss*(-5*tt + 7*s15) + 8*tt*s25 +&
          & 4*tt*s35 - 2*s35**2 + s15*(6*tt - 9*s25 + 6*s35)) - 2*me2*(4*s15**3 + 2*ss**2*(&
          &-4*tt + 5*s15) + 6*tt**2*s25 - 3*tt*s25**2 + s15**2*(-2*tt + 8*s25 - s35) + tt**&
          &2*s35 - 5*tt*s25*s35 - 2*tt*s35**2 + s25*s35**2 - ss*(6*tt**2 + 14*s15**2 - 11*t&
          &t*s25 + s15*(-9*tt + 13*s25 - 8*s35) - 4*tt*s35 + s35**2) + s15*(5*tt**2 + 3*s25&
          &**2 + 3*tt*s35 - 4*s35**2 - 3*s25*(tt + s35))) + ss*(2*s15**3 + s15**2*(-4*tt + &
          &5*s25 + s35) + tt*(-2*s25**2 + s25*(3*tt - 2*s35) + (tt - s35)*s35) + s15*(2*tt*&
          &*2 + 2*s25**2 + 6*tt*s35 - 3*s35**2 - 2*s25*(2*tt + s35)))))/((4*me2 - ss - tt +&
          & s25)*(s15 + s25 - s35)) + (2*tt*s15*(-8*me2**3*(-2*tt + 3*s15) + (ss - s15)*(ss&
          & - s15 - s25)*(2*tt*s15 + ss*(-tt + s15) + tt*s25 - s15*s25) + 2*me2**2*(-7*s15*&
          &*2 + 2*ss*(-5*tt + 7*s15) + 8*tt*s25 + tt*s35 - s35**2 + s15*(15*tt - 11*s25 + s&
          &35)) + me2*(ss**2*(8*tt - 10*s15) - 3*s15**3 + s15**2*(13*tt - 9*s25 + s35) - s1&
          &5*(tt**2 - 18*tt*s25 + 5*s25**2 + tt*s35 - s35**2) + tt*(5*s25**2 - s25*s35 + s3&
          &5**2) + ss*(11*s15**2 + 3*s15*(-8*tt + 5*s25) + tt*(-13*s25 + s35)))))/(s25*(4*m&
          &e2 - ss - tt + s35)) + (2*tt*(8*me2**3*(-2*tt + 3*s15) - 4*me2**2*(-3*ss*tt + 4*&
          &ss*s15 + 5*tt*s15 - 5*s15**2 + 3*tt*s25 - 4*s15*s25 + 3*s15*s35) - s15*(-ss + s1&
          &5 + s25)*(s15*(tt - s25) + ss*(s15 - s35) + s25*s35) + 2*me2*(2*s15**3 + ss**2*(&
          &-tt + s15) - tt*s25**2 + s15**2*(-3*tt + 5*s25 - 2*s35) + s15*(tt**2 - 3*tt*s25 &
          &+ s25**2 + tt*s35 - 4*s25*s35) + ss*(-5*s15**2 + 2*tt*s25 + s15*(3*tt - 2*s25 + &
          &4*s35)))))/(4*me2 - ss - tt + s25))*pvc10(6))/(tt**2*s15**2) + (64*Pi**2*((-2*(2&
          &*me2*(tt**2 + s15**2) + s15*(s15**2 + s15*(tt - s35) + tt*s35)))/tt + (2*s15*(2*&
          &me2*(tt**2 + s15**2) - tt*(tt*(tt - s35) + s15*(tt + s35))))/(tt*s35) + (8*me2**&
          &2*(-tt + s15) - 2*s15*(s15*(tt - s25) + ss*(s15 - s35) + s25*s35) - 4*me2*(-s15*&
          &*2 + ss*(-tt + s15) + tt*s25 + s15*(tt - s25 + s35)))/(4*me2 - ss - tt + s25) + &
          &(s15*(2*tt**3*s15 + 2*tt**2*s15**2 + 2*tt**3*s25 - 2*tt*s15**2*s25 - tt**2*s15*s&
          &35 + tt*s15**2*s35 - 3*tt**2*s25*s35 + 3*tt*s15*s25*s35 + tt*s25*s35**2 - s15*s2&
          &5*s35**2 + 2*me2*(s15**2*s35 + s15*(2*tt**2 + tt*s35 - s35**2) + tt*(2*tt**2 - 2&
          &*tt*s35 + s35**2)) - ss*(s15**2*s35 + s15*(2*tt**2 + tt*s35 - s35**2) + tt*(2*tt&
          &**2 - 2*tt*s35 + s35**2))))/(tt*s25*(tt + s15 - s35)) + (s15*(2*tt**4 + 2*tt**3*&
          &s15 + 2*tt**2*s15**2 + 2*tt*s15**3 - 2*tt**3*s25 + 2*tt*s15**2*s25 - 2*tt**3*s35&
          & + tt**2*s15*s35 - 3*tt*s15**2*s35 + 3*tt**2*s25*s35 - 3*tt*s15*s25*s35 - tt*s25&
          &*s35**2 + s15*s25*s35**2 - 2*me2*(s15**2*s35 + s15*(2*tt**2 + tt*s35 - s35**2) +&
          & tt*(2*tt**2 - 2*tt*s35 + s35**2)) + ss*(s15**2*s35 + s15*(2*tt**2 + tt*s35 - s3&
          &5**2) + tt*(2*tt**2 - 2*tt*s35 + s35**2))))/(tt*(tt + s15 - s35)*(s15 + s25 - s3&
          &5)) - (2*s15*(4*me2**2*tt*(-tt + s15) + tt*(ss - s15)*(2*tt*s15 + ss*(-tt + s15)&
          & + tt*s25 - s15*s25) + me2*(s15**3 - 2*s15**2*(-2*tt + s35) + tt*(4*ss*tt - 3*tt&
          &*s25 - s35**2) + s15*(tt*(-4*ss - 5*tt + 3*s25) - 2*tt*s35 + s35**2))))/(tt*s25*&
          &(4*me2 - ss - tt + s35)) + (s15*(8*me2**2*tt*(tt - s15) + (-(tt*s15) + s25*(-tt &
          &+ s35))*(s15*(tt - s35) + tt*(tt + s35)) - ss*(s15**2*(tt + s35) - s15*(tt**2 + &
          &s35**2) + tt*(-2*tt**2 + tt*s35 + s35**2)) + 2*me2*(s15**2*(2*tt + s35) - s15*(-&
          &2*ss*tt + tt*s35 + s35**2) + tt*(-2*tt*(ss + tt) + 2*tt*s35 + s35**2))))/(tt*s35&
          &*(4*me2 - ss - tt + s35)) + (s15*(8*me2**2*tt*(-tt + s15) + (-tt + s15)*(2*ss**2&
          &*tt + 2*ss*tt*(tt - s25) + ss*s15*s35 - tt*s15*s35 - ss*s35**2 + s25*s35*(-tt + &
          &s35)) + 2*me2*(s15**3 + s15**2*(tt - 3*s35) + tt*(tt*(4*ss + 2*tt - s25) - tt*s3&
          &5 - 2*s35**2) + s15*(tt*(-4*ss + s25) + 2*s35**2))))/(tt*(4*me2 - ss - tt + s25)&
          &*(s15 + s25 - s35)))*pvc10(7))/s15**2)/(16.*Pi**2)

          !print*,"3",diags

  end if

  if (tri81) then

  diags =  diags + (4*(4*me2*tt + (2*tt*s15*(-2*me2 + tt + s15 - s35))/s35 + 2*s15*s35 - (2&
          &*s15*(4*me2**2*tt + tt*(ss - s15)*(ss - s15 - s25) + me2*(tt*(-4*ss + 4*s15 + 3*&
          &s25) + s35**2)))/(s25*(4*me2 - ss - tt + s35)) - (s15*(2*tt**2*s15 + 2*tt*s15**2&
          & + 2*tt**2*s25 + 2*tt*s15*s25 - tt*s15*s35 - 3*tt*s25*s35 + s25*s35**2 + ss*(-2*&
          &tt**2 - 2*tt*s15 + 2*tt*s35 + s15*s35 - s35**2) + me2*(4*tt**2 + 4*tt*s15 - 4*tt&
          &*s35 - 2*s15*s35 + 2*s35**2)))/(s25*(tt + s15 - s35)) + (s15*(-2*tt**3 - 2*tt**2&
          &*s15 + 2*tt**2*s25 + 2*tt*s15*s25 + 2*tt**2*s35 - tt*s15*s35 - 3*tt*s25*s35 + s2&
          &5*s35**2 + ss*(-2*tt**2 - 2*tt*s15 + 2*tt*s35 + s15*s35 - s35**2) + me2*(4*tt**2&
          & + 4*tt*s15 - 4*tt*s35 - 2*s15*s35 + 2*s35**2)))/((tt + s15 - s35)*(s15 + s25 - &
          &s35)) - (s15*(-8*me2**2*tt - 2*ss**2*tt + (tt*s15 + s25*(tt - s35))*s35 + ss*(2*&
          &tt*(-tt + s25) - s15*s35 + s35**2) + 2*me2*(tt*(4*ss + 2*tt - s25) - tt*s35 - 2*&
          &s35**2 + s15*(tt + s35))))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + (8*me2*&
          &*2*tt + me2*(-2*s15**2 + 4*tt*(-ss + s25) + 2*s15*(tt + s35)) + s15*(tt*s15 + ss&
          &*(s15 - s35) + s25*(tt + s35)))/(4*me2 - ss - tt + s25) + (s15*(-8*me2**2*tt + (&
          &tt*s15 + s25*(tt - s35))*s35 + ss*(-2*tt**2 + s35**2 - s15*(2*tt + s35)) + 2*me2&
          &*(2*tt*(ss + tt) - tt*s35 - s35**2 + s15*(2*tt + s35))))/(s35*(4*me2 - ss - tt +&
          & s35))))/(tt*s15**2) + ((128*Pi**2*((s15*(-4*me2*(tt + s15) + 2*ss*(tt + s15) + &
          &(2*tt + s15)*(tt + s15 - s35)))/(tt*(tt + s15 - s35)*(s15 + s25 - s35)) - (2*s15&
          &*(-2*me2 + tt + s15 - s35))/(tt*s35) - (s15*(-2*me2 + tt + s15 - s35))/(s35*(4*m&
          &e2 - ss - tt + s35)) - (2*(2*me2*tt + s15*s35))/tt**2 - (2*me2*tt + s15*s35)/(tt&
          &*(4*me2 - ss - tt + s25)) + (s15*(4*me2*(tt + s15) - 2*ss*(tt + s15) + s15*(tt +&
          & s15 + s35)))/(tt*s25*(tt + s15 - s35)) + (s15*(ss*(tt + s15) + tt*(tt + s15 - s&
          &35) - me2*(2*tt + 3*s15 + 3*s35)))/(tt*(4*me2 - ss - tt + s25)*(s15 + s25 - s35)&
          &) + (s15*(-((ss - s15)*(tt + s15)) + me2*(2*tt + 3*s15 + 3*s35)))/(tt*s25*(4*me2&
          & - ss - tt + s35)))*pva1(1))/s15**2 + (64*Pi**2*(-((tt*s15*(6*tt**3 - 4*ss**2*s1&
          &5 + 7*tt**2*s15 + 2*tt*s15**2 - 32*me2**2*(tt + s15) - tt**2*s25 + 2*tt*s15*s25 &
          &+ 2*s15**2*s25 - 6*tt**2*s35 - 2*tt*s15*s35 + tt*s25*s35 - 2*s15*s25*s35 + 4*me2&
          &*(4*ss*tt + tt**2 + 6*ss*s15 + tt*s15 - 2*tt*s25 - s15*s25 - 2*tt*s35) + ss*(6*t&
          &t**2 + tt*s15 + 4*s15*s25 - tt*s35)))/((tt + s15 - s35)*(s15 + s25 - s35))) + (2&
          &*s15*(3*tt**3 + 3*tt**2*s15 + tt*s15**2 - 8*me2**2*(2*tt + s15) + tt*s15*s25 + s&
          &s*s15*(s15 - s35) - 3*tt**2*s35 - s15*s25*s35 + 2*me2*(tt**2 + 2*ss*s15 + 4*tt*s&
          &15 - s15**2 - 3*tt*s35 + s15*s35)))/s35 - (2*tt*s15*((ss - s15)*(tt + s15)*(ss -&
          & tt - s25) + 12*me2**2*(tt + s15 + s35) + me2*(2*tt**2 + 9*tt*s15 + 8*s15**2 - 2&
          &*ss*(4*tt + 5*s15) + 3*tt*s25 + 5*s15*s25 + 2*tt*s35 - 4*s15*s35 + 3*s35**2)))/(&
          &s25*(4*me2 - ss - tt + s35)) + (2*tt*s15*(-12*me2**2*(tt + s15) + (ss*(-tt + s15&
          &) + tt*(tt + s15 + s25))*(tt + s15 - s35) + 2*me2*(-2*s15**2 + ss*(tt + s15) + 2&
          &*tt*(tt - s25 - s35) + s15*(2*tt + s35))))/(s35*(4*me2 - ss - tt + s35)) + (2*tt&
          &*(12*me2**2*(tt + s15) + 2*me2*(-(ss*(tt + s15)) + s15*(-2*tt + s25) + tt*(tt + &
          &s25)) + s15*(s15*s25 + tt*(s25 + s35))))/(4*me2 - ss - tt + s25) - (tt*s15*(-2*s&
          &s**2*(tt + s15) - 24*me2**2*(tt + s15 + s35) + 2*me2*(8*ss*tt + 4*tt**2 + 10*ss*&
          &s15 - 6*s15**2 - 5*tt*s25 - 3*s15*s25 - 3*tt*s35 + 6*s15*s35 - 3*s35**2) + tt*(2&
          &*s15**2 + s15*(3*tt + 2*s25 - 2*s35) - (2*tt + s25)*(-tt + s35)) + ss*(2*s15**2 &
          &+ s15*(tt + 2*s25 - 2*s35) + tt*(2*s25 + s35))))/((4*me2 - ss - tt + s25)*(s15 +&
          & s25 - s35)) + 2*(8*me2**2*(2*tt + s15) + s15*(-(tt*s15) + tt*s25 + 2*s15*s25 + &
          &3*tt*s35 - s25*s35 + ss*(-s15 + s35)) + 2*me2*(3*tt**2 - 2*ss*s15 + s15**2 + s15&
          &*(-3*tt + 2*s25 + s35))) - (tt*s15*(4*ss**2*s15 + 3*tt**2*s15 + 2*tt*s15**2 + 32&
          &*me2**2*(tt + s15) - tt**2*s25 + 2*s15**2*s25 + 4*tt*s15*s35 + tt*s25*s35 + 4*me&
          &2*(3*s15**2 - 2*ss*(2*tt + 3*s15) + tt*(3*tt + 2*s25) + s15*(5*tt + 3*s25 + s35)&
          &) - ss*(2*s15**2 + tt*(6*tt + s35) + s15*(3*tt + 4*s25 + 2*s35))))/(s25*(tt + s1&
          &5 - s35)))*pvb12(1))/(tt**2*s15**2) + (128*Pi**2*((tt*s15*(tt**3 - 24*me2**2*s15&
          & + tt**2*s15 + tt*s15**2 + me2*(-2*tt**2 + 4*ss*s15 + 6*tt*s15 - 6*s15**2) + tt*&
          &s15*s25 + ss*s15*(s15 - s35) - tt**2*s35 - tt*s15*s35 - s15**2*s35 - s15*s25*s35&
          & + s15*s35**2))/(s35*(4*me2 - ss - tt + s35)) - (2*s15*(-tt**3 + 8*me2**2*s15 - &
          &tt**2*s15 - tt*s15**2 + 2*me2*(tt**2 - 2*ss*s15 - tt*s15 + s15**2) - tt*s15*s25 &
          &+ tt**2*s35 + tt*s15*s35 + s15**2*s35 + s15*s25*s35 - s15*s35**2 + ss*s15*(-s15 &
          &+ s35)))/s35 + (tt*(24*me2**2*s15 + me2*(2*tt**2 - 4*ss*s15 - 6*tt*s15 + 4*s15**&
          &2 + 4*s15*s25 - 6*s15*s35) + s15*(s15**2 + tt*s25 + 2*s15*s25 + tt*s35 - s15*s35&
          & - s25*s35 + ss*(-s15 + s35))))/(4*me2 - ss - tt + s25) + 2*(8*me2**2*s15 + s15*&
          &(s15**2 + tt*s25 + 2*s15*s25 + tt*s35 - s15*s35 - s25*s35 + ss*(-s15 + s35)) - 2&
          &*me2*(-tt**2 + 2*ss*s15 - 2*s15**2 + s15*(tt - 2*s25 + s35))) + (tt*s15*(24*me2*&
          &*2*s15 + 2*ss**2*s15 + (s15**2 + tt*(tt - s25) + s15*(s25 - s35))*(-tt + s35) - &
          &me2*(16*ss*s15 - 7*s15**2 + tt*(-2*tt + s35) + s15*(5*tt - 6*s25 + s35)) + ss*(-&
          &2*s15**2 + tt*(-tt + s35) + s15*(tt - 2*s25 + s35))))/((4*me2 - ss - tt + s25)*(&
          &s15 + s25 - s35)) - (tt*s15*(2*tt**3 - 16*me2**2*s15 - 4*ss**2*s15 + tt**2*s15 +&
          & 2*tt*s15**2 + s15**3 - tt**2*s25 + 3*tt*s15*s25 + 2*s15**2*s25 - 2*tt**2*s35 - &
          &tt*s15*s35 - 2*s15**2*s35 + tt*s25*s35 - 3*s15*s25*s35 + s15*s35**2 + 2*me2*(8*s&
          &s*s15 - 2*s15**2 + s15*(tt - 2*s25) + tt*(-2*tt + s35)) + ss*(s15**2 + tt*(2*tt &
          &- s35) + s15*(-3*tt + 4*s25 + s35))))/((tt + s15 - s35)*(s15 + s25 - s35)) - (tt&
          &*s15*(16*me2**2*s15 + 4*ss**2*s15 + tt**2*s15 + s15**3 - tt**2*s25 - tt*s15*s25 &
          &+ 2*s15**2*s25 + tt*s15*s35 + tt*s25*s35 + s15*s25*s35 + s15*s35**2 + 2*me2*(-8*&
          &ss*s15 + 6*s15**2 + s15*(tt + 6*s25) + tt*(2*tt + s35)) - ss*(3*s15**2 + tt*(2*t&
          &t + s35) + s15*(-tt + 4*s25 + 3*s35))))/(s25*(tt + s15 - s35)) - (tt*s15*(24*me2&
          &**2*s15 + (ss - s15)*(-tt**2 + 2*ss*s15 - s15**2 - 2*s15*s25) + me2*(-16*ss*s15 &
          &+ 11*s15**2 + s15*(tt + 10*s25 - s35) + tt*(2*tt + 5*s35))))/(s25*(4*me2 - ss - &
          &tt + s35)))*pvb12(2))/(tt**2*s15**2) + (64*Pi**2*((-2*s15*(tt**3 + tt**2*s15 - t&
          &t**2*s35 + tt*s15*s35 - tt*s25*s35 - 2*s15*s25*s35 + ss*(s15 - s35)*s35 + s25*s3&
          &5**2 - 4*me2**2*(3*tt + 2*s35) + 2*me2*(2*tt**2 + 2*tt*s15 + 2*ss*s35 - tt*s35 -&
          & s15*s35 - 2*s25*s35 + s35**2)))/s35 + (tt*s15*(-((tt*s15 + s25*(tt - s35))*(2*t&
          &t + s15 - s35)) + 4*me2**2*(5*tt + 2*s35) + ss*(2*tt**2 - s15**2 - 2*tt*s35 + s3&
          &5**2) + 2*me2*(-5*tt**2 + s15**2 + 4*tt*s25 + 2*tt*s35 + 2*s25*s35 - s35**2 - 2*&
          &ss*(tt + s35))))/(s35*(4*me2 - ss - tt + s35)) - 2*(4*me2**2*(3*tt + 2*s15) + 2*&
          &me2*(tt**2 - 2*ss*s15 - 3*tt*s15 + s15**2 + 2*s15*s25) + s15*(-(tt*s15) + tt*s25&
          & + 2*s15*s25 + tt*s35 - s25*s35 + ss*(-s15 + s35))) + (tt*s15*(2*tt**2*s15 + 2*t&
          &t**2*s25 + 2*s15**2*s25 - 2*tt*s25**2 + 4*me2**2*(6*tt + 5*s15 - s35) - tt*s15*s&
          &35 - 3*tt*s25*s35 + 2*s25**2*s35 + s25*s35**2 + 2*ss**2*(s15 + s35) - ss*(2*tt**&
          &2 + 2*s15**2 - 2*tt*s25 + 2*s15*s25 - 2*tt*s35 + s15*s35 + 4*s25*s35 + s35**2) +&
          & 2*me2*(2*tt**2 + 4*tt*s15 + 3*s15**2 + 3*tt*s25 + 4*s15*s25 - 2*tt*s35 + 3*s15*&
          &s35 + s25*s35 + s35**2 - ss*(6*tt + 7*s15 + s35))))/(s25*(tt + s15 - s35)) - (2*&
          &tt*(2*me2**2*(5*tt + 2*s15) + s15*(tt + s15)*s25 + me2*(-2*ss*(tt + s15) + 2*tt*&
          &s25 + s15*(-2*tt + 2*s25 + s35))))/(4*me2 - ss - tt + s25) + (tt*s15*(2*(ss - s1&
          &5)*(tt + s15)*(ss - s25) + 4*me2**2*(5*tt + 2*s35) + me2*(15*tt*s15 + 10*s15**2 &
          &+ 9*tt*s25 + 6*s15*s25 - 8*s15*s35 - s25*s35 + 2*s35**2 + ss*(-14*tt - 11*s15 + &
          &3*s35))))/(s25*(4*me2 - ss - tt + s35)) + (tt*s15*(2*tt**3 + 2*tt**2*s15 + 4*tt*&
          &s15*s25 + 2*s15**2*s25 + 2*tt*s25**2 - 2*tt**2*s35 + tt*s15*s35 - tt*s25*s35 - 4&
          &*s15*s25*s35 - 2*s25**2*s35 + s25*s35**2 - 2*ss**2*(s15 + s35) - 4*me2**2*(6*tt &
          &+ 3*s15 + 5*s35) - ss*(-2*tt**2 + 2*tt*s15 + 2*s15**2 + 2*tt*s25 - 2*s15*s25 - 3&
          &*s15*s35 - 4*s25*s35 + s35**2) + 2*me2*(4*tt**2 + 4*tt*s15 + 3*s15**2 - tt*s25 -&
          & 2*tt*s35 - 7*s15*s35 - 7*s25*s35 + s35**2 + ss*(6*tt + 5*s15 + 7*s35))))/((tt +&
          & s15 - s35)*(s15 + s25 - s35)) + (tt*s15*(2*tt**2*s15 + 2*tt*s15**2 + 2*tt**2*s2&
          &5 + 4*tt*s15*s25 + 2*tt*s25**2 + 4*me2**2*(-5*tt + s15 - 9*s35) - tt*s15*s35 - 3&
          &*tt*s25*s35 - 2*s15*s25*s35 - 2*s25**2*s35 + s25*s35**2 - 2*ss**2*(tt + s35) + s&
          &s*(-2*tt**2 + 2*tt*s35 + s15*s35 + 4*s25*s35 - s35**2) + me2*(10*tt**2 - 5*tt*s1&
          &5 - 4*s15**2 - 7*tt*s25 + 2*s15*s25 - 2*tt*s35 - 13*s25*s35 + ss*(14*tt + s15 + &
          &11*s35))))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)))*pvb2(1))/(tt**2*s15**2) &
          &+ (64*Pi**2*(4*(2*me2 - ss - tt + s25)*(4*me2**2 - 2*me2*(ss - s25) + s15*s25) +&
          & (2*tt*(24*me2**3 - 4*me2**2*(4*ss + 2*tt + s15 - 4*s25) + s15*s25*(-ss - tt + s&
          &25) + 2*me2*(ss**2 + ss*(tt + s15 - 2*s25) + s25*(-tt + s25) + s15*(tt + s25))))&
          &/(4*me2 - ss - tt + s25) + (2*s15*(-16*me2**3 + 2*ss**2*tt + 8*me2**2*(2*ss + 2*&
          &tt - s25) + ss*(-tt + s25)*(-2*tt + s15 + s35) + (-tt + s25)*(tt*s15 + tt*s25 - &
          &s25*s35) - 2*me2*(2*ss**2 + 6*ss*tt + 2*tt**2 - tt*s15 - 2*ss*s25 - 3*tt*s25 + s&
          &15*s25 - tt*s35 + s25*s35)))/s35 + (2*tt*s15*(-24*me2**3 + (ss - s15)*(ss - s25)&
          &*(ss + tt - s25) + 2*me2**2*(14*ss + 4*tt - 4*s15 - 11*s25 + s35) - me2*(10*ss**&
          &2 - 3*tt*s15 - 4*tt*s25 + 6*s15*s25 + 5*s25**2 + ss*(6*tt - 6*s15 - 15*s25 + s35&
          &))))/(s25*(4*me2 - ss - tt + s35)) + (tt*s15*(-48*me2**3 + 2*ss**2*tt + 8*me2**2&
          &*(4*ss + 5*tt - 3*s25 - s35) + ss*(-tt + s25)*(-2*tt + s15 + s35) + (-tt + s25)*&
          &(tt*s15 + tt*s25 - s25*s35) + me2*(-4*ss**2 - 8*tt**2 + 2*tt*s15 + 10*tt*s25 - 6&
          &*s15*s25 + 6*tt*s35 - 6*s25*s35 + 4*ss*(-5*tt + s25 + s35))))/(s35*(4*me2 - ss -&
          & tt + s35)) + (tt*s15*(32*me2**3 - 4*ss**3 - 2*s25*(-tt + s25)*(tt + s15 - s35) &
          &+ 4*me2**2*(-12*ss - 8*tt + s15 + 6*s25 + s35) + ss**2*(-8*tt - s15 + 8*s25 + s3&
          &5) + ss*(-4*tt**2 - tt*s15 + 9*tt*s25 + 2*s15*s25 - 4*s25**2 + 2*tt*s35 - 3*s25*&
          &s35) + 2*me2*(12*ss**2 + 4*tt**2 - tt*s15 - 8*tt*s25 + 2*s25**2 - 2*tt*s35 + 3*s&
          &25*s35 - 2*ss*(-8*tt + 7*s25 + s35))))/((tt + s15 - s35)*(s15 + s25 - s35)) + (t&
          &t*s15*(-32*me2**3 + 4*ss**3 + 8*me2**2*(6*ss + 2*tt - 2*s15 - 5*s25) + 2*s15*(tt&
          & - s25)*s25 + ss*s15*(-3*tt + 4*s25) + ss*s25*(-5*tt + 4*s25 + s35) - ss**2*(-4*&
          &tt + 3*s15 + 8*s25 + s35) - 2*me2*(12*ss**2 - 4*tt*s15 - 5*tt*s25 + 6*s15*s25 + &
          &6*s25**2 - ss*(-8*tt + 7*s15 + 18*s25 + s35))))/(s25*(tt + s15 - s35)) + (tt*s15&
          &*(48*me2**3 + me2**2*(-56*ss - 40*tt + 12*s15 + 36*s25 + 8*s35) + ss*(-2*ss**2 -&
          & 2*tt**2 + s15*(tt - 2*s25) + 3*tt*s25 - 2*s25**2 + ss*(-4*tt + s15 + 4*s25 - s3&
          &5) + s25*s35) + 2*me2*(10*ss**2 + ss*(14*tt - 3*s15 - 13*s25) + s15*(-3*tt + 4*s&
          &25) + (-tt + s25)*(-4*tt + 3*s25 + 2*s35))))/((4*me2 - ss - tt + s25)*(s15 + s25&
          & - s35)))*pvb2(2))/(tt**2*s15**2) + (64*Pi**2*(-((tt*s15 + tt*s25 - s25*s35 - 4*&
          &me2*(s15 + s35) + ss*(s15 + s35))/(4*me2 - ss - tt + s25)) - (tt*s15 + tt*s25 - &
          &s25*s35 - 2*me2*(s15 + s35) + ss*(s15 + s35))/(tt + s15 - s35))*(-me2/3. + 4*pvb&
          &2(3)))/(tt*s15*(s15 + s25 - s35)) + (64*me2*Pi**2*(8*me2**2*(s15 + s35) + (ss - &
          &s15 - s25 + s35)*(tt*s15 + s25*(tt - s35) + ss*(s15 + s35)) - 2*me2*(-2*s15**2 +&
          & s15*(tt - s25) + 2*tt*s25 - tt*s35 - 3*s25*s35 + 2*s35**2 + 3*ss*(s15 + s35)))*&
          &pvb2(4))/(tt*s15*(4*me2 - ss - tt + s25)*(tt + s15 - s35)*(s15 + s25 - s35)) + (&
          &64*Pi**2*(-32*me2**3 - 8*me2**2*(-4*ss + tt + 2*s15 + 4*s25) - 4*me2*(2*ss**2 + &
          &tt**2 - 3*tt*s15 + s15**2 - 2*tt*s25 + 4*s15*s25 + 2*s25**2 - 2*ss*(-tt + s15 + &
          &2*s25)) + 2*s15*(s15*(tt - 2*s25) + tt*s25 - 2*s25**2 + ss*(s15 + 2*s25 - s35) -&
          & tt*s35 + s25*s35) - (2*tt*(24*me2**3 + s15*s25*(-ss + s15 + s25) + 2*me2**2*(-8&
          &*ss + tt + 4*s15 + 8*s25) + me2*(2*ss**2 - 2*tt*s15 - 4*ss*s25 + 4*s15*s25 + 2*s&
          &25**2 - s15*s35)))/(4*me2 - ss - tt + s25) + (2*s15*(16*me2**3 - 2*ss**2*tt - tt&
          &**3 - tt*s15**2 - 4*me2**2*(4*ss + tt - 2*s15 - 2*s25) + tt**2*s25 - 2*tt*s15*s2&
          &5 - tt*s25**2 + tt**2*s35 - tt*s25*s35 + s15*s25*s35 + s25**2*s35 + 2*me2*(2*ss*&
          &*2 + s15**2 - 3*tt*s25 - 2*ss*(-3*tt + s15 + s25) + s15*(-4*tt + s25 - s35) + tt&
          &*s35 + s25*s35) + ss*(-s15**2 + (tt - s25)*(-2*tt + s35) + s15*(tt - s25 + s35))&
          &))/s35 - (tt*s15*(-48*me2**3 + 2*ss**2*tt + tt**2*s15 + 2*tt*s15**2 + tt**2*s25 &
          &+ 3*tt*s15*s25 + tt*s25**2 + 4*me2**2*(8*ss + 5*tt - 6*s15 - 6*s25 - 2*s35) - 2*&
          &tt*s15*s35 - tt*s25*s35 - s25**2*s35 + ss*(2*s15**2 - 2*tt*s25 + s15*(-tt + s25 &
          &- 2*s35) + tt*s35 + s25*s35) + 2*me2*(-2*ss**2 + tt**2 + 4*tt*s15 - 4*s15**2 + t&
          &t*s25 - 3*s15*s25 + 2*s15*s35 - 3*s25*s35 + 2*ss*(-4*tt + s15 + s25 + s35))))/(s&
          &35*(4*me2 - ss - tt + s35)) + (tt*s15*(-32*me2**3 + 4*ss**3 + 2*tt**3 + tt**2*s1&
          &5 - 3*tt**2*s25 + 2*s15**2*s25 + 2*tt*s25**2 + 2*s15*s25**2 - 2*tt**2*s35 + 3*tt&
          &*s25*s35 - 2*s15*s25*s35 - 2*s25**2*s35 + 4*me2**2*(12*ss + 2*tt - 5*s15 - 6*s25&
          & + s35) - ss**2*(-8*tt + 3*s15 + 8*s25 + s35) + ss*(4*s25**2 + 2*s15*(-tt + s25)&
          & + 3*s25*(-3*tt + s35) - 3*tt*(-2*tt + s35)) - 2*me2*(12*ss**2 + s15**2 - 2*ss*(&
          &-5*tt + 4*s15 + 7*s25) + 2*s15*(-tt + s25 - 2*s35) + s25*(-2*tt + 2*s25 + s35)))&
          &)/((tt + s15 - s35)*(s15 + s25 - s35)) + (tt*s15*(32*me2**3 - 4*ss**3 + tt**2*s1&
          &5 - tt**2*s25 - 2*tt*s15*s25 + 2*s15**2*s25 + 2*s15*s25**2 + 2*tt*s15*s35 + tt*s&
          &25*s35 + ss**2*(-4*tt + 7*s15 + 8*s25 + s35) + me2**2*(-48*ss + 8*tt + 44*s15 + &
          &40*s25 + 4*s35) - ss*(2*s15**2 + 4*s25**2 + s25*(-5*tt + s35) + tt*(2*tt + s35) &
          &+ 2*s15*(-2*tt + 4*s25 + s35)) + 2*me2*(12*ss**2 + 2*tt**2 + 5*s15**2 - 2*tt*s25&
          & + 12*s15*s25 + 6*s25**2 + s25*s35 - 2*ss*(-tt + 9*s15 + 9*s25 + s35))))/(s25*(t&
          &t + s15 - s35)) + (tt*s15*(48*me2**3 - 2*(ss - s15)*(ss - s25)*(ss - s15 - s25) &
          &+ 4*me2**2*(-14*ss + tt + 11*s15 + 11*s25 + 6*s35) + me2*(20*ss**2 + 14*s15**2 -&
          & 5*tt*s25 + 10*s25**2 + s15*(tt + 22*s25 - 8*s35) - 2*tt*s35 - 3*s25*s35 + 6*s35&
          &**2 - ss*(2*tt + 33*s15 + 30*s25 + s35))))/(s25*(4*me2 - ss - tt + s35)) + (tt*s&
          &15*(-48*me2**3 + 2*ss**3 + tt*(2*s15**2 + s15*(tt + 2*s25 - 2*s35) + s25*(tt - s&
          &35)) + 4*me2**2*(14*ss + 5*tt - 5*s15 - 9*s25 - 4*s35) + ss**2*(2*tt - 3*s15 - 4&
          &*s25 + s35) + ss*(2*s15**2 + 4*s15*s25 + 2*s25**2 + tt*s35 - 2*s15*(tt + s35) - &
          &s25*(tt + s35)) + me2*(-20*ss**2 + 2*tt**2 - 12*s15**2 + 3*tt*s25 - 6*s25**2 + s&
          &s*(-14*tt + 23*s15 + 26*s25 - s35) + 3*s25*s35 - 6*s35**2 + s15*(3*tt - 14*s25 +&
          & 18*s35))))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)))*pvb3(1))/(tt**2*s15**2)&
          & + (64*Pi**2*(-4*(2*me2 - ss + s15 + s25)*(4*me2**2 - 2*me2*(ss + tt - s15 - s25&
          &) + s15*(s15 + s25 - s35)) - (2*tt*(24*me2**3 - 4*me2**2*(4*ss + 2*tt - 3*s15 - &
          &4*s25) + s15*(-ss + s15 + s25)*(s15 + s25 - s35) + 2*me2*(ss**2 - tt*s15 + 2*s15&
          &**2 + ss*(tt - s15 - 2*s25) - tt*s25 + 3*s15*s25 + s25**2 - 2*s15*s35)))/(4*me2 &
          &- ss - tt + s25) + (2*tt*s15*(24*me2**3 - (ss - s15)*(ss - s15 - s25)*(ss + tt -&
          & s15 - s25) - 2*me2**2*(14*ss + 4*tt - 14*s15 - 11*s25 + s35) + me2*(10*ss**2 - &
          &7*tt*s15 + 9*s15**2 - 4*tt*s25 + 14*s15*s25 + 5*s25**2 - s15*s35 - 2*s25*s35 + s&
          &35**2 + ss*(6*tt - 20*s15 - 15*s25 + s35))))/(s25*(4*me2 - ss - tt + s35)) + (2*&
          &s15*(16*me2**3 - 2*ss**2*tt + tt**2*s15 - tt*s15**2 + tt**2*s25 - 2*tt*s15*s25 -&
          & tt*s25**2 + tt*s15*s35 + 2*s15**2*s35 + 3*s15*s25*s35 + s25**2*s35 - 2*s15*s35*&
          &*2 - s25*s35**2 + 8*me2**2*(-2*ss - 2*tt + s15 + s25 + s35) + ss*(-2*tt**2 - s15&
          &**2 + s15*(tt - s25) + 2*tt*s25 + tt*s35 - s25*s35 + s35**2) + 2*me2*(2*ss**2 + &
          &2*tt**2 + s15**2 - 3*tt*s25 - 2*tt*s35 + 3*s25*s35 - s35**2 - 2*ss*(-3*tt + s15 &
          &+ s25 + s35) + s15*(-2*tt + s25 + 2*s35))))/s35 + (tt*s15*(48*me2**3 - 2*ss**2*t&
          &t + tt**2*s15 - tt*s15**2 + tt**2*s25 - 2*tt*s15*s25 - tt*s25**2 - 8*me2**2*(4*s&
          &s + 5*tt - 3*s15 - 3*s25 - 2*s35) + tt*s15*s35 + 2*s15**2*s35 + 3*s15*s25*s35 + &
          &s25**2*s35 - 2*s15*s35**2 - s25*s35**2 + ss*(-2*tt**2 + tt*s15 - s15**2 + 2*tt*s&
          &25 - s15*s25 + tt*s35 - s25*s35 + s35**2) + 2*me2*(2*ss**2 + 4*tt**2 - 4*tt*s15 &
          &+ 3*s15**2 - 5*tt*s25 + 3*s15*s25 - 4*tt*s35 + 2*s15*s35 + 5*s25*s35 - s35**2 - &
          &2*ss*(-5*tt + s15 + s25 + 2*s35))))/(s35*(4*me2 - ss - tt + s35)) + (tt*s15*(32*&
          &me2**3 - 4*ss**3 + tt**2*s15 - 2*tt*s15**2 + 2*s15**3 - 8*me2**2*(6*ss + 2*tt - &
          &6*s15 - 5*s25) + tt**2*s25 - 4*tt*s15*s25 + 4*s15**2*s25 - 2*tt*s25**2 + 2*s15*s&
          &25**2 - 3*tt*s15*s35 - 2*tt*s25*s35 + 2*s15*s25*s35 + 2*s25**2*s35 + 2*s15*s35**&
          &2 + s25*s35**2 + ss**2*(-4*tt + 9*s15 + 8*s25 + 3*s35) + 2*me2*(12*ss**2 + 10*s1&
          &5**2 - 7*tt*s25 + 6*s25**2 + 2*s25*s35 + s35**2 + s15*(-8*tt + 16*s25 + s35) - s&
          &s*(-8*tt + 21*s15 + 18*s25 + 3*s35)) - ss*(6*s15**2 - 7*tt*s25 + 4*s25**2 - tt*s&
          &35 + 5*s25*s35 + s35**2 + s15*(-8*tt + 10*s25 + 5*s35))))/(s25*(tt + s15 - s35))&
          & - (tt*s15*(48*me2**3 - 4*me2**2*(14*ss + 10*tt - 7*s15 - 9*s25 - 4*s35) - (ss +&
          & tt - s35)*(2*ss**2 + 2*s15**2 + s15*(-tt + 4*s25 - 2*s35) + s25*(-tt + 2*s25 - &
          &s35) + ss*(2*tt - 3*s15 - 4*s25 + s35)) + 2*me2*(10*ss**2 + 4*tt**2 + 3*s15**2 -&
          & 8*tt*s25 + 3*s25**2 - 4*tt*s35 + 5*s25*s35 + 2*s15*(-3*tt + 3*s25 + s35) - ss*(&
          &-14*tt + 10*s15 + 13*s25 + 5*s35))))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35))&
          & + (tt*s15*(-32*me2**3 + 4*ss**3 - 3*tt**2*s15 + 2*tt*s15**2 + 2*s15**3 - 3*tt**&
          &2*s25 + 6*tt*s15*s25 + 4*s15**2*s25 + 4*tt*s25**2 + 2*s15*s25**2 + 8*me2**2*(6*s&
          &s + 4*tt - 2*s15 - 3*s25 - 2*s35) + tt*s15*s35 - 4*s15**2*s35 + 2*tt*s25*s35 - 8&
          &*s15*s25*s35 - 4*s25**2*s35 + 2*s15*s35**2 + s25*s35**2 - ss**2*(-8*tt + 5*s15 +&
          & 8*s25 + 3*s35) + ss*(4*tt**2 - 8*tt*s15 - 11*tt*s25 + 4*s15*s25 + 4*s25**2 - 3*&
          &tt*s35 + 5*s15*s35 + 7*s25*s35 - s35**2) - 2*me2*(12*ss**2 + 4*tt**2 - 6*tt*s15 &
          &- 9*tt*s25 + 2*s15*s25 + 2*s25**2 - 4*tt*s35 + 5*s15*s35 + 8*s25*s35 - s35**2 - &
          &ss*(-16*tt + 9*s15 + 14*s25 + 7*s35))))/((tt + s15 - s35)*(s15 + s25 - s35)))*pv&
          &b3(2))/(tt**2*s15**2) + (64*Pi**2*(32*me2**3*(-tt + s15) + 8*me2**2*(3*s15**2 - &
          &2*ss*(2*tt + s15) + tt*(tt + 4*s25) + s15*(3*tt + 2*s25 - 3*s35)) - 2*tt*s15*(s1&
          &5*(tt - 2*s25) + tt*s25 - 2*s25**2 + ss*(s15 + 2*s25 - s35) - tt*s35 + s25*s35) &
          &+ 4*me2*(2*ss**2*tt + 2*s15**3 + tt*(tt**2 - 2*tt*s25 + 2*s25**2) + 2*s15**2*(tt&
          & + s25 - s35) - ss*(-2*tt**2 + 2*tt*s15 + s15**2 + 4*tt*s25 - s15*s35) - s15*(3*&
          &tt**2 - 5*tt*s25 + s25*s35)) + (2*tt*(8*me2**3*(tt + 3*s15) + s15*(-(ss*tt*s25) &
          &+ tt*s15*s25 + tt*s25**2) + 2*me2**2*(3*s15**2 - 2*ss*(4*tt + s15) + tt*(tt + 8*&
          &s25) + s15*(-tt + 2*s25 - 5*s35)) + me2*(2*ss**2*tt + 2*s15**3 + 2*tt*s25**2 + s&
          &15**2*(tt + 2*s25 - 2*s35) - ss*(s15**2 + 4*tt*s25 - s15*s35) + s15*(5*tt*s25 + &
          &tt*s35 - s25*s35))))/(4*me2 - ss - tt + s25) + (tt*s15*(2*ss**2*tt**2 + tt**3*s1&
          &5 + 2*tt**2*s15**2 - 16*me2**3*(tt + 3*s15) + tt**3*s25 + 3*tt**2*s15*s25 + tt**&
          &2*s25**2 - 2*tt**2*s15*s35 - tt**2*s25*s35 - tt*s25**2*s35 + 4*me2**2*(tt**2 + 2&
          &*tt*s15 - 2*s15**2 + 2*ss*(4*tt + s15) - 2*tt*s25 + tt*s35 - s15*s35 - s35**2) +&
          & ss*(2*tt*s15**2 - 2*tt**2*s25 + tt**2*s35 + tt*s25*s35 + s15*(-tt**2 + tt*s25 -&
          & 2*tt*s35)) - 2*me2*(2*ss**2*tt - tt**3 + tt**2*s25 + s25*s35**2 + 2*s15**2*(tt &
          &+ s35) + s15*(3*tt*s25 - 3*tt*s35 + 2*s25*s35 - 2*s35**2) + ss*(8*tt**2 - 2*tt*s&
          &25 + s15*s35 - s35**2))))/(s35*(4*me2 - ss - tt + s35)) - (2*s15*(16*me2**3*(-tt&
          & + s15) + 4*me2**2*(s15**2 - 2*ss*(2*tt + s15) + tt*(3*tt + 2*s25 - 3*s35) + s15&
          &*(4*tt + s35)) + 2*me2*(2*ss**2*tt + 2*s15**2*s35 + tt*(-3*tt*s25 + tt*s35 + s25&
          &*s35) + s15*(-4*tt**2 + tt*s35 + s25*s35 - 2*s35**2) - ss*(s15**2 + 2*tt*(-3*tt &
          &+ s25) - s15*(-2*tt + s35))) - tt*(2*ss**2*tt + tt*s15**2 - (tt**2 - tt*s25 + s2&
          &5**2)*(-tt + s35) + s15*(2*tt*s25 - s25*s35) + ss*(s15**2 + s15*(-tt + s25 - s35&
          &) + (-tt + s25)*(-2*tt + s35)))))/s35 - (tt*s15*(32*me2**3*tt + 4*ss**3*tt + 2*t&
          &t**4 + tt**3*s15 - 3*tt**3*s25 + 2*tt*s15**2*s25 + 2*tt**2*s25**2 + 2*tt*s15*s25&
          &**2 - 2*tt**3*s35 + 3*tt**2*s25*s35 - 2*tt*s15*s25*s35 - 2*tt*s25**2*s35 - ss**2&
          &*tt*(-8*tt + 3*s15 + 8*s25 + s35) - 4*me2**2*(6*tt**2 + 4*s15**2 - 4*ss*(tt + s1&
          &5) + 2*tt*s25 + s15*(8*tt + 2*s25 - s35) - 8*tt*s35 + s35**2) + ss*(6*tt**3 + 4*&
          &tt*s25**2 + 2*s15*(-tt**2 + tt*s25) - 3*tt**2*s35 + 3*tt*s25*(-3*tt + s35)) + me&
          &2*(4*s15**3 - 8*ss**2*(3*tt + s15) + s15**2*(6*tt + 4*s25 - 8*s35) + s15*(4*tt**&
          &2 + 6*tt*s35 - 8*s25*s35 + 4*s35**2) - 2*(2*tt*s25**2 + s25*s35*(-2*tt + s35)) +&
          & 2*ss*(2*s15**2 + 2*tt*(-5*tt + 7*s25) - 2*tt*s35 + s35**2 + s15*(4*tt + 4*s25 +&
          & s35)))))/((tt + s15 - s35)*(s15 + s25 - s35)) - (tt*s15*(-32*me2**3*tt - 4*ss**&
          &3*tt + tt**3*s15 - tt**3*s25 - 2*tt**2*s15*s25 + 2*tt*s15**2*s25 + 2*tt*s15*s25*&
          &*2 + 2*tt**2*s15*s35 + tt**2*s25*s35 + ss**2*tt*(-4*tt + 7*s15 + 8*s25 + s35) + &
          &4*me2**2*(2*tt**2 + 6*s15**2 - 4*ss*(tt + s15) + 6*tt*s25 + s15*(4*tt + 6*s25 - &
          &3*s35) + s35**2) - ss*(2*tt**3 + 2*tt*s15**2 + 8*tt*s15*s25 + 4*tt*s25**2 + tt**&
          &2*s35 + tt*s25*(-5*tt + s35) + 2*tt*s15*(-2*tt + s35)) + 2*me2*(2*tt**3 + 2*s15*&
          &*3 + 4*ss**2*(3*tt + s15) + 6*tt*s25**2 + s15**2*(3*tt + 2*s25) - 2*tt*s25*s35 +&
          & s25*s35**2 + s15*(-3*tt*s35 + 2*s35**2 + 2*s25*(6*tt + s35)) - ss*(-2*tt**2 + 4&
          &*s15**2 + 18*tt*s25 + s35**2 + s15*(14*tt + 4*s25 + 3*s35)))))/(s25*(tt + s15 - &
          &s35)) + (tt*s15*(-2*ss**3*tt - tt**3*s15 - 2*tt**2*s15**2 - tt**3*s25 - 2*tt**2*&
          &s15*s25 + 16*me2**3*(tt + 3*s15 - 3*s35) + ss**2*tt*(-2*tt + 3*s15 + 4*s25 - s35&
          &) + 2*tt**2*s15*s35 + tt**2*s25*s35 - 4*me2**2*(10*ss*tt + tt**2 + 6*ss*s15 + 5*&
          &tt*s15 + 2*s15**2 - 5*tt*s25 - 3*s15*s25 - 3*tt*s35 - 7*s15*s35 + s35**2) + ss*(&
          &-2*tt*s15**2 - 2*tt*s25**2 - tt**2*s35 + tt*s25*(tt + s35) + 2*s15*(tt**2 - 2*tt&
          &*s25 + tt*s35)) + me2*(-2*tt**3 + 4*ss**2*(5*tt + s15) + tt**2*s25 + 6*tt*s25**2&
          & - 9*tt*s25*s35 + 6*tt*s35**2 + 2*s25*s35**2 + 2*s15**2*(5*tt + 2*s35) + ss*(2*s&
          &15**2 + 2*tt*(7*tt - 13*s25) + 5*tt*s35 - 2*s35**2 - s15*(7*tt + 4*s25 + 4*s35))&
          & + s15*(5*tt**2 - 24*tt*s35 - 4*s35**2 + 2*s25*(4*tt + 5*s35)))))/((4*me2 - ss -&
          & tt + s25)*(s15 + s25 - s35)) - (tt*s15*(-2*tt*(ss - s15)*(ss - s25)*(ss - s15 -&
          & s25) + 16*me2**3*(tt + 3*s15 - 3*s35) - 4*me2**2*(10*ss*tt - tt**2 + 6*ss*s15 -&
          & 11*tt*s25 - 5*s15*s25 - 8*tt*s35 - 6*s15*s35 + 2*s35**2) + me2*(4*s15**3 + 4*ss&
          &**2*(5*tt + s15) - 5*tt**2*s25 + 10*tt*s25**2 + 4*s15**2*(3*tt + s25) - 2*tt**2*&
          &s35 - 3*tt*s25*s35 + 6*tt*s35**2 + s15*(5*tt**2 + 16*tt*s25 - 12*tt*s35 + 6*s25*&
          &s35) - ss*(2*s15**2 + tt*(2*tt + 30*s25 + s35) + s15*(21*tt + 4*s25 + 6*s35)))))&
          &/(s25*(4*me2 - ss - tt + s35)))*pvc7(1))/(tt**2*s15**2) + (64*Pi**2*(2*(64*me2**&
          &4 - 16*me2**3*(4*ss + 2*tt - 3*s15 - 4*s25) + 4*me2**2*(4*ss**2 + 5*s15**2 + ss*&
          &(4*tt - 6*s15 - 8*s25) + (tt - 2*s25)**2 + s15*(-tt + 10*s25 - 3*s35)) + tt*s15*&
          &(2*s15**2 + tt*s25 + s15*(tt + 2*s25 - 2*s35) - tt*s35 - s25*s35 + ss*(-s15 + s3&
          &5)) - 2*me2*(tt**3 - 2*s15**3 + ss*s15*(2*tt + 3*s15 + 4*s25 - 3*s35) - 2*s15**2&
          &*(tt + 3*s25 - s35) + s15*(tt**2 - tt*s25 - 4*s25**2 + 3*s25*s35))) - (2*s15*(64&
          &*me2**4 - 16*me2**3*(4*ss + 4*tt - 2*s15 - 2*s25 - s35) + 4*me2**2*(4*ss**2 + 5*&
          &tt**2 + 2*s15**2 - 6*tt*s25 - 3*tt*s35 + 4*s25*s35 - s35**2 + s15*(-2*tt + 2*s25&
          & + s35) - 2*ss*(-6*tt + 2*s15 + 2*s25 + s35)) + tt*(tt**2*(tt - s35) + ss*s15*(-&
          &s15 + s35) + s15**2*(-tt + 2*s35) + s15*(tt**2 - tt*s25 + 2*tt*s35 + s25*s35 - 2&
          &*s35**2)) - 2*me2*(4*ss**2*tt + 2*tt**3 - 2*tt**2*s25 + 2*tt*s25**2 + s15**2*(tt&
          & - 2*s35) - tt**2*s35 + tt*s25*s35 - 2*s25**2*s35 + s25*s35**2 - 2*s15*(2*s25 - &
          &s35)*(-tt + s35) + ss*(4*tt**2 + 2*s15**2 - 4*tt*s25 + 2*s15*s25 - 2*tt*s35 - s1&
          &5*s35 + 2*s25*s35 - s35**2))))/s35 + (tt*s15*(-192*me2**4 + 16*me2**3*(8*ss + 10&
          &*tt - 6*s15 - 6*s25 - 3*s35) - 4*me2**2*(4*ss**2 + 7*tt**2 + 6*s15**2 - 14*tt*s2&
          &5 + s15*(-14*tt + 6*s25 - s35) - 7*tt*s35 + 8*s25*s35 + s35**2 - 2*ss*(-10*tt + &
          &2*s15 + 2*s25 + 3*s35)) + 2*me2*(4*ss**2*tt + s15**2*(3*tt - 2*s35) + (-tt + s35&
          &)*(tt**2 + 4*tt*s25 - 2*s25**2 + tt*s35 - s25*s35) - s15*(6*tt**2 - 6*tt*s25 + t&
          &t*s35 + 4*s25*s35 - 2*s35**2) + ss*(2*tt**2 + 2*s15**2 - 4*tt*s25 + s15*(-4*tt +&
          & 2*s25 - 3*s35) - 4*tt*s35 + 2*s25*s35 + s35**2)) + tt*(s15**2*(tt - 2*s35) + s2&
          &5*s35*(-tt + s35) + ss*(2*tt**2 + 2*tt*s15 + s15**2 - s35**2) + s15*(tt*s25 - 3*&
          &tt*s35 - s25*s35 + 2*s35**2))))/(s35*(4*me2 - ss - tt + s35)) - (2*tt*(-96*me2**&
          &4 + 8*me2**3*(8*ss + 4*tt - 5*s15 - 8*s25) - tt*s15*(-ss + s15 + s25)*(s15 - s35&
          &) + 2*me2**2*(-4*ss**2 + tt**2 - 3*s15**2 + 4*tt*s25 - 4*s25**2 + 2*ss*(-2*tt + &
          &s15 + 4*s25) + s15*(tt - 10*s25 + 5*s35)) + me2*(-2*s15**3 + 2*tt**2*s25 + ss*(-&
          &2*tt**2 + s15**2 + s15*(4*tt + 4*s25 - s35)) + s15**2*(-5*tt - 6*s25 + 2*s35) + &
          &s15*(-4*s25**2 + s25*(-3*tt + s35) + tt*(2*tt + s35)))))/(4*me2 - ss - tt + s25)&
          & + (tt*s15*(192*me2**4 - 8*me2**3*(28*ss + 20*tt - 11*s15 - 18*s25 - 7*s35) + 4*&
          &me2**2*(20*ss**2 + 7*tt**2 + s15**2 - 21*tt*s25 + 6*s25**2 - 8*tt*s35 + 12*s25*s&
          &35 + s35**2 + 2*s15*(-8*tt + 5*s25 + 2*s35) - 2*ss*(-14*tt + 7*s15 + 13*s25 + 5*&
          &s35)) + me2*(-8*ss**3 + 2*tt**3 + 11*tt**2*s25 - 10*tt*s25**2 - 21*tt*s25*s35 + &
          &10*s25**2*s35 + 2*tt*s35**2 + 2*s25*s35**2 + 2*ss**2*(-8*tt + 3*s15 + 8*s25 + 3*&
          &s35) + s15**2*(-2*tt + 4*s35) + s15*(11*tt**2 - 16*tt*s25 - 12*tt*s35 + 20*s25*s&
          &35 - 4*s35**2) + ss*(-2*tt**2 + 4*s15**2 + 26*tt*s25 - 8*s25**2 + s15*(17*tt - 1&
          &0*s25 - 6*s35) + 9*tt*s35 - 16*s25*s35 - 2*s35**2)) + tt*(2*ss**2*(-tt + s15) + &
          &s25*(tt - s35)*s35 + 2*s15**2*(-tt + s35) + s15*((3*tt - 2*s35)*s35 + 2*s25*(-tt&
          & + s35)) + ss*(-2*s15**2 + 2*tt*(-tt + s25) + s35**2 - s15*(-2*tt + 2*s25 + s35)&
          &))))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) - (tt*s15*(-128*me2**4 + 32*me2&
          &**3*(6*ss + 4*tt - 2*s15 - 3*s25 - s35) - 4*me2**2*(24*ss**2 + 10*tt**2 + 2*s15*&
          &*2 - 16*tt*s25 + 4*s25**2 - 11*tt*s35 + 10*s25*s35 + s35**2 + s15*(-5*tt + 4*s25&
          & + s35) - 4*ss*(-8*tt + 4*s15 + 7*s25 + 2*s35)) + tt*(-4*ss**2*s15 + 2*s15**3 + &
          &2*s15**2*(2*tt + s25 - 2*s35) + (-tt + s35)*(2*tt**2 - 2*tt*s25 + s25*s35) + s15&
          &*(-2*tt**2 + 6*tt*s25 - 3*tt*s35 - 4*s25*s35 + 2*s35**2) + ss*(-2*tt**2 + 2*s15*&
          &*2 + 2*tt*s35 - s35**2 + s15*(-6*tt + 4*s25 + 3*s35))) + 2*me2*(8*ss**3 + 4*tt**&
          &3 + 2*s15**3 - 10*tt**2*s25 + 6*tt*s25**2 + s15**2*(tt + 6*s25 - 4*s35) - 4*tt**&
          &2*s35 + 11*tt*s25*s35 - 6*s25**2*s35 + tt*s35**2 - s25*s35**2 - 4*ss**2*(-4*tt +&
          & 2*s15 + 4*s25 + s35) + s15*(tt**2 + 4*s25**2 + 2*s25*(tt - 5*s35) + 2*tt*s35 + &
          &2*s35**2) + ss*(10*tt**2 + 8*s25**2 - 10*tt*s35 + s35**2 + 10*s25*(-2*tt + s35) &
          &+ s15*(-4*tt + 6*s25 + 3*s35)))))/((tt + s15 - s35)*(s15 + s25 - s35)) - (tt*s15&
          &*(128*me2**4 - 32*me2**3*(6*ss + 2*tt - 5*s15 - 5*s25) + 4*me2**2*(24*ss**2 + 2*&
          &tt**2 + 14*s15**2 - 12*tt*s25 + 12*s25**2 + s15*(-9*tt + 28*s25 - s35) - 3*tt*s3&
          &5 + 2*s25*s35 + 3*s35**2 - 4*ss*(-4*tt + 9*s15 + 9*s25 + s35)) + tt*(4*ss**2*s15&
          & + 2*s15**3 + 2*s15**2*(-tt + s25) - s25*(4*tt**2 - 5*tt*s35 + s35**2) + ss*(2*t&
          &t**2 - 4*s15**2 + s15*(4*tt - 4*s25 - 5*s35) - 4*tt*s35 + s35**2) + s15*(-2*tt**&
          &2 - 4*tt*s25 + tt*s35 + 2*s25*s35 + 2*s35**2)) - 2*me2*(8*ss**3 + 2*tt**3 - 2*s1&
          &5**3 - 4*tt**2*s25 + 2*tt*s25**2 - 3*s15**2*(tt + 2*s25) - 4*tt**2*s35 + 7*tt*s2&
          &5*s35 - 2*s25**2*s35 + tt*s35**2 - 3*s25*s35**2 - 4*ss**2*(-2*tt + 4*s15 + 4*s25&
          & + s35) + s15*(tt**2 - 4*s25**2 + 4*tt*s35 - 2*s35**2 - 2*s25*(tt + s35)) + ss*(&
          &2*tt**2 + 8*s15**2 + 8*s25**2 - 4*tt*s35 + 3*s35**2 + 6*s25*(-2*tt + s35) + s15*&
          &(-6*tt + 18*s25 + 5*s35)))))/(s25*(tt + s15 - s35)) - (tt*s15*(192*me2**4 + 2*tt&
          &*(ss - s15)*(-tt + s15)*(ss - s15 - s25) - 8*me2**3*(28*ss + 8*tt - 21*s15 - 22*&
          &s25 - s35) + 4*me2**2*(20*ss**2 - tt**2 + 7*s15**2 - 11*tt*s25 + 10*s25**2 + 2*t&
          &t*s35 + 4*s25*s35 - 2*ss*(-6*tt + 14*s15 + 15*s25 + 2*s35) + s15*(-8*tt + 22*s25&
          & + 5*s35)) - me2*(8*ss**3 - 4*s15**3 + 3*tt**2*s25 + 6*tt*s25**2 - 4*s15**2*(2*t&
          &t + 3*s25) + 2*tt**2*s35 + 9*tt*s25*s35 - 6*s25**2*s35 - 4*tt*s35**2 - 2*ss**2*(&
          &-4*tt + 7*s15 + 8*s25 + 3*s35) + s15*(-8*s25**2 + 4*s25*(tt - 3*s35) + tt*(7*tt &
          &+ 10*s35)) + ss*(4*s15**2 + 8*s25**2 + tt*(-6*tt + s35) + 6*s25*(-3*tt + 2*s35) &
          &+ s15*(-7*tt + 22*s25 + 12*s35)))))/(s25*(4*me2 - ss - tt + s35)))*pvc7(2))/(tt*&
          &*2*s15**2) + (64*Pi**2*(4*(16*me2**3*s15 + tt*s15*(tt + s15)*(s15 - s35) - 4*me2&
          &**2*s15*(2*ss + tt - 2*s15 - 2*s25 + s35) + 2*me2*(-tt**3 + s15**3 + s15*s25*(tt&
          & - s35) + s15**2*(tt + 2*s25 - s35) + ss*s15*(-s15 + s35))) + (2*tt*(48*me2**3*s&
          &15 + 4*me2**2*s15*(-2*ss - 5*tt + 2*s15 + 2*s25 - 3*s35) + tt*s15*(tt + s15)*(s1&
          &5 - s35) + 2*me2*(-tt**3 + s15**3 + s15*s25*(tt - s35) + s15**2*(tt + 2*s25 - s3&
          &5) + tt*s15*(tt + s35) + ss*s15*(-s15 + s35))))/(4*me2 - ss - tt + s25) - (tt*s1&
          &5*(64*me2**3*s15 - 8*me2**2*(8*ss*s15 - 6*s15**2 + s15*(tt - 6*s25) - tt*s35) + &
          &tt*(2*s15**3 + 2*ss**2*(s15 - s35) - s25*(-tt + s35)*(-5*tt + 2*s25 + s35) + s15&
          &*(-tt + s35)*(3*tt + 2*s25 + 2*s35) + ss*(4*tt**2 - 2*s15**2 - 2*tt*s25 + s15*(3&
          &*tt - 2*s25 - 3*s35) - 5*tt*s35 + 4*s25*s35 + s35**2)) + 2*me2*(8*ss**2*s15 + 2*&
          &s15**3 + 4*s15**2*s25 - tt*(-2*tt + s35)**2 - 2*ss*s15*(-2*tt + 3*s15 + 4*s25 + &
          &3*s35) + s15*(-4*tt**2 - 3*tt*s35 + 2*s25*s35 + 2*s35**2))))/(s25*(tt + s15 - s3&
          &5)) + (tt*s15*(96*me2**3*s15 - 4*me2**2*(16*ss*s15 - s15**2 + s15*(11*tt - 6*s25&
          & - 5*s35) + 5*tt*s35) + 2*me2*(4*ss**2*s15 + s15**2*(-tt + 2*s35) + ss*(2*s15**2&
          & + s15*(5*tt - 4*s25 - 4*s35) + 5*tt*s35) + s15*(tt**2 - 4*tt*s25 - 8*tt*s35 + 8&
          &*s25*s35 - 2*s35**2) + tt*(-2*tt**2 + tt*s25 + 2*tt*s35 - 9*s25*s35 + 3*s35**2))&
          & + tt*(2*ss**2*(s15 - s35) + 2*s15**2*(-tt + s35) + s15*(tt**2 + 3*tt*s35 - 2*s3&
          &5**2) - ss*(-2*tt**2 + 2*s15**2 + 2*tt*s25 + s15*(-3*tt + 2*s25 - s35) + tt*s35 &
          &- 4*s25*s35 + s35**2) + (-tt + s35)*(-2*tt**2 - 2*s25**2 + s25*(tt + s35)))))/((&
          &4*me2 - ss - tt + s25)*(s15 + s25 - s35)) - (2*s15*(32*me2**3*s15 - 8*me2**2*(2*&
          &ss*s15 - s15**2 + tt*s35) - 2*me2*(s15**2*(tt - 2*s35) + 2*ss*(tt + s15)*(s15 - &
          &s35) + tt*(2*tt**2 - tt*s35 + 2*s25*s35 - s35**2) + s15*(tt**2 + 2*tt*s25 - 2*tt&
          &*s35 - 2*s25*s35 + 2*s35**2)) + tt*(-(ss*(s15 - s35)**2) + s15**2*(-tt + 2*s35) &
          &+ (-tt + s35)*(-2*tt**2 + s25*s35) - s15*(-2*tt**2 - 3*tt*s35 + 2*s35**2 + s25*(&
          &tt + s35)))))/s35 - (tt*s15*(-64*me2**3*s15 + 8*me2**2*(8*ss*s15 - 2*s15**2 + s1&
          &5*(tt - 2*s25) + 3*tt*s35) + tt*(2*s15**3 - 2*ss**2*(s15 - s35) - 4*s15**2*(-tt &
          &+ s35) + ss*(-4*tt**2 + 4*s15**2 + 2*tt*s25 + s15*(-3*tt + 2*s25 - s35) + tt*s35&
          & - 4*s25*s35 + s35**2) + s15*(-3*tt**2 - 3*tt*s35 + 2*s35**2) - (-tt + s35)*(-4*&
          &tt**2 - 2*s25**2 + s25*(tt + s35))) - 2*me2*(8*ss**2*s15 - 2*s15**3 - 2*s15**2*(&
          &tt + 2*s25 - 2*s35) + s15*(-2*tt**2 - 4*tt*s25 - 3*tt*s35 + 6*s25*s35 - 2*s35**2&
          &) + tt*(-4*tt**2 + 4*tt*s25 + 2*tt*s35 - 8*s25*s35 + s35**2) - 2*ss*(s15**2 - 4*&
          &tt*s35 + s15*(-2*tt + 4*s25 + s35)))))/((tt + s15 - s35)*(s15 + s25 - s35)) - (2&
          &*tt*s15*(48*me2**3*s15 - tt*(ss - s15)*(-tt**2 + s15**2) - 2*me2**2*(16*ss*s15 -&
          & 5*s15**2 + s15*(9*tt - 10*s25 - 5*s35) - 5*tt*s35) + me2*(4*ss**2*s15 + 2*s15**&
          &3 + s15**2*(tt + 4*s25) + s15*(tt**2 - 4*tt*s25 - 5*tt*s35 + 6*s25*s35) - tt*(2*&
          &tt**2 + 3*tt*s25 + 2*tt*s35 + s25*s35 - 2*s35**2) - ss*(3*tt*s35 + s15*(-9*tt + &
          &4*s25 + 6*s35)))))/(s25*(4*me2 - ss - tt + s35)) + (tt*s15*(-96*me2**3*s15 + 8*m&
          &e2**2*(2*ss*s15 + 4*tt*s15 - 3*s15**2 + tt*s35) + tt*(s15**2*(tt - 2*s35) + ss*(&
          &s15 - s35)**2 - (-tt + s35)*(-2*tt**2 + s25*s35) + s15*(-2*tt**2 - 3*tt*s35 + 2*&
          &s35**2 + s25*(tt + s35))) + me2*(4*ss*(tt + s15)*(s15 - s35) - 2*(s15**2*(-3*tt &
          &+ 2*s35) + s15*(tt**2 - 2*tt*s25 + 2*tt*s35 + 2*s25*s35 - 2*s35**2) + tt*(-2*tt*&
          &*2 + tt*s35 - 2*s25*s35 + s35**2)))))/(s35*(4*me2 - ss - tt + s35)))*pvc7(3))/(t&
          &t**2*s15**2) + (128*Pi**2*((-4*(-4*me2**2 + 2*me2*(tt + s15) + s15*s35))/tt - (2&
          &*(-4*me2**2 + 2*me2*(tt + s15) + s15*s35))/(4*me2 - ss - tt + s25) - (4*s15*(4*m&
          &e2**2 + tt*(tt + s15 - s35) + me2*(-4*tt - s15 + s35)))/(tt*s35) - (s15*(-2*tt**&
          &3 + tt**2*s15 + 2*tt*s15**2 + 3*tt**2*s25 + 2*tt*s15*s25 + 4*me2**2*(-2*tt + 3*s&
          &15 - 3*s35) + 2*tt**2*s35 - 3*tt*s15*s35 - 4*tt*s25*s35 + s25*s35**2 + ss*(2*tt &
          &+ s15 - s35)*(-tt + s35) - 2*me2*(2*s15**2 - 2*tt*(ss + 2*tt - 2*s25) + s15*(5*t&
          &t - 3*s35) + tt*s35 - 2*s35**2)))/(tt*(4*me2 - ss - tt + s25)*(s15 + s25 - s35))&
          & - (s15*(8*me2**2*tt + 2*tt**3 - tt*s15**2 - 2*tt**2*s25 - tt*s15*s25 - 2*tt**2*&
          &s35 + 3*tt*s15*s35 + 3*tt*s25*s35 - s15*s25*s35 - s25*s35**2 - ss*(2*tt + s15 - &
          &s35)*(s15 + s35) + 2*me2*(3*tt*s15 + s15**2 + 4*tt*(-tt + s25) + tt*s35 - s35**2&
          &)))/(tt*s35*(4*me2 - ss - tt + s35)) + (s15*(-16*me2**2*tt + 3*tt**2*s15 + 2*tt*&
          &s15**2 + 3*tt**2*s25 + 2*tt*s15*s25 - tt*s15*s35 - 4*tt*s25*s35 + s25*s35**2 + 2&
          &*me2*(4*tt*(ss + tt - s25) - 2*tt*s35 - s15*s35 + s35**2) + ss*(-4*tt**2 + 3*tt*&
          &s35 - s35**2 + s15*(-3*tt + s35))))/(tt*s25*(tt + s15 - s35)) + (s15*(16*me2**2*&
          &tt + 4*tt**3 + 3*tt**2*s15 - 3*tt**2*s25 - 2*tt*s15*s25 - 4*tt**2*s35 + tt*s15*s&
          &35 + 4*tt*s25*s35 - s25*s35**2 + ss*(4*tt**2 + 3*tt*s15 - 3*tt*s35 - s15*s35 + s&
          &35**2) - 2*me2*(4*tt*(ss + 2*tt - s25) - 4*tt*s35 + s35**2 - s15*(-2*tt + s35)))&
          &)/(tt*(tt + s15 - s35)*(s15 + s25 - s35)) + (2*s15*(-(tt*(ss - s15)*(tt + s15)) &
          &+ me2**2*(-4*tt + 6*s15 - 6*s35) + me2*(2*tt*(ss + tt) - 2*s15**2 + tt*s35 + s35&
          &**2 + s15*(-3*tt + 4*s35))))/(tt*s25*(4*me2 - ss - tt + s35)))*pvc7(4))/s15**2 +&
          & (64*Pi**2*((4*(2*me2 - ss + s15 + s25)*(4*me2**2 - 2*me2*(ss + tt - s15 - s25) &
          &+ s15*(s15 + s25 - s35)))/tt + (2*(2*me2 - ss + s15 + s25)*(4*me2**2 - 2*me2*(ss&
          & + tt - s15 - s25) + s15*(s15 + s25 - s35)))/(4*me2 - ss - tt + s25) - (2*s15*(1&
          &6*me2**3 - 2*ss**2*tt + tt**2*s15 - tt*s15**2 + tt**2*s25 - 2*tt*s15*s25 - tt*s2&
          &5**2 + tt*s15*s35 + 2*s15**2*s35 + 3*s15*s25*s35 + s25**2*s35 - 2*s15*s35**2 - s&
          &25*s35**2 + 8*me2**2*(-2*ss - 2*tt + s15 + s25 + s35) + ss*(-2*tt**2 - s15**2 + &
          &s15*(tt - s25) + 2*tt*s25 + tt*s35 - s25*s35 + s35**2) + 2*me2*(2*ss**2 + 2*tt**&
          &2 + s15**2 - 3*tt*s25 - 2*tt*s35 + 3*s25*s35 - s35**2 - 2*ss*(-3*tt + s15 + s25 &
          &+ s35) + s15*(-2*tt + s25 + 2*s35))))/(tt*s35) - (s15*(16*me2**3 - 2*ss**2*tt + &
          &tt**2*s15 - tt*s15**2 + tt**2*s25 - 2*tt*s15*s25 - tt*s25**2 + tt*s15*s35 + 2*s1&
          &5**2*s35 + 3*s15*s25*s35 + s25**2*s35 - 2*s15*s35**2 - s25*s35**2 + 8*me2**2*(-2&
          &*ss - 2*tt + s15 + s25 + s35) + ss*(-2*tt**2 - s15**2 + s15*(tt - s25) + 2*tt*s2&
          &5 + tt*s35 - s25*s35 + s35**2) + 2*me2*(2*ss**2 + 2*tt**2 + s15**2 - 3*tt*s25 - &
          &2*tt*s35 + 3*s25*s35 - s35**2 - 2*ss*(-3*tt + s15 + s25 + s35) + s15*(-2*tt + s2&
          &5 + 2*s35))))/(s35*(4*me2 - ss - tt + s35)) - (s15*(32*me2**3 - 4*ss**3 + tt**2*&
          &s15 - 2*tt*s15**2 + 2*s15**3 + tt**2*s25 - 4*tt*s15*s25 + 4*s15**2*s25 - 2*tt*s2&
          &5**2 + 2*s15*s25**2 - 3*tt*s15*s35 - 2*tt*s25*s35 + 2*s15*s25*s35 + 2*s25**2*s35&
          & + 2*s15*s35**2 + s25*s35**2 + 4*me2**2*(-12*ss - 4*tt + 11*s15 + 10*s25 + s35) &
          &+ ss**2*(-4*tt + 9*s15 + 8*s25 + 3*s35) - ss*(-8*tt*s15 + 6*s15**2 - 7*tt*s25 + &
          &10*s15*s25 + 4*s25**2 - tt*s35 + 5*s15*s35 + 5*s25*s35 + s35**2) + 2*me2*(12*ss*&
          &*2 - 9*tt*s15 + 8*s15**2 - 8*tt*s25 + 14*s15*s25 + 6*s25**2 + 3*s15*s35 + 3*s25*&
          &s35 + s35**2 - 2*ss*(-4*tt + 10*s15 + 9*s25 + 2*s35))))/(s25*(tt + s15 - s35)) -&
          & (s15*(-32*me2**3 + 4*ss**3 - 3*tt**2*s15 + 2*tt*s15**2 + 2*s15**3 - 3*tt**2*s25&
          & + 6*tt*s15*s25 + 4*s15**2*s25 + 4*tt*s25**2 + 2*s15*s25**2 + 4*me2**2*(12*ss + &
          &8*tt - 3*s15 - 6*s25 - 5*s35) + tt*s15*s35 - 4*s15**2*s35 + 2*tt*s25*s35 - 8*s15&
          &*s25*s35 - 4*s25**2*s35 + 2*s15*s35**2 + s25*s35**2 - ss**2*(-8*tt + 5*s15 + 8*s&
          &25 + 3*s35) + ss*(4*tt**2 - 8*tt*s15 - 11*tt*s25 + 4*s15*s25 + 4*s25**2 - 3*tt*s&
          &35 + 5*s15*s35 + 7*s25*s35 - s35**2) - 2*me2*(12*ss**2 + 4*tt**2 - 7*tt*s15 - 2*&
          &s15**2 - 10*tt*s25 + 2*s25**2 - 4*tt*s35 + 7*s15*s35 + 9*s25*s35 - s35**2 - 2*ss&
          &*(-8*tt + 4*s15 + 7*s25 + 4*s35))))/((tt + s15 - s35)*(s15 + s25 - s35)) + (s15*&
          &(-8*me2**3*(-2*tt + 3*s15 - 3*s35) - tt*(ss + tt - s35)*(2*ss**2 + 2*s15**2 + s1&
          &5*(-tt + 4*s25 - 2*s35) + s25*(-tt + 2*s25 - s35) + ss*(2*tt - 3*s15 - 4*s25 + s&
          &35)) + 4*me2**2*(-4*tt**2 - 3*s15**2 + 6*ss*(-tt + s15 - s35) + tt*s35 + 6*s25*s&
          &35 + 3*s15*(tt - s25 + s35)) - 2*me2*(-2*tt**3 - 2*tt*s15**2 + 3*tt**2*s25 + 3*s&
          &s**2*(-2*tt + s15 - s35) + 2*tt**2*s35 - 3*s25**2*s35 + s15*(3*tt**2 - 2*tt*s25 &
          &- 3*s25*s35) + ss*(-8*tt**2 - 3*s15**2 + 4*tt*s25 + 2*tt*s35 + 6*s25*s35 + s15*(&
          &6*tt - 3*s25 + 3*s35)))))/(tt*(4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + (2*s1&
          &5*(tt*(ss - s15)*(ss - s15 - s25)*(ss + tt - s15 - s25) + 4*me2**3*(-2*tt + 3*s1&
          &5 - 3*s35) + 2*me2**2*(3*s15**2 + s15*(-8*tt + 3*s25 - 3*s35) + 2*tt*(tt + s35) &
          &+ 6*ss*(tt - s15 + s35) - 2*s25*(tt + 3*s35)) + me2*(4*tt**2*s15 - 8*tt*s15**2 +&
          & tt**2*s25 - 10*tt*s15*s25 - 2*tt*s25**2 + 3*ss**2*(-2*tt + s15 - s35) + 3*tt*s1&
          &5*s35 + 5*tt*s25*s35 - 3*s15*s25*s35 - 3*s25**2*s35 - tt*s35**2 + ss*(-3*s15**2 &
          &+ 6*s25*(tt + s35) - 2*tt*(2*tt + s35) + s15*(14*tt - 3*s25 + 3*s35)))))/(tt*s25&
          &*(4*me2 - ss - tt + s35)))*pvc7(5))/s15**2 + (64*Pi**2*(-((s15*(-4*tt**3 - 5*tt*&
          &*2*s15 + 6*tt*s15**2 + 4*s15**3 + tt**2*s25 + 10*tt*s15*s25 + 4*s15**2*s25 + 2*t&
          &t*s25**2 + 4*tt**2*s35 - tt*s15*s35 - 8*s15**2*s35 - 2*tt*s25*s35 - 10*s15*s25*s&
          &35 - 2*s25**2*s35 + 4*s15*s35**2 + s25*s35**2 - 16*me2**2*(s15 + s35) - 2*ss**2*&
          &(3*s15 + s35) + ss*(-4*tt**2 - 11*tt*s15 + 2*s15**2 - 2*tt*s25 + 6*s15*s25 + tt*&
          &s35 + 7*s15*s35 + 4*s25*s35 - s35**2) + 2*me2*(4*tt**2 + 10*ss*s15 + 10*tt*s15 +&
          & 2*s15**2 + 2*tt*s25 - 2*s15*s25 + 6*ss*s35 - 11*s15*s35 - 6*s25*s35 + s35**2)))&
          &/((tt + s15 - s35)*(s15 + s25 - s35))) - (2*s15*(2*tt**3 + 2*tt**2*s15 - tt*s15*&
          &*2 - tt*s15*s25 - 2*tt**2*s35 + 3*tt*s15*s35 + 4*s15**2*s35 + tt*s25*s35 + 3*s15&
          &*s25*s35 - 4*s15*s35**2 - s25*s35**2 + 8*me2**2*(s15 + s35) + ss*(-s15**2 + s35*&
          &*2) - 2*me2*(2*tt**2 + tt*s15 - s15**2 + tt*s35 - 4*s15*s35 - 2*s25*s35 + s35**2&
          & + 2*ss*(s15 + s35))))/(tt*s35) - (s15*(2*tt**3 + 2*tt**2*s15 - tt*s15**2 - tt*s&
          &15*s25 - 2*tt**2*s35 + 3*tt*s15*s35 + 4*s15**2*s35 + tt*s25*s35 + 3*s15*s25*s35 &
          &- 4*s15*s35**2 - s25*s35**2 + 8*me2**2*(s15 + s35) + ss*(-s15**2 + s35**2) - 2*m&
          &e2*(2*tt**2 + tt*s15 - s15**2 + tt*s35 - 4*s15*s35 - 2*s25*s35 + s35**2 + 2*ss*(&
          &s15 + s35))))/(s35*(4*me2 - ss - tt + s35)) - (s15*(32*me2**2*s15 - tt**2*s15 - &
          &2*tt*s15**2 + 4*s15**3 - tt**2*s25 - 4*tt*s15*s25 + 4*s15**2*s25 - 2*tt*s25**2 -&
          & 5*tt*s15*s35 + 4*s15*s25*s35 + 2*s25**2*s35 + 4*s15*s35**2 + s25*s35**2 + 2*ss*&
          &*2*(3*s15 + s35) - ss*(-4*tt**2 - 7*tt*s15 + 8*s15**2 - 2*tt*s25 + 6*s15*s25 + t&
          &t*s35 + 7*s15*s35 + 4*s25*s35 + s35**2) + 2*me2*(-4*tt**2 - 8*tt*s15 + 12*s15**2&
          & - 2*tt*s25 + 10*s15*s25 + 2*tt*s35 + 3*s15*s35 + 2*s25*s35 + s35**2 - 2*ss*(7*s&
          &15 + s35))))/(s25*(tt + s15 - s35)) + (4*(8*me2**2*s15 + s15*(2*s15**2 + tt*s25 &
          &+ s15*(tt + 2*s25 - 2*s35) - tt*s35 - s25*s35 + ss*(-s15 + s35)) - 2*me2*(tt**2 &
          &+ 2*ss*s15 - 3*s15**2 + s15*(tt - 2*s25 + s35))))/tt + (2*(8*me2**2*s15 + s15*(2&
          &*s15**2 + tt*s25 + s15*(tt + 2*s25 - 2*s35) - tt*s35 - s25*s35 + ss*(-s15 + s35)&
          &) - 2*me2*(tt**2 + 2*ss*s15 - 3*s15**2 + s15*(tt - 2*s25 + s35))))/(4*me2 - ss -&
          & tt + s25) - (2*s15*(tt*(ss - s15)*(tt**2 + 2*ss*s15 - 2*s15**2 + s15*(tt - 2*s2&
          &5)) + 2*me2**2*s15*(4*tt - 3*s15 + 3*s35) + me2*(11*tt*s15**2 + ss*s15*(-8*tt + &
          &3*s15 - 3*s35) + s15*s25*(5*tt + 3*s35) - tt*s15*(tt + 4*s35) + tt*(-2*tt**2 - t&
          &t*s35 - 2*s25*s35 + s35**2))))/(tt*s25*(4*me2 - ss - tt + s35)) - (s15*(4*me2**2&
          &*(3*s15**2 - 4*tt*s35 - 3*s15*s35) + 2*me2*(-(tt*s15**2) + s15*s25*(tt - 3*s35) &
          &+ 2*tt*s25*(tt - 2*s35) + 3*tt*s15*(tt - s35) + tt**2*(2*tt + s35) + ss*(2*tt*s1&
          &5 - 3*s15**2 + 6*tt*s35 + 3*s15*s35)) + tt*(-2*ss**2*(s15 + s35) + ss*(-2*tt**2 &
          &+ 2*s15**2 - 2*tt*s25 + tt*s35 + 4*s25*s35 - s35**2 + s15*(-5*tt + 2*s25 + 3*s35&
          &)) + (-tt + s35)*(2*tt**2 - 4*s15**2 - tt*s25 - 2*s25**2 + s25*s35 + s15*(tt - 6&
          &*s25 + 4*s35)))))/(tt*(4*me2 - ss - tt + s25)*(s15 + s25 - s35)))*pvc7(6))/s15**&
          &2 + (128*Pi**2*((2*(tt + s15)*(2*me2*(-tt + s15) + s15*(s15 - s35)))/tt + ((tt +&
          & s15)*(2*me2*(-tt + s15) + s15*(s15 - s35)))/(4*me2 - ss - tt + s25) - (s15*(tt &
          &+ s15)*(-((ss - s15)*(-tt + s15)) + me2*(-2*tt + 3*s15 - s35)))/(s25*(4*me2 - ss&
          & - tt + s35)) - (s15*(me2*(s15**2 + s15*(tt - 3*s35) + tt*(2*tt - s35)) + (-tt +&
          & s35)*(tt**2 - s15**2 + ss*(tt + s15) - tt*s25 - s15*s25 + s15*s35)))/((4*me2 - &
          &ss - tt + s25)*(s15 + s25 - s35)) - (2*s15*(tt**2*(tt - s35) + s15**2*s35 + 2*me&
          &2*(-tt**2 + s15*s35) + s15*(tt**2 + tt*s35 - s35**2)))/(tt*s35) - (s15*(tt**2*(t&
          &t - s35) + s15**2*s35 + 2*me2*(-tt**2 + s15*s35) + s15*(tt**2 + tt*s35 - s35**2)&
          &))/(s35*(4*me2 - ss - tt + s35)) - (s15*(-(tt**2*s15) + s15**3 - tt**2*s25 - tt*&
          &s15*s25 - tt*s15*s35 + tt*s25*s35 + s15*s25*s35 + s15*s35**2 - ss*(tt + s15)*(-2&
          &*tt + s15 + s35) + 2*me2*(-(tt*s15) + 2*s15**2 + tt*(-2*tt + s35))))/(s25*(tt + &
          &s15 - s35)) - (s15*(s15**3 - 2*s15**2*(-tt + s35) + ss*(tt + s15)*(-2*tt + s15 +&
          & s35) + tt*((-2*tt + s25)*(tt - s35) - 2*me2*(-2*tt + s35)) - s15*(tt**2 - tt*s2&
          &5 + tt*s35 + s25*s35 - s35**2 + me2*(-2*tt + 4*s35))))/((tt + s15 - s35)*(s15 + &
          &s25 - s35)))*pvc7(7))/s15**2)/(16.*Pi**2)

          !print*,"4",diags

  end if

  if (box63) then

  diags =  diags + (128*Pi**2*((2/tt + 1/(4*me2 - ss - tt + s25))/s15 + (-2/tt + 1/(-4*me2 &
          &+ ss + tt - s35))/s35)*pva1(1) + (64*Pi**2*((-4*(4*me2**2 + (ss + tt - s25)*(ss &
          &- s15 - s25) + me2*(-4*ss - 3*tt + s15 + 4*s25 - s35)))/s15 - (2*me2*tt*s15)/(s2&
          &5*(4*me2 - ss - tt + s35)) - (tt*(tt*s15 + tt*s25 - 2*me2*(s15 + 2*s25 - s35) + &
          &ss*(s15 + 2*s25 - s35) - s25*s35))/(s25*(tt + s15 - s35)) - (tt*(tt*s15 + tt*s25&
          & - 2*me2*(s15 + 2*s25 - s35) + ss*(s15 + 2*s25 - s35) - s25*s35))/((tt + s15 - s&
          &35)*(s15 + s25 - s35)) + (2*(8*me2**2 + 2*ss**2 + tt**2 + 2*tt*s15 + tt*s25 + ss&
          &*(2*tt + s15 + 2*s25 - 3*s35) - 2*me2*(4*ss - tt + s15 + 2*s25 - s35) - tt*s35 -&
          & s25*s35))/s35 - (2*tt*(12*me2**2 + (ss + tt - s25)*(ss - s15 - s25) - me2*(8*ss&
          & + 5*tt - 3*s15 - 8*s25 + s35)))/(s15*(4*me2 - ss - tt + s25)) + (tt*(-(tt*s15) &
          &- tt*s25 + me2*(4*s15 + 4*s25 - 2*s35) + s25*s35 + ss*(-s15 - 2*s25 + s35)))/((4&
          &*me2 - ss - tt + s25)*(s15 + s25 - s35)) + (2*tt*(12*me2**2 + ss**2 + tt*s15 + t&
          &t*s25 + ss*(s25 - s35) - s25*s35 + me2*(-8*ss + tt - 2*s15 - 6*s25 + 4*s35)))/(s&
          &35*(4*me2 - ss - tt + s35)))*pvb1(1))/tt**2 + (128*Pi**2*((s25*(tt - s35) - 2*me&
          &2*(s15 + 2*s25 - s35) + ss*(s15 + 2*s25 - s35))/s35 + (tt*(s25*(ss + tt - s35) -&
          & 2*me2*(s15 + 3*s25 - s35)))/(s35*(4*me2 - ss - tt + s35)) + (tt*s15 + tt*s25 - &
          &2*s15*s25 - 2*s25**2 - 2*me2*(s15 + 2*s25 - s35) + ss*(s15 + 2*s25 - s35) + tt*s&
          &35 + s25*s35)/s15 + (tt*(tt*s15 + tt*s25 - s15*s25 - s25**2 + ss*(s15 + s25 - s3&
          &5) + s25*s35 + me2*(-4*s15 - 6*s25 + 4*s35)))/(s15*(4*me2 - ss - tt + s25)))*pvb&
          &1(2))/tt**2 + 128*Pi**2*(-((me2 + ss + tt - s15 - 2*s25)/(s15*(4*me2 - ss - tt +&
          & s25))) + (me2*(6*tt + 5*s15 - 3*s35) - 2*tt*(tt + s15 - s35))/(tt*(4*me2 - ss -&
          & tt + s25)*(s15 + s25 - s35)) + (10*me2 + 5*tt + 6*s15 - 5*s35)/(tt*s35) + (-6*m&
          &e2 - tt + s35)/(tt*s15) - (tt*(-ss + s15 + s25) + me2*(6*tt + 4*s15 - 2*s35))/(t&
          &t*s25*(4*me2 - ss - tt + s35)) + (7*me2 - ss + 2*tt + 3*s15 - 2*s35)/(s35*(4*me2&
          & - ss - tt + s35)) + (me2*(s15 - s35) + tt*(ss - 2*tt - 3*s15 - s25 + 2*s35))/(t&
          &t*s25*(tt + s15 - s35)) + (me2*(4*tt + s15 - s35) + tt*(-ss - 3*tt - 2*s15 + s25&
          & + 2*s35))/(tt*(tt + s15 - s35)*(s15 + s25 - s35)))*pvb12(1) + 128*Pi**2*(-2/s15&
          & - tt/(s15*(4*me2 - ss - tt + s25)) + (6*me2 - ss - 2*tt + 2*s25)/((4*me2 - ss -&
          & tt + s25)*(s15 + s25 - s35)) + (4*me2 - 2*ss - 3*tt + 3*s25)/((tt + s15 - s35)*&
          &(s15 + s25 - s35)) + (2*s15)/(tt*s35) - (6*me2 - ss + s15 + s25)/(s25*(4*me2 - s&
          &s - tt + s35)) + s15/(s35*(4*me2 - ss - tt + s35)) + (-4*me2 + 2*ss - tt - 3*s15&
          & - 3*s25 + s35)/(s25*(tt + s15 - s35)))*pvb12(2) + (64*Pi**2*((-2*tt*(-12*me2**2&
          & + me2*(2*ss - tt + s15 + s25 - 4*s35) + tt*(ss + tt - s35)))/(s25*(4*me2 - ss -&
          & tt + s35)) + (tt*(16*me2**2 - 2*tt**2 + tt*s15 + tt*s25 - 4*me2*(2*ss + tt - s2&
          &5 - 2*s35) + 2*tt*s35 - s25*s35 + ss*(-4*tt - s15 + s35)))/(s25*(tt + s15 - s35)&
          &) + (4*(4*me2**2 + ss**2 + ss*(tt - s35) - 2*tt*(tt + s15 - s35) + me2*(-4*ss - &
          &3*tt + 2*s35)))/s35 + (tt*(-16*me2**2 + 2*tt**2 + tt*s15 - tt*s25 + ss*(4*tt + s&
          &15 - s35) - 2*tt*s35 + s25*s35 + 4*me2*(2*ss - 3*tt - 2*s15 - s25 + 2*s35)))/((t&
          &t + s15 - s35)*(s15 + s25 - s35)) + (tt*(-24*me2**2 + 2*tt**2 + tt*s15 - tt*s25 &
          &+ ss*(2*tt + s15 - s35) - 2*tt*s35 + s25*s35 + 2*me2*(2*ss - 5*tt - 4*s15 - 3*s2&
          &5 + 3*s35)))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) - (2*tt*(12*me2**2 + ss&
          &**2 + tt*s15 - ss*(tt + s15 + s25) + me2*(-8*ss + 3*tt + 3*s15 + 2*s25 + 3*s35))&
          &)/(s15*(4*me2 - ss - tt + s25)) + (2*tt*(12*me2**2 + ss**2 + ss*(tt - s35) - 2*t&
          &t*(tt + s15 - s35) + me2*(-8*ss - 5*tt + 4*s35)))/(s35*(4*me2 - ss - tt + s35)) &
          &- (2*(8*me2**2 + 2*ss**2 + 2*tt**2 + 2*tt*s15 - tt*s25 - 2*tt*s35 + s25*s35 - ss&
          &*(-2*tt + s15 + 2*s25 + s35) + me2*(-8*ss + 2*tt + 4*s15 + 4*s25 + 4*s35)))/s15)&
          &*pvb13(1))/tt**2 + (128*Pi**2*(-((tt*(2*me2*(tt + 2*s15 + 3*s25 - 2*s35) + s25*(&
          &s15 + s25 - s35) - ss*(tt + s15 + s25 - s35)))/(s15*(4*me2 - ss - tt + s25))) - &
          &(tt*(6*me2 - ss - tt + s25)*(tt + s15 + s25 - s35))/((4*me2 - ss - tt + s25)*(s1&
          &5 + s25 - s35)) - (tt*(4*me2 - 2*ss - tt + s25)*(tt + s15 + s25 - s35))/((tt + s&
          &15 - s35)*(s15 + s25 - s35)) + (tt*(-tt + s25)*(-6*me2 + ss + tt - s35))/(s25*(4&
          &*me2 - ss - tt + s35)) - (tt*(-tt + s25)*(4*me2 - 2*ss - tt + s15 + s25 + s35))/&
          &(s25*(tt + s15 - s35)) + (-3*tt**2 - 2*tt*s15 + tt*s25 - 2*me2*(s15 + 2*s25 - s3&
          &5) + ss*(s15 + 2*s25 - s35) + 3*tt*s35 - s25*s35)/s35 - (tt*(2*tt**2 + tt*s15 + &
          &ss*(tt - s25) - tt*s25 + 2*me2*(-tt + s15 + 3*s25 - s35) - 2*tt*s35 + s25*s35))/&
          &(s35*(4*me2 - ss - tt + s35)) + (-tt**2 + tt*s25 - 2*s15*s25 - 2*s25**2 - 2*me2*&
          &(s15 + 2*s25 - s35) + ss*(s15 + 2*s25 - s35) + tt*s35 + s25*s35)/s15)*pvb13(2))/&
          &tt**2 + (64*Pi**2*(-((tt*(8*me2**2 + tt*s15 - tt*s25 - 2*me2*(2*ss + 6*tt + 3*s1&
          &5 - 6*s35) + ss*(s15 - s35) + s25*s35))/(s25*(tt + s15 - s35))) + (tt*(8*me2**2 &
          &- tt*s15 - tt*s25 + me2*(-4*ss + 8*tt + 2*s15 - 4*s35) + ss*(s15 - s35) + s25*s3&
          &5))/((tt + s15 - s35)*(s15 + s25 - s35)) - (tt*(-24*me2**2 - 2*ss**2 + tt*s15 + &
          &tt*s25 + 2*me2*(5*ss - 2*s25 - s35) - s25*s35 + ss*(-2*tt + s15 + 2*s25 + s35)))&
          &/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + (2*tt*(-24*me2**2 - 2*ss**2 + tt*&
          &s15 + tt*s25 + me2*(16*ss + 9*tt - 2*s15 - 6*s25 - 6*s35) - s25*s35 + ss*(-3*tt &
          &+ s25 + 2*s35)))/(s35*(4*me2 - ss - tt + s35)) - (2*(16*me2**2 + 4*ss**2 - tt**2&
          & - 2*tt*s15 - tt*s25 + tt*s35 + s25*s35 - ss*(-4*tt + s15 + 2*s25 + 3*s35) + 2*m&
          &e2*(-8*ss - 3*tt + s15 + 2*s25 + 3*s35)))/s35 + (2*(16*me2**2 + 4*ss**2 + (-tt +&
          & 2*s25)*(-tt + s35) - 2*ss*(-2*tt + s15 + 2*s25 + s35) + me2*(-16*ss - 4*tt + 6*&
          &s15 + 8*s25 + 4*s35)))/s15 + (2*tt*(24*me2**2 + 2*ss**2 + s25*(-tt + s35) - ss*(&
          &-tt + s15 + 2*s25 + s35) + me2*(-16*ss - 4*tt + 4*s15 + 6*s25 + 5*s35)))/(s15*(4&
          &*me2 - ss - tt + s25)) - (2*tt*(12*me2**2 + (ss - s25)*(ss + tt - s35) + me2*(-5&
          &*ss - 5*tt - 3*s15 + 2*s25 + 6*s35)))/(s25*(4*me2 - ss - tt + s35)))*pvb2(1))/tt&
          &**2 + (128*Pi**2*(((-2*me2 + ss)*tt)/s25 - ((2*me2 - ss)*tt*(2*me2 - ss - tt + s&
          &35))/(s25*(tt + s15 - s35)) - (2*me2*tt*(2*me2 - ss - tt + s35))/((4*me2 - ss - &
          &tt + s25)*(s15 + s25 - s35)) - ((2*me2 - ss)*tt*(2*me2 - ss - tt + s35))/((tt + &
          &s15 - s35)*(s15 + s25 - s35)) - (2*(2*me2 - ss)*(2*me2 - ss - tt + s35))/s35 - (&
          &tt*(12*me2**2 + ss*(ss + tt - s35) - 4*me2*(2*ss + tt - s35)))/(s35*(4*me2 - ss &
          &- tt + s35)))*pvb2(2))/tt**2 + 64*Pi**2*((2*(2*me2*(2*tt + s15) + tt*(tt + s15 -&
          & s35)))/(tt**2*s15) + (4*(tt + s15 - s35))/(tt*s35) + (2*(tt + s15 - s35))/(s35*&
          &(4*me2 - ss - tt + s35)) - (2*me2*(4*me2 - ss + s15 + s35))/(tt*s25*(4*me2 - ss &
          &- tt + s35)) - (8*me2**2 + tt*s15 + tt*s25 + 2*me2*(-2*ss + tt + s15 + 2*s25) - &
          &s25*s35 + ss*(-2*tt - s15 + s35))/(tt*s25*(tt + s15 - s35)) + (tt*s15 + tt*s25 +&
          & me2*(8*tt + 4*s15 - 2*s35) - s25*s35 + ss*(-2*tt - s15 + s35))/(tt*s15*(4*me2 -&
          & ss - tt + s25)) + (8*me2**2 - 2*tt**2 - tt*s15 + tt*s25 + me2*(-2*ss + 4*(tt + &
          &2*s15 + s25 - s35)) + 2*tt*s35 - s25*s35 + ss*(-2*tt - s15 + s35))/(tt*(4*me2 - &
          &ss - tt + s25)*(s15 + s25 - s35)) + (8*me2**2 - 2*tt**2 - tt*s15 + tt*s25 + me2*&
          &(-4*ss + 2*tt + 6*s15 + 4*s25 - 4*s35) + 2*tt*s35 - s25*s35 + ss*(-2*tt - s15 + &
          &s35))/(tt*(tt + s15 - s35)*(s15 + s25 - s35)))*pvb3(1) + (128*Pi**2*((tt*(12*me2&
          &**2 - 4*me2*(2*ss + tt - s15 - 2*s25) + (ss + tt - s25)*(ss - s15 - s25)))/(s15*&
          &(4*me2 - ss - tt + s25)) + (2*(2*me2 - ss - tt + s25)*(2*me2 - ss + s15 + s25))/&
          &s15 + (tt*(2*me2 - ss - tt + s25)*(2*me2 - ss + s15 + s25))/(s25*(tt + s15 - s35&
          &)) + (tt*(2*me2 - ss + s15 + s25))/(s15 + s25 - s35) + (tt*(2*me2 - ss - tt + s2&
          &5)*(2*me2 - ss + s15 + s25))/((tt + s15 - s35)*(s15 + s25 - s35)) - (2*me2*tt*(-&
          &2*me2 + ss + tt - s25))/(s25*(4*me2 - ss - tt + s35)))*pvb3(2))/tt**2 + (64*Pi**&
          &2*((-2*(8*me2**2 + 2*ss**2 + tt**2 - 2*me2*(4*ss + tt - 3*s15 - 4*s25) - 2*tt*s2&
          &5 + 2*s15*s25 + 2*s25**2 - 2*ss*(-tt + s15 + 2*s25)))/s15 + (tt*(8*me2**2 + 2*ss&
          &**2 + 2*tt**2 + tt*s15 - tt*s25 + 6*s15*s25 + 6*s25**2 - ss*(2*tt + 5*s15 + 8*s2&
          &5 - 3*s35) - 2*tt*s35 - 3*s25*s35 - 2*me2*(4*ss + tt - 3*s15 - 8*s25 + s35)))/(s&
          &25*(tt + s15 - s35)) + (2*tt*(8*me2**2 + (ss - s25)*(ss - s15 - s25) - me2*(6*ss&
          & - 5*s15 - 7*s25 + s35)))/(s25*(4*me2 - ss - tt + s35)) - (tt*(24*me2**2 + 2*ss*&
          &*2 + tt*s15 + tt*s25 + 2*s15*s25 + 2*s25**2 - s25*s35 - 2*me2*(8*ss + tt - 6*s15&
          & - 8*s25 + s35) + ss*(-3*s15 - 4*s25 + s35)))/(s15*(4*me2 - ss - tt + s25)) + (t&
          &t*(8*me2**2 + 4*tt**2 + tt*s15 - 3*tt*s25 + 4*s15*s25 + 4*s25**2 - 2*me2*(2*ss +&
          & 7*tt + s15 - 7*s25 - s35) - 4*tt*s35 - s25*s35 + ss*(2*tt - s15 - 4*s25 + s35))&
          &)/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + (tt*(8*me2**2 + 2*ss**2 + 6*tt**&
          &2 - tt*s15 - 7*tt*s25 + 6*s15*s25 + 6*s25**2 - 4*tt*s35 - s25*s35 + ss*(6*tt - 3&
          &*s15 - 8*s25 + s35) + 2*me2*(-4*ss - 9*tt + s15 + 8*s25 + s35)))/((tt + s15 - s3&
          &5)*(s15 + s25 - s35)) + (2*tt*(24*me2**2 + 2*ss**2 - 2*tt**2 - 5*tt*s15 - 2*tt*s&
          &25 + 2*tt*s35 + s25*s35 - ss*(-4*tt + s15 + 2*s25 + s35) + me2*(-16*ss - 13*tt +&
          & 6*s15 + 12*s25 + 2*s35)))/(s35*(4*me2 - ss - tt + s35)) + (2*(16*me2**2 + 4*ss*&
          &*2 - 6*tt**2 - 9*tt*s15 - 2*tt*s25 + 5*tt*s35 + 2*s25*s35 - 2*ss*(-2*tt + s15 + &
          &2*s25 + s35) + me2*(-16*ss - 10*tt + 4*s15 + 8*s25 + 4*s35)))/s35)*pvb5(1))/tt**&
          &2 + (128*Pi**2*(-((tt*(12*me2**2 - 4*me2*(2*ss + tt - s15 - 2*s25) + (ss - s25)*&
          &(ss + tt - s15 - s25)))/(s15*(4*me2 - ss - tt + s25))) - (2*(2*me2 - ss + s25)*(&
          &2*me2 - ss - tt + s15 + s25))/s15 + (tt*(2*me2 - ss - tt + s15 + s25)*(6*me2 - s&
          &s - 2*tt + 2*s25))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + (tt*(2*me2 - ss&
          & - tt + s15 + s25)*(4*me2 - 2*ss - 3*tt + 3*s25))/((tt + s15 - s35)*(s15 + s25 -&
          & s35)) + (tt*(2*me2 - ss + s25)*(4*me2 - 2*ss + tt + 3*s15 + 3*s25 - s35))/(s25*&
          &(tt + s15 - s35)) + (tt*(2*me2 - ss + s25)*(6*me2 - ss + s15 + s25))/(s25*(4*me2&
          & - ss - tt + s35)) + (16*me2**2 + 4*ss**2 - tt**2 - 5*tt*s15 - 2*tt*s25 + 2*s25*&
          &s35 + 4*me2*(-4*ss + s15 + 2*s25 + s35) - 2*ss*(-2*tt + s15 + 2*s25 + s35))/s35 &
          &+ (tt*(24*me2**2 + 2*ss**2 - 3*tt*s15 - 2*tt*s25 + s25*s35 - ss*(-3*tt + s15 + 2&
          &*s25 + s35) + 2*me2*(-8*ss - 3*tt + 3*s15 + 6*s25 + s35)))/(s35*(4*me2 - ss - tt&
          & + s35)))*pvb5(2))/tt**2 + (256*Pi**2*(2/tt + 1/(4*me2 - ss - tt + s35))*((-2*me&
          &2 - s15)/6. + 4*pvb5(3)))/s35 + (256*Pi**2*(me2 - s15)*(8*me2 - 2*ss - tt + 2*s3&
          &5)*pvb5(4))/(tt*s35*(4*me2 - ss - tt + s35)) + (64*Pi**2*(-((tt*(2*tt**2*s15 + 2&
          &*tt*s15**2 + 4*ss**2*s25 + 4*tt**2*s25 + 3*tt*s15*s25 + 2*s15**2*s25 - tt*s25**2&
          & + 2*s15*s25**2 + 4*me2**2*(6*tt + 7*s15 + 6*s25 - 5*s35) - tt*s15*s35 - 7*tt*s2&
          &5*s35 - 4*s15*s25*s35 + s25**2*s35 + 3*s25*s35**2 + 2*me2*(2*tt**2 + s15**2 - 2*&
          &tt*s25 + 6*s15*s25 + 5*s25**2 - ss*(6*tt + 7*s15 + 10*s25 - 5*s35) - 2*tt*s35 + &
          &s15*s35 + s25*s35 + s35**2) - ss*(2*tt**2 - 6*tt*s25 + 4*s25**2 + s15*(2*tt + s2&
          &5 - s35) - 2*tt*s35 + 3*s25*s35 + s35**2)))/(s25*(tt + s15 - s35))) - (tt*(2*(ss&
          & - s25)*(-(s15*(s15 + s25)) + ss*(tt + 2*s15 + s25 - s35)) + 4*me2**2*(5*tt + 6*&
          &s15 + 2*s25 - 2*s35) + me2*(7*tt*s15 + 10*s15**2 + 9*tt*s25 + 18*s15*s25 + 6*s25&
          &**2 - ss*(14*tt + 23*s15 + 8*s25 - 13*s35) - 2*tt*s35 - 6*s15*s35 - 9*s25*s35 + &
          &2*s35**2)))/(s25*(4*me2 - ss - tt + s35)) - (tt*(2*tt**3 + 2*tt**2*s15 + tt*s15*&
          &*2 - 2*tt**2*s25 + 2*tt*s15*s25 + 2*s15**2*s25 + 3*tt*s25**2 + 2*s15*s25**2 - 4*&
          &me2**2*(6*tt + 9*s15 + 2*s25 - 7*s35) - 4*ss**2*(s15 + s25 - s35) - 2*tt**2*s35 &
          &- 5*s15*s25*s35 - 3*s25**2*s35 + 2*s25*s35**2 + ss*(2*tt**2 + 3*s15**2 - 2*tt*s2&
          &5 + 7*s15*s25 + 4*s25**2 - 3*s15*s35 - 3*s25*s35) + 2*me2*(4*tt**2 - 6*s15**2 - &
          &4*tt*s25 - 3*s25**2 + ss*(6*tt + 13*s15 + 6*s25 - 11*s35) - s15*(tt + 10*s25 - 5&
          &*s35) - 5*tt*s35 + 5*s25*s35 + 2*s35**2)))/((tt + s15 - s35)*(s15 + s25 - s35)) &
          &+ (tt*(4*me2**2*(5*tt + 6*s15 - 2*s35) + 2*ss**2*(tt + 3*s15 + s25 - 2*s35) - (t&
          &t*s15 + s25*(tt - s35))*(2*tt + 3*s15 + s25 - 2*s35) + me2*(-10*tt**2 + 18*s15**&
          &2 + 15*tt*s25 + 4*s25**2 - ss*(14*tt + 27*s15 + 4*s25 - 17*s35) + s15*(7*tt + 22&
          &*s25 - 16*s35) + 6*tt*s35 - 13*s25*s35) - ss*(5*s15**2 + 2*s25**2 + s25*(4*tt - &
          &5*s35) + s15*(-2*tt + 9*s25 - 3*s35) + 2*tt*(-tt + s35))))/((4*me2 - ss - tt + s&
          &25)*(s15 + s25 - s35)) + (2*(4*me2**2*(3*tt + 4*s15 + 2*s25 - 2*s35) + 2*me2*(tt&
          &**2 - 3*tt*s15 + 3*s15**2 - tt*s25 + 6*s15*s25 + 2*s25**2 + 2*s15*s35 - s25*s35 &
          &+ ss*(-9*s15 - 2*s25 + s35)) + s15*(4*ss**2 + tt**2 - tt*s15 - 2*tt*s25 + 2*s15*&
          &s25 + 2*s25**2 - 2*ss*(-2*tt + s15 + 2*s25 + s35))))/s15 + (2*tt*(2*me2**2*(5*tt&
          & + 12*s15 - 4*s35) + me2*(-2*tt*s15 + 7*s15**2 + 6*tt*s25 + 14*s15*s25 + 2*s25**&
          &2 - 2*ss*(tt + 9*s15 + s25 - 2*s35) - tt*s35 - 6*s25*s35 + s35**2) + s15*(2*ss**&
          &2 + s25*(s15 + s25) - ss*(-tt + s15 + 2*s25 + s35))))/(s15*(4*me2 - ss - tt + s2&
          &5)) + (2*(-32*me2**3 + 3*tt**3 + 4*tt**2*s15 + 5*tt*s15**2 - 4*ss**2*(-tt + s15)&
          & - tt**2*s25 + 5*tt*s15*s25 + 2*tt*s25**2 - 4*tt**2*s35 - 3*tt*s15*s35 - tt*s25*&
          &s35 - 5*s15*s25*s35 - 2*s25**2*s35 + tt*s35**2 + 2*s25*s35**2 + 4*me2**2*(8*ss +&
          & tt - 7*s15 - 6*s25 + s35) + ss*(3*s15**2 + tt*(4*tt - 2*s25 - 3*s35) + s15*(-5*&
          &tt + 6*s25 + s35)) - 2*me2*(4*ss**2 - 11*tt*s15 + 3*s15**2 - 5*tt*s25 + 9*s15*s2&
          &5 + 3*s25**2 + 2*tt*s35 + 3*s15*s35 - 2*s35**2 - ss*(-12*tt + 11*s15 + 6*s25 + s&
          &35))))/s35 + (tt*(-96*me2**3 - 4*ss**2*(-tt + s15) + 4*me2**2*(16*ss + 7*tt - 13&
          &*s15 - 12*s25 - 3*s35) + 2*(tt*s15 + s25*(tt - s35))*(tt + 3*s15 + s25 - s35) + &
          &2*ss*(s15**2 - tt*(tt + s25 - s35) + s15*(-6*tt + 3*s25 + s35)) - 2*me2*(4*ss**2&
          & - 5*tt**2 + 5*s15**2 - 2*tt*s25 + 3*s25**2 + 3*tt*s35 + s25*s35 - s35**2 + 3*s1&
          &5*(-6*tt + 5*s25 + s35) - 2*ss*(-7*tt + 7*s15 + 3*s25 + 2*s35))))/(s35*(4*me2 - &
          &ss - tt + s35)))*pvc1(1))/tt**2 + (64*Pi**2*((-2*tt*(24*me2**3 - 2*me2**2*(8*ss &
          &+ 4*tt + 12*s15 - s25 - 7*s35) + ss*s15*(-2*ss - tt + s15 + s25 + s35) + me2*(2*&
          &ss**2 + 4*tt*s15 - 4*s15**2 - 3*tt*s25 - 4*s15*s25 + ss*(2*tt + 18*s15 + s25 - 6&
          &*s35) - 3*s15*s35 + 3*s25*s35)))/(s15*(4*me2 - ss - tt + s25)) + (2*tt*(24*me2**&
          &3 + ss*(-ss**2 + s15**2 + s15*s25 + s25*(tt - s35) - 2*ss*(tt + s15 - s35)) - 2*&
          &me2**2*(14*ss + 9*tt + 5*s15 - 4*s25 - 8*s35) + me2*(10*ss**2 - 3*s15**2 + ss*(1&
          &3*tt + 10*s15 - 4*s25 - 12*s35) - 2*s15*(tt + s25 - s35) + 4*s25*(-tt + s35))))/&
          &(s25*(4*me2 - ss - tt + s35)) + (2*tt*(-24*me2**3 + ss**2*(tt - 3*s15) + tt*s15*&
          &*2 + s15*s25*(tt - s35) + 2*me2**2*(8*ss - 3*tt - 13*s15 + 3*s25 - s35) + ss*(tt&
          & + s25 - s35)*(-tt + s35) + ss*s15*(-6*tt + s25 + 4*s35) + me2*(-2*ss**2 - 2*s15&
          &**2 + s15*(14*tt - 6*s25 - 9*s35) + ss*(21*s15 + s25 - s35) + 2*(-tt + s35)*(-3*&
          &tt + 2*s25 + s35))))/(s35*(4*me2 - ss - tt + s35)) - (2*(16*me2**3 - 2*me2**2*(8&
          &*ss + 4*tt + 7*s15 + 2*s25 - 5*s35) + me2*(4*ss**2 + 6*tt*s15 - 4*s15**2 + tt*s2&
          &5 - 4*s15*s25 + ss*(4*tt + 17*s15 + 2*s25 - 5*s35) - 4*s15*s35 - s25*s35) + s15*&
          &(-4*ss**2 + (-tt + s25)*(tt - s35) + ss*(-4*tt + s15 + 2*s25 + 3*s35))))/s15 - (&
          &2*(16*me2**3 - 2*tt**3 - 2*tt**2*s15 - 2*tt*s15**2 + ss**2*(-2*tt + 6*s15) + tt*&
          &*2*s25 - tt*s15*s25 + 4*tt**2*s35 + 2*tt*s15*s35 - 2*tt*s25*s35 + s15*s25*s35 - &
          &2*tt*s35**2 + s25*s35**2 - 2*me2**2*(8*ss + 2*tt - 13*s15 - 2*s25 + s35) + ss*(-&
          &s15**2 + s15*(7*tt - 2*s25 - 6*s35) - (-2*tt + 2*s25 - s35)*(-tt + s35)) + me2*(&
          &4*ss**2 + 2*s15**2 + (-2*tt + 5*s25 - 6*s35)*(-tt + s35) - ss*(-12*tt + 25*s15 +&
          & 2*s25 + 3*s35) + 4*s15*(-4*tt + s25 + 4*s35))))/s35 - (tt*(-32*me2**3 + 4*ss**3&
          & + tt*s15**2 + 2*tt**2*s25 + 3*tt*s15*s25 + 4*me2**2*(12*ss + 10*tt + 3*s15 + 2*&
          &s25 - 9*s35) + ss**2*(4*tt - 3*s15 - 3*s35) - 4*tt*s25*s35 - 3*s15*s25*s35 + 2*s&
          &25*s35**2 - ss*(s15**2 + s15*(7*tt - 2*s25 - 5*s35) + (-tt + s35)*(-2*tt + s25 +&
          & 2*s35)) - 2*me2*(12*ss**2 - s15**2 + 2*ss*(7*tt + s25 - 6*s35) - (-tt + s35)*(-&
          &2*tt + s25 + 2*s35) + s15*(-5*tt + 2*s25 + 4*s35))))/(s25*(tt + s15 - s35)) - (t&
          &t*(48*me2**3 - 2*ss**3 - 4*me2**2*(14*ss + 15*tt + 4*s15 - 6*s25 - 11*s35) + (tt&
          &*s15 + s25*(tt - s35))*(tt + s15 - s35) + ss**2*(-6*tt - 7*s15 + 7*s35) + ss*(3*&
          &s15**2 + 2*s15*(-2*tt + 2*s25 + s35) - (-tt + s35)*(-4*tt + 3*s25 + 3*s35)) + 2*&
          &me2*(10*ss**2 - 4*s15**2 + 3*ss*(7*tt + 4*s15 - 2*s25 - 6*s35) + s15*(-4*s25 + s&
          &35) + (-tt + s35)*(-9*tt + 7*s25 + 5*s35))))/((4*me2 - ss - tt + s25)*(s15 + s25&
          & - s35)) - (tt*(32*me2**3 - 4*ss**3 - 4*me2**2*(12*ss + 14*tt + 7*s15 - 2*s25 - &
          &13*s35) + ss**2*(-8*tt - 3*s15 + 9*s35) + (tt + s15 - s35)*(tt*(2*tt + s15 - 2*s&
          &35) + s25*(-tt + s35)) + ss*(3*s15**2 + 2*s15*(tt + s25 - s35) - (-tt + s35)*(-2&
          &*tt + s25 + 3*s35)) + 2*me2*(12*ss**2 - 3*s15**2 + 2*ss*(11*tt + 5*s15 - s25 - 1&
          &1*s35) - s15*(2*s25 + s35) + (-tt + s35)*(-8*tt + 3*s25 + 7*s35))))/((tt + s15 -&
          & s35)*(s15 + s25 - s35)))*pvc1(2))/tt**2 + (64*Pi**2*((tt*(-48*me2**3 + 2*ss**3 &
          &- tt**2*s15 - tt*s15**2 - 3*tt**2*s25 - 2*tt*s15*s25 + 2*s15**2*s25 + tt*s25**2 &
          &+ 2*s15*s25**2 + 4*me2**2*(14*ss + 15*tt + 2*s15 - 12*s25 - 9*s35) + ss**2*(6*tt&
          & + 7*s15 - 2*s25 - 7*s35) + 5*tt*s25*s35 + s15*s25*s35 - s25**2*s35 - 2*s25*s35*&
          &*2 + ss*(4*tt**2 + 2*tt*s15 - 7*s15**2 - 9*tt*s25 - 11*s15*s25 - 5*tt*s35 + 3*s1&
          &5*s35 + 8*s25*s35 + 2*s35**2) - 2*me2*(10*ss**2 + 9*tt**2 - 7*tt*s15 - 6*s15**2 &
          &- 20*tt*s25 - 7*s15*s25 + 4*s25**2 + ss*(21*tt + 10*s15 - 14*s25 - 16*s35) - 7*t&
          &t*s35 + 5*s15*s35 + 12*s25*s35 + 2*s35**2)))/((4*me2 - ss - tt + s25)*(s15 + s25&
          & - s35)) + (tt*(-32*me2**3 + 4*ss**3 - 2*tt**3 - tt**2*s15 + tt*s15**2 + tt**2*s&
          &25 - tt*s25**2 + 4*me2**2*(12*ss + 14*tt + 5*s15 - 6*s25 - 11*s35) + ss**2*(8*tt&
          & + s15 - 4*s25 - 7*s35) + 2*tt**2*s35 - 2*tt*s15*s35 + tt*s25*s35 + 3*s15*s25*s3&
          &5 + s25**2*s35 - 2*s25*s35**2 + ss*(2*tt**2 - 4*tt*s15 - 3*s15**2 - 7*tt*s25 - 5&
          &*s15*s25 - 3*tt*s35 + 3*s15*s35 + 6*s25*s35 + 2*s35**2) - 2*me2*(12*ss**2 + 8*tt&
          &**2 - 8*tt*s15 - 6*s15**2 - 15*tt*s25 - 9*s15*s25 + 2*ss*(11*tt + 3*s15 - 5*s25 &
          &- 9*s35) - 7*tt*s35 + 7*s15*s35 + 12*s25*s35 + 2*s35**2)))/((tt + s15 - s35)*(s1&
          &5 + s25 - s35)) + (2*(-16*me2**3 + 2*tt**3 + ss**2*(2*tt - 6*s15) + tt**2*s15 + &
          &3*tt*s15**2 - 2*tt**2*s25 + 8*tt*s15*s25 + 4*tt*s25**2 - 2*tt**2*s35 - 8*s15*s25&
          &*s35 - 4*s25**2*s35 + 2*s25*s35**2 + 2*me2**2*(8*ss + 2*tt - 15*s15 - 6*s25 + 3*&
          &s35) + 2*ss*(tt**2 - 4*tt*s15 + 2*s15**2 - 2*tt*s25 + 4*s15*s25 + 2*s15*s35 + 2*&
          &s25*s35 - s35**2) - me2*(4*ss**2 + 2*tt**2 - 20*tt*s15 + 8*s15**2 - 11*tt*s25 + &
          &22*s15*s25 + 6*s25**2 + 2*tt*s35 + 12*s15*s35 + 5*s25*s35 - 4*s35**2 - ss*(-12*t&
          &t + 27*s15 + 6*s25 + s35))))/s35 + (2*(-16*me2**3 + 2*me2**2*(8*ss + 4*tt + 5*s1&
          &5 - 2*s25 - 3*s35) + s15*(4*ss**2 + tt**2 - tt*s15 - 3*tt*s25 + 2*s15*s25 + 2*s2&
          &5**2 + s25*s35 - ss*(-4*tt + 3*s15 + 6*s25 + s35)) + me2*(-4*ss**2 + 8*s15**2 + &
          &s25*(tt + 2*s25 - 3*s35) + 2*s15*(-3*tt + 7*s25 + s35) + ss*(-4*tt - 15*s15 + 2*&
          &s25 + 3*s35))))/s15 + (2*tt*(-24*me2**3 + ss**2*(tt - 3*s15) + 2*tt*s15**2 + tt*&
          &*2*s25 + 6*tt*s15*s25 + 2*tt*s25**2 + 2*me2**2*(8*ss - 17*s15 - 3*(tt + s25 - s3&
          &5)) - 2*tt*s25*s35 - 5*s15*s25*s35 - 2*s25**2*s35 + s25*s35**2 + me2*(-2*ss**2 +&
          & 6*tt**2 - 7*s15**2 - 6*tt*s25 - 3*s25**2 + ss*(23*s15 + 3*s25 - 3*s35) - 4*tt*s&
          &35 + s25*s35 + 2*s35**2 - 3*s15*(-5*tt + 7*s25 + 2*s35)) + ss*(s15**2 + (tt + 2*&
          &s25 - s35)*(-tt + s35) + s15*(-7*tt + 4*s25 + 3*s35))))/(s35*(4*me2 - ss - tt + &
          &s35)) + (tt*(32*me2**3 - 4*ss**3 - 2*tt*s15**2 - 4*tt**2*s25 - 7*tt*s15*s25 - tt&
          &*s25**2 - 4*me2**2*(12*ss + 10*tt + s15 - 2*s25 - 7*s35) + 6*tt*s25*s35 + 4*s15*&
          &s25*s35 + s25**2*s35 - 2*s25*s35**2 + ss**2*(-4*tt + 5*s15 + 4*s25 + s35) + ss*(&
          &-2*s15**2 + s25*(tt - 2*s35) + s15*(7*tt - 5*s25 - 2*s35) + 2*(-tt + s35)**2) + &
          &2*me2*(12*ss**2 - 2*tt**2 + s15**2 - 7*tt*s25 + 4*tt*s35 + 6*s25*s35 - 2*s35**2 &
          &+ s15*(-5*tt + s25 + 2*s35) - 2*ss*(-7*tt + 2*s15 + 3*s25 + 4*s35))))/(s25*(tt +&
          & s15 - s35)) + (2*tt*(-24*me2**3 + 2*me2**2*(8*ss + 4*tt + 10*s15 - 7*s25 - 5*s3&
          &5) - s15*(ss - s25)*(-2*ss - tt + s15 + s25 + s35) + me2*(-2*ss**2 - 4*tt*s15 + &
          &8*s15**2 + 5*tt*s25 + 17*s15*s25 + s25**2 - 6*s25*s35 + ss*(-2*tt - 18*s15 + s25&
          & + 6*s35))))/(s15*(4*me2 - ss - tt + s25)) - (2*tt*(-24*me2**3 + 2*me2**2*(14*ss&
          & + 9*tt + 3*s15 - 10*s25 - 6*s35) + (ss - s25)*(ss**2 - s15**2 - s15*s25 + 2*ss*&
          &(tt + s15 - s35) + s25*(-tt + s35)) + me2*(-10*ss**2 + 2*s15**2 + 2*s15*(tt + 3*&
          &s25) + s25*(11*tt - 4*s25 - 8*s35) + ss*(-13*tt - 8*s15 + 12*s25 + 10*s35))))/(s&
          &25*(4*me2 - ss - tt + s35)))*pvc1(3))/tt**2 + (64*Pi**2*((8*me2 + (tt*(-2*(tt*s1&
          &5 + s25*(tt - s35) + ss*(s15 + 2*s25 - s35)) + 4*me2*(s15 + 2*s25 - s35)))/(s25*&
          &(tt + s15 - s35)) + (4*me2*tt*(2*s15 + 2*s25 - s35))/(s15*(4*me2 - ss - tt + s25&
          &)) - (4*me2*tt*s15)/(s25*(4*me2 - ss - tt + s35)) + (2*tt*(2*tt*s15 + me2*(10*tt&
          & - 4*s15 - 12*s25) + 2*tt*s25 + 2*ss*(-tt + s25) - 2*s25*s35))/(s35*(4*me2 - ss &
          &- tt + s35)) + (2*(2*tt**2 + 4*tt*s15 + 2*tt*s25 + 2*ss*(s15 + 2*s25 - s35) - 2*&
          &tt*s35 - 2*s25*s35 - 4*me2*(-3*tt + s15 + 2*s25 + s35)))/s35 + (tt*(2*me2*(4*s15&
          & + 4*s25 - 2*s35) - 2*(tt*s15 + tt*s25 + ss*(s15 + 2*s25 - s35) - s25*s35)))/((4&
          &*me2 - ss - tt + s25)*(s15 + s25 - s35)) + (tt*(4*me2*(s15 + 2*s25 - s35) - 2*(t&
          &t*s15 + tt*s25 + ss*(s15 + 2*s25 - s35) - s25*s35)))/((tt + s15 - s35)*(s15 + s2&
          &5 - s35)))/4. + (-16*me2 + (tt*(32*me2**2 + 4*tt*(ss - s15) + 4*(tt*s15 + s25*(t&
          &t - s35) + ss*(s15 + 2*s25 - s35)) - 16*me2*(ss + tt + s25 - s35)))/(s25*(tt + s&
          &15 - s35)) - (8*me2*tt*(2*s15 + 2*s25 - s35))/(s15*(4*me2 - ss - tt + s25)) + (8&
          &*me2*tt*(6*me2 - ss - 2*tt + s15 + s35))/(s25*(4*me2 - ss - tt + s35)) + (2*tt*(&
          &24*me2**2 + 2*ss**2 - 4*tt*s15 - 6*tt*s25 + me2*(-16*ss - 28*tt + 12*s15 + 36*s2&
          &5) + 6*s25*s35 - 2*ss*(-4*tt + 3*s25 + s35)))/(s35*(4*me2 - ss - tt + s35)) + (2&
          &*(16*me2**2 + 4*ss**2 - 6*tt**2 - 8*tt*s15 - 6*tt*s25 + 6*tt*s35 + 6*s25*s35 + s&
          &s*(4*tt - 6*s15 - 12*s25 + 2*s35) + 2*me2*(-8*ss - 12*tt + 6*s15 + 12*s25 + 2*s3&
          &5)))/s35 + (tt*(-48*me2**2 + 8*me2*(ss + 3*tt - s15 - 2*s25) - 4*tt*(ss + tt + s&
          &15 - s35) + 4*(tt*s15 + tt*s25 + ss*(s15 + 2*s25 - s35) - s25*s35)))/((4*me2 - s&
          &s - tt + s25)*(s15 + s25 - s35)) + (tt*(-32*me2**2 + 16*me2*(ss + tt - s25) - 4*&
          &tt*(ss + tt + s15 - s35) + 4*(tt*s15 + tt*s25 + ss*(s15 + 2*s25 - s35) - s25*s35&
          &)))/((tt + s15 - s35)*(s15 + s25 - s35)))*pvc1(4)))/tt**2 + (64*Pi**2*(-4*me2**2&
          & - (2*me2**2*tt*(2*s15 + 2*s25 - s35))/(s15*(4*me2 - ss - tt + s25)) + (2*tt*(24&
          &*me2**3 + 2*me2*ss*(5*ss + 3*tt - 3*s35) - ss**2*(ss + tt - s35) + me2**2*(-28*s&
          &s - 8*tt + s15 + 8*s35)))/(s25*(4*me2 - ss - tt + s35)) + (tt*(32*me2**3 - 2*me2&
          &**2*(24*ss + 8*tt - 3*s15 + 2*s25 - 9*s35) - 2*ss*(2*ss - s15)*(ss + tt - s35) +&
          & me2*(24*ss**2 - 3*tt*s15 + tt*s25 + ss*(16*tt - 7*s15 + 2*s25 - 17*s35) + 4*s15&
          &*s35 - s25*s35)))/(s25*(tt + s15 - s35)) + (2*(16*me2**3 - 2*me2**2*(8*ss + 7*tt&
          & + 3*s15 - 2*s25 - 5*s35) - 2*ss*s15*(ss + tt - s35) + me2*(4*ss**2 + s15*(2*tt &
          &- 4*s35) + ss*(4*tt + 7*s15 - 2*s25 - 3*s35) + (tt + s25)*(-tt + s35))))/s35 + (&
          &2*tt*(24*me2**3 - ss*s15*(ss + tt - s35) + me2**2*(-16*ss - 21*tt - 10*s15 + 6*s&
          &25 + 16*s35) + me2*(2*ss**2 + ss*(7*tt + 8*s15 - s25 - 6*s35) + s15*(3*tt - 4*s3&
          &5) + (-tt + s35)*(-2*tt + s25 + 2*s35))))/(s35*(4*me2 - ss - tt + s35)) + (tt*(-&
          &32*me2**3 + me2**2*(48*ss + 32*tt + 6*s15 - 4*s25 - 30*s35) + 2*ss*(2*ss + 2*tt &
          &+ s15 - 2*s35)*(ss + tt - s35) - me2*(24*ss**2 + ss*(32*tt + 7*s15 - 2*s25 - 31*&
          &s35) + s15*(3*tt - 4*s35) + (-tt + s35)*(-8*tt + s25 + 8*s35))))/((tt + s15 - s3&
          &5)*(s15 + s25 - s35)) + (tt*(-48*me2**3 + me2**2*(56*ss + 40*tt + 4*s15 - 4*s25 &
          &- 38*s35) + 2*ss*(ss + tt - s35)*(ss + tt + s15 - s35) - me2*(20*ss**2 + ss*(28*&
          &tt + 7*s15 - 2*s25 - 27*s35) + s15*(3*tt - 4*s35) + (-tt + s35)*(-8*tt + s25 + 8&
          &*s35))))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)))*pvc1(5))/tt**2 + (64*Pi**2&
          &*(4*me2*(-2*me2 + s15) - (2*me2*tt*(2*me2 - s15)*(2*s15 + 2*s25 - s35))/(s15*(4*&
          &me2 - ss - tt + s25)) + (2*tt*(48*me2**3 - 2*me2**2*(16*ss + 21*tt + 8*s15 - 12*&
          &s25 - 8*s35) - ss**2*(2*s15 + s35) + s15*(tt*s15 + 2*tt*s25 - 2*s25*s35) + me2*(&
          &4*ss**2 - 4*s15**2 + s15*(11*tt - 12*s25 - 6*s35) + 2*ss*(7*tt + 8*s15 - 2*s25 -&
          & 2*s35) + 2*(-2*tt + 3*s25)*(-tt + s35)) + ss*(s25*(-tt + s35) + s15*(-4*tt + 2*&
          &s25 + 3*s35))))/(s35*(4*me2 - ss - tt + s35)) + (2*(32*me2**3 + tt**2*s15 + 2*tt&
          &*s15**2 - tt**2*s25 + 2*tt*s15*s25 - 4*me2**2*(8*ss + 7*tt + 2*s15 - 4*s25 - 2*s&
          &35) - tt*s15*s35 + 2*tt*s25*s35 - 2*s15*s25*s35 - s25*s35**2 - 2*ss**2*(2*s15 + &
          &s35) + 2*me2*(4*ss**2 - 2*s15**2 + s15*(6*tt - 4*s25 - 5*s35) - (-tt + s35)**2 +&
          & 2*ss*(2*tt + 3*s15 - 2*s25 + s35)) + ss*(2*s15**2 + (-tt + s35)*(2*s25 + s35) +&
          & s15*(-5*tt + 4*s25 + 3*s35))))/s35 - (2*tt*(-48*me2**3 + 2*me2**2*(28*ss + 8*tt&
          & - 3*s15 - 12*s25 - 6*s35) + 2*ss*(ss - s25)*(ss + tt - s35) + me2*(-20*ss**2 + &
          &s15**2 + 6*tt*s25 - 6*s25*s35 + 2*ss*(-6*tt + s15 + 8*s25 + 5*s35))))/(s25*(4*me&
          &2 - ss - tt + s35)) + (tt*(64*me2**3 - 8*ss**3 - 4*me2**2*(24*ss + 8*tt - 5*s15 &
          &- 6*s25 - 7*s35) + ss**2*(-8*tt + 6*s15 + 8*s25 + 6*s35) - s15*(tt*s15 + 3*tt*s2&
          &5 - 3*s25*s35) - ss*(3*s15**2 + 6*s25*(-tt + s35) + s15*(-4*tt + 6*s25 + s35)) +&
          & 2*me2*(24*ss**2 + 3*s15**2 + 5*s25*(-tt + s35) + s15*(-3*tt + 6*s25 + s35) - ss&
          &*(-16*tt + 11*s15 + 14*s25 + 13*s35))))/(s25*(tt + s15 - s35)) + (tt*(-96*me2**3&
          & + 4*ss**3 - tt*s15**2 - 2*tt**2*s25 - 3*tt*s15*s25 + 4*me2**2*(28*ss + 20*tt - &
          &6*s15 - 14*s25 - 11*s35) + 2*ss**2*(4*tt + s15 - 2*s25 - 3*s35) + 4*tt*s25*s35 +&
          & 3*s15*s25*s35 - 2*s25*s35**2 + ss*(-3*s15**2 + s15*(2*tt - 6*s25 + s35) + 2*(-t&
          &t + s35)*(-2*tt + 3*s25 + s35)) + 2*me2*(-20*ss**2 + 4*s15**2 + s15*(3*tt + 6*s2&
          &5 - 5*s35) - (-tt + s35)*(-8*tt + 13*s25 + 2*s35) + ss*(-28*tt + 3*s15 + 18*s25 &
          &+ 17*s35))))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) - (tt*(64*me2**3 - 8*ss&
          &**3 + tt*s15**2 + 2*tt**2*s25 + 3*tt*s15*s25 - 4*tt*s25*s35 - 3*s15*s25*s35 + 2*&
          &s25*s35**2 + 2*ss**2*(-8*tt + s15 + 4*s25 + 5*s35) + me2**2*(-96*ss - 64*tt + 12&
          &*s15 + 40*s25 + 36*s35) + ss*(3*s15**2 + s15*(2*tt + 6*s25 - 5*s35) - 2*(-tt + s&
          &35)*(-4*tt + 5*s25 + s35)) + 2*me2*(24*ss**2 - 3*s15**2 + (-tt + s35)*(-8*tt + 1&
          &1*s25 + 2*s35) + s15*(-3*tt - 6*s25 + 5*s35) - ss*(-32*tt + 5*s15 + 18*s25 + 19*&
          &s35))))/((tt + s15 - s35)*(s15 + s25 - s35)))*pvc1(6))/tt**2 + (64*Pi**2*(4*me2*&
          &(-me2 + s15) - (2*me2*tt*(me2 - s15)*(2*s15 + 2*s25 - s35))/(s15*(4*me2 - ss - t&
          &t + s25)) + (2*tt*(24*me2**3 + tt*s15**2 - me2**2*(16*ss + 21*tt + 6*s15 - 18*s2&
          &5) + 3*tt*s15*s25 + tt*s25**2 - 3*s15*s25*s35 - s25**2*s35 - ss**2*(s15 + s35) +&
          & ss*(-3*tt*s15 - tt*s25 + 2*s15*s25 + 2*s15*s35 + 2*s25*s35) + me2*(2*ss**2 + 2*&
          &tt**2 - 4*s15**2 - 5*tt*s25 - s25*s35 - 4*s15*(-2*tt + 3*s25 + s35) + ss*(7*tt +&
          & 8*s15 - 3*s25 + 2*s35))))/(s35*(4*me2 - ss - tt + s35)) + (2*tt*(24*me2**3 - (s&
          &s - s25)**2*(ss + tt - s35) + me2**2*(-28*ss - 8*tt + 5*s15 + 24*s25 + 4*s35) + &
          &me2*(10*ss**2 - s15**2 - 6*tt*s25 + 2*s15*s25 + 6*s25**2 + 4*s25*s35 - 2*ss*(-3*&
          &tt + s15 + 8*s25 + 2*s35))))/(s25*(4*me2 - ss - tt + s35)) + (2*(16*me2**3 + tt*&
          &*2*s15 + 2*tt*s15**2 - tt**2*s25 + 4*tt*s15*s25 + 2*tt*s25**2 - tt*s15*s35 + tt*&
          &s25*s35 - 4*s15*s25*s35 - 2*s25**2*s35 - 2*ss**2*(s15 + s35) - 2*me2**2*(8*ss + &
          &7*tt + s15 - 6*s25 + s35) + ss*(-3*tt*s15 + 2*s15**2 - 2*tt*s25 + 4*s15*s25 - tt&
          &*s35 + 2*s15*s35 + 4*s25*s35) + me2*(4*ss**2 - tt**2 + 10*tt*s15 - 4*s15**2 + tt&
          &*s25 - 8*s15*s25 + 3*tt*s35 - 8*s15*s35 - 5*s25*s35 + ss*(4*tt + 5*s15 - 6*s25 +&
          & 7*s35))))/s35 - (tt*(48*me2**3 - 2*ss**3 + tt*s15**2 + 2*tt**2*s25 + tt*s15*s25&
          & - 2*s15**2*s25 - 2*tt*s25**2 - 2*s15*s25**2 - 2*tt*s25*s35 + s15*s25*s35 + 2*s2&
          &5**2*s35 + 2*ss**2*(-2*tt + 2*s25 + s35) + me2**2*(-56*ss - 40*tt + 28*s15 + 52*&
          &s25 + 6*s35) + ss*(-2*tt**2 + 3*s15**2 + 6*tt*s25 + 4*s15*s25 - 2*s25**2 + 2*tt*&
          &s35 - 3*s15*s35 - 4*s25*s35) + me2*(20*ss**2 + 8*tt**2 - 9*tt*s15 - 4*s15**2 - 2&
          &5*tt*s25 + 4*s15*s25 + 12*s25**2 - 4*tt*s35 + 6*s15*s35 + 9*s25*s35 - ss*(-28*tt&
          & + 13*s15 + 34*s25 + 7*s35))))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) - (tt&
          &*(32*me2**3 - 4*ss**3 + tt*s15**2 + 2*tt**2*s25 + tt*s15*s25 - 2*s15**2*s25 - 2*&
          &tt*s25**2 - 2*s15*s25**2 - 2*tt*s25*s35 + s15*s25*s35 + 2*s25**2*s35 + 2*ss**2*(&
          &-4*tt + 2*s15 + 4*s25 + s35) + me2**2*(-48*ss - 32*tt + 18*s15 + 36*s25 + 6*s35)&
          & + ss*(s15**2 + s15*(4*tt - 3*s35) - 2*(-tt + 2*s25)*(-2*tt + s25 + s35)) + me2*&
          &(24*ss**2 + 8*tt**2 - 9*tt*s15 - 2*s15**2 - 21*tt*s25 + 8*s25**2 - 4*tt*s35 + 6*&
          &s15*s35 + 9*s25*s35 - ss*(-32*tt + 17*s15 + 34*s25 + 7*s35))))/((tt + s15 - s35)&
          &*(s15 + s25 - s35)) + (tt*(32*me2**3 - 4*ss**3 - tt*s15**2 - 3*tt*s15*s25 + 2*s1&
          &5**2*s25 - 2*tt*s25**2 + 2*s15*s25**2 - 2*me2**2*(24*ss + 8*tt - 7*s15 - 14*s25 &
          &- 5*s35) + s15*s25*s35 + 2*s25**2*s35 + 2*ss**2*(-2*tt + 2*s15 + 4*s25 + s35) + &
          &ss*(-3*s15**2 + s15*(2*tt - 8*s25 + s35) - 2*s25*(-3*tt + 2*s25 + 2*s35)) + me2*&
          &(24*ss**2 + 6*s15**2 + s15*(-3*tt + 16*s25 - 2*s35) + s25*(-11*tt + 8*s25 + 7*s3&
          &5) - ss*(-16*tt + 15*s15 + 30*s25 + 9*s35))))/(s25*(tt + s15 - s35)))*pvc1(7))/t&
          &t**2 + (128*Pi**2*(2*(2*me2 - ss)*(2*me2 - ss - tt + s35) + (tt*(12*me2**2 + ss*&
          &(ss + tt - s35) - 4*me2*(2*ss + tt - s35)))/(4*me2 - ss - tt + s35))*(0.16666666&
          &666666666 + 6*pvc1(8)))/(tt**2*s35) + (128*Pi**2*(8*me2**2 + 2*ss**2 + s25*(-tt &
          &+ s35) - ss*(-2*tt + s15 + 2*s25 + s35) + 2*me2*(-4*ss - 2*tt + s15 + 2*s25 + s3&
          &5) + (tt*(12*me2**2 + (ss - s25)*(ss + tt - s35) + 2*me2*(-4*ss - 2*tt + s15 + 3&
          &*s25 + s35)))/(4*me2 - ss - tt + s35))*(0.16666666666666666 + 6*pvc1(9)))/(tt**2&
          &*s35) + (128*me2*Pi**2*(2*(2*me2 - ss)*(2*me2 - ss - tt + s35) + (tt*(12*me2**2 &
          &+ ss*(ss + tt - s35) - 4*me2*(2*ss + tt - s35)))/(4*me2 - ss - tt + s35))*pvc1(1&
          &0))/(tt**2*s35) + (128*Pi**2*(24*me2**3 - 2*me2**2*(12*ss + 6*tt + 3*s15 - 2*s25&
          & - 5*s35) - 2*ss*s15*(ss + tt - s35) + me2*(6*ss**2 + ss*(6*tt + 7*s15 - 2*s25 -&
          & 5*s35) - (4*s15 - s25)*(-tt + s35)) - (tt*(-36*me2**3 + 2*me2**2*(12*ss + 6*tt &
          &+ 5*s15 - 3*s25 - 5*s35) + ss*s15*(ss + tt - s35) + me2*(-3*ss**2 + (4*s15 - s25&
          &)*(-tt + s35) + ss*(-3*tt - 8*s15 + s25 + 3*s35))))/(4*me2 - ss - tt + s35))*pvc&
          &1(11))/(tt**2*s35) + (128*Pi**2*(24*me2**3 - 4*me2**2*(6*ss + 3*tt + 3*s15 - 2*s&
          &25 - 2*s35) + 2*me2*(3*ss**2 - s15**2 + s15*(4*tt - 2*s25 - 3*s35) + ss*(3*tt + &
          &7*s15 - 2*s25 - 2*s35) + s25*(-tt + s35)) + s15*(-4*ss**2 + s25*(tt - s35) + ss*&
          &(-4*tt + s15 + 2*s25 + 3*s35)) + (tt*(36*me2**3 - 4*me2**2*(6*ss + 3*tt + 5*s15 &
          &- 3*s25 - 2*s35) - s15*(2*ss - s25)*(ss + tt - s35) + me2*(3*ss**2 + ss*(3*tt + &
          &16*s15 - 2*s25 - 3*s35) - 2*(-4*tt*s15 + s15**2 + tt*s25 + 3*s15*s25 + 3*s15*s35&
          & - s25*s35))))/(4*me2 - ss - tt + s35))*pvc1(12))/(tt**2*s35) + (128*Pi**2*(me2 &
          &- s15)*(8*me2**2 + 2*ss**2 + s25*(-tt + s35) - ss*(-2*tt + s15 + 2*s25 + s35) + &
          &2*me2*(-4*ss - 2*tt + s15 + 2*s25 + s35) + (tt*(12*me2**2 + (ss - s25)*(ss + tt &
          &- s35) + 2*me2*(-4*ss - 2*tt + s15 + 3*s25 + s35)))/(4*me2 - ss - tt + s35))*pvc&
          &1(13))/(tt**2*s35) + 64*Pi**2*((2*(-16*me2**2 + 2*s15**2 - 3*s15*(-tt + s35) + (&
          &-tt + s35)**2 + me2*(-tt + 2*s15 + s35)))/(tt*s35) - (2*(14*me2**2 + (ss - s15)*&
          &(tt + s15 - s35) + me2*(-3*ss - 3*tt - 2*s15 + 2*s25 + 2*s35)))/(s35*(4*me2 - ss&
          & - tt + s35)) + (4*me2**2*(8*tt + s35) - 2*me2*(6*tt**2 + 2*tt*s15 - 2*s15**2 - &
          &3*tt*s25 + ss*(9*tt + 2*s15 - s35) - 6*tt*s35 + s15*s35 + s25*s35) + tt*(2*ss**2&
          & - 3*tt*s15 - 2*s15**2 - tt*s25 + ss*(4*tt + s15 - 2*s25 - 3*s35) + 2*s15*s35 + &
          &s25*s35))/(tt*(4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + (8*me2**2*(2*tt + s15&
          &) - 2*me2*(tt*(2*tt + s15 - 3*s35) + ss*(6*tt + 3*s15 - s35) + s25*(-2*tt + s35)&
          &) - tt*(2*s15**2 + s15*(5*tt - 2*s25 - 2*s35) + (-2*tt + s25)*(-tt + s35) + ss*(&
          &s15 + s35)))/(tt*(tt + s15 - s35)*(s15 + s25 - s35)) + (28*me2**2*tt + 2*me2*(s1&
          &5*(5*tt + s35) - tt*(7*ss + 3*tt - 5*s25 + s35)) + tt*(2*ss**2 - tt*s15 + 2*s15*&
          &*2 - tt*s25 + 2*s15*s25 + s25*s35 - ss*(-2*tt + 3*s15 + 2*s25 + s35)))/(tt*s15*(&
          &4*me2 - ss - tt + s25)) - (2*(tt*(ss - s15)*(ss - s15 - s25) + 2*me2**2*(6*tt + &
          &s35) + me2*(5*tt*s15 + 2*s15**2 - ss*(5*tt + s15) + tt*(-tt + 3*s25 + s35))))/(t&
          &t*s25*(4*me2 - ss - tt + s35)) + (2*(16*me2**2*tt + tt*(2*ss**2 + s15**2 - tt*s2&
          &5 + s25*s35 - ss*(-2*tt + s15 + 2*s25 + s35)) + me2*(s15*(3*tt + 2*s35) - tt*(8*&
          &ss + 3*tt - 4*s25 + 2*s35))))/(tt**2*s15) + (-8*me2**2*(4*tt + 2*s15 - s35) - 2*&
          &me2*(-4*tt**2 + 2*tt*s25 + 4*tt*s35 - s25*s35 + 2*s15*(tt + s35) + ss*(-6*tt - 3&
          &*s15 + s35)) + tt*(-4*s15**2 + (2*tt + s25 - 2*s35)*(-tt + s35) + ss*(s15 + s35)&
          & + s15*(-5*tt - 2*s25 + 4*s35)))/(tt*s25*(tt + s15 - s35)))*pvc10(1) + (64*Pi**2&
          &*((2*(me2 + tt + s15 - s35)*(16*me2**2 + 4*ss**2 - tt**2 - 3*tt*s15 - 2*tt*s25 +&
          & 2*s25*s35 - 2*ss*(-2*tt + s15 + 2*s25 + s35) + 4*me2*(-4*ss - 2*tt + s15 + 2*s2&
          &5 + s35)))/s35 + (tt*(2*ss**3 + 3*tt**2*s15 + tt*s15**2 + 3*tt**2*s25 + tt*s15*s&
          &25 + 2*s15**2*s25 + 2*s15*s25**2 - 2*ss**2*(3*s15 + 2*(tt + s25 - s35)) - 4*tt*s&
          &15*s35 - 5*tt*s25*s35 - s15*s25*s35 + 2*s25*s35**2 + 4*me2**2*(6*ss - 8*tt - 11*&
          &s15 - 6*s25 + 11*s35) - 2*me2*(8*ss**2 - 3*tt**2 + 9*tt*s15 + 13*s15**2 + 8*tt*s&
          &25 + 15*s15*s25 + 6*s25**2 - ss*(12*tt + 21*s15 + 14*s25 - 15*s35) + 6*tt*s35 - &
          &10*s15*s35 - 11*s25*s35 - 2*s35**2) + ss*(-4*tt**2 + 3*s15**2 + 4*tt*s25 + 2*s25&
          &**2 + s15*(tt + 4*s25 - s35) + 7*tt*s35 - 4*s25*s35 - 2*s35**2)))/((4*me2 - ss -&
          & tt + s25)*(s15 + s25 - s35)) + (tt*(4*ss**3 - 2*tt**3 + tt**2*s15 - tt*s15**2 +&
          & 5*tt**2*s25 + tt*s15*s25 + 4*s15**2*s25 + 4*s15*s25**2 - 2*ss**2*(tt + 3*s15 + &
          &4*s25 - s35) - 2*tt*s15*s35 - 3*tt*s25*s35 - 3*s15*s25*s35 - 2*s25**2*s35 + 2*s2&
          &5*s35**2 + 4*me2**2*(4*ss - 8*tt - 5*s15 + 5*s35) - 2*me2*(8*ss**2 - 7*tt**2 + 5&
          &*tt*s15 + 5*s15**2 + 9*tt*s25 + 2*s15*s25 - ss*(10*tt + 11*s15 + 8*s25 - 7*s35) &
          &+ 6*tt*s35 - 2*s15*s35 - 3*s25*s35 - 2*s35**2) + ss*(-6*tt**2 + s15**2 + 2*tt*s2&
          &5 + 4*s25**2 + 5*tt*s35 - 2*s35**2 + s15*(-tt + 2*s25 + s35))))/((tt + s15 - s35&
          &)*(s15 + s25 - s35)) - (2*tt*(-24*me2**3 + 2*me2**2*(8*ss - tt - 9*s15 - 6*s25 +&
          & 7*s35) + me2*(-2*ss**2 + 7*tt**2 + 6*tt*s15 - 3*s15**2 - 4*tt*s25 - 6*s15*s25 +&
          & ss*(7*tt + 11*s15 + 2*s25 - 11*s35) - 10*tt*s35 + 2*s15*s35 + 7*s25*s35 + 2*s35&
          &**2) + (tt + s15 - s35)*(-2*ss**2 + 2*tt*s15 + 2*tt*s25 - s25*s35 + ss*(-3*tt + &
          &s15 + 2*s25 + s35))))/(s35*(4*me2 - ss - tt + s35)) + (2*tt*(48*me2**3 - (ss - s&
          &15 - s25)*(ss**2 - 2*tt*s15 - tt*s25 + s15*s25 - ss*(-2*tt + s15 + s25)) + 4*me2&
          &**2*(-11*ss - 9*tt + 4*s15 + 11*s25 + s35) + me2*(12*ss**2 + 6*tt**2 - 7*tt*s15 &
          &+ 5*s15**2 - 17*tt*s25 + 11*s15*s25 + 10*s25**2 - 5*tt*s35 - 3*s15*s35 + 2*s25*s&
          &35 - 2*ss*(-9*tt + 6*s15 + 11*s25 + s35))))/(s25*(4*me2 - ss - tt + s35)) + (2*(&
          &-16*me2**3 + tt**2*s15 + 3*tt*s15**2 + tt**2*s25 + tt*s15*s25 - 4*s15**2*s25 - 2&
          &*tt*s25**2 - 4*s15*s25**2 - 2*ss**2*(s15 - s35) + tt**2*s35 - tt*s15*s35 - tt*s2&
          &5*s35 + 3*s15*s25*s35 + 2*s25**2*s35 + 8*me2**2*(2*ss + tt - 2*s15 - 2*s25 + s35&
          &) + ss*(-(tt*s15) + 3*s15**2 + 2*tt*s25 + 6*s15*s25 + tt*s35 - 3*s15*s35 - 4*s25&
          &*s35) - me2*(4*ss**2 + tt**2 - 5*tt*s15 + 6*s15**2 + 16*s15*s25 + 4*s25**2 + 2*t&
          &t*s35 - 6*s15*s35 - 8*s25*s35 + 4*ss*(tt - 3*s15 - 2*s25 + 2*s35))))/s15 - (tt*(&
          &48*me2**3 - tt**2*s15 - 3*tt*s15**2 - tt**2*s25 - tt*s15*s25 + 4*s15**2*s25 + 2*&
          &tt*s25**2 + 4*s15*s25**2 + 2*ss**2*(s15 - s35) - tt*s25*s35 - 3*s15*s25*s35 - 2*&
          &s25**2*s35 - 4*me2**2*(8*ss + 5*tt - 4*s15 - 8*s25 + 6*s35) + ss*(-3*s15**2 - 2*&
          &tt*s25 + tt*s35 + 4*s25*s35 + s15*(tt - 6*s25 + 3*s35)) + 2*me2*(2*ss**2 + tt**2&
          & - 2*tt*s15 + 2*s15**2 - tt*s25 + 6*s15*s25 + 2*s25**2 + tt*s35 - 5*s15*s35 - 8*&
          &s25*s35 + ss*(3*tt - 4*s15 - 4*s25 + 8*s35))))/(s15*(4*me2 - ss - tt + s25)) + (&
          &tt*(64*me2**3 - 4*ss**3 - 2*tt**3 + tt**2*s15 - 5*tt*s15**2 + 5*tt**2*s25 - 7*tt&
          &*s15*s25 + 4*s15**2*s25 - 4*tt*s25**2 + 4*s15*s25**2 + 2*tt**2*s35 - 7*tt*s25*s3&
          &5 - 3*s15*s25*s35 + 2*s25**2*s35 + 2*s25*s35**2 + 2*ss**2*(-7*tt + s15 + 4*s25 +&
          & 3*s35) + me2**2*(-80*ss - 64*tt + 36*s15 + 64*s25 + 12*s35) + ss*(-10*tt**2 - 3&
          &*s15**2 + 18*tt*s25 - 4*s25**2 + 9*tt*s35 - 8*s25*s35 - 2*s35**2 + s15*(7*tt - 6&
          &*s25 + s35)) + 2*me2*(16*ss**2 + 9*tt**2 - 13*tt*s15 + 3*s15**2 - 19*tt*s25 + 14&
          &*s15*s25 + 8*s25**2 - 8*tt*s35 - 2*s15*s35 + 5*s25*s35 + 2*s35**2 - ss*(-30*tt +&
          & 11*s15 + 24*s25 + 9*s35))))/(s25*(tt + s15 - s35)))*pvc10(2))/tt**2 + 128*Pi**2&
          &*((-2*(2*tt + s15)*(me2 + tt + s15 - s35))/(tt*s35) - (16*me2**2*tt + tt*(ss - t&
          &t - s15)*(ss - s15 - s25) - 2*me2*(s15**2 + tt*(3*ss + 2*tt - 2*s25 - 3*s35) + s&
          &15*(tt - s35)))/(tt*s25*(4*me2 - ss - tt + s35)) + (-12*me2**2 + me2*(6*ss + tt &
          &- 4*s15 - 6*s25) - (2*tt + s15)*(tt + s15 - s35))/(s35*(4*me2 - ss - tt + s35)) &
          &+ (2*me2*s15 + tt*s15 + s15**2 + tt*s35 - s15*s35)/(tt*s15) + (12*me2**2 + tt**2&
          & - 2*tt*s25 - s15*s25 + ss*(2*tt + s15 - s35) + s25*s35 + me2*(-6*ss - 7*tt + 2*&
          &s15 + 6*s25 + 2*s35))/(s15*(4*me2 - ss - tt + s25)) - (8*me2**2*tt + tt*(-(ss*s1&
          &5) + s15**2 + tt*s25 + 2*s15*s25) + me2*(s15**2 - s15*(-9*tt + s35) + tt*(-4*ss &
          &+ 2*s25 + s35)))/(tt*s25*(tt + s15 - s35)) + (8*me2**2*tt + tt*(-(ss*s15) + s15*&
          &*2 + tt*s25 + 2*s15*s25 - tt*s35 - s15*s35) + me2*(-s15**2 + s15*(-3*tt + s35) +&
          & tt*(-4*ss - 4*tt + 2*s25 + s35)))/(tt*(tt + s15 - s35)*(s15 + s25 - s35)) + (16&
          &*me2**2*tt + tt*(ss**2 + s15**2 + s15*(tt + s25 - s35) + (-2*tt + s25)*(-tt + s3&
          &5) - ss*(-3*tt + s25 + s35)) + me2*(-3*s15**2 + s15*(-13*tt + 3*s35) + tt*(-6*ss&
          & - 14*tt + 2*s25 + 9*s35)))/(tt*(4*me2 - ss - tt + s25)*(s15 + s25 - s35)))*pvc1&
          &0(3) + (256*Pi**2*((2*(-2*me2 + tt)*(tt + s15))/s15 + ((2*me2 - tt)*tt*(tt + s15&
          &))/(s15*(-4*me2 + ss + tt - s25)) - (tt*(-12*me2**2 + 2*me2*(ss + tt - s15) + tt&
          &*(-ss + s15 + s25)))/(s25*(4*me2 - ss - tt + s35)) - (tt*(8*me2**2 + tt**2 - tt*&
          &s25 + s15*s25 + s25**2 + me2*(-4*ss - 6*tt + 6*s25) - ss*(s15 + s25 - s35) - s25&
          &*s35))/((tt + s15 - s35)*(s15 + s25 - s35)) + (tt*(-12*me2**2 - s25*(s15 + s25 -&
          & s35) + ss*(tt + s15 + s25 - s35) + 2*me2*(ss + tt - s15 - 3*s25 + s35)))/((4*me&
          &2 - ss - tt + s25)*(s15 + s25 - s35)) - (tt*(-8*me2**2 + tt**2 + tt*s15 - ss*s25&
          & + s15*s25 + s25**2 - tt*s35 + 2*me2*(2*ss - tt - 3*s15 - s25 + s35)))/(s25*(tt &
          &+ s15 - s35)))*pvc10(4))/tt**2 + (128*Pi**2*((-2*(tt + s15)*(2*me2 - ss - tt + s&
          &25)*(2*me2 - ss + s15 + s25))/s15 - (tt*(tt + s15)*(2*me2 - ss - tt + s25)*(2*me&
          &2 - ss + s15 + s25))/(s15*(4*me2 - ss - tt + s25)) + (tt*(2*me2 - ss - tt + s25)&
          &*(2*me2 - ss + s15 + s25)*(4*me2 - 2*ss - tt + s15 + 2*s25))/(s25*(tt + s15 - s3&
          &5)) - (tt*(2*me2 - ss - tt + s25)*(2*me2 - ss + s15 + s25)*(4*me2 - 2*ss - tt + &
          &s15 + 2*s25))/((tt + s15 - s35)*(s15 + s25 - s35)) - (tt*(2*me2 - ss + s15 + s25&
          &)*(12*me2**2 + (ss + tt - s25)*(ss - s15 - s25) - 2*me2*(4*ss + tt - 3*s15 - 4*s&
          &25 + s35)))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + (tt*(2*me2 - ss - tt +&
          & s25)*(12*me2**2 + (-ss + s15 + s25)**2 + me2*(-8*ss - 4*tt + 4*s15 + 8*s25 + 2*&
          &s35)))/(s25*(4*me2 - ss - tt + s35)))*pvc10(5))/tt**2 + 128*Pi**2*((2*(tt + s15)&
          &**2)/(tt*s15) + (tt + s15)**2/(s15*(4*me2 - ss - tt + s25)) - (tt**3 - 2*tt**2*s&
          &15 - 4*tt**2*s25 + 3*tt*s15*s25 + s15**2*s25 + 4*tt*s25**2 + s15*s25**2 + 4*me2*&
          &*2*(4*tt + s15 - s35) + ss**2*(4*tt + s15 - s35) - s15*s25*s35 - s25**2*s35 - ss&
          &*(-4*tt**2 + 3*tt*s15 + s15**2 + 8*tt*s25 + 2*s15*s25 - s15*s35 - 2*s25*s35) + 2&
          &*me2*(-4*tt**2 + 3*tt*s15 + s15**2 + 8*tt*s25 + 2*s15*s25 - 2*ss*(4*tt + s15 - s&
          &35) - s15*s35 - 2*s25*s35))/(tt*(tt + s15 - s35)*(s15 + s25 - s35)) - (tt**3 - 2&
          &*tt**2*s15 - 4*tt**2*s25 + 3*tt*s15*s25 + s15**2*s25 + 4*tt*s25**2 + s15*s25**2 &
          &+ 4*me2**2*(4*tt + s15 - s35) + ss**2*(4*tt + s15 - s35) - tt**2*s35 + tt*s15*s3&
          &5 + 2*tt*s25*s35 - s15*s25*s35 - s25**2*s35 - ss*(-4*tt**2 + 3*tt*s15 + s15**2 +&
          & 8*tt*s25 + 2*s15*s25 + 2*tt*s35 - s15*s35 - 2*s25*s35) + 2*me2*(-4*tt**2 + 3*tt&
          &*s15 + s15**2 + 8*tt*s25 + 2*s15*s25 - 2*ss*(4*tt + s15 - s35) + 2*tt*s35 - s15*&
          &s35 - 2*s25*s35))/(tt*s25*(tt + s15 - s35)) + (tt*(tt + s15)*(ss - s15 - s25) - &
          &4*me2**2*(4*tt + s15 - s35) + 2*me2*(3*tt**2 - 4*tt*s25 - s15*s25 + ss*(4*tt + s&
          &15 - s35) - 2*tt*s35 + s25*s35))/(tt*s25*(4*me2 - ss - tt + s35)) - (8*me2**2*(4&
          &*tt + s15 - s35) + (ss - s15 - s25)*(3*tt**2 - 4*tt*s25 - s15*s25 + ss*(4*tt + s&
          &15 - s35) + s25*s35) - 2*me2*(3*tt**2 - 8*tt*s15 - 2*s15**2 - 12*tt*s25 - 3*s15*&
          &s25 + 3*ss*(4*tt + s15 - s35) + tt*s35 + 2*s15*s35 + 3*s25*s35))/(tt*(4*me2 - ss&
          & - tt + s25)*(s15 + s25 - s35)))*pvc10(6) + 128*Pi**2*((2*(tt + s15))/s15 + (tt*&
          &(tt + s15))/(s15*(4*me2 - ss - tt + s25)) + (tt*(ss - s15 - s25) + 2*me2*(tt + s&
          &15 - s35))/(s25*(4*me2 - ss - tt + s35)) + (tt**2 - 2*tt*s25 - s15*s25 - 2*me2*(&
          &2*tt + s15 - s35) + ss*(2*tt + s15 - s35) + s25*s35)/((tt + s15 - s35)*(s15 + s2&
          &5 - s35)) + (2*tt**2 - 3*tt*s25 - s15*s25 - 2*me2*(5*tt + 2*s15 - 2*s35) + ss*(3&
          &*tt + s15 - s35) + s25*s35)/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + (-tt**&
          &2 + tt*s15 + s15**2 + 2*tt*s25 + s15*s25 + 2*me2*(2*tt + s15 - s35) + tt*s35 - s&
          &15*s35 - s25*s35 + ss*(-2*tt - s15 + s35))/(s25*(tt + s15 - s35)))*pvc10(7) + 64&
          &*Pi**2*((-4*(-4*me2**2 + ss**2 + 2*tt**2 + 5*tt*s15 + 3*s15**2 + me2*(-4*ss + 5*&
          &tt + 7*s15 - 5*s35) + ss*(tt - s35) - 4*tt*s35 - 5*s15*s35 + 2*s35**2))/(tt*s35)&
          & - (2*(4*me2**2 + ss**2 + tt**2 + 4*tt*s15 + 3*s15**2 + me2*(-8*ss + 7*tt + 10*s&
          &15 - 2*s25 - 6*s35) - 2*tt*s35 - 4*s15*s35 + s35**2 + ss*(-tt - 2*s15 + s35)))/(&
          &s35*(4*me2 - ss - tt + s35)) + (2*(8*me2**2*(s15 - s35) + me2*(6*tt**2 - 2*ss*s1&
          &5 + 12*tt*s15 + 4*s15**2 + 3*tt*s25 + 2*ss*s35 - 10*tt*s35 - 4*s15*s35 - s25*s35&
          &) + tt*(2*s15**2 - ss*(tt + 2*s15 + s25 - 2*s35) + 2*s15*(tt + s25 - s35) - (tt &
          &+ s25)*(-tt + s35))))/(tt*s25*(4*me2 - ss - tt + s35)) + (2*(-8*me2**2*tt + 2*me&
          &2*(-4*ss*tt + tt**2 + 3*tt*s15 + tt*s35 - s15*s35 + s35**2) + tt*(2*ss**2 - 2*ss&
          &*(-tt + s35) - (tt + s15 - s35)*(-tt + s35))))/(tt**2*s15) + (2*(4*me2**2*tt + m&
          &e2*(tt*(-8*ss + tt - 4*s25) + s15*(tt - s35) + 5*tt*s35 + s35**2) + tt*(ss**2 - &
          &s15**2 - ss*s35 + s25*(-tt + s35) + s15*(-2*s25 + s35))))/(tt*s15*(4*me2 - ss - &
          &tt + s25)) + (-16*me2**2*(s15 - s35) - 2*me2*(-6*tt**2 - 2*ss*s15 + tt*s15 + 4*s&
          &15**2 + tt*s25 + 2*ss*s35 + 3*tt*s35 - 5*s15*s35 - s25*s35 + s35**2) + tt*(4*s15&
          &**2 + s15*(7*tt - 6*s35) - (2*tt + s25 - 2*s35)*(-tt + s35) + ss*(-2*tt - s15 + &
          &2*s25 + s35)))/(tt*(4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + (-16*me2**2*(s15&
          & - s35) + 4*me2*(tt*(4*tt + 3*s15 - 4*s35) + 2*ss*(s15 - s35) + s25*s35) + tt*(4&
          &*s15**2 + s15*(11*tt - 4*s25 - 6*s35) + ss*(s15 - s35) + (-tt + s35)*(-6*tt + 3*&
          &s25 + 2*s35)))/(tt*(tt + s15 - s35)*(s15 + s25 - s35)) + (16*me2**2*(s15 - s35) &
          &- 4*me2*(-4*tt**2 + 2*ss*(s15 - s35) + 3*tt*s35 + s25*s35 + s35**2 - s15*(2*tt +&
          & s35)) + tt*(8*s15**2 + s15*(13*tt + 4*s25 - 12*s35) + ss*(-s15 + s35) + (-tt + &
          &s35)*(-6*tt - 3*s25 + 4*s35)))/(tt*s25*(tt + s15 - s35)))*pvc23(1) + (64*Pi**2*(&
          &-((tt*(-10*tt**3 - 18*tt**2*s15 - 9*tt*s15**2 + 6*tt**2*s25 + 9*tt*s15*s25 + 8*s&
          &15**2*s25 + 4*tt*s25**2 + 8*s15*s25**2 - 4*me2*(2*tt + 2*s15 + 3*s25 - 2*s35)*(2&
          &*tt + s15 - s35) + 16*tt**2*s35 + 17*tt*s15*s35 - 9*tt*s25*s35 - 15*s15*s25*s35 &
          &- 6*s25**2*s35 - 8*tt*s35**2 + 7*s25*s35**2 + ss*(s15**2 + 8*tt*s25 + 4*s25**2 +&
          & 2*s15*(tt + 3*s25 - s35) - 2*tt*s35 - 6*s25*s35 + s35**2)))/((tt + s15 - s35)*(&
          &s15 + s25 - s35))) + (2*(tt**3 - 4*tt*s15**2 + tt*s15*s25 + 8*s15**2*s25 + 4*tt*&
          &s25**2 + 8*s15*s25**2 + 8*me2**2*(s15 + 2*s25 - s35) - ss*(3*s15 - 2*s35)*(s15 +&
          & 2*s25 - s35) - 4*tt**2*s35 + tt*s15*s35 - 2*tt*s25*s35 - 9*s15*s25*s35 - 4*s25*&
          &*2*s35 + 2*tt*s35**2 + 2*s25*s35**2 + 2*me2*(4*tt**2 + 3*s15**2 - 2*tt*s25 + 4*s&
          &25**2 + s15*(tt + 10*s25 - 5*s35) - 2*ss*(s15 + 2*s25 - s35) - 3*tt*s35 - 6*s25*&
          &s35 + 2*s35**2)))/s15 + (2*tt*(-(tt**2*s15) - 3*tt*s15**2 - 2*tt**2*s25 - 2*tt*s&
          &15*s25 + 4*s15**2*s25 + 2*tt*s25**2 + 4*s15*s25**2 + 12*me2**2*(s15 + 2*s25 - s3&
          &5) + 2*tt*s15*s35 - 4*s15*s25*s35 - 2*s25**2*s35 + s25*s35**2 - ss*(tt**2 + s15*&
          &*2 + 3*s15*s25 - tt*s35 - 2*s15*s35 - 2*s25*s35 + s35**2) + me2*(6*tt**2 - 2*tt*&
          &s15 + 5*s15**2 - 8*tt*s25 + 16*s15*s25 + 4*s25**2 - 2*ss*(s15 + 2*s25 - s35) - 2&
          &*tt*s35 - 8*s15*s35 - 8*s25*s35 + 3*s35**2)))/(s15*(4*me2 - ss - tt + s25)) - (2&
          &*(3*tt**3 + 9*tt**2*s15 + 4*tt*s15**2 - 2*tt**2*s25 - 4*tt*s15*s25 + 8*me2**2*(s&
          &15 + 2*s25 - s35) - ss*(2*tt + 4*s15 - 3*s35)*(s15 + 2*s25 - s35) - 9*tt**2*s35 &
          &- 11*tt*s15*s35 + 5*tt*s25*s35 + 4*s15*s25*s35 + 6*tt*s35**2 - 3*s25*s35**2 + 2*&
          &me2*(4*tt**2 + 5*tt*s15 + 4*s15**2 + 2*tt*s25 + 8*s15*s25 - 2*ss*(s15 + 2*s25 - &
          &s35) - 7*tt*s35 - 7*s15*s35 - 4*s25*s35 + 3*s35**2)))/s35 - (2*tt*(tt**3 + 3*tt*&
          &*2*s15 + tt*s15**2 - 3*tt**2*s25 - 4*tt*s15*s25 + 12*me2**2*(s15 + 2*s25 - s35) &
          &- 2*tt**2*s35 - 3*tt*s15*s35 + 4*tt*s25*s35 + 2*s15*s25*s35 + tt*s35**2 - s25*s3&
          &5**2 - ss*(tt**2 + 2*s15**2 + 2*tt*s25 + 2*s15*(tt + 2*s25 - 2*s35) - 3*tt*s35 -&
          & 3*s25*s35 + 2*s35**2) + me2*(6*tt**2 + 9*s15**2 + 4*tt*s25 + 2*s15*(4*tt + 9*s2&
          &5 - 8*s35) - 2*ss*(s15 + 2*s25 - s35) - 12*tt*s35 - 10*s25*s35 + 7*s35**2)))/(s3&
          &5*(4*me2 - ss - tt + s35)) + (tt*(10*tt**3 + 18*tt**2*s15 + 13*tt*s15**2 - 2*tt*&
          &*2*s25 + 3*tt*s15*s25 - 8*s15**2*s25 - 8*s15*s25**2 - 16*tt**2*s35 - 19*tt*s15*s&
          &35 + 5*tt*s25*s35 + 11*s15*s25*s35 + 2*s25**2*s35 + 6*tt*s35**2 - 3*s25*s35**2 -&
          & 4*me2*(-4*tt**2 - 6*tt*s15 - 2*tt*s25 + 5*s15*s25 + 4*s25**2 + 6*tt*s35 - 5*s25&
          &*s35) - ss*(s15**2 + 8*tt*s25 - 4*s25**2 - 6*tt*s35 + 2*s25*s35 + s35**2 - 2*s15&
          &*(-3*tt + s25 + s35))))/(s25*(tt + s15 - s35)) + (tt*(4*tt**3 + 10*tt**2*s15 + 5&
          &*tt*s15**2 - 2*tt**2*s25 - 5*tt*s15*s25 - 4*s15**2*s25 - 2*tt*s25**2 - 4*s15*s25&
          &**2 - 8*tt**2*s35 - 9*tt*s15*s35 + 5*tt*s25*s35 + 7*s15*s25*s35 + 2*s25**2*s35 +&
          & 4*tt*s35**2 - 3*s25*s35**2 - ss*(2*tt**2 + 4*tt*s15 + 3*s15**2 + 4*tt*s25 + 6*s&
          &15*s25 + 2*s25**2 - 4*tt*s35 - 6*s15*s35 - 6*s25*s35 + 3*s35**2) + 2*me2*(3*s15*&
          &*2 + s15*(8*tt + 12*s25 - 7*s35) + 2*(3*tt**2 + 7*tt*s25 + 2*s25**2 - 5*tt*s35 -&
          & 6*s25*s35 + 2*s35**2))))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + (2*tt*(2&
          &*tt**3 + 3*tt**2*s15 + 3*tt*s15**2 + 2*tt*s15*s25 - 2*s15**2*s25 - 2*s15*s25**2 &
          &- 2*tt**2*s35 - 3*tt*s15*s35 + 2*s15*s25*s35 + s25**2*s35 + ss*(s25**2 + s15*(-3&
          &*tt + 2*s25) - 2*s25*(tt + s35) + tt*(-tt + 3*s35)) + me2*(3*s15**2 + s15*(18*tt&
          & - 10*s25 - 5*s35) + 2*(3*tt**2 - 4*s25**2 - 8*tt*s35 + s35**2 + 5*s25*(tt + s35&
          &)))))/(s25*(4*me2 - ss - tt + s35)))*pvc23(2))/tt**2 + 128*Pi**2*(-((s15**2 + tt&
          &*(ss + s25) + me2*(-6*tt + s15 + 6*s25 - 3*s35) + s15*(s25 - s35))/(s15*(4*me2 -&
          & ss - tt + s25))) - (tt**2 + 5*tt*s15 + 3*s15**2 - ss*(tt + s15 - s35) + me2*(6*&
          &tt + 3*s15 - 6*s25 - s35) - 2*tt*s35 - 4*s15*s35 + s35**2)/(s35*(4*me2 - ss - tt&
          & + s35)) + (8*me2*tt + 5*tt**2 + s15**2 - 4*me2*s25 - 4*tt*s25 + s15*(7*tt - 5*s&
          &25 - 2*s35) + ss*(s15 - s35) - 4*tt*s35 + 2*s25*s35 + s35**2)/((tt + s15 - s35)*&
          &(s15 + s25 - s35)) - (6*s15**2 + s15*(11*tt - 9*s35) + me2*(8*tt + 8*s15 - 4*s35&
          &) + 3*(-tt + s35)**2)/(tt*s35) + (-s15**2 + 4*me2*(2*tt + s15) + tt*(tt - 2*s35)&
          & + s15*(tt + s35))/(tt*s15) + (5*tt**2 + 10*tt*s15 + 6*s15**2 + 4*tt*s25 + 5*s15&
          &*s25 + 4*me2*(2*tt + 2*s15 + s25 - 2*s35) - 7*tt*s35 - 8*s15*s35 - 2*s25*s35 + 2&
          &*s35**2 + ss*(-s15 + s35))/(s25*(tt + s15 - s35)) + (tt*(2*tt**2 + 4*tt*s15 + 3*&
          &s15**2 + 3*tt*s25 + 3*s15*s25 - ss*(tt + 2*s15 + s25 - 2*s35) - 2*tt*s35 - 3*s15&
          &*s35 - s25*s35) + 2*me2*(s15**2 + tt*(3*tt + 2*s25 - 5*s35) - s15*(-6*tt + s35))&
          &)/(tt*s25*(4*me2 - ss - tt + s35)) + (tt*(-(ss*tt) + 2*tt**2 + s15**2 + ss*s25 +&
          & s15*(5*tt - 2*s25 - 2*s35) - 3*tt*s35 + s35**2) - 2*me2*(s15**2 + tt*(-3*tt + 2&
          &*s25 - s35) - s15*(-2*tt + s35)))/(tt*(4*me2 - ss - tt + s25)*(s15 + s25 - s35))&
          &)*pvc23(3) + (256*Pi**2*((tt*(24*me2**2 - 2*me2*(8*ss + 3*tt - 5*s15 - 8*s25) + &
          &(2*ss + 3*tt - 2*s25)*(ss - s15 - s25)))/(s15*(4*me2 - ss - tt + s25)) - (-16*me&
          &2**2 - 4*ss**2 + tt**2 + 5*tt*s15 + 4*me2*(4*ss - 3*s15 - 4*s25) + 4*tt*s25 - 4*&
          &s15*s25 - 4*s25**2 + 4*ss*(-tt + s15 + 2*s25))/s15 + (tt*(16*me2**2 - 4*me2*(2*s&
          &s + tt) + 2*ss*s25 - (-tt + s25)*(tt + 2*s15 + 2*s25)))/((tt + s15 - s35)*(s15 +&
          & s25 - s35)) + (tt*(24*me2**2 + tt**2 + 2*tt*s15 + ss*s25 + tt*s25 - s15*s25 - s&
          &25**2 - 2*me2*(2*ss + 4*tt + s15 - s35) - tt*s35))/((4*me2 - ss - tt + s25)*(s15&
          & + s25 - s35)) + (tt*(-16*me2**2 + tt**2 + 3*tt*s15 + 4*me2*(2*ss + tt - 2*s15 -&
          & 2*s25) + tt*s25 - 2*s15*s25 - 2*s25**2 + 2*ss*(s15 + s25 - s35) - tt*s35 + 2*s2&
          &5*s35))/(s25*(tt + s15 - s35)) + (tt*(-24*me2**2 - 2*ss**2 + tt*s15 + 2*me2*(8*s&
          &s + 3*tt - 3*s15 - 6*s25) + 2*tt*s25 - s25*s35 + ss*(-3*tt + s15 + 2*s25 + s35))&
          &)/(s35*(4*me2 - ss - tt + s35)) + (-16*me2**2 - 4*ss**2 + tt**2 + tt*s15 + 4*me2&
          &*(4*ss - s15 - 2*s25) + 2*tt*s25 - 2*s25*s35 + 2*ss*(-2*tt + s15 + 2*s25 + s35))&
          &/s35 + (tt*(-24*me2**2 + tt*s15 + tt*s25 - s15*s25 - s25**2 + ss*(s15 + s25 - s3&
          &5) + s25*s35 + me2*(4*ss - 2*(-4*tt + s15 + 2*s25 + s35))))/(s25*(4*me2 - ss - t&
          &t + s35)))*pvc23(4))/tt**2 + (128*Pi**2*(-((tt*(-tt + s25)*(2*tt**2 + 3*tt*s15 +&
          & 3*s15**2 + 5*s15*s25 + 2*s25**2 + 4*me2*(s15 + 2*s25 - s35) - 2*ss*(s15 + 2*s25&
          & - s35) - 3*tt*s35 - 4*s15*s35 - s25*s35 + s35**2))/(s25*(tt + s15 - s35))) - (t&
          &t**2*s15 + tt*s15**2 - 2*tt**2*s25 + 2*tt*s25**2 + 4*me2*(tt + s15 - s35)*(s15 +&
          & 2*s25 - s35) - 2*ss*(tt + s15 - s35)*(s15 + 2*s25 - s35) - tt**2*s35 - 2*tt*s15&
          &*s35 + 2*tt*s25*s35 - 2*s25**2*s35 + tt*s35**2)/s35 + (tt*(-2*tt**2*s15 - 2*tt*s&
          &15**2 - 2*tt**2*s25 + tt*s15*s25 + 3*s15**2*s25 + 3*tt*s25**2 + 3*s15*s25**2 + 6&
          &*me2*(tt + s15 - s35)*(s15 + 2*s25 - s35) - ss*(tt + s15 - s35)*(s15 + 2*s25 - s&
          &35) + 2*tt*s15*s35 - 4*s15*s25*s35 - 2*s25**2*s35 + s25*s35**2))/(s15*(4*me2 - s&
          &s - tt + s25)) + (-3*tt**2*s15 - 3*tt*s15**2 - 2*tt**2*s25 + 4*tt*s15*s25 + 6*s1&
          &5**2*s25 + 6*tt*s25**2 + 6*s15*s25**2 + 4*me2*(tt + s15 - s35)*(s15 + 2*s25 - s3&
          &5) - 2*ss*(tt + s15 - s35)*(s15 + 2*s25 - s35) - tt**2*s35 + 2*tt*s15*s35 - 2*tt&
          &*s25*s35 - 8*s15*s25*s35 - 4*s25**2*s35 + tt*s35**2 + 2*s25*s35**2)/s15 + (tt*(-&
          &tt + s25)*(tt**2 + tt*s15 + s15**2 + 2*s15*s25 + s25**2 + 6*me2*(s15 + 2*s25 - s&
          &35) - tt*s35 - s15*s35 + ss*(-s15 - 2*s25 + s35)))/(s25*(-4*me2 + ss + tt - s35)&
          &) + (tt*(tt + s15 + s25 - s35)*(6*me2*(s15 + 2*s25 - s35) + ss*(-s15 - 2*s25 + s&
          &35) + (-tt + s25)*(-tt - s15 + s25 + s35)))/((4*me2 - ss - tt + s25)*(s15 + s25 &
          &- s35)) + (tt*(tt + s15 + s25 - s35)*(4*me2*(s15 + 2*s25 - s35) - 2*ss*(s15 + 2*&
          &s25 - s35) + (-tt + s25)*(-2*tt - s15 + 2*s25 + s35)))/((tt + s15 - s35)*(s15 + &
          &s25 - s35)) + (tt*(-6*me2*(tt + s15 - s35)*(s15 + 2*s25 - s35) + ss*(tt + s15 - &
          &s35)*(s15 + 2*s25 - s35) + s25*(tt*(2*tt + s15 - 2*s35) + s25*(-tt + s35))))/(s3&
          &5*(4*me2 - ss - tt + s35)))*pvc23(5))/tt**2 + (128*Pi**2*((-3*tt**2*s15 - 3*tt*s&
          &15**2 - 2*tt**2*s25 + 2*tt*s15*s25 + 4*s15**2*s25 + 4*tt*s25**2 + 4*s15*s25**2 +&
          & 4*me2*(tt + s15)*(s15 + 2*s25 - s35) - 2*ss*(tt + s15)*(s15 + 2*s25 - s35) - tt&
          &**2*s35 + tt*s15*s35 - 2*tt*s25*s35 - 2*s15*s25*s35)/s15 + (-5*tt**2*s15 - 3*tt*&
          &s15**2 + 2*tt**2*s25 + 2*tt*s15*s25 - 4*me2*(tt + s15)*(s15 + 2*s25 - s35) + 2*s&
          &s*(tt + s15)*(s15 + 2*s25 - s35) + tt**2*s35 + 5*tt*s15*s35 - 2*tt*s25*s35 - 2*s&
          &15*s25*s35)/s35 - (tt*(2*tt**2*s15 + tt*s15**2 - 2*tt**2*s25 - 2*tt*s15*s25 + 6*&
          &me2*(tt + s15)*(s15 + 2*s25 - s35) - ss*(tt + s15)*(s15 + 2*s25 - s35) - 2*tt*s1&
          &5*s35 + tt*s25*s35 + s15*s25*s35))/(s35*(4*me2 - ss - tt + s35)) - (tt*(2*tt**2*&
          &s15 + 2*tt*s15**2 + 2*tt**2*s25 - 2*s15**2*s25 - 2*tt*s25**2 - 2*s15*s25**2 - 6*&
          &me2*(tt + s15)*(s15 + 2*s25 - s35) + ss*(tt + s15)*(s15 + 2*s25 - s35) - tt*s15*&
          &s35 + tt*s25*s35 + s15*s25*s35))/(s15*(4*me2 - ss - tt + s25)) - (tt*(s15**2*(-2&
          &*tt + s25) - tt*(-((-2*tt + s25)*(tt + s25 - s35)) + 6*me2*(2*s25 - s35) + ss*(-&
          &2*s25 + s35)) + s15*(s25**2 - s25*(tt + s35) + tt*(-6*me2 + ss - 3*tt + 2*s35)))&
          &)/(s25*(4*me2 - ss - tt + s35)) + (tt*(s15**2*(5*tt - 2*s25) + tt*(4*tt**2 - 4*s&
          &s*s25 + me2*(8*s25 - 4*s35) + 2*ss*s35 - 5*tt*s35 + s25*s35 + s35**2) + s15*(-2*&
          &s25**2 + tt*(4*me2 - 2*ss + 7*tt - 6*s35) + s25*(3*tt + 2*s35))))/(s25*(tt + s15&
          & - s35)) - (tt*(s15**2*(-2*tt + s25) - tt*(2*tt**2 - 2*ss*s25 - tt*s25 + s25**2 &
          &+ 6*me2*(2*s25 - s35) + ss*s35 - 3*tt*s35 + s35**2) + s15*(s25**2 - s25*s35 + tt&
          &*(-6*me2 + ss - 4*tt + 3*s35))))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) - (&
          &tt*(2*s15**2*(-tt + s25) + s15*(2*s25**2 + s25*(3*tt - 2*s35) + tt*(-4*me2 + 2*s&
          &s - 5*tt + 2*s35)) + tt*(ss*(4*s25 - 2*s35) + (tt - s25)*(-4*tt + 3*s35) + me2*(&
          &-8*s25 + 4*s35))))/((tt + s15 - s35)*(s15 + s25 - s35)))*pvc23(6))/tt**2 + 128*P&
          &i**2*((-2*(tt + s15)*(-tt + s25))/((tt + s15 - s35)*(s15 + s25 - s35)) + (2*(tt &
          &+ s15)*(tt + s15 + s25 - s35))/(s25*(tt + s15 - s35)) - (2*s15*(2*tt + s15 - s35&
          &))/(tt*s35) + (s15**2 + s15*(2*tt + s25 - s35) + tt*(tt + 2*s25 - s35))/(s25*(4*&
          &me2 - ss - tt + s35)) - (s15*(2*tt + s15 - s35))/(s35*(4*me2 - ss - tt + s35)) +&
          & (tt**2 + 2*tt*s15 - s15*s25 - tt*s35)/((4*me2 - ss - tt + s25)*(s15 + s25 - s35&
          &)))*pvc23(7) + (64*Pi**2*(-((me2*tt*(-48*me2**2 + tt*s15 + 2*s25**2 + 4*me2*(2*s&
          &s + 3*tt + 4*s15 + s25 - 5*s35) + 2*tt*s35 + 2*s15*s35 - 2*s35**2 - s25*(tt + s3&
          &5) + ss*(-4*tt - 3*s15 + 3*s35)))/(s25*(4*me2 - ss - tt + s35))) - (2*tt*(24*me2&
          &**3 - 2*me2**2*(8*ss + 5*tt - 4*s15 - 5*s25) + ss*(ss - s15 - s25)*(tt + s15 - s&
          &35) + me2*(2*ss**2 + 3*tt**2 + 3*s15**2 - tt*s25 + s25**2 - ss*(tt + 6*s15 + 3*s&
          &25 - 4*s35) + 3*s15*(tt + s25 - s35) - 3*tt*s35)))/(s15*(4*me2 - ss - tt + s25))&
          & + (32*me2**3 + 4*(ss**2 + ss*(tt - s35) - tt*(tt + s15 - s35))*(tt + s15 - s35)&
          & - 4*me2**2*(8*ss - 4*tt - s15 - 2*s25 + s35) + 2*me2*(4*ss**2 + tt**2 - tt*s15 &
          &- tt*s25 - ss*(4*tt + 9*s15 + 2*s25 - 5*s35) + 2*tt*s35 + 4*s15*s35 + s25*s35 - &
          &4*s35**2))/s35 - (2*tt*(-24*me2**3 + 2*me2**2*(8*ss - tt - 2*s15 - 3*s25) + me2*&
          &(-2*ss**2 + tt*s15 + tt*s25 + ss*(3*tt + 5*s15 + s25 - 3*s35) - 2*tt*s35 - 3*s15&
          &*s35 - 2*s25*s35 + 3*s35**2) + (tt + s15 - s35)*(-ss**2 + tt*(tt + s15 - s35) + &
          &ss*(-tt + s35))))/(s35*(4*me2 - ss - tt + s35)) + (-32*me2**3 + 4*me2**2*(8*ss +&
          & 4*tt - 5*s15 - 6*s25 + s35) - 2*me2*(4*ss**2 + tt**2 + 4*s15**2 + tt*s25 + 2*s2&
          &5**2 - ss*(4*tt + 11*s15 + 6*s25 - 7*s35) + 2*s15*(tt + 3*s25 - 2*s35) - tt*s35 &
          &- 3*s25*s35) + 2*(tt + s15 - s35)*(-2*ss**2 + (-tt + s25)*(tt - s35) + ss*(-2*tt&
          & + s15 + 2*s25 + s35)))/s15 + (tt*(-48*me2**3 + 2*tt**3 + 4*tt**2*s15 + tt*s15**&
          &2 - tt*s15*s25 - 4*tt**2*s35 - 4*tt*s15*s35 + s15*s25*s35 + 2*tt*s35**2 + ss**2*&
          &(-4*tt - 3*s15 + 3*s35) + 4*me2**2*(2*ss - 7*tt - 3*s15 - 3*s25 + 4*s35) + me2*(&
          &2*tt**2 - 13*tt*s15 - 6*s15**2 - 11*tt*s25 - 2*s15*s25 + ss*(32*tt + 21*s15 - 21&
          &*s35) - 2*tt*s35 + 4*s15*s35 + 7*s25*s35 + 2*s35**2) + ss*(s15**2 + s15*(tt + s3&
          &5) - (-tt + s35)*(-2*tt + s25 + 2*s35))))/((4*me2 - ss - tt + s25)*(s15 + s25 - &
          &s35)) + (tt*(32*me2**3 + (tt*s15 + s25*(tt - s35))*(tt + s15 - s35) - 4*me2**2*(&
          &4*ss - 4*tt - 5*s15 - 2*s25 + s35) + ss**2*(-4*tt - 3*s15 + 3*s35) + 2*me2*(-3*t&
          &t**2 + 4*tt*s25 + s15*s25 - s25**2 + ss*(2*tt + 3*s15 - 3*s35) + tt*s35 - s15*s3&
          &5 - s25*s35 + s35**2) - ss*(s15**2 + s15*(6*tt - 4*s35) + (-tt + s35)*(-6*tt + s&
          &25 + 3*s35))))/(s25*(tt + s15 - s35)) + (tt*(-32*me2**3 + 2*tt**3 + 4*tt**2*s15 &
          &+ tt*s15**2 - tt*s15*s25 - 4*tt**2*s35 - 4*tt*s15*s35 + s15*s25*s35 + 2*tt*s35**&
          &2 + ss**2*(-4*tt - 3*s15 + 3*s35) + 4*me2**2*(4*ss - 4*tt - 3*s15 - 2*s25 + 3*s3&
          &5) + ss*(s15**2 + s15*(tt + s35) - (-tt + s35)*(-2*tt + s25 + 2*s35)) - 2*me2*(t&
          &t**2 + 9*tt*s15 + 4*s15**2 + 6*tt*s25 + 3*s15*s25 + s25**2 - 2*tt*s35 - 4*s15*s3&
          &5 - 5*s25*s35 + ss*(-14*tt - 9*s15 + 9*s35))))/((tt + s15 - s35)*(s15 + s25 - s3&
          &5)))*pvc8(1))/tt**2 + (64*Pi**2*((2*(-16*me2**3 + (-2*ss**2 - tt**2 + tt*s15 + 2&
          &*tt*s25 - 2*s15*s25 - 2*s25**2 + 2*ss*(-tt + s15 + 2*s25))*(tt + s15 - s35) + 2*&
          &me2**2*(8*ss + 2*tt - 3*s15 - 2*s25 + s35) + me2*(-4*ss**2 + tt**2 - 4*tt*s15 - &
          &6*s15**2 - 7*tt*s25 - 6*s15*s25 + 2*s25**2 + ss*(4*tt + 9*s15 + 2*s25 - 5*s35) +&
          & tt*s35 + 6*s15*s35 + 5*s25*s35)))/s15 + (2*tt*(-24*me2**3 - (ss + tt - s25)*(ss&
          & + tt - s35)*(tt + s15 - s35) + 2*me2**2*(8*ss - 2*tt - 9*s15 - 9*s25 + 4*s35) +&
          & me2*(-2*ss**2 + 5*tt**2 + s15**2 + tt*s25 - 3*s25**2 + ss*(5*tt + 8*s15 + 5*s25&
          & - 6*s35) + s15*(7*tt - 3*s25 - 3*s35) - 9*tt*s35 + 4*s25*s35 + 2*s35**2)))/(s35&
          &*(4*me2 - ss - tt + s35)) - (tt*(32*me2**3 - 4*tt**3 - 6*tt**2*s15 - 3*tt*s15**2&
          & + 2*tt**2*s25 + 2*tt*s15*s25 + 2*s15**2*s25 + tt*s25**2 + 2*s15*s25**2 - 4*me2*&
          &*2*(8*ss + 4*tt - 3*s15 - 6*s25 - s35) + 6*tt**2*s35 + 4*tt*s15*s35 - 4*tt*s25*s&
          &35 - 3*s15*s25*s35 - s25**2*s35 - 2*tt*s35**2 + 2*s25*s35**2 + ss**2*(-4*tt - 3*&
          &s15 + 3*s35) + 2*me2*(4*ss**2 + 2*tt**2 - 3*tt*s15 - s15**2 - 3*tt*s25 + 3*s15*s&
          &25 + 2*s25**2 + ss*(8*tt - 6*s25 - 4*s35) - 4*tt*s35 - s15*s35 + 2*s35**2) + ss*&
          &(-4*tt**2 + s15**2 + 3*tt*s25 + 6*tt*s35 - 2*s25*s35 - 2*s35**2 + s15*(-tt + s25&
          & + s35))))/((tt + s15 - s35)*(s15 + s25 - s35)) - (tt*(48*me2**3 - 2*tt**3 - 4*t&
          &t**2*s15 - 3*tt*s15**2 + 2*s15**2*s25 + tt*s25**2 + 2*s15*s25**2 - 4*me2**2*(8*s&
          &s + 4*tt + s15 - 4*s25 - 5*s35) + 4*tt**2*s35 + 4*tt*s15*s35 - 2*tt*s25*s35 - 3*&
          &s15*s25*s35 - s25**2*s35 - 2*tt*s35**2 + 2*s25*s35**2 + ss**2*(-4*tt - 3*s15 + 3&
          &*s35) + 2*me2*(2*ss**2 + tt**2 - 2*s15**2 + s15*(tt - 4*s25) - 2*s25**2 + ss*(6*&
          &tt + 6*s15 - 8*s35) - 5*tt*s35 + 4*s25*s35 + 2*s35**2) + ss*(s15**2 + s25*(3*tt &
          &- 2*s35) - 2*(-tt + s35)**2 + s15*(tt + s25 + s35))))/((4*me2 - ss - tt + s25)*(&
          &s15 + s25 - s35)) - (2*tt*(24*me2**3 - 2*me2**2*(8*ss + 10*tt - 5*s25 - 3*s35) +&
          & (ss - s25)*(ss - s15 - s25)*(tt + s15 - s35) + me2*(2*ss**2 + 5*tt**2 + 2*s15**&
          &2 - 2*tt*s25 - s25**2 + s15*(3*tt + s25 - 2*s35) - 5*tt*s35 + s25*s35 - ss*(-5*t&
          &t + s15 + s25 + s35))))/(s15*(4*me2 - ss - tt + s25)) - (2*(16*me2**3 + me2**2*(&
          &-16*ss + 4*tt + 22*s15 + 20*s25 - 10*s35) + me2*(4*ss**2 - 3*tt**2 - 6*tt*s15 + &
          &2*s15**2 - tt*s25 + 10*s15*s25 + 6*s25**2 - ss*(4*tt + 13*s15 + 10*s25 - 9*s35) &
          &+ 7*tt*s35 - 5*s25*s35 - 2*s35**2) - (tt + s15 - s35)*(-2*ss**2 + (-tt + s25)*(t&
          &t - s35) + ss*(-2*tt + s15 + 2*s25 + s35))))/s35 - (2*tt*(24*me2**3 + tt*(ss - s&
          &15 - s25)*(tt + s15 - s35) + 4*me2**2*(-4*ss - 2*tt + s15 + 5*s25 + s35) + me2*(&
          &2*ss**2 - 3*tt**2 - 2*s15**2 - 3*tt*s25 + 4*s25**2 + 2*tt*s35 + 2*s25*s35 - 2*ss&
          &*(-tt + 3*s25 + s35) + s15*(-5*tt + s25 + 2*s35))))/(s25*(4*me2 - ss - tt + s35)&
          &) - (tt*(32*me2**3 - 4*tt**3 - 7*tt**2*s15 - 4*tt*s15**2 + tt**2*s25 + tt*s15*s2&
          &5 + 2*s15**2*s25 + tt*s25**2 + 2*s15*s25**2 - 4*me2**2*(8*ss + 12*tt + 3*s15 - 6&
          &*s25 - 7*s35) + 6*tt**2*s35 + 6*tt*s15*s35 - 3*tt*s25*s35 - 4*s15*s25*s35 - s25*&
          &*2*s35 - 2*tt*s35**2 + 2*s25*s35**2 + ss**2*(-4*tt - 3*s15 + 3*s35) + 2*me2*(4*s&
          &s**2 + 6*tt**2 - 2*s15**2 - 9*tt*s25 + 2*s25**2 - s15*(2*tt + s25) + 2*ss*(8*tt &
          &+ 3*s15 - 3*s25 - 5*s35) - 7*tt*s35 + 6*s25*s35 + 2*s35**2) + ss*(-4*tt**2 + 3*t&
          &t*s25 + 5*tt*s35 - 2*s25*s35 - 2*s35**2 + s15*(-2*tt + s25 + 2*s35))))/(s25*(tt &
          &+ s15 - s35)))*pvc8(2))/tt**2 + (64*Pi**2*(-((tt*(-6*tt**3 - 9*tt**2*s15 - 3*tt*&
          &s15**2 + 3*tt**2*s25 + 4*tt*s15*s25 + 2*s15**2*s25 + tt*s25**2 + 2*s15*s25**2 + &
          &16*me2**2*(tt + s15 + s25 - s35) - 2*me2*(4*ss + 4*tt + s15 - 2*s25 - s35)*(tt +&
          & s15 + s25 - s35) + 10*tt**2*s35 + 7*tt*s15*s35 - 6*tt*s25*s35 - 5*s15*s25*s35 -&
          & s25**2*s35 - 4*tt*s35**2 + 3*s25*s35**2 + ss*(s15**2 + 4*tt*s25 + s15*(tt + 3*s&
          &25 - 2*s35) - tt*s35 - 3*s25*s35 + s35**2)))/((tt + s15 - s35)*(s15 + s25 - s35)&
          &)) - (tt*(-4*tt**3 - 7*tt**2*s15 - 3*tt*s15**2 + tt**2*s25 + 2*tt*s15*s25 + 2*s1&
          &5**2*s25 + tt*s25**2 + 2*s15*s25**2 + 8*me2**2*(tt + 2*s15 + 3*s25 - 2*s35) + 8*&
          &tt**2*s35 + 7*tt*s15*s35 - 4*tt*s25*s35 - 5*s15*s25*s35 - s25**2*s35 - 4*tt*s35*&
          &*2 + 3*s25*s35**2 + ss*(s15**2 + 4*tt*s25 + s15*(tt + 3*s25 - 2*s35) - tt*s35 - &
          &3*s25*s35 + s35**2) - 4*me2*(s15**2 + tt*s25 + 3*s15*s25 + s25**2 + ss*(tt + s15&
          & + s25 - s35) - 2*s15*s35 - 3*s25*s35 + s35**2)))/((4*me2 - ss - tt + s25)*(s15 &
          &+ s25 - s35)) + (2*tt*((-tt + s25)*(ss + tt - s35)*(tt + s15 - s35) - 12*me2**2*&
          &(tt + s15 + s25 - s35) + me2*(4*tt**2 + s15**2 + tt*s25 - 3*s25**2 + ss*(4*tt + &
          &3*s15 + 2*s25 - 3*s35) + s15*(6*tt - 3*s25 - 2*s35) - 7*tt*s35 + 4*s25*s35 + s35&
          &**2)))/(s35*(4*me2 - ss - tt + s35)) - (2*(-(((-tt + s25)*(tt - s35) + ss*(s15 +&
          & 2*s25 - s35))*(tt + s15 - s35)) + 8*me2**2*(tt + s15 + s25 - s35) + 2*me2*(s15*&
          &*2 + tt*s25 + 3*s25**2 + s15*(-tt + 5*s25 - 2*s35) - 4*s25*s35 + s35**2 + ss*(-s&
          &15 - 2*s25 + s35))))/s35 - (2*(8*me2**2*(-tt + s25) - (tt + s15 - s35)*(-tt**2 +&
          & s15*(tt - 2*s25) + tt*s25 - 2*s25**2 + ss*(s15 + 2*s25 - s35) + s25*s35) + 2*me&
          &2*(2*tt**2 + 2*s15**2 + tt*s25 - s25**2 + s15*(4*tt + s25 - 4*s35) - 5*tt*s35 + &
          &2*s35**2 + ss*(-s15 - 2*s25 + s35))))/s15 - (2*tt*(12*me2**2*(-tt + s25) - (ss -&
          & s25)*(tt + s15 - s35)*(tt + s15 + s25 - s35) + me2*(6*tt**2 + 2*s15**2 - 5*tt*s&
          &25 - s25**2 + ss*(4*tt + s15 - 2*s25 - s35) - 8*tt*s35 + 4*s25*s35 + 2*s35**2 - &
          &s15*(-5*tt + s25 + 4*s35))))/(s15*(4*me2 - ss - tt + s25)) - (2*tt*(-(tt*(tt + s&
          &15 - s35)*(tt + s15 + s25 - s35)) + 4*me2**2*(-tt + s15 + 3*s25 - s35) + me2*(2*&
          &ss*tt - 2*tt**2 - 2*s15**2 - 2*ss*s25 - 2*tt*s25 + 4*s25**2 + 3*tt*s35 + s25*s35&
          & - 2*s35**2 + s15*(-5*tt + s25 + 4*s35))))/(s25*(4*me2 - ss - tt + s35)) - (tt*(&
          &-6*tt**3 - 9*tt**2*s15 - 4*tt*s15**2 + tt**2*s25 + tt*s15*s25 + 2*s15**2*s25 + t&
          &t*s25**2 + 2*s15*s25**2 + 16*me2**2*(-tt + s25) + 10*tt**2*s35 + 8*tt*s15*s35 - &
          &3*tt*s25*s35 - 4*s15*s25*s35 - s25**2*s35 - 4*tt*s35**2 + 2*s25*s35**2 + ss*(2*s&
          &15**2 + 4*tt*s25 + s15*(3*tt + 3*s25 - 4*s35) - 3*tt*s35 - 3*s25*s35 + 2*s35**2)&
          & - 2*me2*(4*s15**2 + 4*ss*(-tt + s25) + s15*(9*tt + 3*s25 - 8*s35) - (2*s25 - s3&
          &5)*(-5*tt + s25 + 4*s35))))/(s25*(tt + s15 - s35)))*pvc8(3))/tt**2 + (256*(2*me2&
          & - tt)*Pi**2*(-2/s25 + (tt + s15 - s35)/(s25*(-4*me2 + ss + tt - s35)) - 2/(s15 &
          &+ s25 - s35) - (tt + s15 - s35)/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)))*pvc&
          &8(4))/tt + (128*Pi**2*(2*me2 - ss - tt + s25)*(2*me2 - ss + s15 + s25)*(-2/s25 +&
          & (tt + s15 - s35)/(s25*(-4*me2 + ss + tt - s35)) - 2/(s15 + s25 - s35) - (tt + s&
          &15 - s35)/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)))*pvc8(5))/tt - (128*Pi**2*&
          &(2*me2*(s15 + 2*s25 - s35) + (-tt + s25)*(tt + 2*s15 + 2*s25 - s35) + ss*(-s15 -&
          & 2*s25 + s35))*(tt**2*s15 - tt*s15**2 + 2*tt**2*s25 - 3*tt*s15*s25 + s15**2*s25 &
          &- 3*tt*s25**2 + s15*s25**2 + 32*me2**2*(s15 + 2*s25 - s35) + 2*ss**2*(s15 + 2*s2&
          &5 - s35) - tt**2*s35 + s15*s25*s35 + 3*s25**2*s35 + tt*s35**2 - 2*s25*s35**2 - s&
          &s*(s15 + 2*s25 - s35)*(-3*tt + s15 + 2*s25 + s35) + 4*me2*(s15 + 2*s25 - s35)*(-&
          &4*ss - 3*tt + s15 + 2*s25 + s35))*pvc8(6))/(tt*s25*(4*me2 - ss - tt + s25)*(s15 &
          &+ s25 - s35)*(4*me2 - ss - tt + s35)) + (128*Pi**2*(-tt + s25)*(-2/s25 + (tt + s&
          &15 - s35)/(s25*(-4*me2 + ss + tt - s35)) - 2/(s15 + s25 - s35) - (tt + s15 - s35&
          &)/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)))*(tt + s15 + s25 - s35)*pvc8(7))/t&
          &t + (64*Pi**2*((-2*tt*(8*me2**3*(tt + 6*s15 - 6*s35) + tt*s15*(-ss + s15 + s25)*&
          &(tt + s15 - s35) - 2*me2**2*(-tt**2 + 2*s15**2 - 5*tt*s25 + ss*(8*tt + 15*s15 - &
          &15*s35) + s15*(4*tt - 3*s25 - 4*s35) - 5*tt*s35 + 15*s25*s35 + 3*s35**2) + me2*(&
          &2*tt**2*s25 + 2*tt*s25**2 + ss**2*(2*tt + 3*s15 - 3*s35) - 2*tt**2*s35 + 2*tt*s2&
          &5*s35 - 3*s25**2*s35 + 2*tt*s35**2 - 3*s25*s35**2 + s15**2*(4*tt + s35) + s15*(5&
          &*tt**2 + 3*tt*s25 - 6*tt*s35 + 2*s25*s35 - s35**2) + ss*(2*s15**2 - 4*tt*s25 + s&
          &15*(4*tt - 3*s25 - 5*s35) - 3*tt*s35 + 6*s25*s35 + 3*s35**2))))/(s15*(4*me2 - ss&
          & - tt + s25)) + (2*(8*me2**3*(-2*tt + 3*s15 - 3*s35) - 2*me2**2*(-6*tt**2 - 8*tt&
          &*s15 + 2*s15**2 + 4*tt*s25 + 4*ss*(2*tt + 3*s15 - 3*s35) - 3*s15*s35 + 6*s25*s35&
          & + 3*s35**2) - tt*(tt + s15 - s35)*(2*ss**2 + 2*s15**2 - 2*ss*(-tt + s35) - 3*s1&
          &5*(-tt + s35) + (-tt + s35)**2) + me2*(-4*tt*s25**2 + ss**2*(4*tt + 6*s15 - 6*s3&
          &5) + s25*(2*tt*(tt - 2*s15) + 5*tt*s35 - 3*s35**2) + tt*(-(tt*s15) - 4*s15**2 - &
          &2*tt*s35 + 4*s15*s35 + s35**2) + ss*(12*tt**2 + 16*tt*s15 + 4*tt*s25 - 20*tt*s35&
          & - 3*s15*s35 + 6*s25*s35 + 3*s35**2))))/s35 - (2*tt*(-8*me2**3*(tt + 6*s15 - 6*s&
          &35) + tt*(ss**2 - ss*s15 + s15*(tt + s15 - s35))*(tt + s15 - s35) + 2*me2**2*(tt&
          &**2 + 5*tt*s15 + 4*s15**2 + 5*tt*s25 + ss*(8*tt + 15*s15 - 15*s35) - 12*tt*s35 -&
          & 7*s15*s35 + 12*s25*s35 + 4*s35**2) - me2*(-tt**3 - tt*s15**2 + 3*tt**2*s25 - 2*&
          &tt*s25**2 + ss**2*(2*tt + 3*s15 - 3*s35) - tt**2*s35 + 4*tt*s25*s35 + tt*s35**2 &
          &- 4*s25*s35**2 + tt*s15*(-2*tt + s35) + s15*s25*(tt + 3*s35) + ss*(8*tt**2 + 11*&
          &tt*s15 + 3*s15**2 + 2*tt*s25 - 14*tt*s35 - 7*s15*s35 + 3*s25*s35 + 4*s35**2))))/&
          &(s35*(4*me2 - ss - tt + s35)) + (tt*(-80*me2**3*(s15 - s35) + 4*me2**2*(2*tt**2 &
          &+ 10*tt*s15 + 8*s15**2 + 8*tt*s25 + 6*ss*(s15 - s35) - 13*tt*s35 - 15*s15*s35 + &
          &3*s25*s35 + 9*s35**2) - me2*(-2*tt**3 + 8*s15**3 + tt**2*s25 - 6*tt*s25**2 + 4*s&
          &s**2*(s15 - s35) + 2*tt**2*s35 + 5*tt*s25*s35 + 2*s25**2*s35 + 2*tt*s35**2 - 2*s&
          &25*s35**2 - 4*s35**3 - 6*s15**2*(-2*tt + 3*s35) + ss*(16*tt**2 + 11*tt*s15 - 2*s&
          &15**2 + 12*tt*s25 + 2*s15*s25 - 17*tt*s35 + 2*s25*s35 + 2*s35**2) + s15*(-3*tt**&
          &2 - 10*tt*s35 + 14*s35**2)) + tt*(ss**2*(2*tt + s15 - s35) + s15*(2*s15**2 + s15&
          &*(5*tt - 4*s35) - (2*tt + s25 - 2*s35)*(-tt + s35)) + ss*(-s15**2 + s25*(tt - s3&
          &5) + s15*(-3*tt + 2*s25 + s35)))))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) +&
          & (tt*(-32*me2**3*(s15 - s35) + 4*me2**2*(-4*tt**2 - s15**2 + 6*tt*s25 + s15*(-4*&
          &tt + s25) + 4*ss*(s15 - s35) + s25*s35 + s35**2) + 2*me2*(2*tt**3 + 2*tt*s15**2 &
          &+ 4*tt*s25**2 - 4*tt**2*s35 - 2*tt*s25*s35 - 3*s25**2*s35 + 3*tt*s35**2 + s25*s3&
          &5**2 + s15*(6*tt**2 + 2*tt*s25 - 5*tt*s35 - s25*s35) + ss*(s15**2 - s15*(4*tt + &
          &3*s25) + (4*tt + 3*s25 - s35)*(-2*tt + s35))) + tt*(2*s15**3 + s15**2*(7*tt - 2*&
          &s25 - 4*s35) + ss**2*(4*tt + 3*s15 - 3*s35) - 2*(-tt + s25)*(-tt + s35)**2 + s15&
          &*(-tt + s35)*(-6*tt + 3*s25 + 2*s35) + ss*(s15**2 - 3*s15*(-tt + s35) + (-tt + s&
          &35)*(-4*tt + s25 + 2*s35)))))/((tt + s15 - s35)*(s15 + s25 - s35)) - (2*(8*me2**&
          &3*(-2*tt + 3*s15 - 3*s35) + tt*s15*(tt + s15)*(tt + s15 - s35) + 2*me2**2*(3*s15&
          &**2 + s15*(6*tt + 6*s25 - 5*s35) - 4*ss*(2*tt + 3*s15 - 3*s35) + 2*(tt**2 + 4*tt&
          &*s25 - tt*s35 - 6*s25*s35)) + me2*(2*tt**3 - 4*tt**2*s25 + 4*tt*s25**2 + ss**2*(&
          &4*tt + 6*s15 - 6*s35) - 2*tt**2*s35 + 6*tt*s25*s35 - 6*s25**2*s35 + 2*s15**2*(-t&
          &t + s35) - s15*(tt**2 - tt*s25 - 5*tt*s35 + 3*s25*s35 + 2*s35**2) + ss*(-3*s15**&
          &2 - 2*(tt - 2*s25)*(-2*tt + 3*s35) + s15*(2*tt - 6*s25 + 3*s35)))))/s15 + (tt*(3&
          &2*me2**3*(s15 - s35) + 4*me2**2*(-4*tt**2 - 4*ss*s15 + 2*tt*s15 + 6*s15**2 + 6*t&
          &t*s25 + 5*s15*s25 + 4*ss*s35 - 2*tt*s35 - 8*s15*s35 - 7*s25*s35 + 2*s35**2) - 2*&
          &me2*(-2*tt**3 + 4*tt**2*s25 - 4*tt*s25**2 - 2*tt**2*s35 - tt*s25*s35 + 3*s25**2*&
          &s35 + 3*tt*s35**2 - 2*s25*s35**2 - 2*s15**2*(tt + s35) + s15*(s25 + s35)*(-3*tt &
          &+ 2*s35) + ss*(8*tt**2 + 4*s15**2 + 6*tt*s25 + s15*(14*tt + 3*s25 - 6*s35) - 12*&
          &tt*s35 - 3*s25*s35 + 2*s35**2)) + tt*(ss**2*(4*tt + 3*s15 - 3*s35) + (tt + s15 -&
          & s35)*(4*s15**2 + s15*(5*tt + 2*s25 - 4*s35) - (2*tt + s25 - 2*s35)*(-tt + s35))&
          & - ss*(s15**2 + 2*s15*(-tt + s35) - (-tt + s35)*(-4*tt + s25 + 3*s35)))))/(s25*(&
          &tt + s15 - s35)) + (tt*(80*me2**3*(s15 - s35) + 2*tt*(ss - s15)*(ss - s15 - s25)&
          &*(tt + s15 - s35) - 4*me2**2*(-2*tt**2 + 6*s15**2 - 2*tt*s25 + s15*(7*tt - 3*s25&
          & - 12*s35) + 6*ss*(s15 - s35) - 4*tt*s35 + 6*s25*s35 + 8*s35**2) + me2*(2*tt**3 &
          &+ 8*s15**3 + 7*tt**2*s25 + 2*tt*s25**2 + s15**2*(26*tt - 16*s35) + 4*ss**2*(s15 &
          &- s35) - 4*tt**2*s35 - 5*tt*s25*s35 - 4*s25**2*s35 + 6*tt*s35**2 - 4*s35**3 + s1&
          &5*(25*tt**2 + 4*tt*s25 - 30*tt*s35 - 2*s25*s35 + 12*s35**2) + ss*(-16*tt**2 - 4*&
          &s15**2 + 13*tt*s35 + 8*s25*s35 + s15*(-11*tt - 4*s25 + 4*s35)))))/(s25*(4*me2 - &
          &ss - tt + s35)))*pvd1(1))/tt**2 + (64*Pi**2*((-2*(64*me2**4 + s15*(-2*ss**2 - (-&
          &tt + s25)*(tt + 4*s15 + 4*s25 - s35) + ss*(-2*tt + 3*s15 + 6*s25 - s35))*(tt + s&
          &15 - s35) - 16*me2**3*(4*ss + 2*tt - 2*s15 - 4*s25 + s35) + 2*me2**2*(8*ss**2 - &
          &5*s15**2 + 2*(-tt + 2*s25)*(-tt + 2*s25 - 2*s35) - 8*ss*(-tt + s15 + 2*s25 - s35&
          &) + s15*(-10*tt + 6*s25 + s35)) + me2*(-2*tt**3 - 6*s15**3 - 2*s15**2*(tt + 7*s2&
          &5 - 4*s35) - 4*ss**2*s35 + 2*tt**2*s35 + 4*tt*s25*s35 - 4*s25**2*s35 + ss*(9*s15&
          &**2 + s15*(8*tt + 2*s25 - 5*s35) - 4*tt*s35 + 8*s25*s35) + s15*(tt**2 - 11*tt*s2&
          &5 - 2*s25**2 + 3*tt*s35 + 9*s25*s35 - 2*s35**2))))/s15 + (2*tt*(96*me2**4 - 4*me&
          &2**3*(16*ss + 20*tt + 3*s15 - 12*s25 - 5*s35) + 2*me2**2*(4*ss**2 + 7*tt**2 - 14&
          &*s15**2 - 19*tt*s25 + ss*(20*tt + 9*s15 - 4*s25 - 5*s35) - 3*tt*s35 + 6*s25*s35 &
          &+ s15*(-13*tt - 12*s25 + 10*s35)) + (tt + s15 - s35)*(tt*s15**2 + tt*s25*(tt - s&
          &35) - s15*s25*(-2*tt + s35) + ss**2*(-2*s15 + s35) + ss*(-tt**2 - 3*tt*s15 + s15&
          &**2 + 2*s15*s25 + tt*s35 - s25*s35)) + me2*(tt**3 - 3*s15**3 - 2*ss**2*(2*tt + s&
          &15) + 3*tt**2*s25 - 2*tt*s25**2 - 3*tt**2*s35 - tt*s25*s35 + s25**2*s35 + tt*s35&
          &**2 + s15**2*(4*tt - 6*s25 + 2*s35) + s15*(9*tt**2 - 6*tt*s25 - 11*tt*s35 + 8*s2&
          &5*s35 + s35**2) + ss*(-2*tt**2 + 13*s15**2 + 6*tt*s25 + s15*(14*tt + 2*s25 - 13*&
          &s35) - tt*s35 - s25*s35 + 2*s35**2))))/(s35*(4*me2 - ss - tt + s35)) + (2*tt*(-9&
          &6*me2**4 - s15*(ss + 2*tt - 2*s25)*(-ss + s15 + s25)*(tt + s15 - s35) + 4*me2**3&
          &*(16*ss + 8*tt - 5*s15 - 16*s25 + 3*s35) - 2*me2**2*(4*ss**2 - tt**2 - 11*s15**2&
          & - 7*tt*s25 + 4*s25**2 - ss*(-4*tt + s15 + 8*s25 - 5*s35) + 8*tt*s35 - 5*s25*s35&
          & - 3*s35**2 + s15*(-14*tt - 3*s25 + 10*s35)) + me2*(3*s15**3 + s15**2*(-3*tt + 9&
          &*s25 - 4*s35) + 2*ss**2*s35 + s35*(2*tt**2 - 5*tt*s25 + 2*s25**2 - 2*tt*s35 + 3*&
          &s25*s35) + s15*(-7*tt**2 + 9*tt*s25 + s25**2 + 7*tt*s35 - 12*s25*s35 + s35**2) -&
          & ss*(2*tt**2 + 10*s15**2 + s15*(13*tt + s25 - 11*s35) - 6*tt*s35 + 4*s25*s35 + 3&
          &*s35**2))))/(s15*(4*me2 - ss - tt + s25)) - (tt*(16*me2**3*(-5*tt + 2*s25) - 2*(&
          &ss**3 + ss**2*(tt - 2*s15 - 2*s25) - (-tt + s15)*(-tt + s25)*(s15 + s25) + ss*(s&
          &15**2 + (-tt + s25)**2 + s15*(-2*tt + 3*s25)))*(tt + s15 - s35) - 4*me2**2*(-7*t&
          &t**2 - 8*s15**2 + 8*tt*s25 - 6*s25**2 + 6*ss*(-tt + s15 + s25 - s35) + 6*tt*s35 &
          &+ 2*s25*s35 - 2*s35**2 + s15*(-5*tt - 10*s25 + 9*s35)) + me2*(4*s15**3 - 5*tt**2&
          &*s25 + 2*tt*s25**2 + 4*s25**3 + s15**2*(-13*tt + 22*s25 - 6*s35) + 4*ss**2*(3*tt&
          & + 4*s15 + s25 - 4*s35) + 4*tt**2*s35 - 3*tt*s25*s35 - 4*s25**2*s35 - 2*tt*s35**&
          &2 + 4*s25*s35**2 + s15*(-17*tt**2 + 9*tt*s25 + 14*s25**2 + 12*tt*s35 - 21*s25*s3&
          &5 + 2*s35**2) - ss*(-6*tt**2 + 25*s15**2 + 16*tt*s25 + 8*s25**2 + s15*(15*tt + 3&
          &2*s25 - 29*s35) - 3*tt*s35 - 20*s25*s35 + 4*s35**2))))/(s25*(4*me2 - ss - tt + s&
          &35)) - (tt*(-(tt*s15**3) + 2*tt**3*s25 + 5*tt**2*s15*s25 + 3*tt*s15**2*s25 + 2*s&
          &15**3*s25 + tt**2*s25**2 + 2*tt*s15*s25**2 + 2*s15**2*s25**2 - 16*me2**3*(7*tt +&
          & 4*s15 + 4*s25 - 4*s35) + 2*ss**3*(tt + s15 - s35) - 4*tt**2*s25*s35 - 6*tt*s15*&
          &s25*s35 - 3*s15**2*s25*s35 - tt*s25**2*s35 - 2*s15*s25**2*s35 + 2*tt*s25*s35**2 &
          &+ 2*s15*s25*s35**2 - ss**2*(7*s15**2 + s15*(8*tt + 4*s25 - 9*s35) - 2*(2*s25 - s&
          &35)*(-tt + s35)) + 4*me2**2*(7*tt**2 - 13*s15**2 - 18*tt*s25 - 10*s25**2 + 2*ss*&
          &(13*tt + 8*s15 + 5*s25 - 8*s35) - 6*tt*s35 + 13*s25*s35 + s35**2 + s15*(-13*tt -&
          & 19*s25 + 11*s35)) - me2*(-2*tt**3 + 8*s15**3 - 9*tt**2*s25 + 10*tt*s25**2 + 4*s&
          &25**3 + s15**2*(tt + 16*s25 - 8*s35) + 4*ss**2*(7*tt + 5*s15 + s25 - 5*s35) + 2*&
          &tt**2*s35 + 7*tt*s25*s35 - 10*s25**2*s35 + 2*tt*s35**2 - 2*s25*s35**2 + s15*(-13&
          &*tt**2 + 13*tt*s25 + 12*s25**2 + 12*tt*s35 - 19*s25*s35) - ss*(-6*tt**2 + 39*s15&
          &**2 + 40*tt*s25 + 8*s25**2 + s15*(45*tt + 34*s25 - 41*s35) + 3*tt*s35 - 30*s25*s&
          &35 + 2*s35**2)) + ss*(3*s15**3 + s15**2*(tt + 4*s25 - 3*s35) - 2*s25**2*(-tt + s&
          &35) - 2*tt*(-tt + s35)**2 + s25*(-2*tt**2 - tt*s35 + 2*s35**2) + s15*(2*s25**2 +&
          & s25*(4*tt - 7*s35) + 6*tt*(-tt + s35)))))/((4*me2 - ss - tt + s25)*(s15 + s25 -&
          & s35)) + (2*(64*me2**4 - 16*me2**3*(4*ss + 4*tt - 2*s25 - s35) + 2*me2**2*(8*ss*&
          &*2 + 10*tt**2 - 10*s15**2 - 16*tt*s25 + 4*ss*(6*tt + s15 - 2*s25 - s35) - 2*tt*s&
          &35 + 6*s25*s35 - 5*s35**2 + s15*(-12*tt - 4*s25 + 11*s35)) + me2*(-4*tt**3 - 4*s&
          &15**3 - 4*ss**2*(2*tt + s15) + 6*tt**2*s25 - 4*tt*s25**2 - 2*tt**2*s35 + tt*s25*&
          &s35 + 2*s25**2*s35 + 7*tt*s35**2 - 5*s25*s35**2 - 2*s35**3 + s15**2*(tt - 8*s25 &
          &+ 2*s35) + s15*(5*tt**2 - 10*tt*s25 - 13*tt*s35 + 12*s25*s35 + 4*s35**2) + ss*(-&
          &8*tt**2 + 18*s15**2 + 12*tt*s25 + s15*(18*tt + 4*s25 - 23*s35) - 6*tt*s35 - 2*s2&
          &5*s35 + 9*s35**2)) + (tt + s15 - s35)*(tt*s15**2 + ss*(2*s15 - s35)*(-2*tt + s15&
          & + 2*s25 + s35) + ss**2*(-4*s15 + 2*s35) + s15*(2*tt*s25 + tt*s35 - 2*s25*s35) -&
          & (-tt + s35)*(-(s25*s35) + tt*(tt + s35)))))/s35 - (tt*(2*tt**4 + 2*tt**3*s15 - &
          &2*tt**2*s15**2 - 3*tt*s15**3 + tt**2*s15*s25 + 3*tt*s15**2*s25 + 4*s15**3*s25 + &
          &tt**2*s25**2 + 4*tt*s15*s25**2 + 4*s15**2*s25**2 - ss**2*(7*s15**2 + s15*(4*tt +&
          & 8*s25 - 7*s35) + 4*(-tt + 2*s25)*(tt - s35)) + 4*ss**3*(tt + s15 - s35) - 32*me&
          &2**3*(tt + s15 + 2*s25 - s35) - 4*tt**3*s35 + 4*tt*s15**2*s35 + 2*tt**2*s25*s35 &
          &- 2*tt*s15*s25*s35 - 7*s15**2*s25*s35 - 3*tt*s25**2*s35 - 6*s15*s25**2*s35 + 2*t&
          &t**2*s35**2 - 2*tt*s15*s35**2 - 2*tt*s25*s35**2 + 4*s15*s25*s35**2 + 2*s25**2*s3&
          &5**2 + 4*me2**2*(6*tt**2 - 10*s15**2 - 8*tt*s25 - 12*s25**2 + 4*ss*(5*tt + 4*s15&
          & + 3*s25 - 4*s35) - 3*tt*s35 + 13*s25*s35 - s35**2 + s15*(-7*tt - 17*s25 + 11*s3&
          &5)) - 2*me2*(4*tt**3 + 4*s15**3 - 5*tt**2*s25 + 4*tt*s25**2 + 4*s25**3 + s15*(2*&
          &tt**2 + 9*s25**2 + 5*s25*(tt - 2*s35)) + 4*ss**2*(5*tt + 4*s15 + s25 - 4*s35) + &
          &s15**2*(6*tt + 7*s25 - 4*s35) - 6*tt**2*s35 + 6*tt*s25*s35 - 9*s25**2*s35 + 3*tt&
          &*s35**2 - s25*s35**2 - ss*(-10*tt**2 + 20*s15**2 + 24*tt*s25 + 8*s25**2 + s15*(1&
          &8*tt + 25*s25 - 21*s35) + 8*tt*s35 - 25*s25*s35 + s35**2)) + ss*(s15**3 - 4*s25*&
          &*2*(-tt + s35) + 2*tt*(-tt + s35)**2 + s15**2*(-tt + 2*s25 + s35) + s25*(-6*tt**&
          &2 + 7*tt*s35 - 2*s35**2) + s15*(4*s25**2 - 2*(-tt + s35)**2 - s25*(2*tt + s35)))&
          &))/((tt + s15 - s35)*(s15 + s25 - s35)) + (tt*(2*tt**3*s15 + 6*tt**2*s15**2 + 5*&
          &tt*s15**3 - 2*tt**3*s25 + tt**2*s15*s25 + tt*s15**2*s25 - 4*s15**3*s25 + 3*tt**2&
          &*s25**2 - 4*s15**2*s25**2 + 4*ss**3*(tt + s15 - s35) + 32*me2**3*(3*tt + s15 - s&
          &35) - 4*tt**3*s35 - 9*tt**2*s15*s35 - 9*tt*s15**2*s35 + 7*tt**2*s25*s35 + 3*tt*s&
          &15*s25*s35 + 7*s15**2*s25*s35 - 5*tt*s25**2*s35 + 2*s15*s25**2*s35 + 6*tt**2*s35&
          &**2 + 6*tt*s15*s35**2 - 7*tt*s25*s35**2 - 5*s15*s25*s35**2 + 2*s25**2*s35**2 - 2&
          &*tt*s35**3 + 2*s25*s35**3 + 4*me2**2*(-14*tt**2 - 5*s15**2 + 12*tt*s25 - 4*s25**&
          &2 + 4*ss*(-3*tt + s15 + s25 - s35) + 3*tt*s35 - s25*s35 + 4*s35**2 + s15*(-tt - &
          &7*s25 + s35)) - ss**2*(s15**2 - 4*(-tt + s35)*(-2*tt + 2*s25 + s35) + s15*(-8*tt&
          & + 8*s25 + 3*s35)) + ss*(6*tt**3 + 3*s15**3 + 3*s15**2*(tt + 2*s25 - 2*s35) - 14&
          &*tt**2*s35 + 9*tt*s35**2 - 2*s35**3 - 4*s25**2*(-tt + s35) + s25*(-10*tt**2 + 17&
          &*tt*s35 - 6*s35**2) + s15*(8*tt**2 + 4*s25**2 - 11*tt*s35 + 5*s35**2 + s25*(-6*t&
          &t + s35))) - 2*me2*(-2*tt**3 + 4*s15**3 + 3*tt**2*s25 - 4*tt*s25**2 + 4*s25**3 +&
          & s15**2*(-2*tt + 13*s25 - 7*s35) + 4*ss**2*(tt + 3*s15 + s25 - 3*s35) - 7*tt**2*&
          &s35 + 7*tt*s25*s35 - 3*s25**2*s35 + 9*tt*s35**2 - 6*s25*s35**2 - 2*s35**3 + s15*&
          &(3*tt**2 - 6*tt*s25 + 11*s25**2 - 8*tt*s35 - 5*s25*s35 + 5*s35**2) - ss*(-2*tt**&
          &2 + 7*s15**2 + 8*s25**2 + 16*tt*s35 - 15*s25*s35 - 8*s35**2 + s15*(-14*tt + 23*s&
          &25 + s35)))))/(s25*(tt + s15 - s35)))*pvd1(2))/tt**2 + (64*Pi**2*((2*(16*me2**3*&
          &(s15 + 2*s25 - s35) - 4*me2**2*(s15**2 + 6*tt*s25 + s15*(3*tt + 2*s25 - s35) + 2&
          &*ss*(s15 + 2*s25 - s35) - tt*s35 - 2*s25*s35) + (tt + s15 - s35)*(tt*s15**2 + ss&
          &*(2*s15 - s35)*(s15 + 2*s25 - s35) + s15*(tt**2 + 2*tt*s25 - 2*s25*s35) + (-tt +&
          & s35)*(-2*tt**2 + s25*s35)) + me2*(-4*tt**3 - 4*s15**3 + 4*tt**2*s25 - 4*tt*s25*&
          &*2 + 2*ss*(2*tt + s15)*(s15 + 2*s25 - s35) + 2*tt**2*s35 + 4*tt*s25*s35 + 2*s25*&
          &*2*s35 - tt*s35**2 - 6*s25*s35**2 + 2*s35**3 + s15**2*(-7*tt - 8*s25 + 10*s35) +&
          & 2*s15*(-3*tt**2 - 5*tt*s25 + 5*tt*s35 + 6*s25*s35 - 4*s35**2))))/s35 - (2*(4*me&
          &2**2*((2*s25 - s35)*(-tt + 2*s25 - s35) - 2*ss*(s15 + 2*s25 - s35) + s15*(-3*tt &
          &+ 4*s25 - s35)) + s15*(-((-tt + s25)*(3*tt + 4*s15 + 4*s25 - 3*s35)) + ss*(s15 +&
          & 2*s25 - s35))*(tt + s15 - s35) + 16*me2**3*(s15 + 2*s25 - s35) - 2*me2*(2*tt**3&
          & + s15**3 + s15**2*(tt + 3*s25 - 2*s35) - 4*tt**2*s35 - tt*s25*s35 + 2*s25**2*s3&
          &5 + 2*tt*s35**2 - s25*s35**2 + ss*s35*(-2*s25 + s35) + s15*(2*tt**2 + 2*tt*s25 +&
          & s25**2 - ss*s35 - 4*tt*s35 - s25*s35 + s35**2))))/s15 + (2*tt*(-24*me2**3*(s15 &
          &+ 2*s25 - s35) + s15*(-tt + s25)*(tt + s15 - s35)*(-ss + tt + 2*s15 + 2*s25 - s3&
          &5) + 2*me2**2*(3*s15**2 + 8*tt*s25 - 4*s25**2 + 2*s15*(3*tt + s25 - s35) + 2*ss*&
          &(s15 + 2*s25 - s35) - 2*tt*s35 + 2*s25*s35 - s35**2) + me2*(2*tt**3 + s15**3 - 2&
          &*tt**2*s25 + s15**2*(-2*tt + 5*s25 - 3*s35) - 4*tt**2*s35 + 2*s25**2*s35 - 2*ss*&
          &(tt + s25 - s35)*s35 + 3*tt*s35**2 - s35**3 + s15*(-tt**2 + 2*tt*s25 + s25**2 - &
          &2*ss*s35 - tt*s35 - 5*s25*s35 + 3*s35**2))))/(s15*(4*me2 - ss - tt + s25)) + (tt&
          &*(-2*(-tt + s25)*(ss**2 + s15**2 + s15*(-tt + s25) - ss*(2*s15 + s25) - tt*(tt +&
          & s25 - s35))*(tt + s15 - s35) - 4*me2**2*(-2*tt**2 - 6*tt*s25 + 4*s25**2 + 8*tt*&
          &s35 - 4*s25*s35 - s35**2 + s15*(-6*tt + 4*s25 + s35)) + me2*(2*tt**3 + 4*tt**2*s&
          &25 - 4*tt*s25**2 - 4*s25**3 + tt*s25*s35 + 6*s25**2*s35 - 6*tt*s35**2 - s25*s35*&
          &*2 + 2*s35**3 + s15**2*(17*tt - 18*s25 + 2*s35) + ss*(-16*tt**2 + s15**2 + 12*tt&
          &*s25 + 4*s25**2 + 2*s15*(-7*tt + 8*s25 - s35) + 14*tt*s35 - 16*s25*s35 + s35**2)&
          & - s15*(-22*tt**2 + 11*tt*s25 + 14*s25**2 + 11*tt*s35 - 19*s25*s35 + 4*s35**2)))&
          &)/(s25*(4*me2 - ss - tt + s35)) - (tt*(2*tt**4 + 3*tt**3*s15 - tt**2*s15**2 - tt&
          &*s15**3 + 3*tt**3*s25 + 6*tt**2*s15*s25 + 5*tt*s15**2*s25 + 2*s15**3*s25 + tt**2&
          &*s25**2 + 2*tt*s15*s25**2 + 2*s15**2*s25**2 - 6*tt**3*s35 - 5*tt**2*s15*s35 + tt&
          &*s15**2*s35 - 6*tt**2*s25*s35 - 8*tt*s15*s25*s35 - 5*s15**2*s25*s35 - tt*s25**2*&
          &s35 - 2*s15*s25**2*s35 + 6*tt**2*s35**2 + 2*tt*s15*s35**2 + 3*tt*s25*s35**2 + 3*&
          &s15*s25*s35**2 - 2*tt*s35**3 - ss**2*(2*tt**2 + s15**2 + 2*tt*s25 + 2*s15*(tt + &
          &s25 - s35) - 2*tt*s35 - 2*s25*s35 + s35**2) - 4*me2**2*(2*tt**2 + 4*s15**2 + 18*&
          &tt*s25 + 8*s25**2 + s15*(8*tt + 14*s25 - 7*s35) - 6*tt*s35 - 14*s25*s35 + 3*s35*&
          &*2) + ss*(3*s15**3 + s15**2*(6*tt + 6*s25 - 8*s35) - 2*s25**2*(-tt + s35) + s25*&
          &s35*(-4*tt + 3*s35) + s35*(-3*tt**2 + 5*tt*s35 - 2*s35**2) + s15*(5*tt**2 + 6*tt&
          &*s25 + 2*s25**2 - 11*tt*s35 - 9*s25*s35 + 7*s35**2)) + me2*(-2*tt**3 - 8*s15**3 &
          &- 12*tt*s25**2 - 4*s25**3 + 10*tt**2*s35 + 9*tt*s25*s35 + 12*s25**2*s35 - 14*tt*&
          &s35**2 - 5*s25*s35**2 + 4*s35**3 + s15**2*(-13*tt - 20*s25 + 20*s35) + ss*(16*tt&
          &**2 + 9*s15**2 + 28*tt*s25 + 4*s25**2 + 2*s15*(11*tt + 10*s25 - 9*s35) - 22*tt*s&
          &35 - 20*s25*s35 + 9*s35**2) - s15*(12*tt**2 + 19*tt*s25 + 12*s25**2 - 27*tt*s35 &
          &- 25*s25*s35 + 16*s35**2))))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + (tt*(&
          &-2*tt**4 + tt**3*s15 + 7*tt**2*s15**2 + 5*tt*s15**3 + tt**3*s25 + 4*tt**2*s15*s2&
          &5 + tt*s15**2*s25 - 4*s15**3*s25 + 3*tt**2*s25**2 - 4*s15**2*s25**2 - 9*tt**2*s1&
          &5*s35 - 10*tt*s15**2*s35 - tt*s15*s25*s35 + 7*s15**2*s25*s35 - 5*tt*s25**2*s35 +&
          & 2*s15*s25**2*s35 + 4*tt**2*s35**2 + 7*tt*s15*s35**2 - 2*tt*s25*s35**2 - 4*s15*s&
          &25*s35**2 + 2*s25**2*s35**2 - 2*tt*s35**3 + s25*s35**3 + 4*me2**2*(-4*tt**2 + s1&
          &5**2 + 8*tt*s25 + 2*s15*(tt + s25 - s35) - 2*tt*s35 - 2*s25*s35 + s35**2) - ss**&
          &2*(-4*tt**2 + s15**2 + 4*tt*s25 + 4*s15*s25 + 2*tt*s35 - 4*s25*s35 + s35**2 - 2*&
          &s15*(tt + s35)) + 2*me2*(2*tt**3 - 4*tt**2*s25 + 2*tt*s25**2 - 4*s25**3 + 4*tt**&
          &2*s35 - 3*tt*s25*s35 + 5*s25**2*s35 - 3*tt*s35**2 + 3*s25*s35**2 - s35**3 - s15*&
          &*2*(-8*tt + 9*s25 + s35) + ss*(-8*tt**2 + s15**2 + 4*s25**2 + 2*s15*(-5*tt + 5*s&
          &25 - s35) + 10*tt*s35 - 10*s25*s35 + s35**2) + s15*(4*tt**2 + 3*tt*s25 - 11*s25*&
          &*2 - 5*tt*s35 + 6*s25*s35 + 2*s35**2)) + ss*(4*tt**3 - s15**3 - 7*tt**2*s35 + tt&
          &*s35**2 + s35**3 - 4*s25**2*(-tt + s35) + s15**2*(-4*tt + 2*s25 + 3*s35) + s25*(&
          &-6*tt**2 + 10*tt*s35 - 3*s35**2) + s15*(tt**2 + 4*s25**2 + 3*tt*s35 - 3*s35**2 +&
          & s25*(-4*tt + s35)))))/(s25*(tt + s15 - s35)) - (tt*(2*tt**4 + tt**3*s15 - 5*tt*&
          &*2*s15**2 - 3*tt*s15**3 + 3*tt**3*s25 + 6*tt**2*s15*s25 + 7*tt*s15**2*s25 + 4*s1&
          &5**3*s25 + tt**2*s25**2 + 4*tt*s15*s25**2 + 4*s15**2*s25**2 - 4*tt**3*s35 + 3*tt&
          &**2*s15*s35 + 7*tt*s15**2*s35 - 6*tt**2*s25*s35 - 12*tt*s15*s25*s35 - 11*s15**2*&
          &s25*s35 - 3*tt*s25**2*s35 - 6*s15*s25**2*s35 + 2*tt**2*s35**2 - 4*tt*s15*s35**2 &
          &+ 5*tt*s25*s35**2 + 9*s15*s25*s35**2 + 2*s25**2*s35**2 - 2*s25*s35**3 - 4*me2**2&
          &*(-4*tt**2 + s15**2 + 8*tt*s25 + 8*s25**2 + 2*s15*(-tt + 5*s25 - s35) + 2*tt*s35&
          & - 10*s25*s35 + s35**2) - ss**2*(4*tt**2 + 3*s15**2 + 4*tt*s25 + s15*(6*tt + 4*s&
          &25 - 6*s35) - 6*tt*s35 - 4*s25*s35 + 3*s35**2) + 2*me2*(-2*tt**3 - 4*s15**3 - 6*&
          &tt*s25**2 - 4*s25**3 + 4*tt**2*s35 + 7*tt*s25*s35 + 11*s25**2*s35 - 5*tt*s35**2 &
          &- 7*s25*s35**2 + 2*s35**3 + s15**2*(-8*tt - 11*s25 + 10*s35) + ss*(8*tt**2 + 7*s&
          &15**2 + 16*tt*s25 + 4*s25**2 + 14*s15*(tt + s25 - s35) - 14*tt*s35 - 14*s25*s35 &
          &+ 7*s35**2) - s15*(8*tt**2 + 11*tt*s25 + 9*s25**2 - 13*tt*s35 - 18*s25*s35 + 8*s&
          &35**2)) + ss*(s15**3 - 4*s25**2*(-tt + s35) + tt*(-4*tt**2 + 7*tt*s35 - 3*s35**2&
          &) + s15*(-5*tt**2 + 4*tt*s25 + 4*s25**2 + 5*tt*s35 - 9*s25*s35 + s35**2) + s25*(&
          &-2*tt**2 - 2*tt*s35 + 3*s35**2) + s15**2*(6*s25 - 2*(tt + s35)))))/((tt + s15 - &
          &s35)*(s15 + s25 - s35)) + (2*tt*(24*me2**3*(s15 + 2*s25 - s35) - 2*me2**2*(5*s15&
          &**2 + 20*tt*s25 + 2*s15*(4*tt + 6*s25 - 3*s35) + 2*ss*(s15 + 2*s25 - s35) - 4*tt&
          &*s35 - 8*s25*s35 + s35**2) + (tt + s15 - s35)*(tt*s15**2 - tt*(tt + s25 - s35)*(&
          &-tt + s35) + s15*(tt**2 + 2*tt*s25 - tt*s35 - s25*s35) + ss*(s15**2 + 2*s15*(s25&
          & - s35) + s35*(-tt - s25 + s35))) + me2*(-2*tt**3 - 3*s15**3 + 2*ss*tt*s35 + 4*t&
          &t**2*s35 - 2*ss*s35**2 - 4*tt*s35**2 + s35**3 + s25**2*(-2*tt + s35) + s15**2*(-&
          &3*tt - 6*s25 + 7*s35) + s25*(4*tt*(ss + tt) - 3*tt*s35 + s35**2) + s15*(-3*tt**2&
          & - 3*tt*s25 + 7*tt*s35 + 5*s25*s35 - 5*s35**2 + 2*ss*(s25 + s35)))))/(s35*(4*me2&
          & - ss - tt + s35)))*pvd1(3))/tt**2 + 64*Pi**2*((-2*(-8*me2**2*s15 + s15*(tt + s1&
          &5 - s35)*(3*tt + 2*s15 - s35) + me2*(-4*tt**2 - 2*tt*s15 + s15**2 + 6*tt*s35 + s&
          &15*s35)))/(tt*s15) - (2*tt**3 + tt**2*s15 - 4*tt*s15**2 + 5*tt**2*s25 + 6*tt*s15&
          &*s25 + 4*s15**2*s25 - 2*ss**2*(2*tt + s15 - s35) + 8*me2**2*(2*tt + s15 - 2*s25 &
          &- s35) - 4*tt**2*s35 + 2*tt*s15*s35 + 2*s15**2*s35 - 7*tt*s25*s35 - 4*s15*s25*s3&
          &5 + 2*tt*s35**2 - 2*s15*s35**2 + 2*s25*s35**2 - ss*(4*tt**2 + tt*s15 + 2*s15**2 &
          &- 2*tt*s25 - 3*tt*s35 - 2*s15*s35 + 2*s25*s35) + 2*me2*(-2*tt**2 - 8*tt*s15 + s1&
          &5**2 - 2*tt*s25 + 3*s15*s25 + 2*ss*(4*tt + s15 - s35) + 4*tt*s35 + 2*s15*s35 + 3&
          &*s25*s35 - 3*s35**2))/((tt + s15 - s35)*(s15 + s25 - s35)) - (2*(s15*(-ss + tt +&
          & s15)*(tt + s15 - s35) - 2*me2**2*(5*s15 + 6*s25 - s35) + me2*(-2*tt**2 + 6*s15*&
          &*2 + 4*tt*s25 + s15*(3*tt + 7*s25 - 7*s35) + 2*ss*s35 + 2*tt*s35 - 4*s25*s35 + s&
          &35**2)))/(s15*(4*me2 - ss - tt + s25)) - (-2*ss**2*tt + 2*tt**3 + tt**2*s15 - 4*&
          &tt*s15**2 + tt**2*s25 + 2*s15**2*s25 - 4*tt**2*s35 + 2*tt*s15*s35 + 2*s15**2*s35&
          & - tt*s25*s35 + 2*tt*s35**2 - 2*s15*s35**2 - 8*me2**2*(tt + 2*s25 + s35) + 2*me2&
          &*(-tt**2 - 6*tt*s15 + s15**2 - tt*s25 + 3*s15*s25 + ss*(8*tt + s15 + 2*s25 - s35&
          &) + 5*tt*s35 + 4*s15*s35 + 2*s25*s35 - 5*s35**2) + ss*(5*tt*s15 - 2*s15*s25 - 3*&
          &tt*s35 - 2*s15*s35 + 2*s35**2))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + (2&
          &*(-8*me2**2*s35 + (tt + s15 - s35)*(2*tt**2 - tt*s35 + 2*s15*s35 - s35**2) + me2&
          &*(-4*tt**2 + 2*tt*s35 + s35**2 + s15*(-6*tt + s35))))/(tt*s35) - (2*(2*me2**2*(s&
          &15 + 6*s25 + 3*s35) + (tt + s15 - s35)*(ss*s35 - s15*s35 + tt*(-tt + s35)) + me2&
          &*(2*tt**2 - 3*s15**2 - 4*tt*s25 + 2*ss*(s15 - 2*s35) - 3*tt*s35 + 3*s25*s35 + 2*&
          &s35**2 + s15*(2*tt - 6*s25 + s35))))/(s35*(4*me2 - ss - tt + s35)) + (2*(ss - tt&
          & - s15)*(ss + tt - s15 - s25)*(tt + s15 - s35) - 8*me2**2*(-tt + 2*s25 + s35) + &
          &2*me2*(8*s15**2 + 4*s15*(3*tt + s25 - 2*s35) - (tt + 5*s25)*(-tt + s35) + ss*(-8&
          &*tt - 5*s15 + 2*s25 + 5*s35)))/(s25*(4*me2 - ss - tt + s35)) + (-2*tt**3 + 3*tt*&
          &*2*s15 + 8*tt*s15**2 + 4*s15**3 + 3*tt**2*s25 + 6*tt*s15*s25 + 4*s15**2*s25 + 2*&
          &ss**2*(2*tt + s15 - s35) - 8*me2**2*(2*tt + s15 + 2*s25 - s35) - 2*tt**2*s35 - 1&
          &2*tt*s15*s35 - 8*s15**2*s35 - 3*tt*s25*s35 - 4*s15*s25*s35 + 6*tt*s35**2 + 6*s15&
          &*s35**2 - 2*s35**3 + ss*(4*tt**2 + tt*s15 - 2*s15**2 - 2*tt*s25 - 7*tt*s35 + 2*s&
          &25*s35 + 2*s35**2) + 2*me2*(2*tt**2 + 6*tt*s15 + 6*s15**2 + 2*tt*s25 + 3*s15*s25&
          & + 2*tt*s35 - 4*s15*s35 - s25*s35 - 2*s35**2 + ss*(-8*tt - 6*s15 + 6*s35)))/(s25&
          &*(tt + s15 - s35)))*pvd1(4) + (128*Pi**2*((4*(2*me2 - tt)*(-(me2*(2*tt + s15 - 3&
          &*s35)) + s15*(tt + s15 - s35)))/s15 + (2*(2*me2 - tt)*tt*(-(me2*(2*tt + s15 - 3*&
          &s35)) + s15*(tt + s15 - s35)))/(s15*(4*me2 - ss - tt + s25)) - (4*(2*me2 - tt)*(&
          &-((tt + s15 - s35)*(-tt + s35)) + me2*(-2*tt - 3*s15 + s35)))/s35 - (2*(2*me2 - &
          &tt)*tt*(-((tt + s15 - s35)*(-tt + s35)) + me2*(-2*tt - 3*s15 + s35)))/(s35*(4*me&
          &2 - ss - tt + s35)) - (2*tt*(48*me2**3 - tt*(-2*tt + s25)*(tt + s15 - s35) + 4*m&
          &e2**2*(-2*ss - 4*tt + s25 + 2*s35) + me2*(2*ss*tt - 3*tt**2 + 2*tt*s25 + 2*s15*(&
          &-2*tt + s25) + 2*tt*s35 - 2*s25*s35)))/(s25*(4*me2 - ss - tt + s35)) - (tt*(-96*&
          &me2**3 + 8*me2**2*(2*ss + 4*tt + s15 - s25 - s35) + 2*me2*(-2*ss*tt - tt**2 + 2*&
          &s15**2 + s15*(tt - 2*s25) + 3*tt*s35 + 2*s25*s35 - 2*s35**2) + tt*(tt*s15 - 2*s1&
          &5**2 + ss*(s15 - s35) - (2*tt + s25 - 2*s35)*(-tt + s35))))/((4*me2 - ss - tt + &
          &s25)*(s15 + s25 - s35)) - (tt*(64*me2**3 - 16*me2**2*(2*ss + 2*tt - s15 - s25 - &
          &s35) + 2*me2*(4*s15**2 + s25*(2*tt - 4*s35) + s15*(-tt + 4*s25 - 4*s35) + tt*(4*&
          &ss - 2*tt + s35)) + tt*(-2*s15**2 + 3*(-2*tt + s25)*(-tt + s35) + ss*(-s15 + s35&
          &) + s15*(3*tt - 2*s25 + 2*s35))))/(s25*(tt + s15 - s35)) + (tt*(64*me2**3 - 16*m&
          &e2**2*(2*ss + 2*tt - s25) + 2*me2*(4*ss*tt + 6*tt**2 + 2*tt*s25 + s15*(5*tt + 4*&
          &s25 - 4*s35) - 9*tt*s35 - 4*s25*s35 + 4*s35**2) - tt*(s15*(7*tt + 2*s25 - 4*s35)&
          & + ss*(s15 - s35) + (-tt + s35)*(-6*tt - 3*s25 + 4*s35))))/((tt + s15 - s35)*(s1&
          &5 + s25 - s35)))*pvd1(5))/tt**2 + (64*Pi**2*((4*(2*me2 - ss - tt + s25)*(2*me2 -&
          & ss + s15 + s25)*(-(me2*(2*tt + s15 - 3*s35)) + s15*(tt + s15 - s35)))/s15 + (2*&
          &tt*(2*me2 - ss - tt + s25)*(2*me2 - ss + s15 + s25)*(-(me2*(2*tt + s15 - 3*s35))&
          & + s15*(tt + s15 - s35)))/(s15*(4*me2 - ss - tt + s25)) + (4*(2*me2 - ss - tt + &
          &s25)*(2*me2 - ss + s15 + s25)*(me2*(2*tt + 3*s15 - s35) + (tt + s15 - s35)*(-tt &
          &+ s35)))/s35 + (2*tt*(2*me2 - ss - tt + s25)*(2*me2 - ss + s15 + s25)*(me2*(2*tt&
          & + 3*s15 - s35) + (tt + s15 - s35)*(-tt + s35)))/(s35*(4*me2 - ss - tt + s35)) +&
          & (tt*(2*me2 - ss + s15 + s25)*(64*me2**3 - 6*tt**3 - 9*tt**2*s15 - 3*tt*s15**2 -&
          & 16*me2**2*(4*ss + 2*tt - s15 - 3*s25) + tt**2*s25 + 3*tt*s15*s25 + 2*s15**2*s25&
          & + 4*tt*s25**2 + 4*s15*s25**2 + 10*tt**2*s35 + 6*tt*s15*s35 - 5*tt*s25*s35 - 5*s&
          &15*s25*s35 - 4*s25**2*s35 - 4*tt*s35**2 + 4*s25*s35**2 - ss*(4*tt**2 + 5*tt*s15 &
          &+ s15**2 + 4*tt*s25 + 4*s15*s25 - 9*tt*s35 - 5*s15*s35 - 4*s25*s35 + 4*s35**2) +&
          & 2*me2*(8*ss**2 + 6*tt**2 + 3*tt*s15 + s15**2 - 2*tt*s25 + 6*s15*s25 + 4*s25**2 &
          &- 4*ss*(-2*tt + s15 + 3*s25) - 9*tt*s35 - 5*s15*s35 - 4*s25*s35 + 4*s35**2)))/((&
          &tt + s15 - s35)*(s15 + s25 - s35)) + (tt*(2*me2 - ss + s15 + s25)*(96*me2**3 - 2&
          &*tt**3 - 3*tt**2*s15 - tt*s15**2 - tt**2*s25 - tt*s15*s25 + 2*tt*s25**2 + 2*s15*&
          &s25**2 - 8*me2**2*(8*ss + 4*tt - 2*s15 - 7*s25 - s35) + 4*tt**2*s35 + 2*tt*s15*s&
          &35 - tt*s25*s35 - s15*s25*s35 - 2*s25**2*s35 - 2*tt*s35**2 + 2*s25*s35**2 + ss*(&
          &s15**2 - 2*tt*s25 + 3*tt*s35 + 2*s25*s35 - 2*s35**2 + s15*(tt - 2*s25 + s35)) + &
          &2*me2*(4*ss**2 + tt**2 - 2*tt*s15 - 3*s15**2 - 2*tt*s25 + 2*s25**2 - 3*tt*s35 + &
          &s15*s35 + 2*s35**2 - 2*ss*(-2*tt + 3*s25 + s35))))/((4*me2 - ss - tt + s25)*(s15&
          & + s25 - s35)) - (2*tt*(2*me2 - ss - tt + s25)*(48*me2**3 - 4*me2**2*(8*ss + 4*t&
          &t - 3*s15 - 7*s25 - 2*s35) - (ss - s15 - s25)*(-2*tt + s25)*(tt + s15 - s35) + m&
          &e2*(4*ss**2 - 3*tt**2 - 5*tt*s15 - 2*tt*s25 + 2*s15*s25 + 2*s25**2 + 2*tt*s35 + &
          &2*s15*s35 + 2*s25*s35 - 2*ss*(-2*tt + s15 + 3*s25 + 2*s35))))/(s25*(4*me2 - ss -&
          & tt + s35)) - (tt*(2*me2 - ss - tt + s25)*(64*me2**3 - 2*tt**3 - 9*tt**2*s15 - 5&
          &*tt*s15**2 + 2*s15**3 - 5*tt**2*s25 + tt*s15*s25 + 6*s15**2*s25 + 4*tt*s25**2 + &
          &4*s15*s25**2 - 16*me2**2*(4*ss + 2*tt - 2*s15 - 3*s25 - s35) + 2*tt**2*s35 + 8*t&
          &t*s15*s35 - 2*s15**2*s35 + 5*tt*s25*s35 - 7*s15*s25*s35 - 4*s25**2*s35 + 2*me2*(&
          &8*ss**2 - 2*tt**2 - 5*tt*s15 + 5*s15**2 - 2*tt*s25 + 10*s15*s25 + 4*s25**2 + 3*t&
          &t*s35 - s15*s35 - 4*ss*(-2*tt + 2*s15 + 3*s25 + s35)) + ss*(4*tt**2 - 3*s15**2 -&
          & 4*tt*s25 - 5*tt*s35 + 4*s25*s35 + s15*(tt - 4*s25 + 3*s35))))/(s25*(tt + s15 - &
          &s35)))*pvd1(6))/tt**2 + (64*Pi**2*((2*tt*(-2*me2*(s15 + 2*s25 - s35) + ss*(s15 +&
          & 2*s25 - s35) - (-tt + s25)*(tt + 2*s15 + 2*s25 - s35))*(-((tt + s15 - s35)*(-tt&
          & + s35)) + me2*(-2*tt - 3*s15 + s35)))/(s35*(4*me2 - ss - tt + s35)) + (4*(-(me2&
          &*(2*tt + s15 - 3*s35)) + s15*(tt + s15 - s35))*(2*me2*(s15 + 2*s25 - s35) + (-tt&
          & + s25)*(tt + 2*s15 + 2*s25 - s35) + ss*(-s15 - 2*s25 + s35)))/s15 + (2*tt*(-(me&
          &2*(2*tt + s15 - 3*s35)) + s15*(tt + s15 - s35))*(2*me2*(s15 + 2*s25 - s35) + (-t&
          &t + s25)*(tt + 2*s15 + 2*s25 - s35) + ss*(-s15 - 2*s25 + s35)))/(s15*(4*me2 - ss&
          & - tt + s25)) - (4*(-((tt + s15 - s35)*(-tt + s35)) + me2*(-2*tt - 3*s15 + s35))&
          &*(2*me2*(s15 + 2*s25 - s35) + (-tt + s25)*(tt + 2*s15 + 2*s25 - s35) + ss*(-s15 &
          &- 2*s25 + s35)))/s35 - (2*tt*(-((-2*tt + s25)*(ss*(s15 + 2*s25 - s35) - (-tt + s&
          &25)*(tt + 2*s15 + 2*s25 - s35))*(tt + s15 - s35)) + 24*me2**3*(-2*tt + s15 + 4*s&
          &25 - s35) + 2*me2**2*(8*tt**2 - 38*tt*s25 + 28*s25**2 - 8*ss*(-2*tt + s15 + 4*s2&
          &5 - s35) + 3*tt*s35 + 2*s25*s35 - 2*s35**2 + s15*(-13*tt + 12*s25 + 2*s35)) + me&
          &2*(-tt**3 + tt**2*s25 - 8*tt*s25**2 + 4*s25**3 + 2*s15**2*(-2*tt + s25) + 2*ss**&
          &2*(-2*tt + s15 + 4*s25 - s35) + 5*tt**2*s35 - 3*tt*s25*s35 + 4*s25**2*s35 - 2*tt&
          &*s35**2 + s15*(-2*tt**2 - 8*tt*s25 + 4*s25**2 + 4*tt*s35) + ss*(-4*tt**2 + 18*tt&
          &*s25 - 12*s25**2 + s15*(5*tt - 4*s25 - 2*s35) + tt*s35 - 6*s25*s35 + 2*s35**2)))&
          &)/(s25*(4*me2 - ss - tt + s35)) + (tt*(-2*tt**4 - 9*tt**3*s15 - 10*tt**2*s15**2 &
          &- 2*tt*s15**3 - 7*tt**3*s25 - 11*tt**2*s15*s25 - 2*tt*s15**2*s25 + tt**2*s25**2 &
          &+ 6*tt*s15*s25**2 + 4*s15**2*s25**2 + 4*tt*s25**3 + 4*s15*s25**3 + 48*me2**3*(2*&
          &tt + 3*s15 + 4*s25 - 3*s35) + ss**2*(s15 - s35)*(2*tt + s15 - s35) + 6*tt**3*s35&
          & + 17*tt**2*s15*s35 + 8*tt*s15**2*s35 + 12*tt**2*s25*s35 + 3*tt*s15*s25*s35 - 4*&
          &s15**2*s25*s35 - 8*tt*s25**2*s35 - 10*s15*s25**2*s35 - 4*s25**3*s35 - 6*tt**2*s3&
          &5**2 - 8*tt*s15*s35**2 - 3*tt*s25*s35**2 + 6*s15*s25*s35**2 + 7*s25**2*s35**2 + &
          &2*tt*s35**3 - 2*s25*s35**3 - 4*me2**2*(8*tt**2 - 9*s15**2 - 2*tt*s25 - 28*s25**2&
          & + 8*ss*(2*tt + 3*s15 + 4*s25 - 3*s35) - 11*tt*s35 + 16*s25*s35 + 3*s35**2 + s15&
          &*(5*tt - 34*s25 + 6*s35)) + 2*me2*(-3*tt**3 - 4*s15**3 - tt**2*s25 - 2*tt*s25**2&
          & + 4*s25**3 + ss**2*(4*tt + 6*s15 + 8*s25 - 6*s35) + 4*tt**2*s35 - 3*tt*s25*s35 &
          &- 2*s25**2*s35 + tt*s35**2 + 4*s25*s35**2 - 2*s35**3 + s15**2*(-7*tt - 6*s25 + 6&
          &*s35) + s15*(-7*tt**2 - 8*tt*s25 + 4*s25**2 + 6*tt*s35 + 2*s25*s35) + ss*(4*tt**&
          &2 + 3*tt*s15 - 2*s15**2 + 2*tt*s25 - 10*s15*s25 - 12*s25**2 - 5*tt*s35 + 4*s25*s&
          &35 + 2*s35**2)) + ss*(4*tt**3 - 5*tt**2*s35 - tt*s35**2 + 2*s35**3 + 4*s25**2*(-&
          &tt + s35) + s15**2*(tt - 2*s25 + 2*s35) + s15*(7*tt**2 - 6*tt*s25 - 4*s25**2 + 1&
          &0*s25*s35 - 4*s35**2) - 2*s25*(tt**2 - 6*tt*s35 + 4*s35**2))))/((4*me2 - ss - tt&
          & + s25)*(s15 + s25 - s35)) + (tt*(-6*tt**4 - 23*tt**3*s15 - 24*tt**2*s15**2 - 6*&
          &tt*s15**3 - 13*tt**3*s25 - 15*tt**2*s15*s25 + 4*tt*s15**2*s25 + 4*s15**3*s25 + 7&
          &*tt**2*s25**2 + 20*tt*s15*s25**2 + 12*s15**2*s25**2 + 8*tt*s25**3 + 8*s15*s25**3&
          & + 32*me2**3*(2*tt + 3*s15 + 4*s25 - 3*s35) + ss**2*(s15 - s35)*(2*tt + s15 - s3&
          &5) + 16*tt**3*s35 + 39*tt**2*s15*s35 + 18*tt*s15**2*s35 + 18*tt**2*s25*s35 - 5*t&
          &t*s15*s25*s35 - 14*s15**2*s25*s35 - 20*tt*s25**2*s35 - 24*s15*s25**2*s35 - 8*s25&
          &**3*s35 - 14*tt**2*s35**2 - 16*tt*s15*s35**2 - tt*s25*s35**2 + 14*s15*s25*s35**2&
          & + 13*s25**2*s35**2 + 4*tt*s35**3 - 4*s25*s35**3 - 4*me2**2*(8*tt**2 - 9*s15**2 &
          &- 24*s25**2 + 8*ss*(2*tt + 3*s15 + 4*s25 - 3*s35) - 8*tt*s35 + 18*s25*s35 - s35*&
          &*2 + s15*(4*tt - 30*s25 + 10*s35)) + 2*me2*(-2*tt**3 + 2*s15**3 + 6*tt**2*s25 + &
          &2*tt*s25**2 + 8*s25**3 + s15**2*(tt + 12*s25 - 8*s35) + 4*ss**2*(2*tt + 3*s15 + &
          &4*s25 - 3*s35) - tt**2*s35 - 18*tt*s25*s35 - 14*s25**2*s35 + 7*tt*s35**2 + 14*s2&
          &5*s35**2 - 4*s35**3 - 2*ss*(-4*tt**2 + 5*s15**2 + 12*s25**2 + s15*(-tt + 15*s25 &
          &- 6*s35) + 3*tt*s35 - 9*s25*s35 + s35**2) + s15*(-3*tt**2 + 8*tt*s25 + 20*s25**2&
          & - 8*tt*s35 - 26*s25*s35 + 10*s35**2)) - ss*(-4*tt**3 + 2*s15**3 + s15**2*(5*tt &
          &+ 8*s25 - 8*s35) + tt**2*s35 + 7*tt*s35**2 - 4*s35**3 - 8*s25**2*(-tt + s35) + 2&
          &*s25*(5*tt**2 - 13*tt*s35 + 7*s35**2) + s15*(-3*tt**2 + 20*tt*s25 + 8*s25**2 - 1&
          &2*tt*s35 - 22*s25*s35 + 10*s35**2))))/((tt + s15 - s35)*(s15 + s25 - s35)) - (tt&
          &*(-(ss**2*(s15 - s35)*(2*tt + s15 - s35)) + 32*me2**3*(-2*tt + s15 + 4*s25 - s35&
          &) + 4*me2**2*(8*tt**2 + s15**2 - 32*tt*s25 + 24*s25**2 - 8*ss*(-2*tt + s15 + 4*s&
          &25 - s35) + 4*tt*s35 + 2*s25*s35 - 3*s35**2 + 2*s15*(-8*tt + 9*s25 + s35)) + (-t&
          &t + s25)*(4*s15**3 + 2*s15**2*(-4*tt + 6*s25 - 3*s35) - (-tt + s35)*(-10*tt**2 -&
          & 7*tt*s25 + 8*s25**2 + 6*tt*s35 - 3*s25*s35) + s15*(-21*tt**2 + 8*s25**2 + 4*s25&
          &*(tt - 4*s35) + 17*tt*s35 + 2*s35**2)) + ss*(-2*s15**3 + 8*s25**2*(-tt + s35) + &
          &s15**2*(5*tt - 8*s25 + 4*s35) + s15*(9*tt**2 + 4*tt*s25 - 8*s25**2 - 10*tt*s35 +&
          & 10*s25*s35 - 2*s35**2) - 2*s25*(-5*tt**2 + 5*tt*s35 + s35**2) + tt*(4*tt**2 - 7&
          &*tt*s35 + 5*s35**2)) + 2*me2*(-6*tt**3 + 2*s15**3 + 2*tt**2*s25 - 10*tt*s25**2 +&
          & 8*s25**3 + s15**2*(-9*tt + 12*s25 - 4*s35) + 4*ss**2*(-2*tt + s15 + 4*s25 - s35&
          &) + 7*tt**2*s35 + 4*tt*s25*s35 - 2*s25**2*s35 - 3*tt*s35**2 + s15*(-3*tt**2 - 22&
          &*tt*s25 + 20*s25**2 + 8*tt*s35 - 8*s25*s35 + 2*s35**2) - 2*ss*(4*tt**2 - 16*tt*s&
          &25 + 12*s25**2 + 3*tt*s35 + s25*s35 - 2*s35**2 + s15*(-9*tt + 9*s25 + 2*s35)))))&
          &/(s25*(tt + s15 - s35)))*pvd1(7))/tt**2 + 64*Pi**2*((-4*(tt + s15)*(-(me2*(2*tt &
          &+ s15 - 3*s35)) + s15*(tt + s15 - s35)))/(tt*s15) - (2*(tt + s15)*(-(me2*(2*tt +&
          & s15 - 3*s35)) + s15*(tt + s15 - s35)))/(s15*(4*me2 - ss - tt + s25)) + (4*(tt +&
          & s15)*(-((tt + s15 - s35)*(-tt + s35)) + me2*(-2*tt - 3*s15 + s35)))/(tt*s35) + &
          &(2*(tt + s15)*(-((tt + s15 - s35)*(-tt + s35)) + me2*(-2*tt - 3*s15 + s35)))/(s3&
          &5*(4*me2 - ss - tt + s35)) + (64*me2**3 - 10*tt**3 - 15*tt**2*s15 - 5*tt*s15**2 &
          &+ 2*s15**3 + 5*tt**2*s25 + 7*tt*s15*s25 + 6*s15**2*s25 + 2*tt*s25**2 + 4*s15*s25&
          &**2 + 2*ss**2*(s15 - s35) - 8*me2**2*(8*ss + 4*tt - 5*s15 - 6*s25 - s35) + 10*tt&
          &**2*s35 + 8*tt*s15*s35 - 2*s15**2*s35 - 5*tt*s25*s35 - 7*s15*s25*s35 - 2*s25**2*&
          &s35 + 2*me2*(8*ss**2 + 6*tt**2 - tt*s15 + 5*s15**2 - 4*tt*s25 + 12*s15*s25 + 4*s&
          &25**2 - 4*ss*(-2*tt + 3*s15 + 3*s25) - 3*tt*s35 - s15*s35) + ss*(-4*tt**2 - 3*s1&
          &5**2 - 2*tt*s25 - 3*s15*(tt + 2*s25 - s35) + tt*s35 + 4*s25*s35))/(s25*(tt + s15&
          & - s35)) + (64*me2**3 - 6*tt**3 - 17*tt**2*s15 - 13*tt*s15**2 - 7*tt**2*s25 - 9*&
          &tt*s15*s25 + 2*s15**2*s25 + 2*tt*s25**2 + 4*s15*s25**2 + 2*ss**2*(s15 - s35) + 1&
          &0*tt**2*s35 + 18*tt*s15*s35 + 4*s15**2*s35 + 7*tt*s25*s35 + s15*s25*s35 - 2*s25*&
          &*2*s35 - 4*tt*s35**2 - 4*s15*s35**2 - 8*me2**2*(8*ss + 4*tt - 3*s15 - 6*s25 + s3&
          &5) + ss*(4*tt**2 + 5*tt*s15 - 3*s15**2 - 2*tt*s25 - 6*s15*s25 - 3*tt*s35 + 3*s15&
          &*s35 + 4*s25*s35) + 2*me2*(8*ss**2 - 2*tt**2 - 7*tt*s15 + 3*s15**2 - 4*tt*s25 + &
          &8*s15*s25 + 4*s25**2 + 3*tt*s35 - 3*s15*s35 - 4*s25*s35 + 4*ss*(2*tt - 2*s15 - 3&
          &*s25 + s35)))/((tt + s15 - s35)*(s15 + s25 - s35)) + (96*me2**3 - 2*tt**3 - 7*tt&
          &**2*s15 - 5*tt*s15**2 + 2*s15**3 - 5*tt**2*s25 - 5*tt*s15*s25 + 4*s15**2*s25 + 2&
          &*tt*s25**2 + 4*s15*s25**2 + 2*ss**2*(s15 - s35) - 8*me2**2*(8*ss + 4*tt - 2*s15 &
          &- 6*s25 - s35) + 4*tt**2*s35 + 8*tt*s15*s35 + 5*tt*s25*s35 - s15*s25*s35 - 2*s25&
          &**2*s35 - 2*tt*s35**2 - 2*s15*s35**2 + ss*(4*tt**2 - 3*s15**2 - 2*tt*s25 - 3*tt*&
          &s35 + 4*s25*s35 + s15*(5*tt - 6*s25 + 3*s35)) + 2*me2*(4*ss**2 - s15**2 - 2*ss*(&
          &-2*tt + s15 + 2*s25) + s15*(-6*tt + s35) + tt*(-3*tt - 2*s25 + 3*s35)))/((4*me2 &
          &- ss - tt + s25)*(s15 + s25 - s35)) + (2*(48*me2**3 - 4*me2**2*(8*ss + 4*tt - 3*&
          &s15 - 8*s25 - 2*s35) + (tt + s15)*(-2*tt + s25)*(tt + s15 - s35) + me2*(4*ss**2 &
          &+ 4*s25**2 - (-tt + s15)*(tt - 2*s35) - 2*ss*(-2*tt + s15 + 4*s25 + 2*s35) + s25&
          &*(-6*tt + 4*s35))))/(s25*(4*me2 - ss - tt + s35)))*pvd1(8) + (64*Pi**2*((4*(-tt &
          &+ s25)*(-(me2*(2*tt + s15 - 3*s35)) + s15*(tt + s15 - s35))*(tt + s15 + s25 - s3&
          &5))/s15 + (2*tt*(-tt + s25)*(-(me2*(2*tt + s15 - 3*s35)) + s15*(tt + s15 - s35))&
          &*(tt + s15 + s25 - s35))/(s15*(4*me2 - ss - tt + s25)) - (4*(-tt + s25)*(tt + s1&
          &5 + s25 - s35)*(-((tt + s15 - s35)*(-tt + s35)) + me2*(-2*tt - 3*s15 + s35)))/s3&
          &5 - (2*tt*(-tt + s25)*(tt + s15 + s25 - s35)*(-((tt + s15 - s35)*(-tt + s35)) + &
          &me2*(-2*tt - 3*s15 + s35)))/(s35*(4*me2 - ss - tt + s35)) - (tt*(tt + s15 + s25 &
          &- s35)*(4*tt**3 + 6*tt**2*s15 + tt*s15**2 - tt*s15*s25 - 2*tt*s25**2 - 2*s15*s25&
          &**2 + ss*(s15 - s35)*(2*tt + s15 - s35) - 24*me2**2*(s15 + 2*s25 - s35) - 8*tt**&
          &2*s35 - 5*tt*s15*s35 + 3*tt*s25*s35 + 3*s15*s25*s35 + 2*s25**2*s35 + 4*tt*s35**2&
          & - 3*s25*s35**2 + 2*me2*(s15**2 + 4*tt*s25 - 2*s25**2 + s15*(tt + 2*s25 - 2*s35)&
          & + 2*ss*(s15 + 2*s25 - s35) - tt*s35 - 2*s25*s35 + s35**2)))/((4*me2 - ss - tt +&
          & s25)*(s15 + s25 - s35)) + (tt*(tt + s15 + s25 - s35)*(-8*tt**3 - 12*tt**2*s15 -&
          & 3*tt*s15**2 + 2*tt**2*s25 + 5*tt*s15*s25 + 2*s15**2*s25 + 4*tt*s25**2 + 4*s15*s&
          &25**2 - ss*(s15 - s35)*(2*tt + s15 - s35) + 16*me2**2*(s15 + 2*s25 - s35) + 14*t&
          &t**2*s35 + 9*tt*s15*s35 - 7*tt*s25*s35 - 7*s15*s25*s35 - 4*s25**2*s35 - 6*tt*s35&
          &**2 + 5*s25*s35**2 + 2*me2*(s15**2 - 4*tt*s25 + 2*s15*s25 + 4*s25**2 - 4*ss*(s15&
          & + 2*s25 - s35) - 2*s15*s35 - 2*s25*s35 + s35**2)))/((tt + s15 - s35)*(s15 + s25&
          & - s35)) - (tt*(-tt + s25)*(-8*tt**3 - 12*tt**2*s15 - 3*tt*s15**2 + 2*s15**3 - 2&
          &*tt**2*s25 + 3*tt*s15*s25 + 6*s15**2*s25 + 4*tt*s25**2 + 4*s15*s25**2 + ss*(s15 &
          &- s35)*(2*tt + s15 - s35) + 16*me2**2*(s15 + 2*s25 - s35) + 14*tt**2*s35 + 9*tt*&
          &s15*s35 - 4*s15**2*s35 - tt*s25*s35 - 9*s15*s25*s35 - 4*s25**2*s35 - 6*tt*s35**2&
          & + 2*s15*s35**2 + 3*s25*s35**2 + 2*me2*(s15**2 - 4*tt*s25 + 4*s25**2 - 4*ss*(s15&
          & + 2*s25 - s35) + 4*tt*s35 + 2*s25*s35 - 3*s35**2 + 2*s15*(-2*tt + 3*s25 + s35))&
          &))/(s25*(tt + s15 - s35)) + (2*tt*(-tt + s25)*((-2*tt + s25)*(tt + s15 - s35)*(t&
          &t + s15 + s25 - s35) + 12*me2**2*(s15 + 2*s25 - s35) + me2*(2*s25**2 - 2*ss*(s15&
          & + 2*s25 - s35) + 4*s25*(-tt + s35) + (s15 - s35)*(-tt + 2*s35))))/(s25*(-4*me2 &
          &+ ss + tt - s35)))*pvd1(9))/tt**2 + 64*Pi**2*((-4*(-(me2*(2*tt + s15 - 3*s35)) +&
          & s15*(tt + s15 - s35))*(2*tt + s15 - s35))/(tt*s15) - (2*(-(me2*(2*tt + s15 - 3*&
          &s35)) + s15*(tt + s15 - s35))*(2*tt + s15 - s35))/(s15*(4*me2 - ss - tt + s25)) &
          &- (4*(-2*tt - s15 + s35)*(-((tt + s15 - s35)*(-tt + s35)) + me2*(-2*tt - 3*s15 +&
          & s35)))/(tt*s35) - (2*(-2*tt - s15 + s35)*(-((tt + s15 - s35)*(-tt + s35)) + me2&
          &*(-2*tt - 3*s15 + s35)))/(s35*(4*me2 - ss - tt + s35)) - (16*tt**3 + 30*tt**2*s1&
          &5 + 13*tt*s15**2 + 8*tt**2*s25 + 7*tt*s15*s25 - 2*s15**2*s25 - 2*tt*s25**2 - 4*s&
          &15*s25**2 + ss*(4*tt + 3*s15 + 2*s25 - 3*s35)*(s15 - s35) - 16*me2**2*(s15 + 2*s&
          &25 - s35) - 34*tt**2*s35 - 35*tt*s15*s35 - 4*s15**2*s35 - 9*tt*s25*s35 + s15*s25&
          &*s35 + 2*s25**2*s35 + 22*tt*s35**2 + 8*s15*s35**2 + s25*s35**2 - 4*s35**3 + 2*me&
          &2*(-3*s15**2 + 4*tt*s25 - 4*s25**2 - 2*s15*(tt + 2*s25 - 3*s35) + 4*ss*(s15 + 2*&
          &s25 - s35) + 2*tt*s35 + 4*s25*s35 - 3*s35**2))/((tt + s15 - s35)*(s15 + s25 - s3&
          &5)) - (8*tt**3 + 14*tt**2*s15 + 3*tt*s15**2 - 2*s15**3 + 4*tt**2*s25 + tt*s15*s2&
          &5 - 4*s15**2*s25 - 2*tt*s25**2 - 4*s15*s25**2 + ss*(4*tt + 3*s15 + 2*s25 - 3*s35&
          &)*(s15 - s35) - 24*me2**2*(s15 + 2*s25 - s35) - 18*tt**2*s35 - 15*tt*s15*s35 + 2&
          &*s15**2*s35 - 3*tt*s25*s35 + 5*s15*s25*s35 + 2*s25**2*s35 + 12*tt*s35**2 + 2*s15&
          &*s35**2 - s25*s35**2 - 2*s35**3 + 2*me2*(s15**2 + 6*tt*s25 + s15*(tt + 4*s25 - 2&
          &*s35) + 2*ss*(s15 + 2*s25 - s35) - tt*s35 - 4*s25*s35 + s35**2))/((4*me2 - ss - &
          &tt + s25)*(s15 + s25 - s35)) + (-16*tt**3 - 18*tt**2*s15 - 3*tt*s15**2 + 2*s15**&
          &3 + 8*tt**2*s25 + 9*tt*s15*s25 + 6*s15**2*s25 + 2*tt*s25**2 + 4*s15*s25**2 + ss*&
          &(s15 - s35)*(4*tt + s15 - 2*s25 - s35) + 16*me2**2*(s15 + 2*s25 - s35) + 22*tt**&
          &2*s35 + 9*tt*s15*s35 - 4*s15**2*s35 - 11*tt*s25*s35 - 9*s15*s25*s35 - 2*s25**2*s&
          &35 - 6*tt*s35**2 + 2*s15*s35**2 + 3*s25*s35**2 + 2*me2*(s15**2 - 4*tt*s25 + 4*s2&
          &5**2 - 4*ss*(s15 + 2*s25 - s35) + 6*tt*s35 - 3*s35**2 + 2*s15*(-3*tt + 4*s25 + s&
          &35)))/(s25*(tt + s15 - s35)) + (2*(12*me2**2*(s15 + 2*s25 - s35) + (-2*tt + s25)&
          &*(2*tt**2 + 3*tt*s15 + s15**2 - 3*tt*s35 - 2*s15*s35 + s35**2) + me2*(4*s25**2 -&
          & 2*ss*(s15 + 2*s25 - s35) + (s15 - s35)*(-tt + 2*s35) + s25*(-6*tt + 4*s35))))/(&
          &s25*(4*me2 - ss - tt + s35)))*pvd1(10) + 128*Pi**2*((tt*(me2*(2*tt + s15 - 3*s35&
          &) - s15*(tt + s15 - s35)))/(s15*(4*me2 - ss - tt + s25)) + (2*me2*(2*tt + s15 - &
          &3*s35))/s15 - 2*(tt + s15 - s35) + (tt*(-2*me2*s25 + (-2*tt + s25)*(tt + s15 - s&
          &35)))/(s25*(4*me2 - ss - tt + s35)) - (2*(me2*(2*tt + 3*s15 - s35) + (tt + s15 -&
          & s35)*(-tt + s35)))/s35 + (tt*(-((tt + s15 - s35)*(-tt + s35)) + me2*(-2*tt - 3*&
          &s15 + s35)))/(s35*(4*me2 - ss - tt + s35)) - (tt*(4*tt**2 + 3*tt*s15 - tt*s25 + &
          &2*me2*(s15 - s35) - 4*tt*s35 + s25*s35 + ss*(-s15 + s35)))/(s25*(tt + s15 - s35)&
          &) + (tt*(-4*tt**2 - 5*tt*s15 - tt*s25 + 2*me2*(s15 - s35) + 6*tt*s35 + 2*s15*s35&
          & + s25*s35 - 2*s35**2 + ss*(-s15 + s35)))/((tt + s15 - s35)*(s15 + s25 - s35)) +&
          & (tt*(-2*tt**2 + s15**2 - 2*me2*s25 + s15*(-2*tt + s25) + 3*tt*s35 - s35**2 + ss&
          &*(-s15 + s35)))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)))*pvd1(11) + (512*Pi*&
          &*2*((-2*(6*me2**2 + tt*(ss + tt - s25) + me2*(-3*ss - 4*tt + s15 + 3*s25)))/s25 &
          &+ (2*(6*me2**2 + tt*(ss - s15 - s25) + me2*(-3*ss - 3*tt + 2*s15 + 3*s25)))/(s15&
          & + s25 - s35) - ((-6*me2**2 + me2*(3*ss + 3*tt - 2*s15 - 3*s25) + tt*(-ss + s15 &
          &+ s25))*(tt + s15 - s35))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) - ((6*me2*&
          &*2 + tt*(ss + tt - s25) + me2*(-3*ss - 4*tt + s15 + 3*s25))*(tt + s15 - s35))/(s&
          &25*(4*me2 - ss - tt + s35)))*pvd1(12))/tt + (512*Pi**2*((2*me2*(tt + 2*s15 + 3*s&
          &25 - 2*s35) - 2*tt*(tt + s15 + s25 - s35))/(s15 + s25 - s35) - ((-(me2*(tt + 2*s&
          &15 + 3*s25 - 2*s35)) + tt*(tt + s15 + s25 - s35))*(tt + s15 - s35))/((4*me2 - ss&
          & - tt + s25)*(s15 + s25 - s35)) - ((tt*(tt - s25) + me2*(-tt + s15 + 3*s25 - s35&
          &))*(tt + s15 - s35))/(s25*(4*me2 - ss - tt + s35)) + (2*(tt*(-tt + s25) + me2*(t&
          &t - s15 - 3*s25 + s35)))/s25)*pvd1(13))/tt + 512*(me2 - tt)*Pi**2*(2/s25 + 2/(s1&
          &5 + s25 - s35) + (tt + s15 - s35)/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + &
          &(tt + s15 - s35)/(s25*(4*me2 - ss - tt + s35)))*pvd1(14) + (128*Pi**2*(2*me2 - s&
          &s + s15 + s25)*((-2*(-2*me2 + ss + tt - s25)**2)/s25 + (2*(2*me2 - ss - tt + s25&
          &)*(2*me2 - ss + s15 + s25))/(s15 + s25 - s35) + ((2*me2 - ss - tt + s25)*(2*me2 &
          &- ss + s15 + s25)*(tt + s15 - s35))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) &
          &- ((-2*me2 + ss + tt - s25)**2*(tt + s15 - s35))/(s25*(4*me2 - ss - tt + s35)))*&
          &pvd1(15))/tt + (128*Pi**2*((2*(2*me2 - ss + s15 + s25)*(2*me2*(tt + 2*s15 + 3*s2&
          &5 - 2*s35) - ss*(tt + 2*s15 + 3*s25 - 2*s35) + (-tt + s25)*(2*tt + 3*s15 + 3*s25&
          & - 2*s35)))/(s15 + s25 - s35) + ((2*me2 - ss + s15 + s25)*(2*me2*(tt + 2*s15 + 3&
          &*s25 - 2*s35) - ss*(tt + 2*s15 + 3*s25 - 2*s35) + (-tt + s25)*(2*tt + 3*s15 + 3*&
          &s25 - 2*s35))*(tt + s15 - s35))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) - (2&
          &*(2*me2 - ss - tt + s25)*(2*me2*(-tt + s15 + 3*s25 - s35) + (-tt + s25)*(tt + 3*&
          &s15 + 3*s25 - s35) + ss*(tt - s15 - 3*s25 + s35)))/s25 - ((2*me2 - ss - tt + s25&
          &)*(tt + s15 - s35)*(2*me2*(-tt + s15 + 3*s25 - s35) + (-tt + s25)*(tt + 3*s15 + &
          &3*s25 - s35) + ss*(tt - s15 - 3*s25 + s35)))/(s25*(4*me2 - ss - tt + s35)))*pvd1&
          &(16))/tt + 128*Pi**2*((2*(2*me2 - ss - tt + s25)*(2*me2 - ss + tt + 2*s15 + s25)&
          &)/s25 + (2*(2*me2 - ss - 2*tt - s15 + s25)*(2*me2 - ss + s15 + s25))/(s15 + s25 &
          &- s35) + ((2*me2 - ss - 2*tt - s15 + s25)*(2*me2 - ss + s15 + s25)*(tt + s15 - s&
          &35))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + ((2*me2 - ss - tt + s25)*(2*m&
          &e2 - ss + tt + 2*s15 + s25)*(tt + s15 - s35))/(s25*(4*me2 - ss - tt + s35)))*pvd&
          &1(17) + (128*Pi**2*((-2*(-tt + s25)*(2*me2*(tt + 2*s15 + 3*s25 - 2*s35) - ss*(tt&
          & + 2*s15 + 3*s25 - 2*s35) + (-tt + s25)*(2*tt + 3*s15 + 3*s25 - 2*s35)))/s25 + (&
          &(-tt + s25)*(2*me2*(tt + 2*s15 + 3*s25 - 2*s35) - ss*(tt + 2*s15 + 3*s25 - 2*s35&
          &) + (-tt + s25)*(2*tt + 3*s15 + 3*s25 - 2*s35))*(tt + s15 - s35))/(s25*(-4*me2 +&
          & ss + tt - s35)) + (2*(tt + s15 + s25 - s35)*(2*me2*(-tt + s15 + 3*s25 - s35) + &
          &(-tt + s25)*(tt + 3*s15 + 3*s25 - s35) + ss*(tt - s15 - 3*s25 + s35)))/(s15 + s2&
          &5 - s35) + ((tt + s15 - s35)*(tt + s15 + s25 - s35)*(2*me2*(-tt + s15 + 3*s25 - &
          &s35) + (-tt + s25)*(tt + 3*s15 + 3*s25 - s35) + ss*(tt - s15 - 3*s25 + s35)))/((&
          &4*me2 - ss - tt + s25)*(s15 + s25 - s35)))*pvd1(18))/tt + 256*Pi**2*((2*(2*me2*(&
          &tt + s15 + s25 - s35) - ss*(tt + s15 + s25 - s35) + (-tt + s25)*(2*tt + 2*s15 + &
          &s25 - s35)))/s25 - ((-2*me2*(tt + s15 + s25 - s35) + ss*(tt + s15 + s25 - s35) -&
          & (-tt + s25)*(2*tt + 2*s15 + s25 - s35))*(tt + s15 - s35))/(s25*(4*me2 - ss - tt&
          & + s35)) - ((tt + s15 - s35)*(-(ss*tt) + tt**2 + s15**2 + ss*s25 + 2*tt*s25 - s2&
          &5**2 - 2*me2*(-tt + s25) - tt*s35 - s15*(-3*tt + s35)))/((4*me2 - ss - tt + s25)&
          &*(s15 + s25 - s35)) + (2*(ss*tt - tt**2 - s15**2 - ss*s25 - 2*tt*s25 + s25**2 + &
          &2*me2*(-tt + s25) + tt*s35 + s15*(-3*tt + s35)))/(s15 + s25 - s35))*pvd1(19) + 1&
          &28*tt*Pi**2*((2*(2*me2 - ss - 2*tt - s15 + s25))/s25 - (2*(2*me2 - ss + tt + 2*s&
          &15 + s25))/(s15 + s25 - s35) - ((2*me2 - ss + tt + 2*s15 + s25)*(tt + s15 - s35)&
          &)/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + ((2*me2 - ss - 2*tt - s15 + s25)&
          &*(tt + s15 - s35))/(s25*(4*me2 - ss - tt + s35)))*pvd1(20) + (128*Pi**2*(-tt + s&
          &25)*(-2 + (2*tt)/s25 + ((-tt + s25)*(tt + s15 - s35))/(s25*(-4*me2 + ss + tt - s&
          &35)) + (2*(tt + s15 + s25 - s35))/(s15 + s25 - s35) + ((tt + s15 - s35)*(tt + s1&
          &5 + s25 - s35))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)))*(tt + s15 + s25 - s&
          &35)*pvd1(21))/tt + 128*Pi**2*((2*(-tt + s25)*(3*tt + 2*s15 + s25 - 2*s35))/s25 +&
          & ((-tt + s25)*(3*tt + 2*s15 + s25 - 2*s35)*(tt + s15 - s35))/(s25*(4*me2 - ss - &
          &tt + s35)) - (2*(3*tt**2 + 4*tt*s15 + s15**2 + 2*tt*s25 - s25**2 - 4*tt*s35 - 2*&
          &s15*s35 + s35**2))/(s15 + s25 - s35) - ((tt + s15 - s35)*(3*tt**2 + 4*tt*s15 + s&
          &15**2 + 2*tt*s25 - s25**2 - 4*tt*s35 - 2*s15*s35 + s35**2))/((4*me2 - ss - tt + &
          &s25)*(s15 + s25 - s35)))*pvd1(22) + 128*tt*Pi**2*((-2*(3*tt + 2*s15 + s25 - 2*s3&
          &5))/(s15 + s25 - s35) - ((3*tt + 2*s15 + s25 - 2*s35)*(tt + s15 - s35))/((4*me2 &
          &- ss - tt + s25)*(s15 + s25 - s35)) + (2*(-3*tt - s15 + s25 + s35))/s25 + ((tt +&
          & s15 - s35)*(-3*tt - s15 + s25 + s35))/(s25*(4*me2 - ss - tt + s35)))*pvd1(23) +&
          & 128*tt**2*Pi**2*(-2/s25 - 2/(s15 + s25 - s35) - (tt + s15 - s35)/((4*me2 - ss -&
          & tt + s25)*(s15 + s25 - s35)) - (tt + s15 - s35)/(s25*(4*me2 - ss - tt + s35)))*&
          &pvd1(24))/(16.*Pi**2)

          !print*,"5",diags

  end if

  if (box60) then

  diags =  diags + ((64*Pi**2*((8*me2**2 - 4*me2*s15)/(4*me2 - ss - tt + s25) + (2*(4*me2**&
          &2 - 2*me2*(ss - s25) + s15*s25))/tt - (2*me2*s15*(4*me2 - 2*ss + 2*s15 + s25))/(&
          &s25*(4*me2 - ss - tt + s35)) + (2*me2*s15*(4*me2 - 2*ss - 2*tt + s15 + s25 + s35&
          &))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) - (2*me2*s15*(4*me2 - 2*tt - s25 &
          &+ 2*s35))/(s35*(4*me2 - ss - tt + s35)) - (2*s15*(4*me2**2 - (ss - s15 - s25)*(-&
          &tt + s35) + me2*(-2*ss - 2*tt + s15 + s35)))/(tt*s35) + (2*s15*(4*me2**2 + (ss -&
          & s25)*(ss + tt + s15 - s35) + me2*(-4*ss - 2*tt - s15 + s25 + 2*s35)))/((tt + s1&
          &5 - s35)*(s15 + s25 - s35)) - (s15*(8*me2**2 + 2*ss**2 + tt*s15 - tt*s25 + s25*s&
          &35 - ss*(s15 + 2*s25 + s35) + me2*(-8*ss + 4*s15 + 6*s25 + 2*s35)))/(s25*(tt + s&
          &15 - s35)))*pvb11(1))/s15**2 + (64*Pi**2*((4*(-2*me2**2 + me2*s15))/(4*me2 - ss &
          &- tt + s25) - (2*(4*me2**2 - 2*me2*(ss - s25) + s15*s25))/tt + (2*s15*(4*me2**2 &
          &+ ss*(ss - s15 - s25) + me2*(-4*ss + 3*(s15 + s25))))/(s25*(tt + s15 - s35)) + (&
          &2*me2*s15*(4*me2 - 2*ss + s15 + s25))/(s25*(4*me2 - ss - tt + s35)) - (2*me2*s15&
          &*(4*me2 - 2*ss - 2*tt + s15 + s25 + s35))/((4*me2 - ss - tt + s25)*(s15 + s25 - &
          &s35)) + (2*me2*s15*(4*me2 - 2*tt + s15 - s25 + 2*s35))/(s35*(4*me2 - ss - tt + s&
          &35)) + (s15*(8*me2**2 + tt*s15 - 3*tt*s25 + ss*(2*tt + 3*s15 - 3*s35) - 2*me2*(2&
          &*ss + 2*tt + 3*s15 + s25 - 3*s35) + 3*s25*s35))/(tt*s35) - (2*s15*(4*me2**2 + (s&
          &s - s25)*(ss + tt + s15 - s35) + me2*(-4*ss - 2*tt - s15 + s25 + 2*s35)))/((tt +&
          & s15 - s35)*(s15 + s25 - s35)))*pvb2(1))/s15**2 + 128*Pi**2*(-((me2*s25)/(s15*(t&
          &t + s15 - s35)*(s15 + s25 - s35))) + (-(s15*(ss + tt - s35)) + me2*(3*s15 + s25 &
          &- s35))/(tt*s15*s35) + (2*me2)/(s25*(4*me2 - ss - tt + s35)) + (me2*(-s25 + s35)&
          &)/(s15*(4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + (-(tt*s25) - ss*s35 + s15*s3&
          &5 + s25*s35 + me2*(s15 + s25 + s35))/(s15*s25*(tt + s15 - s35)))*pvb3(1) + (128*&
          &Pi**2*((4*(-2*me2**2 + me2*s15))/(4*me2 - ss - tt + s25) + (s15*(4*me2**2 + ss**&
          &2 + tt*s15 - ss*(tt + s15 + s25) + me2*(-4*ss + 2*tt + 4*s15 + 2*s25 - s35)))/(s&
          &25*(tt + s15 - s35)) + (s15*(-4*me2**2 + me2*(4*ss - 3*s15) - (ss - tt - s15 - s&
          &25)*(ss + tt + s15 - s35)))/((tt + s15 - s35)*(s15 + s25 - s35)) - (me2*s15*(-8*&
          &me2 + 4*tt + s15 + s25 - 3*s35))/(s35*(4*me2 - ss - tt + s35)) + (me2*s15*(8*me2&
          & - 4*ss + 3*s15 + s25 + s35))/(s25*(4*me2 - ss - tt + s35)) - (2*me2*s15*(4*me2 &
          &- 2*ss - 2*tt + s15 + 2*s35))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + (s15&
          &*(4*me2**2 - tt**2 - 2*tt*s15 - tt*s25 + ss*(tt - s35) + tt*s35 + s15*s35 + s25*&
          &s35 + me2*(-2*ss + s15 + s35)))/(tt*s35) - (4*me2**2 + 2*me2*(-ss + tt + s25) + &
          &s15*(s25 + s35))/tt)*pvb5(1))/s15**2 + (64*Pi**2*((16*me2**2 - 8*me2*s15)/(4*me2&
          & - ss - tt + s25) - (2*s15*(4*me2**2 + ss**2 + tt*s15 - ss*(tt + s15 + s25) + me&
          &2*(-4*ss + 2*tt + 4*s15 + 2*s25 - s35)))/(s25*(tt + s15 - s35)) + (2*s15*(4*me2*&
          &*2 + me2*(-4*ss + 3*s15 + s25) + (ss - tt - s15 - s25)*(ss + tt + s15 - s35)))/(&
          &(tt + s15 - s35)*(s15 + s25 - s35)) - (2*me2*s15*(8*me2 - 4*ss + 3*s15 + s25 + s&
          &35))/(s25*(4*me2 - ss - tt + s35)) - (2*me2*s15*(8*me2 - 4*tt - s25 + 3*s35))/(s&
          &35*(4*me2 - ss - tt + s35)) + (2*me2*s15*(8*me2 - 4*ss - 4*tt + 2*s15 + s25 + 3*&
          &s35))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) - (s15*(8*me2**2 - 2*tt**2 - 3&
          &*tt*s15 - 3*tt*s25 + ss*(2*tt + s15 - 3*s35) - 4*me2*(ss - s35) + 2*tt*s35 + 2*s&
          &15*s35 + 3*s25*s35))/(tt*s35) + (2*(4*me2**2 + 2*me2*(-ss + tt + s25) + s15*(s25&
          & + s35)))/tt)*pvb7(1))/s15**2 + (64*Pi**2*((2*(4*me2**2*(2*ss + tt - 2*s15 - 2*s&
          &25) - 2*me2*(2*ss**2 - tt*s15 + s15**2 + ss*(tt - 2*s15 - 4*s25) - tt*s25 + 2*s1&
          &5*s25 + 2*s25**2) + s15*(-((s15 + s25)*(-tt + 2*s25)) + ss*(s15 + 2*s25))))/tt +&
          & (4*me2*(8*me2**2 + 2*me2*(ss - tt - 2*s15 - s25) + s15*(-2*ss + 3*s15 + 3*s25 -&
          & s35)))/(4*me2 - ss - tt + s25) + (2*me2*s15*(-16*me2**2 + 2*ss**2 + 2*tt*s15 + &
          &s15**2 + 2*tt*s25 + 2*s15*s25 + 2*s25**2 + 4*me2*(ss + tt - s35) - 2*ss*(tt + 2*&
          &s15 + 2*s25 - s35) - 3*s15*s35 - 3*s25*s35))/(s25*(4*me2 - ss - tt + s35)) + (2*&
          &me2*s15*(16*me2**2 - 2*ss**2 + 2*tt**2 - 2*s15**2 - 3*s15*s25 - 2*me2*(2*ss + 6*&
          &tt + s15 + 2*s25 - 3*s35) - 2*tt*s35 + 2*s15*s35 + ss*(4*s15 + 4*s25 + s35)))/((&
          &4*me2 - ss - tt + s25)*(s15 + s25 - s35)) - (2*me2*s15*(16*me2**2 + 2*tt**2 + 2*&
          &tt*s25 + 2*s15*s25 + 2*s25**2 - 2*tt*s35 - 2*s15*s35 - 3*s25*s35 + 2*me2*(2*ss -&
          & 6*tt - s15 - 4*s25 + s35) + ss*(-2*tt - 2*s15 + 3*s35)))/(s35*(4*me2 - ss - tt &
          &+ s35)) + (s15*(4*tt**2*s15 + 3*tt*s15**2 + 2*tt**2*s25 - 5*tt*s15*s25 - 4*tt*s2&
          &5**2 + ss*(5*s15**2 + s15*(6*tt - 7*s35) + 2*(-tt + 4*s25)*(tt - s35)) - 2*ss**2&
          &*(2*tt + s15 - 2*s35) - 4*me2**2*(4*ss + 2*tt - 3*s15 - 4*s25 - s35) - 4*tt*s15*&
          &s35 - 2*tt*s25*s35 + 5*s15*s25*s35 + 4*s25**2*s35 + 2*me2*(4*ss**2 + 2*tt**2 - 6&
          &*s15**2 - 4*tt*s25 + ss*(6*tt - 4*s25 - 6*s35) - 2*s15*(3*tt + s25 - 4*s35) - 2*&
          &tt*s35 + 5*s25*s35)))/(tt*s35) - (2*s15*(2*ss**3 + ss**2*(tt - 4*s15 - 4*s25) + &
          &ss*(-2*tt*s15 + 2*s15**2 - tt*s25 + 3*s15*s25 + 2*s25**2) + s15*(tt*s15 + s25*(t&
          &t - s35)) + 4*me2**2*(2*ss + tt - s15 - 2*s25 - s35) + me2*(-8*ss**2 - 4*s15**2 &
          &+ 2*ss*(-2*tt + 7*s15 + 7*s25) + s15*(4*tt - 7*s25 + s35) - s25*(-4*tt + 4*s25 +&
          & s35))))/(s25*(tt + s15 - s35)) + (2*s15*((2*ss**2 - 2*tt*s15 + ss*(tt - 3*s15 -&
          & 4*s25) - tt*s25 + 3*s15*s25 + 2*s25**2)*(ss + tt + s15 - s35) + me2**2*(8*ss - &
          &2*(-2*tt + s15 + 4*s25 + 3*s35)) + me2*(-8*ss**2 + 7*s15**2 + 2*s15*(3*tt + s25 &
          &- 3*s35) + 2*(-tt + 3*s25)*(tt - s35) + 2*ss*(-4*tt + 3*s15 + 5*s25 + 3*s35))))/&
          &((tt + s15 - s35)*(s15 + s25 - s35)))*pvc11(1))/s15**2 + (64*Pi**2*((-4*(4*me2**&
          &3 - 2*me2**2*(ss + tt - s25) + me2*s15*(tt + s25)))/(4*me2 - ss - tt + s25) + (2&
          &*me2*s15*(8*me2**2 - 2*me2*(2*ss + 4*tt + s15 - 2*s25) + ss*(2*tt + s15 - s25) +&
          & 2*tt*(tt - s35)))/(s35*(4*me2 - ss - tt + s35)) + (2*me2*s15*(-8*me2**2 - 2*ss*&
          &*2 - 2*tt**2 + tt*s15 + 3*tt*s25 + s25**2 + 2*me2*(4*ss + 4*tt + s15 - 4*s25 - 2&
          &*s35) - ss*(4*tt + s15 - 3*s25 - 2*s35) + 2*tt*s35 - 4*s25*s35))/((4*me2 - ss - &
          &tt + s25)*(s15 + s25 - s35)) - (2*me2*s15*(-(tt*s15) + tt*s25 + 2*s15*s25 - s25*&
          &*2 + me2*(4*s15 + 4*s25 - 2*s35) - s25*s35 + ss*(-2*s15 + s35)))/((tt + s15 - s3&
          &5)*(s15 + s25 - s35)) + (2*me2*s15*(8*me2**2 + 2*ss**2 - tt*s15 - 3*tt*s25 + s15&
          &*s25 + 3*s25*s35 - ss*(-2*tt + s15 + 3*s25 + s35) + 2*me2*(-4*ss - 2*tt + s15 + &
          &5*s25 + s35)))/(s25*(4*me2 - ss - tt + s35)) + (2*s15*(ss*s15*s25 + me2**2*(4*s1&
          &5 + 4*s25 - 2*s35) - me2*(2*ss*s15 + tt*s15 - tt*s25 + s25**2 - ss*s35 + s25*s35&
          &)))/(s25*(tt + s15 - s35)) + (s15*(2*me2*(s15*(-tt + 3*s25) + s25*(tt + s25 - 2*&
          &s35) - ss*(2*s15 + s25 - 2*s35)) + ss**2*(s15 - s35) + 4*me2**2*(s15 + s25 - s35&
          &) + ss*(tt*s15 - tt*s25 - 3*s15*s25 + 2*s25*s35) - s25*(tt*s15 + s25*(-tt + s35)&
          &)))/(tt*s35))*pvc11(2))/s15**2 + (128*Pi**2*((2*(-4*me2**3 + 2*me2**2*(tt + s15)&
          & + me2*s15*(-tt + 2*s15)))/(4*me2 - ss - tt + s25) + (-4*me2**2*s15 + s15*(s15*(&
          &tt - s25) + tt*s25) + 2*me2*(2*tt*s15 + ss*(-tt + s15) + tt*s25 - s15*s25))/tt -&
          & (me2*s15*(8*me2**2 + 2*ss*tt + 2*tt**2 + 3*ss*s15 + 2*tt*s15 + 2*s15**2 + s15*s&
          &25 - 2*me2*(2*ss + 4*tt + 5*s15 - 2*s35) - 2*tt*s35 - 4*s15*s35))/((4*me2 - ss -&
          & tt + s25)*(s15 + s25 - s35)) + (me2*s15*(8*me2**2 + 2*tt**2 + ss*s15 + tt*s15 +&
          & s15**2 - 2*me2*(4*tt + 3*s15) + tt*s25 - s15*s25 - 2*tt*s35 + 2*s15*s35))/(s35*&
          &(4*me2 - ss - tt + s35)) + (me2*s15*(8*me2**2 + tt*s15 - s15**2 + 2*ss*(tt + s15&
          &) - tt*s25 - 4*s15*s35 + 2*me2*(-2*ss - 2*tt - 3*s15 + s25 + s35)))/(s25*(4*me2 &
          &- ss - tt + s35)) + (s15*(2*tt**2*s15 + 2*tt*s15**2 + tt**2*s25 + ss*(-tt + 3*s1&
          &5)*(tt + s15 - s35) + 2*me2**2*(3*s15 + s25 - s35) - 2*tt*s15*s35 - tt*s25*s35 +&
          & s15*s25*s35 + me2*(-8*tt*s15 - 6*s15**2 - 2*tt*s25 - 3*s15*s25 + 6*s15*s35 + s2&
          &5*s35 + ss*(2*tt - 3*s15 + s35))))/(tt*s35) + (s15*(ss**2*(-tt + s15) + me2**2*(&
          &4*s15 - 2*s35) + me2*(2*ss*tt - 4*ss*s15 - 3*tt*s15 + s15**2 + tt*s25 + 4*s15*s2&
          &5 - 2*s25*s35) + s15*(-(tt*s15) + s25*(-2*tt + s35)) + ss*(-2*s15**2 + tt*s25 + &
          &s15*(tt - s25 + s35))))/(s25*(tt + s15 - s35)) - (s15*(me2**2*(4*s15 - 2*s35) + &
          &(2*tt*s15 + ss*(-tt + s15) + tt*s25 - s15*s25)*(ss + tt + s15 - s35) + me2*(ss*(&
          &2*tt - 4*s15) + s15*(-6*tt - 5*s15 + s25 + 4*s35))))/((tt + s15 - s35)*(s15 + s2&
          &5 - s35)))*pvc11(3))/s15**2 + (64*Pi**2*((2*(24*me2**3 + 2*s15*s25*(-ss + s15 + &
          &s25) + me2**2*(-28*ss + 4*tt + 14*s15 + 28*s25) + me2*(4*ss**2 - s15**2 - 8*ss*s&
          &25 + 6*s15*s25 + 4*s25**2 + 2*s15*s35)))/(4*me2 - ss - tt + s25) - (2*s15*(24*me&
          &2**3 - 2*(ss - s15)*(ss - s25)*(ss - s15 - s25) + me2**2*(-40*ss + 4*tt + 24*s15&
          & + 34*s25 + 4*s35) + me2*(18*ss**2 + 5*tt*s15 + 10*s15**2 + 19*s15*s25 + 10*s25*&
          &*2 - 5*s15*s35 - s25*s35 + s35**2 + ss*(-2*tt - 25*s15 - 28*s25 + s35))))/(s25*(&
          &4*me2 - ss - tt + s35)) + (2*s15*(24*me2**3 - 2*ss**3 + 2*ss**2*(-tt + s15 + 2*s&
          &25) - (s15 + s25)*(tt*s15 + s25*(tt - s35)) + me2**2*(-40*ss - 8*tt + 12*s15 + 3&
          &0*s25 + 12*s35) - ss*(s15**2 + s15*(-2*tt + 3*s25 - s35) + s25*(-2*tt + 2*s25 + &
          &s35)) + me2*(18*ss**2 - 2*tt**2 - 5*tt*s15 + 4*s15**2 - 8*tt*s25 + 11*s15*s25 + &
          &6*s25**2 + tt*s35 - 3*s15*s35 + 6*s25*s35 + s35**2 - 6*ss*(-2*tt + 2*s15 + 4*s25&
          & + s35))))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + (32*me2**3 + 8*me2**2*(&
          &-6*ss + tt + 4*s15 + 6*s25) - 2*s15*(s15*(tt - 4*s25) + tt*s25 - 4*s25**2 + ss*(&
          &s15 + 4*s25) - tt*s35) + 4*me2*(4*ss**2 + tt**2 + 2*s15**2 + ss*(tt - 5*s15 - 8*&
          &s25) - tt*s25 + 4*s25**2 + s15*(-2*tt + 8*s25 + s35)))/tt - (2*s15*(16*me2**3 - &
          &4*ss**2*tt - tt**3 - tt**2*s15 - 2*tt*s15**2 - tt**2*s25 - 5*tt*s15*s25 - 2*tt*s&
          &25**2 - 4*me2**2*(6*ss + tt - 3*s15 - 4*s25 - s35) + tt**2*s35 + 2*tt*s15*s35 + &
          &2*s15**2*s35 + 2*tt*s25*s35 + 3*s15*s25*s35 + 2*s25**2*s35 - s15*s35**2 - s25*s3&
          &5**2 - ss*(tt**2 + s15**2 + tt*s35 - s35**2 + 2*s25*(-2*tt + s35) + s15*(-4*tt +&
          & 2*s25 + s35)) + me2*(8*ss**2 + 4*s15**2 - 6*tt*s25 + 2*tt*s35 + 6*s25*s35 - s35&
          &**2 + s15*(-8*tt + 4*s25 + s35) - 2*ss*(-7*tt + 3*s15 + 4*s25 + 2*s35))))/(tt*s3&
          &5) - (2*s15*(-16*me2**3 + 4*ss**3 + tt**3 - tt**2*s25 + 4*tt*s15*s25 + 3*s15**2*&
          &s25 + 3*tt*s25**2 + 2*s15*s25**2 + 4*me2**2*(8*ss + tt - 4*s15 - 5*s25 - s35) - &
          &tt**2*s35 + tt*s15*s35 + tt*s25*s35 - 4*s15*s25*s35 - 3*s25**2*s35 - ss**2*(-5*t&
          &t + 3*s15 + 8*s25 + 2*s35) + ss*(2*tt**2 - 2*s15**2 - 7*tt*s25 + 4*s25**2 - tt*s&
          &35 + 5*s25*s35 + s15*(-5*tt + 2*s25 + 2*s35)) + me2*(-20*ss**2 + 14*ss*s15 + 3*s&
          &15**2 + s15*(9*tt - 5*s25 - 4*s35) + 6*ss*(-2*tt + 4*s25 + s35) - 2*s25*(-4*tt +&
          & 2*s25 + 3*s35))))/((tt + s15 - s35)*(s15 + s25 - s35)) - (s15*(32*me2**3 - 8*ss&
          &**3 + 4*tt**2*s15 + tt*s15**2 + 4*tt**2*s25 - tt*s15*s25 + 4*s15**2*s25 - 2*tt*s&
          &25**2 + 4*s15*s25**2 + 8*me2**2*(-8*ss + tt + 7*s15 + 7*s25) - 6*tt*s15*s35 - 2*&
          &s15**2*s35 - 6*tt*s25*s35 + 3*s15*s25*s35 + 2*s25**2*s35 + 2*s15*s35**2 + 2*s25*&
          &s35**2 + 2*ss**2*(-tt + 7*s15 + 8*s25 + 2*s35) + 2*me2*(20*ss**2 + 2*tt**2 - 3*t&
          &t*s15 + 7*s15**2 - 4*tt*s25 + 23*s15*s25 + 12*s25**2 - 2*tt*s35 + 3*s15*s35 + s3&
          &5**2 - 2*ss*(15*s15 + 16*s25 + s35)) - ss*(5*s15**2 + 8*s25**2 + 6*s25*(-tt + s3&
          &5) + 2*(-tt + s35)**2 + s15*(-4*tt + 16*s25 + 3*s35))))/(s25*(tt + s15 - s35)) -&
          & (2*s15*(24*me2**3 - 2*ss**2*tt - (s15 + s25)*(tt*s15 + s25*(tt - s35)) + me2**2&
          &*(-28*ss - 8*tt + 14*s15 + 24*s25 + 8*s35) - ss*(s15**2 + s15*(-2*tt + s25 - s35&
          &) + s25*(-2*tt + s35)) + me2*(4*ss**2 - 2*tt**2 - 8*tt*s15 + 3*s15**2 - 8*tt*s25&
          & + 5*s15*s25 + tt*s35 + 3*s15*s35 + 7*s25*s35 - ss*(-14*tt + s15 + 4*s25 + 7*s35&
          &))))/(s35*(4*me2 - ss - tt + s35)))*pvc16(1))/s15**2 + (128*Pi**2*((-2*(-8*me2**&
          &3 + 2*me2**2*(ss + 3*s15 - s25) + me2*s15*(-ss + s15 + s25)))/(4*me2 - ss - tt +&
          & s25) - (me2*s15*(16*me2**2 + 4*tt*s15 - 2*tt*s25 + s15*s25 - 2*me2*(2*ss + 4*tt&
          & + 8*s15 - s25 - 2*s35) + ss*(2*tt + 3*s15 - s35) - 3*s15*s35 + s25*s35))/(s35*(&
          &4*me2 - ss - tt + s35)) + (me2*s15*(16*me2**2 + 2*ss**2 + 5*tt*s15 + s15**2 - 2*&
          &tt*s25 + 2*s15*s25 - 4*me2*(3*ss + 2*tt + 3*s15 - s25 - 2*s35) + 2*ss*(tt + 2*s1&
          &5 - s25 - s35) - 6*s15*s35 + 2*s25*s35))/((4*me2 - ss - tt + s25)*(s15 + s25 - s&
          &35)) + (me2*s15*(-16*me2**2 - 2*ss**2 - tt*s15 + s15**2 + s15*s25 + 2*me2*(6*ss &
          &+ s15 - 4*s25 - s35) + s15*s35 - s25*s35 + ss*(-s15 + 2*s25 + s35)))/(s25*(4*me2&
          & - ss - tt + s35)) + (s15*(8*me2**3 - 2*me2**2*(4*ss + s15 - s25) - (-2*s15**2 +&
          & ss*(-tt + s15) + tt*s25 - s15*s25)*(ss + tt + s15 - s35) + me2*(2*ss**2 - 2*s15&
          &**2 + 2*ss*(-tt + s15 - s25) + s15*(-tt + 2*s25 - s35) + 2*tt*(-tt + s35))))/((t&
          &t + s15 - s35)*(s15 + s25 - s35)) + (8*me2**3 - 4*me2**2*(ss - tt + s15 - s25) +&
          & 2*me2*(-(ss*tt) + 2*ss*s15 - 3*s15**2 + tt*s25 - 2*s15*s25 + s15*s35) - s15*(s1&
          &5**2 + s15*s25 - s25*s35 + ss*(-s15 + s35)))/tt - (s15*(8*me2**3 + tt*s15**2 - t&
          &t**2*s25 + ss*(s15**2 + s15*(tt - 2*s35) + tt*(tt - s35)) - 2*me2**2*(2*ss + 5*s&
          &15 - s35) + s15**2*s35 + tt*s25*s35 + 2*me2*(-2*s15*s35 + tt*(-tt + s35) + ss*(2&
          &*s15 + s35))))/(tt*s35) + (s15*(-8*me2**3 + ss**2*(-tt + s15) + ss*tt*s25 + s15*&
          &*2*s35 + 2*me2**2*(4*ss - 2*tt - 2*s15 - 3*s25 + s35) - ss*s15*(-2*tt + s25 + 2*&
          &s35) + me2*(-2*ss**2 + 2*s15**2 + s25*(-3*tt + s35) - s15*(2*tt + s35) + ss*(4*t&
          &t - s15 + 2*s25 + s35))))/(s25*(tt + s15 - s35)))*pvc16(2))/s15**2 + (64*Pi**2*(&
          &(-4*(2*me2**2*(-2*tt + s15) + me2*s15*(tt + s15 - s35)))/(4*me2 - ss - tt + s25)&
          & + (2*me2*s15*(-4*tt*s15 + 4*me2*(-2*tt + s15) - 2*tt*s25 + s15*s25 + 2*s15*s35 &
          &+ s25*s35 - s35**2 - ss*(-4*tt + s15 + s35)))/(s25*(4*me2 - ss - tt + s35)) + (2&
          &*me2*s15*(-4*tt**2 + 3*tt*s15 + s15*s25 + ss*(-4*tt + 3*s15 - s35) + 3*tt*s35 - &
          &3*s15*s35 - s25*s35 + 2*s35**2 + me2*(8*tt - 8*s15 + 4*s35)))/((4*me2 - ss - tt &
          &+ s25)*(s15 + s25 - s35)) + (2*s15*(-4*me2**2*(-tt + s15) - (-s15**2 + ss*(-tt +&
          & s15) - s15*(-2*tt + s25) + tt*(tt + s25))*(ss + tt + s15 - s35) + me2*(s15**2 +&
          & 4*ss*(-tt + s15) + tt*s25 + s15*(6*tt + s25 - 3*s35) - tt*s35 - 2*s25*s35 + s35&
          &**2)))/((tt + s15 - s35)*(s15 + s25 - s35)) + (2*s15*(tt**3 + 2*tt**2*s15 + 4*me&
          &2**2*(-tt + s15) + tt**2*s25 - ss*(s15**2 + s15*(tt - 2*s35) + tt*(tt - s35)) - &
          &tt**2*s35 - tt*s25*s35 - s15*s35**2 - me2*(-s15**2 + 2*ss*(-tt + s15) + tt*s35 +&
          & s15*(tt + s35))))/(tt*s35) + (2*me2*s15*(4*tt**2 - 2*ss*s15 + s15**2 + tt*s25 -&
          & s15*s25 + 2*ss*s35 - 2*tt*s35 - s35**2 + me2*(8*s15 - 4*(2*tt + s35))))/(s35*(4&
          &*me2 - ss - tt + s35)) - (2*(4*me2**2*(-tt + s15) + s15*(s15**2 + s15*(s25 - s35&
          &) - (tt + s25)*s35 + ss*(-s15 + s35)) - 2*me2*(-2*s15**2 + ss*(-tt + s15) + tt*(&
          &tt + s25) + s15*(tt - s25 + s35))))/tt - (s15*(tt**2*s15 + tt*s15**2 - 8*me2**2*&
          &(-tt + s15) - 2*ss**2*(-tt + s15) + tt**2*s25 + tt*s15*s25 - ss*(s15**2 + s15*(7&
          &*tt - 2*s25 - 5*s35) + tt*(2*tt + 2*s25 - s35)) + 2*tt*s15*s35 - tt*s25*s35 - s1&
          &5*s25*s35 - 2*s15*s35**2 + 2*me2*(2*tt**2 + 4*ss*(-tt + s15) + 3*tt*s25 - 2*tt*s&
          &35 - 2*s25*s35 + s35**2 - s15*(-7*tt + s25 + 4*s35))))/(s25*(tt + s15 - s35)))*p&
          &vc16(3))/s15**2 + (64*Pi**2*((4*me2*(2*me2 - s15)*(2*me2 - ss + s15 + s25))/tt +&
          & (2*me2*(16*me2**2 - 2*me2*(ss + 2*tt + 2*s15 - s25) + s15*(s15 + s25 - 2*s35)))&
          &/(4*me2 - ss - tt + s25) + (me2*s15*(32*me2**2 + 2*ss**2 + 4*tt**2 + 3*tt*s15 - &
          &3*tt*s25 - 4*s15*s25 - 2*s25**2 + ss*(6*tt + 3*s15 - 5*s35) - 4*me2*(5*ss + 6*tt&
          & + 3*s15 - s25 - 4*s35) - 4*tt*s35 + 5*s25*s35))/((4*me2 - ss - tt + s25)*(s15 +&
          & s25 - s35)) + (2*me2*s15*(-16*me2**2 - ss**2 + 4*tt*s15 + 2*s15**2 + 2*tt*s25 +&
          & 3*s15*s25 + s25**2 + me2*(10*ss + 4*tt - 7*s15 - 4*s25 - 2*s35) - 3*s15*s35 - s&
          &25*s35 + ss*(-2*tt + s35)))/(s25*(4*me2 - ss - tt + s35)) + (4*me2*s15*(4*me2**2&
          & + ss**2 + ss*(tt - s25) - s15*s35 + me2*(-4*ss - 2*tt - 2*s15 + s35)))/((tt + s&
          &15 - s35)*(s15 + s25 - s35)) - (me2*s15*(32*me2**2 + 4*tt**2 + tt*s15 - tt*s25 -&
          & 4*me2*(ss + 6*tt + 2*s15 - 2*s35) - 4*tt*s35 - 4*s15*s35 - s25*s35 + ss*(2*tt +&
          & s15 + s35)))/(s35*(4*me2 - ss - tt + s35)) - (s15*(16*me2**3 - 2*tt**2*s15 - 2*&
          &tt*s15**2 - 2*tt**2*s25 + 2*tt*s25**2 - 2*ss**2*s35 + 5*tt*s15*s35 + 2*s15**2*s3&
          &5 + 3*tt*s25*s35 - 2*s15*s25*s35 - 2*s25**2*s35 - 2*s15*s35**2 - s25*s35**2 - 2*&
          &me2**2*(8*ss - 6*s15 - 5*s25 + 2*s35) + ss*(-2*tt*s15 - 2*s15**2 - 2*tt*s25 - 2*&
          &tt*s35 + 3*s15*s35 + 4*s25*s35 + s35**2) + me2*(4*ss**2 + 7*tt*s15 + 8*s15**2 + &
          &7*tt*s25 + 2*s15*s25 - 8*s15*s35 - 5*s25*s35 + ss*(-5*s15 - 4*s25 + 3*s35))))/(s&
          &25*(tt + s15 - s35)) - (s15*(16*me2**3 + tt**2*s15 + 2*tt*s15**2 + tt**2*s25 + 3&
          &*tt*s15*s25 + tt*s25**2 - 4*me2**2*(2*ss + 2*tt + 3*s15 + s25 - s35) - 3*tt*s15*&
          &s35 - 4*s15**2*s35 - 2*tt*s25*s35 - 4*s15*s25*s35 - s25**2*s35 + 2*s15*s35**2 + &
          &s25*s35**2 - ss**2*(s15 + s35) + ss*(2*s15**2 + (tt - s35)*s35 + s25*(-tt + 2*s3&
          &5) + s15*(s25 + 3*s35)) - 2*me2*(4*s15**2 + s25*(2*tt + s25 + s35) + 2*s15*(tt +&
          & 2*s25 + s35) - ss*(4*s15 + s25 + 2*(tt + s35)))))/(tt*s35))*pvc17(1))/s15**2 + &
          &(64*Pi**2*((4*me2*(s15*(s15 + s25) + me2*(-2*ss + 4*s15 + 2*s25)))/(4*me2 - ss -&
          & tt + s25) + (2*(s15*(s15 + s25)*(-ss + s15 + s25) + me2**2*(-4*ss + 8*s15 + 4*s&
          &25) + 2*me2*(ss**2 - 3*ss*s15 + 3*s15**2 - 2*ss*s25 + 4*s15*s25 + s25**2)))/tt -&
          & (2*me2*s15*(2*ss**2 - 3*ss*s15 + s15**2 - 2*ss*s25 + me2*(-4*ss + 6*s15 + 2*s25&
          &)))/(s25*(4*me2 - ss - tt + s35)) + (2*me2*s15*(-2*ss*tt + s15**2 + 2*me2*(2*ss &
          &- 5*s15 - 3*s25) + 2*tt*s25 + s25**2 + s15*(4*tt + 2*s25 - 5*s35) + 2*ss*s35 - 3&
          &*s25*s35))/(s35*(4*me2 - ss - tt + s35)) - (2*me2*s15*(-2*ss**2 + 3*ss*s15 + 4*t&
          &t*s15 + 2*s15**2 + 4*me2*(ss - 2*s15 - s25) + 2*tt*s25 + 2*s15*s25 + s25**2 - 5*&
          &s15*s35 - 3*s25*s35 + 2*ss*(-tt + s25 + s35)))/((4*me2 - ss - tt + s25)*(s15 + s&
          &25 - s35)) + (s15*(8*me2**2*(ss - 2*s15 - s25) + (ss - s15)*(-(s25*(tt + s35)) +&
          & ss*(2*tt + s15 + s35) - s15*(tt + 2*s35)) + 2*me2*(-2*ss**2 + 3*s15**2 + s15*(4&
          &*tt + 3*s25 - 5*s35) + s25*(2*tt + s25 - 3*s35) + ss*(-2*tt + 2*s15 + s25 + 2*s3&
          &5))))/(tt*s35) - (2*s15*(me2**2*(4*ss - 2*(3*s15 + s25)) + (ss**2 - 3*ss*s15 + 2&
          &*s15**2 - 2*ss*s25 + 3*s15*s25 + s25**2)*(ss + tt + s15 - s35) + me2*(-4*ss**2 +&
          & 4*tt*s15 + 2*s15**2 + 2*tt*s25 - 5*s15*s35 - 3*s25*s35 + ss*(-2*tt + 7*s15 + 5*&
          &s25 + 2*s35))))/((tt + s15 - s35)*(s15 + s25 - s35)) - (s15*(-2*ss**3 + 4*me2**2&
          &*(-2*ss + 3*s15 + s25) + 2*me2*(4*ss**2 - 9*ss*s15 + 5*s15**2 - 6*ss*s25 + 5*s15&
          &*s25 + s25**2) + ss**2*(5*s15 + 4*s25 + s35) + s15*(-(tt*s15) - tt*s25 + 2*s15*s&
          &35 + s25*s35) - ss*(3*s15**2 + s25*(-tt + 2*s25 + s35) + s15*(-tt + 4*s25 + 3*s3&
          &5))))/(s25*(tt + s15 - s35)))*pvc17(2))/s15**2 + (64*Pi**2*((4*me2*(4*me2**2 - 2&
          &*me2*(ss + tt - s15 - s25) + s15*(s15 + s25 - s35)))/(4*me2 - ss - tt + s25) + (&
          &2*(2*me2 - ss + s15 + s25)*(4*me2**2 - 2*me2*(ss + tt - s15 - s25) + s15*(s15 + &
          &s25 - s35)))/tt - (2*me2*s15*(8*me2**2 + 2*ss*tt + 2*tt**2 - tt*s25 - s25**2 - s&
          &15*(tt + s25 - 2*s35) - ss*s35 - 2*tt*s35 + 3*s25*s35 - s35**2 + me2*(-4*ss - 8*&
          &tt + 4*s15 + 4*s25 + 2*s35)))/(s35*(4*me2 - ss - tt + s35)) - (2*me2*s15*(8*me2*&
          &*2 + 2*ss**2 - 2*tt*s15 - tt*s25 + s15*s35 + s25*s35 - ss*(-2*tt + 2*s15 + 2*s25&
          & + s35) + me2*(-8*ss - 4*tt + 6*s15 + 4*s25 + 2*s35)))/(s25*(4*me2 - ss - tt + s&
          &35)) + (s15*(-16*me2**3 + 4*me2**2*(4*ss + 4*tt - s25 - 2*s35) + 2*me2*(-2*ss**2&
          & - 2*tt**2 + s15**2 + 2*tt*s25 + s25**2 + s15*(tt + 2*s25) + 2*tt*s35 - 2*s25*s3&
          &5 + s35**2 + ss*(-6*tt - s15 + s25 + s35)) + (ss + tt - s35)*(-(s25*(tt + s35)) &
          &+ ss*(2*tt + s15 + s35) - s15*(tt + 2*s35))))/(tt*s35) - (2*s15*(-8*me2**3 + 2*m&
          &e2**2*(6*ss + 4*tt - 2*s25 - 3*s35) + (ss - s15 - s25)*(ss + tt - s15 - s25)*(ss&
          & + tt + s15 - s35) + me2*(-6*ss**2 - 2*tt**2 + 3*s15**2 + 5*tt*s25 + s15*(3*tt +&
          & 3*s25 - 5*s35) + 2*tt*s35 - 6*s25*s35 + s35**2 + ss*(-8*tt + 3*s15 + 7*s25 + 4*&
          &s35))))/((tt + s15 - s35)*(s15 + s25 - s35)) - (s15*(16*me2**3 - 2*ss**3 + tt**2&
          &*s15 - tt*s15**2 + tt**2*s25 - 2*tt*s15*s25 - tt*s25**2 - 4*me2**2*(6*ss + 2*tt &
          &- 6*s15 - 5*s25 - s35) - 3*tt*s15*s35 - 2*tt*s25*s35 + s15*s25*s35 + s25**2*s35 &
          &+ 2*s15*s35**2 + s25*s35**2 + 2*ss**2*(-tt + 2*s15 + 2*s25 + s35) - ss*(s15**2 -&
          & 4*tt*s25 + 2*s25**2 - tt*s35 + 3*s25*s35 + s35**2 + s15*(-5*tt + 3*s25 + 4*s35)&
          &) + 2*me2*(6*ss**2 + 2*s15**2 - ss*(-4*tt + 10*s15 + 9*s25 + 3*s35) + s25*(-5*tt&
          & + 2*s25 + 4*s35) + s15*(-6*tt + 4*s25 + 5*s35))))/(s25*(tt + s15 - s35)) - (2*m&
          &e2*s15*(-8*me2**2 - 2*ss**2 - 2*tt**2 + tt*s15 + s15**2 + 2*tt*s25 + 2*s15*s25 +&
          & s25**2 + 2*tt*s35 - 3*s15*s35 - 4*s25*s35 + s35**2 + ss*(s15 + 2*(-2*tt + s25 +&
          & s35)) + me2*(8*ss - 2*(s15 + 2*(-2*tt + s25 + s35)))))/((4*me2 - ss - tt + s25)&
          &*(s15 + s25 - s35)))*pvc17(3))/s15**2 + (64*Pi**2*((-2*me2)/(4*me2 - ss - tt + s&
          &35) + (tt*s15 + tt*s25 - 2*me2*(2*s15 + s25) - 2*s15*s35 - s25*s35 + ss*(s15 + s&
          &35))/(s15*(tt + s15 - s35)))*(-0.5 + 4*pvc17(4)))/s25 + (64*Pi**2*(-ss + s15 + s&
          &25)*((2*me2)/(4*me2 - ss - tt + s35) - (tt*s15 + tt*s25 - 2*me2*(2*s15 + s25) - &
          &2*s15*s35 - s25*s35 + ss*(s15 + s35))/(s15*(tt + s15 - s35)))*pvc17(5))/s25 + (6&
          &4*Pi**2*(-ss + s15 + s25)*((2*me2)/(4*me2 - ss - tt + s35) - (tt*s15 + tt*s25 - &
          &2*me2*(2*s15 + s25) - 2*s15*s35 - s25*s35 + ss*(s15 + s35))/(s15*(tt + s15 - s35&
          &)))*pvc17(6))/s25 + (64*me2*Pi**2*((-2*me2)/(4*me2 - ss - tt + s35) + (tt*s15 + &
          &tt*s25 - 2*me2*(2*s15 + s25) - 2*s15*s35 - s25*s35 + ss*(s15 + s35))/(s15*(tt + &
          &s15 - s35)))*pvc17(7))/s25 + (64*Pi**2*((2*(32*me2**3 + 2*s15*s25*(-ss + s15 + s&
          &25) + me2**2*(-26*ss + 6*s15 + 26*s25) + me2*(4*ss**2 - 2*s15**2 - 8*ss*s25 + 5*&
          &s15*s25 + 4*s25**2)))/(4*me2 - ss - tt + s25) - (2*s15*(32*me2**3 - 2*(ss - s15)&
          &*(ss - s25)*(ss - s15 - s25) + me2**2*(-42*ss + 24*s15 + 34*s25 + 6*s35) + me2*(&
          &17*ss**2 - 22*ss*s15 + 2*tt*s15 + 7*s15**2 - 26*ss*s25 - tt*s25 + 18*s15*s25 + 9&
          &*s25**2 - 3*s15*s35 + s25*s35 + s35**2)))/(s25*(4*me2 - ss - tt + s35)) + (2*(16&
          &*me2**3 + 4*me2**2*(-5*ss + tt + 3*s15 + 5*s25) + 2*me2*(3*ss**2 + tt**2 - 2*tt*&
          &s15 + s15**2 + ss*(tt - 3*s15 - 6*s25) - tt*s25 + 5*s15*s25 + 3*s25**2) + s15*(-&
          &(tt*s15) + 3*s15*s25 + 3*s25**2 + tt*s35 - s25*s35 + ss*(-s15 - 3*s25 + s35))))/&
          &tt - (s15*(32*me2**3 - 6*ss**3 + 2*tt**2*s15 - tt*s15**2 + 2*tt**2*s25 - 2*tt*s1&
          &5*s25 + 4*s15**2*s25 - tt*s25**2 + 4*s15*s25**2 + 2*ss**2*(-tt + 6*s15 + 6*s25) &
          &+ me2**2*(-56*ss + 8*tt + 52*s15 + 48*s25 - 4*s35) - tt*s15*s35 - 3*tt*s25*s35 +&
          & s15*s25*s35 + s25**2*s35 + s25*s35**2 - ss*(2*tt**2 - 4*tt*s15 + 5*s15**2 - 2*t&
          &t*s25 + 15*s15*s25 + 6*s25**2 - 2*tt*s35 + s25*s35 + s35**2) + 2*me2*(16*ss**2 +&
          & 2*tt**2 + 7*s15**2 - tt*s25 + 21*s15*s25 + 10*s25**2 - 2*tt*s35 - s15*s35 - 2*s&
          &25*s35 + s35**2 + ss*(-27*s15 - 25*s25 + 2*s35))))/(s25*(tt + s15 - s35)) - (s15&
          &*(32*me2**3 - 2*tt**3 + tt**2*s15 + tt**2*s25 - 5*tt*s15*s25 - 5*tt*s25**2 - 2*s&
          &s**2*(3*tt + s15 - 2*s35) - 8*me2**2*(5*ss + tt - s15 - 3*s25 - 2*s35) + 2*tt**2&
          &*s35 - 3*tt*s15*s35 + 6*s15*s25*s35 + 5*s25**2*s35 - s25*s35**2 + ss*(-2*tt**2 +&
          & 10*tt*s25 + tt*s35 - 9*s25*s35 + s35**2 - s15*(-5*tt + s25 + 3*s35)) + 2*me2*(6&
          &*ss**2 - s15**2 - 8*tt*s25 + ss*(12*tt + s15 - 6*s25 - 9*s35) + 2*tt*s35 + 10*s2&
          &5*s35 - s35**2 + s15*(-9*tt + s25 + 4*s35))))/(tt*s35) - (2*s15*(-16*me2**3 + 3*&
          &ss**3 + tt**3 - tt**2*s25 + 2*tt*s15*s25 + s15**2*s25 + 2*tt*s25**2 + s15*s25**2&
          & + 4*me2**2*(7*ss + tt - 3*s15 - 4*s25 - s35) - tt**2*s35 + tt*s15*s35 + tt*s25*&
          &s35 - 2*s15*s25*s35 - 2*s25**2*s35 - ss**2*(-4*tt + 2*s15 + 6*s25 + s35) + ss*(2&
          &*tt**2 - 3*tt*s15 - 5*tt*s25 + 2*s15*s25 + 3*s25**2 - tt*s35 + 3*s25*s35) + me2*&
          &(-16*ss**2 + 6*tt*s15 + 6*tt*s25 - 6*s15*s25 - 4*s25**2 - 5*s25*s35 + ss*(-10*tt&
          & + 11*s15 + 19*s25 + 4*s35))))/((tt + s15 - s35)*(s15 + s25 - s35)) - (s15*(64*m&
          &e2**3 + 4*me2**2*(-13*ss - 8*tt + s15 + 10*s25 + 5*s35) + me2*(8*ss**2 - 5*tt*s1&
          &5 + 8*s15**2 - 13*tt*s25 + 10*s15*s25 + 2*s25**2 - 2*tt*s35 - 6*s15*s35 + 15*s25&
          &*s35 - ss*(-26*tt + s15 + 8*s25 + 13*s35)) - 2*(2*ss**2*tt + (s15 + s25)*(tt*s15&
          & + s25*(tt - s35)) + ss*(s15**2 + s15*(-2*tt + s25 - s35) + s25*(-2*tt + s35))))&
          &)/(s35*(4*me2 - ss - tt + s35)) + (s15*(64*me2**3 - 4*me2**2*(21*ss + 8*tt - 5*s&
          &15 - 14*s25 - 7*s35) + me2*(34*ss**2 - 7*tt*s15 + 12*s15**2 - 17*tt*s25 + 18*s15&
          &*s25 + 12*s25**2 - 2*tt*s35 - 10*s15*s35 + 13*s25*s35 + 2*s35**2 - ss*(-26*tt + &
          &21*s15 + 44*s25 + 9*s35)) - 2*(2*ss**3 - 2*ss**2*(-tt + s15 + 2*s25) + (s15 + s2&
          &5)*(tt*s15 + s25*(tt - s35)) + ss*(s15**2 + s15*(-2*tt + 3*s25 - s35) + s25*(-2*&
          &tt + 2*s25 + s35)))))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)))*pvc6(1))/s15*&
          &*2 + (64*Pi**2*(8*me2**2 - 4*me2*(ss - s25) + 2*s15*s25 + (4*(4*me2**3 - 2*me2**&
          &2*(ss - s25) + me2*s15*s25))/(4*me2 - ss - tt + s25) - (2*me2*s15*(8*me2**2 + 2*&
          &ss*tt - tt*s25 - ss*s35 + s25*s35 + 2*me2*(-2*ss - 2*tt + s25 + s35)))/(s35*(4*m&
          &e2 - ss - tt + s35)) - (2*me2*s15*(8*me2**2 + 2*ss**2 - s25*(tt - s15 + s25 - 2*&
          &s35) - ss*(s15 + 2*s25 + s35) + 2*me2*(-4*ss + s15 + 3*s25 + s35)))/(s25*(4*me2 &
          &- ss - tt + s35)) + (2*s15*(tt*(ss - s25)*(ss + tt + s15 - s35) + 2*me2**2*(2*tt&
          & + 2*s15 + s25 - s35) + me2*(-2*tt**2 - 2*tt*s15 + 2*tt*s25 + 2*s15*s25 + 2*tt*s&
          &35 - s25*s35 + ss*(-4*tt - 2*s15 + s35))))/((tt + s15 - s35)*(s15 + s25 - s35)) &
          &+ (s15*(-(ss*tt*(2*tt + s15 - 2*s35)) + 2*tt*s25*(tt - s35) + ss**2*(s15 - s35) &
          &+ 4*me2**2*(-2*tt + s15 - s35) + ss*s25*(-tt + s35) + me2*(2*tt*(2*tt + s15 - 2*&
          &s35) - 2*s25*s35 + 4*ss*(tt - s15 + s35))))/(tt*s35) + (2*me2*s15*(8*me2**2 + 2*&
          &ss**2 - s25*(2*tt + s15 + s25 - 3*s35) + ss*(s15 - 2*(-tt + s25 + s35)) - 2*me2*&
          &(4*ss + s15 - 2*(-tt + s25 + s35))))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35))&
          & - (s15*(4*me2**2*(2*tt + 3*s15 + s25 - 2*s35) + ss*(-(tt*s15) - 3*tt*s25 + ss*(&
          &2*tt + s15 - s35) + s25*s35) + 2*me2*(tt*s15 + 4*tt*s25 + 2*s15*s25 - 2*s25*s35 &
          &+ ss*(-4*tt - 4*s15 + 3*s35))))/(s25*(tt + s15 - s35)))*pvc6(2))/s15**2 + (64*Pi&
          &**2*(-2*(2*me2*(-tt + s15) + s15*(s15 - s35)) - (4*(2*me2**2*(-tt + s15) + me2*s&
          &15*(s15 - s35)))/(4*me2 - ss - tt + s25) + (2*s15*(-2*me2**2*(s15 - s35) + tt*(-&
          &tt + s15)*(ss + tt + s15 - s35) - me2*(-tt + s15)*(2*tt + 2*s15 - s35)))/((tt + &
          &s15 - s35)*(s15 + s25 - s35)) + (2*me2*s15*(-2*tt**2 + tt*s15 + s15**2 - 4*me2*(&
          &-tt + s15) + 2*ss*(-tt + s15) + s15*s25 + 2*tt*s35 - 3*s15*s35 - s25*s35 + s35**&
          &2))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + (2*me2*s15*((-2*tt + s15)*(-tt&
          & + s35) + 2*me2*(-2*tt + s15 + s35)))/(s35*(4*me2 - ss - tt + s35)) + (s15*(ss*(&
          &s15 - s35)*(-3*tt + s35) + 2*me2*(-2*tt**2 + 4*tt*s15 - 2*tt*s35 - s15*s35 + s35&
          &**2) + (-tt + s35)*(tt*(-2*tt + s15) + s25*(-3*tt + s35))))/(tt*s35) + (2*me2*s1&
          &5*(2*ss*tt - 2*ss*s15 - 2*tt*s15 + s15**2 - s15*s25 + 2*s15*s35 + s25*s35 - s35*&
          &*2 + me2*(6*s15 - 2*(2*tt + s35))))/(s25*(4*me2 - ss - tt + s35)) + (s15*(4*me2*&
          &*2*(s15 - s35) + me2*(-4*tt**2 - 4*tt*s15 + 4*s15**2 + 6*tt*s35 - 2*s35**2) + (-&
          &tt + s35)*(tt*s15 + 3*tt*s25 - s25*s35 + ss*(-2*tt - s15 + s35))))/(s25*(tt + s1&
          &5 - s35)))*pvc6(3))/s15**2 + (64*Pi**2*((2*(2*me2 - ss + s15 + s25)*(32*me2**3 +&
          & 16*me2**2*(-2*ss + s15 + 2*s25) + 2*me2*(4*ss**2 + tt**2 - 2*tt*s15 + 2*s15**2 &
          &+ ss*(2*tt - 3*s15 - 8*s25) - 2*tt*s25 + 7*s15*s25 + 4*s25**2) - s15*(tt*s15 + t&
          &t*s25 - 3*s15*s25 - 4*s25**2 + ss*(s15 + 4*s25 - s35) - tt*s35 + s25*s35)))/tt -&
          & (2*s15*(96*me2**4 + 2*(ss - s15)*(ss - s25)*(-ss + s15 + s25)**2 - 8*me2**3*(20&
          &*ss + 2*tt - 16*s15 - 17*s25 - 2*s35) + 2*me2**2*(48*ss**2 + 32*s15**2 + s15*(60&
          &*s25 - 3*s35) - ss*(-2*tt + 75*s15 + 80*s25 + 2*s35) + 2*(tt**2 - tt*s25 + 16*s2&
          &5**2 - tt*s35 + s25*s35 + s35**2)) + me2*(-24*ss**3 + 11*s15**3 + s15**2*(2*tt +&
          & 34*s25 - 3*s35) + ss**2*(2*tt + 55*s15 + 58*s25 - 2*s35) + s15*(2*tt**2 + 2*tt*&
          &s25 + 33*s25**2 - tt*s35 - 3*s25*s35 + s35**2) - 2*ss*(tt**2 + 21*s15**2 + tt*s2&
          &5 + 22*s25**2 + s15*(2*tt + 44*s25 - 3*s35) - tt*s35 - 2*s25*s35 + s35**2) + s25&
          &*(2*tt**2 + 10*s25**2 - 3*tt*s35 - 2*s25*s35 + 2*s35**2))))/(s25*(4*me2 - ss - t&
          &t + s35)) + (2*(96*me2**4 - 8*me2**3*(14*ss - 9*s15 + 2*(tt - 7*s25)) + 2*s15*s2&
          &5*(-ss + s15 + s25)**2 + 2*me2**2*(20*ss**2 + 6*s15**2 - ss*(2*tt + 19*s15 + 40*&
          &s25) + 2*(tt**2 + tt*s25 + 10*s25**2) + s15*(2*tt + 27*s25 - 2*s35)) + me2*(-4*s&
          &s**3 + 4*s25**3 + 4*ss**2*(s15 + 3*s25) + s15**2*(-tt + 11*s25) + s15*(16*s25**2&
          & + 2*tt*s35 + s25*(tt + s35)) + ss*(s15**2 - 12*s25**2 - s15*(20*s25 + s35)))))/&
          &(4*me2 - ss - tt + s25) - (s15*(128*me2**4 + 8*ss**4 + 2*tt**2*s15**2 - tt*s15**&
          &3 + 6*tt**2*s15*s25 - 2*tt*s15**2*s25 + 4*s15**3*s25 + 4*tt**2*s25**2 - 3*tt*s15&
          &*s25**2 + 8*s15**2*s25**2 - 2*tt*s25**3 + 4*s15*s25**3 - 32*me2**3*(8*ss - 7*(s1&
          &5 + s25)) - tt*s15**2*s35 - 5*tt*s15*s25*s35 + s15**2*s25*s35 - 6*tt*s25**2*s35 &
          &+ s15*s25**2*s35 + 2*s25**3*s35 + s15*s25*s35**2 + 2*s25**2*s35**2 - 2*ss**3*(9*&
          &s15 + 2*(-tt + 6*s25 + s35)) + 2*ss**2*(9*s15**2 + 12*s25**2 + s15*(-2*tt + 19*s&
          &25) + (-tt + s35)**2 + s25*(-6*tt + 5*s35)) + 4*me2**2*(48*ss**2 + 31*s15**2 + s&
          &15*(-2*tt + 60*s25 - s35) - 2*ss*(-2*tt + 37*s15 + 40*s25 + 2*s35) + 2*(tt**2 - &
          &2*tt*s25 + 16*s25**2 - tt*s35 + s25*s35 + s35**2)) - ss*(5*s15**3 + s15**2*(-6*t&
          &t + 23*s25) + 2*s25*(3*tt**2 - 5*tt*s25 + 4*s25**2 - 5*tt*s35 + 4*s25*s35 + 2*s3&
          &5**2) + s15*(2*tt**2 + 24*s25**2 - 2*tt*s35 + s35**2 + s25*(-10*tt + s35))) - 2*&
          &me2*(32*ss**3 - 11*s15**3 - 4*ss**2*(-2*tt + 16*s15 + 19*s25 + 2*s35) + s15**2*(&
          &tt - 35*s25 + 3*s35) + s15*(-4*tt**2 + 7*tt*s25 - 36*s25**2 + 7*tt*s35 - 3*s35**&
          &2) - s25*(4*tt**2 - 6*tt*s25 + 12*s25**2 - 9*tt*s35 + 4*s25*s35 + 4*s35**2) + ss&
          &*(47*s15**2 + 56*s25**2 + 4*(-tt + s35)**2 + s15*(-6*tt + 97*s25 + 2*s35) + 2*s2&
          &5*(-7*tt + 6*s35)))))/(s25*(tt + s15 - s35)) - (s15*(192*me2**4 - 16*me2**3*(14*&
          &ss + 8*tt - 10*s15 - 12*s25 - 3*s35) + 4*me2**2*(20*ss**2 + 6*tt**2 + 7*s15**2 -&
          & 14*tt*s25 + 12*s25**2 - 3*tt*s35 + 14*s25*s35 + s15*(-19*tt + 22*s25 + 6*s35) -&
          & ss*(-26*tt + 23*s15 + 32*s25 + 12*s35)) + 2*(ss - s15 - s25)*(2*ss**2*tt + (s15&
          & + s25)*(tt*s15 + s25*(tt - s35)) + ss*(s15**2 + s15*(-2*tt + s25 - s35) + s25*(&
          &-2*tt + s35))) - me2*(8*ss**3 + 4*tt**3 - 8*s15**3 + 4*tt**2*s25 + 20*tt*s25**2 &
          &- 4*tt**2*s35 - 2*tt*s25*s35 - 20*s25**2*s35 - 4*ss**2*(-10*tt + 3*s15 + 4*s25 +&
          & 3*s35) + s15**2*(17*tt - 18*s25 + 6*s35) + s15*(37*tt*s25 - 14*s25**2 + 2*tt*s3&
          &5 - 21*s25*s35) + ss*(-4*tt**2 + 11*s15**2 - 52*tt*s25 + 8*s25**2 + 2*tt*s35 + 3&
          &2*s25*s35 + s15*(-54*tt + 28*s25 + 7*s35)))))/(s35*(4*me2 - ss - tt + s35)) - (2&
          &*s15*(-64*me2**4 + 16*me2**3*(8*ss + 2*tt - 4*s15 - 5*s25 - s35) - 2*me2**2*(48*&
          &ss**2 + 2*tt**2 + 8*s15**2 - 16*tt*s25 + 16*s25**2 - tt*s35 + 14*s25*s35 + s15*(&
          &-15*tt + 20*s25 + 4*s35) - 2*ss*(-14*tt + 21*s15 + 32*s25 + 6*s35)) - (ss - s15 &
          &- s25)*(4*ss**3 + s15**2*s25 - (tt**2 - 2*tt*s25 + 3*s25**2)*(-tt + s35) - ss**2&
          &*(-6*tt + s15 + 8*s25 + 2*s35) + s15*(tt*s25 + 2*s25**2 + tt*s35 - 2*s25*s35) + &
          &ss*(4*s25**2 + tt*(3*tt - 2*s15 - 2*s35) + s25*(-8*tt + 5*s35))) + me2*(32*ss**3&
          & + 2*s15**2*(4*tt + s25) - 4*ss**2*(-8*tt + 9*s15 + 17*s25 + 3*s35) + s15*(-2*tt&
          &**2 + 23*tt*s25 - 2*s25**2 + 4*tt*s35 - 17*s25*s35) - 2*(2*s25**3 + 3*tt*s25*(tt&
          & - s35) + tt**2*(-tt + s35) + s25**2*(-9*tt + 8*s35)) + ss*(8*tt**2 + 9*s15**2 -&
          & 46*tt*s25 + 40*s25**2 - 6*tt*s35 + 28*s25*s35 + s15*(-32*tt + 39*s25 + 10*s35))&
          &)))/((tt + s15 - s35)*(s15 + s25 - s35)) + (s15*(192*me2**4 - 16*me2**3*(20*ss +&
          & 8*tt - 11*s15 - 15*s25 - 5*s35) + 4*me2**2*(48*ss**2 + 6*tt**2 + 16*s15**2 - 18&
          &*tt*s25 + 24*s25**2 - 5*tt*s35 + 16*s25*s35 + 2*s35**2 + s15*(-17*tt + 35*s25 + &
          &2*s35) - ss*(-30*tt + 51*s15 + 72*s25 + 14*s35)) + 2*(ss - s15 - s25)*(2*ss**3 -&
          & 2*ss**2*(-tt + s15 + 2*s25) + (s15 + s25)*(tt*s15 + s25*(tt - s35)) + ss*(s15**&
          &2 + s15*(-2*tt + 3*s25 - s35) + s25*(-2*tt + 2*s25 + s35))) + me2*(-48*ss**3 + 1&
          &2*s15**3 + s15**2*(-19*tt + 30*s25 - 10*s35) + 2*ss**2*(-18*tt + 37*s15 + 54*s25&
          & + 4*s35) + 4*(3*s25**3 + tt**2*(-tt + s35) + s25*s35*(-tt + s35) + s25**2*(-5*t&
          &t + 4*s35)) + ss*(-43*s15**2 - 72*s25**2 + 2*(tt - 2*s35)*s35 - 24*s25*(-2*tt + &
          &s35) + s15*(50*tt - 108*s25 + 9*s35)) + s15*(32*s25**2 + 2*s35*(-2*tt + s35) + s&
          &25*(-39*tt + 11*s35)))))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + (s15*(-12&
          &8*me2**4 - 8*ss**3*tt + 2*tt**3*s15 - tt**2*s15**2 + 2*tt**3*s25 - tt**2*s15*s25&
          & + 7*tt*s15**2*s25 + 11*tt*s15*s25**2 + 4*tt*s25**3 + 32*me2**3*(6*ss + 2*tt - 3&
          &*s15 - 4*s25 - s35) - 2*tt**2*s15*s35 + 3*tt*s15**2*s35 - 2*tt**2*s25*s35 + 2*tt&
          &*s15*s25*s35 - 6*s15**2*s25*s35 - 2*tt*s25**2*s35 - 11*s15*s25**2*s35 - 4*s25**3&
          &*s35 + s15*s25*s35**2 + 2*s25**2*s35**2 + 2*ss**2*(-2*tt**2 + s15**2 + 8*tt*s25 &
          &+ s15*(8*tt - 2*s25 - 3*s35) - 2*s25*s35 + s35**2) - 4*me2**2*(24*ss**2 + 2*tt**&
          &2 + 4*s15**2 - 16*tt*s25 + 8*s25**2 + tt*s35 + 12*s25*s35 - 2*s35**2 - 2*ss*(-14&
          &*tt + 11*s15 + 16*s25 + 4*s35) + s15*(-17*tt + 14*s25 + 6*s35)) + 2*me2*(8*ss**3&
          & + 2*tt**3 + s15**3 - 4*tt**2*s25 + 16*tt*s25**2 + s15**2*(14*tt - 3*s25 - 6*s35&
          &) - 2*tt**2*s35 - 3*tt*s25*s35 - 12*s25**2*s35 + 4*s25*s35**2 + ss*(6*tt**2 - 38&
          &*tt*s15 + s15**2 - 40*tt*s25 + 18*s15*s25 + 8*s25**2 + 2*tt*s35 + 11*s15*s35 + 1&
          &6*s25*s35 - 4*s35**2) - s15*(2*tt**2 - 29*tt*s25 + 4*s25**2 + 3*tt*s35 + 20*s25*&
          &s35 - 3*s35**2) - 2*ss**2*(5*s15 + 2*(-7*tt + 4*s25 + s35))) + ss*(s15**2*(-5*tt&
          & + 3*s25 + 3*s35) + s15*(6*tt**2 - 22*tt*s25 + 4*s25**2 - 5*tt*s35 + 17*s25*s35 &
          &- s35**2) + 2*(tt**2*(-tt + s35) + s25**2*(-6*tt + 4*s35) + s25*(2*tt**2 + tt*s3&
          &5 - 2*s35**2)))))/(tt*s35))*pvd8(1))/s15**2 + (64*Pi**2*((-2*me2*(4*me2 + ss - 2&
          &*tt - s25)*(4*me2**2 - 2*me2*(ss - s25) + s15*s25))/(4*me2 - ss - tt + s25) - (2&
          &*(ss - s25)*(8*me2**3 + s15*s25*(-ss + s15 + s25) + me2**2*(-8*ss + 4*s15 + 8*s2&
          &5) + 2*me2*(ss**2 + s25*(2*s15 + s25) - ss*(s15 + 2*s25))))/tt + (2*me2*s15*(16*&
          &me2**3 + ss**3 + 2*me2*(2*s15**2 - s25*(2*tt + s25 - 2*s35) + ss*(4*tt + s15 + s&
          &25 - 2*s35) + s15*(6*s25 - s35)) - ss**2*(2*tt + s15 + 3*s25 - s35) + 4*me2**2*(&
          &-3*ss - 2*tt + 2*s25 + s35) + ss*(-2*s15**2 + s25*(2*tt + 3*s25 - 2*s35) + s15*(&
          &-2*s25 + s35)) + s25*(2*s15**2 + s15*(s25 - s35) + s25*(-s25 + s35))))/(s25*(4*m&
          &e2 - ss - tt + s35)) - (me2*s15*(32*me2**3 + 2*ss**3 - 8*me2**2*(3*ss + 4*tt + 3&
          &*s15 - s25 - 3*s35) - ss**2*(2*tt + s15 + 6*s25 + s35) + 2*me2*(4*tt**2 + 4*s15*&
          &*2 - 11*tt*s25 - 2*s25**2 + s15*(tt + 12*s25 - 6*s35) + ss*(10*tt + 7*s15 + 4*s2&
          &5 - 5*s35) - 4*tt*s35 + s25*s35) - s25*(-2*s15**2 + (4*tt + s25)*(-tt + s35) + s&
          &15*(tt + 2*s35)) + ss*(-4*s15**2 + 4*s25**2 - s15*(tt + 5*s25 - 6*s35) + 4*tt*(-&
          &tt + s35) + s25*(5*tt + 2*s35))))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + &
          &(me2*s15*(32*me2**3 - 8*me2**2*(ss + 4*tt + 4*s15 - 3*s35) + ss**2*(2*tt - 3*s15&
          & + s35) + s25*(2*s25**2 + s15*(tt - 2*s25 - 2*s35) + 4*tt*(tt - s35) + s25*(3*tt&
          & + s35)) - ss*(2*s15**2 + 2*s25**2 + 4*tt*(tt - s35) + s25*(tt + 2*s35) - 3*s15*&
          &(-tt + s25 + 2*s35)) - 2*me2*(2*ss**2 - 4*tt**2 - 2*s15**2 + 9*tt*s25 + 2*s25**2&
          & - ss*(6*tt + 11*s15 + 6*s25 - 7*s35) + 4*tt*s35 - 3*s25*s35 + s15*(-3*tt + 2*s2&
          &5 + 6*s35))))/(s35*(4*me2 - ss - tt + s35)) + (s15*(-2*ss**4 + 8*me2**3*(2*ss + &
          &s15 - 2*s25 - s35) + ss**3*(6*s25 + 4*s35) + ss**2*(-2*s15**2 + 4*tt*s25 - 6*s25&
          &**2 + 2*tt*s35 - 9*s25*s35 - s35**2 + s15*(-6*tt + s25 + s35)) + s25*(-(tt*s15**&
          &2) + s15*s25*s35 + tt*s15*(-2*tt + s35) - s25*(-tt + s35)*(-2*tt + s25 + s35)) -&
          & 4*me2**2*(6*ss**2 + s15**2 + ss*(2*s15 - 11*s25 - 5*s35) + s15*(4*tt - 4*s25 + &
          &s35) + 2*s25*(tt + 2*s25 + 3*s35)) + ss*(-(s15**2*s25) + s25*(2*tt**2 - 5*tt*s25&
          & + 2*s25**2 - 5*tt*s35 + 6*s25*s35 + 2*s35**2) + s15*(-s25**2 + s25*(tt - 2*s35)&
          & + tt*(-2*tt + s35))) + 2*me2*(6*ss**3 + 3*s15**2*s25 + ss**2*(s15 - 15*s25 - 8*&
          &s35) + s25*(2*tt**2 - 2*s25**2 + s25*(tt - 4*s35) + tt*s35 - s35**2) + ss*(3*s15&
          &**2 + 11*s25**2 - 2*s15*(-5*tt + s25) - 2*tt*s35 + 12*s25*s35 + s35**2) + s15*(s&
          &25**2 + tt*(2*tt - s35) + s25*(tt + s35)))))/(s25*(tt + s15 - s35)) + (s15*(2*ss&
          &**3*(-tt + s15 - 2*s35) + 16*me2**3*(ss - s15 - s25 + s35) + ss**2*(s15**2 + s15&
          &*(7*tt - 5*s25) + s35*(-3*tt + 9*s25 + s35)) - 4*me2**2*(4*ss**2 - s15**2 + 2*s2&
          &5*(-tt + s25 - 3*s35) + s15*(-3*tt + 3*s25 - s35) + ss*(2*tt - 7*s15 - 6*s25 + 7&
          &*s35)) + s25*(tt*s15*(3*tt + 2*s15 - 3*s35) + s25**2*(-tt + s35) + s25*(tt*(3*tt&
          & + s15) - 4*tt*s35 + s35**2)) + ss*(s15**2*(-tt + 2*s25) - s25*(-3*tt + 3*s25 + &
          &s35)*(-tt + 2*s35) + s15*(-3*tt*s25 + 3*s25**2 + tt*(tt + s35))) + 2*me2*(2*ss**&
          &3 + s15**2*(tt - 3*s25) + ss**2*(4*tt - 7*s15 - 4*s25 + 9*s35) - s15*(3*s25**2 -&
          & 2*s25*s35 + tt*(tt + s35)) - ss*(2*s15**2 + 4*tt*s25 - 2*s25**2 - 3*tt*s35 + 14&
          &*s25*s35 + s35**2 + s15*(10*tt - 9*s25 + s35)) + s25*(-tt**2 - 2*tt*s35 + s35**2&
          & + s25*(tt + 5*s35)))))/(tt*s35) - (2*s15*(8*me2**3*(ss - s25) - (ss - s25)**2*(&
          &ss - s15 - s25)*(ss + tt + s15 - s35) - 2*me2**2*(6*ss**2 - 2*s15**2 + ss*(2*tt &
          &+ s15 - 9*s25 - 2*s35) + s15*(-6*s25 + 3*s35) + s25*(-tt + 2*s25 + 4*s35)) + me2&
          &*(6*ss**3 + ss**2*(4*tt + s15 - 13*s25 - 4*s35) + s25*(4*s15**2 + 5*tt*s25 + s15&
          &*(3*tt + 4*s25 - 3*s35) - 4*s25*s35) + ss*(-4*s15**2 + s15*(-2*tt - 6*s25 + 5*s3&
          &5) + s25*(-7*tt + 7*s25 + 8*s35)))))/((tt + s15 - s35)*(s15 + s25 - s35)))*pvd8(&
          &2))/s15**2 + (64*Pi**2*((-2*me2*(4*me2**2 - 4*me2*s15 - s15**2)*(4*me2 + ss - 2*&
          &tt - s25))/(4*me2 - ss - tt + s25) + (2*(-4*me2**2 + 4*me2*s15 + s15**2)*(ss - s&
          &25)*(2*me2 - ss + s15 + s25))/tt + (2*me2*s15*(16*me2**3 + 4*me2**2*(ss - 4*tt -&
          & 8*s15 - 2*s25 + 3*s35) - s15*(4*tt**2 + 4*ss*s15 + 2*tt*s15 + s15**2 + 3*tt*s25&
          & + 2*s25**2 - ss*s35 - 4*tt*s35 - 4*s15*s35 + s25*s35) + me2*(4*tt**2 + 23*tt*s1&
          &5 + 14*s15**2 + 3*tt*s25 + 10*s15*s25 + 2*s25**2 - 4*tt*s35 - 14*s15*s35 + s25*s&
          &35 - ss*(2*tt + s15 + s35))))/(s35*(4*me2 - ss - tt + s35)) + (4*me2*s15*(8*me2*&
          &*3 - 2*me2**2*(ss + 2*tt + 4*s15 - s35) + s15*(ss**2 - 2*s15**2 - ss*(tt + 2*s25&
          &) + s25*(tt + s25 - 2*s35) + s15*(-s25 + s35)) - me2*(ss**2 + 3*s15**2 + s25*(2*&
          &tt + s25 - 3*s35) + ss*(-2*tt - 3*s15 - 2*s25 + s35) + s15*(-2*tt - s25 + s35)))&
          &)/(s25*(4*me2 - ss - tt + s35)) + (2*s15*(-8*me2**3*(ss - s25) - 2*s15*(ss - s25&
          &)*(-ss + s15 + s25)*(ss + tt + s15 - s35) + 2*me2**2*(4*ss**2 + s15**2 - tt*s25 &
          &- 7*s15*s25 + ss*(2*tt + 5*s15 - 5*s25 - 2*s35) + s15*s35 + 2*s25*s35) + me2*(-2&
          &*ss**3 + 4*s15**3 + s15**2*(tt + 3*s25 - 4*s35) - 2*ss**2*(tt + 4*s15 - 2*s25 - &
          &s35) + 2*s25**2*(-tt + s35) - 2*s15*s25*(-tt + s25 + s35) + 2*ss*s15*(-tt + 6*s2&
          &5 + s35) - 2*ss*s25*(-2*tt + s25 + 2*s35))))/((tt + s15 - s35)*(s15 + s25 - s35)&
          &) - (2*me2*s15*(16*me2**3 - 4*me2**2*(ss + 4*tt + 7*s15 + s25 - 3*s35) + me2*(-2&
          &*ss**2 + 2*s15**2 + s15*(17*tt + 8*s25 - 10*s35) + (-4*tt + 3*s25)*(-tt + s35) +&
          & ss*(2*tt + 5*s15 + 4*s25 + s35)) - s15*(-2*ss**2 + 3*s15**2 + 2*s15*(s25 - 2*s3&
          &5) + (-4*tt + s25)*(-tt + s35) + ss*(2*tt + 4*s25 + 3*s35))))/((4*me2 - ss - tt &
          &+ s25)*(s15 + s25 - s35)) + (s15*(8*me2**3*(2*ss + s15 - 2*s25 - s35) - 4*me2**2&
          &*(4*ss**2 + 6*s15**2 + ss*(5*s15 - 7*s25 - 4*s35) + s15*(6*tt - 4*s25 - 3*s35) +&
          & s25*(4*tt + 2*s25 + 3*s35)) + 2*me2*(2*ss**3 - 2*s15**3 - s15**2*(s25 - 2*s35) &
          &+ s15*(2*tt**2 - 3*tt*s25 + 4*s25**2 - tt*s35 + 8*s25*s35) - s25*(-2*tt**2 + 3*t&
          &t*s25 - tt*s35 + s25*s35 + s35**2) + ss*(6*s15**2 + 4*tt*s25 + 2*s25**2 + s15*(1&
          &4*tt - 13*s25 - 9*s35) - 2*tt*s35 + 5*s25*s35 + s35**2) + ss**2*(8*s15 - 4*(s25 &
          &+ s35))) + s15*(-4*ss**3 + tt*s15**2 + 3*s15*s25*(tt - s35) - ss*(4*tt**2 + 4*tt&
          &*s15 + s15**2 - 4*tt*s25 + 4*s25**2 - 6*tt*s35 - s15*s35 + 10*s25*s35 + 2*s35**2&
          &) + 2*s25*(s35*(-2*tt + s35) + s25*(tt + s35)) - 2*ss**2*(s15 - 4*(-tt + s25 + s&
          &35)))))/(s25*(tt + s15 - s35)) + (s15*(16*me2**3*(ss - s15 - s25 + s35) - 4*me2*&
          &*2*(2*ss**2 - 5*s15**2 - ss*(-2*tt + s15 + 2*s25 - 5*s35) - 4*s25*(tt + s35) + s&
          &15*(-5*tt - 3*s25 + 3*s35)) + 2*me2*(2*s15**3 + 2*s15**2*(tt + 2*s25 - 2*s35) + &
          &2*ss**2*(tt + s15 + 2*s35) - s15*(tt**2 - 2*tt*s25 + tt*s35 + 9*s25*s35) + s25*(&
          &-tt**2 + 3*tt*s25 - 2*tt*s35 + s25*s35 + s35**2) - ss*(13*s15**2 + 6*tt*s25 + s1&
          &5*(11*tt + 3*s25 - 10*s35) - 3*tt*s35 + 5*s25*s35 + s35**2)) + s15*(-3*tt*s15**2&
          & + ss**2*(4*tt + 6*s15 - 8*s35) + s15*(-2*tt**2 - 5*tt*s25 + 4*tt*s35 + s25*s35)&
          & - 2*s25*(tt**2 - 3*tt*s35 + s35**2 + s25*(tt + s35)) + ss*(-s15**2 + s15*(6*tt &
          &- 4*s25 + s35) + 2*(2*tt**2 - 4*tt*s35 + 5*s25*s35 + s35**2)))))/(tt*s35))*pvd8(&
          &3))/s15**2 + (64*Pi**2*((2*me2*(4*me2 + ss - 2*tt - s25)*(2*me2*(-tt + s15) + s1&
          &5*(s15 - s35)))/(4*me2 - ss - tt + s25) + (2*(ss - s25)*(2*me2 - ss + s15 + s25)&
          &*(2*me2*(-tt + s15) + s15*(s15 - s35)))/tt - (2*me2*s15*(ss**2*(tt - s15) - 2*tt&
          &*s15**2 + 2*s15**3 + 8*me2**2*(-tt + s15) + 2*tt**2*s25 - tt*s15*s25 + s15**2*s2&
          &5 + tt*s25**2 - s15*s25**2 + tt*s15*s35 - s15**2*s35 - 5*tt*s25*s35 + s15*s25*s3&
          &5 + 2*s25*s35**2 - ss*(s15**2 + 2*tt*(tt + s25) + s15*(tt - 2*s25 - 3*s35) - 3*t&
          &t*s35 + 2*s35**2) + 2*me2*(2*tt**2 + ss*(tt - s15) + 5*s15**2 - 3*tt*s35 - 6*s15&
          &*s35 + 2*s35**2)))/(s25*(4*me2 - ss - tt + s35)) + (2*s15*((-tt + s15)*(ss - s25&
          &)*(ss - s15 - s25)*(ss + tt + s15 - s35) + 2*me2**2*(3*s15**2 + 2*ss*(-tt + s15)&
          & + 2*tt*s25 - 2*s15*s25 - 5*s15*s35 + 2*s35**2) - me2*(-2*s15**3 + 4*ss**2*(-tt &
          &+ s15) + tt**2*s25 + ss*(-tt + s15)*(2*tt + s15 - 5*s25 - 2*s35) + s15**2*(tt - &
          &2*s25 + s35) + s15*s35*(-2*tt + s25 + 2*s35))))/((tt + s15 - s35)*(s15 + s25 - s&
          &35)) + (me2*s15*(4*tt**3 - tt**2*s15 + tt*s15**2 - 16*me2**2*(-tt + s15) + 3*tt*&
          &*2*s25 - tt*s15*s25 + 2*s15**2*s25 + 2*tt*s25**2 - 2*s15*s25**2 - 4*tt**2*s35 - &
          &4*tt*s15*s35 + tt*s25*s35 - 5*s15*s25*s35 + 4*s15*s35**2 + 4*me2*(-4*tt**2 + ss*&
          &(tt - s15) + 2*s15**2 - 2*tt*s25 + tt*s35 - 2*s35**2 + s15*(2*tt + 2*s25 + s35))&
          & + ss*(-5*s15**2 + tt*(-2*tt + 3*s35) + s15*(tt + 3*s35))))/(s35*(4*me2 - ss - t&
          &t + s35)) + (me2*s15*(-4*tt**3 + 3*tt**2*s15 - 5*tt*s15**2 + 2*s15**3 + 16*me2**&
          &2*(-tt + s15) - 2*ss**2*(-tt + s15) + 3*tt**2*s25 - 3*tt*s15*s25 + 4*tt**2*s35 +&
          & 4*tt*s15*s35 - 7*tt*s25*s35 + 3*s15*s25*s35 - 4*s15*s35**2 + 4*s25*s35**2 - ss*&
          &(s15**2 + 2*tt*(tt + 2*s25) + tt*s35 + 4*s35**2 - s15*(tt + 4*s25 + 7*s35)) + 4*&
          &me2*(4*tt**2 + ss*(tt - s15) + 4*s15**2 + tt*s25 - 3*tt*s35 + 4*s35**2 - s15*(tt&
          & + s25 + 8*s35))))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + (s15*(2*tt**3*s&
          &15 + 2*tt**2*s15**2 + tt*s15**3 - 2*ss**3*(-tt + s15) + 2*tt**3*s25 - tt**2*s15*&
          &s25 - 3*tt**2*s25**2 - tt*s15*s25**2 - 2*ss**2*(s15**2 + s15*(4*tt - 2*s25 - 3*s&
          &35) + 2*tt*(s25 - s35)) - 5*tt**2*s15*s35 - 2*tt*s15**2*s35 - 3*tt**2*s25*s35 + &
          &2*tt*s15*s25*s35 - s15**2*s25*s35 + 3*tt*s25**2*s35 + s15*s25**2*s35 + 2*tt*s15*&
          &s35**2 + tt*s25*s35**2 - 4*me2**2*(4*s15**2 + 2*ss*(-tt + s15) + 2*tt*s25 + s15*&
          &(tt - 2*s25 - 6*s35) - tt*s35 + 2*s35**2) + 2*me2*(-3*s15**3 + 4*ss**2*(-tt + s1&
          &5) - s15**2*(5*tt + s25 - 5*s35) + tt*s25*(-4*tt - 2*s25 + 5*s35) + ss*(7*tt*s15&
          & + 3*s15**2 + 7*tt*s25 - 7*s15*s25 - 4*tt*s35 - 6*s15*s35) + s15*(-4*tt**2 - 4*t&
          &t*s25 + 2*s25**2 + 7*tt*s35 + 4*s25*s35)) + ss*(s15**3 + s15**2*(4*tt + s25 - 3*&
          &s35) + s15*(2*tt**2 + 9*tt*s25 - 2*s25**2 - 5*tt*s35 - 7*s25*s35) + tt*(4*tt*s25&
          & + 2*s25**2 + 2*tt*s35 - 7*s25*s35 - s35**2))))/(s25*(tt + s15 - s35)) - (s15*(t&
          &t**3*s15 + 2*tt**2*s15**2 + 2*tt*s15**3 + tt**3*s25 - tt**2*s15*s25 + tt*s15**2*&
          &s25 - 3*tt**2*s25**2 - tt*s15*s25**2 - 8*me2**2*(ss*(tt - s15) + s15**2 - tt*s25&
          & + s15*(s25 - s35)) - 3*tt**2*s15*s35 - 4*tt*s15**2*s35 - 2*tt**2*s25*s35 + 3*tt&
          &*s25**2*s35 + s15*s25**2*s35 + 2*tt*s15*s35**2 + tt*s25*s35**2 - 2*ss**2*(tt**2 &
          &+ 2*tt*s15 + 2*s15**2 - 2*tt*s35 - 3*s15*s35) + ss*(2*s15**3 + s15*s25*(5*tt - 7&
          &*s35) + tt*s25*(6*tt - 7*s35) + s15**2*(2*tt + 3*s25 - 4*s35) + 3*tt*s15*(tt - s&
          &35) + tt*(tt - s35)*s35) - 2*me2*(3*s15**3 + 2*ss**2*(-tt + s15) - ss*(7*s15**2 &
          &+ s15*(tt + 2*s25 - 7*s35) + tt*(2*tt - 2*s25 - 3*s35)) + s15**2*(5*tt + 3*s25 -&
          & 7*s35) + 2*tt*s25*(2*tt - s35) + s15*(3*tt**2 + tt*s25 - 6*tt*s35 - 6*s25*s35 +&
          & 2*s35**2))))/(tt*s35))*pvd8(4))/s15**2 + (128*Pi**2*((-2*me2*s15*(ss + tt + s15&
          & - s35) + 4*me2**2*(2*s15 + s25 - s35))/((4*me2 - ss - tt + s25)*(s15 + s25 - s3&
          &5)) + (4*me2**2*(s15 - s35) - 2*me2*s15*(tt + s15 - s35))/((tt + s15 - s35)*(s15&
          & + s25 - s35)) + (2*me2*ss*s15 - 4*me2**2*(s15 + s25))/(s25*(4*me2 - ss - tt + s&
          &35)) + (2*me2*(-2*me2*(s15 - s35) + s15*(tt + s15 - s35)))/(s35*(4*me2 - ss - tt&
          & + s35)) - (2*me2*(tt*s15 + ss*(s15 - s35) + s25*(tt + s35)) + s15*(tt*s15 + s25&
          &*(tt - s35) + ss*(-2*tt - s15 + s35)))/(s25*(tt + s15 - s35)) + (-4*me2**2*(s15 &
          &- s35) + 2*me2*(2*tt*s15 + s15**2 + tt*s25 + ss*(s15 - s35) - s15*s35 + s25*s35)&
          & + s15*(tt*s15 + tt*s25 - s25*s35 + ss*(-2*tt - s15 + s35)))/(tt*s35))*pvd8(5))/&
          &s15 + (64*Pi**2*((-2*me2*s25*(2*me2 - ss + s15 + s25))/(4*me2 - ss - tt + s35) -&
          & (2*me2*(tt*s25**2 + ss*s25*(tt + s15 - s35) + ss**2*s35 + 4*me2**2*(s25 + s35) &
          &- 2*me2*(s25*(tt + s15 + s25 - s35) + ss*(s25 + 2*s35))))/((tt + s15 - s35)*(s15&
          & + s25 - s35)) + (2*me2*(tt*s25**2 + ss*s25*(tt + s15 - s35) + ss**2*s35 + 4*me2&
          &**2*(s25 + s35) - 2*me2*(s25*(tt + s15 + s25 - s35) + ss*(s25 + 2*s35))))/(s35*(&
          &4*me2 - ss - tt + s35)) - (2*me2*(-(s25**2*(-tt + s15 + s25)) + ss*s25*(tt + s15&
          & + s25 - s35) + ss**2*s35 + 4*me2**2*(s25 + s35) - 2*me2*(s25*(tt + s15 + 2*s25 &
          &- s35) + ss*(s25 + 2*s35))))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + (-8*m&
          &e2**3*(s15 - s35) + 4*me2**2*(s15*(tt - s25) + 3*ss*(s15 - s35) + 2*s25*s35) + m&
          &e2*(4*ss*s15*(-tt + s25) + 2*ss*s25*(tt - 4*s35) - 6*ss**2*(s15 - s35) + 2*s25*(&
          &-(tt*s15) + s25*s35)) + ss*(s25*(tt*s15 + s25*(tt - s35)) + ss**2*(s15 - s35) + &
          &ss*(tt*s15 - tt*s25 - s15*s25 + 2*s25*s35)))/(tt*s35) + (8*me2**3*(s15 + s25) - &
          &4*me2**2*(tt*s15 + s25**2 + ss*(3*s15 + s25 - s35) + s25*(tt + s35)) + ss*(-(tt*&
          &s15*s25) + ss*s15*(-tt + s25) + ss*s25*(tt - 2*s35) + s25**2*(-tt + s35) + ss**2&
          &*(-s15 + s35)) + 2*me2*(s25*(tt*s15 + s25*(tt - s35)) + ss**2*(3*s15 - 2*s35) + &
          &ss*(2*tt*s15 - s15*s25 + 3*s25*s35)))/(s25*(tt + s15 - s35)))*pvd8(6))/s15 + (64&
          &*Pi**2*((-2*me2*(4*me2**2*(s15 + s25) + s15*(ss**2 + 3*ss*s25 - 2*s25*(s15 + s25&
          &)) - 2*me2*(3*s15*s25 + ss*(2*s15 + s25))))/(s25*(4*me2 - ss - tt + s35)) + (2*m&
          &e2*(4*me2**2*(s15 - s25 - 2*s35) - 2*me2*(tt*s15 + s15**2 - tt*s25 + ss*(s15 - 2&
          &*s35) - 3*s15*s35 + s25*s35) + s15*(ss*(tt + s15 - 3*s35) + s25*(tt - s15 + s35)&
          &)))/((tt + s15 - s35)*(s15 + s25 - s35)) - (2*me2*(4*me2**2*(s15 - s25 - 2*s35) &
          &- 2*me2*(tt*s15 + s15**2 - tt*s25 + ss*(s15 - 2*s35) - 3*s15*s35 + s25*s35) + s1&
          &5*(ss*(tt + s15 - 3*s35) + s25*(tt - s15 + s35))))/(s35*(4*me2 - ss - tt + s35))&
          & + (2*me2*(8*me2**2*(s15 - s35) - 2*me2*(s15**2 + s15*(tt + 3*s25 - 3*s35) + ss*&
          &(3*s15 + s25 - 2*s35) + s25*(-tt + s35)) + s15*(ss**2 + ss*(tt + s15 + 3*s25 - 3&
          &*s35) + s25*(tt - 3*s15 - 2*s25 + s35))))/((4*me2 - ss - tt + s25)*(s15 + s25 - &
          &s35)) + (8*me2**3*(s15 + s25) - 4*me2**2*(2*s15**2 - s15*(-2*tt + s25) + ss*(3*s&
          &15 - 2*s35) + 2*s25*(tt + s35)) - s15*(s25*(tt*s15 + s25*(tt - s35)) + ss**2*(2*&
          &tt + 3*s15 - 3*s35) + ss*(tt*s15 - tt*s25 - s15*s25 + 4*s25*s35)) + 2*me2*(tt*s1&
          &5**2 + s25**2*(tt - s35) + 2*ss**2*(s15 - s35) + s15*s25*(2*tt + 3*s35) + ss*(5*&
          &s15**2 + 3*s25*s35 - s15*(-4*tt + s25 + 3*s35))))/(s25*(tt + s15 - s35)) + (-16*&
          &me2**3*(s15 - s35) + 4*me2**2*(3*s15**2 + 4*ss*(s15 - s35) + s25*(tt + 3*s35) - &
          &s15*(-3*tt + s25 + 3*s35)) + s15*(s25*(tt*s15 + s25*(tt - s35)) + ss**2*(2*tt + &
          &3*s15 - 3*s35) + ss*(tt*s15 - tt*s25 - s15*s25 + 4*s25*s35)) - 2*me2*(s15**2*(tt&
          & - s25) + s25**2*(tt - s35) + 2*ss**2*(s15 - s35) + s15*s25*(3*tt + 4*s35) + ss*&
          &(6*s15**2 + 3*s25*s35 - s15*(-5*tt + s25 + 6*s35))))/(tt*s35))*pvd8(7))/s15 + (6&
          &4*Pi**2*((2*me2*(-4*me2**2*(s15 - s35) + 2*s25*(s15 + s25)*(s15 - s35) + ss*s25*&
          &(tt - 2*s15 + s35) + ss**2*(-s15 + s35) + me2*(4*ss*s15 - 2*tt*s25 + 4*s15*s25 -&
          & 4*ss*s35 - 2*s25*s35)))/(s25*(4*me2 - ss - tt + s35)) + (2*me2*(4*me2**2*(s15 -&
          & s35) + ss*(s15**2 + s15*(tt - 2*s35) + (tt - s35)*s35) - 2*me2*(s15**2 + tt*s25&
          & + s15*(tt + 2*s25 - 2*s35) + ss*(s15 - s35) + tt*s35 - 3*s25*s35 - s35**2) + s2&
          &5*(tt**2 + 3*tt*s15 - 4*tt*s35 - s15*s35 + s35**2)))/((tt + s15 - s35)*(s15 + s2&
          &5 - s35)) - (2*me2*(4*me2**2*(s15 - s35) + ss*(s15**2 + s15*(tt - 2*s35) + (tt -&
          & s35)*s35) - 2*me2*(s15**2 + tt*s25 + s15*(tt + 2*s25 - 2*s35) + ss*(s15 - s35) &
          &+ tt*s35 - 3*s25*s35 - s35**2) + s25*(tt**2 + 3*tt*s15 - 4*tt*s35 - s15*s35 + s3&
          &5**2)))/(s35*(4*me2 - ss - tt + s35)) + (2*me2*(8*me2**2*(s15 - s35) + ss**2*(s1&
          &5 - s35) - 2*me2*(s15**2 + s15*(tt + 4*s25 - 2*s35) + 3*ss*(s15 - s35) + (tt - 4&
          &*s25 - s35)*s35) + ss*(s15**2 + s15*(tt + 2*s25 - 2*s35) + (tt - s35)*s35 - s25*&
          &(tt + s35)) + s25*(tt**2 - 2*s15**2 - 4*tt*s35 + 2*s25*s35 + s35**2 + s15*(3*tt &
          &- 2*s25 + s35))))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + (4*me2**2*(s15 -&
          & s35)*(-tt + s15 + s35) + ss**2*(s15**2 - s35**2) - s25*(-tt + s35)*(-(tt*s15) +&
          & s25*(-tt + s35)) + ss*(-(tt*s15*(2*tt + s15 - 3*s35)) + 2*s25*s35*(-tt + s35)) &
          &+ 2*me2*(tt*s15*(2*tt + s15 - 3*s35) - ss*(s15 - s35)*(-tt + 2*s15 + 2*s35) + s2&
          &5*(tt**2 - 2*tt*s15 + 3*tt*s35 - 2*s35**2)))/(tt*s35) + (4*me2**2*(s25*(tt - 3*s&
          &35) + ss*(s15 - s35) + 2*s15*(tt + s25 - s35)) - 8*me2**3*(s15 - s35) + ss**2*(-&
          &s15**2 + s35**2) + s25*(-tt + s35)*(-(tt*s15) + s25*(-tt + s35)) + ss*(tt*s15*(2&
          &*tt + s15 - 3*s35) - 2*s25*s35*(-tt + s35)) + 2*me2*(-(tt*s15**2) + s15*(-2*tt**&
          &2 - tt*s25 + 3*tt*s35 + s25*s35) + s25*(-2*tt**2 + tt*s35 + s35**2) + ss*(s15**2&
          & - s35**2 + 2*s15*(-tt + s35))))/(s25*(tt + s15 - s35)))*pvd8(8))/s15 + (128*Pi*&
          &*2*((2*me2*(-(me2*s15*(ss + tt + s15 + 2*s25 - 3*s35)) + s15**2*(s15 + s25 - s35&
          &) + 2*me2**2*(2*s15 + s25 - s35)))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) +&
          & (me2*(-2*me2*s15*(tt + s15 - 3*s35) + s15**2*(s15 - 2*s35) + 4*me2**2*(s15 - s3&
          &5)))/((tt + s15 - s35)*(s15 + s25 - s35)) - (me2*(4*me2**2*(s15 + s25) - 2*me2*s&
          &15*(ss + 2*s25) + s15**2*(s15 + 2*s25)))/(s25*(4*me2 - ss - tt + s35)) - (me2*(-&
          &2*me2*s15*(tt + s15 - 3*s35) + s15**2*(s15 - 2*s35) + 4*me2**2*(s15 - s35)))/(s3&
          &5*(4*me2 - ss - tt + s35)) + (-4*me2**3*(s15 - s35) + s15**2*(ss*(tt + s15 - s35&
          &) + s25*s35) + 2*me2**2*(2*tt*s15 + 3*s15**2 + tt*s25 + ss*(s15 - s35) - 3*s15*s&
          &35 + s25*s35) - me2*s15*(tt*s15 + 2*s15**2 + tt*s25 + ss*(2*tt + 3*s15 - 3*s35) &
          &- 2*s15*s35 + 3*s25*s35))/(tt*s35) + (-(s15**2*(ss*(tt + s15 - s35) + s25*s35)) &
          &- 2*me2**2*(tt*s15 + 2*s15**2 + ss*(s15 - s35) + s25*(tt + s35)) + me2*s15*(tt*s&
          &15 + s15**2 + ss*(2*tt + 3*s15 - 3*s35) + s25*(tt + 3*s35)))/(s25*(tt + s15 - s3&
          &5)))*pvd8(9))/s15 + (64*Pi**2*((2*me2*(4*me2**2*(s15 - s35) + 2*me2*s35*(-tt + s&
          &35) + s15*(-tt**2 + s15**2 + 5*tt*s35 - 2*s35**2 - s15*(2*tt + s35))))/((tt + s1&
          &5 - s35)*(s15 + s25 - s35)) + (-8*me2**3*(s15 - s35) - 4*me2**2*s35*(-tt + s35) &
          &+ 2*me2*s15*(tt**2 - s15**2 - 5*tt*s35 + 2*s35**2 + s15*(2*tt + s35)))/(s35*(4*m&
          &e2 - ss - tt + s35)) + (2*me2*(8*me2**2*(s15 - s35) + s15*(-tt**2 - 2*tt*s15 + 3&
          &*s15**2 - 2*tt*s25 + 2*s15*s25 - ss*(tt + s15 - 2*s35) + 5*tt*s35 - 3*s15*s35 - &
          &2*s35**2) + 2*me2*(tt*s15 + s15**2 + 2*tt*s25 - tt*s35 - 2*s15*s35 - 2*s25*s35 +&
          & s35**2 + ss*(-s15 + s35))))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) - (2*me&
          &2*(4*me2**2*(s15 - s35) + 2*me2*(tt*s15 + s15**2 + 2*tt*s25 - 2*s15*s35 - 2*s25*&
          &s35 + ss*(-s15 + s35)) + s15*(-(ss*(tt + s15 - 2*s35)) + 2*(s15**2 - tt*s25 + s1&
          &5*s25 - s15*s35))))/(s25*(4*me2 - ss - tt + s35)) + (-4*me2**2*s15*(-tt + s15) -&
          & 8*me2**3*(s15 - s35) + 2*me2*(s15**2*(-3*tt + 2*s35) + s15*(-2*tt**2 - tt*s25 +&
          & 3*tt*s35 + s25*s35) + ss*(s15**2 - s35**2) + s25*(-2*tt**2 + tt*s35 + s35**2)) &
          &+ s15*(tt*s15**2 + s15*(tt**2 + tt*s25 - 2*tt*s35 - s25*s35) + s25*(tt**2 + tt*s&
          &35 - 2*s35**2) - ss*(-2*tt**2 + s15**2 + 3*tt*s35 - 2*s35**2 + s15*(-tt + s35)))&
          &)/(s25*(tt + s15 - s35)) - (-4*me2**2*(s15 - s35)*(-tt + s15 + s35) + 2*me2*(s15&
          &**3 + s15**2*(-5*tt + s35) + s15*(-3*tt**2 - tt*s25 + 8*tt*s35 + s25*s35 - 2*s35&
          &**2) + ss*(s15**2 - s35**2) + s25*(-2*tt**2 + tt*s35 + s35**2)) + s15*(tt*s15**2&
          & + s15*(tt**2 + tt*s25 - 2*tt*s35 - s25*s35) + s25*(tt**2 + tt*s35 - 2*s35**2) -&
          & ss*(-2*tt**2 + s15**2 + 3*tt*s35 - 2*s35**2 + s15*(-tt + s35))))/(tt*s35))*pvd8&
          &(10))/s15 + (64*Pi**2*((2*me2*(-(tt**2*s15) - 2*tt*s15**2 + s15**3 + tt**2*s25 +&
          & s15**2*s25 + 4*me2*(tt + s15 - 2*s35)*(s15 - s35) - ss*(tt + s15 - 2*s35)*(s15 &
          &- s35) + 5*tt*s15*s35 - s15**2*s35 - 2*tt*s25*s35 - 2*s15*s25*s35 - tt*s35**2 - &
          &s15*s35**2 + 2*s25*s35**2))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) - (2*me2&
          &*(s15**3 + tt**2*s25 + s15**2*s25 + 2*me2*(tt + s15 - 2*s35)*(s15 - s35) - ss*(t&
          &t + s15 - 2*s35)*(s15 - s35) - 2*s15**2*s35 - 2*tt*s25*s35 - 2*s15*s25*s35 + s15&
          &*s35**2 + 2*s25*s35**2))/(s25*(4*me2 - ss - tt + s35)) + (2*me2*(2*me2*(tt + s15&
          & - 2*s35)*(s15 - s35) - tt*s35**2 + s15**2*(-2*tt + s35) - s15*(tt**2 - 5*tt*s35&
          & + 2*s35**2)))/((tt + s15 - s35)*(s15 + s25 - s35)) - (2*me2*(2*me2*(tt + s15 - &
          &2*s35)*(s15 - s35) - tt*s35**2 + s15**2*(-2*tt + s35) - s15*(tt**2 - 5*tt*s35 + &
          &2*s35**2)))/(s35*(4*me2 - ss - tt + s35)) + (-4*me2**2*(tt + s15 - 2*s35)*(s15 -&
          & s35) + 2*me2*s15*(-tt + s35)**2 + (-tt + s35)*(tt*s15**2 + tt*s25*(tt - s35) - &
          &ss*(tt + s15)*(s15 - s35) + s15*(tt**2 + tt*s25 - 2*tt*s35 - s25*s35)))/(s25*(tt&
          & + s15 - s35)) - (2*me2*(s15 - s35)*(tt*s35 + s15*(-2*tt + s35)) + (-tt + s35)*(&
          &tt*s15**2 + tt*s25*(tt - s35) - ss*(tt + s15)*(s15 - s35) + s15*(tt**2 + tt*s25 &
          &- 2*tt*s35 - s25*s35)))/(tt*s35))*pvd8(11))/s15)/(16.*Pi**2)

          !print*,"6",diags

  end if

  if (box57) then

  diags =  diags + ((64*Pi**2*((2*(8*me2**2 - 2*me2*(ss + tt - s25) + s15*(s15 + s25 - s35)&
          &))/(4*me2 - ss - tt + s25) + (2*(4*me2**2 - 2*me2*(ss + tt - s15 - s25) + s15*(s&
          &15 + s25 - s35)))/tt + (2*s15*(-4*me2**2 + me2*(2*ss + 4*tt + s15 - 5*s35) + (ss&
          & + tt - s25)*(-tt + s35)))/(tt*s35) - (2*s15*(8*me2**2 + (ss - s15)*(ss + tt - s&
          &15 - s25) - me2*(6*ss + 2*tt - 6*s15 - 4*s25 + s35)))/(s25*(4*me2 - ss - tt + s3&
          &5)) - (2*s15*(4*me2**2 + (ss - s15)*(ss + tt - s15 - s25) - me2*(4*ss + 2*tt - 6&
          &*s15 - 3*s25 + s35)))/(s25*(tt + s15 - s35)) + (s15*(16*me2**2 + 2*ss**2 + 2*tt*&
          &*2 + tt*s15 - tt*s25 - 2*me2*(6*ss + 6*tt - 2*s25 - 3*s35) - 2*tt*s35 + s25*s35 &
          &- ss*(-4*tt + s15 + 2*s25 + s35)))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) +&
          & (s15*(8*me2**2 + 2*ss**2 + 2*tt**2 + tt*s15 - tt*s25 - 2*tt*s35 + s25*s35 - ss*&
          &(-4*tt + s15 + 2*s25 + s35) + 2*me2*(-4*ss - 4*tt + s15 + s25 + 2*s35)))/((tt + &
          &s15 - s35)*(s15 + s25 - s35)) - (2*s15*(8*me2**2 - (ss + tt - s25)*(-tt + s35) +&
          & me2*(-2*ss - 6*tt + s25 + 6*s35)))/(s35*(4*me2 - ss - tt + s35)))*pvb11(1))/s15&
          &**2 + (64*Pi**2*((-2*(8*me2**2 - 2*me2*(ss + tt - s25) + s15*(s15 + s25 - s35)))&
          &/(4*me2 - ss - tt + s25) - (2*(4*me2**2 - 2*me2*(ss + tt - s15 - s25) + s15*(s15&
          & + s25 - s35)))/tt + (2*s15*(-2*me2 + ss + tt - s35))/(s15 + s25 - s35) + (s15*(&
          &8*me2**2 + 2*tt**2 - tt*s15 - 3*tt*s25 - 2*me2*(2*ss + 4*tt + 4*s15 + s25 - 6*s3&
          &5) + ss*(2*tt + 3*s15 - 3*s35) - 2*tt*s35 + 3*s25*s35))/(tt*s35) + (2*s15*(8*me2&
          &**2 + (ss - s15)*(ss + tt - s15 - s25) - me2*(6*ss + 2*tt - 6*s15 - 4*s25 + s35)&
          &))/(s25*(4*me2 - ss - tt + s35)) + (2*s15*(4*me2**2 + (ss - s15)*(ss + tt - s15 &
          &- s25) - me2*(4*ss + 2*tt - 6*s15 - 3*s25 + s35)))/(s25*(tt + s15 - s35)) - (s15&
          &*(-16*me2**2 - 2*tt**2 + tt*s15 + 3*tt*s25 + 2*me2*(2*ss + 6*tt + 2*s15 - 7*s35)&
          & + 2*tt*s35 - 3*s25*s35 + ss*(-2*tt - 3*s15 + 3*s35)))/(s35*(4*me2 - ss - tt + s&
          &35)) - (2*s15*(4*me2**2 + (ss + tt - s25)*(ss + tt - s35) + me2*(-4*ss - 4*tt + &
          &s25 + 3*s35)))/((tt + s15 - s35)*(s15 + s25 - s35)))*pvb2(1))/s15**2 + (128*Pi**&
          &2*(-((me2*(s15 + s25 - s35))/(s25*(tt + s15 - s35))) + (-(ss*s15) + me2*(2*s15 +&
          & s25))/(tt*s35) + (-(ss*s15) + me2*(2*s15 + s25))/(s35*(4*me2 - ss - tt + s35)) &
          &+ (me2*s35)/(s25*(4*me2 - ss - tt + s35)) + (-(tt*s15) - tt*s25 - ss*s35 + s25*s&
          &35 + me2*(s25 + 2*s35))/((tt + s15 - s35)*(s15 + s25 - s35)) + (-(tt*s15) - tt*s&
          &25 - ss*s35 + s25*s35 + me2*(2*s15 + s25 + 2*s35))/((4*me2 - ss - tt + s25)*(s15&
          & + s25 - s35)))*pvb3(1))/s15 + (128*Pi**2*(-((4*me2**2 - 2*me2*(ss + 2*tt - s15 &
          &- s25) + s15*(s15 + s25 - 2*s35))/tt) - (12*me2**2 - 2*me2*(ss + 2*tt + s15 - s2&
          &5) + s15*(s15 + s25 - 2*s35))/(4*me2 - ss - tt + s25) + (s15*(4*me2**2 + (ss - s&
          &15)*(ss + 2*tt - s25) + me2*(-4*ss - 4*tt + 7*s15 + 4*s25 - 4*s35)))/(s25*(tt + &
          &s15 - s35)) + (s15*(12*me2**2 + (ss - s15)*(ss + 2*tt - s25) + me2*(-8*ss - 4*tt&
          & + 5*s15 + 4*s25 - 4*s35)))/(s25*(4*me2 - ss - tt + s35)) + (s15*(4*me2**2 + 2*t&
          &t**2 + tt*s15 - tt*s25 - me2*(2*ss + 6*tt + s15 - 5*s35) + ss*(tt - s35) - 2*tt*&
          &s35 + s25*s35))/(tt*s35) - (s15*(4*me2**2 + ss**2 + 2*tt**2 + tt*s15 - tt*s25 - &
          &2*tt*s35 + s25*s35 - ss*(-3*tt + s25 + s35) + me2*(-4*ss - 6*tt + 2*s15 + 2*s25 &
          &+ s35)))/((tt + s15 - s35)*(s15 + s25 - s35)) - (s15*(12*me2**2 + ss**2 + 2*tt**&
          &2 + tt*s15 - tt*s25 - 2*tt*s35 + s25*s35 - ss*(-3*tt + s25 + s35) + me2*(-8*ss -&
          & 10*tt + 3*s25 + 3*s35)))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + (s15*(12&
          &*me2**2 + 2*tt**2 + tt*s15 - tt*s25 + ss*(tt - s35) - 2*tt*s35 + s25*s35 + me2*(&
          &-2*ss - 10*tt - s15 + s25 + 7*s35)))/(s35*(4*me2 - ss - tt + s35)))*pvb5(1))/s15&
          &**2 + (64*Pi**2*((2*(4*me2**2 - 2*me2*(ss + 2*tt - s15 - s25) + s15*(s15 + s25 -&
          & 2*s35)))/tt + (2*(12*me2**2 - 2*me2*(ss + 2*tt + s15 - s25) + s15*(s15 + s25 - &
          &2*s35)))/(4*me2 - ss - tt + s25) - (2*s15*(4*me2**2 + (ss - s15)*(ss + 2*tt - s2&
          &5) + me2*(-4*ss - 4*tt + 6*s15 + 3*s25 - 3*s35)))/(s25*(tt + s15 - s35)) - (2*s1&
          &5*(12*me2**2 + (ss - s15)*(ss + 2*tt - s25) + me2*(-8*ss - 4*tt + 5*s15 + 4*s25 &
          &- 3*s35)))/(s25*(4*me2 - ss - tt + s35)) - (s15*(24*me2**2 + 4*tt**2 + tt*s15 - &
          &3*tt*s25 - 2*me2*(2*ss + 10*tt + s15 - s25 - 8*s35) + ss*(2*tt + s15 - 3*s35) - &
          &4*tt*s35 + 3*s25*s35))/(s35*(4*me2 - ss - tt + s35)) - (s15*(8*me2**2 + 4*tt**2 &
          &+ tt*s15 - 3*tt*s25 + ss*(2*tt + s15 - 3*s35) - 4*me2*(ss + 3*tt + s15 - 3*s35) &
          &- 4*tt*s35 + 3*s25*s35))/(tt*s35) + (2*s15*(4*me2**2 + ss**2 + 2*tt**2 + tt*s15 &
          &- tt*s25 - 2*tt*s35 + s25*s35 - ss*(-3*tt + s25 + s35) + me2*(-4*ss - 6*tt + 2*s&
          &15 + 2*s25 + s35)))/((tt + s15 - s35)*(s15 + s25 - s35)) + (2*s15*(12*me2**2 + s&
          &s**2 + 2*tt**2 + tt*s15 - tt*s25 - 2*tt*s35 + s25*s35 - ss*(-3*tt + s25 + s35) +&
          & me2*(-8*ss - 10*tt + 3*s25 + 3*s35)))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35&
          &)))*pvb9(1))/s15**2 + (64*Pi**2*((2*s15*(-64*me2**3 + (ss - s15)*(2*ss**2 + tt**&
          &2 - s15**2 - 3*tt*s25 + 2*s25**2 + s15*(-2*tt + s25) - ss*(-3*tt + s15 + 4*s25))&
          & + 4*me2**2*(15*ss + 7*tt - 9*s15 - 11*s25 + 2*s35) + me2*(-18*ss**2 - 2*tt**2 -&
          & 4*s15**2 + 12*tt*s25 - 8*s25**2 + ss*(-20*tt + 20*s15 + 26*s25 - s35) - 3*tt*s3&
          &5 + s35**2 + s15*(16*tt - 15*s25 + s35))))/(s25*(4*me2 - ss - tt + s35)) + (64*m&
          &e2**3 - 8*me2**2*(6*ss + 5*tt - 4*s15 - 6*s25) + 4*me2*(2*ss**2 + tt**2 + 3*s15*&
          &*2 + ss*(3*tt - 2*s15 - 4*s25) - 3*tt*s25 + 2*s25**2 + s15*(-2*tt + 6*s25 - 4*s3&
          &5)) - 2*s15*(ss*(s15 + 2*s25 - 2*s35) - (-tt + 2*s25)*(s25 - s35) + s15*(tt - 2*&
          &s25 + s35)))/tt + (2*(64*me2**3 - 4*me2**2*(7*ss + 7*tt + s15 - 7*s25) + 2*me2*(&
          &2*ss**2 + 3*ss*tt + tt**2 + 3*s15**2 - 4*ss*s25 - 3*tt*s25 + 3*s15*s25 + 2*s25**&
          &2 - 4*s15*s35) - s15*(ss*(s15 + 2*s25 - 2*s35) - (-tt + 2*s25)*(s25 - s35) + s15&
          &*(tt - 2*s25 + s35))))/(4*me2 - ss - tt + s25) - (s15*(64*me2**3 - 2*tt**3 + 2*t&
          &t**2*s15 - tt*s15**2 + 6*tt**2*s25 - 3*tt*s15*s25 - 4*tt*s25**2 - 4*me2**2*(12*s&
          &s + 18*tt + s15 - 4*s25 - 13*s35) - 2*ss**2*(2*tt + s15 - 2*s35) + 2*tt**2*s35 +&
          & 2*tt*s15*s35 + 4*s15**2*s35 - 6*tt*s25*s35 + 3*s15*s25*s35 + 4*s25**2*s35 - 4*s&
          &15*s35**2 + 2*me2*(4*ss**2 + 12*tt**2 - 2*tt*s15 + 6*s15**2 - 16*tt*s25 + 2*s15*&
          &s25 + 2*ss*(9*tt + 2*s15 - 2*s25 - 7*s35) - 10*tt*s35 - s15*s35 + 15*s25*s35 - s&
          &35**2) + ss*(-5*s15**2 + 2*(-3*tt + 4*s25)*(tt - s35) + s15*(-2*tt + 3*s35))))/(&
          &tt*s35) - (s15*(128*me2**3 - 2*tt**3 + 2*tt**2*s15 - tt*s15**2 + 6*tt**2*s25 - 3&
          &*tt*s15*s25 - 4*tt*s25**2 - 8*me2**2*(7*ss + 15*tt + s15 - 4*s25 - 9*s35) - 2*ss&
          &**2*(2*tt + s15 - 2*s35) + 2*tt**2*s35 + 2*tt*s15*s35 + 4*s15**2*s35 - 6*tt*s25*&
          &s35 + 3*s15*s25*s35 + 4*s25**2*s35 - 4*s15*s35**2 + 2*me2*(4*ss**2 + 16*tt**2 - &
          &2*tt*s15 + 6*s15**2 - 18*tt*s25 + 4*s15*s25 + 2*s25**2 + ss*(20*tt + 6*s15 - 4*s&
          &25 - 17*s35) - 15*tt*s35 - 4*s15*s35 + 14*s25*s35 + s35**2) + ss*(-5*s15**2 + 2*&
          &(-3*tt + 4*s25)*(tt - s35) + s15*(-2*tt + 3*s35))))/(s35*(4*me2 - ss - tt + s35)&
          &) + (2*s15*(32*me2**3 - 2*ss**3 - tt**3 + tt**2*s15 - tt*s15**2 + 3*tt**2*s25 - &
          &2*tt*s15*s25 - 2*tt*s25**2 - 4*me2**2*(10*ss + 9*tt - s15 - 4*s25 - 5*s35) + tt*&
          &*2*s35 + tt*s15*s35 + 2*s15**2*s35 - 3*tt*s25*s35 + 2*s15*s25*s35 + 2*s25**2*s35&
          & - 2*s15*s35**2 + ss**2*(-5*tt + 4*s25 + 2*s35) - ss*(4*tt**2 + s15**2 - 7*tt*s2&
          &5 + 2*s25**2 + s15*(-tt + s25 - s35) - 3*tt*s35 + 4*s25*s35) + me2*(16*ss**2 + 1&
          &2*tt**2 - 4*tt*s15 + 3*s15**2 - 18*tt*s25 + 3*s15*s25 + 4*s25**2 - 10*tt*s35 - s&
          &15*s35 + 11*s25*s35 + s35**2 - 2*ss*(-14*tt + 9*s25 + 7*s35))))/((tt + s15 - s35&
          &)*(s15 + s25 - s35)) + (2*s15*(64*me2**3 - 2*ss**3 - tt**3 + tt**2*s15 - tt*s15*&
          &*2 + 3*tt**2*s25 - 2*tt*s15*s25 - 2*tt*s25**2 + tt**2*s35 + tt*s15*s35 + 2*s15**&
          &2*s35 - 3*tt*s25*s35 + 2*s15*s25*s35 + 2*s25**2*s35 - 2*s15*s35**2 + ss**2*(-5*t&
          &t + 4*s25 + 2*s35) + 4*me2**2*(-15*ss - 15*tt + s15 + 8*s25 + 7*s35) - ss*(4*tt*&
          &*2 + s15**2 - 7*tt*s25 + 2*s25**2 + s15*(-tt + s25 - s35) - 3*tt*s35 + 4*s25*s35&
          &) + me2*(18*ss**2 + 16*tt**2 - 4*tt*s15 + 4*s15**2 - 24*tt*s25 + 5*s15*s25 + 6*s&
          &25**2 - 12*tt*s35 - 3*s15*s35 + 14*s25*s35 - 2*ss*(-17*tt + 11*s25 + 8*s35))))/(&
          &(4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + (2*s15*(-32*me2**3 + (ss - s15)*(2*&
          &ss**2 + tt**2 - s15**2 - 3*tt*s25 + 2*s25**2 + s15*(-2*tt + s25) - ss*(-3*tt + s&
          &15 + 4*s25)) + me2**2*(40*ss + 20*tt - 38*s15 - 32*s25 + 6*s35) + me2*(-16*ss**2&
          & - 5*s15**2 + 2*ss*(-8*tt + 10*s15 + 11*s25) + 2*s15*(8*tt - 9*s25 + s35) - 2*(t&
          &t**2 + 4*s25**2 + s35**2 - 2*s25*(2*tt + s35)))))/(s25*(tt + s15 - s35)))*pvc12(&
          &1))/s15**2 + (64*Pi**2*((4*me2*(4*me2**2 + 2*me2*(-ss + s15 + s25) + s15*(-tt + &
          &s15 + s25 - s35)))/(4*me2 - ss - tt + s25) - (2*me2*s15*(3*s15**2 + tt*s25 + 4*s&
          &15*s25 + s25**2 + me2*(8*s15 + 4*s25 - 6*s35) - 3*s15*s35 - 3*s25*s35 + s35**2 +&
          & ss*(-2*s15 + s35)))/(s25*(tt + s15 - s35)) - (2*me2*s15*(8*me2**2 + 2*ss**2 - 2&
          &*tt*s15 + 2*s15**2 + tt*s25 + 2*s15*s25 + me2*(-8*ss + 14*s15 + 8*s25 - 6*s35) -&
          & s15*s35 - 2*s25*s35 + s35**2 + ss*(-6*s15 - 3*s25 + 2*s35)))/(s25*(4*me2 - ss -&
          & tt + s35)) - (s15*(-(tt**2*s15) - tt*s15**2 - tt**2*s25 + tt*s25**2 + 4*me2**2*&
          &(2*s15 + s25 - 2*s35) + ss**2*(s15 - s35) + 2*tt*s15*s35 + 2*s15**2*s35 + tt*s25&
          &*s35 + s15*s25*s35 - s25**2*s35 - 2*s15*s35**2 + 2*me2*(-(tt*s15) + 4*s15**2 + 5&
          &*s15*s25 + s25**2 - ss*(3*s15 + s25 - 3*s35) + 2*tt*s35 - 4*s15*s35 - 3*s25*s35)&
          & - ss*(3*s15**2 + tt*s25 + 3*s15*s25 + tt*s35 - 3*s15*s35 - 2*s25*s35)))/(tt*s35&
          &) - (s15*(16*me2**3 - tt**2*s15 - tt*s15**2 - tt**2*s25 + tt*s25**2 + 4*me2**2*(&
          &-2*ss - 2*tt + s15 + s25) + ss**2*(s15 - s35) + 2*tt*s15*s35 + 2*s15**2*s35 + tt&
          &*s25*s35 + s15*s25*s35 - s25**2*s35 - 2*s15*s35**2 - ss*(3*s15**2 + s25*(tt - 2*&
          &s35) + 3*s15*(s25 - s35) + tt*s35) + 2*me2*(-(ss*s15) + 4*s15**2 + tt*s25 + s25*&
          &*2 + s15*(tt + 5*s25 - 6*s35) + tt*s35 - 4*s25*s35 + s35**2 + 2*ss*(tt + s35))))&
          &/(s35*(4*me2 - ss - tt + s35)) + (2*s15*(me2**2*(8*s15 + 4*s25 - 6*s35) - s15*(s&
          &s + tt - s35)*(s15 + s25 - s35) + me2*(5*s15**2 + tt*s25 + 6*s15*s25 + s25**2 - &
          &5*s15*s35 - 3*s25*s35 + s35**2 + ss*(-2*s15 + s35))))/((tt + s15 - s35)*(s15 + s&
          &25 - s35)) + (2*s15*(8*me2**3 - s15*(ss + tt - s35)*(s15 + s25 - s35) - 2*me2**2&
          &*(4*ss + 2*tt - 4*s15 - 3*s25 + s35) + me2*(2*ss**2 + 4*s15**2 + 5*s15*(s25 - s3&
          &5) + (tt + s25 - 2*s35)*(s25 - s35) + ss*(2*tt - 4*s15 - 3*s25 + s35))))/((4*me2&
          & - ss - tt + s25)*(s15 + s25 - s35)))*pvc12(2))/s15**2 + (128*Pi**2*((s15*(8*me2&
          &**3 - tt**3 + tt**2*s15 + tt*s15**2 + tt**2*s25 + 2*me2**2*(-8*tt + 3*s15 + s25 &
          &- 2*s35) + ss*(-tt + 3*s15)*(tt + s15 - s35) + tt**2*s35 - 4*tt*s15*s35 - 2*s15*&
          &*2*s35 - tt*s25*s35 + s15*s25*s35 + 2*s15*s35**2 + me2*(8*tt**2 - 6*tt*s15 - 7*s&
          &15**2 - 3*tt*s25 - 2*s15*s25 - 4*tt*s35 + 15*s15*s35 + s25*s35 - 2*s35**2 + ss*(&
          &2*tt - 4*s15 + s35))))/(s35*(4*me2 - ss - tt + s35)) + (s15*(-tt**3 + tt**2*s15 &
          &+ tt*s15**2 + tt**2*s25 + 2*me2**2*(-4*tt + 4*s15 + s25 - 2*s35) + ss*(-tt + 3*s&
          &15)*(tt + s15 - s35) + tt**2*s35 - 4*tt*s15*s35 - 2*s15**2*s35 - tt*s25*s35 + s1&
          &5*s25*s35 + 2*s15*s35**2 + me2*(6*tt**2 - 5*tt*s15 - 9*s15**2 - 2*tt*s25 - 3*s15&
          &*s25 - 3*tt*s35 + 13*s15*s35 + s25*s35 - 2*s35**2 + ss*(2*tt - 3*s15 + s35))))/(&
          &tt*s35) + (-8*me2**3 + 12*me2**2*tt + 2*me2*(s15**2 + ss*(-tt + s15) + tt*(-tt +&
          & s25) - s15*(tt + s25)) - s15*(s15**2 + s15*(s25 - s35) + tt*(-s25 + s35)))/(4*m&
          &e2 - ss - tt + s25) - (4*me2**2*(-2*tt + s15) + 2*me2*(tt**2 + ss*(tt - s15) + s&
          &15**2 - tt*s25 + s15*s25) + s15*(s15**2 + s15*(s25 - s35) + tt*(-s25 + s35)))/tt&
          & + (s15*((ss - s15)*(-s15**2 + ss*(-tt + s15) - s15*s25 + tt*(-tt + s25)) + me2*&
          &*2*(4*s15 - 2*(4*tt + s35)) + me2*(ss*(6*tt - 4*s15) + 2*s15**2 + 2*tt*(tt - 2*s&
          &25 + s35) + s15*(-6*tt + 3*s25 + s35))))/(s25*(tt + s15 - s35)) + (s15*(8*me2**3&
          & + (ss - s15)*(-s15**2 + ss*(-tt + s15) - s15*s25 + tt*(-tt + s25)) - 2*me2**2*(&
          &2*ss + 6*tt - s15 + s35) + me2*(s15**2 - ss*(-8*tt + s15) + 2*tt*(tt - 2*s25 + s&
          &35) + s15*(-5*tt + 4*s25 + s35))))/(s25*(4*me2 - ss - tt + s35)) + (s15*(-8*me2*&
          &*3 + ss**2*(tt - s15) + 2*me2**2*(2*ss + 8*tt - 2*s15 - s25 + s35) + 2*me2*(-4*t&
          &t**2 + 4*tt*s15 + 3*s15**2 + ss*(-4*tt + s15) + 3*tt*s25 + tt*s35 - 6*s15*s35 - &
          &s25*s35 + s35**2) + (tt + s15 - s35)*(tt*(tt - s25) + 2*s15*(-tt + s35)) - ss*(s&
          &15**2 + tt*(-2*tt + s25 + s35) - s15*(-2*tt + s25 + 2*s35))))/((4*me2 - ss - tt &
          &+ s25)*(s15 + s25 - s35)) + (s15*(ss**2*(tt - s15) + me2**2*(8*tt - 4*s15 + 2*s3&
          &5) + me2*(-6*ss*tt - 6*tt**2 + 4*ss*s15 + 8*tt*s15 + 7*s15**2 + 5*tt*s25 + tt*s3&
          &5 - 10*s15*s35 - 2*s25*s35 + 2*s35**2) + (tt + s15 - s35)*(tt*(tt - s25) + 2*s15&
          &*(-tt + s35)) - ss*(s15**2 + tt*(-2*tt + s25 + s35) - s15*(-2*tt + s25 + 2*s35))&
          &))/((tt + s15 - s35)*(s15 + s25 - s35)))*pvc12(3))/s15**2 + (64*Pi**2*((2*s15*(7&
          &2*me2**3 - 2*ss**2*tt - 2*tt**3 + 3*tt**2*s25 - tt*s25**2 + 2*tt**2*s35 + tt*s15&
          &*s35 + 2*s15**2*s35 - 3*tt*s25*s35 + s15*s25*s35 + s25**2*s35 - 2*s15*s35**2 + m&
          &e2**2*(-36*ss - 64*tt + 2*s15 + 24*s25 + 20*s35) + me2*(4*ss**2 + 18*tt**2 + 6*s&
          &15**2 - 14*tt*s25 + s15*(-tt + 5*s25 - 11*s35) + ss*(24*tt + s15 - 4*s25 - 7*s35&
          &) - 16*tt*s35 + 9*s25*s35 + s35**2) - ss*(s15**2 + s15*(s25 - 2*s35) + 3*tt*(tt &
          &- s35) + s25*(-2*tt + s35))))/(s35*(4*me2 - ss - tt + s35)) + (2*s15*(72*me2**3 &
          &- (ss - s15)*(2*ss**2 + 2*tt**2 - tt*s15 - s15**2 - 3*tt*s25 + s15*s25 + 2*s25**&
          &2 - ss*(-3*tt + s15 + 4*s25)) + me2**2*(-72*ss - 28*tt + 42*s15 + 54*s25 - 6*s35&
          &) + me2*(22*ss**2 + 4*tt**2 - 26*ss*s15 - 14*tt*s15 + 3*s15**2 - 12*tt*s25 + 18*&
          &s15*s25 + 10*s25**2 + 5*tt*s35 - 3*s15*s35 - 4*s25*s35 + s35**2 + 4*ss*(5*tt - 8&
          &*s25 + s35))))/(s25*(4*me2 - ss - tt + s35)) + (2*(-48*me2**3 + 4*me2**2*(10*ss &
          &+ 7*tt - 4*s15 - 10*s25) - 2*me2*(4*ss**2 + 4*tt**2 + 4*s15**2 + ss*(7*tt - 3*s1&
          &5 - 8*s25) - 7*tt*s25 + 4*s25**2 + s15*(-4*tt + 8*s25 - 4*s35)) + s15*(3*tt*s25 &
          &- 4*s25**2 + ss*(3*s15 + 4*s25 - 4*s35) - 4*tt*s35 + 4*s25*s35 + s15*(3*tt - 4*s&
          &25 + s35))))/tt - (2*(72*me2**3 - 2*me2**2*(18*ss + 14*tt + s15 - 18*s25) + me2*&
          &(4*ss**2 + 4*tt**2 + 3*s15**2 + 2*ss*(3*tt + s15 - 4*s25) - 6*tt*s25 + 6*s15*s25&
          & + 4*s25**2 - 8*s15*s35) - s15*(tt*s25 - 2*s25**2 + ss*(s15 + 2*s25 - 2*s35) - 2&
          &*tt*s35 + 2*s25*s35 + s15*(tt - 2*s25 + s35))))/(4*me2 - ss - tt + s25) + (2*s15&
          &*(48*me2**3 - 4*ss**2*tt - 4*tt**3 + 5*tt**2*s25 - tt*s15*s25 - 2*tt*s25**2 + 4*&
          &tt**2*s35 + 2*s15**2*s35 - 4*tt*s25*s35 + 3*s15*s25*s35 + 2*s25**2*s35 - 2*s15*s&
          &35**2 - s25*s35**2 + 4*me2**2*(-10*ss - 13*tt + s15 + 4*s25 + 3*s35) + ss*(-7*tt&
          &**2 - s15**2 + 4*tt*s25 - 2*s15*s25 + 5*tt*s35 + s15*s35 - 2*s25*s35 + s35**2) +&
          & me2*(8*ss**2 + 22*tt**2 - 4*tt*s15 + 4*s15**2 - 18*tt*s25 + 4*s15*s25 - 14*tt*s&
          &35 - 3*s15*s35 + 10*s25*s35 - 3*s35**2 - 2*ss*(-17*tt + s15 + 4*s25 + 2*s35))))/&
          &(tt*s35) - (s15*(144*me2**3 - 4*ss**3 - 4*tt**3 - tt*s15**2 + 6*tt**2*s25 - tt*s&
          &15*s25 - 2*tt*s25**2 + 2*ss**2*(-5*tt + s15 + 4*s25) - 4*me2**2*(36*ss + 32*tt -&
          & 5*s15 - 21*s25 - 7*s35) + 4*tt**2*s35 + 2*tt*s15*s35 + 4*s15**2*s35 - 6*tt*s25*&
          &s35 + 3*s15*s25*s35 + 2*s25**2*s35 - 4*s15*s35**2 - ss*(s15**2 + 4*s25**2 + 2*tt&
          &*(5*tt - 3*s35) + s15*(-2*tt + 4*s25 - 3*s35) + 2*s25*(-5*tt + s35)) + 2*me2*(22&
          &*ss**2 + 18*tt**2 + 7*s15**2 - 20*tt*s25 + 6*s25**2 + s15*(-tt + 8*s25 - 12*s35)&
          & - 11*tt*s35 + 5*s25*s35 + 2*s35**2 - ss*(-38*tt + 9*s15 + 28*s25 + 3*s35))))/((&
          &4*me2 - ss - tt + s25)*(s15 + s25 - s35)) - (2*s15*(-48*me2**3 + 4*ss**3 - 4*tt*&
          &*2*s15 + tt*s15**2 + s15**3 + 4*me2**2*(16*ss + 7*tt - 11*s15 - 13*s25) - 2*tt**&
          &2*s25 + 4*tt*s15*s25 - s15**2*s25 + tt*s25**2 - 2*s15*s25**2 + tt*s15*s35 + 3*tt&
          &*s25*s35 - s25**2*s35 - s25*s35**2 - ss**2*(-7*tt + 5*s15 + 8*s25 + 2*s35) + ss*&
          &(4*tt**2 + 4*s25**2 - 2*tt*s35 + s35**2 + 3*s25*(-3*tt + s35) + s15*(-8*tt + 6*s&
          &25 + s35)) + me2*(-28*ss**2 - 4*s15**2 + s15*(21*tt - 21*s25 + s35) + ss*(-28*tt&
          & + 30*s15 + 40*s25 + 6*s35) - 2*(4*tt**2 - 10*tt*s25 + 6*s25**2 - tt*s35 + s25*s&
          &35 + 2*s35**2))))/(s25*(tt + s15 - s35)) + (s15*(-96*me2**3 + 8*ss**3 + 8*tt**3 &
          &- 3*tt*s15**2 - 14*tt**2*s25 - 3*tt*s15*s25 + 4*s15**2*s25 + 6*tt*s25**2 + 4*s15&
          &*s25**2 + 8*me2**2*(16*ss + 13*tt - 2*s15 - 7*s25 - 3*s35) - 8*tt**2*s35 + 2*tt*&
          &s15*s35 - 4*s15**2*s35 + 14*tt*s25*s35 - 7*s15*s25*s35 - 6*s25**2*s35 + 4*s15*s3&
          &5**2 - 2*ss**2*(-11*tt + s15 + 8*s25 + 2*s35) + ss*(22*tt**2 - 2*tt*s15 - 3*s15*&
          &*2 - 26*tt*s25 + 8*s25**2 - 14*tt*s35 + s15*s35 + 10*s25*s35) + me2*(-56*ss**2 +&
          & 4*s15**2 + 2*s15*(3*tt + s25 + 2*s35) + 4*ss*(-24*tt + 3*s15 + 16*s25 + 5*s35) &
          &- 2*(22*tt**2 + 4*s25**2 - 16*tt*s35 + s35**2 + 12*s25*(-2*tt + s35)))))/((tt + &
          &s15 - s35)*(s15 + s25 - s35)))*pvc16(1))/s15**2 + (128*Pi**2*((s15*(8*me2**3 + t&
          &t**3 + ss**2*(tt - s15) + tt*s15**2 - tt**2*s25 + tt*s15*s25 + ss*(s15**2 + tt*(&
          &2*tt - s25 - s35) + s15*(-tt + s25 - s35)) - tt**2*s35 - 2*tt*s15*s35 - 2*s15**2&
          &*s35 + tt*s25*s35 - s15*s25*s35 + 2*s15*s35**2 - 2*me2**2*(2*ss + 4*s15 - s25 + &
          &s35) + me2*(-4*ss*tt - 4*tt**2 + 8*ss*s15 + 2*tt*s15 - 6*s15**2 + tt*s25 - 5*s15&
          &*s25 + 3*tt*s35 + 9*s15*s35 + 2*s25*s35 - 2*s35**2)))/((4*me2 - ss - tt + s25)*(&
          &s15 + s25 - s35)) + (s15*(-8*me2**3 - tt**3 + tt**2*s25 - ss*(s15**2 + s15*(tt -&
          & 2*s35) + tt*(tt - s35)) + tt**2*s35 + 2*tt*s15*s35 + 2*s15**2*s35 - tt*s25*s35 &
          &- 2*s15*s35**2 + me2**2*(4*ss + 4*tt + 6*s15 + 6*s35) + 2*me2*(2*s15**2 - 4*s15*&
          &s35 + (-tt + s35)**2 - ss*(2*s15 + s35))))/(tt*s35) + (8*me2**3 + 4*me2**2*(tt -&
          & 4*s15) - 2*me2*(ss*(tt - s15) + s15**2 + tt*(tt - s25) + s15*(-2*tt + s25 - s35&
          &)) + s15*(ss*(s15 - s35) + (-tt + s25)*s35 + s15*(tt - s25 + s35)))/(4*me2 - ss &
          &- tt + s25) + (8*me2**3 - 4*me2**2*(ss + 2*s15 - s25) - 2*me2*(ss*(tt - 2*s15) +&
          & s15**2 + tt*(tt - s25) + s15*(-3*tt + 2*s25 - s35)) + s15*(ss*(s15 - s35) + (-t&
          &t + s25)*s35 + s15*(tt - s25 + s35)))/tt - (s15*(-8*me2**3 - tt**3 - tt*s15**2 +&
          & ss**2*(-tt + s15) + me2**2*(8*ss + 4*tt + 6*s15 - 2*s25) + tt**2*s25 - tt*s15*s&
          &25 + tt**2*s35 + 2*tt*s15*s35 + 2*s15**2*s35 - tt*s25*s35 + s15*s25*s35 - 2*s15*&
          &s35**2 + ss*(-s15**2 + s15*(tt - s25 + s35) + tt*(-2*tt + s25 + s35)) + me2*(-2*&
          &ss**2 + 2*tt**2 + 6*s15**2 + tt*s25 + 4*s15*s25 - 2*tt*s35 - 9*s15*s35 - 3*s25*s&
          &35 + 2*s35**2 + ss*(-5*s15 + 2*s25 + s35))))/((tt + s15 - s35)*(s15 + s25 - s35)&
          &) + (s15*(-8*me2**3 - tt**3 + tt**2*s25 - ss*(s15**2 + s15*(tt - 2*s35) + tt*(tt&
          & - s35)) + tt**2*s35 + 2*tt*s15*s35 + 2*s15**2*s35 - tt*s25*s35 - 2*s15*s35**2 +&
          & 2*me2**2*(6*s15 + s25 + 4*s35) + me2*(4*tt**2 + 5*s15**2 - 2*tt*s25 + s15*(-3*t&
          &t + s25 - 8*s35) - 5*tt*s35 + s25*s35 + 2*s35**2 - ss*(-2*tt + s15 + 3*s35))))/(&
          &s35*(4*me2 - ss - tt + s35)) - (s15*(8*me2**3 - (ss - s15)*(s15**2 + ss*(-tt + s&
          &15) - s15*(-2*tt + s25) + tt*(-tt + s25)) + me2**2*(-8*ss - 4*s15 + 6*s25 + 2*s3&
          &5) + me2*(2*ss**2 + 4*ss*s15 - 4*s15**2 + 2*(-tt + s25)*(tt + s35) - 2*ss*(tt + &
          &s25 + s35) + s15*(7*tt - 6*s25 + 5*s35))))/(s25*(tt + s15 - s35)) - (s15*(8*me2*&
          &*3 - (ss - s15)*(s15**2 + ss*(-tt + s15) - s15*(-2*tt + s25) + tt*(-tt + s25)) +&
          & 2*me2**2*(-2*ss + 2*tt - 6*s15 + s25 + s35) + me2*(-3*s15**2 + 2*ss*(-2*tt + 5*&
          &s15 - s35) + 2*(-tt + s25)*(tt + s35) + s15*(6*tt - 4*s25 + 5*s35))))/(s25*(4*me&
          &2 - ss - tt + s35)))*pvc16(2))/s15**2 + (64*Pi**2*((2*s15*(-2*tt**3 - 2*tt**2*s1&
          &5 - tt*s15**2 + 4*me2**2*(-tt + s15) + tt**2*s25 + 2*tt**2*s35 + 2*tt*s15*s35 + &
          &s15**2*s35 - tt*s25*s35 - s15*s35**2 + me2*(2*ss*tt + 6*tt**2 - 2*ss*s15 + 3*tt*&
          &s15 + 3*s15**2 - 5*tt*s35 - 5*s15*s35) - ss*(tt**2 + tt*s15 + s15**2 - tt*s35 - &
          &2*s15*s35)))/(tt*s35) + (2*(me2**2*(12*tt - 8*s15) - 2*me2*(2*tt**2 + ss*(tt - s&
          &15) + 2*s15**2 - tt*s25 + s15*s25 - 2*s15*s35) + s15*(tt*s15 - s15*s25 + ss*(s15&
          & - s35) - 2*tt*s35 + s25*s35)))/(4*me2 - ss - tt + s25) + (2*s15*(-2*tt**3 - 2*t&
          &t**2*s15 - tt*s15**2 + tt**2*s25 + 2*tt**2*s35 + 2*tt*s15*s35 + s15**2*s35 - tt*&
          &s25*s35 - s15*s35**2 + 4*me2**2*(-3*tt + s15 + s35) - ss*(tt**2 + tt*s15 + s15**&
          &2 - tt*s35 - 2*s15*s35) + me2*(2*ss*tt + 10*tt**2 + 5*s15**2 - tt*s25 + s15*(4*t&
          &t + s25 - 8*s35) - 2*ss*s35 - 8*tt*s35 + s35**2)))/(s35*(4*me2 - ss - tt + s35))&
          & - (s15*(-4*tt**3 - 3*tt**2*s15 - 3*tt*s15**2 + 8*me2**2*(-tt + s15) + 2*ss**2*(&
          &-tt + s15) + 3*tt**2*s25 - tt*s15*s25 - ss*(3*s15**2 + tt*(6*tt - 2*s25 - 3*s35)&
          & + s15*(tt + 2*s25 - 3*s35)) + 4*tt**2*s35 + 4*tt*s15*s35 + 2*s15**2*s35 - 3*tt*&
          &s25*s35 + s15*s25*s35 - 2*s15*s35**2 + 2*me2*(4*ss*tt + 6*tt**2 - 4*ss*s15 + 2*t&
          &t*s15 + 5*s15**2 - tt*s25 + 3*s15*s25 - 3*tt*s35 - 7*s15*s35 - 2*s25*s35 + s35**&
          &2)))/((tt + s15 - s35)*(s15 + s25 - s35)) + (2*s15*(4*me2**2*(-tt + s15) + (ss -&
          & s15)*(ss*(-tt + s15) - s15*s25 + tt*(-2*tt + s25)) + me2*(4*ss*tt + 4*tt**2 - 4&
          &*ss*s15 - 5*tt*s15 + 4*s15**2 - 3*tt*s25 + 5*s15*s25 + 4*tt*s35 - 4*s15*s35 - 2*&
          &s25*s35 + s35**2)))/(s25*(tt + s15 - s35)) + (2*s15*(4*me2**2*(-3*tt + 2*s15) + &
          &(ss - s15)*(ss*(-tt + s15) - s15*s25 + tt*(-2*tt + s25)) + me2*(4*tt**2 + 3*s15*&
          &*2 - 3*tt*s25 + s15*(-5*tt + 4*s25 - 2*s35) + 4*tt*s35 - s25*s35 + s35**2 + ss*(&
          &8*tt - 7*s15 + s35))))/(s25*(4*me2 - ss - tt + s35)) - (s15*(-4*tt**3 - 3*tt**2*&
          &s15 - 3*tt*s15**2 + 2*ss**2*(-tt + s15) + 3*tt**2*s25 - tt*s15*s25 - ss*(3*s15**&
          &2 + tt*(6*tt - 2*s25 - 3*s35) + s15*(tt + 2*s25 - 3*s35)) + 4*tt**2*s35 + 4*tt*s&
          &15*s35 + 2*s15**2*s35 - 3*tt*s25*s35 + s15*s25*s35 - 2*s15*s35**2 + 8*me2**2*(-3&
          &*tt + s15 + s35) - 2*me2*(-10*tt**2 - 3*tt*s15 - 6*s15**2 + 3*tt*s25 - 4*s15*s25&
          & + 6*tt*s35 + 8*s15*s35 + s25*s35 - 2*s35**2 + ss*(-8*tt + 5*s15 + s35))))/((4*m&
          &e2 - ss - tt + s25)*(s15 + s25 - s35)) + (2*(-4*me2**2*(-tt + s15) + s15*(s15*(t&
          &t - s25) + ss*(s15 - s35) + (-2*tt + s25)*s35) + 2*me2*(-s15**2 + ss*(-tt + s15)&
          & + tt*(-2*tt + s25) + s15*(tt - s25 + s35))))/tt)*pvc16(3))/s15**2 + (64*Pi**2*(&
          &(4*me2*(2*me2 - s15)*(2*me2 - ss - tt + s25))/tt - (2*me2*(-16*me2**2 + 2*me2*(s&
          &s + 3*tt + 5*s15 - s25) + s15*(-2*ss - 2*tt + 3*s25 + s35)))/(4*me2 - ss - tt + &
          &s25) - (4*me2*s15*(4*me2**2 + ss**2 - tt*s15 - tt*s25 + s25*s35 - ss*(-tt + s15 &
          &+ s25 + s35) + me2*(-4*ss - 2*tt + 2*s15 + 4*s25 + s35)))/(s25*(tt + s15 - s35))&
          & + (me2*s15*(-32*me2**2 - 2*ss**2 + 7*tt*s15 + 2*s15**2 + 7*tt*s25 - 2*s25**2 + &
          &4*me2*(5*ss + 3*tt - 3*s15 - 5*s25 - 2*s35) - 2*s15*s35 - 5*s25*s35 + ss*(-6*tt &
          &+ s15 + 4*s25 + 5*s35)))/(s25*(4*me2 - ss - tt + s35)) - (s15*(16*me2**3 + 2*tt*&
          &*2*s15 + tt*s15**2 - 4*me2**2*(2*ss + 4*tt - s25) + 2*tt**2*s25 - tt*s25**2 - tt&
          &*s15*s35 - 3*tt*s25*s35 - s15*s25*s35 + s25**2*s35 + s25*s35**2 + ss**2*(s15 + s&
          &35) + ss*(3*tt*s15 + s15**2 + tt*s25 - s15*s25 + 2*tt*s35 - 2*s25*s35 - s35**2) &
          &- 2*me2*(-2*tt**2 + 3*tt*s15 + s15**2 + 5*tt*s25 - s25**2 + tt*s35 - 2*s25*s35 -&
          & s35**2 + ss*(-2*tt + s15 + s25 + s35))))/(tt*s35) - (s15*(32*me2**3 + 2*tt**2*s&
          &15 + tt*s15**2 + 2*tt**2*s25 - tt*s25**2 - 4*me2**2*(ss + 7*tt + s15 - s25 - 2*s&
          &35) - tt*s15*s35 - 3*tt*s25*s35 - s15*s25*s35 + s25**2*s35 + s25*s35**2 + ss**2*&
          &(s15 + s35) + ss*(3*tt*s15 + s15**2 + tt*s25 - s15*s25 + 2*tt*s35 - 2*s25*s35 - &
          &s35**2) - me2*(-6*tt**2 + 5*tt*s15 + 2*s15**2 + 9*tt*s25 - 2*s25**2 + 6*tt*s35 +&
          & 2*s15*s35 - 5*s25*s35 - 2*s35**2 + ss*(-2*tt + 3*s15 + 2*s25 + 3*s35))))/(s35*(&
          &4*me2 - ss - tt + s35)) + (s15*(16*me2**3 + (tt*s15 + s25*(tt - s35))*(2*s15 + 2&
          &*s25 - s35) - 2*ss**2*s35 - 2*me2**2*(8*ss + 8*tt + s15 - 3*s25 + 7*s35) + ss*(2&
          &*s15**2 - 2*tt*s25 + s15*s35 + 4*s25*s35 - s35**2) + me2*(4*ss**2 + 4*tt**2 - 2*&
          &s15**2 - 3*tt*s25 + s15*(tt - 2*s25 - 6*s35) - 7*s25*s35 + 2*s35**2 + ss*(8*tt +&
          & s15 - 4*s25 + 9*s35))))/((tt + s15 - s35)*(s15 + s25 - s35)) + (s15*(32*me2**3 &
          &- 2*ss**2*s35 - 2*me2**2*(10*ss + 14*tt + 4*s15 - 7*s25 + 7*s35) + (2*s15 + 2*s2&
          &5 - s35)*(tt*s15 + tt*s25 - s25*s35) + ss*(2*s15**2 - 2*tt*s25 + s15*s35 + 4*s25&
          &*s35 - s35**2) + me2*(2*ss**2 + 6*tt**2 + 5*tt*s15 - 2*s15**2 - 7*tt*s25 - 4*s15&
          &*s25 + 2*s25**2 + 2*tt*s35 - 8*s15*s35 - 9*s25*s35 + 2*s35**2 + ss*(8*tt + s15 -&
          & 4*s25 + 11*s35))))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)))*pvc19(1))/s15**&
          &2 + (64*Pi**2*(2*(-8*me2**2 + 2*me2*(ss + tt + s15 - s25) + s15*(-s25 + s35)) + &
          &(2*(2*me2 - ss - tt + s25)*(-8*me2**2 + 2*me2*(ss + tt + s15 - s25) + s15*(-s25 &
          &+ s35)))/tt - (s15*(64*me2**3 - 2*ss**3 - 2*tt**3 - tt**2*s15 - tt*s15**2 + 3*tt&
          &**2*s25 - tt*s15*s25 - 2*tt*s25**2 - 16*me2**2*(4*ss + 4*tt - 2*s25 - s35) + 2*t&
          &t**2*s35 + tt*s15*s35 - 2*tt*s25*s35 + s15*s25*s35 + 2*s25**2*s35 - s25*s35**2 +&
          & ss**2*(-6*tt - s15 + 4*s25 + s35) - ss*(6*tt**2 + 2*tt*s15 + s15**2 - 7*tt*s25 &
          &+ 2*s25**2 - 3*tt*s35 + 3*s25*s35 - s35**2) + 2*me2*(10*ss**2 + 10*tt**2 + 2*tt*&
          &s15 + 2*s15**2 - 10*tt*s25 + 3*s15*s25 + 3*s25**2 + 2*ss*(10*tt + s15 - 6*s25 - &
          &2*s35) - 6*tt*s35 - s15*s35 + 4*s25*s35 - s35**2)))/((4*me2 - ss - tt + s25)*(s1&
          &5 + s25 - s35)) + (2*s15*(16*me2**3 - (ss - s15)*(ss + tt - s25)*(ss + tt + s15 &
          &- s25) - 2*me2**2*(10*ss + 6*tt - 2*s15 - 7*s25 + s35) + me2*(8*ss**2 + 2*tt**2 &
          &- 4*tt*s15 - 2*s15**2 - 5*tt*s25 + 4*s15*s25 + 4*s25**2 + tt*s35 - 2*s25*s35 + s&
          &s*(10*tt - 2*s15 - 11*s25 + s35))))/(s25*(tt + s15 - s35)) + (s15*(64*me2**3 - 4&
          &*me2**2*(8*ss + 16*tt + 6*s15 - 3*s25 - s35) - (ss + tt + s15 - s35)*(tt*(2*tt +&
          & s15) - s25*(tt + s35) + ss*(2*tt + s15 + s35)) + 2*me2*(2*ss**2 + 10*tt**2 + s1&
          &5**2 - 3*tt*s25 + s15*(9*tt + s25 - 2*s35) - 5*tt*s35 - s25*s35 - s35**2 + ss*(1&
          &2*tt + 5*s15 - s25 + s35))))/(s35*(4*me2 - ss - tt + s35)) + (2*s15*(32*me2**3 -&
          & (ss - s15)*(ss + tt - s25)*(ss + tt + s15 - s25) - 2*me2**2*(16*ss + 8*tt - 2*s&
          &15 - 9*s25 + s35) + me2*(10*ss**2 - 3*s15**2 + (-tt + s25)*(-2*tt + 3*s25 - s35)&
          & + ss*(12*tt - s15 - 13*s25 + s35) + s15*(-5*tt + 4*s25 + s35))))/(s25*(4*me2 - &
          &ss - tt + s35)) + (s15*(32*me2**3 - 8*me2**2*(3*ss + 5*tt + 2*s15 + s35) - (ss +&
          & tt + s15 - s35)*(tt*(2*tt + s15) - s25*(tt + s35) + ss*(2*tt + s15 + s35)) + 2*&
          &me2*(2*ss**2 + 8*tt**2 + s15**2 - tt*s25 - s25**2 + s15*(7*tt + s25) - 3*tt*s35 &
          &- 2*s25*s35 - s35**2 + ss*(10*tt + 5*s15 - s25 + 3*s35))))/(tt*s35) + (s15*(-32*&
          &me2**3 + 2*ss**3 + 2*tt**3 + tt**2*s15 + tt*s15**2 - 3*tt**2*s25 + tt*s15*s25 + &
          &2*tt*s25**2 + 4*me2**2*(10*ss + 10*tt - 5*s25 - s35) + ss**2*(6*tt + s15 - 4*s25&
          & - s35) - 2*tt**2*s35 - tt*s15*s35 + 2*tt*s25*s35 - s15*s25*s35 - 2*s25**2*s35 +&
          & s25*s35**2 + ss*(6*tt**2 + 2*tt*s15 + s15**2 - 7*tt*s25 + 2*s25**2 - 3*tt*s35 +&
          & 3*s25*s35 - s35**2) - 2*me2*(8*ss**2 + 8*tt**2 + tt*s15 + s15**2 - 8*tt*s25 + 3&
          &*s15*s25 + 3*s25**2 - 4*tt*s35 + 2*s25*s35 - s35**2 + ss*(s15 - 2*(-8*tt + 5*s25&
          & + s35)))))/((tt + s15 - s35)*(s15 + s25 - s35)))*pvc19(2))/s15**2 + (64*Pi**2*(&
          &-8*me2**2 + 4*me2*(ss - s25) - 2*s15*s25 - (2*(2*me2 - ss - tt + s25)*(4*me2**2 &
          &- 2*me2*(ss - s25) + s15*s25))/tt + (2*s15*(16*me2**3 - (ss - s15)*(ss - s25)*(s&
          &s + tt - s25) - 2*me2**2*(10*ss + 2*tt - 3*s15 - 6*s25 + s35) + me2*(8*ss**2 - 2&
          &*tt*s15 - 2*tt*s25 + 5*s15*s25 + 3*s25**2 - s25*s35 + ss*(4*tt - 5*s15 - 11*s25 &
          &+ s35))))/(s25*(4*me2 - ss - tt + s35)) + (2*s15*(8*me2**3 - (ss - s15)*(ss - s2&
          &5)*(ss + tt - s25) - 2*me2**2*(6*ss + 2*tt - 2*s15 - 4*s25 + s35) + me2*(6*ss**2&
          & - 2*tt*s15 - 2*tt*s25 + 5*s15*s25 + 4*s25**2 - s25*s35 + ss*(4*tt - 4*s15 - 9*s&
          &25 + s35))))/(s25*(tt + s15 - s35)) + (s15*(32*me2**3 - 4*me2**2*(6*ss + 6*tt + &
          &s15 - 3*s25) - ss*(tt*(2*tt + s15) - s25*(tt + s35) + ss*(2*tt + s15 + s35)) + 2&
          &*me2*(2*ss**2 + 2*tt*(tt - s25) + s15*(tt + s25) + ss*(8*tt + 2*s15 - s25 + s35)&
          &)))/(s35*(4*me2 - ss - tt + s35)) + (s15*(-16*me2**3 + 2*ss**3 + 4*me2**2*(6*ss &
          &+ 4*tt - s15 - 3*s25) - 4*ss**2*(-tt + s25) + ss*(-tt + s25)*(-2*tt + s15 + 2*s2&
          &5 + s35) + (-tt + s25)*(tt*s15 + tt*s25 - s25*s35) - 2*me2*(6*ss**2 - ss*(-8*tt &
          &+ s15 + 7*s25) + (-tt + s25)*(-2*tt + 2*s15 + 2*s25 + s35))))/((tt + s15 - s35)*&
          &(s15 + s25 - s35)) - (s15*(32*me2**3 - 2*ss**3 + 4*ss**2*(-tt + s25) - (-tt + s2&
          &5)*(tt*s15 + s25*(tt - s35)) - ss*(-tt + s25)*(-2*tt + s15 + 2*s25 + s35) + 4*me&
          &2**2*(-10*ss - 6*tt + 2*s15 + 5*s25 + s35) + 2*me2*(8*ss**2 + s15*(-3*tt + 2*s25&
          &) + (-tt + 2*s25)*(-2*tt + s25 + s35) - ss*(-10*tt + s15 + 9*s25 + s35))))/((4*m&
          &e2 - ss - tt + s25)*(s15 + s25 - s35)) + (s15*(16*me2**3 - 4*me2**2*(4*ss + 4*tt&
          & + s15 - s25 + s35) - ss*(tt*(2*tt + s15) - s25*(tt + s35) + ss*(2*tt + s15 + s3&
          &5)) + 2*me2*(2*ss**2 + tt*(2*tt + s15) - s25**2 - s25*(tt + s35) + ss*(6*tt + 2*&
          &s15 - s25 + 2*s35))))/(tt*s35))*pvc19(3))/s15**2 + (64*Pi**2*((tt*s15 + tt*s25 -&
          & s25*s35 + ss*(s15 + s35) - 2*me2*(s15 + s25 + s35))/(tt + s15 - s35) + (tt*s15 &
          &+ tt*s25 - s25*s35 + ss*(s15 + s35) - 2*me2*(2*s15 + s25 + s35))/(4*me2 - ss - t&
          &t + s25))*(-0.5 + 4*pvc19(4)))/(s15*(s15 + s25 - s35)) - (64*Pi**2*(8*me2**2*(s1&
          &5 + s25 + s35) + (ss - s15 - s25 + s35)*(tt*s15 + s25*(tt - s35) + ss*(s15 + s35&
          &)) - 2*me2*(-2*s15**2 + 2*tt*s25 - s25**2 - 2*s25*s35 + s35**2 + s15*(tt - 2*s25&
          & + s35) + ss*(3*s15 + s25 + 3*s35)))*pvc19(5))/(s15*(tt + s15 - s35)*(s15 + s25 &
          &- s35)) - (64*Pi**2*(8*me2**2*(s15 + s25 + s35) + (ss - s15 - s25 + s35)*(tt*s15&
          & + s25*(tt - s35) + ss*(s15 + s35)) - 2*me2*(-2*s15**2 + 2*tt*s25 - s25**2 - 2*s&
          &25*s35 + s35**2 + s15*(tt - 2*s25 + s35) + ss*(3*s15 + s25 + 3*s35)))*pvc19(6))/&
          &(s15*(tt + s15 - s35)*(s15 + s25 - s35)) + (64*me2*Pi**2*((tt*s15 + tt*s25 - s25&
          &*s35 + ss*(s15 + s35) - 2*me2*(s15 + s25 + s35))/(tt + s15 - s35) + (tt*s15 + tt&
          &*s25 - s25*s35 + ss*(s15 + s35) - 2*me2*(2*s15 + s25 + s35))/(4*me2 - ss - tt + &
          &s25))*pvc19(7))/(s15*(s15 + s25 - s35)) + (64*Pi**2*((-2*(40*me2**3 + me2**2*(-2&
          &2*ss - 10*tt + 4*s15 + 22*s25) + s15*(-(tt*s15) - ss*s25 - tt*s25 + s15*s25 + s2&
          &5**2 + tt*s35) + me2*(2*ss**2 + 2*tt**2 + s15**2 + 2*ss*(tt + s15 - 2*s25) - 2*t&
          &t*s25 + 3*s15*s25 + 2*s25**2 - s15*s35)))/(4*me2 - ss - tt + s25) + (s15*(80*me2&
          &**3 - 2*tt**3 + 3*tt**2*s15 - tt*s15**2 + 5*tt**2*s25 - 4*tt*s15*s25 - 3*tt*s25*&
          &*2 - 2*ss**2*(tt + s15 - 2*s35) + 2*tt**2*s35 - 2*tt*s15*s35 - 7*tt*s25*s35 + s1&
          &5*s25*s35 + 3*s25**2*s35 + 2*s25*s35**2 + me2**2*(-44*ss - 60*tt + 16*s15 + 32*s&
          &25 + 52*s35) + me2*(4*ss**2 + 14*tt**2 - 21*tt*s15 - 25*tt*s25 - 2*s25**2 + ss*(&
          &26*tt + 9*s15 - 4*s25 - 31*s35) - 18*tt*s35 + 8*s15*s35 + 31*s25*s35 + 4*s35**2)&
          & + ss*(-2*tt**2 - s15**2 + 6*tt*s25 + 5*tt*s35 - 7*s25*s35 - 2*s35**2 + s15*(tt &
          &+ s25 + s35))))/(s35*(4*me2 - ss - tt + s35)) - (2*(32*me2**3 - 4*me2**2*(7*ss +&
          & 4*tt - 4*s15 - 7*s25) + 2*me2*(3*ss**2 + 3*tt**2 - 4*tt*s15 + 3*s15**2 + ss*(5*&
          &tt - 3*s15 - 6*s25) - 5*tt*s25 + 7*s15*s25 + 3*s25**2 - 2*s15*s35) + s15*(-3*tt*&
          &s15 - 3*tt*s25 + 3*s15*s25 + 3*s25**2 + 3*tt*s35 - 2*s25*s35 + ss*(-2*s15 - 3*s2&
          &5 + 2*s35))))/tt + (s15*(80*me2**3 - 2*(ss**3 + ss**2*(tt - 2*s15 - 2*s25) - s15&
          &*(tt**2 - tt*s15 - tt*s25 + s15*s25 + s25**2) + ss*(tt**2 - 2*tt*s15 + s15**2 - &
          &tt*s25 + 3*s15*s25 + s25**2)) + 4*me2**2*(-21*ss - 5*tt + 16*s15 + 16*s25 + s35)&
          & + me2*(26*ss**2 + 4*tt**2 - 13*tt*s15 + 18*s15**2 - 9*tt*s25 + 26*s15*s25 + 12*&
          &s25**2 + 2*tt*s35 - 8*s15*s35 - 5*s25*s35 + 4*s35**2 + ss*(14*tt - 39*s15 - 38*s&
          &25 + 3*s35))))/(s25*(4*me2 - ss - tt + s35)) + (2*s15*(32*me2**3 - 3*ss**3 + 3*t&
          &t**2*s15 - tt*s15**2 + 2*tt**2*s25 - 2*tt*s15*s25 + s15**2*s25 - tt*s25**2 + s15&
          &*s25**2 - 4*me2**2*(11*ss + 4*tt - 9*s15 - 9*s25 - s35) - tt*s15*s35 - 3*tt*s25*&
          &s35 + s25**2*s35 + s25*s35**2 + ss**2*(-5*tt + 4*s15 + 6*s25 + 2*s35) - ss*(3*tt&
          &**2 + s15**2 - 7*tt*s25 + 3*s25**2 - 2*tt*s35 + 3*s25*s35 + s35**2 + s15*(-6*tt &
          &+ 4*s25 + s35)) + me2*(20*ss**2 + 6*tt**2 - 16*tt*s15 + 6*s15**2 - 15*tt*s25 + 1&
          &4*s15*s25 + 8*s25**2 - 3*tt*s35 + 3*s15*s35 + 6*s25*s35 + 2*s35**2 - ss*(-18*tt &
          &+ 24*s15 + 29*s25 + 7*s35))))/(s25*(tt + s15 - s35)) - (s15*(-64*me2**3 + 6*tt**&
          &3 - 3*tt**2*s15 + tt*s15**2 - 9*tt**2*s25 + 6*tt*s15*s25 + 5*tt*s25**2 + 8*me2**&
          &2*(7*ss + 8*tt - s15 - 3*s25 - 5*s35) + 2*ss**2*(3*tt + s15 - 2*s35) - 6*tt**2*s&
          &35 + 4*tt*s15*s35 + 9*tt*s25*s35 - 5*s15*s25*s35 - 5*s25**2*s35 - 2*me2*(6*ss**2&
          & + 14*tt**2 - 18*tt*s25 + ss*(24*tt + s15 - 6*s25 - 13*s35) - 9*tt*s35 + 14*s25*&
          &s35 + s15*(-10*tt + s25 + 4*s35)) + ss*(s15**2 + s15*(-tt + s25 + s35) + (-tt + &
          &s25)*(-10*tt + 9*s35))))/(tt*s35) + (s15*(-64*me2**3 + 6*ss**3 + 6*tt**3 - 2*tt*&
          &*2*s15 - 4*tt*s15**2 - 12*tt**2*s25 - 3*tt*s15*s25 + 4*s15**2*s25 + 5*tt*s25**2 &
          &+ 4*s15*s25**2 + 4*me2**2*(22*ss + 16*tt - 3*s15 - 10*s25 - 9*s35) - 6*tt**2*s35&
          & + 6*tt*s15*s35 + 14*tt*s25*s35 - 4*s15*s25*s35 - 5*s25**2*s35 - 2*s25*s35**2 - &
          &2*ss**2*(-8*tt + 6*s25 + 3*s35) + me2*(-40*ss**2 - 28*tt**2 + 8*s15**2 + 40*tt*s&
          &25 - 4*s25**2 + 2*s15*(7*tt + 5*s25 - 4*s35) + 24*tt*s35 - 30*s25*s35 - 4*s35**2&
          & + ss*(-64*tt + 4*s15 + 46*s25 + 30*s35)) + ss*(-4*s15**2 + 6*s25**2 + 11*s25*(-&
          &2*tt + s35) + s15*(-2*tt - 3*s25 + 2*s35) + 2*(8*tt**2 - 6*tt*s35 + s35**2))))/(&
          &(tt + s15 - s35)*(s15 + s25 - s35)) - (s15*(80*me2**3 - 2*ss**3 - 2*tt**3 + 2*tt&
          &**2*s15 + 4*tt**2*s25 - tt*s15*s25 - tt*s25**2 + 2*tt**2*s35 - 2*tt*s15*s35 - 6*&
          &tt*s25*s35 + s25**2*s35 + 2*s25*s35**2 + 2*ss**2*(-2*tt + 2*s25 + s35) + me2**2*&
          &(-84*ss - 60*tt + 24*s15 + 52*s25 + 32*s35) + 2*me2*(13*ss**2 + 7*tt**2 - 8*tt*s&
          &15 + 2*s15**2 - 13*tt*s25 + 3*s15*s25 + 3*s25**2 - 5*tt*s35 - 4*s15*s35 + 6*s25*&
          &s35 + 4*s35**2 - ss*(-18*tt + 6*s15 + 17*s25 + 7*s35)) - ss*(4*tt**2 - 6*tt*s25 &
          &+ 2*s25**2 - 4*tt*s35 + 3*s25*s35 + 2*s35**2 + s15*(s25 - 2*(tt + s35)))))/((4*m&
          &e2 - ss - tt + s25)*(s15 + s25 - s35)))*pvc6(1))/s15**2 + (64*Pi**2*(8*me2**2 - &
          &4*me2*(ss + tt - s15 - s25) - (2*(2*me2 - tt)*(4*me2**2 - 2*me2*(ss + tt - s15 -&
          & s25) + s15*(s15 + s25 - s35)))/(4*me2 - ss - tt + s25) + 2*s15*(s15 + s25 - s35&
          &) - (s15*(4*me2**2*(-2*tt + 2*s15 + s25 - s35) + 2*me2*(4*ss*tt + 4*tt**2 + 2*s1&
          &5**2 - 2*tt*s25 + s15*(-tt + 2*s25 - 2*s35) - ss*s35 - 3*tt*s35 + s35**2) + (ss &
          &+ tt - s35)*(-2*tt**2 + tt*s15 + 3*tt*s25 - s25*s35 + ss*(-2*tt - s15 + s35))))/&
          &((tt + s15 - s35)*(s15 + s25 - s35)) - (2*s15*(tt*(ss - s15)*(ss + tt - s15 - s2&
          &5) - 2*me2**2*(3*s15 + s25 - 2*(tt + s35)) + me2*(-2*s15**2 + ss*(-4*tt + 2*s15 &
          &- s35) + (-tt + s25)*(2*tt + s35) + s15*(6*tt - 2*s25 + s35))))/(s25*(tt + s15 -&
          & s35)) + (2*s15*(8*me2**3 - tt*(ss - s15)*(ss + tt - s15 - s25) - 2*me2**2*(4*ss&
          & + 4*tt - 6*s15 - 3*s25 + 2*s35) + me2*(2*ss**2 + 2*tt**2 - 7*tt*s15 + 2*s15**2 &
          &- 2*tt*s25 + s15*s25 - s25**2 + tt*s35 + ss*(6*tt - 5*s15 - 2*s25 + s35))))/(s25&
          &*(4*me2 - ss - tt + s35)) + (s15*(16*me2**3 - 4*me2**2*(2*ss + 6*tt - s25 - 3*s3&
          &5) + ss**2*(-s15 + s35) + 2*me2*(6*tt**2 - 3*tt*s25 + ss*(4*tt + 2*s15 - 3*s35) &
          &- 6*tt*s35 + 2*s25*s35 + s35**2) + (-tt + s35)*(tt*(2*tt - s15) + s25*(-3*tt + s&
          &35)) + ss*(s15*(-2*tt + s35) - (-tt + s35)*(-2*tt + s25 + s35))))/(s35*(4*me2 - &
          &ss - tt + s35)) - (s15*(ss**2*(s15 - s35) + 4*me2**2*(2*tt + s15 - s35) - (-tt +&
          & s35)*(tt*(2*tt - s15) + s25*(-3*tt + s35)) - 2*me2*(s15*(tt - s35) + 2*ss*(tt +&
          & s15 - s35) + (-2*tt + s35)*(-2*tt + s25 + s35)) + ss*(-(s15*(-2*tt + s35)) + (-&
          &tt + s35)*(-2*tt + s25 + s35))))/(tt*s35) + (s15*(-16*me2**3 + 4*me2**2*(4*ss + &
          &6*tt - 2*s15 - 2*s25 - s35) + (ss + tt - s35)*(tt*(2*tt - s15) + ss*(2*tt + s15 &
          &- s35) + s25*(-3*tt + s35)) - 2*me2*(2*ss**2 + 6*tt**2 - 3*tt*s25 - s25**2 - s15&
          &*(tt + s25) - 5*tt*s35 + 2*s25*s35 + s35**2 - ss*(s15 + 2*(-4*tt + s25 + s35))))&
          &)/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)))*pvc6(2))/s15**2 + (64*Pi**2*(4*me&
          &2*(-tt + s15) - (2*(2*me2 - tt)*(2*me2*(-tt + s15) + s15*(s15 - s35)))/(4*me2 - &
          &ss - tt + s25) + 2*s15*(s15 - s35) + (2*s15*(tt*(ss - s15)*(-tt + s15) + 2*me2**&
          &2*(s15 - s35) + me2*(-tt + s15)*(-2*tt + 2*s15 - s35)))/(s25*(tt + s15 - s35)) +&
          & (s15*(ss*(s15 - s35)*(-3*tt + s35) + 4*me2**2*(-2*tt + s15 + s35) + (-tt + s35)&
          &*(2*tt**2 - tt*s15 - 3*tt*s25 + s25*s35) + 2*me2*(tt*(4*tt + s15) - 6*tt*s35 + s&
          &35**2)))/(s35*(4*me2 - ss - tt + s35)) - (s15*(4*me2**2*(s15 - s35) + (-tt + s35&
          &)*(2*tt**2 - tt*s15 - 3*tt*s25 + ss*(2*tt + s15 - s35) + s25*s35) + 2*me2*(2*tt*&
          &*2 + 2*s15**2 - 3*tt*s35 - 2*s15*s35 + s35**2)))/((tt + s15 - s35)*(s15 + s25 - &
          &s35)) - (s15*(8*me2**2*(-tt + s15) + (-tt + s35)*(2*tt**2 - tt*s15 - 3*tt*s25 + &
          &ss*(2*tt + s15 - s35) + s25*s35) + 2*me2*(2*ss*tt + 4*tt**2 - 2*ss*s15 - s15*s25&
          & - 5*tt*s35 + s25*s35 + s35**2)))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + &
          &(s15*(ss*(s15 - s35)*(-3*tt + s35) + 2*me2*(2*tt**2 + 2*tt*s15 - 4*tt*s35 - s15*&
          &s35 + s35**2) + (-tt + s35)*(tt*(2*tt - s15) + s25*(-3*tt + s35))))/(tt*s35) + (&
          &2*s15*(tt*(ss - s15)*(-tt + s15) + me2*(2*ss*tt + 2*tt**2 - 2*ss*s15 - 5*tt*s15 &
          &+ 2*s15**2 - s15*s25 + tt*s35 + s25*s35) + me2**2*(6*s15 - 2*(2*tt + s35))))/(s2&
          &5*(4*me2 - ss - tt + s35)))*pvc6(3))/s15**2 + (64*Pi**2*((2*(2*me2 - ss - tt + s&
          &25)*(32*me2**3 - 8*me2**2*(4*ss + 2*tt - 3*s15 - 4*s25) + 2*me2*(4*ss**2 + 3*tt*&
          &*2 - 5*tt*s15 + 5*s15**2 + ss*(6*tt - 5*s15 - 8*s25) - 6*tt*s25 + 9*s15*s25 + 4*&
          &s25**2 - 2*s15*s35) + s15*(-3*tt*s15 + s15**2 - 3*tt*s25 + 5*s15*s25 + 4*s25**2 &
          &+ 3*tt*s35 - s15*s35 - 3*s25*s35 + ss*(-3*s15 - 4*s25 + 3*s35))))/tt + (s15*(128&
          &*me2**4 + 8*ss**4 + 6*tt**4 - 2*tt**3*s15 - 2*tt**2*s15**2 - 2*tt*s15**3 - 18*tt&
          &**3*s25 + 2*tt**2*s15*s25 + 3*tt*s15**2*s25 + 18*tt**2*s25**2 + 3*tt*s15*s25**2 &
          &- 4*s15**2*s25**2 - 6*tt*s25**3 - 4*s15*s25**3 - 32*me2**3*(8*ss + 6*tt - 3*s15 &
          &- 5*s25 - s35) - 6*tt**3*s35 + 2*tt**2*s15*s35 + 18*tt**2*s25*s35 - 8*tt*s15*s25&
          &*s35 + 2*s15**2*s25*s35 - 18*tt*s25**2*s35 + 7*s15*s25**2*s35 + 6*s25**3*s35 + 4&
          &*tt*s15*s35**2 - 4*s15*s25*s35**2 + 2*ss**2*(19*tt**2 + 2*s15**2 - 30*tt*s25 + 1&
          &2*s25**2 + s15*(-5*tt + 5*s25 - s35) - 8*tt*s35 + 7*s25*s35) - 2*ss**3*(3*s15 + &
          &2*(-7*tt + 6*s25 + s35)) + 4*me2**2*(48*ss**2 + 7*s15**2 + s15*(-12*tt + 14*s25 &
          &- 7*s35) - 2*ss*(-38*tt + 15*s15 + 32*s25 + 6*s35) + 2*(15*tt**2 - 20*tt*s25 + 8&
          &*s25**2 - 9*tt*s35 + 7*s25*s35)) - 2*me2*(32*ss**3 + 20*tt**3 - 4*s15**3 - 42*tt&
          &**2*s25 + 26*tt*s25**2 - 4*s25**3 - 18*tt**2*s35 + 33*tt*s25*s35 - 16*s25**2*s35&
          & + tt*s35**2 + s15**2*(-2*tt + s25 + 2*s35) - 4*ss**2*(-20*tt + 6*s15 + 17*s25 +&
          & 3*s35) + ss*(68*tt**2 + 10*s15**2 - 102*tt*s25 + 40*s25**2 + s15*(-24*tt + 25*s&
          &25 - 7*s35) - 34*tt*s35 + 28*s25*s35) + s15*(-8*tt**2 + 10*tt*s25 - 2*tt*s35 - 3&
          &*s25*s35 + 4*s35**2)) + ss*(-2*s15**3 + s15**2*(2*tt + s25 - 2*s35) - 2*(-tt + s&
          &25)*(12*tt**2 - 15*tt*s25 + 4*s25**2 - 9*tt*s35 + 8*s25*s35) + s15*(-6*tt**2 - 2&
          &*tt*s35 + 4*s35**2 - 5*s25*(-2*tt + s35)))))/((tt + s15 - s35)*(s15 + s25 - s35)&
          &) - (s15*(192*me2**4 + 2*(ss - s15)*(ss + tt - s25)*(2*ss**2 + tt**2 - 2*tt*s15 &
          &+ s15**2 + ss*(2*tt - 3*s15 - 4*s25) - 2*tt*s25 + 3*s15*s25 + 2*s25**2) - 16*me2&
          &**3*(20*ss + 14*tt - 13*s15 - 17*s25 - 2*s35) + 4*me2**2*(48*ss**2 + 18*s15**2 +&
          & s15*(-43*tt + 49*s25 - 5*s35) - ss*(-58*tt + 61*s15 + 80*s25 + 2*s35) + 2*(7*tt&
          &**2 - 23*tt*s25 + 16*s25**2 + s25*s35 + s35**2)) - me2*(48*ss**3 + 4*tt**3 - 6*s&
          &15**3 + s15**2*(41*tt - 46*s25) - 28*tt**2*s25 + 40*tt*s25**2 - 20*s25**3 + 6*tt&
          &**2*s35 - 4*tt*s25*s35 + 4*s25**2*s35 + 2*tt*s35**2 - 4*s25*s35**2 + ss*(40*tt**&
          &2 + 51*s15**2 - 116*tt*s25 + 88*s25**2 + s15*(-114*tt + 146*s25 - 11*s35) + 6*tt&
          &*s35 - 8*s25*s35 + 4*s35**2) + s15*(-32*tt**2 + 83*tt*s25 - 56*s25**2 - 10*tt*s3&
          &5 + 5*s25*s35 + 4*s35**2) + ss**2*(-90*s15 + 4*(19*tt - 29*s25 + s35)))))/(s25*(&
          &4*me2 - ss - tt + s35)) - (2*s15*(64*me2**4 - 16*me2**3*(8*ss + 4*tt - 6*s15 - 7&
          &*s25) + 2*me2**2*(48*ss**2 + 14*tt**2 + 16*s15**2 - 44*tt*s25 + 32*s25**2 + s15*&
          &(-41*tt + 50*s25 - 2*s35) - tt*s35 + 2*s25*s35 + 2*s35**2 - 2*ss*(-26*tt + 31*s1&
          &5 + 40*s25 + 2*s35)) - me2*(32*ss**3 - 4*s15**3 - 4*ss**2*(-14*tt + 13*s15 + 19*&
          &s25 + 2*s35) + s15**2*(17*tt - 22*s25 + 5*s35) - 2*(-tt + s25)*(3*tt**2 - 9*tt*s&
          &25 + 6*s25**2 - 2*tt*s35 + 2*s25*s35 + 2*s35**2) + ss*(32*tt**2 - 70*tt*s15 + 22&
          &*s15**2 - 86*tt*s25 + 79*s15*s25 + 56*s25**2 - 12*tt*s35 + 3*s15*s35 + 12*s25*s3&
          &5 + 4*s35**2) - 2*s15*(11*tt**2 + 15*s25**2 - tt*s35 + s35**2 - s25*(24*tt + s35&
          &))) + (ss + tt - s25)*(4*ss**3 - s15**3 + s15**2*(2*tt - 3*s25) - s25*(-tt + s35&
          &)*(-2*tt + s25 + s35) - ss**2*(-6*tt + 7*s15 + 8*s25 + 2*s35) + s15*(3*tt*s25 - &
          &2*s25**2 + tt*(-3*tt + s35)) + ss*(3*tt**2 + 4*s15**2 - 8*tt*s25 + 4*s25**2 - 2*&
          &tt*s35 + 3*s25*s35 + s35**2 + s15*(-8*tt + 8*s25 + s35)))))/(s25*(tt + s15 - s35&
          &)) + (2*(96*me2**4 - 16*me2**3*(7*ss + 7*tt - 3*s15 - 7*s25) + 2*me2**2*(20*ss**&
          &2 + 14*tt**2 + 11*s15**2 + ss*(30*tt - 11*s15 - 40*s25) - 30*tt*s25 + 20*s25**2 &
          &+ s15*(-11*tt + 19*s25 - 8*s35)) + s15*(ss + tt - s25)*(-s15**2 + tt*s25 - 2*s25&
          &**2 + ss*(s15 + 2*s25 - s35) - tt*s35 + s25*s35 + s15*(tt - 3*s25 + s35)) + me2*&
          &(-4*ss**3 - 2*tt**3 + 3*s15**3 + 6*tt**2*s25 - 8*tt*s25**2 + 4*s25**3 + 2*ss**2*&
          &(-4*tt + s15 + 6*s25) + s15**2*(-11*tt + 13*s25 - 3*s35) + ss*(-6*tt**2 + 4*tt*s&
          &15 - 9*s15**2 + 16*tt*s25 - 16*s15*s25 - 12*s25**2 + 7*s15*s35) + s15*(14*s25**2&
          & + 2*tt*(tt + 5*s35) - s25*(15*tt + 7*s35)))))/(4*me2 - ss - tt + s25) - (s15*(1&
          &92*me2**4 + 4*ss**3*tt + 2*tt**4 - 2*tt**3*s15 + tt**2*s15**2 - tt*s15**3 - 6*tt&
          &**3*s25 + 5*tt**2*s15*s25 - 2*tt*s15**2*s25 + 6*tt**2*s25**2 - 3*tt*s15*s25**2 -&
          & 2*tt*s25**3 - 16*me2**3*(14*ss + 20*tt - 7*s15 - 12*s25 - 3*s35) - 2*tt**3*s35 &
          &- 2*tt**2*s15*s35 + 6*tt**2*s25*s35 + tt*s15*s25*s35 + s15**2*s25*s35 - 6*tt*s25&
          &**2*s35 + s15*s25**2*s35 + 2*s25**3*s35 + 4*tt*s15*s35**2 - 4*s15*s25*s35**2 + 2&
          &*ss**2*(3*s15**2 + s15*(s25 - 4*s35) + s25*(-4*tt + s35) - 2*tt*(-2*tt + s35)) +&
          & 4*me2**2*(20*ss**2 + 42*tt**2 - 13*tt*s15 + 14*s15**2 - 38*tt*s25 + 16*s15*s25 &
          &+ 12*s25**2 - 26*tt*s35 - 12*s15*s35 + 14*s25*s35 - ss*(-58*tt + 15*s15 + 32*s25&
          & + 12*s35)) - ss*(s15**3 + s15**2*(-7*tt + 5*s25 + s35) + s15*(2*tt**2 - 6*tt*s2&
          &5 + 2*s25**2 + 11*tt*s35 - 7*s25*s35 - 4*s35**2) + 2*(-tt + s25)*(3*tt*(tt - s35&
          &) + s25*(-3*tt + 2*s35))) + me2*(-8*ss**3 + 8*s15**3 + s15**2*(-17*tt + 20*s25 -&
          & 8*s35) + 4*ss**2*(-14*tt + 2*s15 + 4*s25 + 3*s35) + s15*(18*tt**2 - 29*tt*s25 +&
          & 14*s25**2 + 20*tt*s35 - 21*s25*s35 - 8*s35**2) - 2*(-tt + s35)*(24*tt*s25 - 10*&
          &s25**2 + tt*(-16*tt + s35)) + ss*(-72*tt**2 - 39*s15**2 + 68*tt*s25 - 8*s25**2 +&
          & 50*tt*s35 - 32*s25*s35 + s15*(10*tt - 24*s25 + 41*s35)))))/(s35*(4*me2 - ss - t&
          &t + s35)) - (s15*(128*me2**4 + 8*ss**3*tt + 6*tt**4 - 2*tt**3*s15 + tt**2*s15**2&
          & - tt*s15**3 - 14*tt**3*s25 + 7*tt**2*s15*s25 - 2*tt*s15**2*s25 + 12*tt**2*s25**&
          &2 - 5*tt*s15*s25**2 - 4*tt*s25**3 - 32*me2**3*(6*ss + 6*tt - 2*s15 - 4*s25 - s35&
          &) - 6*tt**3*s35 + 12*tt**2*s25*s35 - 5*tt*s15*s25*s35 + s15**2*s25*s35 - 10*tt*s&
          &25**2*s35 + 5*s15*s25**2*s35 + 4*s25**3*s35 + 4*tt*s15*s35**2 + 2*tt*s25*s35**2 &
          &- 4*s15*s25*s35**2 - 2*s25**2*s35**2 + 2*ss**2*(10*tt**2 + 3*s15**2 - 8*tt*s25 +&
          & 2*s15*s25 - 4*tt*s35 - 3*s15*s35 + 2*s25*s35 - s35**2) + 4*me2**2*(24*ss**2 + 3&
          &0*tt**2 + 8*s15**2 - 32*tt*s25 + 8*s25**2 + s15*(-9*tt + 10*s25 - 8*s35) - 17*tt&
          &*s35 + 12*s25*s35 - 2*s35**2 - 2*ss*(-30*tt + 7*s15 + 16*s25 + 4*s35)) - 2*me2*(&
          &8*ss**3 + 20*tt**3 - 2*s15**3 + s15**2*(4*tt - 5*s25) - 36*tt**2*s25 + 16*tt*s25&
          &**2 - 14*tt**2*s35 + 25*tt*s25*s35 - 12*s25**2*s35 - 3*tt*s35**2 + 4*s25*s35**2 &
          &- 2*ss**2*(-22*tt + 3*s15 + 8*s25 + 2*s35) + s15*(-10*tt**2 + 15*tt*s25 - 4*s25*&
          &*2 - 4*tt*s35 + 6*s35**2) + ss*(54*tt**2 + 15*s15**2 + 14*s15*s25 + 8*s25**2 - 2&
          &2*tt*s35 - 4*s35**2 + 8*s25*(-7*tt + 2*s35) - 5*s15*(2*tt + 3*s35))) - ss*(s15**&
          &3 + s15**2*(-7*tt + 5*s25 + s35) + s15*(2*tt**2 + 4*s25**2 + 7*tt*s35 - 4*s35**2&
          & - s25*(10*tt + s35)) + 2*(s25**2*(-6*tt + 4*s35) + s25*(14*tt**2 - 9*tt*s35 - 2&
          &*s35**2) + tt*(-9*tt**2 + 7*tt*s35 + s35**2)))))/(tt*s35) + (s15*(192*me2**4 + 4&
          &*ss**4 + 2*tt**4 - 2*tt**3*s15 + 2*tt**2*s15**2 - 2*tt*s15**3 - 6*tt**3*s25 + 6*&
          &tt**2*s15*s25 - 5*tt*s15**2*s25 + 6*tt**2*s25**2 - 5*tt*s15*s25**2 - 2*tt*s25**3&
          & - 6*ss**3*(-2*tt + s15 + 2*s25) - 2*tt**3*s35 - 2*tt**2*s15*s35 + 6*tt**2*s25*s&
          &35 + 2*s15**2*s25*s35 - 6*tt*s25**2*s35 + 3*s15*s25**2*s35 + 2*s25**3*s35 + 4*tt&
          &*s15*s35**2 - 4*s15*s25*s35**2 + 2*ss**2*(4*s15**2 + 6*s25**2 + s15*(-5*tt + 7*s&
          &25 - 3*s35) + tt*(7*tt - 2*s35) + s25*(-12*tt + s35)) - 16*me2**3*(20*ss - 8*s15&
          & - 5*(-4*tt + 3*s25 + s35)) + 4*me2**2*(48*ss**2 + 15*s15**2 + s15*(-17*tt + 26*&
          &s25 - 11*s35) - ss*(-86*tt + 37*s15 + 72*s25 + 14*s35) + 2*(21*tt**2 - 27*tt*s25&
          & + 12*s25**2 - 13*tt*s35 + 8*s25*s35 + s35**2)) - ss*(2*s15**3 + 2*(-tt + s25)*(&
          &4*tt**2 - 5*tt*s25 + 2*s25**2 - 3*tt*s35 + 2*s25*s35) + s15**2*(7*s25 + 2*(-5*tt&
          & + s35)) + s15*(6*tt**2 + 8*s25**2 + 10*tt*s35 - 4*s35**2 - 3*s25*(6*tt + s35)))&
          & - 2*me2*(24*ss**3 - 4*s15**3 + s15**2*(12*tt - 13*s25) - ss**2*(-58*tt + 27*s15&
          & + 54*s25 + 4*s35) - 2*(-tt + s25)*(8*tt**2 + 3*s25**2 - 7*tt*s35 + s35**2 + 4*s&
          &25*(-2*tt + s35)) + s15*(-9*tt**2 - 13*s25**2 - 12*tt*s35 + 6*s35**2 + 6*s25*(4*&
          &tt + s35)) + ss*(24*s15**2 + s15*(-28*tt + 41*s25 - 19*s35) + 2*(25*tt**2 - 38*t&
          &t*s25 + 18*s25**2 - 11*tt*s35 + 6*s25*s35 + s35**2)))))/((4*me2 - ss - tt + s25)&
          &*(s15 + s25 - s35)))*pvd9(1))/s15**2 + (64*Pi**2*((2*(2*me2 - ss - tt + s25)*(4*&
          &me2 - ss - tt + s15 + s25)*(4*me2**2 - 2*me2*(ss + tt - s15 - s25) + s15*(s15 + &
          &s25 - s35)))/tt + (2*(16*me2**2 + (ss + tt - s25)*(ss + tt - s15 - s25) + me2*(-&
          &7*ss - 9*tt + 3*s15 + 7*s25))*(4*me2**2 - 2*me2*(ss + tt - s15 - s25) + s15*(s15&
          & + s25 - s35)))/(4*me2 - ss - tt + s25) - (2*s15*(32*me2**4 + (ss - s15)*(ss + t&
          &t - s25)*(ss + tt - s15 - s25)**2 - 8*me2**3*(7*ss + 5*tt - 5*s15 - 6*s25 + s35)&
          & + 2*me2**2*(18*ss**2 + 8*tt**2 + 12*s15**2 - 18*tt*s25 + 12*s25**2 + s15*(-19*t&
          &t + 26*s25 - 11*s35) + 2*tt*s35 - 7*s25*s35 + 3*s35**2 + ss*(26*tt - 26*s15 - 29&
          &*s25 + 3*s35)) - me2*(10*ss**3 + 2*tt**3 - 4*s15**3 - 6*tt**2*s25 + 10*tt*s25**2&
          & - 4*s25**3 - s15*(-tt + s25)*(-11*tt + 16*s25 - 7*s35) - 7*tt*s25*s35 + 3*s25**&
          &2*s35 + 3*tt*s35**2 - s25*s35**2 + ss**2*(22*tt - 22*s15 - 23*s25 + s35) + s15**&
          &2*(13*tt - 16*s25 + 4*s35) + ss*(14*tt**2 + 15*s15**2 - 29*tt*s25 + 17*s25**2 + &
          &s15*(-33*tt + 36*s25 - 5*s35) + tt*s35 - 4*s25*s35 + s35**2))))/(s25*(tt + s15 -&
          & s35)) + (s15*(128*me2**4 + 2*ss**4 + 2*tt**4 + 3*tt**2*s15**2 - 2*tt*s15**3 - 4&
          &*tt**3*s25 + 4*tt**2*s15*s25 - 5*tt*s15**2*s25 + 3*tt**2*s25**2 - 4*tt*s15*s25**&
          &2 - tt*s25**3 - 2*tt**3*s35 - 6*tt**2*s15*s35 - tt*s15**2*s35 + 2*tt**2*s25*s35 &
          &+ 2*tt*s15*s25*s35 + 2*s15**2*s25*s35 - tt*s25**2*s35 + 3*s15*s25**2*s35 + s25**&
          &3*s35 + 6*tt*s15*s35**2 + 2*tt*s25*s35**2 - 5*s15*s25*s35**2 - 2*s25**2*s35**2 -&
          & 8*me2**3*(23*ss + 25*tt - 13*s15 - 16*s25 + s35) + ss**3*(8*tt - 6*s15 - 6*s25 &
          &+ 2*s35) + ss**2*(12*tt**2 + 9*s15**2 - 14*tt*s25 + 6*s25**2 + s15*(-8*tt + 13*s&
          &25 - 5*s35) - 3*s25*s35 - 2*s35**2) + 4*me2**2*(24*ss**2 + 28*tt**2 + 14*s15**2 &
          &- 22*tt*s25 + 11*s25**2 + s15*(-9*tt + 24*s25 - 13*s35) - 15*tt*s35 - 4*s25*s35 &
          &+ ss*(52*tt - 29*s15 - 34*s25 + 5*s35)) + ss*(-2*s15**3 + 7*tt*s25**2 - 2*s25**3&
          & + s15**2*(12*tt - 7*s25 - 3*s35) - 2*tt*(-4*tt**2 + 2*tt*s35 + s35**2) + s25*(-&
          &12*tt**2 + tt*s35 + 4*s35**2) + s15*(-2*tt**2 + 15*tt*s25 - 7*s25**2 - 13*tt*s35&
          & + 2*s25*s35 + 5*s35**2)) - 2*me2*(11*ss**3 + 13*tt**3 - 4*s15**3 - 16*tt**2*s25&
          & + 7*tt*s25**2 - 3*s25**3 + s15**2*(11*tt - 12*s25 - 2*s35) - 12*tt**2*s35 + 7*t&
          &t*s25*s35 + 4*s25*s35**2 + ss**2*(35*tt - 22*s15 - 24*s25 + 6*s35) + ss*(37*tt**&
          &2 + 23*s15**2 - 34*tt*s25 + 16*s25**2 + s15*(-16*tt + 36*s25 - 17*s35) - 12*tt*s&
          &35 - 6*s25*s35 - 4*s35**2) + s15*(-2*tt**2 + 15*tt*s25 - 11*s25**2 - 14*tt*s35 +&
          & 2*s25*s35 + 9*s35**2))))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) - (s15*(-6&
          &4*me2**4 - 2*ss**4 - 2*tt**4 - 3*tt**2*s15**2 + 2*tt*s15**3 + 4*tt**3*s25 - 4*tt&
          &**2*s15*s25 + 5*tt*s15**2*s25 - 3*tt**2*s25**2 + 4*tt*s15*s25**2 + tt*s25**3 + 2&
          &*tt**3*s35 + 6*tt**2*s15*s35 + tt*s15**2*s35 - 2*tt**2*s25*s35 - 2*tt*s15*s25*s3&
          &5 - 2*s15**2*s25*s35 + tt*s25**2*s35 - 3*s15*s25**2*s35 - s25**3*s35 - 6*tt*s15*&
          &s35**2 - 2*tt*s25*s35**2 + 5*s15*s25*s35**2 + 2*s25**2*s35**2 + 8*me2**3*(14*ss &
          &+ 14*tt - 7*s15 - 8*s25 + 3*s35) + ss**3*(6*s15 + 6*s25 - 2*(4*tt + s35)) + ss**&
          &2*(-12*tt**2 - 9*s15**2 + 14*tt*s25 - 6*s25**2 + 3*s25*s35 + 2*s35**2 + s15*(8*t&
          &t - 13*s25 + 5*s35)) - 4*me2**2*(18*ss**2 + 18*tt**2 + 13*s15**2 - 9*tt*s25 + 6*&
          &s25**2 + s15*(tt + 16*s25 - 16*s35) - 10*tt*s35 - 7*s25*s35 + ss*(36*tt - 21*s15&
          & - 23*s25 + 8*s35)) + 2*me2*(10*ss**3 + 10*tt**3 - 4*s15**3 + s15**2*(8*tt - 9*s&
          &25) - 11*tt**2*s25 + 4*tt*s25**2 - 2*s25**3 - 9*tt**2*s35 + 3*tt*s25*s35 + s25**&
          &2*s35 + 4*s25*s35**2 + ss**2*(30*tt - 20*s15 - 21*s25 + 7*s35) + ss*(30*tt**2 + &
          &20*s15**2 - 26*tt*s25 + 13*s25**2 + 2*s15*(-5*tt + 14*s25 - 8*s35) - 8*tt*s35 - &
          &8*s25*s35 - 4*s35**2) + s15*(2*tt**2 + 7*tt*s25 - 7*s25**2 - 15*tt*s35 + 5*s25*s&
          &35 + 7*s35**2)) + ss*(2*s15**3 - 7*tt*s25**2 + 2*s25**3 + s15*(2*tt**2 - 15*tt*s&
          &25 + 7*s25**2 + 13*tt*s35 - 2*s25*s35 - 5*s35**2) + 2*tt*(-4*tt**2 + 2*tt*s35 + &
          &s35**2) - s25*(-12*tt**2 + tt*s35 + 4*s35**2) + s15**2*(7*s25 + 3*(-4*tt + s35))&
          &)))/((tt + s15 - s35)*(s15 + s25 - s35)) - (s15*(128*me2**4 + 2*(ss - s15)*(ss +&
          & tt - s25)*(ss + tt - s15 - s25)**2 - 8*me2**3*(23*ss + 17*tt - 19*s15 - 19*s25 &
          &+ 4*s35) + 2*me2**2*(48*ss**2 + 22*tt**2 + 24*s15**2 - 55*tt*s25 + 30*s25**2 + s&
          &15*(-61*tt + 62*s25 - 8*s35) + 14*tt*s35 - 11*s25*s35 + ss*(74*tt - 75*s15 - 78*&
          &s25 + 11*s35)) - me2*(22*ss**3 - 6*s15**3 + s15**2*(29*tt - 30*s25) + ss**2*(52*&
          &tt - 49*s15 - 52*s25 + 3*s35) + ss*(34*tt**2 + 35*s15**2 - 75*tt*s25 + 38*s25**2&
          & + s15*(-82*tt + 85*s25 - 9*s35) + 9*tt*s35 - 6*s25*s35) + s15*(-29*tt**2 - 32*s&
          &25**2 - 8*tt*s35 + 4*s35**2 + s25*(64*tt + s35)) - (-tt + s25)*(8*s25**2 - 3*s25&
          &*(5*tt + s35) + 2*tt*(2*tt + 3*s35)))))/(s25*(4*me2 - ss - tt + s35)) - (s15*(12&
          &8*me2**4 + 2*tt**4 + tt**3*s15 + tt**2*s15**2 - tt*s15**3 - 3*tt**3*s25 - tt**2*&
          &s15*s25 - tt*s15**2*s25 + tt*s15*s25**2 + tt*s25**3 - 2*tt**3*s35 - 6*tt**2*s15*&
          &s35 - tt*s15**2*s35 + tt**2*s25*s35 + 4*tt*s15*s25*s35 + s15**2*s25*s35 + 2*tt*s&
          &25**2*s35 - s25**3*s35 + 6*tt*s15*s35**2 + 2*tt*s25*s35**2 - 5*s15*s25*s35**2 - &
          &2*s25**2*s35**2 - 8*me2**3*(15*ss + 25*tt - 11*s15 - 12*s25 + 3*s35) + ss**3*(2*&
          &tt - 2*s15 + 4*s35) + ss**2*(6*tt**2 + 6*s15**2 + s15*(tt + 5*s25 - 8*s35) + 3*t&
          &t*s35 - 9*s25*s35 - 2*s35**2) + 2*me2**2*(18*ss**2 + 56*tt**2 + 28*s15**2 - 25*t&
          &t*s25 + 10*s25**2 + s15*(-7*tt + 34*s25 - 38*s35) - 26*tt*s35 - 23*s25*s35 + ss*&
          &(70*tt - 33*s15 - 26*s25 + 25*s35)) - me2*(4*ss**3 + 26*tt**3 - 8*s15**3 - 19*tt&
          &**2*s25 - 5*tt*s25**2 - 2*s25**3 - 24*tt**2*s35 + tt*s25*s35 + 15*s25**2*s35 + 8&
          &*s25*s35**2 + s15**2*(15*tt - 20*s25 + 4*s35) + ss**2*(30*tt - 21*s15 - 8*s25 + &
          &27*s35) + ss*(52*tt**2 + 37*s15**2 - 9*tt*s25 + 6*s25**2 + s15*(4*tt + 37*s25 - &
          &49*s35) - 11*tt*s35 - 42*s25*s35 - 8*s35**2) + s15*(5*tt**2 + 2*tt*s25 - 14*s25*&
          &*2 - 34*tt*s35 + 27*s25*s35 + 14*s35**2)) - ss*(s15**3 + s25**2*(3*tt - 6*s35) +&
          & s15**2*(-7*tt + 4*s25 + 2*s35) + s25*(3*tt**2 + 5*tt*s35 - 4*s35**2) + tt*(-6*t&
          &t**2 + 3*tt*s35 + 2*s35**2) + s15*(-4*tt**2 + 3*s25**2 + 15*tt*s35 - 5*s35**2 - &
          &s25*(tt + 8*s35)))))/(s35*(4*me2 - ss - tt + s35)) - (s15*(64*me2**4 + 2*tt**4 +&
          & tt**3*s15 + tt**2*s15**2 - tt*s15**3 - 3*tt**3*s25 - tt**2*s15*s25 - tt*s15**2*&
          &s25 + tt*s15*s25**2 + tt*s25**3 - 2*tt**3*s35 - 6*tt**2*s15*s35 - tt*s15**2*s35 &
          &+ tt**2*s25*s35 + 4*tt*s15*s25*s35 + s15**2*s25*s35 + 2*tt*s25**2*s35 - s25**3*s&
          &35 + 6*tt*s15*s35**2 + 2*tt*s25*s35**2 - 5*s15*s25*s35**2 - 2*s25**2*s35**2 - 16&
          &*me2**3*(5*ss + 7*tt - 3*s15 - 3*s25 + 2*s35) + ss**3*(2*tt - 2*s15 + 4*s35) + s&
          &s**2*(6*tt**2 + 6*s15**2 + s15*(tt + 5*s25 - 8*s35) + 3*tt*s35 - 9*s25*s35 - 2*s&
          &35**2) + 4*me2**2*(8*ss**2 + 18*tt**2 + 8*s15**2 - 4*tt*s25 + 2*s25**2 + s15*(4*&
          &tt + 9*s25 - 11*s35) - 9*tt*s35 - 8*s25*s35 - 3*s35**2 + ss*(26*tt - 13*s15 - 10&
          &*s25 + 13*s35)) - ss*(s15**3 + s25**2*(3*tt - 6*s35) + s15**2*(-7*tt + 4*s25 + 2&
          &*s35) + s25*(3*tt**2 + 5*tt*s35 - 4*s35**2) + tt*(-6*tt**2 + 3*tt*s35 + 2*s35**2&
          &) + s15*(-4*tt**2 + 3*s25**2 + 15*tt*s35 - 5*s35**2 - s25*(tt + 8*s35))) - 2*me2&
          &*(2*ss**3 + 10*tt**3 - 2*s15**3 - 7*tt**2*s25 - 5*tt*s25**2 - 9*tt**2*s35 + 4*tt&
          &*s25*s35 + 5*s25**2*s35 - 3*tt*s35**2 + 5*s25*s35**2 - s15**2*(-3*tt + 5*s25 + s&
          &35) + ss**2*(14*tt - 9*s15 - 4*s25 + 13*s35) + s15*(5*tt**2 - 5*tt*s25 - 3*s25**&
          &2 - 11*tt*s35 + 7*s25*s35 + 7*s35**2) + ss*(22*tt**2 + 15*s15**2 + 15*s15*s25 + &
          &2*s25**2 + 4*s15*(tt - 5*s35) - 3*tt*s35 - 5*s35**2 - 2*s25*(2*tt + 9*s35)))))/(&
          &tt*s35))*pvd9(2))/s15**2 + (64*Pi**2*((2*(-4*me2**2 + 4*me2*s15 + s15**2)*(2*me2&
          & - ss - tt + s25)*(4*me2 - ss - tt + s15 + s25))/tt + (2*(-4*me2**2 + 4*me2*s15 &
          &+ s15**2)*(16*me2**2 + (ss + tt - s25)*(ss + tt - s15 - s25) + me2*(-7*ss - 9*tt&
          & + 3*s15 + 7*s25)))/(4*me2 - ss - tt + s25) + (2*s15*(64*me2**4 + 2*(ss - s15)*s&
          &15*(ss + tt - s25)*(ss + tt - s15 - s25) - 4*me2**3*(15*ss + 9*tt + 5*s15 - 11*s&
          &25 + 4*s35) + me2**2*(18*ss**2 + 4*tt**2 + 15*tt*s15 - 34*s15**2 - 15*tt*s25 - 2&
          &2*s15*s25 + 8*s25**2 + 6*tt*s35 + 20*s15*s35 + s25*s35 - 4*s35**2 + ss*(26*tt + &
          &37*s15 - 26*s25 + 3*s35)) - me2*(2*ss**3 + 2*ss**2*(2*tt + 7*s15 - 2*s25) + s15*&
          &(2*tt**2 - 18*tt*s15 + 6*s15**2 - 11*tt*s25 + 19*s15*s25 + 6*s25**2 + 8*tt*s35 -&
          & s25*s35 - 4*s35**2) + ss*(-22*s15**2 + 2*(-tt + s25)**2 + 5*s15*(4*tt - 4*s25 +&
          & s35)))))/(s25*(4*me2 - ss - tt + s35)) - (s15*(64*me2**4 - 8*me2**3*(10*ss + 10&
          &*tt + 5*s15 - 4*s25 + 3*s35) + 4*me2**2*(8*ss**2 + 8*tt**2 - 10*s15**2 - 13*tt*s&
          &25 + 2*s25**2 - 2*s15*(tt + 3*s25 - 2*s35) + 5*tt*s35 - 2*s25*s35 - 2*s35**2 + s&
          &s*(16*tt + 12*s15 - 9*s25 + 5*s35)) + s15*(4*ss**3 + 5*tt*s15**2 - 4*tt**2*s25 +&
          & 6*tt*s25**2 + 6*tt**2*s35 - 6*tt*s25*s35 - 2*s25**2*s35 - 4*tt*s35**2 + 4*s25*s&
          &35**2 + ss**2*(-10*s15 + 4*(tt - 2*s25 + s35)) + ss*(5*s15**2 + 4*s25**2 + 2*(7*&
          &tt - 2*s35)*s35 - 2*s25*(6*tt + s35) + s15*(-12*tt + 8*s25 + s35)) - s15*(s25*(-&
          &9*tt + s35) + 2*tt*(tt + 2*s35))) - 2*me2*(2*ss**3 + 2*tt**3 + 7*s15**3 - 6*tt**&
          &2*s25 + 5*tt*s25**2 + 2*tt**2*s35 - 2*tt*s25*s35 - s25**2*s35 - 2*tt*s35**2 + 2*&
          &s25*s35**2 + 2*ss**2*(3*tt + 6*s15 - 2*s25 + s35) + s15*(-2*tt**2 - 4*tt*s25 + 4&
          &*s25**2 + 10*tt*s35 - 3*s25*s35 - 4*s35**2) + s15**2*(7*s25 - 3*(tt + s35)) + ss&
          &*(6*tt**2 - 19*s15**2 + 2*s25**2 + 6*tt*s35 - 2*s35**2 - s25*(12*tt + s35) + s15&
          &*(2*tt - 15*s25 + 9*s35)))))/((tt + s15 - s35)*(s15 + s25 - s35)) + (s15*(-128*m&
          &e2**4 + 8*me2**3*(15*ss + 17*tt + 11*s15 - 8*s25 + s35) - 4*me2**2*(9*ss**2 + 11&
          &*tt**2 - 13*s15**2 - 17*tt*s25 + 3*s25**2 + 4*tt*s35 + s25*s35 - 4*s35**2 + s15*&
          &(7*tt - 11*s25 + 4*s35) + ss*(20*tt + 21*s15 - 11*s25 + 4*s35)) + s15*(-4*ss**3 &
          &+ 2*tt**2*s15 - 5*tt*s15**2 + 4*tt**2*s25 - 9*tt*s15*s25 - 6*tt*s25**2 - 6*tt**2&
          &*s35 + 4*tt*s15*s35 + 6*tt*s25*s35 + s15*s25*s35 + 2*s25**2*s35 + 4*tt*s35**2 - &
          &4*s25*s35**2 + 2*ss**2*(5*s15 + 4*s25 - 2*(tt + s35)) - ss*(5*s15**2 + 4*s25**2 &
          &+ 2*(7*tt - 2*s35)*s35 - 2*s25*(6*tt + s35) + s15*(-12*tt + 8*s25 + s35))) + 2*m&
          &e2*(2*ss**3 + 2*tt**3 + 7*s15**3 - 6*tt**2*s25 + 5*tt*s25**2 + 2*tt**2*s35 - 2*t&
          &t*s25*s35 - s25**2*s35 - 2*tt*s35**2 + 2*s25*s35**2 + 2*ss**2*(3*tt + 7*s15 - 2*&
          &s25 + s35) + s15**2*(-9*tt + 13*s25 + s35) + ss*(6*tt**2 - 23*s15**2 + 2*s25**2 &
          &+ 6*tt*s35 - 2*s35**2 - s25*(12*tt + s35) + s15*(8*tt - 19*s25 + 9*s35)) + s15*(&
          &6*s25**2 + s25*(-10*tt + s35) + 2*(tt**2 + 5*tt*s35 - 4*s35**2)))))/((4*me2 - ss&
          & - tt + s25)*(s15 + s25 - s35)) + (s15*(128*me2**4 - 8*me2**3*(7*ss + 17*tt + 13&
          &*s15 - 4*s25 + 3*s35) + 2*me2**2*(4*ss**2 + 22*tt**2 - 22*s15**2 - 23*tt*s25 + 2&
          &*s25**2 + 8*tt*s35 - 13*s25*s35 - 4*s35**2 + s15*(21*tt - 12*s25 + 4*s35) + ss*(&
          &22*tt + 15*s15 - 4*s25 + 19*s35)) - 2*me2*(2*tt**3 + 7*s15**3 - 5*tt**2*s25 + 3*&
          &tt*s25**2 + s15**2*(-4*tt + 8*s25 - 7*s35) + 2*tt**2*s35 - 3*tt*s25*s35 + s25**2&
          &*s35 - 2*tt*s35**2 + 2*s25*s35**2 + 2*ss**2*(tt + s15 + 2*s35) + s15*(3*tt**2 - &
          &2*tt*s25 + 2*s25**2 + 8*tt*s35 - 11*s25*s35 - 4*s35**2) - ss*(-4*tt**2 + 18*s15*&
          &*2 + 6*tt*s25 + s15*(7*tt + 3*s25 - 17*s35) - 7*tt*s35 + 5*s25*s35 + 2*s35**2)) &
          &+ s15*(3*tt*s15**2 - 2*tt**2*s25 + 3*tt*s15*s25 + 2*tt*s25**2 + 6*tt**2*s35 - 4*&
          &tt*s15*s35 - 8*tt*s25*s35 + s15*s25*s35 + 2*s25**2*s35 - 4*tt*s35**2 + 4*s25*s35&
          &**2 + ss**2*(-4*tt - 6*s15 + 8*s35) + ss*(3*s15**2 + s15*(-6*tt + 4*s25 - s35) -&
          & 10*s25*s35 - 4*(tt**2 - 4*tt*s35 + s35**2)))))/(s35*(4*me2 - ss - tt + s35)) + &
          &(s15*(64*me2**4 - 16*me2**3*(3*ss + 5*tt + 3*s15 - s25 + 2*s35) + 4*me2**2*(2*ss&
          &**2 + 8*tt**2 - 6*s15**2 - 10*tt*s25 - 3*s15*s25 + 5*tt*s35 + 5*s15*s35 - 4*s25*&
          &s35 - 3*s35**2 + ss*(10*tt + 7*s15 - 2*s25 + 9*s35)) - 2*me2*(2*tt**3 + 4*s15**3&
          & - 5*tt**2*s25 + 3*tt*s25**2 + s15**2*(tt + 4*s25 - 2*s35) + 2*tt**2*s35 - 3*tt*&
          &s25*s35 + s25**2*s35 - 2*tt*s35**2 + 2*s25*s35**2 + 2*ss**2*(tt + s15 + 2*s35) -&
          & ss*(-4*tt**2 + 14*s15**2 + 6*tt*s25 + s15*(7*tt + 3*s25 - 16*s35) - 7*tt*s35 + &
          &5*s25*s35 + 2*s35**2) - s15*(tt**2 - tt*s25 - 8*tt*s35 + 6*s25*s35 + 6*s35**2)) &
          &+ s15*(3*tt*s15**2 - 2*tt**2*s25 + 3*tt*s15*s25 + 2*tt*s25**2 + 6*tt**2*s35 - 4*&
          &tt*s15*s35 - 8*tt*s25*s35 + s15*s25*s35 + 2*s25**2*s35 - 4*tt*s35**2 + 4*s25*s35&
          &**2 + ss**2*(-4*tt - 6*s15 + 8*s35) + ss*(3*s15**2 + s15*(-6*tt + 4*s25 - s35) -&
          & 10*s25*s35 - 4*(tt**2 - 4*tt*s35 + s35**2)))))/(tt*s35) - (2*s15*(-32*me2**4 - &
          &2*(ss - s15)*s15*(ss + tt - s25)*(ss + tt - s15 - s25) + 8*me2**3*(5*ss + 3*tt +&
          & s15 - 4*s25 + s35) - 2*me2**2*(8*ss**2 + 2*tt**2 + 5*tt*s15 - 12*s15**2 - 4*tt*&
          &s25 - 7*s15*s25 + 4*s25**2 + 2*s15*s35 - 3*s25*s35 + s35**2 + ss*(10*tt + 10*s15&
          & - 11*s25 + s35)) + me2*(2*ss**3 + 4*ss**2*(tt + 3*s15 - s25) - 2*ss*(9*s15**2 -&
          & (-tt + s25)**2 + s15*(-7*tt + 8*s25 - s35)) + s15*(7*s15**2 + s15*(-13*tt + 17*&
          &s25 - 7*s35) + 2*(tt**2 + 3*s25**2 + tt*s35 + s35**2 - 3*s25*(tt + s35))))))/(s2&
          &5*(tt + s15 - s35)))*pvd9(3))/s15**2 + (64*Pi**2*((2*(2*me2 - ss - tt + s25)*(4*&
          &me2 - ss - tt + s15 + s25)*(2*me2*(-tt + s15) + s15*(s15 - s35)))/tt + (2*(16*me&
          &2**2 + (ss + tt - s25)*(ss + tt - s15 - s25) + me2*(-7*ss - 9*tt + 3*s15 + 7*s25&
          &))*(2*me2*(-tt + s15) + s15*(s15 - s35)))/(4*me2 - ss - tt + s25) - (2*s15*(16*m&
          &e2**3*(-tt + s15) - (ss - s15)*(-tt + s15)*(ss + tt - s25)*(ss + tt - s15 - s25)&
          & + me2*(4*s15**3 + 8*ss**2*(-tt + s15) + s15**2*(-11*tt + 10*s25 - 4*s35) - ss*(&
          &-tt + s15)*(-10*tt + 10*s15 + 11*s25 - s35) + s15*(9*tt**2 - 15*tt*s25 + 4*s25**&
          &2 + 7*tt*s35 - 4*s25*s35) + tt*(-2*tt**2 + 4*tt*s25 - 4*s25**2 + 5*s25*s35 - 3*s&
          &35**2)) + 2*me2**2*(9*s15**2 - 10*ss*(-tt + s15) + s15*(-12*tt + 8*s25 - 7*s35) &
          &+ 2*(3*tt**2 - 4*tt*s25 + tt*s35 + s35**2))))/(s25*(tt + s15 - s35)) - (s15*(64*&
          &me2**3*(-tt + s15) - 2*(ss - s15)*(-tt + s15)*(ss + tt - s25)*(ss + tt - s15 - s&
          &25) + 4*me2**2*(9*tt**2 + 9*s15**2 - 15*ss*(-tt + s15) - 11*tt*s25 + s15*(-20*tt&
          & + 11*s25 - 4*s35) + 4*tt*s35 + 2*s35**2) + me2*(-4*tt**3 + 6*s15**3 + 18*ss**2*&
          &(-tt + s15) + 15*tt**2*s25 - 8*tt*s25**2 + 5*s15**2*(-5*tt + 4*s25) - 6*tt**2*s3&
          &5 - tt*s25*s35 + 4*s25*s35**2 - ss*(25*s15**2 + 26*tt*(tt - s25) + s15*(-49*tt +&
          & 26*s25 - 9*s35) + 3*tt*s35 + 4*s35**2) + s15*(25*tt**2 + 8*s25**2 + 8*tt*s35 - &
          &4*s35**2 - s25*(37*tt + s35)))))/(s25*(4*me2 - ss - tt + s35)) + (s15*(2*tt**4 +&
          & 3*tt**2*s15**2 - 2*tt*s15**3 + 64*me2**3*(-tt + s15) - 2*ss**3*(-tt + s15) - 6*&
          &tt**3*s25 + 4*tt**2*s15*s25 - 3*tt*s15**2*s25 + 5*tt**2*s25**2 - tt*s15*s25**2 -&
          & 2*tt**3*s35 - 6*tt**2*s15*s35 - tt*s15**2*s35 + 6*tt**2*s25*s35 + 2*tt*s15*s25*&
          &s35 + 2*s15**2*s25*s35 - 5*tt*s25**2*s35 + s15*s25**2*s35 + 6*tt*s15*s35**2 - 5*&
          &s15*s25*s35**2 + 4*me2**2*(17*tt**2 + 11*s15**2 - 15*ss*(-tt + s15) - 8*tt*s25 +&
          & s15*(-2*tt + 8*s25 - 11*s35) - 19*tt*s35 + 4*s35**2) + ss**2*(6*s15**2 + 4*s15*&
          &s25 + 2*tt*(3*tt - 2*s25 - 3*s35) - 2*s15*(tt + 2*s35)) + ss*(-2*s15**3 + s15**2&
          &*(9*tt - 5*s25 - 3*s35) + s15*(-2*s25**2 + 3*s25*(tt + s35) + s35*(-9*tt + 5*s35&
          &)) + tt*(2*s25**2 + 6*tt*(tt - s35) + s25*(-12*tt + 11*s35))) + 2*me2*(-11*tt**3&
          & + 4*s15**3 + 9*ss**2*(-tt + s15) + 17*tt**2*s25 - 3*tt*s25**2 + 14*tt**2*s35 - &
          &19*tt*s25*s35 - 2*tt*s35**2 + 2*s25*s35**2 + 2*s15**2*(-4*tt + 4*s25 + s35) + s1&
          &5*(3*s25**2 + (10*tt - 9*s35)*s35 - 4*s25*(tt + s35)) + ss*(-20*tt**2 - 17*s15**&
          &2 + 11*tt*s25 + 22*tt*s35 - 2*s35**2 + s15*(3*tt - 11*s25 + 14*s35)))))/((4*me2 &
          &- ss - tt + s25)*(s15 + s25 - s35)) - (s15*(2*tt**4 + tt**3*s15 + tt**2*s15**2 -&
          & tt*s15**3 + 32*me2**3*(-tt + s15) - 5*tt**3*s25 + 3*tt**2*s25**2 + tt*s15*s25**&
          &2 + 8*me2**2*(3*s15**2 - 3*ss*(-tt + s15) + tt*(5*tt - s25 - 7*s35) + s15*(3*tt &
          &+ s25 - 4*s35)) - 2*tt**3*s35 - 6*tt**2*s15*s35 - tt*s15**2*s35 + 5*tt**2*s25*s3&
          &5 + 4*tt*s15*s25*s35 + s15**2*s25*s35 - 3*tt*s25**2*s35 - s15*s25**2*s35 + 6*tt*&
          &s15*s35**2 - 5*s15*s25*s35**2 + 2*ss**2*(tt**2 + 2*tt*s15 + 2*s15**2 - 2*tt*s35 &
          &- 3*s15*s35) - ss*(s15**3 + s15**2*(-5*tt + 3*s25 + 2*s35) + tt*(-4*tt**2 + 6*tt&
          &*s25 + 5*tt*s35 - 7*s25*s35) + s15*(-5*tt**2 + 5*tt*s25 + 11*tt*s35 - 7*s25*s35 &
          &- 5*s35**2)) + 2*me2*(2*s15**3 + 2*ss**2*(-tt + s15) + s15**2*(-tt + 3*s25 + s35&
          &) + s15*(-6*tt**2 + 7*tt*s25 + 7*tt*s35 - 6*s25*s35 - 7*s35**2) + tt*(-8*tt**2 +&
          & 10*tt*s25 + 11*tt*s35 - 14*s25*s35 + s35**2) + ss*(-11*s15**2 + s15*(-9*tt - 2*&
          &s25 + 15*s35) + tt*(-10*tt + 2*s25 + 15*s35)))))/(tt*s35) + (s15*(2*tt**4 + 3*tt&
          &**2*s15**2 - 2*tt*s15**3 + 32*me2**3*(-tt + s15) - 2*ss**3*(-tt + s15) - 6*tt**3&
          &*s25 + 4*tt**2*s15*s25 - 3*tt*s15**2*s25 + 5*tt**2*s25**2 - tt*s15*s25**2 - 2*tt&
          &**3*s35 - 6*tt**2*s15*s35 - tt*s15**2*s35 + 6*tt**2*s25*s35 + 2*tt*s15*s25*s35 +&
          & 2*s15**2*s25*s35 - 5*tt*s25**2*s35 + s15*s25**2*s35 + 6*tt*s15*s35**2 - 5*s15*s&
          &25*s35**2 + 4*me2**2*(10*tt**2 + 10*s15**2 - 10*ss*(-tt + s15) - 4*tt*s25 + s15*&
          &(5*tt + 4*s25 - 12*s35) - 15*tt*s35 + 2*s35**2) + ss**2*(6*s15**2 + 4*s15*s25 + &
          &2*tt*(3*tt - 2*s25 - 3*s35) - 2*s15*(tt + 2*s35)) + ss*(-2*s15**3 + s15**2*(9*tt&
          & - 5*s25 - 3*s35) + s15*(-2*s25**2 + 3*s25*(tt + s35) + s35*(-9*tt + 5*s35)) + t&
          &t*(2*s25**2 + 6*tt*(tt - s35) + s25*(-12*tt + 11*s35))) + 2*me2*(4*s15**3 + 8*ss&
          &**2*(-tt + s15) + 5*s15**2*(-tt + s25) + s15*(-4*tt**2 + tt*s25 + 2*s25**2 + 11*&
          &tt*s35 - 5*s25*s35 - 7*s35**2) - tt*(8*tt**2 - 13*tt*s25 + 2*s25**2 - 11*tt*s35 &
          &+ 14*s25*s35 + 2*s35**2) + ss*(-14*s15**2 + s15*(-9*s25 + 11*s35) + tt*(-16*tt +&
          & 9*s25 + 19*s35)))))/((tt + s15 - s35)*(s15 + s25 - s35)) - (s15*(2*tt**4 + tt**&
          &3*s15 + tt**2*s15**2 - tt*s15**3 + 64*me2**3*(-tt + s15) - 5*tt**3*s25 + 3*tt**2&
          &*s25**2 + tt*s15*s25**2 - 2*tt**3*s35 - 6*tt**2*s15*s35 - tt*s15**2*s35 + 5*tt**&
          &2*s25*s35 + 4*tt*s15*s25*s35 + s15**2*s25*s35 - 3*tt*s25**2*s35 - s15*s25**2*s35&
          & + 6*tt*s15*s35**2 - 5*s15*s25*s35**2 + 2*ss**2*(tt**2 + 2*tt*s15 + 2*s15**2 - 2&
          &*tt*s35 - 3*s15*s35) + 4*me2**2*(7*ss*tt + 17*tt**2 - 7*ss*s15 + 11*s15**2 - 4*t&
          &t*s25 + 4*s15*s25 - 17*tt*s35 - 13*s15*s35 + 2*s35**2) - ss*(s15**3 + s15**2*(-5&
          &*tt + 3*s25 + 2*s35) + tt*(-4*tt**2 + 6*tt*s25 + 5*tt*s35 - 7*s25*s35) + s15*(-5&
          &*tt**2 + 5*tt*s25 + 11*tt*s35 - 7*s25*s35 - 5*s35**2)) + me2*(8*s15**3 + 4*ss**2&
          &*(-tt + s15) + s15**2*(-11*tt + 12*s25 - 4*s35) + s15*(-7*tt**2 + 9*tt*s25 + 2*s&
          &25**2 + 26*tt*s35 - 21*s25*s35 - 14*s35**2) - tt*(22*tt**2 + 2*s25**2 - 28*tt*s3&
          &5 + 4*s35**2 + 23*s25*(-tt + s35)) + ss*(-27*s15**2 + s15*(-17*tt - 4*s25 + 33*s&
          &35) + tt*(-22*tt + 4*s25 + 33*s35)))))/(s35*(4*me2 - ss - tt + s35)))*pvd9(4))/s&
          &15**2 + 128*Pi**2*((2*me2*(1 - (2*me2*(s15 - s35))/(s15*(tt + s15 - s35))))/s25 &
          &+ (2*me2*s15*(ss + tt - s35) - 4*me2**2*(2*s15 + s25 - s35))/(s15*s25*(4*me2 - s&
          &s - tt + s35)) + (4*me2**2*(4*s15 + s25 - 3*s35) - 2*me2*(2*ss*s15 + 6*tt*s15 + &
          &2*s15**2 + tt*s25 - ss*s35 - 2*tt*s35 - 3*s15*s35 + s25*s35) + s15*(tt*(2*tt + s&
          &15 - 2*s35) + ss*(2*tt + s15 - s35) + s25*(-tt + s35)))/(s15*(4*me2 - ss - tt + &
          &s25)*(s15 + s25 - s35)) + (8*me2**2*(s15 - s35) - 2*me2*(5*tt*s15 + 2*s15**2 + t&
          &t*s25 + ss*(s15 - s35) - 2*tt*s35 - 2*s15*s35 + s25*s35) + s15*(tt*(2*tt + s15 -&
          & 2*s35) + ss*(2*tt + s15 - s35) + s25*(-tt + s35)))/(s15*(tt + s15 - s35)*(s15 +&
          & s25 - s35)) - (8*me2**2*(s15 - s35) - 2*me2*(5*tt*s15 + 2*s15**2 + tt*s25 + ss*&
          &(s15 - s35) - 2*tt*s35 - 2*s15*s35 + s25*s35) + s15*(tt*(2*tt + s15 - 2*s35) + s&
          &s*(2*tt + s15 - s35) + s25*(-tt + s35)))/(s15*s35*(4*me2 - ss - tt + s35)) - (4*&
          &me2**2*(s15 - s35) - 2*me2*(4*tt*s15 + s15**2 + tt*s25 + ss*(s15 - s35) - 2*tt*s&
          &35 - s15*s35 + s25*s35) + s15*(tt*(2*tt + s15 - 2*s35) + ss*(2*tt + s15 - s35) +&
          & s25*(-tt + s35)))/(tt*s15*s35))*pvd9(5) + (64*Pi**2*((-2*me2*(2*me2 - ss - tt +&
          & s25)*(s15 + s25 - s35)**2)/(s25*(4*me2 - ss - tt + s35)) - (2*me2*tt*(s15 + s25&
          & - s35)**2 - (-2*me2 + ss + tt - s35)**2*(tt*s15 + tt*s25 - 2*me2*(s15 - s35) + &
          &ss*(s15 - s35) - 2*tt*s35 + s25*s35) + (s15 + s25 - s35)*(2*me2 - ss - tt + s35)&
          &*(-2*tt**2 - tt*s15 + tt*s25 + 2*me2*(3*tt + s15 - s35) + 2*tt*s35 - s25*s35 + s&
          &s*(-2*tt - s15 + s35)))/(tt*s35) + (2*me2*(tt**2*s15 + 2*tt*s15**2 + tt**2*s25 +&
          & 3*tt*s15*s25 + tt*s25**2 + 4*me2**2*(s15 + s25) + ss**2*s35 - 5*tt*s15*s35 - s1&
          &5**2*s35 - 4*tt*s25*s35 - s15*s25*s35 + tt*s35**2 + 2*s15*s35**2 + s25*s35**2 + &
          &ss*(s15**2 + s15*(tt + s25 - 2*s35) - (-tt + s35)*(s25 + s35)) - 2*me2*(2*tt*s15&
          & + 2*s15**2 + 2*tt*s25 + 3*s15*s25 + s25**2 - 5*s15*s35 - 4*s25*s35 + s35**2 + s&
          &s*(s15 + s25 + s35))))/(s25*(tt + s15 - s35)) - (8*me2**3*(s25 + s35) - 4*me2**2&
          &*(3*s15**2 + 4*tt*s25 + s25**2 + s15*(2*tt + 4*s25 - 5*s35) + tt*s35 - 6*s25*s35&
          & + ss*(-2*s15 + s25 + 4*s35)) - (ss + tt - s35)*(tt*s15**2 + s25*(-tt + s25)*(-t&
          &t + s35) + ss**2*(-s15 + s35) + ss*(s15**2 + tt*s25 + s15*(s25 - s35) + tt*s35 -&
          & 2*s25*s35) + s15*(tt**2 - 2*tt*s35 + s25*s35)) - 2*me2*(ss**2*(3*s15 - 4*s35) +&
          & s15**2*(-3*tt + 2*s35) - s25*(-tt + s35)*(-4*tt + s25 + 3*s35) + s15*(-3*tt**2 &
          &- 2*tt*s25 + 7*tt*s35 + s25*s35 - 3*s35**2) + ss*(-3*s15**2 - 4*tt*s25 - 3*s15*s&
          &25 - 4*tt*s35 + 2*s15*s35 + 5*s25*s35 + 3*s35**2)))/((tt + s15 - s35)*(s15 + s25&
          & - s35)) - (-8*me2**3*(s25 + s35) + 4*me2**2*(3*s15**2 + 4*tt*s25 + s25**2 + s15&
          &*(2*tt + 4*s25 - 5*s35) + tt*s35 - 6*s25*s35 + ss*(-2*s15 + s25 + 4*s35)) + (ss &
          &+ tt - s35)*(tt*s15**2 + s25*(-tt + s25)*(-tt + s35) + ss**2*(-s15 + s35) + ss*(&
          &s15**2 + tt*s25 + s15*(s25 - s35) + tt*s35 - 2*s25*s35) + s15*(tt**2 - 2*tt*s35 &
          &+ s25*s35)) + 2*me2*(ss**2*(3*s15 - 4*s35) + s15**2*(-3*tt + 2*s35) - s25*(-tt +&
          & s35)*(-4*tt + s25 + 3*s35) + s15*(-3*tt**2 - 2*tt*s25 + 7*tt*s35 + s25*s35 - 3*&
          &s35**2) + ss*(-3*s15**2 - 4*tt*s25 - 3*s15*s25 - 4*tt*s35 + 2*s15*s35 + 5*s25*s3&
          &5 + 3*s35**2)))/(s35*(4*me2 - ss - tt + s35)) + (-8*me2**3*(s25 + s35) + 4*me2**&
          &2*(4*s15**2 + 4*tt*s25 + 2*s25**2 + s15*(2*tt + 6*s25 - 7*s35) + tt*s35 - 8*s25*&
          &s35 + s35**2 + ss*(-2*s15 + s25 + 4*s35)) + (ss + tt - s35)*(tt*s15**2 + s25*(-t&
          &t + s25)*(-tt + s35) + ss**2*(-s15 + s35) + ss*(s15**2 + tt*s25 + s15*(s25 - s35&
          &) + tt*s35 - 2*s25*s35) + s15*(tt**2 - 2*tt*s35 + s25*s35)) + 2*me2*(-4*tt**2*s2&
          &5 + s25**3 + ss**2*(3*s15 - 4*s35) + 9*tt*s25*s35 - 3*s25**2*s35 - tt*s35**2 - 2&
          &*s25*s35**2 + s15**2*(-4*tt + s25 + 2*s35) - ss*(4*s15**2 + 4*tt*s25 + 5*s15*s25&
          & + s25**2 + 4*tt*s35 - 4*s15*s35 - 7*s25*s35 - 2*s35**2) + s15*(2*s25**2 - s25*(&
          &4*tt + s35) - 3*(tt**2 - 3*tt*s35 + s35**2))))/((4*me2 - ss - tt + s25)*(s15 + s&
          &25 - s35)))*pvd9(6))/s15 + (64*Pi**2*((-2*me2*(4*me2**2*(s25 + s35) + s15*(-tt**&
          &2 + s15**2 - tt*s25 - ss*(tt + s15 - 3*s35) + s15*(-2*tt + s25 - s35) + 5*tt*s35&
          & - s25*s35 - 2*s35**2) + 2*me2*(s15**2 + s15*(tt - 3*s35) + ss*(s15 - 2*s35) + (&
          &-tt + s35)*(s25 + s35))))/(s25*(tt + s15 - s35)) + (2*me2*(4*me2**2*(2*s15 + s25&
          & - s35) - 2*me2*(3*s15**2 + 3*s15*(tt + s25 - 2*s35) + ss*(3*s15 + s25 - s35) - &
          &(s25 - s35)*(-tt + s35)) + s15*(ss**2 + tt**2 + 3*tt*s25 - 2*s25**2 + ss*(2*tt +&
          & 3*s15 + 3*s25 - 5*s35) - 5*tt*s35 + s25*s35 + 2*s35**2 - s15*(-3*tt + 2*s25 + s&
          &35))))/(s25*(4*me2 - ss - tt + s35)) - (32*me2**3*(s15 - s35) - 4*me2**2*(6*s15*&
          &*2 - tt*s25 + ss*(8*s15 + s25 - 7*s35) + 2*s15*(4*tt + s25 - 6*s35) - 5*tt*s35 +&
          & 3*s25*s35 + 3*s35**2) + s15*(tt*s15**2 + ss**2*(-2*tt - 3*s15 + 3*s35) + s15*(-&
          &tt**2 + s25*s35) + (-tt + s35)*(2*tt**2 - tt*s25 + s25**2 - 4*tt*s35 + 2*s25*s35&
          &) + ss*(-4*tt**2 + s15**2 + tt*s25 + 9*tt*s35 - 4*s25*s35 - 2*s35**2 + s15*(-4*t&
          &t + s25 + s35))) + 2*me2*(-2*s15**3 + ss**2*(3*s15 - 2*s35) - s15**2*(-5*tt + 4*&
          &s25 + s35) - (-tt + s35)*(s25**2 - 2*tt*s35 + s25*s35) + s15*(7*tt**2 - 2*s25**2&
          & - 16*tt*s35 + 5*s25*s35 + 6*s35**2) + ss*(9*s15**2 + 2*s15*(5*tt + s25 - 7*s35)&
          & + s35*(-4*tt + 3*s25 + s35))))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + (1&
          &6*me2**3*(s15 - s35) - 4*me2**2*(2*s15**2 - tt*s25 + 4*ss*(s15 - s35) - 3*tt*s35&
          & + 3*s25*s35 + s35**2 - s15*(-4*tt + s25 + 3*s35)) + s15*(tt*s15**2 + ss**2*(-2*&
          &tt - 3*s15 + 3*s35) + s15*(-tt**2 + s25*s35) + (-tt + s35)*(2*tt**2 - tt*s25 + s&
          &25**2 - 4*tt*s35 + 2*s25*s35) + ss*(-4*tt**2 + s15**2 + tt*s25 + 9*tt*s35 - 4*s2&
          &5*s35 - 2*s35**2 + s15*(-4*tt + s25 + s35))) + 2*me2*(-s15**3 + 2*ss**2*(s15 - s&
          &35) - s15**2*(s25 + s35) - (-tt + s35)*(s25**2 - 2*tt*s35 + s25*s35) + s15*(5*tt&
          &**2 - 4*tt*s25 - 6*tt*s35 + 3*s25*s35 + 2*s35**2) + ss*(5*s15**2 + s35*(-4*tt + &
          &3*s25 + s35) - s15*(-7*tt + s25 + 6*s35))))/(tt*s35) + (8*me2**3*(-2*s15 + s25 +&
          & 3*s35) + 4*me2**2*(5*ss*s15 + 5*tt*s15 + 3*s15**2 - 2*tt*s25 - s15*s25 - 6*ss*s&
          &35 - 4*tt*s35 - 6*s15*s35 + 4*s25*s35 + 2*s35**2) - s15*(tt*s15**2 + ss**2*(-2*t&
          &t - 3*s15 + 3*s35) + s15*(-tt**2 + s25*s35) + (-tt + s35)*(2*tt**2 - tt*s25 + s2&
          &5**2 - 4*tt*s35 + 2*s25*s35) + ss*(-4*tt**2 + s15**2 + tt*s25 + 9*tt*s35 - 4*s25&
          &*s35 - 2*s35**2 + s15*(-4*tt + s25 + s35))) - 2*me2*(-2*s15**3 - 2*s15**2*(-tt +&
          & s25) + 2*ss**2*(s15 - s35) + s15*(-2*tt + s25 + s35)*(-3*tt + 4*s35) - (-tt + s&
          &35)*(s25**2 - 2*tt*s35 + s25*s35) + ss*(6*s15**2 + s35*(-4*tt + 3*s25 + s35) - s&
          &15*(-8*tt + s25 + 9*s35))))/((tt + s15 - s35)*(s15 + s25 - s35)) + (8*me2**3*(2*&
          &s15 - s25 - 3*s35) - 4*me2**2*(5*ss*s15 + 5*tt*s15 + 3*s15**2 - 2*tt*s25 - s15*s&
          &25 - 6*ss*s35 - 4*tt*s35 - 6*s15*s35 + 4*s25*s35 + 2*s35**2) + s15*(tt*s15**2 + &
          &ss**2*(-2*tt - 3*s15 + 3*s35) + s15*(-tt**2 + s25*s35) + (-tt + s35)*(2*tt**2 - &
          &tt*s25 + s25**2 - 4*tt*s35 + 2*s25*s35) + ss*(-4*tt**2 + s15**2 + tt*s25 + 9*tt*&
          &s35 - 4*s25*s35 - 2*s35**2 + s15*(-4*tt + s25 + s35))) + 2*me2*(-2*s15**3 - 2*s1&
          &5**2*(-tt + s25) + 2*ss**2*(s15 - s35) + s15*(-2*tt + s25 + s35)*(-3*tt + 4*s35)&
          & - (-tt + s35)*(s25**2 - 2*tt*s35 + s25*s35) + ss*(6*s15**2 + s35*(-4*tt + 3*s25&
          & + s35) - s15*(-8*tt + s25 + 9*s35))))/(s35*(4*me2 - ss - tt + s35)))*pvd9(7))/s&
          &15 + (64*Pi**2*((-2*me2*(-2*tt*s15**2 + tt**2*s25 - 2*tt*s15*s25 + 2*s15**2*s25 &
          &+ 2*s15*s25**2 - 4*me2**2*(s15 - s35) + 4*tt*s15*s35 - 4*s15*s25*s35 - 2*s25**2*&
          &s35 - 2*tt*s35**2 + 3*s25*s35**2 + ss**2*(-s15 + s35) + 2*me2*(2*s15**2 - tt*s25&
          & + s15*(tt + 2*s25 - 5*s35) + 2*ss*(s15 - s35) - tt*s35 - s25*s35 + 3*s35**2) + &
          &ss*(-2*s15**2 - s15*(tt + 2*s25 - 5*s35) + (tt - 3*s35)*s35 + s25*(tt + s35))))/&
          &(s25*(4*me2 - ss - tt + s35)) + (2*me2*(2*tt**2*s15 + 4*tt*s15**2 + tt**2*s25 + &
          &3*tt*s15*s25 + 4*me2**2*(s15 - s35) - 10*tt*s15*s35 - 2*s15**2*s35 - 4*tt*s25*s3&
          &5 - s15*s25*s35 + 2*tt*s35**2 + 4*s15*s35**2 + s25*s35**2 + ss*(s15**2 + s15*(tt&
          & - 2*s35) + (tt - s35)*s35) - 2*me2*(3*s15**2 + s15*(3*tt + 2*s25 - 8*s35) + ss*&
          &(s15 - s35) - (s25 - s35)*(-tt + 3*s35))))/(s25*(tt + s15 - s35)) - ((-2*tt*s15*&
          &*2 + s15*(-2*tt + s25)*(tt - 2*s35) + s25**2*(tt - s35))*(-tt + s35) + 4*me2**2*&
          &(s15 - s35)*(tt + s15 + s35) + ss**2*(s15**2 - s35**2) + ss*(s15**2*(3*tt - 2*s3&
          &5) + 2*s25*s35*(-tt + s35) + s15*(2*tt**2 - 5*tt*s35 + 2*s35**2)) - 2*me2*(tt**2&
          &*s25 + s15**2*(tt - 2*s35) - 2*tt**2*s35 - tt*s25*s35 + 2*s25*s35**2 + ss*(s15 -&
          & s35)*(tt + 2*s15 + 2*s35) + s15*(4*tt**2 - 2*tt*s25 - 3*tt*s35 + 2*s35**2)))/(t&
          &t*s35) + (-8*me2**3*(s15 - s35) + (-2*tt*s15**2 + s15*(-2*tt + s25)*(tt - 2*s35)&
          & + s25**2*(tt - s35))*(-tt + s35) + ss**2*(s15**2 - s35**2) + 4*me2**2*(4*s15**2&
          & + tt*s25 + 2*s15*(2*tt + s25 - 4*s35) + ss*(s15 - s35) - 2*tt*s35 - 3*s25*s35 +&
          & 2*s35**2) + ss*(s15**2*(3*tt - 2*s35) + 2*s25*s35*(-tt + s35) + s15*(2*tt**2 - &
          &5*tt*s35 + 2*s35**2)) - 2*me2*(s15**2*(5*tt - 4*s35) + (-tt + s35)*(-2*tt*s25 + &
          &2*tt*s35 + 3*s25*s35) + ss*(2*tt*s15 + 3*s15**2 - 2*s15*s35 - 3*s35**2) + s15*(6&
          &*tt**2 + tt*s25 - 13*tt*s35 - s25*s35 + 6*s35**2)))/((tt + s15 - s35)*(s15 + s25&
          & - s35)) + (8*me2**3*(s15 - s35) + ss**2*(-s15**2 + s35**2) - 4*me2**2*(4*s15**2&
          & + tt*s25 + 2*s15*(2*tt + s25 - 4*s35) + ss*(s15 - s35) - 2*tt*s35 - 3*s25*s35 +&
          & 2*s35**2) + (-tt + s35)*(2*tt*s15**2 - s15*(-2*tt + s25)*(tt - 2*s35) + s25**2*&
          &(-tt + s35)) + ss*(2*s25*(tt - s35)*s35 + s15**2*(-3*tt + 2*s35) + s15*(-2*tt**2&
          & + 5*tt*s35 - 2*s35**2)) + 2*me2*(s15**2*(5*tt - 4*s35) + (-tt + s35)*(-2*tt*s25&
          & + 2*tt*s35 + 3*s25*s35) + ss*(2*tt*s15 + 3*s15**2 - 2*s15*s35 - 3*s35**2) + s15&
          &*(6*tt**2 + tt*s25 - 13*tt*s35 - s25*s35 + 6*s35**2)))/(s35*(4*me2 - ss - tt + s&
          &35)) + (-16*me2**3*(s15 - s35) + (-2*tt*s15**2 + s15*(-2*tt + s25)*(tt - 2*s35) &
          &+ s25**2*(tt - s35))*(-tt + s35) + ss**2*(s15**2 - s35**2) + 4*me2**2*(6*s15**2 &
          &+ s15*(5*tt + 4*s25 - 13*s35) + 3*ss*(s15 - s35) + s35*(-3*tt - 4*s25 + 5*s35)) &
          &+ ss*(s15**2*(3*tt - 2*s35) + 2*s25*s35*(-tt + s35) + s15*(2*tt**2 - 5*tt*s35 + &
          &2*s35**2)) - 2*me2*(tt**2*s25 + s15**2*(7*tt - 2*s25 - 4*s35) + ss**2*(s15 - s35&
          &) - 2*tt**2*s35 - 5*tt*s25*s35 + 2*s25**2*s35 + 4*tt*s35**2 + ss*(5*s15**2 + s15&
          &*(3*tt + 2*s25 - 7*s35) - tt*s35 - s25*(tt + s35)) + s15*(6*tt**2 - 2*s25**2 - 1&
          &7*tt*s35 + 6*s35**2 + 3*s25*(tt + s35))))/((4*me2 - ss - tt + s25)*(s15 + s25 - &
          &s35)))*pvd9(8))/s15 + (128*Pi**2*(-((me2*(-2*me2*s15*(tt + s15 - 3*s35) + s15**2&
          &*(s15 - 2*s35) + 4*me2**2*(s15 - s35)))/(s25*(tt + s15 - s35))) - (me2*(-2*me2*s&
          &15*(ss + tt + 2*s15 + 2*s25 - 3*s35) + s15**2*(s15 + 2*s25 - 2*s35) + 4*me2**2*(&
          &2*s15 + s25 - s35)))/(s25*(4*me2 - ss - tt + s35)) + (4*me2**3*(4*s15 + s25 - 3*&
          &s35) - s15**2*(tt*(tt + s15 - 2*s35) + ss*(tt + s15 - s35) + s25*s35) - 2*me2**2&
          &*(2*ss*s15 + 6*tt*s15 + 6*s15**2 + tt*s25 + 2*s15*s25 - ss*s35 - 2*tt*s35 - 9*s1&
          &5*s35 + s25*s35) + me2*s15*(2*tt**2 + 4*s15**2 + tt*s25 + s15*(7*tt + 2*s25 - 6*&
          &s35) + ss*(2*tt + 3*s15 - 3*s35) - 6*tt*s35 + 3*s25*s35))/((4*me2 - ss - tt + s2&
          &5)*(s15 + s25 - s35)) + (-8*me2**3*(s15 - s35) + s15**2*(tt*(tt + s15 - 2*s35) +&
          & ss*(tt + s15 - s35) + s25*s35) + 2*me2**2*(5*tt*s15 + 4*s15**2 + tt*s25 + ss*(s&
          &15 - s35) - 2*tt*s35 - 6*s15*s35 + s25*s35) - me2*s15*(2*tt**2 + 7*tt*s15 + 3*s1&
          &5**2 + tt*s25 + ss*(2*tt + 3*s15 - 3*s35) - 6*tt*s35 - 4*s15*s35 + 3*s25*s35))/(&
          &s35*(4*me2 - ss - tt + s35)) + (8*me2**3*(s15 - s35) - s15**2*(tt*(tt + s15 - 2*&
          &s35) + ss*(tt + s15 - s35) + s25*s35) - 2*me2**2*(5*tt*s15 + 4*s15**2 + tt*s25 +&
          & ss*(s15 - s35) - 2*tt*s35 - 6*s15*s35 + s25*s35) + me2*s15*(2*tt**2 + 7*tt*s15 &
          &+ 3*s15**2 + tt*s25 + ss*(2*tt + 3*s15 - 3*s35) - 6*tt*s35 - 4*s15*s35 + 3*s25*s&
          &35))/((tt + s15 - s35)*(s15 + s25 - s35)) + (-4*me2**3*(s15 - s35) + s15**2*(tt*&
          &(tt + s15 - 2*s35) + ss*(tt + s15 - s35) + s25*s35) + 2*me2**2*(4*tt*s15 + 3*s15&
          &**2 + tt*s25 + ss*(s15 - s35) - 2*tt*s35 - 3*s15*s35 + s25*s35) - me2*s15*(2*tt*&
          &*2 + 7*tt*s15 + 2*s15**2 + tt*s25 + ss*(2*tt + 3*s15 - 3*s35) - 6*tt*s35 - 2*s15&
          &*s35 + 3*s25*s35))/(tt*s35))*pvd9(9))/s15 + (64*Pi**2*((-2*me2*(4*me2**2*(s15 - &
          &s35) + 2*me2*s35*(-tt + s35) + s15*(-tt**2 + s15**2 + 5*tt*s35 - 2*s35**2 - s15*&
          &(2*tt + s35))))/(s25*(tt + s15 - s35)) + (-8*me2**3*(s15 - s35) - 4*me2**2*(2*tt&
          &*s15 + s15**2 + 2*tt*s25 - tt*s35 - 3*s15*s35 - 2*s25*s35 + s35**2 + ss*(-s15 + &
          &s35)) + 2*me2*s15*(tt**2 + 2*tt*s25 + ss*(tt + s15 - 2*s35) - 5*tt*s35 + 2*s35**&
          &2 - s15*(-3*tt + 2*s25 + s35)))/(s25*(4*me2 - ss - tt + s35)) + (16*me2**3*(s15 &
          &- s35) - 4*me2**2*(-(tt*s15) - 2*tt*s25 + ss*(s15 - s35) + tt*s35 + 3*s15*s35 + &
          &2*s25*s35 - 3*s35**2) + 2*me2*(2*s15**3 - ss*s35**2 + s15**2*(-2*tt + 2*s25 + s3&
          &5) + (-tt + s35)*(2*tt*s25 - 2*tt*s35 + s25*s35) + s15*(-(ss*tt) - 5*tt**2 - 3*t&
          &t*s25 + 2*ss*s35 + 10*tt*s35 + s25*s35 - 6*s35**2)) + s15*(-(tt*s15**2) + s15*(t&
          &t**2 + s25*(tt - s35)) - ss*(-2*tt**2 + s15**2 + 3*tt*s35 - 2*s35**2 + s15*(-tt &
          &+ s35)) - (-tt + s35)*(2*tt*(tt - 2*s35) + s25*(tt + 2*s35))))/((4*me2 - ss - tt&
          & + s25)*(s15 + s25 - s35)) + (8*me2**3*(s15 - s35) - 4*me2**2*(tt*s15 + s15**2 -&
          & 2*s35**2) + 2*me2*(tt*s15**2 + 2*s15**3 + (-tt + s35)*(2*tt*s25 - 2*tt*s35 + s2&
          &5*s35) + s15*(-4*tt**2 - tt*s25 + 5*tt*s35 + s25*s35 - 4*s35**2) + ss*(s15**2 - &
          &s35**2)) + s15*(-(tt*s15**2) + s15*(tt**2 + s25*(tt - s35)) - ss*(-2*tt**2 + s15&
          &**2 + 3*tt*s35 - 2*s35**2 + s15*(-tt + s35)) - (-tt + s35)*(2*tt*(tt - 2*s35) + &
          &s25*(tt + 2*s35))))/((tt + s15 - s35)*(s15 + s25 - s35)) + (-8*me2**3*(s15 - s35&
          &) + 4*me2**2*(tt*s15 + s15**2 - 2*s35**2) - 2*me2*(tt*s15**2 + 2*s15**3 + (-tt +&
          & s35)*(2*tt*s25 - 2*tt*s35 + s25*s35) + s15*(-4*tt**2 - tt*s25 + 5*tt*s35 + s25*&
          &s35 - 4*s35**2) + ss*(s15**2 - s35**2)) + s15*(tt*s15**2 + ss*(-2*tt**2 + s15**2&
          & + 3*tt*s35 - 2*s35**2 + s15*(-tt + s35)) + s15*(-tt**2 + s25*(-tt + s35)) + (-t&
          &t + s35)*(2*tt*(tt - 2*s35) + s25*(tt + 2*s35))))/(s35*(4*me2 - ss - tt + s35)) &
          &+ (4*me2**2*(s15 - s35)*(tt + s15 + s35) - 2*me2*(s15**3 + s15**2*(3*tt + s35) +&
          & (-tt + s35)*(2*tt*s25 - 2*tt*s35 + s25*s35) + s15*(-3*tt**2 - tt*s25 + s25*s35 &
          &- 2*s35**2) + ss*(s15**2 - s35**2)) + s15*(tt*s15**2 + ss*(-2*tt**2 + s15**2 + 3&
          &*tt*s35 - 2*s35**2 + s15*(-tt + s35)) + s15*(-tt**2 + s25*(-tt + s35)) + (-tt + &
          &s35)*(2*tt*(tt - 2*s35) + s25*(tt + 2*s35))))/(tt*s35))*pvd9(10))/s15 + (64*Pi**&
          &2*((-2*me2*(-(tt*s15**2) + tt**2*s25 + s15**2*s25 + 2*me2*(tt + s15 - 2*s35)*(s1&
          &5 - s35) - ss*(tt + s15 - 2*s35)*(s15 - s35) + 2*tt*s15*s35 - 2*tt*s25*s35 - 2*s&
          &15*s25*s35 - tt*s35**2 + 2*s25*s35**2))/(s25*(4*me2 - ss - tt + s35)) - (2*me2*(&
          &2*me2*(tt + s15 - 2*s35)*(s15 - s35) - tt*s35**2 + s15**2*(-2*tt + s35) - s15*(t&
          &t**2 - 5*tt*s35 + 2*s35**2)))/(s25*(tt + s15 - s35)) + (4*me2**2*(tt + s15 - 2*s&
          &35)*(s15 - s35) + 2*me2*(-tt + s35)*(3*tt*s15 + 2*s15**2 - 2*tt*s35 - 3*s15*s35)&
          & - (-tt + s35)*(tt*s15**2 + ss*(tt + s15)*(s15 - s35) + tt*s25*(-tt + s35) + s15&
          &*(tt**2 - tt*s25 - 2*tt*s35 + s25*s35)))/((tt + s15 - s35)*(s15 + s25 - s35)) + &
          &(-4*me2**2*(tt + s15 - 2*s35)*(s15 - s35) + 2*me2*(-tt + s35)*(-2*s15**2 + 2*tt*&
          &s35 + 3*s15*(-tt + s35)) + (-tt + s35)*(tt*s15**2 + ss*(tt + s15)*(s15 - s35) + &
          &tt*s25*(-tt + s35) + s15*(tt**2 - tt*s25 - 2*tt*s35 + s25*s35)))/(s35*(4*me2 - s&
          &s - tt + s35)) + ((-tt + s35)*(tt*s15**2 + ss*(tt + s15)*(s15 - s35) + tt*s25*(-&
          &tt + s35) + s15*(tt**2 - tt*s25 - 2*tt*s35 + s25*s35)) - 2*me2*(s15**2*s35 - tt*&
          &s35*(-2*tt + s35) - s15*(2*tt**2 - tt*s35 + s35**2)))/(tt*s35) + (8*me2**2*(tt +&
          & s15 - 2*s35)*(s15 - s35) - (-tt + s35)*(tt*s15**2 + ss*(tt + s15)*(s15 - s35) +&
          & tt*s25*(-tt + s35) + s15*(tt**2 - tt*s25 - 2*tt*s35 + s25*s35)) - 2*me2*(-(tt**&
          &2*s25) + ss*(tt + s15 - 2*s35)*(s15 - s35) - 2*tt**2*s35 + 2*tt*s25*s35 + 3*tt*s&
          &35**2 - 2*s25*s35**2 - s15**2*(-3*tt + s25 + 2*s35) + s15*(3*tt**2 - 8*tt*s35 + &
          &2*s25*s35 + 3*s35**2)))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)))*pvd9(11))/s&
          &15)/(16.*Pi**2)

          !print*,"7", diags

  end if

  if (ct) then

  diags =   diags -(20*((-4*tt*s15*(24*me2**3 + s15*s25*(-ss + s15 + s25) + 4*me2**2*(-4*ss&
          & + s15 + 4*s25) + 2*me2*(ss**2 - 2*ss*s25 + s25*(2*s15 + s25))))/(4*me2 - ss - t&
          &t + s25) + 4*s15*(-16*me2**3 + 8*me2**2*(2*ss - s15 - 2*s25) - 2*me2*(2*ss**2 + &
          &tt**2 - 2*tt*s15 + s15**2 - 2*tt*s25 + 4*s15*s25 + 2*s25**2 - 2*ss*(-tt + s15 + &
          &2*s25)) + s15*(tt*s15 + tt*s25 - 2*s15*s25 - 2*s25**2 + ss*(s15 + 2*s25 - s35) -&
          & tt*s35 + s25*s35)) - (2*me2*tt*s15*(48*me2**3 - tt**2*s15 + tt*s15**2 - ss**2*(&
          &2*tt + s15) + tt*s15*s25 - tt*s25**2 - 4*me2**2*(8*ss + 6*tt - 6*s25 - 3*s35) + &
          &tt*s15*s35 - s15**2*s35 - 2*s15*s25*s35 + s25**2*s35 + ss*(-2*tt*s15 + 2*tt*s25 &
          &+ s15*s25 + 3*s15*s35 - s25*s35) + me2*(4*ss**2 + 2*tt*s15 - 2*s15**2 - 6*tt*s25&
          & - 6*s15*s25 + 2*ss*(8*tt + 3*s15 - 2*s25 - 3*s35) - 2*tt*s35 - 5*s15*s35 + 8*s2&
          &5*s35)))/(s35*(4*me2 - ss - tt + s35)) + (4*tt*s15**2*(24*me2**3 - (ss - s15)*(s&
          &s - s25)*(ss - s15 - s25) + me2**2*(-28*ss + 18*s15 + 22*s25 + 4*s35) + me2*(10*&
          &ss**2 + 5*s15**2 - tt*s25 + 5*s25**2 + s15*(tt + 10*s25 - 2*s35) - s25*s35 + s35&
          &**2 + ss*(-14*s15 - 15*s25 + s35))))/(s25*(4*me2 - ss - tt + s35)) - (2*me2*s15*&
          &(32*me2**3 - 2*tt**3 + tt**2*s15 + tt*s15**2 - 2*ss**2*(2*tt + s15) + 2*tt**2*s2&
          &5 - tt*s15*s25 - 2*tt*s25**2 - 8*me2**2*(4*ss + 2*tt - 2*s25 - s35) + 2*tt**2*s3&
          &5 - 2*tt*s15*s35 - tt*s25*s35 + s15*s25*s35 + 2*s25**2*s35 - s25*s35**2 + ss*(-4&
          &*tt**2 + s15**2 + 4*tt*s25 + 2*s15*s25 + 2*tt*s35 - 2*s25*s35 + s35**2) + 2*me2*&
          &(4*ss**2 + 2*tt**2 - s15**2 - 6*tt*s25 + 2*ss*(6*tt + s15 - 2*s25 - s35) + 4*s25&
          &*s35 - s35**2 - s15*(4*tt + 2*s25 + s35))))/s35 - (2*s15**2*(-32*me2**3 + 4*ss**&
          &2*tt + 2*tt**3 + tt*s15**2 - 2*tt**2*s25 + 3*tt*s15*s25 + 2*tt*s25**2 + 8*me2**2&
          &*(4*ss + 2*tt - s15 - 2*s25 - s35) - 2*tt**2*s35 + tt*s15*s35 + tt*s25*s35 - 3*s&
          &15*s25*s35 - 2*s25**2*s35 + s25*s35**2 + ss*(4*tt**2 - 2*tt*s15 + s15**2 - 4*tt*&
          &s25 + 2*s15*s25 - 2*tt*s35 + 2*s25*s35 - s35**2) - 2*me2*(4*ss**2 + 2*tt**2 - 6*&
          &tt*s15 + s15**2 - 6*tt*s25 + 2*s15*s25 + 4*s25*s35 - s35**2 - 2*ss*(-6*tt + s15 &
          &+ 2*s25 + s35))))/s35 + 2*me2*(32*me2**3 + 8*me2**2*(-4*ss + s15 + 4*s25) + 2*me&
          &2*(4*ss**2 + 4*ss*tt + 2*tt**2 - 4*tt*s15 - 3*s15**2 - 8*ss*s25 - 4*tt*s25 + 2*s&
          &15*s25 + 4*s25**2) + s15*(-2*ss**2 + s25*(tt - s35) - tt*(tt + s15 - s35) + ss*(&
          &-2*tt + s15 + 2*s25 + s35))) - (2*me2*tt*s15*(32*me2**3 - 4*ss**3 + tt**2*s15 - &
          &2*tt*s15**2 - 8*me2**2*(6*ss - 4*s15 - 5*s25) + 2*tt**2*s25 - tt*s15*s25 + s15**&
          &2*s25 - tt*s25**2 + s15*s25**2 - 3*tt*s25*s35 + s25**2*s35 + s25*s35**2 + 2*ss**&
          &2*(-2*tt + 3*s15 + 4*s25 + s35) - ss*(2*tt**2 - 4*tt*s15 + 3*s15**2 - 6*tt*s25 +&
          & 7*s15*s25 + 4*s25**2 - 2*tt*s35 + 3*s25*s35 + s35**2) + 2*me2*(12*ss**2 + 2*tt*&
          &*2 - 2*tt*s15 + 3*s15**2 - 4*tt*s25 + 10*s15*s25 + 6*s25**2 - 2*tt*s35 + s25*s35&
          & + s35**2 - 2*ss*(-2*tt + 7*s15 + 9*s25 + s35))))/(s25*(tt + s15 - s35)) + (2*tt&
          &*s15**2*(32*me2**3 - 4*ss**3 + 2*tt**2*s15 + 2*tt**2*s25 - tt*s15*s25 + 2*s15**2&
          &*s25 - tt*s25**2 + 2*s15*s25**2 - 8*me2**2*(6*ss - 5*(s15 + s25)) - tt*s15*s35 -&
          & 3*tt*s25*s35 + s25**2*s35 + s25*s35**2 + 2*ss**2*(-2*tt + 3*s15 + 4*s25 + s35) &
          &- ss*(2*tt**2 + 2*s15**2 + 4*s25**2 - 2*tt*s35 + s35**2 + 3*s25*(-2*tt + s35) + &
          &s15*(-4*tt + 7*s25 + s35)) + 2*me2*(12*ss**2 + 2*tt**2 + 4*s15**2 - 4*tt*s25 + 6&
          &*s25**2 - 2*tt*s35 + s25*s35 + s35**2 - 2*ss*(-2*tt + 8*s15 + 9*s25 + s35) + s15&
          &*(-2*tt + 11*s25 + s35))))/(s25*(tt + s15 - s35)) + (2*me2*tt*s15*(32*me2**3 - 4&
          &*ss**3 - 2*tt**3 + tt**2*s15 + 2*tt*s15**2 + 4*tt**2*s25 - s15**2*s25 - 3*tt*s25&
          &**2 - s15*s25**2 - 8*me2**2*(6*ss + 2*tt - s15 - 3*s25 - s35) + 2*tt**2*s35 - 2*&
          &tt*s15*s35 - 4*tt*s25*s35 + s15*s25*s35 + 3*s25**2*s35 + 2*ss**2*(-4*tt + s15 + &
          &4*s25 + s35) - ss*(6*tt**2 + 4*s25**2 + s15*(-2*tt + s25) - 4*tt*s35 + 5*s25*(-2&
          &*tt + s35)) + 2*me2*(12*ss**2 + 2*tt**2 + s15**2 - 6*tt*s25 + 2*s25**2 - 2*tt*s3&
          &5 + 5*s25*s35 - s15*(2*tt + s35) - 2*ss*(-6*tt + 2*s15 + 7*s25 + 2*s35))))/((tt &
          &+ s15 - s35)*(s15 + s25 - s35)) + (2*tt*s15**2*(-32*me2**3 + 4*ss**3 + 2*tt**3 -&
          & tt*s15**2 - 4*tt**2*s25 + 2*s15**2*s25 + 3*tt*s25**2 + 2*s15*s25**2 + 8*me2**2*&
          &(6*ss + 2*tt - 2*s15 - 3*s25 - s35) - 2*tt**2*s35 + 2*tt*s15*s35 + 4*tt*s25*s35 &
          &- 3*s15*s25*s35 - 3*s25**2*s35 - 2*ss**2*(-4*tt + s15 + 4*s25 + s35) + ss*(6*tt*&
          &*2 - s15**2 + 4*s25**2 - 4*tt*s35 + 5*s25*(-2*tt + s35) + s15*(-2*tt + s25 + s35&
          &)) - 2*me2*(12*ss**2 + 2*tt**2 - s15**2 - 6*tt*s25 + 2*s25**2 - 2*tt*s35 + 5*s25&
          &*s35 + s15*(-4*tt + s25 + s35) - 2*ss*(-6*tt + 3*s15 + 7*s25 + 2*s35))))/((tt + &
          &s15 - s35)*(s15 + s25 - s35)) + (2*me2*tt*s15*(48*me2**3 - 2*ss**3 - tt**2*s15 -&
          & tt*s15**2 + s15**2*s25 - tt*s25**2 + 2*ss**2*(-tt + s15 + 2*s25) - 4*me2**2*(14&
          &*ss + 6*tt - s15 - 9*s25 - 5*s35) + 2*tt*s15*s35 + s15**2*s35 - s15*s25*s35 + s2&
          &5**2*s35 - s15*s35**2 - ss*(2*s15**2 + 2*s15*(s25 - s35) + s25*(-2*tt + 2*s25 + &
          &s35)) + me2*(20*ss**2 + 4*tt*s15 + 11*s15**2 - 8*tt*s25 + 5*s15*s25 + 6*s25**2 -&
          & 2*tt*s35 - 15*s15*s35 + 6*s25*s35 + 2*s35**2 - 2*ss*(-8*tt + 6*s15 + 13*s25 + 2&
          &*s35))))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + (2*tt*s15**2*(-48*me2**3 &
          &+ 2*ss**3 - 2*ss**2*(-tt + s15 + 2*s25) + (s15 + s25)*(tt*s15 + s25*(tt - s35)) &
          &+ 4*me2**2*(14*ss + 6*tt - 4*s15 - 9*s25 - 5*s35) + ss*(s15**2 + s15*(-2*tt + 3*&
          &s25 - s35) + s25*(-2*tt + 2*s25 + s35)) - 2*me2*(10*ss**2 + 3*s15**2 - 4*tt*s25 &
          &+ 5*s15*s25 + 3*s25**2 - tt*s35 + 3*s25*s35 + s35**2 - 3*s15*(tt + s35) - ss*(-8&
          &*tt + 7*s15 + 13*s25 + 2*s35))))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) - (&
          &2*tt*s15**2*(-48*me2**3 + 2*ss**2*tt + (s15 + s25)*(tt*s15 + s25*(tt - s35)) + s&
          &s*(s15**2 + s15*(-2*tt + s25 - s35) + s25*(-2*tt + s35)) + 4*me2**2*(8*ss - 3*(-&
          &2*tt + s15 + 2*s25 + s35)) + 2*me2*(-2*ss**2 - 2*s15**2 + 3*tt*s25 + tt*s35 - 4*&
          &s25*s35 + s15*(4*tt - 3*s25 + s35) + ss*(-8*tt + s15 + 2*s25 + 3*s35))))/(s35*(4&
          &*me2 - ss - tt + s35)) + (me2*tt*(96*me2**3 - 8*me2**2*(8*ss + s15 - 8*s25) + 2*&
          &me2*(4*ss**2 - 9*s15**2 + 8*ss*(s15 - s25) + 4*s25**2 - 2*s15*(3*s25 + s35)) + s&
          &15*(-2*ss**2 + s15*(tt - 2*s35) + 3*s25*(tt - s35) + ss*(3*s15 + 2*s25 + 3*s35))&
          &))/(4*me2 - ss - tt + s25) - (2*me2*tt*s15*(48*me2**3 - 2*ss**3 + 4*ss**2*(s15 +&
          & s25) + me2**2*(-56*ss + 24*s15 + 44*s25 + 8*s35) + s15*(-s15**2 + s25**2 + (tt &
          &- s35)*s35 + 2*s15*(-tt + s35) + s25*(-tt + s35)) - ss*(s15**2 + 2*s25**2 + s15*&
          &(-2*tt + 5*s25 + s35)) + me2*(20*ss**2 + 10*s15**2 + s15*(2*tt + 19*s25 - 10*s35&
          &) + ss*(-26*s15 - 30*s25 + 2*s35) + 2*(-(tt*s25) + 5*s25**2 - s25*s35 + s35**2))&
          &))/(s25*(4*me2 - ss - tt + s35))))/(tt**2*s15**3)

          !print*,"8",diags
  end if

  END FUNCTION DIAGS

  FUNCTION PENTA(ss,tt,s15,s25,s35,m)
  real(kind=prec) :: ss,tt,s25,s35,s15,m, penta
  me2 = m**2

  !call SetMuIR2_cll(me2)
  !call SetMuUV2_cll(me2)

  !call pv_evaluate(ss,tt,s25,s35,s15,m)

  penta = 0.

  penta = penta + (128*Pi**2*((-2*me2 + ss + 2*tt - s25)/(tt*s15) + (-6*me2 + ss + 2*tt - &
          &s25)/(s15*(4*me2 - ss - tt + s25)) + (-2*me2 + ss + 4*tt + 3*s15 - 4*s35)/(s25*(&
          &tt + s15 - s35)) + (2*me2 - ss - 2*tt + s35)/(tt*s35) + (-10*me2 + ss + 4*tt + 3&
          &*s15 - 4*s35)/(s25*(4*me2 - ss - tt + s35)) + (6*me2 - ss - 2*tt + s35)/(s35*(4*&
          &me2 - ss - tt + s35)) + (2*me2 - ss - 4*tt - 3*s15 + s25 + 3*s35)/((tt + s15 - s&
          &35)*(s15 + s25 - s35)) + (10*me2 - ss - 4*tt - 3*s15 + s25 + 3*s35)/((4*me2 - ss&
          & - tt + s25)*(s15 + s25 - s35)))*pvb1(1) + 128*Pi**2*(1/tt - (2*me2 - ss - 2*tt &
          &+ s15 + s25)/(tt*s15) - (6*me2 - ss - 2*tt + s15 + s25)/(s15*(4*me2 - ss - tt + &
          &s25)) + (2*tt + 2*s15 + s25 - 2*s35)/(s25*(tt + s15 - s35)) + (2*me2 - ss)/((4*m&
          &e2 - ss - tt + s25)*(s15 + s25 - s35)) + (2*me2 - ss)/((tt + s15 - s35)*(s15 + s&
          &25 - s35)) - 2/s35 + (-4*me2 + 2*tt + 2*s15 + s25 - 2*s35)/(s25*(4*me2 - ss - tt&
          & + s35)) + (4*me2 - 2*tt + s35)/(s35*(4*me2 - ss - tt + s35)))*pvb10(1) + 128*Pi&
          &**2*(-((4*me2 - 2*ss - 2*tt + s15 + 2*s25)/(tt*s15)) - (8*me2 - 2*ss - 2*tt + s1&
          &5 + 2*s25)/(s15*(4*me2 - ss - tt + s25)) + (-2*me2 + ss + 3*tt + 2*s15 + s25 - 3&
          &*s35)/(s25*(tt + s15 - s35)) + (-8*me2 + ss + 3*tt + 2*s15 + s25 - 3*s35)/(s25*(&
          &4*me2 - ss - tt + s35)) + (4*me2 - 2*ss - 3*tt + 2*s35)/(tt*s35) + (10*me2 - 2*s&
          &s - 3*tt + 2*s35)/(s35*(4*me2 - ss - tt + s35)))*pvb11(1) + 128*Pi**2*(-((tt + s&
          &15)/(tt*s15)) - (-2*me2 + tt + s15)/(s15*(4*me2 - ss - tt + s25)) + (2*me2 - ss &
          &- 2*tt + s15 + s25)/(tt*s15) + (6*me2 - ss - 2*tt + s15 + s25)/(s15*(4*me2 - ss &
          &- tt + s25)) + (2*(-6*me2 + ss + 2*tt + s15 - s25 - s35))/((4*me2 - ss - tt + s2&
          &5)*(s15 + s25 - s35)) + (2*(-2*me2 + ss + 2*tt + s15 - s25 - s35))/((tt + s15 - &
          &s35)*(s15 + s25 - s35)) + 1/s35 + (-2*me2 + tt)/(s35*(4*me2 - ss - tt + s35)) + &
          &(2*me2 - ss - tt + s25 + s35)/(s25*(tt + s15 - s35)) + (4*me2 - ss - tt + s25 + &
          &s35)/(s25*(4*me2 - ss - tt + s35)) - (4*me2 - 2*ss - tt + s15 + s25 + s35)/(s25*&
          &(tt + s15 - s35)) - (6*me2 - 2*ss - tt + s15 + s25 + s35)/(s25*(4*me2 - ss - tt &
          &+ s35)))*pvb2(1) + 128*Pi**2*(-(1/s15) + (2*me2 - tt)/(s15*(4*me2 - ss - tt + s2&
          &5)) - (-6*me2 + 3*ss + 4*tt + s15 - 4*s35)/(s25*(tt + s15 - s35)) + (-2*me2 + ss&
          &)/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + (-2*me2 + ss)/((tt + s15 - s35)*&
          &(s15 + s25 - s35)) + 1/s35 + (-2*me2 + ss + 2*tt - 2*s35)/(tt*s35) - (-14*me2 + &
          &3*ss + 4*tt + s15 - 4*s35)/(s25*(4*me2 - ss - tt + s35)) + (-2*me2 + tt)/(s35*(4&
          &*me2 - ss - tt + s35)) + (-6*me2 + ss + 2*tt - 2*s35)/(s35*(4*me2 - ss - tt + s3&
          &5)))*pvb3(1) + 128*Pi**2*((2*me2 - ss - 2*tt + s25)/(tt*s15) + (6*me2 - ss - 2*t&
          &t + s25)/(s15*(4*me2 - ss - tt + s25)) + (-2*me2 + ss - s15)/(s25*(tt + s15 - s3&
          &5)) + (3*tt + 2*s15 - s25 - 2*s35)/((tt + s15 - s35)*(s15 + s25 - s35)) + (-2*me&
          &2 + ss + tt)/(tt*s35) + (-2*me2 + ss - s15)/(s25*(4*me2 - ss - tt + s35)) + (-4*&
          &me2 + ss + tt)/(s35*(4*me2 - ss - tt + s35)) - (6*me2 - 3*tt - 2*s15 + s25 + 2*s&
          &35)/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)))*pvb5(1) + 128*Pi**2*((4*me2 - 2&
          &*ss - 2*tt + s15 + 2*s25)/(tt*s15) + (8*me2 - 2*ss - 2*tt + s15 + 2*s25)/(s15*(4&
          &*me2 - ss - tt + s25)) - (2*tt + 2*s15 + s25 - 2*s35)/(s25*(tt + s15 - s35)) + (&
          &-2*me2 + ss + 2*tt - s35)/(tt*s35) - (-4*me2 + 2*tt + 2*s15 + s25 - 2*s35)/(s25*&
          &(4*me2 - ss - tt + s35)) + (-6*me2 + ss + 2*tt - s35)/(s35*(4*me2 - ss - tt + s3&
          &5)))*pvb8(1) + 128*Pi**2*(-(1/tt) + (2*me2 - ss - tt + s15 + s25)/(tt*s15) + (4*&
          &me2 - ss - tt + s15 + s25)/(s15*(4*me2 - ss - tt + s25)) - (3*tt + 3*s15 + s25 -&
          & 3*s35)/(s25*(tt + s15 - s35)) + (-4*me2 + ss + tt + s15 - s35)/((4*me2 - ss - t&
          &t + s25)*(s15 + s25 - s35)) + (-2*me2 + ss + tt + s15 - s35)/((tt + s15 - s35)*(&
          &s15 + s25 - s35)) + 1/s35 - (-6*me2 + 3*tt + 3*s15 + s25 - 3*s35)/(s25*(4*me2 - &
          &ss - tt + s35)) + (-2*me2 + tt - s35)/(s35*(4*me2 - ss - tt + s35)))*pvb9(1) + 1&
          &28*Pi**2*((-14*me2**2 - 2*ss**2 - tt**2 + me2*(12*ss + 9*tt - 5*s15 - 10*s25) + &
          &2*tt*s25 - 2*s15*s25 - 2*s25**2 + ss*(-3*tt + s15 + 4*s25 - s35) + 2*s15*s35 + s&
          &25*s35)/(tt*s15) + (-28*me2**2 - 2*ss**2 - tt**2 + 2*tt*s25 - 2*s15*s25 - 2*s25*&
          &*2 + me2*(14*ss + 12*tt - s15 - 9*s25 - s35) + ss*(-3*tt + s15 + 4*s25 - s35) + &
          &2*s15*s35 + s25*s35)/(s15*(4*me2 - ss - tt + s25)) + (48*me2**2 + 4*ss**2 + tt**&
          &2 - tt*s15 - s15**2 - 3*tt*s25 + s15*s25 + 2*s25**2 - 2*me2*(13*ss + 9*tt + s15 &
          &- 7*s25 - 6*s35) + ss*(5*tt - 5*s25 - 3*s35) - tt*s35 + 2*s25*s35)/(s25*(4*me2 -&
          & ss - tt + s35)) + (26*me2**2 + 4*ss**2 + tt**2 - tt*s15 - s15**2 - 3*tt*s25 + s&
          &15*s25 + 2*s25**2 - 2*me2*(10*ss + 7*tt - s15 - 5*s25 - 5*s35) + ss*(5*tt - 5*s2&
          &5 - 3*s35) - tt*s35 + 2*s25*s35)/(s25*(tt + s15 - s35)) + (-2*me2**2 - (ss - tt &
          &+ s25)*(ss + tt - s35) + me2*(2*ss - 5*tt + s15 + 5*s25 + s35))/((tt + s15 - s35&
          &)*(s15 + s25 - s35)) + (4*me2**2 - (ss - tt + s25)*(ss + tt - s35) + me2*(4*ss -&
          & 6*tt + s15 + 4*s25 + 2*s35))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + (6*m&
          &e2**2 + ss**2 + (tt - s15 - 2*s25)*s35 + ss*(tt - s15 + s35) - me2*(6*ss - 2*s15&
          & + s25 + 3*s35))/(tt*s35) + (8*me2**2 + ss**2 + (tt - s15 - 2*s25)*s35 + ss*(tt &
          &- s15 + s35) - me2*(6*ss + 2*tt - 2*s15 + s25 + 5*s35))/(s35*(4*me2 - ss - tt + &
          &s35)))*pvc12(1) + 128*Pi**2*((-2*me2 + ss + tt - s15 - s25)/s15 - (tt*(4*me2 - s&
          &s - tt + s15 + s25))/(s15*(4*me2 - ss - tt + s25)) - (2*me2 - ss + s35)**2/(tt*s&
          &35) - (2*me2 - ss + s35)**2/(s35*(4*me2 - ss - tt + s35)) + (4*me2**2 + ss**2 - &
          &tt**2 + tt*s15 + tt*s25 + s35**2 - ss*(tt + 2*s35) + me2*(-4*ss + 2*tt + 4*s35))&
          &/(s25*(tt + s15 - s35)) + (8*me2**2 + ss**2 - tt**2 + tt*s15 + tt*s25 + s35**2 -&
          & ss*(tt + 2*s35) + me2*(-6*ss + 4*tt + 6*s35))/(s25*(4*me2 - ss - tt + s35)))*pv&
          &c12(2) + 128*Pi**2*(((-tt + s15)*(2*me2 - ss - tt + s15 + s25))/(tt*s15) + ((-tt&
          & + s15)*(4*me2 - ss - tt + s15 + s25))/(s15*(4*me2 - ss - tt + s25)) + ((2*me2 -&
          & tt)*(2*me2 - ss + s35))/(tt*s35) + ((2*me2 - tt)*(2*me2 - ss + s35))/(s35*(4*me&
          &2 - ss - tt + s35)) - (8*me2**2 + 3*ss*tt + 2*tt**2 - 2*ss*s15 - 2*tt*s15 + s15*&
          &*2 - tt*s25 + s15*s25 - 2*me2*(ss + 6*tt - 3*s15 - s35) - 2*tt*s35 + s15*s35)/(s&
          &25*(4*me2 - ss - tt + s35)) - (4*me2**2 + 3*ss*tt + 2*tt**2 - 2*ss*s15 - 2*tt*s1&
          &5 + s15**2 - tt*s25 + s15*s25 - 2*me2*(ss + 4*tt - 2*s15 - s35) - 2*tt*s35 + s15&
          &*s35)/(s25*(tt + s15 - s35)))*pvc12(3) + 128*Pi**2*((-2*me2 + ss + tt - s15 - s2&
          &5)/(tt*s15) + (-4*me2 + ss + tt - s15 - s25)/(s15*(4*me2 - ss - tt + s25)) + (4*&
          &me2 - 2*ss - tt + s15 + s25 + s35)/(s25*(tt + s15 - s35)) + (6*me2 - 2*ss - tt +&
          & s15 + s25 + s35)/(s25*(4*me2 - ss - tt + s35)))*(-0.5 + 4*pvc12(4)) + 128*me2*P&
          &i**2*((-2*me2 + ss + tt - s15 - s25)/(tt*s15) + (-4*me2 + ss + tt - s15 - s25)/(&
          &s15*(4*me2 - ss - tt + s25)) + (4*me2 - 2*ss - tt + s15 + s25 + s35)/(s25*(tt + &
          &s15 - s35)) + (6*me2 - 2*ss - tt + s15 + s25 + s35)/(s25*(4*me2 - ss - tt + s35)&
          &))*pvc12(5) + 128*Pi**2*(-2*me2 + ss + tt - s15 - s25)*((-2*me2 + ss + tt - s15 &
          &- s25)/(tt*s15) + (-4*me2 + ss + tt - s15 - s25)/(s15*(4*me2 - ss - tt + s25)) +&
          & (4*me2 - 2*ss - tt + s15 + s25 + s35)/(s25*(tt + s15 - s35)) + (6*me2 - 2*ss - &
          &tt + s15 + s25 + s35)/(s25*(4*me2 - ss - tt + s35)))*pvc12(6) + 128*Pi**2*(me2 -&
          & s15)*((-2*me2 + ss + tt - s15 - s25)/(tt*s15) + (-4*me2 + ss + tt - s15 - s25)/&
          &(s15*(4*me2 - ss - tt + s25)) + (4*me2 - 2*ss - tt + s15 + s25 + s35)/(s25*(tt +&
          & s15 - s35)) + (6*me2 - 2*ss - tt + s15 + s25 + s35)/(s25*(4*me2 - ss - tt + s35&
          &)))*pvc12(7) + 128*Pi**2*((8*me2**2 + 2*ss**2 + tt**2 - tt*s15 + ss*(tt - 3*s15 &
          &- 2*s25) + s15*s25 + me2*(-8*ss + tt + 7*s15 + 4*s25 - 3*s35) - tt*s35 + s15*s35&
          &)/(tt*s15) + (6*me2**2 + ss**2 - tt*s15 + tt*s25 + s15*s25 + me2*(-5*ss + tt + 9&
          &*s15 + 2*s25 - 2*s35) - ss*(tt + 3*s15 + s25 - s35) + s15*s35 - s25*s35)/(s15*(4&
          &*me2 - ss - tt + s25)) + (-2*me2**2 - ss**2 - (-tt + s15 + s25 - s35)*(-tt + s35&
          &) + ss*(-2*tt + s15 + s35) + me2*(5*ss + 4*tt - 4*s15 - 2*s25 + s35))/(s35*(4*me&
          &2 - ss - tt + s35)) + (22*me2**2 + 2*ss**2 + 2*tt**2 - me2*(13*ss + 14*tt + s15 &
          &- 10*s25) - 3*tt*s25 - s15*s25 + s15*s35 + 3*s25*s35 - 2*s35**2 - ss*(-4*tt + 2*&
          &s25 + s35))/(s25*(4*me2 - ss - tt + s35)) + (-8*me2**2 - 2*ss**2 + me2*(8*ss + 7&
          &*tt - 3*s15 - s35) - (-2*tt + s15 + s25 - s35)*(-tt + s35) + ss*(-4*tt + s15 + 2&
          &*s35))/(tt*s35) + (8*me2**2 + ss**2 + ss*(3*tt + s15 - 3*s35) + (-tt + s25)*(-tt&
          & + s35) + me2*(-6*ss - 5*tt + 2*s25 + 3*s35))/((tt + s15 - s35)*(s15 + s25 - s35&
          &)) + (2*me2**2 + ss*(tt + s15 + s25 - 2*s35) + me2*(-3*ss + tt - 2*s25 + 4*s35))&
          &/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + (16*me2**2 + 3*ss**2 + 3*tt**2 - &
          &3*tt*s25 - s15*s25 - tt*s35 + s15*s35 + 3*s25*s35 - 2*s35**2 - 2*ss*(-3*tt + s25&
          & + s35) + me2*(-14*ss - 13*tt + 2*s15 + 4*s25 + 5*s35))/(s25*(tt + s15 - s35)))*&
          &pvc13(1) + 128*Pi**2*((-6*me2 + ss + 2*tt - 2*s35)/s25 + (2*me2 - tt + s35)/s35 &
          &+ ((2*me2 - ss)*(4*me2 - ss - tt + s35))/((4*me2 - ss - tt + s25)*(s15 + s25 - s&
          &35)) + ((2*me2 - ss)*(4*me2 - ss - tt + s35))/((tt + s15 - s35)*(s15 + s25 - s35&
          &)) + ((-tt + s35)*(4*me2 - ss - tt + s35))/(tt*s35) - (8*me2**2 + ss**2 - 2*me2*&
          &(3*ss + 5*tt + s15 - 5*s35) - 3*ss*(-tt + s35) + 2*(-tt + s35)**2)/(s25*(tt + s1&
          &5 - s35)))*pvc13(2) + 128*Pi**2*(((2*me2 - ss)*(4*me2 - ss - tt + 2*s35))/((4*me&
          &2 - ss - tt + s25)*(s15 + s25 - s35)) + ((2*me2 - ss)*(4*me2 - ss - tt + 2*s35))&
          &/((tt + s15 - s35)*(s15 + s25 - s35)) + ((-tt + s35)*(4*me2 - ss - tt + 2*s35))/&
          &(tt*s35) + ((2*me2 - tt + s35)*(4*me2 - ss - tt + 2*s35))/(s35*(4*me2 - ss - tt &
          &+ s35)) - (24*me2**2 + ss**2 + 2*tt**2 + tt*s15 - 2*me2*(5*ss + 7*tt + 2*s15 - 1&
          &2*s35) + ss*(3*tt + s15 - 5*s35) - 7*tt*s35 - 2*s15*s35 + 5*s35**2)/(s25*(4*me2 &
          &- ss - tt + s35)) - (8*me2**2 + ss**2 + 2*tt**2 + tt*s15 - 2*me2*(3*ss + 5*tt + &
          &2*s15 - 7*s35) + ss*(3*tt + s15 - 5*s35) - 7*tt*s35 - 2*s15*s35 + 5*s35**2)/(s25&
          &*(tt + s15 - s35)))*pvc13(3) + 128*Pi**2*((-12*me2**2 + me2*(2*ss + 4*tt + 3*s15&
          & - 6*s35) - s35*(-2*tt - s15 + s25 + s35))/(s35*(4*me2 - ss - tt + s35)) + (-4*m&
          &e2**2 + me2*(2*ss + 4*tt + 3*s15 - 3*s25 + s35) - s35*(-2*tt - s15 + s25 + s35))&
          &/(tt*s35) - (-44*me2**2 - 6*ss**2 - 5*tt**2 + 2*tt*s15 + s15**2 + 8*tt*s25 - 2*s&
          &25**2 + 2*tt*s35 - 2*s15*s35 - 4*s25*s35 + s35**2 + me2*(34*ss + 24*tt - 8*s15 -&
          & 24*s25 + s35) + 2*ss*(-5*tt + s15 + 4*s25 + s35))/((tt + s15 - s35)*(s15 + s25 &
          &- s35)) + (-16*me2**2 - 2*ss**2 - 2*tt**2 + 4*tt*s25 + 2*s15*s25 + me2*(12*ss + &
          &10*tt - 3*s15 - 8*s25 - 6*s35) - 3*s15*s35 - 4*s25*s35 + 2*s35**2 + ss*(-4*tt + &
          &2*s25 + 3*s35))/(s25*(tt + s15 - s35)) + (-28*me2**2 - 2*ss**2 - 2*tt**2 + 4*tt*&
          &s25 + 2*s15*s25 + me2*(14*ss + 16*tt - s15 - 11*s25 - 5*s35) - 3*s15*s35 - 4*s25&
          &*s35 + 2*s35**2 + ss*(-4*tt + 2*s25 + 3*s35))/(s25*(4*me2 - ss - tt + s35)) + (-&
          &24*me2**2 - 4*ss**2 - 3*tt**2 + 2*tt*s15 + s15**2 + 4*tt*s25 - 2*s15*s25 - 2*s25&
          &**2 + ss*(-6*tt + 2*s15 + 6*s25 - s35) + s25*s35 + me2*(20*ss + 10*tt - 8*s15 - &
          &13*s25 + 6*s35))/(tt*s15) + (68*me2**2 + 4*ss**2 + 3*tt**2 - 2*tt*s15 - s15**2 -&
          & 5*tt*s25 + s25**2 - tt*s35 + 2*s15*s35 + 3*s25*s35 - s35**2 - ss*(-6*tt + 2*s15&
          & + 5*s25 + s35) + me2*(-32*ss - 28*tt + 6*s15 + 22*s25 + 6*s35))/((4*me2 - ss - &
          &tt + s25)*(s15 + s25 - s35)) + (-28*me2**2 - 2*ss**2 - tt**2 + 2*tt*s15 + s15**2&
          & + tt*s25 - 2*s15*s25 - s25**2 - tt*s35 + 2*s25*s35 + me2*(16*ss + 8*tt - 8*s15 &
          &- 11*s25 + 5*s35) + ss*(2*s15 + 3*s25 - 2*(tt + s35)))/(s15*(4*me2 - ss - tt + s&
          &25)))*pvc18(1) + 128*Pi**2*(-((12*me2**2 + ss**2 + tt*s15 + tt*s25 - 2*me2*(4*ss&
          & + 2*tt + 3*s15 - 6*s35) + ss*(2*tt + 2*s15 - s25 - 2*s35) - 4*tt*s35 - 3*s15*s3&
          &5 + 3*s35**2)/((4*me2 - ss - tt + s25)*(s15 + s25 - s35))) - (ss**2 + tt*s15 + t&
          &t*s25 - 2*me2*(ss + 2*tt + 3*s15 - 3*s35) + ss*(2*tt + 2*s15 - s25 - 2*s35) - 4*&
          &tt*s35 - 3*s15*s35 + 3*s35**2)/((tt + s15 - s35)*(s15 + s25 - s35)) + (16*me2**2&
          & + ss**2 + tt**2 + tt*s15 - 2*me2*(4*ss + 4*tt + 2*s15 - s25 - 7*s35) + ss*(2*tt&
          & + s15 - s25 - 3*s35) - 4*tt*s35 - 2*s15*s35 + s25*s35 + 3*s35**2)/(s25*(4*me2 -&
          & ss - tt + s35)) - (4*me2**2 + tt**2 - tt*s25 - 2*me2*(ss + tt - s15 - s25 - s35&
          &) + s15*s35 + s25*s35 - ss*(s15 + s35))/(tt*s15) - (4*me2**2 + tt**2 - tt*s25 + &
          &s15*s35 + s25*s35 - ss*(s15 + s35) + 2*me2*(-2*tt + s15 + s25 + s35))/(s15*(4*me&
          &2 - ss - tt + s25)) + (4*me2**2 + ss**2 + tt**2 + tt*s15 + ss*(2*tt + s15 - s25 &
          &- 3*s35) - 4*tt*s35 - 2*s15*s35 + s25*s35 + 3*s35**2 + me2*(-4*ss - 6*tt - 4*s15&
          & + 2*s25 + 8*s35))/(s25*(tt + s15 - s35)))*pvc18(2) + 128*Pi**2*((-2*me2*s15 + s&
          &s*s15 + 2*tt*s15 + s15**2 + tt*s25 - tt*s35 - s15*s35)/(tt*s15) + (ss*s15 + 2*tt&
          &*s15 + s15**2 + tt*s25 - 2*me2*(3*s15 + s25 - s35) - tt*s35 - s15*s35)/(s15*(4*m&
          &e2 - ss - tt + s25)) - (-(tt*s15) - 2*tt*s25 + s25**2 + 2*me2*(s15 + s25 - s35) &
          &- ss*(s15 + s25 - s35) + 2*tt*s35 + 2*s15*s35 + s25*s35 - 2*s35**2)/(s25*(tt + s&
          &15 - s35)) + (2*me2*(5*s15 + 4*s25 - 4*s35) - (3*tt + s15 - s25 - 2*s35)*(s15 + &
          &s25 - s35) + ss*(-2*s15 - s25 + s35))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)&
          &) + (-((3*tt + s15 - s25 - 2*s35)*(s15 + s25 - s35)) + 2*me2*(2*s15 + s25 - s35)&
          & + ss*(-2*s15 - s25 + s35))/((tt + s15 - s35)*(s15 + s25 - s35)) + (tt*s15 + 2*t&
          &t*s25 - s25**2 + ss*(s15 + s25 - s35) - 2*tt*s35 - 2*s15*s35 - s25*s35 + 2*s35**&
          &2 + me2*(-4*s15 - 6*s25 + 6*s35))/(s25*(4*me2 - ss - tt + s35)))*pvc18(3) + 128*&
          &Pi**2*((-2*me2**2 + me2*(2*ss + 9*tt + 8*s15 + 2*s25 - 8*s35) - (ss + tt - s25)*&
          &(2*tt + 2*s15 + s25 - 2*s35))/(s25*(tt + s15 - s35)) + (-10*me2**2 - ss*(ss + 2*&
          &tt + s15 + s25 - 3*s35) + 2*me2*(3*ss + 2*tt + s15 + s25 - 2*s35))/((tt + s15 - &
          &s35)*(s15 + s25 - s35)) + (-12*me2**2 + me2*(2*ss + 12*tt + 10*s15 + s25 - 8*s35&
          &) - (ss + tt - s25)*(2*tt + 2*s15 + s25 - 2*s35))/(s25*(4*me2 - ss - tt + s35)) &
          &- ((2*me2 - ss)*(6*me2 - ss - 2*tt - s15 - s25 + 3*s35))/((4*me2 - ss - tt + s25&
          &)*(s15 + s25 - s35)) + (14*me2**2 + 2*ss**2 - me2*(12*ss + 11*tt - 2*s15 + s25 -&
          & 3*s35) + tt*(2*tt - s15 - s25 - s35) - ss*(-4*tt + s15 + 2*s35))/(tt*s35) + (-4&
          &4*me2**2 - 3*ss**2 - 2*tt**2 + 3*tt*s15 + 2*tt*s25 - 2*s15*s25 - s25**2 + ss*(-4&
          &*tt + 4*s15 + 4*s25 - s35) + s25*s35 + me2*(20*ss + 20*tt - 14*s15 - 13*s25 + 3*&
          &s35))/(s15*(4*me2 - ss - tt + s25)) + (28*me2**2 + 2*ss**2 + tt*(2*tt - s15 - s2&
          &5 - s35) - ss*(-4*tt + s15 + 2*s35) + me2*(-14*ss - 16*tt + 4*s15 + 2*s25 + 3*s3&
          &5))/(s35*(4*me2 - ss - tt + s35)) + (-22*me2**2 - 3*ss**2 - 2*tt**2 + 3*tt*s15 +&
          & 2*tt*s25 - 2*s15*s25 - s25**2 + ss*(-4*tt + 4*s15 + 4*s25 - s35) + s25*s35 + me&
          &2*(18*ss + 12*tt - 12*s15 - 13*s25 + 5*s35))/(tt*s15))*pvc20(1) + 128*Pi**2*((-6&
          &*me2 + ss + 2*tt - s35)/s15 - (-4*me2 + ss + tt)/s35 - ((2*me2 - ss)*(4*me2 - ss&
          & - tt + s35))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) - ((2*me2 - ss)*(4*me2&
          & - ss - tt + s35))/((tt + s15 - s35)*(s15 + s25 - s35)) + ((2*me2 - ss - tt)*(4*&
          &me2 - ss - tt + s35))/(tt*s35) - (8*me2**2 + (ss + tt - s25)*(ss + 2*tt - s35) +&
          & 2*me2*(-3*ss - 5*tt + 2*s25 + s35))/(tt*s15))*pvc20(2) + 128*Pi**2*(-(((2*me2 -&
          & ss)*(2*me2 - tt + s35))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35))) - ((2*me2 &
          &- ss)*(2*me2 - tt + s35))/((tt + s15 - s35)*(s15 + s25 - s35)) + ((2*me2 - ss - &
          &tt)*(2*me2 - tt + s35))/(tt*s35) + ((4*me2 - ss - tt)*(2*me2 - tt + s35))/(s35*(&
          &4*me2 - ss - tt + s35)) + (-12*me2**2 - 2*tt**2 + tt*s25 + 2*me2*(ss + 5*tt - s2&
          &5 - 2*s35) + tt*s35 - s25*s35 + ss*(-tt + s35))/(s15*(4*me2 - ss - tt + s25)) + &
          &(-4*me2**2 - 2*tt**2 + tt*s25 + 2*me2*(ss + 3*tt - s25 - s35) + tt*s35 - s25*s35&
          & + ss*(-tt + s35))/(tt*s15))*pvc20(3) + 128*Pi**2*(-((-40*me2**2 - 2*ss**2 - 2*t&
          &t**2 + 3*tt*s15 + s15**2 + 2*me2*(8*ss + 10*tt - 5*s15 - 6*s25) + 2*tt*s25 - 2*s&
          &15*s25 - 2*s25**2 + ss*(-4*tt + 2*s15 + 4*s25 - s35) + tt*s35 + s25*s35)/(s15*(4&
          &*me2 - ss - tt + s25))) + (56*me2**2 + 2*ss**2 + 4*tt**2 + 2*tt*s15 - 3*tt*s25 -&
          & 3*s15*s25 - 2*s25**2 - 2*me2*(10*ss + 16*tt + 6*s15 - 3*s25 - 14*s35) + 2*ss*(3&
          &*tt + s15 - 3*s35) - 6*tt*s35 + 4*s25*s35 + 2*s35**2)/(s25*(4*me2 - ss - tt + s3&
          &5)) + (-20*me2**2 - 2*ss**2 - 2*tt**2 + 2*tt*s15 + 2*tt*s25 + me2*(14*ss + 14*tt&
          & - 7*s15 - s25 - 5*s35) + tt*s35 - s15*s35 + 2*ss*(-2*tt + s15 + s35))/(tt*s35) &
          &+ (-40*me2**2 - 2*ss**2 - 2*tt**2 + 2*tt*s15 + 2*tt*s25 + 2*me2*(8*ss + 10*tt - &
          &4*s15 - 2*s25 - 3*s35) + tt*s35 - s15*s35 + 2*ss*(-2*tt + s15 + s35))/(s35*(4*me&
          &2 - ss - tt + s35)) + (20*me2**2 + 2*ss**2 + 2*tt**2 - 3*tt*s15 - s15**2 - 2*tt*&
          &s25 + 2*s15*s25 + 2*s25**2 + me2*(-14*ss - 14*tt + 8*s15 + 13*s25 - 4*s35) - tt*&
          &s35 - s25*s35 + ss*(4*tt - 2*s15 - 4*s25 + s35))/(tt*s15) + (-20*me2**2 - 2*ss**&
          &2 - 4*tt**2 - tt*s15 + s15**2 + 3*tt*s25 + s15*s25 + me2*(14*ss + 22*tt + 8*s15 &
          &- 13*s25 - 12*s35) + 6*tt*s35 + s15*s35 - 3*s25*s35 - 2*s35**2 + ss*(-6*tt - 2*s&
          &15 + 4*s25 + 3*s35))/((tt + s15 - s35)*(s15 + s25 - s35)) + (-56*me2**2 - 2*ss**&
          &2 - 4*tt**2 - tt*s15 + s15**2 + 3*tt*s25 + s15*s25 + 2*me2*(10*ss + 16*tt + 5*s1&
          &5 - 7*s25 - 11*s35) + 6*tt*s35 + s15*s35 - 3*s25*s35 - 2*s35**2 + ss*(-6*tt - 2*&
          &s15 + 4*s25 + 3*s35))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + (20*me2**2 +&
          & 2*ss**2 + 4*tt**2 + 2*tt*s15 - 3*tt*s25 - 3*s15*s25 - 2*s25**2 + 2*ss*(3*tt + s&
          &15 - 3*s35) - 6*tt*s35 + 4*s25*s35 + 2*s35**2 + me2*(-14*ss - 22*tt - 9*s15 + s2&
          &5 + 21*s35))/(s25*(tt + s15 - s35)))*pvc21(1) + 128*Pi**2*(-((32*me2**2 + ss**2 &
          &+ 4*ss*tt + 3*tt**2 - 4*me2*(3*ss + 5*tt - 4*s35) - 2*ss*s35 - 6*tt*s35 + 2*s35*&
          &*2)/(s35*(4*me2 - ss - tt + s35))) - (8*me2**2 + ss**2 + 4*ss*tt + 3*tt**2 - 2*m&
          &e2*(3*ss + 7*tt - 3*s35) - 2*ss*s35 - 6*tt*s35 + 2*s35**2)/(tt*s35) - (8*me2**2 &
          &+ ss**2 + 3*tt**2 + 2*tt*s15 - tt*s25 - 2*me2*(3*ss + 7*tt + 4*s15 - 2*s25 - 6*s&
          &35) + ss*(4*tt + 2*s15 - s25 - 4*s35) - 6*tt*s35 - 3*s15*s35 + s25*s35 + 3*s35**&
          &2)/((tt + s15 - s35)*(s15 + s25 - s35)) - (32*me2**2 + ss**2 + 3*tt**2 + 2*tt*s1&
          &5 - tt*s25 - 4*me2*(3*ss + 5*tt + 2*s15 - s25 - 5*s35) + ss*(4*tt + 2*s15 - s25 &
          &- 4*s35) - 6*tt*s35 - 3*s15*s35 + s25*s35 + 3*s35**2)/((4*me2 - ss - tt + s25)*(&
          &s15 + s25 - s35)) + (8*me2**2 + ss**2 + 3*tt**2 + 2*tt*s15 - 2*me2*(3*ss + 7*tt &
          &+ 4*s15 - 7*s35) + 2*ss*(2*tt + s15 - 2*s35) - 8*tt*s35 - 4*s15*s35 - s25*s35 + &
          &5*s35**2)/(s25*(tt + s15 - s35)) + (32*me2**2 + ss**2 + 3*tt**2 + 2*tt*s15 - 4*m&
          &e2*(3*ss + 5*tt + 2*s15 - 6*s35) + 2*ss*(2*tt + s15 - 2*s35) - 8*tt*s35 - 4*s15*&
          &s35 - s25*s35 + 5*s35**2)/(s25*(4*me2 - ss - tt + s35)) + (32*me2**2 + ss**2 + 3&
          &*tt**2 - tt*s25 - 4*me2*(3*ss + 5*tt - s25 - 3*s35) - 4*tt*s35 + s15*s35 + 2*s25&
          &*s35 - ss*(-4*tt + s25 + 2*s35))/(s15*(4*me2 - ss - tt + s25)) + (8*me2**2 + ss*&
          &*2 + 3*tt**2 - tt*s25 - 4*tt*s35 + s15*s35 + 2*s25*s35 - ss*(-4*tt + s25 + 2*s35&
          &) + me2*(-6*ss - 14*tt + 4*s25 + 4*s35))/(tt*s15))*pvc21(2) + 128*Pi**2*((s15**2&
          & - s15*(-2*tt + s25) + (-2*me2 + ss + tt - s35)*s35)/((tt + s15 - s35)*(s15 + s2&
          &5 - s35)) - (-2*me2*s15 + ss*s15 + tt*s15 - s15*s25 + 2*tt*s35 + s15*s35 + s25*s&
          &35 - 2*s35**2)/(s25*(tt + s15 - s35)) + (-2*me2*s15 + ss*s15 + tt*s15 + 2*tt*s35&
          & - s35**2)/(tt*s35) - (s15**2 + (-2*me2 + ss + tt - s25)*s35 - s15*(-2*tt + s35)&
          &)/(tt*s15) + (s15**2 - s15*(-2*tt + s25) + (ss + tt - s35)*s35 - 4*me2*(s15 + s3&
          &5))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) - (ss*s15 + tt*s15 - s15*s25 + 2&
          &*tt*s35 + s15*s35 + s25*s35 - 2*s35**2 - 4*me2*(s15 + s35))/(s25*(4*me2 - ss - t&
          &t + s35)) + (ss*s15 + tt*s15 + 2*tt*s35 - s35**2 - 4*me2*(s15 + s35))/(s35*(4*me&
          &2 - ss - tt + s35)) - (s15**2 + (ss + tt - s25)*s35 - s15*(-2*tt + s35) - 4*me2*&
          &(s15 + s35))/(s15*(4*me2 - ss - tt + s25)))*pvc21(3) + 128*Pi**2*((-48*me2**2 - &
          &8*ss**2 - 10*tt**2 - tt*s15 + s15**2 + 10*tt*s25 - 2*s25**2 + me2*(40*ss + 39*tt&
          & + s15 - 26*s25 - 12*s35) - ss*(17*tt + s15 - 10*s25 - 7*s35) + 10*tt*s35 + s15*&
          &s35 - 6*s25*s35 - 2*s35**2)/((tt + s15 - s35)*(s15 + s25 - s35)) - (86*me2**2 + &
          &5*ss**2 + 7*tt**2 + tt*s15 - s15**2 - 6*tt*s25 + s25**2 - me2*(43*ss + 46*tt + s&
          &15 - 23*s25 - 24*s35) + ss*(11*tt + s15 - 6*s25 - 5*s35) - 8*tt*s35 - s15*s35 + &
          &4*s25*s35 + 2*s35**2)/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + (-8*me2**2 -&
          & 2*ss**2 - tt**2 + me2*(8*ss - 3*tt - 10*s15 - 4*s25) + tt*s25 - tt*s35 - 2*s15*&
          &s35 + s35**2 + ss*(-tt + 2*s15 + 2*s25 + s35))/(tt*s35) + (6*me2**2 + ss**2 + 2*&
          &tt**2 - tt*s15 - 2*s15**2 - 2*tt*s25 + s25**2 - s25*s35 + ss*(3*tt + s15 - 2*s25&
          & + s35) - me2*(7*ss + 8*tt + s15 - 5*s25 + 2*s35))/(s15*(4*me2 - ss - tt + s25))&
          & + (-2*me2**2 - ss**2 + me2*(5*ss - 5*s15 - 3*(tt + s25 - 2*s35)) + ss*(tt + 2*s&
          &15 + s25 - s35) + s35*(-3*tt - 2*s15 + s25 + 2*s35))/(s35*(4*me2 - ss - tt + s35&
          &)) + (8*me2**2 + 2*ss**2 + 3*tt**2 - tt*s15 - 2*s15**2 - 4*tt*s25 + 2*s25**2 - s&
          &25*s35 + ss*(5*tt + s15 - 4*s25 + s35) - me2*(8*ss + 9*tt - 8*s25 + 6*s35))/(tt*&
          &s15) + (24*me2**2 + 4*ss**2 + 4*tt**2 + 2*tt*s15 + 2*s15**2 - 3*tt*s25 + s15*s25&
          & - 5*tt*s35 + 3*s25*s35 + s35**2 - ss*(-6*tt + 2*s15 + 4*s25 + 5*s35) + me2*(-20&
          &*ss - 13*tt + 7*s15 + 12*s25 + 10*s35))/(s25*(tt + s15 - s35)) + (46*me2**2 + 3*&
          &ss**2 + 3*tt**2 + 2*tt*s15 + 2*s15**2 - 2*tt*s25 + s15*s25 - 3*tt*s35 + 2*s25*s3&
          &5 - ss*(-4*tt + 2*s15 + 3*s25 + 3*s35) + me2*(-23*ss - 21*tt + 4*s15 + 11*s25 + &
          &12*s35))/(s25*(4*me2 - ss - tt + s35)))*pvc22(1) + (128*Pi**2*(-(tt**3*s15**3) +&
          & 3*tt**3*s15**2*s25 + 4*tt**2*s15**3*s25 + 3*tt**3*s15*s25**2 - 2*tt*s15**3*s25*&
          &*2 - tt**3*s25**3 - 4*tt**2*s15*s25**3 - 2*tt*s15**2*s25**3 + tt**3*s15**2*s35 -&
          & tt*s15**4*s35 - 4*tt**3*s15*s25*s35 - 10*tt**2*s15**2*s25*s35 - 2*tt*s15**3*s25&
          &*s35 + s15**4*s25*s35 + tt**3*s25**2*s35 + 7*tt*s15**2*s25**2*s35 + 2*s15**3*s25&
          &**2*s35 + 2*tt**2*s25**3*s35 + 6*tt*s15*s25**3*s35 + s15**2*s25**3*s35 + 2*tt*s1&
          &5**3*s35**2 + 6*tt**2*s15*s25*s35**2 + 3*tt*s15**2*s25*s35**2 - 3*s15**3*s25*s35&
          &**2 - 2*tt**2*s25**2*s35**2 - 6*tt*s15*s25**2*s35**2 - 5*s15**2*s25**2*s35**2 - &
          &tt*s25**3*s35**2 - 2*s15*s25**3*s35**2 - tt*s15**2*s35**3 - tt*s15*s25*s35**3 + &
          &3*s15**2*s25*s35**3 + tt*s25**2*s35**3 + 3*s15*s25**2*s35**3 - s15*s25*s35**4 - &
          &32*me2**3*s15*(tt*(s15 - s35) + s25*(-tt + s35)) + ss**3*s15*(tt*(s15 - s35) + s&
          &25*(-tt + s35)) + 8*me2**2*(-(tt*s25*(s25 - s35)*(-tt + s35)) + s15**3*(-2*tt + &
          &2*s25 + s35) + 2*s15**2*(tt*(2*ss + tt) + s25**2 + tt*s35 - 3*s25*s35 - s35**2) &
          &+ s15*(s25**2*(6*tt - 5*s35) - 4*ss*tt*s35 - 2*tt**2*s35 + s35**3 + 4*ss*s25*(-t&
          &t + s35) + 3*s25*(-tt**2 + s35**2))) + ss**2*(-(tt*s25*(s25 - s35)*(-tt + s35)) &
          &+ s15**3*(-tt + s25 + s35) + s15**2*(2*tt**2 + s25**2 + tt*s35 - 2*s35**2 - s25*&
          &(tt + 3*s35)) + s15*(s25**2*(4*tt - 3*s35) - 2*tt**2*s35 + s35**3 + s25*(-3*tt**&
          &2 + 2*tt*s35 + s35**2))) - ss*(s15**4*s35 - tt*s25*(s25 - s35)*(-tt + s35)*(-tt &
          &+ s25 + s35) + s15**3*(2*tt**2 - 5*tt*s25 + s25**2 - tt*s35 + 2*s25*s35 - 2*s35*&
          &*2) + s15**2*(-tt**3 + s25**3 - 2*tt**2*s35 + 2*tt*s35**2 + s35**3 - 2*s25**2*(t&
          &t + s35) + s25*(-2*tt**2 + 11*tt*s35 - 5*s35**2)) + s15*(s25**3*(3*tt - 2*s35) +&
          & tt**3*s35 - tt*s35**3 - s25**2*(7*tt**2 - 7*tt*s35 + s35**2) + s25*(2*tt**3 + 2&
          &*tt**2*s35 - 7*tt*s35**2 + 3*s35**3))) - 2*me2*(-2*s15**4*s35 + tt*s25*(s25 - s3&
          &5)*(-tt + s35)*(-tt + 2*s25 + s35) + s15**3*(-4*tt**2 + 10*tt*s25 - 2*s25**2 + t&
          &t*s35 - 3*s25*s35 + 4*s35**2) + 5*ss**2*s15*(tt*(s15 - s35) + s25*(-tt + s35)) +&
          & s15**2*(tt**3 - 2*s25**3 + 4*tt**2*s35 - 2*tt*s35**2 - 2*s35**3 + s25**2*(4*tt &
          &+ 5*s35) + s25*(6*tt**2 - 23*tt*s35 + 7*s35**2)) + s15*(s25**3*(-6*tt + 4*s35) -&
          & 2*tt*s25**2*(-6*tt + 5*s35) + tt*s35*(-tt**2 + s35**2) - 2*s25*(tt**3 + 4*tt**2&
          &*s35 - 7*tt*s35**2 + 2*s35**3)) + ss*(-3*tt*s25*(s25 - s35)*(-tt + s35) + s15**3&
          &*(-4*tt + 4*s25 + 3*s35) + 2*s15**2*(3*tt**2 + 2*s25**2 + 2*tt*s35 - 3*s35**2 - &
          &s25*(tt + 6*s35)) + s15*(s25**2*(14*tt - 11*s35) + 3*s35*(-2*tt**2 + s35**2) + s&
          &25*(-9*tt**2 + 4*tt*s35 + 5*s35**2)))))*pvc22(2))/(s15*s25*(4*me2 - ss - tt + s2&
          &5)*(tt + s15 - s35)*(s15 + s25 - s35)*(4*me2 - ss - tt + s35)) + 128*Pi**2*(-(((&
          &tt + s15)*(2*me2 - ss - tt + s15 + s25))/(tt*s15)) - ((-2*me2 + tt + s15)*(2*me2&
          & - ss - tt + s15 + s25))/(s15*(4*me2 - ss - tt + s25)) + (4*me2**2 + ss**2 + 2*t&
          &t**2 - 3*tt*s25 + s25**2 - 2*me2*(2*ss + 4*tt + s15 - 2*s25 - 3*s35) + ss*(3*tt &
          &- 2*s25 - 2*s35) - 2*tt*s35 + s15*s35 + 2*s25*s35)/(s25*(tt + s15 - s35)) - (16*&
          &me2**2 + ss**2 + 4*tt**2 - tt*s15 - 2*s15**2 - 5*tt*s25 - s15*s25 + s25**2 - 2*m&
          &e2*(5*ss + 9*tt - 2*s15 - 5*s25 - 4*s35) + ss*(5*tt + s15 - 2*s25 - 3*s35) - 3*t&
          &t*s35 + 2*s15*s35 + 3*s25*s35)/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) - (ss&
          &**2 + 4*tt**2 - tt*s15 - 2*s15**2 - 5*tt*s25 - s15*s25 + s25**2 - 2*me2*(ss + 5*&
          &tt + 2*s15 - s25 - 4*s35) + ss*(5*tt + s15 - 2*s25 - 3*s35) - 3*tt*s35 + 2*s15*s&
          &35 + 3*s25*s35)/((tt + s15 - s35)*(s15 + s25 - s35)) + (16*me2**2 + ss**2 + 2*tt&
          &**2 - 3*tt*s25 + s25**2 + ss*(3*tt - 2*s25 - 2*s35) - 2*tt*s35 + s15*s35 + 2*s25&
          &*s35 + me2*(-8*ss - 12*tt + 8*s25 + 6*s35))/(s25*(4*me2 - ss - tt + s35)))*pvc22&
          &(3) + 128*Pi**2*(1/tt + 1/s15 + (-2*me2 + tt + s15)/(s15*(4*me2 - ss - tt + s25)&
          &) + (-2*me2 + ss + tt - s25 - s35)/(s25*(tt + s15 - s35)) + (-4*me2 + ss + tt - &
          &s25 - s35)/(s25*(4*me2 - ss - tt + s35)) + (2*(2*me2 - ss - 2*tt - s15 + s25 + s&
          &35))/((tt + s15 - s35)*(s15 + s25 - s35)) + (2*(6*me2 - ss - 2*tt - s15 + s25 + &
          &s35))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)))*(-0.5 + 4*pvc22(4)) + 128*Pi*&
          &*2*((tt + s15)/s15 + (tt*(-2*me2 + tt + s15))/(s15*(4*me2 - ss - tt + s25)) - (t&
          &t*(2*me2 - ss - tt + s25 + s35))/(s25*(tt + s15 - s35)) - (tt*(4*me2 - ss - tt +&
          & s25 + s35))/(s25*(4*me2 - ss - tt + s35)) + (2*tt*(2*me2 - ss - 2*tt - s15 + s2&
          &5 + s35))/((tt + s15 - s35)*(s15 + s25 - s35)) + (2*tt*(6*me2 - ss - 2*tt - s15 &
          &+ s25 + s35))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)))*pvc22(5) + 128*Pi**2*&
          &(-tt + s15)*(-((tt + s15)/(tt*s15)) - (-2*me2 + tt + s15)/(s15*(4*me2 - ss - tt &
          &+ s25)) + (2*(-6*me2 + ss + 2*tt + s15 - s25 - s35))/((4*me2 - ss - tt + s25)*(s&
          &15 + s25 - s35)) + (2*(-2*me2 + ss + 2*tt + s15 - s25 - s35))/((tt + s15 - s35)*&
          &(s15 + s25 - s35)) + (2*me2 - ss - tt + s25 + s35)/(s25*(tt + s15 - s35)) + (4*m&
          &e2 - ss - tt + s25 + s35)/(s25*(4*me2 - ss - tt + s35)))*pvc22(6) + 128*Pi**2*(m&
          &e2 - s15)*(1/tt + 1/s15 + (-2*me2 + tt + s15)/(s15*(4*me2 - ss - tt + s25)) + (-&
          &2*me2 + ss + tt - s25 - s35)/(s25*(tt + s15 - s35)) + (-4*me2 + ss + tt - s25 - &
          &s35)/(s25*(4*me2 - ss - tt + s35)) + (2*(2*me2 - ss - 2*tt - s15 + s25 + s35))/(&
          &(tt + s15 - s35)*(s15 + s25 - s35)) + (2*(6*me2 - ss - 2*tt - s15 + s25 + s35))/&
          &((4*me2 - ss - tt + s25)*(s15 + s25 - s35)))*pvc22(7) + 128*Pi**2*((me2 - ss + t&
          &t + 2*s15 + s25 - s35)/s15 + (2*me2**2 - (ss + tt - s25)*(ss - s35) + me2*(2*ss &
          &- tt - s35))/(tt*s35) + (me2*(5*ss - tt - 3*s15 - 3*s25) - (ss + tt - s25)*(ss -&
          & s35))/(s35*(4*me2 - ss - tt + s35)) + (5*me2 - ss - tt + s35)/(s15 + s25 - s35)&
          & + (2*me2**2 + me2*(-4*ss + 4*tt + 6*s15 + 3*s25 - 3*s35) + (ss + tt - s25)*(ss &
          &- tt - 2*s15 - s25 + s35))/(tt*s15) + ((ss + tt - s25)*(tt + s15 - s35) + me2*(s&
          &s - 5*tt - 3*s15 + 2*s25 + s35))/(s25*(4*me2 - ss - tt + s35)) + (-2*me2**2 + (s&
          &s + tt - s25)*(tt + s15 - s35) + me2*(-5*tt - 4*s15 + s25 + 2*s35))/(s25*(tt + s&
          &15 - s35)) + (10*me2**2 + (ss + tt - s25)*(ss + tt - s35) + me2*(-6*ss - 6*tt + &
          &4*s25 + 2*s35))/((tt + s15 - s35)*(s15 + s25 - s35)))*pvc4(1) - (128*(2*me2 - ss&
          &)*Pi**2*(2*me2 - ss + s25)*pvc4(2))/(tt*s15) + (128*Pi**2*(2*me2 - ss + s25)*(-4&
          &*me2 + ss + tt + s15 - s35)*pvc4(3))/(tt*s15) + 128*Pi**2*((-38*me2**2 + me2*(26&
          &*ss + 26*tt - 6*s25 - 22*s35) - (4*ss + 4*tt - 3*s25 - 2*s35)*(ss + tt - s35))/(&
          &s25*(tt + s15 - s35)) + (8*me2**2 - me2*(7*ss + tt + 2*s15 - 2*s25 - 2*s35) + ss&
          &*(ss + tt - s35))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + (6*me2**2 + me2*&
          &(-4*ss + tt - 2*s15) + ss*(ss + tt - s35))/((tt + s15 - s35)*(s15 + s25 - s35)) &
          &+ (-76*me2**2 + me2*(33*ss + 37*tt + 4*s15 - 10*s25 - 29*s35) - (4*ss + 4*tt - 3&
          &*s25 - 2*s35)*(ss + tt - s35))/(s25*(4*me2 - ss - tt + s35)) + (8*me2**2 + (ss +&
          & s15 - s25)*(ss + tt - s35) + me2*(-7*ss - tt - 4*s15 + s25 + 3*s35))/(s15*(4*me&
          &2 - ss - tt + s25)) + (2*me2**2 + (ss + s15 - s25)*(ss + tt - s35) + me2*(-4*ss &
          &- tt - 2*s15 + s25 + 3*s35))/(tt*s15) + (10*me2**2 - (ss + tt - s35)*(-2*tt + s1&
          &5 + s25 + s35) + me2*(-4*ss - 12*tt + 4*s15 + 3*s25 + 7*s35))/(tt*s35) + (20*me2&
          &**2 - (ss + tt - s35)*(-2*tt + s15 + s25 + s35) + me2*(-3*ss - 15*tt + 4*s15 + 4&
          &*s25 + 9*s35))/(s35*(4*me2 - ss - tt + s35)))*pvc5(1) + (128*Pi**2*(-(tt*s15**2*&
          &s35) + s15*(-2*tt + s25)*(tt - s35)*s35 + s25*s35*(-tt + s35)**2 - 4*me2**2*(s15&
          &*s25 + s25*(tt - s35) - 2*tt*s35) + ss*s15*(tt*s35 + s25*(-tt + s35)) - ss*(-tt &
          &+ s35)*(2*tt*s35 + s25*(-tt + s35)) + 2*me2*(s15*(tt*s25 + tt*s35 - 2*s25*s35) +&
          & ss*(tt*s25 + s15*s25 - 2*tt*s35 - s25*s35) + (-tt + s35)*(-(tt*s25) + 2*tt*s35 &
          &+ 2*s25*s35)))*pvc5(2))/(tt*s25*s35*(-tt - s15 + s35)) - (128*Pi**2*(8*me2**2*(s&
          &15*s25 + s25*(tt - s35) - 2*tt*s35) + ss**2*(s15*s25 + s25*(tt - s35) - 2*tt*s35&
          &) - 2*me2*(tt**2*s25 - 2*tt**2*s35 - 4*tt*s25*s35 + 3*tt*s35**2 + 4*s25*s35**2 +&
          & s15*(tt*s25 + 4*tt*s35 - 4*s25*s35) + 3*ss*(tt*s25 + s15*s25 - 2*tt*s35 - s25*s&
          &35)) - s35*(s15*(-2*tt**2 + 2*tt*s25 + 3*tt*s35 - 2*s25*s35) + s25*(tt**2 - 3*tt&
          &*s35 + 2*s35**2)) + ss*(s15*s25*(tt - 3*s35) + 2*tt*s15*s35 + tt*s35*(-2*tt + 3*&
          &s35) + s25*(tt**2 - 3*tt*s35 + 3*s35**2)))*pvc5(3))/(tt*s25*s35*(-tt - s15 + s35&
          &)) + 128*Pi**2*((6*me2**2 - 2*me2*(2*ss + 3*tt + 2*s15 + s25 - 3*s35) + (ss + tt&
          & - s25)*(tt + s15 + s25 - s35))/(s25*(tt + s15 - s35)) + (4*me2**2 - me2*(5*ss +&
          & 2*(tt + s25 - s35)) + ss*(ss + tt - s35))/((4*me2 - ss - tt + s25)*(s15 + s25 -&
          & s35)) + (2*me2**2 + ss*(ss + tt - s35) - 2*me2*(ss + tt + s25 - s35))/((tt + s1&
          &5 - s35)*(s15 + s25 - s35)) - ((5*me2 - ss - tt + s25)*(-2*me2 + tt + s15 + s25 &
          &- s35))/(s25*(4*me2 - ss - tt + s35)) + (12*me2**2 + ss**2 + 2*tt**2 - 3*tt*s15 &
          &- 2*tt*s25 + s15*s25 + s25**2 + me2*(-8*ss - 8*tt + 7*s15 + 8*s25 - 4*s35) - s25&
          &*s35 + ss*(3*tt - 2*s15 - 2*s25 + s35))/(tt*s15) + (24*me2**2 + ss**2 + 2*tt**2 &
          &- 3*tt*s15 - 2*tt*s25 + s15*s25 + s25**2 + me2*(-8*ss - 15*tt + 9*s15 + 8*s25 - &
          &3*s35) - s25*s35 + ss*(3*tt - 2*s15 - 2*s25 + s35))/(s15*(4*me2 - ss - tt + s25)&
          &) + (-22*me2**2 - ss**2 + ss*(-3*tt + s15 + s25) + me2*(8*ss + 15*tt - 4*s15 - 4&
          &*s25 - s35) + tt*(-2*tt + s15 + s25 + s35))/(s35*(4*me2 - ss - tt + s35)) + (-12&
          &*me2**2 - ss**2 + ss*(-3*tt + s15 + s25) + tt*(-2*tt + s15 + s25 + s35) + me2*(8&
          &*ss - 3*s15 - 2*(-5*tt + s25 + s35)))/(tt*s35))*pvc6(1) + 128*Pi**2*((-2*me2 + s&
          &s + tt - s15 - s25)/s15 + ((2*me2 - tt)*(2*me2 - ss - tt + s15 + s25))/(s15*(4*m&
          &e2 - ss - tt + s25)) + (2*me2 - ss - tt + s35)/s35 + ((2*me2 - tt)*(-2*me2 + ss &
          &+ tt - s35))/(s35*(4*me2 - ss - tt + s35)))*pvc6(2) + (128*Pi**2*(8*me2**2*tt*(-&
          &s15 + s35) + tt*(-((s15 - s25)*s35*(-tt + s35)) + ss**2*(-s15 + s35) + ss*(s15 -&
          & s35)*(-tt + s25 + s35)) - 2*me2*(tt*(3*ss + tt - 2*s25 - s35)*s35 + s15*(-(tt*(&
          &3*ss + tt)) + 2*tt*s35 - s35**2 + s25*(tt + s35))))*pvc6(3))/(s15*(4*me2 - ss - &
          &tt + s25)*s35*(4*me2 - ss - tt + s35)) + 128*Pi**2*((2*me2 - ss + s25)/(s15*(4*m&
          &e2 - ss - tt + s25)) + (-2*me2 + ss - s35)/(s35*(4*me2 - ss - tt + s35)))*(-0.5 &
          &+ 4*pvc6(4)) + 128*me2*Pi**2*((2*me2 - ss + s25)/(s15*(4*me2 - ss - tt + s25)) +&
          & (-2*me2 + ss - s35)/(s35*(4*me2 - ss - tt + s35)))*pvc6(5) + (128*tt*Pi**2*(-8*&
          &me2**2*(s15 - s35) + ss**2*(-s15 + s35) + ss*(s15 - s35)*(-tt + s25 + s35) + s35&
          &*(s15*(tt - s25) + s25*(-tt + s35)) + 2*me2*(s15*(tt - s25 - 2*s35) + 3*ss*(s15 &
          &- s35) + s35*(-tt + 2*s25 + s35)))*pvc6(6))/(s15*(4*me2 - ss - tt + s25)*s35*(4*&
          &me2 - ss - tt + s35)) + 128*tt*Pi**2*((2*me2 - ss + s25)/(s15*(4*me2 - ss - tt +&
          & s25)) + (-2*me2 + ss - s35)/(s35*(4*me2 - ss - tt + s35)))*pvc6(7) + 128*Pi**2*&
          &((4*me2**2 - me2*(2*ss + 4*tt + s25 - 6*s35) + s15*(2*tt + 2*s15 + s25 - 2*s35))&
          &/(tt*s15) + (12*me2**2 + s15*(2*tt + 2*s15 + s25 - 2*s35) + me2*(-2*ss - 4*tt - &
          &4*s15 + 2*s25 + 3*s35))/(s15*(4*me2 - ss - tt + s25)) + (16*me2**2 + 2*ss**2 + 2&
          &*tt**2 - s15**2 - 2*tt*s25 - s15*s25 + ss*(4*tt + s15 - 3*s25 - 2*s35) - 3*tt*s3&
          &5 + 2*s25*s35 + s35**2 + me2*(-12*ss - 10*tt - 2*s15 + 6*s25 + 5*s35))/((tt + s1&
          &5 - s35)*(s15 + s25 - s35)) + (28*me2**2 + 2*ss**2 + tt**2 - 2*tt*s15 - 2*tt*s25&
          & + 3*s15*s35 + 2*s25*s35 - s35**2 - ss*(-2*tt + 3*s15 + s25 + 2*s35) + me2*(-16*&
          &ss - 8*tt + 10*s15 + 7*s25 + 7*s35))/(s35*(4*me2 - ss - tt + s35)) + (24*me2**2 &
          &+ 4*ss**2 + 3*tt**2 - 2*tt*s15 - 3*tt*s25 - 3*tt*s35 + 3*s15*s35 + 3*s25*s35 - s&
          &s*(-6*tt + 3*s15 + 2*s25 + 5*s35) + me2*(-20*ss - 10*tt + 13*s15 + 7*s25 + 7*s35&
          &))/(tt*s35) + (28*me2**2 + 2*ss**2 + 2*tt**2 - s15**2 - 2*tt*s25 - s15*s25 + ss*&
          &(4*tt + s15 - 3*s25 - 2*s35) - 3*tt*s35 + 2*s25*s35 + s35**2 + me2*(-14*ss - 16*&
          &tt - 2*s15 + 9*s25 + 8*s35))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) - (44*m&
          &e2**2 + 6*ss**2 + 5*tt**2 + s15**2 - 5*tt*s25 - 6*tt*s35 + s15*s35 + 5*s25*s35 +&
          & s35**2 - ss*(-10*tt + 2*s15 + 5*s25 + 7*s35) + me2*(-34*ss - 24*tt + 11*s15 + 1&
          &2*s25 + 18*s35))/(s25*(tt + s15 - s35)) - (68*me2**2 + 4*ss**2 + 3*tt**2 + s15**&
          &2 - 4*tt*s25 - 3*tt*s35 + s15*s35 + 4*s25*s35 - 2*ss*(-3*tt + s15 + 2*s25 + 2*s3&
          &5) + me2*(-32*ss - 28*tt + 4*s15 + 18*s25 + 18*s35))/(s25*(4*me2 - ss - tt + s35&
          &)))*pvc9(1) + 128*Pi**2*((ss**2 - 2*me2*(ss + 2*tt + 3*s15 - 2*s35) + s15*(tt - &
          &s35) + 2*ss*(tt + s15 - s35))/(s25*(tt + s15 - s35)) + (12*me2**2 + ss**2 - 2*me&
          &2*(4*ss + 2*tt + 3*s15 - 2*s35) + s15*(tt - s35) + 2*ss*(tt + s15 - s35))/(s25*(&
          &4*me2 - ss - tt + s35)) + (2*me2 - tt + s35)**2/(s35*(4*me2 - ss - tt + s35)) + &
          &(-4*me2**2 - ss**2 + me2*(4*ss + 6*(tt + s15 - s35)) - 2*ss*(tt + s15 - s35) + (&
          &tt + s15 - s35)*(-tt + s35))/((tt + s15 - s35)*(s15 + s25 - s35)) + (-16*me2**2 &
          &- ss**2 + me2*(8*ss + 8*tt + 6*s15 - 8*s35) - 2*ss*(tt + s15 - s35) + (tt + s15 &
          &- s35)*(-tt + s35))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + (4*me2**2 - 2*&
          &me2*(ss + tt - s35) + (-tt + s35)**2)/(tt*s35))*pvc9(2) + 128*Pi**2*(-((-2*me2*s&
          &25 + ss*s25 + (tt + s15 - s35)*(2*s25 - s35))/((tt + s15 - s35)*(s15 + s25 - s35&
          &))) + (3*tt*s25 + 2*s15*s25 - s15*s35 - 3*s25*s35 - 2*me2*(s25 + s35) + ss*(s25 &
          &+ s35))/(s25*(tt + s15 - s35)) + (2*me2*s35 - ss*s35 + (-tt + s35)*(s25 + s35))/&
          &(tt*s35) + (3*tt*s25 + 2*s15*s25 - s15*s35 - 3*s25*s35 + ss*(s25 + s35) - 2*me2*&
          &(4*s25 + s35))/(s25*(4*me2 - ss - tt + s35)) - (ss*s25 + (tt + s15 - s35)*(2*s25&
          & - s35) + me2*(-6*s25 + 2*s35))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + (-&
          &(ss*s35) + (-tt + s35)*(s25 + s35) + 2*me2*(s25 + 2*s35))/(s35*(4*me2 - ss - tt &
          &+ s35)))*pvc9(3) - (128*Pi**2*(256*me2**5*(s15 - s35)*(3*s15**2*s25 - 3*s25*(s25&
          & - s35)*(-tt + s35) + s15*(3*tt*s25 + 3*s25**2 - tt*s35 - 6*s25*s35)) - 32*me2**&
          &4*(-2*s15**4*s25 + 32*ss*s25*(s15 - s35)*(tt + s15 - s35)*(s15 + s25 - s35) - s2&
          &5*(s25 - s35)*s35*(-tt + s35)*(-15*tt + 22*s25 + 8*s35) + s15**3*(13*tt*s25 - 12&
          &*s25**2 + tt*s35 - 18*s25*s35) + s15*(-(tt*s35**2*(3*tt + 13*s35)) + 2*s25**3*(-&
          &5*tt + 16*s35) + s25*s35*(-30*tt**2 + 101*tt*s35 - 38*s35**2) + s25**2*(15*tt**2&
          & - 32*tt*s35 - 24*s35**2)) + s15**2*(-10*s25**3 + 3*tt*s35*(tt + 4*s35) + s25**2&
          &*(3*tt + 22*s35) + 5*s25*(3*tt**2 - 15*tt*s35 + 10*s35**2))) + 8*me2**3*(s15**4*&
          &(-3*tt*s25 + 2*s25**2 + tt*s35 + 8*s25*s35) + s25*(s25 - s35)*s35*(-tt + s35)*(2&
          &3*tt**2 - 54*tt*s25 + 24*s25**2 - 16*tt*s35 + 36*s25*s35 - 4*s35**2) + 2*ss**2*(&
          &s15 - s35)*(31*s15**2*s25 - 31*s25*(s25 - s35)*(-tt + s35) + s15*(31*s25**2 + 31&
          &*s25*(tt - 2*s35) + 19*tt*s35)) + 2*s15**3*(3*s25**3 - tt*s35*(16*tt + 9*s35) + &
          &s25**2*(-11*tt + 17*s35) + 2*s25*(5*tt**2 - 3*tt*s35 + 3*s35**2)) + s15**2*(4*s2&
          &5**4 - s25**3*(15*tt + 4*s35) + tt*s35*(7*tt**2 + 69*tt*s35 + 3*s35**2) - s25**2&
          &*(tt**2 - 122*tt*s35 + 74*s35**2) + s25*(23*tt**3 - 170*tt**2*s35 + 82*tt*s35**2&
          & - 44*s35**3)) - s15*(4*s25**4*(-tt + 7*s35) + tt*s35**2*(7*tt**2 + 37*tt*s35 - &
          &14*s35**2) + s25**3*(21*tt**2 - 97*tt*s35 + 14*s35**2) + s25**2*(-23*tt**3 + 52*&
          &tt**2*s35 + 134*tt*s35**2 - 78*s35**3) + s25*s35*(46*tt**3 - 224*tt**2*s35 + 119&
          &*tt*s35**2 - 20*s35**3)) + ss*(-6*s15**4*s25 - s25*(s25 - s35)*s35*(-tt + s35)*(&
          &-73*tt + 82*s25 + 36*s35) - s15**3*(-67*tt*s25 + 48*s25**2 + 27*tt*s35 + 74*s25*&
          &s35) + s15*(-(tt*s35**2*(53*tt + 49*s35)) + 2*s25**3*(-21*tt + 62*s35) + s25*s35&
          &*(-146*tt**2 + 479*tt*s35 - 158*s35**2) + s25**2*(73*tt**2 - 148*tt*s35 - 84*s35&
          &**2)) + s15**2*(-42*s25**3 + tt*s35*(53*tt + 76*s35) + s25**2*(25*tt + 86*s35) +&
          & s25*(73*tt**2 - 381*tt*s35 + 202*s35**2)))) - 2*me2**2*(s15**5*(-6*tt*s35 + 8*s&
          &25*s35) + 4*ss**3*(s15 - s35)*(13*s15**2*s25 - 13*s25*(s25 - s35)*(-tt + s35) + &
          &s15*(13*s25**2 + 13*s25*(tt - 2*s35) + 25*tt*s35)) + s15**4*(9*tt*s35*(tt + 2*s3&
          &5) + s25**2*(-5*tt + 16*s35) + s25*(5*tt**2 - 5*tt*s35 - 46*s35**2)) - s25*(s25 &
          &- s35)*s35*(-tt + s35)*(-22*tt**3 + 8*s25**3 + 29*tt**2*s35 + tt*s35**2 - 6*s35*&
          &*3 + s25**2*(-46*tt + 36*s35) + s25*(56*tt**2 - 62*tt*s35 + 8*s35**2)) + s15**3*&
          &(s25**3*(3*tt + 16*s35) - tt**2*s35*(65*tt + 18*s35) + s25**2*(-26*tt**2 + 39*tt&
          &*s35 - 118*s35**2) + s25*(27*tt**3 + 34*tt**2*s35 + 40*tt*s35**2 + 72*s35**3)) +&
          & s15**2*(8*s25**4*(tt + 2*s35) - s25**3*(23*tt**2 + 37*tt*s35 + 52*s35**2) + tt*&
          &s35*(18*tt**3 + 89*tt**2*s35 - 21*tt*s35**2 - 12*s35**3) + s25**2*(tt**3 + 237*t&
          &t**2*s35 - 188*tt*s35**2 + 178*s35**3) + s25*(22*tt**4 - 226*tt**3*s35 + 71*tt**&
          &2*s35**2 - 35*tt*s35**3 - 44*s35**4)) + s15*(8*s25**5*s35 + 6*tt**2*s35**2*(-3*t&
          &t**2 - 4*tt*s35 + 5*s35**2) + 2*s25**4*(4*tt**2 - 23*tt*s35 + 6*s35**2) + 2*s25*&
          &*3*(-13*tt**3 + 54*tt**2*s35 - 23*tt*s35**2 + 4*s35**3) + s25**2*(22*tt**4 - 32*&
          &tt**3*s35 - 209*tt**2*s35**2 + 255*tt*s35**3 - 90*s35**4) + s25*s35*(-44*tt**4 +&
          & 256*tt**3*s35 - 205*tt**2*s35**2 + 17*tt*s35**3 + 16*s35**4)) - 2*ss**2*(3*s15*&
          &*4*s25 + s25*(s25 - s35)*s35*(-tt + s35)*(-53*tt + 50*s25 + 25*s35) + s15**3*(-5&
          &0*tt*s25 + 31*s25**2 + 46*tt*s35 + 48*s25*s35) + s15**2*(28*s25**3 - 11*s25**2*(&
          &2*tt + 5*s35) - tt*s35*(105*tt + 64*s35) + s25*(-53*tt**2 + 322*tt*s35 - 130*s35&
          &**2)) + s15*(s25**3*(28*tt - 78*s35) + 3*tt*s35**2*(35*tt + 6*s35) + s25**2*(-53&
          &*tt**2 + 104*tt*s35 + 49*s35**2) + 2*s25*s35*(53*tt**2 - 191*tt*s35 + 52*s35**2)&
          &)) + ss*(s25*(s25 - s35)*s35*(-tt + s35)*(80*tt**2 - 146*tt*s25 + 56*s25**2 - 67&
          &*tt*s35 + 92*s25*s35 - 4*s35**2) + s15**4*(4*s25**2 + 25*tt*s35 + s25*(tt + 8*s3&
          &5)) + s15**3*(16*s25**3 - tt*s35*(143*tt + 62*s35) + s25**2*(-67*tt + 68*s35) + &
          &s25*(81*tt**2 - 17*tt*s35 + 76*s35**2)) + s15**2*(12*s25**4 - 8*s25**3*(7*tt + 2&
          &*s35) + s25**2*(9*tt**2 + 377*tt*s35 - 140*s35**2) + tt*s35*(132*tt**2 + 185*tt*&
          &s35 + s35**2) + s25*(80*tt**3 - 702*tt**2*s35 + 296*tt*s35**2 - 172*s35**3)) - s&
          &15*(4*s25**4*(-3*tt + 17*s35) + 6*tt*s35**2*(22*tt**2 + 7*tt*s35 - 6*s35**2) + s&
          &25**3*(72*tt**2 - 262*tt*s35 + 36*s35**2) + s25**2*(-80*tt**3 + 162*tt**2*s35 + &
          &361*tt*s35**2 - 164*s35**3) + s25*s35*(160*tt**3 - 846*tt**2*s35 + 447*tt*s35**2&
          & - 84*s35**3)))) + me2*(-(s15**5*s35*(7*tt**2 - 11*tt*s25 + 4*s25**2 + 4*s25*s35&
          &)) + 8*ss**4*(s15 - s35)*(s15**2*s25 - s25*(s25 - s35)*(-tt + s35) + s15*(tt*s25&
          & + s25**2 + 6*tt*s35 - 2*s25*s35)) + s25*s35*(-tt + s35)**2*(4*s25**4 + tt**2*(4&
          &*tt - 5*s35)*s35 + s25**3*(-11*tt + 2*s35) + s25**2*(12*tt**2 + 3*tt*s35 - 9*s35&
          &**2) + s25*(-4*tt**3 - 7*tt**2*s35 + 8*tt*s35**2 + 3*s35**3)) + s15**4*(3*s25**3&
          &*(tt - 4*s35) + tt*s35*(4*tt**2 + 13*tt*s35 - 4*s35**2) + s25**2*(-6*tt**2 + 29*&
          &tt*s35 + 11*s35**2) + s25*(3*tt**3 - 18*tt**2*s35 - 39*tt*s35**2 + 20*s35**3)) +&
          & s15**3*(s25**4*(3*tt - 16*s35) - 20*tt**4*s35 + 8*tt*s35**4 + s25**3*(-tt**2 + &
          &22*tt*s35 + 37*s35**2) + s25**2*(-9*tt**3 + 8*tt**2*s35 - 99*tt*s35**2 + 12*s35*&
          &*3) + s25*(7*tt**4 + 17*tt**3*s35 + 27*tt**2*s35**2 + 40*tt*s35**3 - 36*s35**4))&
          & + s15*(-tt + s35)*(4*s25**5*s35 + 16*tt**4*s35**2 - s25**4*(2*tt**2 + 3*tt*s35 &
          &+ 16*s35**2) + s25**3*(6*tt**3 - 16*tt**2*s35 + 24*tt*s35**2 + 11*s35**3) + s25*&
          &s35*(8*tt**4 - 73*tt**3*s35 + 30*tt**2*s35**2 + 6*tt*s35**3 - 8*s35**4) + s25**2&
          &*(-4*tt**4 - 4*tt**3*s35 + 75*tt**2*s35**2 - 70*tt*s35**3 + 16*s35**4)) + s15**2&
          &*(-8*s25**5*s35 + s25**4*(5*tt**2 + 7*tt*s35 + 30*s35**2) - s25**3*(10*tt**3 + 1&
          &5*tt**2*s35 + 35*tt*s35**2 + 27*s35**3) + s25**2*(tt**4 + 101*tt**3*s35 - 112*tt&
          &**2*s35**2 + 134*tt*s35**3 - 38*s35**4) - 2*tt*s35*(-8*tt**4 - 2*tt**3*s35 + 2*t&
          &t**2*s35**2 + 3*tt*s35**3 + 2*s35**4) + s25*(4*tt**5 - 83*tt**4*s35 + 56*tt**3*s&
          &35**2 - 18*tt**2*s35**3 - 26*tt*s35**4 + 28*s35**5)) + ss**3*(-(s15**4*s25) - s2&
          &5*(s25 - s35)*s35*(-tt + s35)*(-24*tt + 20*s25 + 11*s35) - s15**3*(-23*tt*s25 + &
          &13*s25**2 + 48*tt*s35 + 20*s25*s35) + s15*(16*tt*s35**2*(-9*tt + s35) + 4*s25**3&
          &*(-3*tt + 8*s35) + s25*s35*(-48*tt**2 + 241*tt*s35 - 44*s35**2) + s25**2*(24*tt*&
          &*2 - 46*tt*s35 - 19*s35**2)) + s15**2*(-12*s25**3 + 16*tt*s35*(9*tt + 2*s35) + s&
          &25**2*(11*tt + 23*s35) + s25*(24*tt**2 - 217*tt*s35 + 54*s35**2))) + ss**2*(s15*&
          &*4*(2*tt*s25 + s25**2 + 24*tt*s35 - 4*s25*s35) + s25*s35*(-tt + s35)*(16*s25**3 &
          &+ 3*tt*s35*(-10*tt + 9*s35) + s25**2*(-47*tt + 13*s35) + s25*(30*tt**2 + 20*tt*s&
          &35 - 29*s35**2)) + s15**3*(5*s25**3 + 6*s25**2*(-4*tt + s35) - tt*s35*(97*tt + 3&
          &4*s35) + s25*(32*tt**2 + 34*tt*s35 + 52*s35**2)) + s15**2*(4*s25**4 - s25**3*(22&
          &*tt + 13*s35) + s25**2*(5*tt**2 + 177*tt*s35 - 4*s35**2) + tt*s35*(166*tt**2 + 4&
          &0*tt*s35 + 3*s35**2) + s25*(30*tt**3 - 419*tt**2*s35 + 127*tt*s35**2 - 92*s35**3&
          &)) + s15*(4*s25**4*(tt - 5*s35) + s25**3*(-27*tt**2 + 78*tt*s35 - 5*s35**2) + tt&
          &*s35**2*(-166*tt**2 + 57*tt*s35 + 7*s35**2) + 2*s25*s35*(-30*tt**3 + 232*tt**2*s&
          &35 - 115*tt*s35**2 + 22*s35**3) + s25**2*(30*tt**3 - 55*tt**2*s35 - 146*tt*s35**&
          &2 + 26*s35**3))) + ss*(s15**5*(-7*tt + 8*s25)*s35 + s15**4*(6*s25**2*(-tt + 4*s3&
          &5) + tt*s35*(28*tt + 13*s35) + s25*(6*tt**2 - 34*tt*s35 - 39*s35**2)) - s25*(s25&
          & - s35)*s35*(-tt + s35)*(-18*tt**3 + 4*s25**3 + 27*tt**2*s35 - 5*tt*s35**2 - 3*s&
          &35**3 + s25**2*(-27*tt + 22*s35) + s25*(39*tt**2 - 44*tt*s35 + 6*s35**2)) + s15*&
          &*3*(s25**3*(tt + 34*s35) + tt*s35*(-69*tt**2 - 34*tt*s35 + 8*s35**2) - s25**2*(2&
          &3*tt**2 + 8*tt*s35 + 121*s35**2) + s25*(24*tt**3 + 56*tt**2*s35 + 86*tt*s35**2 +&
          & 52*s35**3)) + s15**2*(s25**4*(7*tt + 22*s35) - 2*s25**3*(11*tt**2 + 17*tt*s35 +&
          & 44*s35**2) + tt*s35*(86*tt**3 + 12*tt**2*s35 + 3*tt*s35**2 - 18*s35**3) + s25**&
          &2*(tt**3 + 243*tt**2*s35 - 114*tt*s35**2 + 159*s35**3) + s25*(18*tt**4 - 315*tt*&
          &*3*s35 + 151*tt**2*s35**2 - 88*tt*s35**3 - 22*s35**4)) + s15*(4*s25**5*s35 + s25&
          &**4*(7*tt**2 - 16*tt*s35 - 4*s35**2) + tt*s35**2*(-86*tt**3 + 57*tt**2*s35 + 3*t&
          &t*s35**2 + 4*s35**3) + s25**3*(-23*tt**3 + 67*tt**2*s35 - 44*tt*s35**2 + 38*s35*&
          &*3) + s25**2*(18*tt**4 - 25*tt**3*s35 - 189*tt**2*s35**2 + 192*tt*s35**3 - 71*s3&
          &5**4) + 2*s25*s35*(-18*tt**4 + 169*tt**3*s35 - 148*tt**2*s35**2 + 29*tt*s35**3 +&
          & 2*s35**4)))) + s15*s35*(4*ss**5*tt*(-s15 + s35) + 4*ss**4*tt*(s15 - s35)*(-4*tt&
          & + s15 + 3*s25 + s35) - s15**4*(-tt + s25)*(tt**2 + s25*(-tt + s35)) + s15**3*(-&
          &3*s25**3*(-tt + s35) + tt**2*s35*(-tt + s35) + tt*s25*(2*tt**2 + 3*tt*s35 - 5*s3&
          &5**2) + 5*s25**2*(-tt**2 + s35**2)) + (-tt + s35)**2*(2*s25**5 + 2*tt**4*s35 - 3&
          &*s25**4*(tt + 2*s35) + tt*s25*s35*(-5*tt**2 + 2*tt*s35 + s35**2) + 2*s25**3*(tt*&
          &*2 + 3*tt*s35 + 3*s35**2) - s25**2*(tt**3 - 2*tt**2*s35 + 6*tt*s35**2 + 2*s35**3&
          &)) + s15**2*(tt**5 - 2*tt**2*s35**3 - 4*s25**4*(-tt + s35) + s25**3*(-5*tt**2 - &
          &6*tt*s35 + 12*s35**2) + s25**2*(2*tt**3 + 11*tt**2*s35 - 7*tt*s35**2 - 9*s35**3)&
          & + tt*s25*(-2*tt**3 - 4*tt**2*s35 + tt*s35**2 + 8*s35**3)) + s15*(-2*s25**5*(-tt&
          & + s35) + s25**4*(-tt**2 - 9*tt*s35 + 10*s35**2) + s25**3*(2*tt**3 + 6*tt**2*s35&
          & + 7*tt*s35**2 - 15*s35**3) + tt*s25*(6*tt**4 - 9*tt**3*s35 + 10*tt**2*s35**2 - &
          &2*tt*s35**3 - 5*s35**4) + tt**2*(-2*tt**4 + 3*tt**3*s35 - 2*tt**2*s35**2 + s35**&
          &4) + s25**2*(-7*tt**4 + 8*tt**3*s35 - 16*tt**2*s35**2 + 8*tt*s35**3 + 7*s35**4))&
          & + ss**3*(s15**3*(-3*tt + s25) - 2*s25**3*(-tt + s35) + s25**2*s35*(7*tt + 5*s35&
          &) + s15**2*(10*tt**2 - 9*tt*s25 + 3*s25**2 + 3*tt*s35 - 5*s25*s35) + s25*s35*(-3&
          &8*tt**2 + 17*tt*s35 - 3*s35**2) + tt*s35*(27*tt**2 - 16*tt*s35 + s35**2) + s15*(&
          &2*s25**3 - s25**2*(11*tt + 8*s35) - tt*(27*tt**2 - 6*tt*s35 + s35**2) + s25*(38*&
          &tt**2 - 6*tt*s35 + 7*s35**2))) + ss**2*(s15**4*(tt - s25) + 4*s25**4*(-tt + s35)&
          & + s25**3*(4*tt**2 + tt*s35 - 9*s35**2) + 2*s25**2*s35*(9*tt**2 - 5*tt*s35 + 2*s&
          &35**2) + s25*s35*(-48*tt**3 + 45*tt**2*s35 - 10*tt*s35**2 + s35**3) - tt*s35*(-2&
          &4*tt**3 + 24*tt**2*s35 - 5*tt*s35**2 + s35**3) - s15**3*(4*s25**2 - 4*s25*(2*tt &
          &+ s35) + tt*(6*tt + s35)) + s15**2*(-7*s25**3 + 2*s25**2*(6*tt + 7*s35) + tt*(9*&
          &tt**2 + 6*tt*s35 - 2*s35**2) - s25*(15*tt**2 + 14*tt*s35 + 4*s35**2)) + s15*(-4*&
          &s25**4 + s25**3*(5*tt + 16*s35) + 2*tt*s25*(24*tt**2 - 13*tt*s35 + 7*s35**2) - 2&
          &*s25**2*(13*tt**2 + tt*s35 + 7*s35**2) + tt*(-24*tt**3 + 15*tt**2*s35 - 5*tt*s35&
          &**2 + 3*s35**3))) + ss*(s15**4*(2*tt**2 + s25**2 + s25*(-3*tt + s35)) + s15**3*(&
          &3*s25**3 - s25**2*(10*tt + s35) + s25*(9*tt**2 + 7*tt*s35 - 5*s35**2) + tt*(-3*t&
          &t**2 - 2*tt*s35 + s35**2)) + (-tt + s35)*(-2*s25**5 + 2*s25**4*(3*tt + s35) - tt&
          &**2*s35*(11*tt**2 - 5*tt*s35 + s35**2) + s25**3*(-3*tt**2 - 8*tt*s35 + 5*s35**2)&
          & + s25**2*(tt**3 - 16*tt**2*s35 + 11*tt*s35**2 - 7*s35**3) + s25*s35*(27*tt**3 -&
          & 13*tt**2*s35 + 2*tt*s35**2 + 2*s35**3)) + s15**2*(4*s25**4 - s25**3*(12*tt + 5*&
          &s35) + s25**2*(9*tt**2 + 24*tt*s35 - 8*s35**2) + tt*(4*tt**3 + 3*tt**2*s35 - 2*t&
          &t*s35**2 - 2*s35**3) + s25*(-8*tt**3 - 13*tt**2*s35 - 3*tt*s35**2 + 9*s35**3)) +&
          & s15*(2*s25**5 - 2*s25**4*(2*tt + 3*s35) + s25**3*(4*tt**2 + 19*tt*s35 - 3*s35**&
          &2) + s25**2*(-23*tt**3 + 17*tt**2*s35 - 28*tt*s35**2 + 15*s35**3) + tt*(-11*tt**&
          &4 + 12*tt**3*s35 - 6*tt**2*s35**2 + 3*tt*s35**3 + s35**4) - s25*(-28*tt**4 + 29*&
          &tt**3*s35 - 17*tt**2*s35**2 + tt*s35**3 + 7*s35**4)))))*pvd10(1))/(tt*s15*s25*(4&
          &*me2 - ss - tt + s25)*(tt + s15 - s35)*(s15 + s25 - s35)*s35*(4*me2 - ss - tt + &
          &s35)) + 128*Pi**2*(-(((4*me2**2 + me2*(-2*tt + s15) + s15*(-ss + tt + 2*s15 + s2&
          &5 - s35))*(4*me2 - ss - tt + s35))/(s15*(4*me2 - ss - tt + s25))) + (4*me2**2 - &
          &me2*(2*tt + s35) + s35*(ss - tt - s15 + s35))/s35 + ((4*me2 - ss - tt + s35)*(-2&
          &4*me2**2 - 4*ss**2 - 3*tt**2 + s15**2 + 4*tt*s25 + 2*s15*s25 + 2*tt*s35 - 2*s15*&
          &s35 - 4*s25*s35 + s35**2 + 2*me2*(10*ss + 4*tt - 3*s15 - 5*s25 + s35) + 2*ss*(-3&
          &*tt + 2*s25 + s35)))/((tt + s15 - s35)*(s15 + s25 - s35)) + (24*me2**2 + 2*ss**2&
          & + tt**2 + s15**2 - 2*tt*s25 + me2*(-16*ss - 6*tt + 9*s15 + 10*s25 - 2*s35) + tt&
          &*s35 + 2*s15*s35 + 2*s25*s35 - 2*s35**2 - ss*(-2*tt + 2*s15 + 2*s25 + s35))/s25 &
          &+ ((-4*me2 + ss + tt - s35)*(s15*(-2*ss + 2*s15 + 2*s25 - s35) + me2*(-2*tt + 3*&
          &s15 + 3*s35)))/(tt*s15) - ((4*me2 - ss - tt + s35)*((-2*ss + s15)*s35 + me2*(2*t&
          &t + 3*s15 + 3*s35)))/(tt*s35) + ((4*me2 - ss - tt + s35)*(-24*me2**2 - 2*ss**2 -&
          & tt**2 + tt*s15 + s15**2 + 2*tt*s25 + s15*s25 + ss*(-2*tt + s15 + 2*s25) - 2*s15&
          &*s35 - 2*s25*s35 + s35**2 + me2*(16*ss + 6*tt - 8*s15 - 10*s25 + 3*s35)))/((4*me&
          &2 - ss - tt + s25)*(s15 + s25 - s35)) + ((4*me2 - ss - tt + s35)*(24*me2**2 + 4*&
          &ss**2 + 3*tt**2 + s15**2 - 4*tt*s25 - 2*tt*s35 + 2*s15*s35 + 4*s25*s35 - s35**2 &
          &- 2*ss*(-3*tt + s15 + 2*s25 + 2*s35) + me2*(-20*ss - 8*tt + 12*s15 + 10*s25 + 4*&
          &s35)))/(s25*(tt + s15 - s35)))*pvd10(2) + 128*Pi**2*(-(((4*me2**2 + me2*(-2*tt +&
          & s15) + s15*(-ss + tt + 2*s15 + s25 - s35))*(4*me2 - ss - tt + 2*s35))/(s15*(4*m&
          &e2 - ss - tt + s25))) + ((4*me2 - ss - tt + 2*s35)*(4*me2**2 - me2*(2*tt + s35) &
          &+ s35*(ss - tt - s15 + s35)))/(s35*(4*me2 - ss - tt + s35)) + ((4*me2 - ss - tt &
          &+ 2*s35)*(-24*me2**2 - 4*ss**2 - 3*tt**2 + s15**2 + 4*tt*s25 + 2*s15*s25 + 2*tt*&
          &s35 - 2*s15*s35 - 4*s25*s35 + s35**2 + 2*me2*(10*ss + 4*tt - 3*s15 - 5*s25 + s35&
          &) + 2*ss*(-3*tt + 2*s25 + s35)))/((tt + s15 - s35)*(s15 + s25 - s35)) + ((4*me2 &
          &- ss - tt + 2*s35)*(24*me2**2 + 2*ss**2 + tt**2 + s15**2 - 2*tt*s25 + me2*(-16*s&
          &s - 6*tt + 9*s15 + 10*s25 - 2*s35) + tt*s35 + 2*s15*s35 + 2*s25*s35 - 2*s35**2 -&
          & ss*(-2*tt + 2*s15 + 2*s25 + s35)))/(s25*(4*me2 - ss - tt + s35)) + ((-4*me2 + s&
          &s + tt - 2*s35)*(s15*(-2*ss + 2*s15 + 2*s25 - s35) + me2*(-2*tt + 3*s15 + 3*s35)&
          &))/(tt*s15) - ((4*me2 - ss - tt + 2*s35)*((-2*ss + s15)*s35 + me2*(2*tt + 3*s15 &
          &+ 3*s35)))/(tt*s35) + ((4*me2 - ss - tt + 2*s35)*(-24*me2**2 - 2*ss**2 - tt**2 +&
          & tt*s15 + s15**2 + 2*tt*s25 + s15*s25 + ss*(-2*tt + s15 + 2*s25) - 2*s15*s35 - 2&
          &*s25*s35 + s35**2 + me2*(16*ss + 6*tt - 8*s15 - 10*s25 + 3*s35)))/((4*me2 - ss -&
          & tt + s25)*(s15 + s25 - s35)) + ((4*me2 - ss - tt + 2*s35)*(24*me2**2 + 4*ss**2 &
          &+ 3*tt**2 + s15**2 - 4*tt*s25 - 2*tt*s35 + 2*s15*s35 + 4*s25*s35 - s35**2 - 2*ss&
          &*(-3*tt + s15 + 2*s25 + 2*s35) + me2*(-20*ss - 8*tt + 12*s15 + 10*s25 + 4*s35)))&
          &/(s25*(tt + s15 - s35)))*pvd10(3) + 128*Pi**2*(s25 - s35)*((4*me2**2 + me2*(-2*t&
          &t + s15) + s15*(-ss + tt + 2*s15 + s25 - s35))/(s15*(4*me2 - ss - tt + s25)) + (&
          &-4*me2**2 + (-ss + tt + s15 - s35)*s35 + me2*(2*tt + s35))/(s35*(4*me2 - ss - tt&
          & + s35)) - (-24*me2**2 - 4*ss**2 - 3*tt**2 + s15**2 + 4*tt*s25 + 2*s15*s25 + 2*t&
          &t*s35 - 2*s15*s35 - 4*s25*s35 + s35**2 + 2*me2*(10*ss + 4*tt - 3*s15 - 5*s25 + s&
          &35) + 2*ss*(-3*tt + 2*s25 + s35))/((tt + s15 - s35)*(s15 + s25 - s35)) - (24*me2&
          &**2 + 2*ss**2 + tt**2 + s15**2 - 2*tt*s25 + me2*(-16*ss - 6*tt + 9*s15 + 10*s25 &
          &- 2*s35) + tt*s35 + 2*s15*s35 + 2*s25*s35 - 2*s35**2 - ss*(-2*tt + 2*s15 + 2*s25&
          & + s35))/(s25*(4*me2 - ss - tt + s35)) + (s15*(-2*ss + 2*s15 + 2*s25 - s35) + me&
          &2*(-2*tt + 3*s15 + 3*s35))/(tt*s15) + ((-2*ss + s15)*s35 + me2*(2*tt + 3*s15 + 3&
          &*s35))/(tt*s35) - (-24*me2**2 - 2*ss**2 - tt**2 + tt*s15 + s15**2 + 2*tt*s25 + s&
          &15*s25 + ss*(-2*tt + s15 + 2*s25) - 2*s15*s35 - 2*s25*s35 + s35**2 + me2*(16*ss &
          &+ 6*tt - 8*s15 - 10*s25 + 3*s35))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) - &
          &(24*me2**2 + 4*ss**2 + 3*tt**2 + s15**2 - 4*tt*s25 - 2*tt*s35 + 2*s15*s35 + 4*s2&
          &5*s35 - s35**2 - 2*ss*(-3*tt + s15 + 2*s25 + 2*s35) + me2*(-20*ss - 8*tt + 12*s1&
          &5 + 10*s25 + 4*s35))/(s25*(tt + s15 - s35)))*pvd10(4) + (512*Pi**2*(8*me2**2*(s1&
          &5 - s35) + ss**2*(s15 - s35) - ss*(-tt + s15 + s25)*(s15 - s35) - 2*me2*(tt*s15 &
          &- 2*s15**2 - s15*s25 + s25**2 + 3*ss*(s15 - s35) - tt*s35 + 2*s15*s35) + (s15 + &
          &s25 - s35)*(-(tt*s15) + tt*s25 + s15*s25 - s25*s35))*pvd10(5))/(s25*(4*me2 - ss &
          &- tt + s25)*(s15 + s25 - s35)) - (128*Pi**2*(4*me2 - ss - tt + s35)*(-8*me2**2*(&
          &s15 - s35) + ss*(-tt + s15 + s25)*(s15 - s35) - (s15*(-tt + s25) + s25*(tt - s35&
          &))*(s15 + s25 - s35) + ss**2*(-s15 + s35) + 2*me2*(-2*s15**2 + s25**2 + 3*ss*(s1&
          &5 - s35) - tt*s35 + s15*(tt - s25 + 2*s35)))*pvd10(6))/(s25*(4*me2 - ss - tt + s&
          &25)*(s15 + s25 - s35)) + (256*Pi**2*(4*me2 - ss - tt + 2*s35)*(8*me2**2*(s15 - s&
          &35) + ss**2*(s15 - s35) - ss*(-tt + s15 + s25)*(s15 - s35) - 2*me2*(tt*s15 - 2*s&
          &15**2 - s15*s25 + s25**2 + 3*ss*(s15 - s35) - tt*s35 + 2*s15*s35) + (s15 + s25 -&
          & s35)*(-(tt*s15) + tt*s25 + s15*s25 - s25*s35))*pvd10(7))/(s25*(4*me2 - ss - tt &
          &+ s25)*(s15 + s25 - s35)) + (256*Pi**2*(s25 - s35)*(-8*me2**2*(s15 - s35) + ss*(&
          &-tt + s15 + s25)*(s15 - s35) - (s15*(-tt + s25) + s25*(tt - s35))*(s15 + s25 - s&
          &35) + ss**2*(-s15 + s35) + 2*me2*(-2*s15**2 + s25**2 + 3*ss*(s15 - s35) - tt*s35&
          & + s15*(tt - s25 + 2*s35)))*pvd10(8))/(s25*(4*me2 - ss - tt + s25)*(s15 + s25 - &
          &s35)) + (128*Pi**2*(-4*me2 + ss + tt - 2*s35)**2*(8*me2**2*(s15 - s35) + ss**2*(&
          &s15 - s35) - ss*(-tt + s15 + s25)*(s15 - s35) - 2*me2*(tt*s15 - 2*s15**2 - s15*s&
          &25 + s25**2 + 3*ss*(s15 - s35) - tt*s35 + 2*s15*s35) + (s15 + s25 - s35)*(-(tt*s&
          &15) + tt*s25 + s15*s25 - s25*s35))*pvd10(9))/(s25*(4*me2 - ss - tt + s25)*(s15 +&
          & s25 - s35)*(4*me2 - ss - tt + s35)) + (256*Pi**2*(s25 - s35)*(4*me2 - ss - tt +&
          & 2*s35)*(-8*me2**2*(s15 - s35) + ss*(-tt + s15 + s25)*(s15 - s35) - (s15*(-tt + &
          &s25) + s25*(tt - s35))*(s15 + s25 - s35) + ss**2*(-s15 + s35) + 2*me2*(-2*s15**2&
          & + s25**2 + 3*ss*(s15 - s35) - tt*s35 + s15*(tt - s25 + 2*s35)))*pvd10(10))/(s25&
          &*(4*me2 - ss - tt + s25)*(s15 + s25 - s35)*(4*me2 - ss - tt + s35)) + (128*Pi**2&
          &*(s25 - s35)**2*(8*me2**2*(s15 - s35) + ss**2*(s15 - s35) - ss*(-tt + s15 + s25)&
          &*(s15 - s35) - 2*me2*(tt*s15 - 2*s15**2 - s15*s25 + s25**2 + 3*ss*(s15 - s35) - &
          &tt*s35 + 2*s15*s35) + (s15 + s25 - s35)*(-(tt*s15) + tt*s25 + s15*s25 - s25*s35)&
          &)*pvd10(11))/(s25*(4*me2 - ss - tt + s25)*(s15 + s25 - s35)*(4*me2 - ss - tt + s&
          &35)) + 128*Pi**2*((40*me2**3 + me2**2*(-48*ss - 48*tt + 4*s15 + 42*s25 + 34*s35)&
          & + me2*(18*ss**2 + 18*tt**2 - 25*tt*s25 - s15*s25 + 6*s25**2 + ss*(36*tt - 24*s2&
          &5 - 22*s35) - 16*tt*s35 + 8*s15*s35 + 19*s25*s35) - (ss + tt - s25)*(2*ss**2 + 2&
          &*tt**2 - 2*tt*s25 - s15*s25 - s25**2 - 2*tt*s35 + 2*s15*s35 + 3*s25*s35 - ss*(-4&
          &*tt + s25 + 4*s35)))/(s25*(tt + s15 - s35)) + (80*me2**3 + me2**2*(-72*ss - 80*t&
          &t + 56*s25 + 44*s35) + me2*(20*ss**2 + 24*tt**2 + 2*tt*s15 - 31*tt*s25 - 3*s15*s&
          &25 + 7*s25**2 + ss*(44*tt + 2*s15 - 27*s25 - 25*s35) - 19*tt*s35 + 8*s15*s35 + 2&
          &0*s25*s35 + 2*s35**2) - (ss + tt - s25)*(2*ss**2 + 2*tt**2 - 2*tt*s25 - s15*s25 &
          &- s25**2 - 2*tt*s35 + 2*s15*s35 + 3*s25*s35 - ss*(-4*tt + s25 + 4*s35)))/(s25*(4&
          &*me2 - ss - tt + s35)) + (40*me2**3 - (ss + tt - s25)*s35*(-ss - 2*tt + s15 + s2&
          &5 + s35) + me2**2*(-28*ss - 28*tt + 22*s25 + 14*s35) + me2*(4*ss**2 + 4*tt**2 - &
          &6*tt*s25 - 11*tt*s35 + 2*s15*s35 + 11*s25*s35 + s35**2 - 2*ss*(-4*tt + 2*s25 + 5&
          &*s35)))/(tt*s35) + (32*me2**3 - 2*me2**2*(24*ss + 16*tt - 11*s15 - 6*s25 + 8*s35&
          &) - (ss + tt - s25)*(4*ss**2 + 3*tt**2 + ss*(6*tt - 3*s15 - 4*s25) - 2*tt*s25 + &
          &2*s25**2 + s15*(-2*tt + s25 - s35) - 2*tt*s35 - 2*s25*s35 + s35**2) + me2*(24*ss&
          &**2 + 14*tt**2 - 17*tt*s25 + 8*s25**2 + s15*(-16*tt + 13*s25 - 5*s35) + 2*tt*s35&
          & - 10*s25*s35 + ss*(36*tt - 18*s15 - 28*s25 + 8*s35)))/(tt*s15) + (48*me2**3 - 2&
          &*me2**2*(28*ss + 28*tt - 14*s15 - 9*s25 + 7*s35) - (ss + tt - s25)*(2*ss**2 + tt&
          &**2 - 3*ss*s15 + s25**2 + s15*(-2*tt + s25 - s35) - 2*s25*s35 + 2*ss*(tt - s25 +&
          & s35)) + me2*(20*ss**2 + 14*tt**2 - 12*tt*s25 + 8*s25**2 + 3*s15*(-6*tt + 5*s25 &
          &- 2*s35) + 3*tt*s35 - 14*s25*s35 + s35**2 + ss*(30*tt - 20*s15 - 23*s25 + 12*s35&
          &)))/(s15*(4*me2 - ss - tt + s25)) + (-112*me2**3 + 2*me2**2*(62*ss + 54*tt - 13*&
          &s15 - 38*s25 - 16*s35) + (ss + tt - s25)*(6*ss**2 + 5*tt**2 - 3*ss*s15 - 2*tt*s1&
          &5 - 4*tt*s25 + s25**2 - 6*tt*s35 + 2*s15*s35 + 2*s25*s35 + 2*s35**2 - 5*ss*(-2*t&
          &t + s25 + s35)) - me2*(46*ss**2 + 36*tt**2 - 16*tt*s15 - 48*tt*s25 + 12*s15*s25 &
          &+ 14*s25**2 - 25*tt*s35 + 5*s15*s35 + 20*s25*s35 + s35**2 - 2*ss*(-40*tt + 9*s15&
          & + 28*s25 + 12*s35)))/((tt + s15 - s35)*(s15 + s25 - s35)) + (80*me2**3 - 4*me2*&
          &*2*(8*ss + 10*tt - 5*s25 - 12*s35) - (ss + tt - s25)*s35*(-ss - 2*tt + s15 + s25&
          & + s35) + me2*(4*ss**2 + 4*tt**2 - 3*tt*s25 - 3*s25**2 - 21*tt*s35 + 5*s15*s35 +&
          & 18*s25*s35 + 2*s35**2 - ss*(-8*tt + s25 + 17*s35)))/(s35*(4*me2 - ss - tt + s35&
          &)) + (-208*me2**3 + 2*me2**2*(80*ss + 88*tt - 14*s15 - 47*s25 - 39*s35) + (ss + &
          &tt - s25)*(4*ss**2 - 3*ss*(-2*tt + s15 + s25 + s35) + (-tt + s35)*(-3*tt + 2*s15&
          & + 2*s25 + s35)) - me2*(44*ss**2 + 42*tt**2 - 16*tt*s15 - 46*tt*s25 + 12*s15*s25&
          & + 12*s25**2 - 37*tt*s35 + 7*s15*s35 + 24*s25*s35 + 5*s35**2 - ss*(-82*tt + 18*s&
          &15 + 51*s25 + 30*s35)))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)))*pvd12(1) + &
          &128*Pi**2*(4*me2 - ss - tt + s35)*((40*me2**2 + (2*ss + 2*tt - s15)*(ss + tt - s&
          &25) + 2*me2*(-8*ss - 10*tt + s15 + 4*s25))/(s15*(4*me2 - ss - tt + s25)) + (-56*&
          &me2**2 + 2*me2*(12*ss + 14*tt - s15 - 9*s25 - 5*s35) - (ss + tt - s25)*(3*ss + 3&
          &*tt - s15 - s25 - 2*s35))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) - ((2*me2 &
          &- ss - tt + s25)*(14*me2 - 3*ss - 3*tt + s15 + s25 + 2*s35))/((tt + s15 - s35)*(&
          &s15 + s25 - s35)) + (20*me2**2 + (2*ss + 2*tt - s15)*(ss + tt - s25) - 2*me2*(7*&
          &ss + 7*tt - s15 - 4*s25 + s35))/(tt*s15) + (4*me2**2 - (ss + tt - s25)*s35 + me2&
          &*(-2*ss - 2*tt + 3*s25 + 2*s35))/(tt*s35) + (4*me2**2 + (ss + tt - s25)*(ss + tt&
          & - s25 - s35) + me2*(-4*ss - 4*tt + 5*s25 + 4*s35))/(s25*(tt + s15 - s35)) + (8*&
          &me2**2 - (ss + tt - s25)*s35 + me2*(-2*ss - 2*tt + 3*s25 + 5*s35))/(s35*(4*me2 -&
          & ss - tt + s35)) + (8*me2**2 + (ss + tt - s25)*(ss + tt - s25 - s35) + me2*(-6*s&
          &s - 6*tt + 7*s25 + 5*s35))/(s25*(4*me2 - ss - tt + s35)))*pvd12(2) + 128*Pi**2*(&
          &2*me2 - tt + s35)*((40*me2**2 + (2*ss + 2*tt - s15)*(ss + tt - s25) + 2*me2*(-8*&
          &ss - 10*tt + s15 + 4*s25))/(s15*(4*me2 - ss - tt + s25)) + (-56*me2**2 + 2*me2*(&
          &12*ss + 14*tt - s15 - 9*s25 - 5*s35) - (ss + tt - s25)*(3*ss + 3*tt - s15 - s25 &
          &- 2*s35))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) - ((2*me2 - ss - tt + s25)&
          &*(14*me2 - 3*ss - 3*tt + s15 + s25 + 2*s35))/((tt + s15 - s35)*(s15 + s25 - s35)&
          &) + (20*me2**2 + (2*ss + 2*tt - s15)*(ss + tt - s25) - 2*me2*(7*ss + 7*tt - s15 &
          &- 4*s25 + s35))/(tt*s15) + (4*me2**2 - (ss + tt - s25)*s35 + me2*(-2*ss - 2*tt +&
          & 3*s25 + 2*s35))/(tt*s35) + (4*me2**2 + (ss + tt - s25)*(ss + tt - s25 - s35) + &
          &me2*(-4*ss - 4*tt + 5*s25 + 4*s35))/(s25*(tt + s15 - s35)) + (8*me2**2 - (ss + t&
          &t - s25)*s35 + me2*(-2*ss - 2*tt + 3*s25 + 5*s35))/(s35*(4*me2 - ss - tt + s35))&
          & + (8*me2**2 + (ss + tt - s25)*(ss + tt - s25 - s35) + me2*(-6*ss - 6*tt + 7*s25&
          & + 5*s35))/(s25*(4*me2 - ss - tt + s35)))*pvd12(3) + 128*Pi**2*((40*me2**2 + (2*&
          &ss + 2*tt - s15)*(ss + tt - s25) + 2*me2*(-8*ss - 10*tt + s15 + 4*s25))/(4*me2 -&
          & ss - tt + s25) - (s15*(2*me2 - ss - tt + s25)*(14*me2 - 3*ss - 3*tt + s15 + s25&
          & + 2*s35))/((tt + s15 - s35)*(s15 + s25 - s35)) + (20*me2**2 + (2*ss + 2*tt - s1&
          &5)*(ss + tt - s25) - 2*me2*(7*ss + 7*tt - s15 - 4*s25 + s35))/tt + (s15*(4*me2**&
          &2 - (ss + tt - s25)*s35 + me2*(-2*ss - 2*tt + 3*s25 + 2*s35)))/(tt*s35) + (s15*(&
          &4*me2**2 + (ss + tt - s25)*(ss + tt - s25 - s35) + me2*(-4*ss - 4*tt + 5*s25 + 4&
          &*s35)))/(s25*(tt + s15 - s35)) + (s15*(8*me2**2 - (ss + tt - s25)*s35 + me2*(-2*&
          &ss - 2*tt + 3*s25 + 5*s35)))/(s35*(4*me2 - ss - tt + s35)) + (s15*(8*me2**2 + (s&
          &s + tt - s25)*(ss + tt - s25 - s35) + me2*(-6*ss - 6*tt + 7*s25 + 5*s35)))/(s25*&
          &(4*me2 - ss - tt + s35)) - (s15*(56*me2**2 + (ss + tt - s25)*(3*ss + 3*tt - s15 &
          &- s25 - 2*s35) + 2*me2*(-12*ss - 14*tt + s15 + 9*s25 + 5*s35)))/((4*me2 - ss - t&
          &t + s25)*(s15 + s25 - s35)))*pvd12(4) + 512*me2*Pi**2*(1/s15 + (2*me2 - ss - tt &
          &+ s25)/(tt*s15) - 1/(s15 + s25 - s35) + (-2*me2 + ss + tt - s25)/((tt + s15 - s3&
          &5)*(s15 + s25 - s35)))*pvd12(5) + (128*Pi**2*(-4*me2 + ss + tt - s35)**2*((s15*(&
          &-tt + s25) + s25*(tt - s35))*(s15 + s25 - s35) + 2*me2*(s15**2 + s15*(s25 - 2*s3&
          &5) - (s25 - s35)*(-tt + s35)) - ss*(s15**2 + s15*(s25 - 2*s35) - (s25 - s35)*(-t&
          &t + s35)))*pvd12(6))/(tt*s15*(tt + s15 - s35)*(s15 + s25 - s35)) + (256*Pi**2*(2&
          &*me2 - tt + s35)*(4*me2 - ss - tt + s35)*((s15*(-tt + s25) + s25*(tt - s35))*(s1&
          &5 + s25 - s35) + 2*me2*(s15**2 + s15*(s25 - 2*s35) - (s25 - s35)*(-tt + s35)) - &
          &ss*(s15**2 + s15*(s25 - 2*s35) - (s25 - s35)*(-tt + s35)))*pvd12(7))/(tt*s15*(tt&
          & + s15 - s35)*(s15 + s25 - s35)) + 256*Pi**2*((2*me2 - ss + s25)/tt - (s15*(2*me&
          &2 - ss + s15 + s25 - s35))/((tt + s15 - s35)*(s15 + s25 - s35)))*(4*me2 - ss - t&
          &t + s35)*pvd12(8) + (128*Pi**2*(2*me2 - tt + s35)**2*((s15*(-tt + s25) + s25*(tt&
          & - s35))*(s15 + s25 - s35) + 2*me2*(s15**2 + s15*(s25 - 2*s35) - (s25 - s35)*(-t&
          &t + s35)) - ss*(s15**2 + s15*(s25 - 2*s35) - (s25 - s35)*(-tt + s35)))*pvd12(9))&
          &/(tt*s15*(tt + s15 - s35)*(s15 + s25 - s35)) + 256*Pi**2*((2*me2 - ss + s25)/tt &
          &- (s15*(2*me2 - ss + s15 + s25 - s35))/((tt + s15 - s35)*(s15 + s25 - s35)))*(2*&
          &me2 - tt + s35)*pvd12(10) + 128*Pi**2*s15*((2*me2 - ss + s25)/tt - (s15*(2*me2 -&
          & ss + s15 + s25 - s35))/((tt + s15 - s35)*(s15 + s25 - s35)))*pvd12(11) + 128*Pi&
          &**2*((-16*me2**3 + 2*me2**2*(8*ss + 9*tt + 4*s15 - 8*s35) + (ss + tt - s25)*(tt*&
          &s25 + ss*(tt + s15 + 2*s25 - 2*s35) - tt*s35 + s15*s35) + me2*(-4*ss**2 - 5*tt**&
          &2 - 4*tt*s15 + tt*s25 + 4*s15*s25 + 4*s25**2 - 2*ss*(6*tt + 3*s15 + 2*s25 - 6*s3&
          &5) + 9*tt*s35 - 8*s25*s35))/(tt*s35) + (-24*me2**3 - (ss + tt - s25)*(ss - s15 -&
          & s25)*(tt + s15 - s25) + 2*me2**2*(8*ss - 2*tt - 9*s15 - 7*s35) + me2*(-2*ss**2 &
          &+ 5*ss*tt + tt**2 + 11*ss*s15 - 5*s15**2 - 8*tt*s25 - 8*s15*s25 + 2*s25**2 + 2*t&
          &t*s35 + 2*s15*s35 + 4*s25*s35 - 4*s35**2))/(s25*(4*me2 - ss - tt + s35)) + (-24*&
          &me2**3 + 2*me2**2*(8*ss + 2*tt - s15 - 13*s35) - me2*(2*ss**2 - tt**2 + 2*tt*s15&
          & + 2*tt*s25 - 5*s15*s25 - 6*s25**2 + ss*(7*tt + 7*s15 + 6*s25 - 20*s35) - 16*tt*&
          &s35 + 3*s15*s35 + 16*s25*s35 + 2*s35**2) + (ss + tt - s25)*(ss*(tt + s15 + s25 -&
          & 3*s35) + s35*(-2*tt + s15 + s25 + s35)))/(s35*(4*me2 - ss - tt + s35)) - (16*me&
          &2**3 + ss**2*(tt + s15 + s35) + 2*me2**2*(-8*ss + tt + 9*s15 + 8*s25 + 3*s35) + &
          &(-tt + s25)*(tt*s15 + s15**2 - s25**2 - tt*s35 + s25*s35 + s35**2) - ss*(-tt**2 &
          &+ s15**2 + tt*s25 + s15*s25 - s25**2 - 2*tt*s35 + 2*s25*s35 + s35**2) + me2*(4*s&
          &s**2 + tt**2 + 4*s15**2 - 3*tt*s25 + 5*s15*s25 + 2*s25**2 - 4*tt*s35 + 2*s15*s35&
          & + 4*s25*s35 + 2*s35**2 - 4*ss*(2*s15 + 2*s25 + s35)))/(s25*(tt + s15 - s35)) + &
          &(48*me2**3 + me2**2*(-64*ss - 58*tt + 18*s15 + 40*s25 + 22*s35) - (ss + tt - s25&
          &)*(4*ss**2 - 3*ss*s15 - 4*ss*(-2*tt + s25 + s35) + (-2*tt + s25 + s35)**2 + s15*&
          &(-3*tt + s25 + 2*s35)) + me2*(28*ss**2 + 25*tt**2 + s15**2 - 33*tt*s25 + 10*s25*&
          &*2 - 20*tt*s35 + 14*s25*s35 + 2*s35**2 + s15*(-10*tt + 9*s25 + s35) - 6*ss*(-9*t&
          &t + 2*s15 + 6*s25 + 3*s35)))/((tt + s15 - s35)*(s15 + s25 - s35)) + (16*me2**3 +&
          & me2**2*(-16*ss + 10*tt + 8*s15 + 8*s25 + 8*s35) - (ss + tt - s25)*(-tt**2 + 3*t&
          &t*s15 + s15**2 + 2*tt*s25 - s15*s25 - 2*s25**2 + ss*(-2*tt + s15 + 2*s25 - 2*s35&
          &) - tt*s35 + 2*s25*s35) + me2*(4*ss**2 - 5*tt**2 + 4*tt*s15 + 3*s15**2 + 7*tt*s2&
          &5 - 4*s25**2 - 3*tt*s35 + 3*s15*s35 + 8*s25*s35 - 2*ss*(3*tt + s15 + 4*s35)))/(t&
          &t*s15) + (24*me2**3 - (ss + tt - s25)*(-tt**2 + s15**2 + tt*s25 - s25**2 + s25*s&
          &35 + s15*(tt + s35) - ss*(2*tt + s15 - s25 + s35)) + 2*me2**2*(-8*ss + 7*s15 + 2&
          &*(5*tt + s25 + 3*s35)) + me2*(2*ss**2 - 10*tt**2 - tt*s15 + 2*s15**2 + 10*tt*s25&
          & - 6*s25**2 - 2*tt*s35 + 5*s15*s35 + 8*s25*s35 - ss*(12*tt + 3*s15 - 4*s25 + 8*s&
          &35)))/(s15*(4*me2 - ss - tt + s25)) + (72*me2**3 - (ss + tt - s25)*(ss + tt - s3&
          &5)*(2*ss + 2*tt - s15 - s25 - s35) + me2**2*(-72*ss - 76*tt + 22*s15 + 36*s25 + &
          &44*s35) + me2*(22*ss**2 + 22*tt**2 + 3*s15**2 - 19*tt*s25 + 6*s25**2 + s15*(-7*t&
          &t + 11*s25 - s35) - 26*tt*s35 + 11*s25*s35 + 6*s35**2 - ss*(-44*tt + 13*s15 + 26&
          &*s25 + 19*s35)))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)))*pvd3(1) + 128*Pi**&
          &2*(-2*me2 + ss + tt - s35)*((-4*me2**2 + me2*(4*ss - 3*s15 - 4*s25) - (ss + tt -&
          & s25)*(ss - s15 - s25))/(tt*s15) - (2*me2 - ss + s15 + s25)/s15 + (2*me2*(me2 + &
          &tt + s15))/(s25*(tt + s15 - s35)) + (-5*me2 + ss + tt - s35)/(s15 + s25 - s35) +&
          & (-10*me2**2 + me2*(6*ss + 6*tt - 4*s25 - 2*s35) - (ss + tt - s25)*(ss + tt - s3&
          &5))/((tt + s15 - s35)*(s15 + s25 - s35)) + (2*me2*(3*me2 - ss + s35))/(s25*(4*me&
          &2 - ss - tt + s35)) + (6*me2**2 + (ss + tt - s25)*(ss - s35) + me2*(-6*ss - 2*tt&
          & + s15 + 4*s25 + 2*s35))/(s35*(4*me2 - ss - tt + s35)) + (4*me2**2 + (ss + tt - &
          &s25)*(ss - s35) - me2*(4*ss + s15 - 2*(-tt + s25 + s35)))/(tt*s35))*pvd3(2) + 12&
          &8*Pi**2*(-tt + s35)*((2*me2 - ss + s15 + s25)/s15 + (4*me2**2 + (ss + tt - s25)*&
          &(ss - s15 - s25) + me2*(-4*ss + 3*s15 + 4*s25))/(tt*s15) - (2*me2*(me2 + tt + s1&
          &5))/(s25*(tt + s15 - s35)) + (-6*me2**2 + me2*(6*ss + 2*tt - s15 - 4*s25 - 2*s35&
          &) - (ss + tt - s25)*(ss - s35))/(s35*(4*me2 - ss - tt + s35)) - (2*me2*(3*me2 - &
          &ss + s35))/(s25*(4*me2 - ss - tt + s35)) + (5*me2 - ss - tt + s35)/(s15 + s25 - &
          &s35) + (10*me2**2 + (ss + tt - s25)*(ss + tt - s35) + me2*(-6*ss - 6*tt + 4*s25 &
          &+ 2*s35))/((tt + s15 - s35)*(s15 + s25 - s35)) + (-4*me2**2 - (ss + tt - s25)*(s&
          &s - s35) + me2*(4*ss + s15 - 2*(-tt + s25 + s35)))/(tt*s35))*pvd3(3) + 256*Pi**2&
          &*(me2 - s15/2.)*((-4*me2**2 + me2*(4*ss - 3*s15 - 4*s25) - (ss + tt - s25)*(ss -&
          & s15 - s25))/(tt*s15) - (2*me2 - ss + s15 + s25)/s15 + (2*me2*(me2 + tt + s15))/&
          &(s25*(tt + s15 - s35)) + (-5*me2 + ss + tt - s35)/(s15 + s25 - s35) + (-10*me2**&
          &2 + me2*(6*ss + 6*tt - 4*s25 - 2*s35) - (ss + tt - s25)*(ss + tt - s35))/((tt + &
          &s15 - s35)*(s15 + s25 - s35)) + (2*me2*(3*me2 - ss + s35))/(s25*(4*me2 - ss - tt&
          & + s35)) + (6*me2**2 + (ss + tt - s25)*(ss - s35) + me2*(-6*ss - 2*tt + s15 + 4*&
          &s25 + 2*s35))/(s35*(4*me2 - ss - tt + s35)) + (4*me2**2 + (ss + tt - s25)*(ss - &
          &s35) - me2*(4*ss + s15 - 2*(-tt + s25 + s35)))/(tt*s35))*pvd3(4) + 512*Pi**2*((-&
          &4*me2**2 + me2*(4*ss - 3*s15 - 4*s25) - (ss + tt - s25)*(ss - s15 - s25))/(tt*s1&
          &5) - (2*me2 - ss + s15 + s25)/s15 + (2*me2*(me2 + tt + s15))/(s25*(tt + s15 - s3&
          &5)) + (-5*me2 + ss + tt - s35)/(s15 + s25 - s35) + (-10*me2**2 + me2*(6*ss + 6*t&
          &t - 4*s25 - 2*s35) - (ss + tt - s25)*(ss + tt - s35))/((tt + s15 - s35)*(s15 + s&
          &25 - s35)) + (2*me2*(3*me2 - ss + s35))/(s25*(4*me2 - ss - tt + s35)) + (6*me2**&
          &2 + (ss + tt - s25)*(ss - s35) + me2*(-6*ss - 2*tt + s15 + 4*s25 + 2*s35))/(s35*&
          &(4*me2 - ss - tt + s35)) + (4*me2**2 + (ss + tt - s25)*(ss - s35) - me2*(4*ss + &
          &s15 - 2*(-tt + s25 + s35)))/(tt*s35))*pvd3(5) + 128*me2*Pi**2*((-4*me2**2 + me2*&
          &(4*ss - 3*s15 - 4*s25) - (ss + tt - s25)*(ss - s15 - s25))/(tt*s15) - (2*me2 - s&
          &s + s15 + s25)/s15 + (2*me2*(me2 + tt + s15))/(s25*(tt + s15 - s35)) + (-5*me2 +&
          & ss + tt - s35)/(s15 + s25 - s35) + (-10*me2**2 + me2*(6*ss + 6*tt - 4*s25 - 2*s&
          &35) - (ss + tt - s25)*(ss + tt - s35))/((tt + s15 - s35)*(s15 + s25 - s35)) + (2&
          &*me2*(3*me2 - ss + s35))/(s25*(4*me2 - ss - tt + s35)) + (6*me2**2 + (ss + tt - &
          &s25)*(ss - s35) + me2*(-6*ss - 2*tt + s15 + 4*s25 + 2*s35))/(s35*(4*me2 - ss - t&
          &t + s35)) + (4*me2**2 + (ss + tt - s25)*(ss - s35) - me2*(4*ss + s15 - 2*(-tt + &
          &s25 + s35)))/(tt*s35))*pvd3(6) + 128*Pi**2*(-((tt*(2*me2 - ss + s15 + s25))/s15)&
          & - (4*me2**2 + (ss + tt - s25)*(ss - s15 - s25) + me2*(-4*ss + 3*s15 + 4*s25))/s&
          &15 + (2*me2*tt*(me2 + tt + s15))/(s25*(tt + s15 - s35)) + (2*me2*tt*(3*me2 - ss &
          &+ s35))/(s25*(4*me2 - ss - tt + s35)) - (tt*(5*me2 - ss - tt + s35))/(s15 + s25 &
          &- s35) - (tt*(10*me2**2 + (ss + tt - s25)*(ss + tt - s35) + me2*(-6*ss - 6*tt + &
          &4*s25 + 2*s35)))/((tt + s15 - s35)*(s15 + s25 - s35)) + (tt*(6*me2**2 + (ss + tt&
          & - s25)*(ss - s35) + me2*(-6*ss - 2*tt + s15 + 4*s25 + 2*s35)))/(s35*(4*me2 - ss&
          & - tt + s35)) + (4*me2**2 + (ss + tt - s25)*(ss - s35) - me2*(4*ss + s15 - 2*(-t&
          &t + s25 + s35)))/s35)*pvd3(7) + 128*Pi**2*(-2*me2 + ss + tt - s15 - s25)*((-4*me&
          &2**2 + me2*(4*ss - 3*s15 - 4*s25) - (ss + tt - s25)*(ss - s15 - s25))/(tt*s15) -&
          & (2*me2 - ss + s15 + s25)/s15 + (2*me2*(me2 + tt + s15))/(s25*(tt + s15 - s35)) &
          &+ (-5*me2 + ss + tt - s35)/(s15 + s25 - s35) + (-10*me2**2 + me2*(6*ss + 6*tt - &
          &4*s25 - 2*s35) - (ss + tt - s25)*(ss + tt - s35))/((tt + s15 - s35)*(s15 + s25 -&
          & s35)) + (2*me2*(3*me2 - ss + s35))/(s25*(4*me2 - ss - tt + s35)) + (6*me2**2 + &
          &(ss + tt - s25)*(ss - s35) + me2*(-6*ss - 2*tt + s15 + 4*s25 + 2*s35))/(s35*(4*m&
          &e2 - ss - tt + s35)) + (4*me2**2 + (ss + tt - s25)*(ss - s35) - me2*(4*ss + s15 &
          &- 2*(-tt + s25 + s35)))/(tt*s35))*pvd3(8) + 128*Pi**2*((-4*me2**2 + me2*(4*ss - &
          &3*s15 - 4*s25) - (ss + tt - s25)*(ss - s15 - s25))/s15 - (tt*(2*me2 - ss + s15 +&
          & s25))/s15 + (2*me2*tt*(me2 + tt + s15))/(s25*(tt + s15 - s35)) + (2*me2*tt*(3*m&
          &e2 - ss + s35))/(s25*(4*me2 - ss - tt + s35)) - (tt*(5*me2 - ss - tt + s35))/(s1&
          &5 + s25 - s35) - (tt*(10*me2**2 + (ss + tt - s25)*(ss + tt - s35) + me2*(-6*ss -&
          & 6*tt + 4*s25 + 2*s35)))/((tt + s15 - s35)*(s15 + s25 - s35)) + (tt*(6*me2**2 + &
          &(ss + tt - s25)*(ss - s35) + me2*(-6*ss - 2*tt + s15 + 4*s25 + 2*s35)))/(s35*(4*&
          &me2 - ss - tt + s35)) + (4*me2**2 + (ss + tt - s25)*(ss - s35) - me2*(4*ss + s15&
          & - 2*(-tt + s25 + s35)))/s35)*pvd3(9) + 128*Pi**2*(-tt + s15)*((2*me2 - ss + s15&
          & + s25)/s15 + (4*me2**2 + (ss + tt - s25)*(ss - s15 - s25) + me2*(-4*ss + 3*s15 &
          &+ 4*s25))/(tt*s15) - (2*me2*(me2 + tt + s15))/(s25*(tt + s15 - s35)) + (-6*me2**&
          &2 + me2*(6*ss + 2*tt - s15 - 4*s25 - 2*s35) - (ss + tt - s25)*(ss - s35))/(s35*(&
          &4*me2 - ss - tt + s35)) - (2*me2*(3*me2 - ss + s35))/(s25*(4*me2 - ss - tt + s35&
          &)) + (5*me2 - ss - tt + s35)/(s15 + s25 - s35) + (10*me2**2 + (ss + tt - s25)*(s&
          &s + tt - s35) + me2*(-6*ss - 6*tt + 4*s25 + 2*s35))/((tt + s15 - s35)*(s15 + s25&
          & - s35)) + (-4*me2**2 - (ss + tt - s25)*(ss - s35) + me2*(4*ss + s15 - 2*(-tt + &
          &s25 + s35)))/(tt*s35))*pvd3(10) + 128*Pi**2*(me2 - s15)*((-4*me2**2 + me2*(4*ss &
          &- 3*s15 - 4*s25) - (ss + tt - s25)*(ss - s15 - s25))/(tt*s15) - (2*me2 - ss + s1&
          &5 + s25)/s15 + (2*me2*(me2 + tt + s15))/(s25*(tt + s15 - s35)) + (-5*me2 + ss + &
          &tt - s35)/(s15 + s25 - s35) + (-10*me2**2 + me2*(6*ss + 6*tt - 4*s25 - 2*s35) - &
          &(ss + tt - s25)*(ss + tt - s35))/((tt + s15 - s35)*(s15 + s25 - s35)) + (2*me2*(&
          &3*me2 - ss + s35))/(s25*(4*me2 - ss - tt + s35)) + (6*me2**2 + (ss + tt - s25)*(&
          &ss - s35) + me2*(-6*ss - 2*tt + s15 + 4*s25 + 2*s35))/(s35*(4*me2 - ss - tt + s3&
          &5)) + (4*me2**2 + (ss + tt - s25)*(ss - s35) - me2*(4*ss + s15 - 2*(-tt + s25 + &
          &s35)))/(tt*s35))*pvd3(11) + 128*Pi**2*((24*me2**3 + ss*(tt + s15 + s25 - 2*s35)*&
          &(ss + tt - s35) - 2*me2**2*(8*ss - 2*tt - s15 - 8*s25 + 9*s35) + me2*(2*ss**2 - &
          &tt**2 - 3*tt*s25 - ss*(5*tt + 4*s15 + 4*s25 - 11*s35) - s15*(tt + 4*s25 - 3*s35)&
          & + 8*tt*s35 + s25*s35 - 5*s35**2))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) +&
          & (-72*me2**3 + 2*me2**2*(36*ss + 38*tt + 4*s15 - 18*s25 - 7*s35) + (2*ss**2 + 4*&
          &ss*tt + 2*tt**2 - 3*ss*s25 - 3*tt*s25 + s25**2)*(ss + tt - s35) - me2*(22*ss**2 &
          &+ 22*tt**2 + tt*s15 - 25*tt*s25 - 4*s15*s25 + 2*s25**2 + ss*(44*tt + s15 - 18*s2&
          &5 - 13*s35) - 12*tt*s35 + 12*s25*s35 - 2*s35**2))/(s25*(4*me2 - ss - tt + s35)) &
          &+ (24*me2**3 - (tt*s15 + ss*(tt + 2*s15 - s25) - tt*s25 + s25**2)*(ss + tt - s35&
          &) - 2*me2**2*(8*ss + 2*tt + 5*s15 - 8*s25 + s35) + me2*(2*ss**2 - tt**2 + 7*tt*s&
          &15 - 9*tt*s25 + 2*s25**2 + ss*(7*tt + 10*s15 - 10*s25 - s35) - 5*s15*s35 + 8*s25&
          &*s35 - s35**2))/(s15*(4*me2 - ss - tt + s25)) + (-16*me2**3 + 2*me2**2*(8*ss - 5&
          &*tt - 4*s25) + me2*(-4*ss**2 + 5*tt**2 + tt*s25 + ss*(6*tt + 8*s25 - 2*s35) - 3*&
          &tt*s35 - 6*s25*s35 + s35**2 - s15*(2*tt + s35)) + (ss + tt - s35)*(tt*(-tt + s15&
          & - s35) + 2*s25*s35 + ss*(-2*tt - 2*s25 + s35)))/(tt*s35) + (16*me2**3 - 2*me2**&
          &2*(8*ss - tt + 3*s15 + s35) - (ss + tt - s35)*(-((-tt + s25)*(2*s15 + s25 - s35)&
          &) + ss*(-tt + s15 + s35)) + me2*(4*ss**2 + tt**2 + 4*ss*s15 + 3*tt*s25 - 2*s25**&
          &2 - 3*tt*s35 + s25*s35 + s35**2 - s15*(-7*tt + 4*s25 + s35)))/((tt + s15 - s35)*&
          &(s15 + s25 - s35)) + (16*me2**3 - 2*me2**2*(8*ss + 9*tt - 8*s25 - 4*s35) - (ss +&
          & tt - s35)*(s15*(-tt + 2*s25) + (-tt + s25)*(2*s25 - s35) + ss*(tt - 2*s25 + s35&
          &)) + me2*(4*ss**2 + 5*tt**2 - 11*tt*s25 + 4*s15*s25 + 4*s25**2 - 5*tt*s35 + 6*s2&
          &5*s35 - 2*s15*(tt + s35) - 2*ss*(-6*tt + 6*s25 + s35)))/(tt*s15) + (-24*me2**3 +&
          & (tt*(-tt + s15 + s25) - ss*(2*tt + s25 - 2*s35))*(ss + tt - s35) + 2*me2**2*(8*&
          &ss - 10*tt - 6*s25 + 5*s35) - me2*(2*ss**2 - 10*tt**2 + 4*tt*s15 + 2*tt*s25 + 11&
          &*tt*s35 + 2*s15*s35 + 5*s25*s35 - 4*s35**2 + ss*(-12*tt - 8*s25 + 7*s35)))/(s35*&
          &(4*me2 - ss - tt + s35)) + (-48*me2**3 + (ss + tt - s35)*(4*ss**2 + 4*tt**2 - 4*&
          &tt*s25 + s25**2 - tt*s35 - ss*(-8*tt + 4*s25 + s35)) + me2**2*(64*ss - 2*(-29*tt&
          & + s15 + 12*s25 + 11*s35)) + me2*(-28*ss**2 - 25*tt**2 + tt*s15 + 21*tt*s25 - 2*&
          &s25**2 + 23*tt*s35 - 13*s25*s35 - 2*s35**2 + 2*ss*(-27*tt + s15 + 10*s25 + 12*s3&
          &5)))/(s25*(tt + s15 - s35)))*pvd5(1) + 128*Pi**2*((s15*(5*me2 - ss - tt + s25))/&
          &s25 + (-4*me2**2 + me2*(4*ss + 2*tt - 2*s25 - 3*s35) - (ss - s25)*(ss + tt - s35&
          &))/tt + (-6*me2**2 + me2*(6*ss + 2*tt - 2*s25 - 3*s35) - (ss - s25)*(ss + tt - s&
          &35))/(4*me2 - ss - tt + s25) - (2*me2*s15*(3*me2 - ss + s25))/((4*me2 - ss - tt &
          &+ s25)*(s15 + s25 - s35)) - (2*me2*s15*(me2 + tt - s35))/((tt + s15 - s35)*(s15 &
          &+ s25 - s35)) + ((2*me2 - ss)*s15)/s35 + (s15*(4*me2**2 + ss*(ss + tt - s35) + m&
          &e2*(-4*ss + s35)))/(tt*s35) + (s15*(10*me2**2 + (ss + tt - s25)*(ss + tt - s35) &
          &+ me2*(-6*ss - 6*tt + 2*s25 + 4*s35)))/(s25*(tt + s15 - s35)))*pvd5(2) + 128*Pi*&
          &*2*(-s25 + s35)*((5*me2 - ss - tt + s25)/s25 + (-4*me2**2 + me2*(4*ss + 2*tt - 2&
          &*s25 - 3*s35) - (ss - s25)*(ss + tt - s35))/(tt*s15) + (-6*me2**2 + me2*(6*ss + &
          &2*tt - 2*s25 - 3*s35) - (ss - s25)*(ss + tt - s35))/(s15*(4*me2 - ss - tt + s25)&
          &) - (2*me2*(3*me2 - ss + s25))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) - (2*&
          &me2*(me2 + tt - s35))/((tt + s15 - s35)*(s15 + s25 - s35)) + (2*me2 - ss)/s35 + &
          &(4*me2**2 + ss*(ss + tt - s35) + me2*(-4*ss + s35))/(tt*s35) + (10*me2**2 + (ss &
          &+ tt - s25)*(ss + tt - s35) + me2*(-6*ss - 6*tt + 2*s25 + 4*s35))/(s25*(tt + s15&
          & - s35)))*pvd5(3) + 128*Pi**2*s35*((5*me2 - ss - tt + s25)/s25 + (-4*me2**2 + me&
          &2*(4*ss + 2*tt - 2*s25 - 3*s35) - (ss - s25)*(ss + tt - s35))/(tt*s15) + (-6*me2&
          &**2 + me2*(6*ss + 2*tt - 2*s25 - 3*s35) - (ss - s25)*(ss + tt - s35))/(s15*(4*me&
          &2 - ss - tt + s25)) - (2*me2*(3*me2 - ss + s25))/((4*me2 - ss - tt + s25)*(s15 +&
          & s25 - s35)) - (2*me2*(me2 + tt - s35))/((tt + s15 - s35)*(s15 + s25 - s35)) + (&
          &2*me2 - ss)/s35 + (4*me2**2 + ss*(ss + tt - s35) + me2*(-4*ss + s35))/(tt*s35) +&
          & (10*me2**2 + (ss + tt - s25)*(ss + tt - s35) + me2*(-6*ss - 6*tt + 2*s25 + 4*s3&
          &5))/(s25*(tt + s15 - s35)))*pvd5(4) - (128*Pi**2*(128*me2**5*(4*s15**3*s25 - 5*s&
          &25*(s25 - s35)*s35*(-tt + s35) + s15**2*(4*tt*s25 + 4*s25**2 - 14*tt*s35 - 3*s25&
          &*s35) + s15*(14*tt*s35**2 + s25**2*(4*tt + s35) - 2*s25*s35*(4*tt + 3*s35))) - 3&
          &2*me2**4*(-26*s15**4*s25 + 2*s25*(s25 - s35)*s35*(-tt + s35)*(-7*tt + 4*s25 + 9*&
          &s35) + s15**3*(-8*tt*s25 - 48*s25**2 + 34*tt*s35 + 47*s25*s35) + s15*(tt*(56*tt &
          &- 27*s35)*s35**2 + 2*s25**3*(-11*tt + 7*s35) + s25**2*(18*tt**2 + 35*tt*s35 - 19&
          &*s35**2) + s25*s35*(-36*tt**2 - 50*tt*s35 + 31*s35**2)) - s15**2*(22*s25**3 + s2&
          &5**2*(30*tt - 57*s35) + 7*tt*s35*(8*tt + s35) + s25*(-18*tt**2 - 75*tt*s35 + 34*&
          &s35**2)) + 2*ss*(16*s15**3*s25 - 12*s25*(s25 - s35)*s35*(-tt + s35) + s15**2*(16&
          &*tt*s25 + 16*s25**2 - 45*tt*s35 - 20*s25*s35) + s15*(45*tt*s35**2 - 4*s25**2*(-4&
          &*tt + s35) - 8*s25*s35*(4*tt + s35)))) + 8*me2**3*(4*s15**5*s25 + s15**4*(-64*tt&
          &*s25 + 42*s25**2 + 4*tt*s35 + 62*s25*s35) - s25*(s25 - s35)*s35*(-tt + s35)*(11*&
          &tt**2 - 23*tt*s25 + 3*s25**2 - 37*tt*s35 + 27*s25*s35 + 19*s35**2) + s15**3*(64*&
          &s25**3 + 3*tt*(50*tt - 31*s35)*s35 + s25**2*(-95*tt + 41*s35) + s25*(-38*tt**2 +&
          & 74*tt*s35 - 145*s35**2)) + s15*(s25**4*(26*tt - 23*s35) + s25**3*(-69*tt**2 + 4&
          &5*tt*s35 - 4*s35**2) + tt*s35**2*(80*tt**2 - 75*tt*s35 + 14*s35**2) + s25**2*(30&
          &*tt**3 + 62*tt**2*s35 - 53*tt*s35**2 + 25*s35**3) - s25*s35*(60*tt**3 + 75*tt**2&
          &*s35 - 124*tt*s35**2 + 47*s35**3)) + s15**2*(26*s25**4 - s25**3*(5*tt + 36*s35) &
          &+ s25**2*(-107*tt**2 + 110*tt*s35 - 116*s35**2) + 5*tt*s35*(-16*tt**2 - 15*tt*s3&
          &5 + 15*s35**2) + s25*(30*tt**3 + 227*tt**2*s35 - 162*tt*s35**2 + 107*s35**3)) + &
          &ss**2*(100*s15**3*s25 - 41*s25*(s25 - s35)*s35*(-tt + s35) + s15**2*(100*tt*s25 &
          &+ 100*s25**2 - 230*tt*s35 - 159*s25*s35) + s15*(s25**2*(100*tt - 59*s35) + 230*t&
          &t*s35**2 + 2*s25*s35*(-100*tt + 9*s35))) + ss*(-108*s15**4*s25 + s25*(s25 - s35)&
          &*s35*(-tt + s35)*(-56*tt + 33*s25 + 57*s35) + 2*s15**3*(9*tt*s25 - 106*s25**2 + &
          &73*tt*s35 + 75*s25*s35) + s15**2*(-104*s25**3 + 2*tt*s35*(-157*tt + 9*s35) + s25&
          &**2*(-86*tt + 217*s35) + s25*(126*tt**2 + 190*tt*s35 - 33*s35**2)) + s15*(2*tt*(&
          &157*tt - 82*s35)*s35**2 + s25**3*(-104*tt + 71*s35) + s25**2*(126*tt**2 + 72*tt*&
          &s35 - 29*s35**2) + 6*s25*s35*(-42*tt**2 - 15*tt*s35 + 8*s35**2)))) - 2*me2**2*(-&
          &4*s15**5*(-2*tt*s25 + s25**2 - 2*tt*s35 + 3*s25*s35) - 2*s15**4*(8*s25**3 + tt*s&
          &35*(-2*tt + 3*s35) + s25**2*(-35*tt + 51*s35) + s25*(25*tt**2 - 68*tt*s35 + 18*s&
          &35**2)) + s25*s35*(-tt + s35)*(s25**3*(-7*tt + 9*s35) + s25**2*(17*tt**2 - 37*tt&
          &*s35 + 16*s35**2) + s25*(-2*tt**3 + 6*tt**2*s35 + 17*tt*s35**2 - 19*s35**3) + s3&
          &5*(2*tt**3 - 23*tt**2*s35 + 27*tt*s35**2 - 6*s35**3)) + s15**3*(-20*s25**4 + s25&
          &**3*(109*tt - 140*s35) + tt*s35*(216*tt**2 - 239*tt*s35 + 52*s35**2) + s25**2*(-&
          &71*tt**2 + 107*tt*s35 + 125*s35**2) + s25*(-18*tt**3 - 119*tt**2*s35 - 120*tt*s3&
          &5**2 + 118*s35**3)) + s15**2*(-8*s25**5 + s25**4*(39*tt - 42*s35) + 2*s25**3*(19&
          &*tt**2 - 66*tt*s35 + 99*s35**2) - s25**2*(105*tt**3 - 17*tt**2*s35 + 105*tt*s35*&
          &*2 + 15*s35**3) - tt*s35*(66*tt**3 + 112*tt**2*s35 - 181*tt*s35**2 + 42*s35**3) &
          &+ s25*(40*tt**4 + 149*tt**3*s35 + 17*tt**2*s35**2 + 34*tt*s35**3 - 86*s35**4)) +&
          & s15*(8*s25**5*(-tt + s35) + s25**4*(59*tt**2 - 106*tt*s35 + 53*s35**2) + s25**3&
          &*(-87*tt**3 + 102*tt**2*s35 + 21*tt*s35**2 - 58*s35**3) + 2*tt*s35**2*(33*tt**3 &
          &- 52*tt**2*s35 + 27*tt*s35**2 - 6*s35**3) + s25**2*(40*tt**4 - 24*tt**3*s35 + 29&
          &*tt**2*s35**2 - 30*tt*s35**3 + 15*s35**4) + s25*s35*(-80*tt**4 + 85*tt**3*s35 - &
          &22*tt**2*s35**2 - 23*tt*s35**3 + 22*s35**4)) + 2*ss**3*(76*s15**3*s25 - 15*s25*(&
          &s25 - s35)*s35*(-tt + s35) + s15**2*(76*tt*s25 + 76*s25**2 - 147*tt*s35 - 137*s2&
          &5*s35) + s15*(s25**2*(76*tt - 61*s35) + 147*tt*s35**2 + 2*s25*s35*(-76*tt + 23*s&
          &35))) + ss**2*(-170*s15**4*s25 + s25*(s25 - s35)*s35*(-tt + s35)*(-66*tt + 41*s2&
          &5 + 59*s35) + s15**3*(146*tt*s25 - 372*s25**2 + 240*tt*s35 + 147*s25*s35) + s15*&
          &(3*tt*(214*tt - 115*s35)*s35**2 + s25**3*(-202*tt + 161*s35) + s25*s35*(-632*tt*&
          &*2 + 136*tt*s35 - 75*s35**2) + s25**2*(316*tt**2 - 39*tt*s35 + 14*s35**2)) + s15&
          &**2*(-202*s25**3 + 3*tt*s35*(-214*tt + 35*s35) + 4*s25**2*(-14*tt + 85*s35) + s2&
          &5*(316*tt**2 + 15*tt*s35 + 157*s35**2))) + ss*(16*s15**5*s25 + 2*s15**4*(-98*tt*&
          &s25 + 69*s25**2 - 10*tt*s35 + 93*s25*s35) - s25*(s25 - s35)*s35*(-tt + s35)*(38*&
          &tt**2 - 54*tt*s25 + 11*s25**2 - 90*tt*s35 + 62*s25*s35 + 37*s35**2) + s15**3*(21&
          &2*s25**3 + s25*(46*tt - 387*s35)*s35 + tt*(432*tt - 223*s35)*s35 + s25**2*(-361*&
          &tt + 165*s35)) + s15*(s25**4*(90*tt - 79*s35) + s25**3*(-287*tt**2 + 260*tt*s35 &
          &- 77*s35**2) + tt*s35**2*(422*tt**2 - 447*tt*s35 + 108*s35**2) + s25*s35*(-424*t&
          &t**3 + 79*tt**2*s35 + 130*tt*s35**2 - 41*s35**3) + s25**2*(212*tt**3 + 89*tt**2*&
          &s35 - 122*tt*s35**2 + 87*s35**3)) + s15**2*(90*s25**4 - 3*s25**3*(25*tt + 28*s35&
          &) + s25**2*(-287*tt**2 + 385*tt*s35 - 415*s35**2) + tt*s35*(-422*tt**2 + 15*tt*s&
          &35 + 135*s35**2) + s25*(212*tt**3 + 406*tt**2*s35 - 157*tt*s35**2 + 189*s35**3))&
          &)) - s15*(ss + tt - s35)*(s15**4*(-tt + s25)**2*s35 + s15**3*(-tt + s25)*s35*(-4&
          &*tt*s25 + 3*s25**2 + tt*s35) + ss**4*(4*s15**2*s25 + 6*tt*s35**2 + 4*s25*s35*(-2&
          &*tt + s35) - 4*s25**2*(-tt + s35) + s15*(4*tt*s25 + 4*s25**2 - 6*tt*s35 - 8*s25*&
          &s35)) + s15**2*(s25**4*(-tt + 4*s35) + s25**3*(3*tt**2 - 10*tt*s35 - 3*s35**2) +&
          & 2*tt**2*s25*(tt**2 - 5*tt*s35 + s35**2) + tt**2*s35*(3*tt**2 - 3*tt*s35 + s35**&
          &2) + s25**2*(-4*tt**3 + 11*tt**2*s35 + 8*tt*s35**2 - 3*s35**3)) - (-tt + s35)*(s&
          &25**5*(-tt + 2*s35) + s25**4*(3*tt**2 - 4*tt*s35 - 2*s35**2) + tt**2*s35**2*(2*t&
          &t**2 - 2*tt*s35 + s35**2) + tt*s25**3*(-4*tt**2 + 3*tt*s35 + 3*s35**2) + tt*s25*&
          &s35*(-4*tt**3 + 5*tt**2*s35 - 5*tt*s35**2 + s35**3) + tt*s25**2*(2*tt**3 + tt**2&
          &*s35 - 2*tt*s35**2 + s35**3)) + s15*(tt**4*s35*(-2*tt + s35) + s25**5*(-tt + 2*s&
          &35) - tt*s25**3*(tt**2 + 3*tt*s35 - 14*s35**2) - s25**4*(-2*tt**2 + tt*s35 + 6*s&
          &35**2) + tt*s25*(2*tt**4 - 4*tt**3*s35 + 14*tt**2*s35**2 - 9*tt*s35**3 + 2*s35**&
          &4) + s25**2*(-2*tt**4 + 4*tt**3*s35 - 11*tt**2*s35**2 - 3*tt*s35**3 + 2*s35**4))&
          & + ss**3*(-4*s15**3*s25 + tt*(18*tt - 7*s35)*s35**2 + 8*s25**3*(-tt + s35) - 2*s&
          &25**2*(-6*tt**2 + tt*s35 + s35**2) - 2*s25*s35*(12*tt**2 - 5*tt*s35 + 3*s35**2) &
          &+ s15**2*(-12*s25**2 + 6*tt*s35 + 2*s25*(4*tt + s35)) + s15*(-8*s25**3 + 14*s25*&
          &*2*s35 + tt*s35*(-18*tt + s35) + 4*s25*(3*tt**2 - 2*tt*s35 + 2*s35**2))) + ss**2&
          &*(s15**4*s25 - 6*s25**4*(-tt + s35) + s15**3*(-5*tt*s25 + 7*s25**2 - 3*tt*s35 + &
          &4*s25*s35) - 2*s25**3*(9*tt**2 - 8*tt*s35 + 2*s35**2) + tt*s35**2*(21*tt**2 - 18&
          &*tt*s35 + 4*s35**2) + s25*s35*(-30*tt**3 + 17*tt**2*s35 - 10*tt*s35**2 + 2*s35**&
          &3) + s25**2*(15*tt**3 + 5*tt**2*s35 - 8*tt*s35**2 + 8*s35**3) + s15**2*(12*s25**&
          &3 + 12*tt**2*s35 + s25**2*(-17*tt + 3*s35) + s25*(9*tt**2 - 14*tt*s35 - 9*s35**2&
          &)) + s15*(6*s25**4 - 2*s25**3*(3*tt + 4*s35) - tt*s35*(21*tt**2 - 6*tt*s35 + s35&
          &**2) - 9*s25**2*(tt**2 - 2*tt*s35 + 2*s35**2) + s25*(15*tt**3 - 4*tt**2*s35 + 20&
          &*tt*s35**2 + 2*s35**3))) - ss*(-2*s25**5*(-tt + s35) + s15**4*(-tt + s25)*(s25 +&
          & s35) + s25**4*(-9*tt**2 + 11*tt*s35 - 4*s35**2) + tt*s25*s35*(18*tt**3 - 19*tt*&
          &*2*s35 + 12*tt*s35**2 - 3*s35**3) + tt*s35**2*(-11*tt**3 + 15*tt**2*s35 - 7*tt*s&
          &35**2 + s35**3) + s25**3*(15*tt**3 - 15*tt**2*s35 + 4*tt*s35**2 + 4*s35**3) + s2&
          &5**2*(-9*tt**4 - 4*tt**3*s35 + 11*tt**2*s35**2 - 12*tt*s35**3 + 2*s35**4) + s15*&
          &*3*(3*s25**3 + tt*s25*(tt - 11*s35) + tt*s35*(3*tt + s35) + s25**2*(-4*tt + 7*s3&
          &5)) + s15**2*(4*s25**4 + s25**3*(-10*tt + 9*s35) + s25**2*(12*tt**2 - 19*tt*s35 &
          &- 12*s35**2) - tt*s35*(9*tt**2 - 3*tt*s35 + s35**2) + s25*(-7*tt**3 + 23*tt**2*s&
          &35 + 10*tt*s35**2 - 3*s35**3)) + s15*(-5*tt*s25**4 + 2*s25**5 + s25**3*(2*tt**2 &
          &+ 9*tt*s35 - 16*s35**2) + tt**2*s35*(11*tt**2 - 6*tt*s35 + s35**2) + s25**2*(8*t&
          &t**3 - 14*tt**2*s35 + 27*tt*s35**2 + 2*s35**3) + s25*(-9*tt**4 + 7*tt**3*s35 - 2&
          &3*tt**2*s35**2 + 2*tt*s35**3 + 2*s35**4)))) + me2*(2*s15**5*(-2*tt*s35*(-2*tt + &
          &s35) + s25**2*(-tt + 3*s35) + s25*(tt**2 - 7*tt*s35 + 2*s35**2)) - s25*(s25 - s3&
          &5)*s35*(-tt + s35)**2*(3*tt*(tt - s35)*s35 + s25**2*(-2*tt + 3*s35) + s25*(tt**2&
          & - 5*tt*s35 + 3*s35**2)) + s15**4*(tt*s35**2*(-7*tt + 4*s35) + 4*s25**3*(-2*tt +&
          & 5*s35) + tt*s25*(-6*tt**2 + 55*tt*s35 - 31*s35**2) + s25**2*(14*tt**2 - 71*tt*s&
          &35 + 30*s35**2)) + s15**3*(13*s25**4*(-tt + 2*s35) + s25**3*(34*tt**2 - 101*tt*s&
          &35 + 26*s35**2) + s25**2*(-29*tt**3 + 98*tt**2*s35 + 48*tt*s35**2 - 71*s35**3) +&
          & tt*s35*(62*tt**3 - 97*tt**2*s35 + 42*tt*s35**2 - 4*s35**3) + s25*(12*tt**4 - 11&
          &8*tt**3*s35 + 61*tt**2*s35**2 + 36*tt*s35**3 - 12*s35**4)) + s15*(-tt + s35)*(s2&
          &5**5*(7*tt - 12*s35) + s25**4*(-28*tt**2 + 47*tt*s35 - 7*s35**2) + tt*s25*s35*(4&
          &0*tt**3 - 69*tt**2*s35 + 70*tt*s35**2 - 20*s35**3) + 2*tt*s35**2*(-11*tt**3 + 15&
          &*tt**2*s35 - 9*tt*s35**2 + 2*s35**3) + s25**3*(37*tt**3 - 41*tt**2*s35 - 20*tt*s&
          &35**2 + 24*s35**3) + s25**2*(-20*tt**4 + 16*tt**3*s35 - 11*tt**2*s35**2 + 4*tt*s&
          &35**3 - 11*s35**4)) + s15**2*(s25**5*(-7*tt + 12*s35) + s25**4*(15*tt**2 - 22*tt&
          &*s35 - 16*s35**2) + s25**3*(5*tt**3 - 45*tt**2*s35 + 153*tt*s35**2 - 70*s35**3) &
          &- tt**2*s35*(22*tt**3 + 10*tt**2*s35 - 49*tt*s35**2 + 21*s35**3) + s25**2*(-25*t&
          &t**4 + 8*tt**3*s35 - 55*tt**2*s35**2 - 12*tt*s35**3 + 43*s35**4) + s25*(20*tt**5&
          & - 23*tt**4*s35 + 146*tt**3*s35**2 - 151*tt**2*s35**3 + 20*tt*s35**4 + 8*s35**5)&
          &) + 2*ss**4*(28*s15**3*s25 - 2*s25*(s25 - s35)*s35*(-tt + s35) + s15**2*(28*tt*s&
          &25 + 28*s25**2 - 47*tt*s35 - 54*s25*s35) + s15*(s25**2*(28*tt - 26*s35) + 47*tt*&
          &s35**2 + 8*s25*s35*(-7*tt + 3*s35))) - 2*ss**3*(30*s15**4*s25 - s25*(s25 - s35)*&
          &s35*(-tt + s35)*(-6*tt + 4*s25 + 5*s35) + s15**3*(-54*tt*s25 + 76*s25**2 - 44*tt&
          &*s35 - 7*s25*s35) + s15**2*(46*s25**3 + 2*tt*(72*tt - 17*s35)*s35 - s25**2*(8*tt&
          & + 65*s35) + s25*(-84*tt**2 + 65*tt*s35 - 71*s35**2)) + s15*(s25**3*(46*tt - 42*&
          &s35) + 6*tt*s35**2*(-24*tt + 13*s35) + s25**2*(-84*tt**2 + 39*tt*s35 - 10*s35**2&
          &) + s25*s35*(168*tt**2 - 88*tt*s35 + 43*s35**2))) + ss**2*(10*s15**5*s25 + 2*s15&
          &**4*(-51*tt*s25 + 38*s25**2 - 12*tt*s35 + 47*s25*s35) - s25*(s25 - s35)*s35*(-tt&
          & + s35)*(12*tt**2 - 15*tt*s25 + 4*s25**2 - 25*tt*s35 + 17*s25*s35 + 9*s35**2) + &
          &s15**3*(122*s25**3 + tt*(214*tt - 89*s35)*s35 + 6*s25**2*(-41*tt + 20*s35) - 3*s&
          &25*(-28*tt**2 + 31*tt*s35 + 55*s35**2)) + s15**2*(56*s25**4 - 2*s25**3*(44*tt + &
          &13*s35) + s25**2*(-126*tt**2 + 259*tt*s35 - 274*s35**2) + tt*s35*(-324*tt**2 + 1&
          &41*tt*s35 + 22*s35**2) + s25*(196*tt**3 - tt**2*s35 + 147*tt*s35**2 + 17*s35**3)&
          &) + s15*(s25**4*(56*tt - 52*s35) + s25**3*(-210*tt**2 + 231*tt*s35 - 83*s35**2) &
          &+ tt*s35**2*(324*tt**2 - 355*tt*s35 + 91*s35**2) + s25*s35*(-392*tt**3 + 278*tt*&
          &*2*s35 - 104*tt*s35**2 + 35*s35**3) + s25**2*(196*tt**3 - 25*tt**2*s35 - 60*tt*s&
          &35**2 + 70*s35**3))) - ss*(2*s15**5*(-6*tt*s25 + 3*s25**2 - 4*tt*s35 + 7*s25*s35&
          &) + s15**4*(20*s25**3 + tt*(24*tt - 5*s35)*s35 + 2*s25**2*(-39*tt + 53*s35) + s2&
          &5*(48*tt**2 - 149*tt*s35 + 34*s35**2)) + s15**3*(26*s25**4 + s25**3*(-137*tt + 1&
          &48*s35) + s25**2*(135*tt**2 - 191*tt*s35 - 103*s35**2) - 2*tt*s35*(94*tt**2 - 87&
          &*tt*s35 + 15*s35**2) + s25*(-44*tt**3 + 225*tt**2*s35 + 92*tt*s35**2 - 107*s35**&
          &3)) + s25*s35*(-tt + s35)*(s25**3*(6*tt - 7*s35) + s25**2*(-8*tt**2 + 15*tt*s35 &
          &- 5*s35**2) + s35*(-4*tt**3 + 18*tt**2*s35 - 17*tt*s35**2 + 3*s35**3) + s25*(4*t&
          &t**3 - 10*tt**2*s35 - 4*tt*s35**2 + 9*s35**3)) + s15*(-12*s25**5*(-tt + s35) + s&
          &25**4*(-85*tt**2 + 133*tt*s35 - 59*s35**2) + s25**3*(159*tt**3 - 218*tt**2*s35 +&
          & 77*tt*s35**2 + 25*s35**3) + tt*s35**2*(-152*tt**3 + 255*tt**2*s35 - 139*tt*s35*&
          &*2 + 26*s35**3) + s25*s35*(208*tt**4 - 263*tt**3*s35 + 138*tt**2*s35**2 - 39*tt*&
          &s35**3 - 3*s35**4) + s25**2*(-104*tt**4 + 31*tt**3*s35 + 62*tt**2*s35**2 - 79*tt&
          &*s35**3 + 27*s35**4)) + s15**2*(12*s25**5 + s25**4*(-59*tt + 40*s35) + 2*s25**3*&
          &(tt**2 + 57*tt*s35 - 94*s35**2) + s25**2*(115*tt**3 - 143*tt**2*s35 + 270*tt*s35&
          &**2 - 45*s35**3) + tt*s35*(152*tt**3 - 67*tt**2*s35 - 59*tt*s35**2 + 17*s35**3) &
          &+ s25*(-104*tt**4 + 2*tt**3*s35 - 164*tt**2*s35**2 + 46*tt*s35**3 + 59*s35**4)))&
          &))*pvd7(1))/(tt*s15*s25*(4*me2 - ss - tt + s25)*(tt + s15 - s35)*(s15 + s25 - s3&
          &5)*s35*(4*me2 - ss - tt + s35)) - (128*Pi**2*(2*me2 - tt + s35)*(tt*s15*(4*me2 -&
          & ss - tt + s25)*(-56*me2**2 + me2*(24*ss + 28*tt + 4*s15 - 6*s25 - 24*s35) - (3*&
          &ss + 3*tt - 2*s25 - 2*s35)*(ss + tt - s35))*(tt + s15 - s35)*(s15 + s25 - s35)*s&
          &35 + s15*s25*(4*me2 - ss - tt + s25)*(20*me2**2 - 2*me2*(7*ss + 7*tt - 4*s15 - 3&
          &*s25 - 5*s35) + (ss + tt - s35)*(2*ss + 2*tt - 2*s15 - 2*s25 - s35))*(tt + s15 -&
          & s35)*(s15 + s25 - s35)*(4*me2 - ss - tt + s35) + tt*s15*s25*(4*me2 - ss - tt + &
          &s25)*(4*me2**2 - me2*(4*ss + 4*tt + 5*s15 + s25 - 5*s35) + (ss + tt - s35)*(ss +&
          & tt + s15 - s35))*s35*(4*me2 - ss - tt + s35) + tt*s15*s25*(8*me2**2 - me2*(6*ss&
          & + 6*tt + 6*s15 + s25 - 7*s35) + (ss + tt - s35)*(ss + tt + s15 - s35))*(tt + s1&
          &5 - s35)*s35*(4*me2 - ss - tt + s35) + s25*(4*me2 - ss - tt + s25)*(4*me2**2 - m&
          &e2*(2*ss + 2*tt + 3*s15 + s25 - 3*s35) + s15*(ss + tt - s35))*(tt + s15 - s35)*(&
          &s15 + s25 - s35)*s35*(4*me2 - ss - tt + s35) + tt*s25*(8*me2**2 - me2*(2*ss + 2*&
          &tt + 6*s15 + s25 - 3*s35) + s15*(ss + tt - s35))*(tt + s15 - s35)*(s15 + s25 - s&
          &35)*s35*(4*me2 - ss - tt + s35) - tt*s15*(4*me2 - ss - tt + s25)*(s15 + s25 - s3&
          &5)*s35*(2*me2 - ss - tt + s35)*(4*me2 - ss - tt + s35)*(14*me2 - 3*ss - 3*tt + 2&
          &*s25 + 2*s35) + tt*s15*s25*(4*me2 - ss - tt + s25)*(tt + s15 - s35)*(s15 + s25 -&
          & s35)*(40*me2**2 + (ss + tt - s35)*(2*ss + 2*tt - 2*s15 - 2*s25 - s35) + me2*(-1&
          &6*ss - 20*tt + 8*s15 + 8*s25 + 14*s35)))*pvd7(2))/(tt*s15*s25*(4*me2 - ss - tt +&
          & s25)*(tt + s15 - s35)*(s15 + s25 - s35)*s35*(4*me2 - ss - tt + s35)) + 128*Pi**&
          &2*(((4*me2**2 - me2*(2*ss + 2*tt + 3*s15 + s25 - 3*s35) + s15*(ss + tt - s35))*(&
          &-4*me2 + ss + tt - 2*s35))/(tt*s15) + ((-8*me2**2 + me2*(2*ss + 2*tt + 6*s15 + s&
          &25 - 3*s35) - s15*(ss + tt - s35))*(4*me2 - ss - tt + 2*s35))/(s15*(4*me2 - ss -&
          & tt + s25)) - ((8*me2**2 - me2*(6*ss + 6*tt + 6*s15 + s25 - 7*s35) + (ss + tt - &
          &s35)*(ss + tt + s15 - s35))*(4*me2 - ss - tt + 2*s35))/((4*me2 - ss - tt + s25)*&
          &(s15 + s25 - s35)) - ((4*me2**2 - me2*(4*ss + 4*tt + 5*s15 + s25 - 5*s35) + (ss &
          &+ tt - s35)*(ss + tt + s15 - s35))*(4*me2 - ss - tt + 2*s35))/((tt + s15 - s35)*&
          &(s15 + s25 - s35)) - ((20*me2**2 - 2*me2*(7*ss + 7*tt - 4*s15 - 3*s25 - 5*s35) +&
          & (ss + tt - s35)*(2*ss + 2*tt - 2*s15 - 2*s25 - s35))*(4*me2 - ss - tt + 2*s35))&
          &/(tt*s35) + ((2*me2 - ss - tt + s35)*(4*me2 - ss - tt + 2*s35)*(14*me2 - 3*ss - &
          &3*tt + 2*s25 + 2*s35))/(s25*(tt + s15 - s35)) - ((4*me2 - ss - tt + 2*s35)*(40*m&
          &e2**2 + (ss + tt - s35)*(2*ss + 2*tt - 2*s15 - 2*s25 - s35) + me2*(-16*ss - 20*t&
          &t + 8*s15 + 8*s25 + 14*s35)))/(s35*(4*me2 - ss - tt + s35)) + ((4*me2 - ss - tt &
          &+ 2*s35)*(56*me2**2 + (3*ss + 3*tt - 2*s25 - 2*s35)*(ss + tt - s35) + me2*(-24*s&
          &s - 28*tt - 4*s15 + 6*s25 + 24*s35)))/(s25*(4*me2 - ss - tt + s35)))*pvd7(3) + 1&
          &28*Pi**2*(-((20*me2**2 - 2*me2*(7*ss + 7*tt - 4*s15 - 3*s25 - 5*s35) + (ss + tt &
          &- s35)*(2*ss + 2*tt - 2*s15 - 2*s25 - s35))/tt) + ((-4*me2**2 + me2*(2*ss + 2*tt&
          & + 3*s15 + s25 - 3*s35) - s15*(ss + tt - s35))*s35)/(tt*s15) + ((-8*me2**2 + me2&
          &*(2*ss + 2*tt + 6*s15 + s25 - 3*s35) - s15*(ss + tt - s35))*s35)/(s15*(4*me2 - s&
          &s - tt + s25)) - ((8*me2**2 - me2*(6*ss + 6*tt + 6*s15 + s25 - 7*s35) + (ss + tt&
          & - s35)*(ss + tt + s15 - s35))*s35)/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) &
          &- ((4*me2**2 - me2*(4*ss + 4*tt + 5*s15 + s25 - 5*s35) + (ss + tt - s35)*(ss + t&
          &t + s15 - s35))*s35)/((tt + s15 - s35)*(s15 + s25 - s35)) + (s35*(2*me2 - ss - t&
          &t + s35)*(14*me2 - 3*ss - 3*tt + 2*s25 + 2*s35))/(s25*(tt + s15 - s35)) - (40*me&
          &2**2 + (ss + tt - s35)*(2*ss + 2*tt - 2*s15 - 2*s25 - s35) + me2*(-16*ss - 20*tt&
          & + 8*s15 + 8*s25 + 14*s35))/(4*me2 - ss - tt + s35) + (s35*(56*me2**2 + (3*ss + &
          &3*tt - 2*s25 - 2*s35)*(ss + tt - s35) + me2*(-24*ss - 28*tt - 4*s15 + 6*s25 + 24&
          &*s35)))/(s25*(4*me2 - ss - tt + s35)))*pvd7(4) - (512*Pi**2*(me2 + s35)*(-2*me2*&
          &(tt*s25 + s15*s25 - tt*s35 - s25*s35) + ss*(tt*s25 + s15*s25 - tt*s35 - s25*s35)&
          & + s35*(tt*s15 - tt*s25 - s15*s25 + s25*s35))*pvd7(5))/(tt*s25*s35*(-tt - s15 + &
          &s35)) - (128*Pi**2*(2*me2 - tt + s35)**2*(-2*me2*(tt*s25 + s15*s25 - tt*s35 - s2&
          &5*s35) + ss*(tt*s25 + s15*s25 - tt*s35 - s25*s35) + s35*(tt*s15 - tt*s25 - s15*s&
          &25 + s25*s35))*pvd7(6))/(tt*s25*s35*(-tt - s15 + s35)) - (256*Pi**2*(2*me2 - tt &
          &+ s35)*(4*me2 - ss - tt + 2*s35)*(-2*me2*(tt*s25 + s15*s25 - tt*s35 - s25*s35) +&
          & ss*(tt*s25 + s15*s25 - tt*s35 - s25*s35) + s35*(tt*s15 - tt*s25 - s15*s25 + s25&
          &*s35))*pvd7(7))/(tt*s25*s35*(-tt - s15 + s35)) + (256*Pi**2*(2*me2 - tt + s35)*(&
          &-2*me2*(tt*s25 + s15*s25 - tt*s35 - s25*s35) + ss*(tt*s25 + s15*s25 - tt*s35 - s&
          &25*s35) + s35*(tt*s15 - tt*s25 - s15*s25 + s25*s35))*pvd7(8))/(tt*s25*(tt + s15 &
          &- s35)) - (128*Pi**2*(-4*me2 + ss + tt - 2*s35)**2*(-2*me2*(tt*s25 + s15*s25 - t&
          &t*s35 - s25*s35) + ss*(tt*s25 + s15*s25 - tt*s35 - s25*s35) + s35*(tt*s15 - tt*s&
          &25 - s15*s25 + s25*s35))*pvd7(9))/(tt*s25*s35*(-tt - s15 + s35)) + (256*Pi**2*(4&
          &*me2 - ss - tt + 2*s35)*(-2*me2*(tt*s25 + s15*s25 - tt*s35 - s25*s35) + ss*(tt*s&
          &25 + s15*s25 - tt*s35 - s25*s35) + s35*(tt*s15 - tt*s25 - s15*s25 + s25*s35))*pv&
          &d7(10))/(tt*s25*(tt + s15 - s35)) + (128*Pi**2*s35*(-2*me2*(tt*s25 + s15*s25 - t&
          &t*s35 - s25*s35) + ss*(tt*s25 + s15*s25 - tt*s35 - s25*s35) + s35*(tt*s15 - tt*s&
          &25 - s15*s25 + s25*s35))*pvd7(11))/(tt*s25*(tt + s15 - s35)) + (128*Pi**2*(1024*&
          &me2**6*(s15 - s35)*(s15**2*s25 - s25*(s25 - s35)*(-tt + s35) + s15*(s25**2 + s25&
          &*(tt - 2*s35) + tt*s35)) - 128*me2**5*(-(s25*(s25 - s35)*s35*(-tt + s35)*(-9*tt &
          &+ 10*s25 + 6*s35)) - s15**3*(-9*tt*s25 + 6*s25**2 + 7*tt*s35 + 6*s25*s35) + 4*ss&
          &*(s15 - s35)*(4*s15**2*s25 - 4*s25*(s25 - s35)*(-tt + s35) + s15*(4*tt*s25 + 4*s&
          &25**2 + 5*tt*s35 - 8*s25*s35)) + s15*(tt*s35**2*(-13*tt + 3*s35) + 2*s25**3*(-3*&
          &tt + 8*s35) + s25*s35*(-18*tt**2 + 53*tt*s35 - 18*s35**2) + s25**2*(9*tt**2 - 12&
          &*tt*s35 - 14*s35**2)) + s15**2*(-6*s25**3 + tt*s35*(13*tt + 4*s35) + s25**2*(3*t&
          &t + 16*s35) + s25*(9*tt**2 - 47*tt*s35 + 18*s35**2))) + tt*s15*(ss + tt - s25)*(&
          &ss + tt - s35)*s35*(s15**4*(-tt + s25) + 4*ss**4*(s15 - s35) - 4*ss**3*(s15 - s3&
          &5)*(-3*tt + s15 + 2*s25 + s35) + 3*ss**2*(s15 - s35)*(5*tt**2 + s15**2 + 2*s25**&
          &2 + 2*s15*(-tt + s25) - 4*tt*s35 + s35**2 + 2*s25*(-3*tt + s35)) + s15**3*(3*s25&
          &**2 + tt*s35 - 2*s25*(tt + s35)) + s15**2*(-tt**3 + 3*s25**3 - 7*s25**2*s35 + s2&
          &5*(tt**2 + 3*tt*s35 + s35**2)) - (-tt + s35)*(s25**4 - 2*s25**3*s35 - tt*s35*(2*&
          &tt**2 - 2*tt*s35 + s35**2) + s25*s35*(3*tt**2 - 2*tt*s35 + s35**2) + s25**2*(tt*&
          &*2 - 3*tt*s35 + 2*s35**2)) + s15*(s25**4 + s25**3*(tt - 5*s35) + s25**2*(4*tt**2&
          & - 4*tt*s35 + 6*s35**2) + tt*(2*tt**3 - 3*tt**2*s35 + 3*tt*s35**2 - s35**3) + s2&
          &5*(-4*tt**3 + 4*tt**2*s35 - 5*tt*s35**2 + s35**3)) - ss*(s15 - s35)*(-9*tt**3 + &
          &s15**3 + 2*s25**3 + s15**2*(-3*tt + 5*s25) + 12*tt**2*s35 - 6*tt*s35**2 + s35**3&
          & + s25**2*(-7*tt + s35) + s25*(15*tt**2 - 14*tt*s35 + 5*s35**2) + s15*(3*tt**2 +&
          & 5*s25**2 - 4*s25*(tt + s35)))) + 32*me2**4*(2*s15**4*(tt + s25)*s35 + s25*(s25 &
          &- s35)*s35*(-tt + s35)*(15*tt**2 - 46*tt*s25 + 16*s25**2 - 17*tt*s35 + 30*s25*s3&
          &5 + 4*s35**2) + 2*ss**2*(s15 - s35)*(25*s15**2*s25 - 25*s25*(s25 - s35)*(-tt + s&
          &35) + s15*(25*s25**2 + 25*s25*(tt - 2*s35) + 41*tt*s35)) + s15**3*(4*s25**3 + 7*&
          &tt*s35*(-7*tt + 3*s35) + s25**2*(-17*tt + 20*s35) + s25*(15*tt**2 + 9*tt*s35 - 2&
          &*s35**2)) + s15**2*(4*s25**4 - s25**3*(13*tt + 6*s35) + s25**2*(-2*tt**2 + 115*t&
          &t*s35 - 66*s35**2) + tt*s35*(33*tt**2 + 41*tt*s35 - 25*s35**2) - 3*s25*(-5*tt**3&
          & + 49*tt**2*s35 - 18*tt*s35**2 + 2*s35**3)) + s15*(4*s25**4*(tt - 5*s35) + s25**&
          &3*(-17*tt**2 + 73*tt*s35 - 12*s35**2) + tt*s35**2*(-33*tt**2 + 8*tt*s35 + 2*s35*&
          &*2) + s25*s35*(-30*tt**3 + 163*tt**2*s35 - 84*tt*s35**2 + 10*s35**3) + 3*s25**2*&
          &(5*tt**3 - 4*tt**2*s35 - 47*tt*s35**2 + 24*s35**3)) + ss*(-(s25*(s25 - s35)*s35*&
          &(-tt + s35)*(-63*tt + 62*s25 + 38*s35)) - s15**3*(-63*tt*s25 + 38*s25**2 + 51*tt&
          &*s35 + 38*s25*s35) + s15*(tt*s35**2*(-113*tt + 31*s35) + 2*s25**3*(-19*tt + 50*s&
          &35) + s25*s35*(-126*tt**2 + 371*tt*s35 - 114*s35**2) + s25**2*(63*tt**2 - 88*tt*&
          &s35 - 86*s35**2)) + s15**2*(-38*s25**3 + 25*s25**2*(tt + 4*s35) + tt*s35*(113*tt&
          & + 20*s35) + 3*s25*(21*tt**2 - 111*tt*s35 + 38*s35**2)))) - 8*me2**3*(-4*tt*s15*&
          &*5*s35 + s15**4*s35*(14*tt**2 - 9*tt*s25 - 6*s25**2 - 9*tt*s35 + 2*s25*s35) + 4*&
          &ss**3*(s15 - s35)*(19*s15**2*s25 - 19*s25*(s25 - s35)*(-tt + s35) + s15*(19*s25*&
          &*2 + 19*s25*(tt - 2*s35) + 44*tt*s35)) + s15**3*(6*s25**3*(tt - 3*s35) + tt*s35*&
          &(-112*tt**2 + 60*tt*s35 + 3*s35**2) + s25**2*(-29*tt**2 - 5*tt*s35 + 4*s35**2) +&
          & s25*(20*tt**3 + 121*tt**2*s35 - 46*tt*s35**2 + 2*s35**3)) - s25*s35*(-tt + s35)&
          &*(8*s25**4 + 8*s25**3*(-8*tt + 5*s35) + s25**2*(77*tt**2 - 24*tt*s35 - 28*s35**2&
          &) + tt*s35*(20*tt**2 - 29*tt*s35 + 6*s35**2) - 2*s25*(10*tt**3 + 24*tt**2*s35 - &
          &41*tt*s35**2 + 10*s35**3)) + s15**2*(s25**4*(6*tt - 4*s35) + s25**3*(-23*tt**2 -&
          & 96*tt*s35 + 68*s35**2) + tt*s35*(50*tt**3 + 77*tt**2*s35 - 70*tt*s35**2 + 10*s3&
          &5**3) + s25**2*(-9*tt**3 + 328*tt**2*s35 - 182*tt*s35**2 + 38*s35**3) + s25*(20*&
          &tt**4 - 252*tt**3*s35 + 13*tt**2*s35**2 + 42*tt*s35**3 - 10*s35**4)) + s15*(8*s2&
          &5**5*s35 + tt**2*s35**2*(-50*tt**2 + 35*tt*s35 - 4*s35**2) + s25**4*(6*tt**2 - 8&
          &2*tt*s35 + 44*s35**2) + s25**3*(-29*tt**3 + 118*tt**2*s35 + 62*tt*s35**2 - 78*s3&
          &5**3) + s25**2*(20*tt**4 + 21*tt**3*s35 - 371*tt**2*s35**2 + 293*tt*s35**3 - 56*&
          &s35**4) + s25*s35*(-40*tt**4 + 249*tt**3*s35 - 163*tt**2*s35**2 + 11*tt*s35**3 +&
          & 6*s35**4)) - 2*ss**2*(s25*(s25 - s35)*s35*(-tt + s35)*(-79*tt + 70*s25 + 44*s35&
          &) + s15**3*(-79*tt*s25 + 44*s25**2 + 72*tt*s35 + 44*s25*s35) + s15**2*(44*s25**3&
          & - 12*tt*s35*(16*tt + s35) - s25**2*(35*tt + 114*s35) + s25*(-79*tt**2 + 439*tt*&
          &s35 - 132*s35**2)) + s15*(s25**3*(44*tt - 114*s35) + 12*tt*(16*tt - 5*s35)*s35**&
          &2 + s25**2*(-79*tt**2 + 114*tt*s35 + 96*s35**2) + s25*s35*(158*tt**2 - 483*tt*s3&
          &5 + 132*s35**2))) + 2*ss*(s15**4*(11*tt + 4*s25)*s35 + s25*(s25 - s35)*s35*(-tt &
          &+ s35)*(53*tt**2 - 108*tt*s25 + 36*s25**2 - 50*tt*s35 + 68*s25*s35 + 10*s35**2) &
          &+ s15**3*(10*s25**3 + 2*tt*s35*(-62*tt + 19*s35) + s25**2*(-50*tt + 46*s35) + s2&
          &5*(53*tt**2 + 26*tt*s35 - 2*s35**2)) + s15**2*(10*s25**4 - 2*s25**3*(20*tt + 7*s&
          &35) + s25**2*(3*tt**2 + 298*tt*s35 - 150*s35**2) + tt*s35*(131*tt**2 + 53*tt*s35&
          & - 42*s35**2) + s25*(53*tt**3 - 456*tt**2*s35 + 162*tt*s35**2 - 18*s35**3)) - s1&
          &5*(2*s25**4*(-5*tt + 23*s35) + tt*s35**2*(131*tt**2 - 71*tt*s35 + 7*s35**2) + 2*&
          &s25**3*(25*tt**2 - 90*tt*s35 + 14*s35**2) + s25**2*(-53*tt**3 + 50*tt**2*s35 + 3&
          &38*tt*s35**2 - 162*s35**3) + 2*s25*s35*(53*tt**3 - 250*tt**2*s35 + 124*tt*s35**2&
          & - 13*s35**3)))) + 2*me2**2*(2*tt*s15**5*s35*(-8*tt + 6*s25 + 5*s35) + 8*ss**4*(&
          &s15 - s35)*(7*s15**2*s25 - 7*s25*(s25 - s35)*(-tt + s35) + s15*(7*s25**2 + 7*s25&
          &*(tt - 2*s35) + 26*tt*s35)) + s15**4*s35*(4*s25**3 + s25**2*(15*tt + 2*s35) + tt&
          &*(24*tt**2 - 5*tt*s35 - 16*s35**2) + s25*(-55*tt**2 + 23*tt*s35 - 4*s35**2)) + s&
          &25*s35*(-tt + s35)*(24*s25**4*(-tt + s35) + tt**2*s35*(-20*tt**2 + 41*tt*s35 - 2&
          &1*s35**2) + s25**3*(78*tt**2 - 88*tt*s35 + 8*s35**2) + s25**2*(-63*tt**3 + 19*tt&
          &**2*s35 + 80*tt*s35**2 - 32*s35**3) + 2*tt*s25*(10*tt**3 + 11*tt**2*s35 - 38*tt*&
          &s35**2 + 16*s35**3)) + s15**3*(4*s25**4*s35 + s25**3*(21*tt**2 + 13*tt*s35 + 2*s&
          &35**2) - s25**2*(41*tt**3 + 122*tt**2*s35 - 49*tt*s35**2 + 14*s35**3) + tt*s35*(&
          &-110*tt**3 + 65*tt**2*s35 - 3*tt*s35**2 + 14*s35**3) + s25*(20*tt**4 + 191*tt**3&
          &*s35 - 48*tt**2*s35**2 - 39*tt*s35**3 + 4*s35**4)) + s15*(-24*s25**5*s35*(-tt + &
          &s35) + s25**4*(21*tt**3 - 101*tt**2*s35 + 66*tt*s35**2 + 8*s35**3) + 2*tt**2*s35&
          &**2*(-30*tt**3 + 51*tt**2*s35 - 33*tt*s35**2 + 10*s35**3) + s25**3*(-41*tt**4 + &
          &53*tt**3*s35 + 216*tt**2*s35**2 - 284*tt*s35**3 + 78*s35**4) + s25**2*(20*tt**5 &
          &+ 69*tt**4*s35 - 426*tt**3*s35**2 + 398*tt**2*s35**3 - 89*tt*s35**4 - 2*s35**5) &
          &- s25*s35*(40*tt**5 - 240*tt**4*s35 + 254*tt**3*s35**2 - 77*tt**2*s35**3 + tt*s3&
          &5**4 + 4*s35**5)) + s15**2*(s25**4*(21*tt**2 + 34*tt*s35 - 20*s35**2) - s25**3*(&
          &20*tt**3 + 249*tt**2*s35 - 175*tt*s35**2 + 52*s35**3) + tt*s35*(60*tt**4 + 8*tt*&
          &*3*s35 - 23*tt**2*s35**2 + 4*tt*s35**3 - 8*s35**4) + s25**2*(-21*tt**4 + 426*tt*&
          &*3*s35 - 174*tt**2*s35**2 - 11*tt*s35**3 + 14*s35**4) + s25*(20*tt**5 - 267*tt**&
          &4*s35 + 50*tt**3*s35**2 + 16*tt**2*s35**3 + 13*tt*s35**4 + 4*s35**5)) - 4*ss**3*&
          &(2*s25*(s25 - s35)*s35*(-tt + s35)*(-21*tt + 17*s25 + 11*s35) + s15**3*(-42*tt*s&
          &25 + 22*s25**2 + 49*tt*s35 + 22*s25*s35) + s15**2*(22*s25**3 + 3*tt*s35*(-53*tt &
          &+ 2*s35) - 4*s25**2*(5*tt + 14*s35) - 6*s25*(7*tt**2 - 44*tt*s35 + 11*s35**2)) +&
          & s15*(s25**3*(22*tt - 56*s35) + tt*(159*tt - 55*s35)*s35**2 + 2*s25*s35*(42*tt**&
          &2 - 143*tt*s35 + 33*s35**2) + s25**2*(-42*tt**2 + 62*tt*s35 + 46*s35**2))) + ss*&
          &*2*(2*s15**4*(32*tt + 5*s25)*s35 + 4*s25*(s25 - s35)*s35*(-tt + s35)*(49*tt**2 -&
          & 82*tt*s25 + 26*s25**2 - 44*tt*s35 + 50*s25*s35 + 8*s35**2) + s15**3*(32*s25**3 &
          &+ 7*tt*s35*(-66*tt + 13*s35) + 2*s25**2*(-88*tt + 69*s35) + 2*s25*(98*tt**2 + 68&
          &*tt*s35 + s35**2)) + s15**2*(32*s25**4 - 8*s25**3*(18*tt + 5*s35) + s25**2*(20*t&
          &t**2 + 1036*tt*s35 - 444*s35**2) + tt*s35*(724*tt**2 - 43*tt*s35 - 71*s35**2) + &
          &s25*(196*tt**3 - 1857*tt**2*s35 + 628*tt*s35**2 - 66*s35**3)) - s15*(8*s25**4*(-&
          &4*tt + 17*s35) + 8*s25**3*(22*tt**2 - 71*tt*s35 + 11*s35**2) + tt*s35**2*(724*tt&
          &**2 - 505*tt*s35 + 84*s35**2) + s25*s35*(392*tt**3 - 2007*tt**2*s35 + 972*tt*s35&
          &**2 - 86*s35**3) - 2*s25**2*(98*tt**3 - 95*tt**2*s35 - 554*tt*s35**2 + 237*s35**&
          &3))) + ss*(-16*tt*s15**5*s35 + s15**4*s35*(88*tt**2 - 65*tt*s25 - 14*s25**2 - 31&
          &*tt*s35 + 2*s25*s35) + s15**3*(s25**3*(32*tt - 54*s35) - s25**2*(139*tt**2 + 13*&
          &tt*s35 + 4*s35**2) + tt*s35*(-376*tt**2 + 130*tt*s35 + 35*s35**2) + s25*(104*tt*&
          &*3 + 403*tt**2*s35 - 104*tt*s35**2 + 10*s35**3)) - s25*s35*(-tt + s35)*(24*s25**&
          &4 + 8*s25**3*(-23*tt + 14*s35) + s25**2*(253*tt**2 - 104*tt*s35 - 72*s35**2) + t&
          &t*s35*(104*tt**2 - 139*tt*s35 + 32*s35**2) - 2*s25*(52*tt**3 + 57*tt**2*s35 - 12&
          &8*tt*s35**2 + 32*s35**3)) + s15*(24*s25**5*s35 + 32*s25**4*(tt**2 - 8*tt*s35 + 4&
          &*s35**2) + s25**3*(-139*tt**3 + 438*tt**2*s35 + 160*tt*s35**2 - 214*s35**3) + tt&
          &*s35**2*(-356*tt**3 + 397*tt**2*s35 - 156*tt*s35**2 + 28*s35**3) + s25**2*(104*t&
          &t**4 + 29*tt**3*s35 - 1343*tt**2*s35**2 + 1015*tt*s35**3 - 176*s35**4) + s25*s35&
          &*(-208*tt**4 + 1281*tt**3*s35 - 939*tt**2*s35**2 + 137*tt*s35**3 + 14*s35**4)) -&
          & s15**2*(16*s25**4*(-2*tt + s35) + s25**3*(107*tt**2 + 304*tt*s35 - 196*s35**2) &
          &+ s25**2*(35*tt**3 - 1264*tt**2*s35 + 682*tt*s35**2 - 130*s35**3) + tt*s35*(-356&
          &*tt**3 + 21*tt**2*s35 + 62*tt*s35**2 + 16*s35**3) + s25*(-104*tt**4 + 1240*tt**3&
          &*s35 - 391*tt**2*s35**2 - 48*tt*s35**3 + 26*s35**4)))) - me2*(-(tt*s15**5*(-tt +&
          & s25)*s35*(-10*tt + 4*s25 + 9*s35)) + 8*ss**5*(s15 - s35)*(s15**2*s25 - s25*(s25&
          & - s35)*(-tt + s35) + s15*(tt*s25 + s25**2 + 8*tt*s35 - 2*s25*s35)) + s25*s35*(-&
          &tt + s35)**2*(-8*s25**4*(-tt + s35) + tt*s25**2*(11*tt**2 + 3*tt*s35 - 16*s35**2&
          &) + tt**2*s35*(4*tt**2 - 7*tt*s35 + 3*s35**2) + s25**3*(-15*tt**2 + 8*tt*s35 + 8&
          &*s35**2) + tt**2*s25*(-4*tt**2 - 4*tt*s35 + 9*s35**2)) + s15**4*s35*(-(s25**3*(7&
          &*tt + 2*s35)) + 6*tt**2*(tt**2 + tt*s35 - 2*s35**2) + s25**2*(34*tt**2 - 15*tt*s&
          &35 + 2*s35**2) + tt*s25*(-31*tt**2 + 4*tt*s35 + 15*s35**2)) - s15**3*(s25**4*(3*&
          &tt**2 + 5*tt*s35 + 2*s35**2) + tt**2*s35*(24*tt**3 - 17*tt**2*s35 + 3*tt*s35**2 &
          &- 7*s35**3) - s25**3*(10*tt**3 + 33*tt**2*s35 - 14*tt*s35**2 + 6*s35**3) + s25**&
          &2*(11*tt**4 + 46*tt**3*s35 + 22*tt**2*s35**2 - 38*tt*s35**3 + 2*s35**4) + tt*s25&
          &*(-4*tt**4 - 49*tt**3*s35 - 3*tt**2*s35**2 + 23*tt*s35**3 + 12*s35**4)) + s15*(-&
          &tt + s35)*(s25**5*(3*tt**2 - 6*tt*s35 + 8*s35**2) + 2*tt**2*s35**2*(12*tt**3 - 1&
          &8*tt**2*s35 + 11*tt*s35**2 - 2*s35**3) - 2*s25**4*(5*tt**3 - 2*tt**2*s35 - 5*tt*&
          &s35**2 + 8*s35**3) + s25**3*(11*tt**4 + 16*tt**3*s35 - 63*tt**2*s35**2 + 38*tt*s&
          &35**3 - 2*s35**4) + tt*s25*s35*(8*tt**4 - 66*tt**3*s35 + 68*tt**2*s35**2 - 32*tt&
          &*s35**3 + s35**4) + s25**2*(-4*tt**5 - 29*tt**4*s35 + 100*tt**3*s35**2 - 60*tt**&
          &2*s35**3 + 13*tt*s35**4 + 2*s35**5)) - s15**2*(tt*s25**5*(3*tt + 2*s35) - s25**4&
          &*(7*tt**3 + 29*tt**2*s35 - 23*tt*s35**2 + 10*s35**3) + tt**3*s35*(-24*tt**3 + 36&
          &*tt**2*s35 - 35*tt*s35**2 + 19*s35**3) + s25**3*(tt**4 + 86*tt**3*s35 - 43*tt**2&
          &*s35**2 - 3*tt*s35**3 + 2*s35**4) + s25**2*(7*tt**5 - 132*tt**4*s35 + 103*tt**3*&
          &s35**2 - 55*tt**2*s35**3 + 34*tt*s35**4 + 2*s35**5) - tt*s25*(4*tt**5 - 85*tt**4&
          &*s35 + 89*tt**3*s35**2 - 76*tt**2*s35**3 + 43*tt*s35**4 + 5*s35**5)) - 8*ss**4*(&
          &s25*(s25 - s35)*s35*(-tt + s35)*(-4*tt + 3*s25 + 2*s35) + 2*s15**3*(s25**2 + 4*t&
          &t*s35 + s25*(-2*tt + s35)) + s15**2*(2*s25**3 + 4*tt*s35*(-8*tt + s35) - s25**2*&
          &(2*tt + 5*s35) + s25*(-4*tt**2 + 35*tt*s35 - 6*s35**2)) + s15*(s25**3*(2*tt - 5*&
          &s35) + 4*tt*(8*tt - 3*s35)*s35**2 + s25**2*(-4*tt**2 + 6*tt*s35 + 4*s35**2) + s2&
          &5*s35*(8*tt**2 - 37*tt*s35 + 6*s35**2))) + 2*ss**3*(s15**4*(17*tt + s25)*s35 + s&
          &25*(s25 - s35)*s35*(-tt + s35)*(27*tt**2 + 12*s25**2 - 24*tt*s35 + 4*s35**2 + 8*&
          &s25*(-5*tt + 3*s35)) + s15**3*(4*s25**3 + tt*s35*(-94*tt + 11*s35) + s25**2*(-24&
          &*tt + 17*s35) + s25*(27*tt**2 + 43*tt*s35 + s35**2)) - s15*(4*s25**4*(-tt + 4*s3&
          &5) + 12*s25**3*(2*tt**2 - 6*tt*s35 + s35**2) + tt*s35**2*(209*tt**2 - 164*tt*s35&
          & + 31*s35**2) + s25**2*(-27*tt**3 + 26*tt**2*s35 + 173*tt*s35**2 - 57*s35**3) + &
          &s25*s35*(54*tt**3 - 395*tt**2*s35 + 185*tt*s35**2 - 11*s35**3)) + s15**2*(4*s25*&
          &*4 - 4*s25**3*(5*tt + s35) + s25**2*(3*tt**2 + 169*tt*s35 - 54*s35**2) + tt*s35*&
          &(209*tt**2 - 70*tt*s35 + 3*s35**2) - 3*s25*(-9*tt**3 + 125*tt**2*s35 - 38*tt*s35&
          &**2 + 3*s35**3))) + ss**2*(-10*tt*s15**5*s35 - s15**4*s35*(61*tt*s25 + 4*s25**2 &
          &+ tt*(-74*tt + 19*s35)) + s15**3*(-4*s25**3*(-4*tt + 5*s35) - s25**2*(64*tt**2 +&
          & 41*tt*s35 + 6*s35**2) + 4*tt*s35*(-52*tt**2 + 9*tt*s35 + 7*s35**2) + s25*(48*tt&
          &**3 + 239*tt**2*s35 - 15*tt*s35**2 + 4*s35**3)) - 2*s25*s35*(-tt + s35)*(4*s25**&
          &4 + 4*s25**3*(-8*tt + 5*s35) + s25**2*(49*tt**2 - 24*tt*s35 - 12*s35**2) + 8*tt*&
          &s35*(3*tt**2 - 4*tt*s35 + s35**2) - s25*(24*tt**3 + 17*tt**2*s35 - 48*tt*s35**2 &
          &+ 12*s35**3)) + s15*(8*s25**5*s35 + 16*s25**4*(tt**2 - 6*tt*s35 + 3*s35**2) + s2&
          &5**3*(-64*tt**3 + 182*tt**2*s35 + 74*tt*s35**2 - 74*s35**3) + tt*s35**2*(-348*tt&
          &**3 + 437*tt**2*s35 - 185*tt*s35**2 + 30*s35**3) + s25**2*(48*tt**4 + 4*tt**3*s3&
          &5 - 694*tt**2*s35**2 + 473*tt*s35**3 - 66*s35**4) + s25*s35*(-96*tt**4 + 879*tt*&
          &*3*s35 - 719*tt**2*s35**2 + 139*tt*s35**3 + 4*s35**4)) - s15**2*(8*s25**4*(-2*tt&
          & + s35) + s25**3*(48*tt**2 + 140*tt*s35 - 70*s35**2) + s25**2*(16*tt**3 - 676*tt&
          &**2*s35 + 314*tt*s35**2 - 52*s35**3) + tt*s35*(-348*tt**3 + 229*tt**2*s35 - 75*t&
          &t*s35**2 + 29*s35**3) + s25*(-48*tt**4 + 851*tt**3*s35 - 412*tt**2*s35**2 + 51*t&
          &t*s35**3 + 8*s35**4))) + ss*(tt*s15**5*s35*(-20*tt + 14*s25 + 9*s35) + s15**4*s3&
          &5*(2*s25**3 + 2*s25**2*(18*tt + s35) + tt*(46*tt**2 - 13*tt*s35 - 12*s35**2) + s&
          &25*(-94*tt**2 + 29*tt*s35 - 2*s35**2)) + s25*s35*(-tt + s35)*(16*s25**4*(-tt + s&
          &35) + tt**2*s35*(-22*tt**2 + 43*tt*s35 - 21*s35**2) + s25**3*(55*tt**2 - 64*tt*s&
          &35 + 8*s35**2) + s25**2*(-53*tt**3 + 31*tt**2*s35 + 48*tt*s35**2 - 24*s35**3) + &
          &tt*s25*(22*tt**3 + 10*tt**2*s35 - 65*tt*s35**2 + 32*s35**3)) + s15**3*(2*s25**4*&
          &s35 + 3*s25**3*(7*tt**2 + 8*tt*s35 + 2*s35**2) + tt*s35*(-108*tt**3 + 31*tt**2*s&
          &35 + 25*tt*s35**2 + 7*s35**3) - s25**2*(43*tt**3 + 119*tt**2*s35 - 6*tt*s35**2 +&
          & 10*s35**3) + s25*(22*tt**4 + 186*tt**3*s35 + 11*tt**2*s35**2 - 60*tt*s35**3 + 2&
          &*s35**4)) + s15*(-16*s25**5*s35*(-tt + s35) + tt*s25**4*(21*tt**2 - 84*tt*s35 + &
          &52*s35**2) + tt*s35**2*(-146*tt**4 + 265*tt**3*s35 - 181*tt**2*s35**2 + 56*tt*s3&
          &5**3 - 4*s35**4) + s25**3*(-43*tt**4 + 70*tt**3*s35 + 176*tt**2*s35**2 - 220*tt*&
          &s35**3 + 60*s35**4) + s25**2*(22*tt**5 + 49*tt**4*s35 - 508*tt**3*s35**2 + 500*t&
          &t**2*s35**3 - 124*tt*s35**4 - 2*s35**5) - s25*s35*(44*tt**5 - 435*tt**4*s35 + 54&
          &4*tt**3*s35**2 - 230*tt**2*s35**3 + 34*tt*s35**4 + 2*s35**5)) + s15**2*(s25**4*(&
          &21*tt**2 + 22*tt*s35 - 10*s35**2) + tt**2*s35*(146*tt**3 - 157*tt**2*s35 + 104*t&
          &t*s35**2 - 48*s35**3) - 2*s25**3*(11*tt**3 + 111*tt**2*s35 - 68*tt*s35**2 + 22*s&
          &35**3) + s25**2*(-21*tt**4 + 510*tt**3*s35 - 294*tt**2*s35**2 + 48*tt*s35**3 + 1&
          &0*s35**4) + s25*(22*tt**5 - 442*tt**4*s35 + 330*tt**3*s35**2 - 146*tt**2*s35**3 &
          &+ 55*tt*s35**4 + 2*s35**5)))))*pve2(1))/(tt*s15*s25*(4*me2 - ss - tt + s25)*(tt &
          &+ s15 - s35)*(s15 + s25 - s35)*s35*(4*me2 - ss - tt + s35)))/(16.*Pi**2)

          !print*,penta

  penta = penta + (128*Pi**2*((-4*me2)/(s15*(4*me2 - ss - tt + s25)) - (2*me2 - ss + tt + &
          &s15 + s25)/(tt*s15) + (2*me2 - ss + 3*tt + 4*s15 + s25 - 3*s35)/(s25*(tt + s15 -&
          & s35)) - (8*me2)/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + (-2*me2 + ss - 3*&
          &(tt + s15 - s35))/((tt + s15 - s35)*(s15 + s25 - s35)) + (2*me2 - ss + tt)/(tt*s&
          &35) + (8*me2)/(s25*(4*me2 - ss - tt + s35)) + (4*me2)/(s35*(4*me2 - ss - tt + s3&
          &5)))*pvb1(1) + 128*Pi**2*((-4*me2)/(s15*(4*me2 - ss - tt + s25)) - (4*me2 - 2*ss&
          & + s15 + 2*s25)/(tt*s15) - (6*me2)/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) +&
          & (4*me2 - 2*ss + tt)/(tt*s35) + (6*me2)/(s35*(4*me2 - ss - tt + s35)) + (-2*me2 &
          &+ ss - 2*tt - s15 + s25 + s35)/((tt + s15 - s35)*(s15 + s25 - s35)))*pvb11(1) + &
          &128*Pi**2*(1/tt + 1/s15 + (6*me2)/(s15*(4*me2 - ss - tt + s25)) + (2*me2 - ss + &
          &tt + s25)/(tt*s15) + (-4*me2 + 2*ss + tt - s25)/((tt + s15 - s35)*(s15 + s25 - s&
          &35)) + (2*me2 - ss + s15 + s25 - s35)/((tt + s15 - s35)*(s15 + s25 - s35)) - (2*&
          &(2*me2 - ss + tt + 2*s15 + s25 - s35))/(s25*(tt + s15 - s35)) - 1/s35 - (8*me2)/&
          &(s25*(4*me2 - ss - tt + s35)) - (2*me2)/(s35*(4*me2 - ss - tt + s35)))*pvb2(1) +&
          & 128*Pi**2*(1/s15 + (2*me2)/(s15*(4*me2 - ss - tt + s25)) + (-2*me2 + ss + tt - &
          &s35)/(s25*(tt + s15 - s35)) + (8*me2)/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)&
          &) + (6*me2 - 3*ss + tt + s15 - s35)/((tt + s15 - s35)*(s15 + s25 - s35)) - 1/s35&
          & - (6*me2)/(s35*(4*me2 - ss - tt + s35)) + (-2*me2 + ss - tt + s35)/(tt*s35))*pv&
          &b3(1) + 128*Pi**2*(-(1/tt) - (4*me2)/(s15*(4*me2 - ss - tt + s25)) - (2*me2 - ss&
          & + tt + s25)/(tt*s15) - (-2*me2 + ss + tt - s35)/(s25*(tt + s15 - s35)) - (4*me2&
          &)/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + 2/s35 + (4*me2)/(s35*(4*me2 - ss&
          & - tt + s35)) + (-2*tt - s15 + s25 + s35)/((tt + s15 - s35)*(s15 + s25 - s35)))*&
          &pvb4(1) + 128*Pi**2*((4*me2)/(s15*(4*me2 - ss - tt + s25)) + (2*me2 - ss + tt + &
          &s15 + s25)/(tt*s15) - (3*tt + 3*s15 + s25 - 3*s35)/(s25*(tt + s15 - s35)) + (-2*&
          &me2 + ss + tt + s15 - s35)/((tt + s15 - s35)*(s15 + s25 - s35)) + (-2*me2 + ss -&
          & s35)/(tt*s35) - (6*me2)/(s25*(4*me2 - ss - tt + s35)) - (2*me2)/(s35*(4*me2 - s&
          &s - tt + s35)))*pvb5(1) + 128*Pi**2*(1/tt + (2*me2 - ss + s25)/(tt*s15) + (2*me2&
          &)/(s15*(4*me2 - ss - tt + s25)) + (-2*me2 + ss - s15)/(s25*(tt + s15 - s35)) + (&
          &6*me2)/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + (3*tt + 2*s15 - s25 - 2*s35&
          &)/((tt + s15 - s35)*(s15 + s25 - s35)) - 1/s35 - (2*me2)/(s25*(4*me2 - ss - tt +&
          & s35)) - (2*me2)/(s35*(4*me2 - ss - tt + s35)))*pvb7(1) + 128*Pi**2*((4*me2)/(s1&
          &5*(4*me2 - ss - tt + s25)) + (4*me2 - 2*ss + s15 + 2*s25)/(tt*s15) + (4*me2)/((4&
          &*me2 - ss - tt + s25)*(s15 + s25 - s35)) + (2*tt + s15 - s25 - s35)/((tt + s15 -&
          & s35)*(s15 + s25 - s35)) + (-2*me2 + ss - tt)/(tt*s35) - (4*me2)/(s35*(4*me2 - s&
          &s - tt + s35)))*pvb8(1) + 128*Pi**2*(-((me2*(6*me2 + 2*ss - tt - 3*s15 + s25 - 2&
          &*s35))/(s15*(4*me2 - ss - tt + s25))) + (10*me2**2 + me2*(-6*ss - tt + s25) + ss&
          &*(ss + 2*tt + s15 + s25 - 2*s35))/(s25*(tt + s15 - s35)) - (2*me2*(me2 - 3*ss - &
          &tt + 4*s15 + 2*s25))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + (-10*me2**2 -&
          & 4*ss**2 + 2*me2*(6*ss + 3*tt - 6*s15 - 5*s25) + ss*(-3*tt + 5*s15 + 5*s25) - (-&
          &tt + s25)*(3*s15 + 2*s25 - s35))/((tt + s15 - s35)*(s15 + s25 - s35)) - (2*me2*(&
          &-me2 + tt + s35))/(s35*(4*me2 - ss - tt + s35)) + (me2*(14*me2 - 2*ss - 3*tt + s&
          &15 + s25 + 2*s35))/(s25*(4*me2 - ss - tt + s35)) - (-2*me2**2 + ss**2 + tt*s15 +&
          & ss*(tt + s15 - 3*s35) - tt*s35 + 2*s25*s35 + me2*(-2*ss + 2*tt - s15 + s25 + 2*&
          &s35))/(tt*s35) + (-2*me2**2 + 2*ss**2 - tt*s15 - 2*tt*s25 + 2*s15*s25 + 2*s25**2&
          & + s25*s35 - ss*(-tt + 3*s15 + 4*s25 + s35) + me2*(-4*ss - tt + 7*s15 + 6*s25 + &
          &2*s35))/(tt*s15))*pvc11(1) + 128*Pi**2*((-2*me2 + ss - s25)/s15 - (2*me2*tt)/(s1&
          &5*(4*me2 - ss - tt + s25)) + (2*me2*(-2*me2 + ss + 2*tt))/((4*me2 - ss - tt + s2&
          &5)*(s15 + s25 - s35)) + (-4*me2**2 - ss**2 - 3*ss*tt + me2*(4*ss + 6*tt) + tt*(-&
          &tt + s25))/((tt + s15 - s35)*(s15 + s25 - s35)) + (-2*me2 + ss + tt)**2/(tt*s35)&
          &)*pvc11(2) + 128*Pi**2*(((-tt + s15)*(2*me2 - ss + s25))/(tt*s15) + (2*me2*(-tt &
          &+ s15))/(s15*(4*me2 - ss - tt + s25)) - (2*me2*(2*me2 - 2*tt + s15))/((4*me2 - s&
          &s - tt + s25)*(s15 + s25 - s35)) + (-4*me2**2 + 2*me2*(ss + 3*tt - 2*s15) + ss*(&
          &-3*tt + 2*s15) + (-tt + s15)*(tt - s25))/((tt + s15 - s35)*(s15 + s25 - s35)) + &
          &((2*me2 - tt)*(2*me2 - ss - tt))/(tt*s35))*pvc11(3) + 128*Pi**2*((-2*me2 + ss - &
          &s25)/(tt*s15) - (2*me2)/(s15*(4*me2 - ss - tt + s25)) + (2*me2)/((4*me2 - ss - t&
          &t + s25)*(s15 + s25 - s35)) + (4*me2 - 2*ss - tt + s25)/((tt + s15 - s35)*(s15 +&
          & s25 - s35)))*(-0.5 + 4*pvc11(4)) + 128*me2*Pi**2*((-2*me2 + ss - s25)/(tt*s15) &
          &- (2*me2)/(s15*(4*me2 - ss - tt + s25)) + (2*me2)/((4*me2 - ss - tt + s25)*(s15 &
          &+ s25 - s35)) + (4*me2 - 2*ss - tt + s25)/((tt + s15 - s35)*(s15 + s25 - s35)))*&
          &pvc11(5) + 128*Pi**2*(-2*me2 + ss - s25)*((2*me2 - ss + s25)/(tt*s15) + (2*me2)/&
          &(s15*(4*me2 - ss - tt + s25)) - (2*me2)/((4*me2 - ss - tt + s25)*(s15 + s25 - s3&
          &5)) + (-4*me2 + 2*ss + tt - s25)/((tt + s15 - s35)*(s15 + s25 - s35)))*pvc11(6) &
          &+ 128*Pi**2*(me2 - s15)*((-2*me2 + ss - s25)/(tt*s15) - (2*me2)/(s15*(4*me2 - ss&
          & - tt + s25)) + (2*me2)/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + (4*me2 - 2&
          &*ss - tt + s25)/((tt + s15 - s35)*(s15 + s25 - s35)))*pvc11(7) + 128*Pi**2*((-10&
          &*me2**2 + me2*(5*ss + 3*tt) + ss*(-ss + s15 + s25))/(s25*(4*me2 - ss - tt + s35)&
          &) + (-8*me2**2 - 2*ss**2 - 2*tt**2 + s15**2 + 2*tt*s25 + s15*s25 - ss*(3*tt + s1&
          &5 - 2*s25 - 2*s35) + me2*(8*ss + 3*tt + s15 - 4*s25 - s35) + 2*tt*s35 - s15*s35 &
          &- 2*s25*s35)/(tt*s15) + (8*me2**2 + 2*ss**2 + ss*(s15 - 2*s35) - (s15 + s25)*(-t&
          &t + s35) + me2*(-8*ss + tt - s15 + s35))/(tt*s35) + (-8*me2**2 - 3*ss**2 + me2*(&
          &10*ss - tt - 6*s15 - 4*s25 - 3*s35) - (s15 + s25)*(tt + s15 - s35) + 2*ss*(s15 +&
          & s25 + s35))/((tt + s15 - s35)*(s15 + s25 - s35)) + (10*me2**2 + ss*(ss - s35) +&
          & me2*(-5*ss + s15 + 2*s25 + s35))/(s35*(4*me2 - ss - tt + s35)) + (-6*me2**2 + s&
          &s*(-ss + s15 + s25) + me2*(5*ss - 3*tt - 2*s25 + 2*s35))/(s15*(4*me2 - ss - tt +&
          & s25)) + (-ss**2 + ss*(tt + s15 - s35) + (-tt + s35)*(-tt + s25 + s35) + me2*(2*&
          &ss - 5*tt - 2*s15 + 2*s25 + 5*s35))/(s25*(tt + s15 - s35)) + (-6*me2**2 + ss*(-s&
          &s + s35) + me2*(7*ss - 9*s15 - 2*(tt + 3*s25 + s35)))/((4*me2 - ss - tt + s25)*(&
          &s15 + s25 - s35)))*pvc13(1) + 128*Pi**2*(ss*(-(1/tt) + 1/s35) - (ss*(-2*me2 + ss&
          & + tt - s35))/(s25*(tt + s15 - s35)) - (2*me2*(2*ss + s15))/((4*me2 - ss - tt + &
          &s25)*(s15 + s25 - s35)) + (2*me2*ss)/(s35*(4*me2 - ss - tt + s35)) + (-2*me2*(ss&
          & + s15) + ss*(ss - tt + s35))/((tt + s15 - s35)*(s15 + s25 - s35)))*pvc13(2) + 1&
          &28*Pi**2*(-((ss**2*(s15 - s35))/(s25*(tt + s15 - s35)*(s15 + s25 - s35))) + 2*me&
          &2*(s35/(s25*(tt + s15 - s35)) + ((-3/(4*me2 - ss - tt + s25) - 2/(tt + s15 - s35&
          &))*s35)/(s15 + s25 - s35) + 1/(4*me2 - ss - tt + s35)) + ss*(-(1/tt) + me2*(2/(s&
          &25*(tt + s15 - s35)) + (2*(-2/(4*me2 - ss - tt + s25) - 1/(tt + s15 - s35)))/(s1&
          &5 + s25 - s35)) - tt/(s25*(tt + s15 - s35)) - tt/((tt + s15 - s35)*(s15 + s25 - &
          &s35)) - s15/((tt + s15 - s35)*(s15 + s25 - s35)) + (3*s35)/((tt + s15 - s35)*(s1&
          &5 + s25 - s35)) + (1 + (2*me2)/(4*me2 - ss - tt + s35))/s35) + (s15**2*s25*(tt -&
          & s35) - (-tt + s35)*(s25**2*(tt - s35) + tt*s35**2 + s25*s35*(-3*tt + s35)) + s1&
          &5*(s25**2*(tt - s35) + tt*s35*(-tt + s35) + s25*(tt**2 - 4*tt*s35 + 2*s35**2)))/&
          &(tt*s25*(tt + s15 - s35)*(s15 + s25 - s35)))*pvc13(3) + 128*Pi**2*((-2*me2*(7*me&
          &2 + ss - 3*tt + s15))/(s15*(4*me2 - ss - tt + s25)) + (-6*me2**2 + me2*(2*ss + t&
          &t - 2*s15 - 2*s25) - (ss - s15 - s25)*(2*tt + s15 - s25 - s35))/((tt + s15 - s35&
          &)*(s15 + s25 - s35)) + (2*me2*(3*me2 - ss - tt))/(s25*(4*me2 - ss - tt + s35)) +&
          & (me2*(6*me2 + 2*ss - 3*tt - s15 - 3*s25 + s35))/(s35*(4*me2 - ss - tt + s35)) -&
          & (me2*(10*me2 - 3*tt - 3*s15 - s25 + s35))/((4*me2 - ss - tt + s25)*(s15 + s25 -&
          & s35)) + (-2*me2**2 + 3*ss**2 + tt**2 - tt*s15 - s15**2 - 2*tt*s25 + s25**2 + ss&
          &*(2*tt - 4*s25 - 3*s35) - me2*(6*ss + 2*tt + s15 - 3*s25 - 2*s35) - tt*s35 + s15&
          &*s35 + 3*s25*s35)/(tt*s15) + (2*me2**2 - 2*ss**2 - ss*s15 - tt*s15 - tt*s25 + 2*&
          &ss*s35 + s15*s35 + me2*(4*ss - tt + s15 - s25 + 2*s35))/(tt*s35) + (2*me2**2 + (&
          &ss + tt - s35)*(ss - tt + s25 + s35) - 2*me2*(ss - tt + s25 + 2*s35))/(s25*(tt +&
          & s15 - s35)))*pvc14(1) + 128*Pi**2*(((6*me2 - ss - tt + s25)*(s15 + s25))/(s15*(&
          &4*me2 - ss - tt + s25)) + ss**2*(1/(tt*s15) + 1/(s25*(tt + s15 - s35)) - 1/(tt*s&
          &35)) + ss*((-((tt + s25)/tt) + me2*(-2/tt - 4/(4*me2 - ss - tt + s25)))/s15 + (t&
          &t - s35)/(s25*(tt + s15 - s35)) + me2*(-2/(s25*(tt + s15 - s35)) + (2*(1/tt + 1/&
          &(4*me2 - ss - tt + s35)))/s35)))*pvc14(2) + 128*Pi**2*((-2*me2*(4*me2 - 2*tt + s&
          &35))/(s15*(4*me2 - ss - tt + s25)) + ((2*me2 - ss + s35)*(2*me2 - tt + s35))/(tt&
          &*s35) - ((2*me2 - tt + s35)*(2*me2 - ss - tt + s35))/(s25*(tt + s15 - s35)) + (2&
          &*me2*(2*me2 - tt + s35))/(s35*(4*me2 - ss - tt + s35)) + (-4*me2**2 + tt**2 + tt&
          &*s15 + tt*s25 + 2*me2*(ss - s15 - s25 - s35) - s15*s35 - s25*s35 + ss*(-tt + s35&
          &))/(tt*s15))*pvc14(3) + 128*Pi**2*((me2*(12*me2 + 2*ss - 4*tt + 3*s15 + s25 + s3&
          &5))/(s15*(4*me2 - ss - tt + s25)) - (me2*(12*me2 + 2*ss - 4*tt - 2*s15 - 3*s25 +&
          & 2*s35))/(s35*(4*me2 - ss - tt + s35)) - (me2*(12*me2 + 6*ss - 4*tt - 3*s15 - s2&
          &5 + 5*s35))/(s25*(4*me2 - ss - tt + s35)) + (me2*(12*me2 + 6*ss - 4*tt - 8*s15 -&
          & 5*s25 + 6*s35))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + (4*me2**2 - 2*ss*&
          &*2 - 3*tt*s15 - s15**2 - 3*tt*s25 + s15*s25 + 2*s25**2 + me2*(2*ss + 2*s15 + s25&
          &) + 2*ss*(tt + s15 - s35) + tt*s35 + s15*s35)/((tt + s15 - s35)*(s15 + s25 - s35&
          &)) - (4*me2**2 - 2*ss**2 - 2*ss*s15 - 2*tt*s15 - 2*tt*s25 + me2*(2*ss + 2*s15 + &
          &s25) + 2*ss*s35 + tt*s35 + s15*s35)/(tt*s35) + (-4*me2**2 + 2*ss**2 - 2*tt*s15 -&
          & tt*s25 + s15*s25 - ss*(2*tt + 6*s15 + 4*s25 - 3*s35) + s15*s35 + s25*s35 - me2*&
          &(2*ss - 3*s15 - 3*s25 + s35))/(s25*(tt + s15 - s35)) + (4*me2**2 - 2*ss**2 + 3*t&
          &t*s15 + s15**2 + 2*tt*s25 - 2*s15*s25 - 2*s25**2 - s15*s35 - s25*s35 + me2*(2*ss&
          & - 3*s15 - 3*s25 + s35) + ss*(2*s15 + 4*s25 + s35))/(tt*s15))*pvc15(1) + 128*Pi*&
          &*2*(-2 + ss**2*(-(1/(tt*s15)) + (s15 - s35)/(s25*(tt + s15 - s35)*(s15 + s25 - s&
          &35)) + 1/(tt*s35)) + s35/tt + (s25*s35)/(tt*s15) - (s15*s35)/(s25*(tt + s15 - s3&
          &5)) + (2*tt*s35)/((tt + s15 - s35)*(s15 + s25 - s35)) + (s15*s35)/((tt + s15 - s&
          &35)*(s15 + s25 - s35)) - (s25*s35)/((tt + s15 - s35)*(s15 + s25 - s35)) - s35**2&
          &/((tt + s15 - s35)*(s15 + s25 - s35)) + ss*(1/tt + (me2*(2/tt + 6/(4*me2 - ss - &
          &tt + s25)) + (2*tt + s25 - s35)/tt)/s15 - 1/(tt + s15 - s35) - (2*me2)/(s25*(tt &
          &+ s15 - s35)) - (2*tt)/(s25*(tt + s15 - s35)) + (6*me2)/((4*me2 - ss - tt + s25)&
          &*(s15 + s25 - s35)) + (2*me2)/((tt + s15 - s35)*(s15 + s25 - s35)) + (2*tt)/((tt&
          & + s15 - s35)*(s15 + s25 - s35)) - (s15*(3*s15 + s25 - 3*s35))/(s25*(tt + s15 - &
          &s35)*(s15 + s25 - s35)) - 2/s35 - (2*me2)/(tt*s35) + (3*s35)/(s25*(tt + s15 - s3&
          &5)) - (2*s35)/((tt + s15 - s35)*(s15 + s25 - s35)) - (6*me2)/(s25*(4*me2 - ss - &
          &tt + s35)) - (6*me2)/(s35*(4*me2 - ss - tt + s35))) + me2*((2*(1/tt + 1/(4*me2 -&
          & ss - tt + s25))*s35)/s15 + (4*s35)/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) &
          &- 4/(4*me2 - ss - tt + s35) + (s35*(-2/(tt + s15 - s35) - 2/(4*me2 - ss - tt + s&
          &35)))/s25))*pvc15(2) + 128*Pi**2*(2 + s15/tt + ((2*me2 - ss + s25)*s35)/(tt*s15)&
          & + (2*me2*(2*s15 + s35))/(s15*(4*me2 - ss - tt + s25)) - (2*me2*(2*s15 + s35))/(&
          &s25*(4*me2 - ss - tt + s35)) + (2*me2*(s15 + 2*s35))/((4*me2 - ss - tt + s25)*(s&
          &15 + s25 - s35)) - (2*me2*(s15 + 2*s35))/(s35*(4*me2 - ss - tt + s35)) + (2*me2*&
          &s15 - ss*s15 + s15**2 + s15*s25 + 2*tt*s35 - s25*s35 - s35**2)/((tt + s15 - s35)&
          &*(s15 + s25 - s35)) + (-2*me2*s15 + ss*s15 + s35*(-2*tt - s15 + s35))/(tt*s35) +&
          & (-2*s15**2 + (-2*me2 + ss)*s35 + s15*(-2*tt - s25 + s35))/(s25*(tt + s15 - s35)&
          &))*pvc15(3) + 128*Pi**2*((12*me2**2 + 2*ss**2 - 3*ss*(s15 + s25) + (s15 + s25)**&
          &2 + me2*(-12*ss + 2*tt + 10*s15 + 10*s25 - 3*s35))/(s15*(4*me2 - ss - tt + s25))&
          & - (me2*(4*me2 + 2*ss - 4*tt - 5*s15 - 3*s25))/((4*me2 - ss - tt + s25)*(s15 + s&
          &25 - s35)) - (me2*(8*me2 + 3*s15 + 3*s25 + 4*s35))/(s35*(4*me2 - ss - tt + s35))&
          & + (2*ss**2 + me2*(-4*ss + 2*tt + 3*s15 - 2*s35) + ss*(-2*s15 - 2*s25 + s35) + (&
          &tt + s15 - s35)*(2*s15 + 2*s25 + s35))/((tt + s15 - s35)*(s15 + s25 - s35)) - (4&
          &*me2**2 + 6*ss**2 + tt**2 + s15**2 + 4*s15*s25 + 2*s25**2 + me2*(-14*ss + 2*tt +&
          & 8*s15 + 8*s25 - 3*s35) - 2*tt*s35 + s35**2 - 2*ss*(-tt + 3*s15 + 4*s25 + s35))/&
          &(s25*(tt + s15 - s35)) + (-4*me2**2 + me2*(2*ss - 2*tt - 6*s15 - 3*s25) + s35*(-&
          &2*tt - 2*s15 - s25 + 2*s35))/(tt*s35) + (8*me2**2 + 4*ss**2 + tt**2 - 2*tt*s15 -&
          & s15**2 - 2*tt*s25 + 2*s15*s25 + 2*s25**2 + me2*(-12*ss + 2*tt + 11*s15 + 11*s25&
          & - s35) - tt*s35 + 3*s15*s35 + 3*s25*s35 - ss*(-2*tt + 4*s15 + 6*s25 + 3*s35))/(&
          &tt*s15) + (-2*ss**2 + 3*ss*(s15 + s25) - (s15 + s25)**2 + me2*(14*ss - 6*tt - 12&
          &*s15 - 10*s25 + 7*s35))/(s25*(4*me2 - ss - tt + s35)))*pvc18(1) + 128*Pi**2*((-2&
          &*me2*(4*me2 - ss - 2*tt + s35))/(s15*(4*me2 - ss - tt + s25)) + (6*me2*(2*me2 - &
          &ss - tt))/(s25*(4*me2 - ss - tt + s35)) + (2*me2*(-2*me2 + 2*ss + tt + s35))/((4&
          &*me2 - ss - tt + s25)*(s15 + s25 - s35)) + (-4*me2**2 + tt**2 + 2*tt*s15 + tt*s2&
          &5 + 2*me2*(ss - 2*s15 - s25 - s35) - s15*s35 - s25*s35 + ss*(s15 + s35))/(tt*s15&
          &) + (8*me2**2 + ss**2 - tt**2 - 3*tt*s15 - 2*me2*(3*ss + tt - 3*s15 - 2*s25) - 2&
          &*tt*s25 + s25*s35 + s35**2 + ss*(-3*s15 - s25 + s35))/(s25*(tt + s15 - s35)) + (&
          &-4*me2**2 - ss**2 + tt*s15 + tt*s25 + ss*(2*s15 + s25 - 2*s35) + s15*s35 - s35**&
          &2 + 2*me2*(2*ss + tt - s15 - s25 + s35))/((tt + s15 - s35)*(s15 + s25 - s35)))*p&
          &vc18(2) + 128*Pi**2*((-2*me2*(s15 - s25))/(s15*(4*me2 - ss - tt + s25)) + (-2*me&
          &2*s15 + ss*s15 - s15**2 + tt*s25)/(tt*s15) + (2*me2*(s15 + 2*s25))/((4*me2 - ss &
          &- tt + s25)*(s15 + s25 - s35)) + (s15**2 + s25*(2*me2 - ss + tt + s25 - 2*s35) +&
          & s15*(tt + 2*s25 - s35))/((tt + s15 - s35)*(s15 + s25 - s35)) - (6*me2)/(4*me2 -&
          & ss - tt + s35) + (-(tt*s15) + 2*me2*(s15 - s25) - 2*tt*s25 - 2*s15*s25 - s25**2&
          & + ss*(-s15 + s25) + s15*s35 + 2*s25*s35)/(s25*(tt + s15 - s35)))*pvc18(3) + 128&
          &*Pi**2*((-2*me2**2 + me2*(4*ss + tt - 5*s15 - 3*s25) - ss*(ss + tt - 2*s15 - s25&
          &))/(tt*s15) + (-6*me2**2 + me2*(4*ss - tt + 2*s15) - ss*(ss + tt - s35))/(s25*(t&
          &t + s15 - s35)) - (me2*(10*me2 + 7*ss - 4*(tt + 2*s15 + s25 - s35)))/((4*me2 - s&
          &s - tt + s25)*(s15 + s25 - s35)) + (6*me2**2 + ss*(2*tt + s25 - 2*s35) - me2*(4*&
          &ss + s15 + s25 - 2*s35))/(tt*s35) - (me2*(10*me2 - 3*ss - tt + 2*s15 + 2*s25 - s&
          &35))/(s25*(4*me2 - ss - tt + s35)) - (me2*(6*me2 - 3*ss - 3*tt + 2*s15 + 3*s35))&
          &/(s15*(4*me2 - ss - tt + s25)) + (me2*(14*me2 - ss - 4*tt - s15 - s25 + 4*s35))/&
          &(s35*(4*me2 - ss - tt + s35)) + (-2*me2**2 - 2*me2*(3*ss - 3*s15 - 3*s25 + s35) &
          &+ ss*(4*ss - 3*s15 - 3*s25 + s35))/((tt + s15 - s35)*(s15 + s25 - s35)))*pvc2(1)&
          & + 256*Pi**2*(((2*me2 - ss)*(2*me2 - tt + s35))/(2.*tt*s35) + (me2*(2*me2 - tt +&
          & s35))/(s35*(4*me2 - ss - tt + s35)) - ((2*me2 - ss)*(2*me2 - tt - s15/2. + s35)&
          &)/((tt + s15 - s35)*(s15 + s25 - s35)) - (me2*(4*me2 - 2*tt - s15 + 2*s35))/((4*&
          &me2 - ss - tt + s25)*(s15 + s25 - s35)))*pvc2(2) + 256*Pi**2*((me2*(-2*ss + s15 &
          &+ s25 - 2*s35))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + ((2*me2 - ss)*(ss &
          &+ s35))/(2.*tt*s35) + (me2*(ss + s35))/(s35*(4*me2 - ss - tt + s35)) - ((2*me2 -&
          & ss)*(2*ss - s15 - s25 + 2*s35))/(2.*(tt + s15 - s35)*(s15 + s25 - s35)))*pvc2(3&
          &) + 128*Pi**2*((22*me2**2 + 3*ss**2 - 4*ss*(s15 + s25) + (s15 + s25)**2 + me2*(-&
          &27*ss + 4*tt + 13*s15 + 13*s25 - 6*s35))/(s25*(4*me2 - ss - tt + s35)) + (16*me2&
          &**2 + 8*ss**2 + tt**2 + s15**2 + 4*s15*s25 + 2*s25**2 + me2*(-24*ss + 5*tt + 17*&
          &s15 + 14*s25 - 6*s35) - ss*(tt + 11*s15 + 10*s25 - s35) - 2*tt*s35 + s35**2)/(s2&
          &5*(tt + s15 - s35)) + (10*me2**2 + me2*(-5*ss + 8*s15 + 3*(tt + s25)) + ss*(ss -&
          & s15 - s25 + s35))/(s35*(4*me2 - ss - tt + s35)) + (8*me2**2 + 2*ss**2 + 2*tt**2&
          & + tt*s15 - me2*(8*ss + tt - 6*s15 - 4*s25) - tt*s25 + 2*s15*s35 + 2*s25*s35 - 2&
          &*s35**2 - ss*(-3*tt + 2*s25 + s35))/(tt*s35) + (-8*me2**2 - 2*ss**2 + 2*tt*s15 -&
          & 4*s15*s25 - 2*s25**2 - 2*s15*s35 - s25*s35 + ss*(tt + 5*s15 + 4*s25 + s35) + me&
          &2*(8*ss - 3*tt - 12*s15 - 8*s25 + 2*s35))/(tt*s15) + (-14*me2**2 - (-ss + s15 + &
          &s25)**2 + me2*(7*ss - 6*s15 - 5*s25 + 2*s35))/(s15*(4*me2 - ss - tt + s25)) + (-&
          &8*me2**2 - 4*ss**2 + ss*(-2*tt + 2*s15 + 4*s25 - s35) - (tt + s15 - s35)*(2*tt +&
          & s15 - s25 + s35) + me2*(12*ss + tt - 3*s15 - 4*s25 + 2*s35))/((tt + s15 - s35)*&
          &(s15 + s25 - s35)) + (-6*me2**2 + ss*(-ss + s15 + s25 - s35) + me2*(11*ss - 5*tt&
          & - 6*s15 - 3*s25 + 2*s35))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)))*pvc22(1)&
          & + 128*Pi**2*((-(s15**3*(-4*tt + s25)) + tt*s25*(s25 - s35)*(-tt + s35) - s15**2&
          &*(2*ss*tt - 2*tt**2 - 3*tt*s25 + s25**2 - ss*s35 + 5*tt*s35 + s35**2) + s15*(tt*&
          &(tt - s25)*s25 - 2*tt**2*s35 + tt*s35**2 + s35**3 - ss*(s25*(tt - s35) + s35*(-2&
          &*tt + s35))))/(s15*s25*(tt + s15 - s35)*(s15 + s25 - s35)) + me2*((-2/(s25*(tt +&
          & s15 - s35)) - 2/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)))*s35 + 2*tt*(-(1/(s&
          &15*(4*me2 - ss - tt + s25))) + (1/(-4*me2 + ss + tt - s25) - 1/(tt + s15 - s35))&
          &/(s15 + s25 - s35) + (2/(tt + s15 - s35) + 4/(4*me2 - ss - tt + s35))/s25)))*pvc&
          &22(2) + 128*Pi**2*(-(((tt + s15)*(2*me2 - ss + s25))/(tt*s15)) - (2*me2*(2*me2 -&
          & ss + s25))/(s15*(4*me2 - ss - tt + s25)) - (2*me2*(2*me2 - 2*ss + s15 + 2*s25))&
          &/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + (8*me2*(2*me2 - ss + s25))/(s25*(&
          &4*me2 - ss - tt + s35)) + (8*me2**2 + ss**2 + tt*s15 + 3*tt*s25 + 3*s15*s25 + s2&
          &5**2 + me2*(-6*ss + 4*tt + 6*s15 + 6*s25 - 4*s35) - ss*(3*tt + 3*s15 + 2*s25 - 3&
          &*s35) - s15*s35 - 3*s25*s35)/(s25*(tt + s15 - s35)) - (4*me2**2 + ss**2 + tt*s15&
          & + s15**2 + tt*s25 + 2*s15*s25 + s25**2 - ss*(tt + 2*s15 + 2*s25 - 2*s35) - s15*&
          &s35 - 2*s25*s35 - 2*me2*(2*ss - s15 - 2*s25 + s35))/((tt + s15 - s35)*(s15 + s25&
          & - s35)))*pvc22(3) + 128*Pi**2*(-((tt + s15)/(tt*s15)) - (2*me2)/(s15*(4*me2 - s&
          &s - tt + s25)) - (2*me2)/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + (2*(2*me2&
          & - ss + tt + 2*s15 + s25 - s35))/(s25*(tt + s15 - s35)) + (8*me2)/(s25*(4*me2 - &
          &ss - tt + s35)) + (-2*me2 + ss - s15 - s25 + s35)/((tt + s15 - s35)*(s15 + s25 -&
          & s35)))*(-0.5 + 4*pvc22(4)) + 128*Pi**2*(-((tt + s15)/s15) - (2*me2*tt)/(s15*(4*&
          &me2 - ss - tt + s25)) - (2*me2*tt)/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) -&
          & (tt*(2*me2 - ss + s15 + s25 - s35))/((tt + s15 - s35)*(s15 + s25 - s35)) + (2*t&
          &t*(2*me2 - ss + tt + 2*s15 + s25 - s35))/(s25*(tt + s15 - s35)) + (8*me2*tt)/(s2&
          &5*(4*me2 - ss - tt + s35)))*pvc22(5) + 128*Pi**2*(-tt + s15)*(1/tt + 1/s15 + (2*&
          &me2)/(s15*(4*me2 - ss - tt + s25)) + (2*me2)/((4*me2 - ss - tt + s25)*(s15 + s25&
          & - s35)) + (2*me2 - ss + s15 + s25 - s35)/((tt + s15 - s35)*(s15 + s25 - s35)) -&
          & (2*(2*me2 - ss + tt + 2*s15 + s25 - s35))/(s25*(tt + s15 - s35)) - (8*me2)/(s25&
          &*(4*me2 - ss - tt + s35)))*pvc22(6) + 128*Pi**2*(me2 - s15)*(-((tt + s15)/(tt*s1&
          &5)) - (2*me2)/(s15*(4*me2 - ss - tt + s25)) - (2*me2)/((4*me2 - ss - tt + s25)*(&
          &s15 + s25 - s35)) + (2*(2*me2 - ss + tt + 2*s15 + s25 - s35))/(s25*(tt + s15 - s&
          &35)) + (8*me2)/(s25*(4*me2 - ss - tt + s35)) + (-2*me2 + ss - s15 - s25 + s35)/(&
          &(tt + s15 - s35)*(s15 + s25 - s35)))*pvc22(7) + 128*Pi**2*(-((me2*(2*me2 - ss + &
          &2*s25))/(s15*(4*me2 - ss - tt + s25))) + (-2*me2**2 + me2*(4*ss - 3*s15 - 5*s25)&
          & - (ss - s15 - s25)*(ss + 2*tt + s15 - s25 - s35))/(tt*s15) + (-2*me2**2 + 2*me2&
          &*ss + ss*(-ss + s15 + s25))/(s25*(tt + s15 - s35)) - (me2*(2*me2 - 3*ss + s15 + &
          &s25))/(s25*(4*me2 - ss - tt + s35)) + (me2*(6*me2 - ss - tt - s25 + s35))/((4*me&
          &2 - ss - tt + s25)*(s15 + s25 - s35)) + (me2*(10*me2 - 3*ss - 3*tt + 3*s25 + s35&
          &))/(s35*(4*me2 - ss - tt + s35)) + (6*me2**2 + (ss + tt)*(ss - s15 - s25) - me2*&
          &(6*ss + tt - 4*s15 - 4*s25 + s35))/(tt*s35) + (2*me2**2 + (ss - s15 - s25)*(tt +&
          & s15 - s35) + me2*(tt + s15 + s25 + s35))/((tt + s15 - s35)*(s15 + s25 - s35)))*&
          &pvc3(1) + (128*Pi**2*((-2*me2)/(4*me2 - ss - tt + s25) - (2*me2 - ss + s15 + s25&
          &)/tt)*(-2*me2 + ss + tt - s35)*pvc3(2))/s15 + (128*Pi**2*(ss - s15)*(8*me2**2 + &
          &(ss + tt - s25)*(ss - s15 - s25) + me2*(-6*ss + 4*s15 + 6*s25))*pvc3(3))/(tt*s15&
          &*(-4*me2 + ss + tt - s25)) + 128*Pi**2*((me2*(12*me2 - 2*ss - 3*tt + s25))/((4*m&
          &e2 - ss - tt + s25)*(s15 + s25 - s35)) + (10*me2**2 - 2*me2*(2*ss + tt - s15 - s&
          &25) - (ss - s15 - s25)*(-tt + s25))/((tt + s15 - s35)*(s15 + s25 - s35)) + (-10*&
          &me2**2 + 3*me2*(ss + tt - s35))/(s25*(4*me2 - ss - tt + s35)) + (me2*(12*me2 - 7&
          &*tt + 2*s15 + s35))/(s15*(4*me2 - ss - tt + s25)) - (me2*(10*me2 - 5*tt - s15 - &
          &2*s25 + s35))/(s35*(4*me2 - ss - tt + s35)) + (-4*me2**2 + ss**2 + me2*(s15 + 2*&
          &(tt + s25)) + s25*s35 - ss*(tt + s25 + s35))/(tt*s35) - (-4*me2**2 + ss**2 - tt*&
          &s15 - me2*(-4*tt + s15) + s15*s25 + s25**2 + s25*s35 - ss*(tt + 2*s25 + s35))/(t&
          &t*s15) + (-10*me2**2 - ss*(ss + tt - s35) + me2*(6*ss - 2*(-2*tt + s15 + s25 + s&
          &35)))/(s25*(tt + s15 - s35)))*pvc6(1) + 128*Pi**2*((-2*me2 + ss - s25)/s15 - (2*&
          &me2*(2*me2 - ss + s25))/(s15*(4*me2 - ss - tt + s25)) + (2*me2 - ss)/s35 - (2*me&
          &2*(-2*me2 + ss))/(s35*(4*me2 - ss - tt + s35)))*pvc6(2) + (128*Pi**2*(24*me2**2*&
          &tt*(s15 - s35) + tt*(ss + tt - s25)*(ss + tt - s35)*(s15 - s35) - 2*me2*(tt*s35*&
          &(-5*ss - 5*tt + 2*s25 + 3*s35) + s15*(5*tt*(ss + tt) - 2*tt*s35 - s35**2 + s25*(&
          &-3*tt + s35))))*pvc6(3))/(s15*(4*me2 - ss - tt + s25)*s35*(4*me2 - ss - tt + s35&
          &)) + 128*Pi**2*((-1 - (2*me2)/(4*me2 - ss - tt + s25))/s15 + (1 + (2*me2)/(4*me2&
          & - ss - tt + s35))/s35)*(-0.5 + 4*pvc6(4)) + 128*me2*Pi**2*((-1 - (2*me2)/(4*me2&
          & - ss - tt + s25))/s15 + (1 + (2*me2)/(4*me2 - ss - tt + s35))/s35)*pvc6(5) + (1&
          &28*tt*Pi**2*(24*me2**2*(s15 - s35) + (ss + tt - s25)*(ss + tt - s35)*(s15 - s35)&
          & + 2*me2*(-5*ss*(s15 - s35) + (5*tt - 2*s25 - 3*s35)*s35 + s15*(-5*tt + 3*s25 + &
          &2*s35)))*pvc6(6))/(s15*(4*me2 - ss - tt + s25)*s35*(4*me2 - ss - tt + s35)) + 12&
          &8*tt*Pi**2*((-1 - (2*me2)/(4*me2 - ss - tt + s25))/s15 + (1 + (2*me2)/(4*me2 - s&
          &s - tt + s35))/s35)*pvc6(7) + 128*Pi**2*((me2*(8*me2 - 7*s15 - 3*s25))/(s15*(4*m&
          &e2 - ss - tt + s25)) + (ss*(2*ss - s15 - s25) + me2*(-14*ss + 6*tt + 17*s15 + 10&
          &*s25 - 8*s35))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + (4*me2**2 + 6*ss**2&
          & + ss*(2*tt - 3*s15 - 5*s25) + me2*(-14*ss + 2*tt + 11*s15 + 8*s25 - 4*s35) + (t&
          &t + s15)*(tt + s15 - s35))/((tt + s15 - s35)*(s15 + s25 - s35)) + (me2*(4*me2 + &
          &2*ss - 4*tt - 3*s15 - 3*s25 + 4*s35))/(s25*(4*me2 - ss - tt + s35)) + (-8*me2**2&
          & - 4*ss**2 - tt**2 - 2*tt*s15 - tt*s25 + tt*s35 + s15*s35 + s25*s35 + ss*(-2*tt &
          &- s15 + 2*s25 + s35) + me2*(12*ss - 2*tt - 2*s15 - s25 + 2*s35))/(tt*s35) + (4*m&
          &e2**2 + s15*(-2*tt - s15 + s25 + s35) - me2*(2*ss - 2*tt + s15 + s25 + 3*s35))/(&
          &tt*s15) + (-12*me2**2 + ss*(-2*ss + s15 + s25) + me2*(12*ss - 2*tt - 7*s15 - 4*s&
          &25 + 4*s35))/(s35*(4*me2 - ss - tt + s35)) + (-2*ss**2 + 2*tt*s15 + tt*s25 - s15&
          &*s25 + ss*(4*s15 + 3*s25 - s35) - s15*s35 - s25*s35 + me2*(4*ss - 2*tt - 8*s15 -&
          & 6*s25 + 5*s35))/(s25*(tt + s15 - s35)))*pvc9(1) + 128*Pi**2*((6*me2*(-2*me2 + s&
          &s + tt - s35))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + (2*me2*(2*me2 - 2*s&
          &s - tt + s35))/(s25*(4*me2 - ss - tt + s35)) + (2*me2*(4*me2 - ss - 2*tt + 2*s35&
          &))/(s35*(4*me2 - ss - tt + s35)) + (4*me2**2 + ss**2 - 2*ss*s15 - 2*me2*(2*ss + &
          &tt - s15 - s35) + s15*(-tt + s35))/(s25*(tt + s15 - s35)) - (8*me2**2 + ss**2 - &
          &2*ss*s15 - 2*me2*(3*ss + tt - s15 - s35) + (tt + s15 - s35)*(-tt + s35))/((tt + &
          &s15 - s35)*(s15 + s25 - s35)) - (-4*me2**2 + 2*me2*ss + (-tt + s35)**2)/(tt*s35)&
          &)*pvc9(2) + 128*Pi**2*((6*me2)/(4*me2 - ss - tt + s25) + (2*me2*(s15 + s25 - 2*s&
          &35) - ss*(s15 + s25 - 2*s35) + (tt + s15 - s35)*(2*s15 + 2*s25 - s35))/((tt + s1&
          &5 - s35)*(s15 + s25 - s35)) - (2*me2*(2*s15 + 2*s25 - s35))/(s25*(4*me2 - ss - t&
          &t + s35)) - (2*me2*(s15 + s25 - 2*s35))/(s35*(4*me2 - ss - tt + s35)) - (tt*s15 &
          &+ 2*s15**2 + tt*s25 + 2*s15*s25 + 2*me2*(s15 + s25 - s35) - ss*(s15 + s25 - s35)&
          & - 2*s15*s35 - s25*s35)/(s25*(tt + s15 - s35)) + (2*me2*s35 - ss*s35 + (s15 + s2&
          &5 - s35)*(-tt + s35))/(tt*s35))*pvc9(3) + 128*Pi**2*((me2*(24*me2**2 + 4*ss**2 +&
          & 3*s15**2 + 3*s15*s25 + me2*(-24*ss + 4*tt + 26*s15 + 12*s25 - 10*s35) + 2*tt*s3&
          &5 + 3*s15*s35 + s25*s35 - 2*s35**2 + ss*(-5*s15 - 2*s25 + s35)))/(s35*(4*me2 - s&
          &s - tt + s35)) + (72*me2**3 - 2*ss**3 + 4*ss**2*(s15 + s25) + s15*(s15 + s25)**2&
          & - ss*(3*s15**2 + 5*s15*s25 + 2*s25**2) - 2*me2**2*(40*ss + 2*tt - 24*s15 - 25*s&
          &25 + s35) + me2*(24*ss**2 + 2*tt**2 + 3*tt*s15 + 12*s15**2 + 2*tt*s25 + 22*s15*s&
          &25 + 10*s25**2 - 3*tt*s35 - s15*s35 - 3*s25*s35 + 2*s35**2 + ss*(-2*tt - 31*s15 &
          &- 32*s25 + s35)))/(s25*(4*me2 - ss - tt + s35)) + (16*me2**3 - 4*me2**2*(6*ss - &
          &tt - 2*s15 - 2*s25 + s35) + s35*(s15*(tt + s25 - s35) + (ss - 2*s25)*(-tt + s35)&
          &) + me2*(8*ss**2 + 2*tt**2 + 2*tt*s15 + tt*s25 + 3*tt*s35 + s15*s35 - 5*s35**2 +&
          & ss*(4*tt - 3*s15 - 4*s25 + s35)))/(tt*s35) + (-16*me2**3 + 4*me2**2*(6*ss - tt &
          &- 5*s15 - 4*s25) + s15*(-s15**2 + tt*s25 - 2*s25**2 + ss*(-tt + s15 + 2*s25) + t&
          &t*s35 - s35**2 + s15*(tt - 3*s25 + s35)) + me2*(-8*ss**2 - 2*tt**2 - 5*s15**2 + &
          &3*tt*s25 - 4*s25**2 + tt*s35 + 2*s25*s35 + s15*(6*tt - 14*s25 + s35) + ss*(-4*tt&
          & + 13*s15 + 12*s25 + s35)))/(tt*s15) + (-72*me2**3 + 2*ss*(-ss + s15 + s25)**2 +&
          & me2**2*(80*ss - 2*(-2*tt + 24*s15 + 23*s25 + s35)) - me2*(24*ss**2 + 2*tt**2 + &
          &5*tt*s15 + 11*s15**2 + 2*tt*s25 + 17*s15*s25 + 8*s25**2 - tt*s35 + 3*s15*s35 + s&
          &25*s35 - ss*(2*tt + 29*s15 + 28*s25 + s35)))/((4*me2 - ss - tt + s25)*(s15 + s25&
          & - s35)) + (-24*me2**3 - s15*(s15 + s25)*(-ss + s15 + s25) + 2*me2**2*(12*ss - 2&
          &*tt - 13*s15 - 8*s25 + 7*s35) + me2*(-4*ss**2 - 4*s15**2 + ss*(7*s15 + 6*s25 - 3&
          &*s35) + s15*(2*tt - 8*s25 + s35) + s25*(-2*s25 + 3*s35)))/(s15*(4*me2 - ss - tt &
          &+ s25)) + (-48*me2**3 + 4*ss**3 + (s15 + s25)*(tt + s15 - s35)*(-tt + s25 - s35)&
          & + 4*me2**2*(18*ss - tt - 9*s15 - 9*s25 + s35) - 2*ss**2*(-tt + 3*s15 + 4*s25 + &
          &s35) + ss*(tt**2 - tt*s15 + 2*s15**2 - 3*tt*s25 + 5*s15*s25 + 4*s25**2 - tt*s35 &
          &+ 2*s15*s35 + 3*s25*s35) - me2*(32*ss**2 + 4*tt**2 + 6*tt*s15 + 7*s15**2 + 10*s1&
          &5*s25 + 6*s25**2 - 2*tt*s35 + 4*s15*s35 + 4*s25*s35 - 2*s35**2 - 2*ss*(-4*tt + 1&
          &3*s15 + 17*s25 + 3*s35)))/((tt + s15 - s35)*(s15 + s25 - s35)) + (48*me2**3 - 4*&
          &ss**3 + tt**2*s15 + s15**3 + tt**2*s25 - tt*s15*s25 + 2*s15**2*s25 - tt*s25**2 +&
          & s15*s25**2 + me2**2*(-72*ss + 4*tt + 48*s15 + 44*s25) - 2*tt*s15*s35 - 2*tt*s25&
          &*s35 + s15*s25*s35 + s25**2*s35 + s15*s35**2 + s25*s35**2 + 2*ss**2*(-tt + 3*s15&
          & + 4*s25 + s35) - ss*(3*s15**2 + 4*s25**2 + 3*s25*(-tt + s35) + (-tt + s35)**2 +&
          & s15*(-2*tt + 7*s25 + 2*s35)) + me2*(32*ss**2 + 4*tt**2 + 12*s15**2 - 4*tt*s25 +&
          & 10*s25**2 - 6*tt*s35 + 2*s25*s35 + 3*s35**2 + 2*s15*(-tt + 12*s25 + s35) - 2*ss&
          &*(-4*tt + 18*s15 + 21*s25 + 4*s35)))/(s25*(tt + s15 - s35)))*pvd10(1) + 128*ss*P&
          &i**2*((16*me2**2 + (2*ss - s15)*(ss - s15 - s25) + me2*(-12*ss + 2*tt + 10*s15 +&
          & 8*s25 - 3*s35))/(s25*(4*me2 - ss - tt + s35)) + (4*me2**2 + me2*(3*s15 - 2*s35)&
          & + ss*s35)/(s35*(4*me2 - ss - tt + s35)) + (me2*(2*tt + 3*s15 - 5*s35) + (2*ss +&
          & 2*tt + s15 - 2*s35)*s35)/(tt*s35) - (8*me2**2 + 4*ss**2 + tt**2 + 2*tt*s15 + s1&
          &5**2 - 2*ss*(-tt + s15 + 2*s25) + me2*(-12*ss + 4*tt + 10*s15 + 6*s25 - 6*s35) -&
          & s35**2)/((tt + s15 - s35)*(s15 + s25 - s35)) + (8*me2**2 + 4*ss**2 + tt**2 + s1&
          &5**2 + 2*s15*s25 + me2*(-12*ss + 4*tt + 12*s15 + 6*s25 - 4*s35) - 2*tt*s35 + s35&
          &**2 - 2*ss*(-tt + 2*s15 + 2*s25 + s35))/(s25*(tt + s15 - s35)) + (-16*me2**2 - s&
          &s*(2*ss - 2*s15 - 2*s25 + s35) + me2*(12*ss - 2*tt - 11*s15 - 8*s25 + 2*s35))/((&
          &4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + (s15*(2*ss + 2*tt - 2*s25 - s35) + m&
          &e2*(-2*tt - 5*s15 + 3*s35))/(tt*s15) + (-4*me2**2 - s15*(-ss + s15 + s25) + me2*&
          &(-2*s15 + 3*s35))/(s15*(4*me2 - ss - tt + s25)))*pvd10(2) + 128*Pi**2*(ss + s35)&
          &*((16*me2**2 + (2*ss - s15)*(ss - s15 - s25) + me2*(-12*ss + 2*tt + 10*s15 + 8*s&
          &25 - 3*s35))/(s25*(4*me2 - ss - tt + s35)) + (4*me2**2 + me2*(3*s15 - 2*s35) + s&
          &s*s35)/(s35*(4*me2 - ss - tt + s35)) + (me2*(2*tt + 3*s15 - 5*s35) + (2*ss + 2*t&
          &t + s15 - 2*s35)*s35)/(tt*s35) - (8*me2**2 + 4*ss**2 + tt**2 + 2*tt*s15 + s15**2&
          & - 2*ss*(-tt + s15 + 2*s25) + me2*(-12*ss + 4*tt + 10*s15 + 6*s25 - 6*s35) - s35&
          &**2)/((tt + s15 - s35)*(s15 + s25 - s35)) + (8*me2**2 + 4*ss**2 + tt**2 + s15**2&
          & + 2*s15*s25 + me2*(-12*ss + 4*tt + 12*s15 + 6*s25 - 4*s35) - 2*tt*s35 + s35**2 &
          &- 2*ss*(-tt + 2*s15 + 2*s25 + s35))/(s25*(tt + s15 - s35)) + (-16*me2**2 - ss*(2&
          &*ss - 2*s15 - 2*s25 + s35) + me2*(12*ss - 2*tt - 11*s15 - 8*s25 + 2*s35))/((4*me&
          &2 - ss - tt + s25)*(s15 + s25 - s35)) + (s15*(2*ss + 2*tt - 2*s25 - s35) + me2*(&
          &-2*tt - 5*s15 + 3*s35))/(tt*s15) + (-4*me2**2 - s15*(-ss + s15 + s25) + me2*(-2*&
          &s15 + 3*s35))/(s15*(4*me2 - ss - tt + s25)))*pvd10(3) + 128*Pi**2*(s15 + s25)*((&
          &16*me2**2 + (2*ss - s15)*(ss - s15 - s25) + me2*(-12*ss + 2*tt + 10*s15 + 8*s25 &
          &- 3*s35))/(s25*(4*me2 - ss - tt + s35)) + (4*me2**2 + me2*(3*s15 - 2*s35) + ss*s&
          &35)/(s35*(4*me2 - ss - tt + s35)) + (me2*(2*tt + 3*s15 - 5*s35) + (2*ss + 2*tt +&
          & s15 - 2*s35)*s35)/(tt*s35) - (8*me2**2 + 4*ss**2 + tt**2 + 2*tt*s15 + s15**2 - &
          &2*ss*(-tt + s15 + 2*s25) + me2*(-12*ss + 4*tt + 10*s15 + 6*s25 - 6*s35) - s35**2&
          &)/((tt + s15 - s35)*(s15 + s25 - s35)) + (8*me2**2 + 4*ss**2 + tt**2 + s15**2 + &
          &2*s15*s25 + me2*(-12*ss + 4*tt + 12*s15 + 6*s25 - 4*s35) - 2*tt*s35 + s35**2 - 2&
          &*ss*(-tt + 2*s15 + 2*s25 + s35))/(s25*(tt + s15 - s35)) + (-16*me2**2 - ss*(2*ss&
          & - 2*s15 - 2*s25 + s35) + me2*(12*ss - 2*tt - 11*s15 - 8*s25 + 2*s35))/((4*me2 -&
          & ss - tt + s25)*(s15 + s25 - s35)) + (s15*(2*ss + 2*tt - 2*s25 - s35) + me2*(-2*&
          &tt - 5*s15 + 3*s35))/(tt*s15) + (-4*me2**2 - s15*(-ss + s15 + s25) + me2*(-2*s15&
          & + 3*s35))/(s15*(4*me2 - ss - tt + s25)))*pvd10(4) + 512*ss*Pi**2*((-6*me2 + ss &
          &+ tt - s25)/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + (1 + (2*me2)/(4*me2 - &
          &ss - tt + s35))/s25)*pvd10(5) + (128*ss**2*Pi**2*(24*me2**2*(s15 - s35) + (ss + &
          &tt - s25)*(ss + tt - s35)*(s15 - s35) + 2*me2*(-5*ss*s15 - 5*tt*s15 + 3*s15*s25 &
          &+ s25**2 + 5*ss*s35 + 5*tt*s35 + 2*s15*s35 - 4*s25*s35 - 2*s35**2))*pvd10(6))/(s&
          &25*(4*me2 - ss - tt + s25)*(s15 + s25 - s35)*(4*me2 - ss - tt + s35)) + (256*ss*&
          &Pi**2*(ss + s35)*(24*me2**2*(s15 - s35) + (ss + tt - s25)*(ss + tt - s35)*(s15 -&
          & s35) + 2*me2*(-5*ss*s15 - 5*tt*s15 + 3*s15*s25 + s25**2 + 5*ss*s35 + 5*tt*s35 +&
          & 2*s15*s35 - 4*s25*s35 - 2*s35**2))*pvd10(7))/(s25*(4*me2 - ss - tt + s25)*(s15 &
          &+ s25 - s35)*(4*me2 - ss - tt + s35)) + (256*ss*Pi**2*(s15 + s25)*(24*me2**2*(s1&
          &5 - s35) + (ss + tt - s25)*(ss + tt - s35)*(s15 - s35) + 2*me2*(-5*ss*s15 - 5*tt&
          &*s15 + 3*s15*s25 + s25**2 + 5*ss*s35 + 5*tt*s35 + 2*s15*s35 - 4*s25*s35 - 2*s35*&
          &*2))*pvd10(8))/(s25*(4*me2 - ss - tt + s25)*(s15 + s25 - s35)*(4*me2 - ss - tt +&
          & s35)) + (128*Pi**2*(ss + s35)**2*(24*me2**2*(s15 - s35) + (ss + tt - s25)*(ss +&
          & tt - s35)*(s15 - s35) + 2*me2*(-5*ss*s15 - 5*tt*s15 + 3*s15*s25 + s25**2 + 5*ss&
          &*s35 + 5*tt*s35 + 2*s15*s35 - 4*s25*s35 - 2*s35**2))*pvd10(9))/(s25*(4*me2 - ss &
          &- tt + s25)*(s15 + s25 - s35)*(4*me2 - ss - tt + s35)) + (256*Pi**2*(s15 + s25)*&
          &(ss + s35)*(24*me2**2*(s15 - s35) + (ss + tt - s25)*(ss + tt - s35)*(s15 - s35) &
          &+ 2*me2*(-5*ss*s15 - 5*tt*s15 + 3*s15*s25 + s25**2 + 5*ss*s35 + 5*tt*s35 + 2*s15&
          &*s35 - 4*s25*s35 - 2*s35**2))*pvd10(10))/(s25*(4*me2 - ss - tt + s25)*(s15 + s25&
          & - s35)*(4*me2 - ss - tt + s35)) + (128*Pi**2*(s15 + s25)**2*(24*me2**2*(s15 - s&
          &35) + (ss + tt - s25)*(ss + tt - s35)*(s15 - s35) + 2*me2*(-5*ss*s15 - 5*tt*s15 &
          &+ 3*s15*s25 + s25**2 + 5*ss*s35 + 5*tt*s35 + 2*s15*s35 - 4*s25*s35 - 2*s35**2))*&
          &pvd10(11))/(s25*(4*me2 - ss - tt + s25)*(s15 + s25 - s35)*(4*me2 - ss - tt + s35&
          &)) + 128*Pi**2*((me2*(-24*me2**2 + 2*ss**2 + 3*tt*s15 + 3*s15**2 + 3*tt*s25 + 4*&
          &s15*s25 + s25**2 + 2*me2*(4*ss + 4*tt + s15 - s25 - 4*s35) - ss*(4*tt + 5*s15 + &
          &3*s25 - 2*s35) + tt*s35))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + (me2*(24&
          &*me2**2 - 3*s15**2 - 6*s15*s25 - 3*s25**2 - 3*tt*s35 - s15*s35 - 4*s25*s35 + s35&
          &**2 + ss*(3*s15 + 3*s25 + 4*s35) + 2*me2*(2*ss - 4*tt - 5*s15 - 5*s25 + 6*s35)))&
          &/(s35*(4*me2 - ss - tt + s35)) - (48*me2**3 - 2*ss**3 + 4*ss**2*(s15 + s25) - 3*&
          &ss*(s15 + s25)**2 + (s15 + s25)**3 + me2**2*(-44*ss - 8*tt + 28*s15 + 26*s25 + 8&
          &*s35) + me2*(22*ss**2 + 2*tt**2 + 3*tt*s15 + 10*s15**2 + 3*tt*s25 + 20*s15*s25 +&
          & 10*s25**2 - ss*(6*tt + 27*s15 + 27*s25 - 5*s35) - 3*tt*s35 - 3*s15*s35 - 5*s25*&
          &s35 + 3*s35**2))/(s25*(4*me2 - ss - tt + s35)) + (-8*me2**3 - 2*me2**2*(5*s15 + &
          &3*s25 + 2*s35) + me2*(6*ss**2 + 3*tt*s15 - 3*s15**2 + 3*tt*s25 - 9*s15*s25 - 6*s&
          &25**2 - 2*ss*s35 - tt*s35 + 3*s25*s35 - s35**2) - (ss - s15 - s25)*(2*ss**2 + tt&
          &*s25 - s25**2 - ss*(s15 + s25 - s35) + tt*s35 - s35**2 + s15*(tt - s25 + s35)))/&
          &((tt + s15 - s35)*(s15 + s25 - s35)) + (8*me2**3 + me2**2*(4*ss + 6*(s15 + s25))&
          & - (ss - s15 - s25)*s35*(ss - tt - s25 + s35) + me2*(-4*ss**2 - 2*tt*s15 - 2*tt*&
          &s25 - tt*s35 + s15*s35 - s25*s35 + 2*s35**2 + 2*ss*(2*s15 + 2*s25 + s35)))/(tt*s&
          &35) + (48*me2**3 - 2*ss**3 + 4*ss**2*(s15 + s25) - 3*ss*(s15 + s25)**2 + (s15 + &
          &s25)**3 + me2**2*(-56*ss - 8*tt + 36*s15 + 38*s25 + 4*s35) + me2*(20*ss**2 + 2*t&
          &t**2 + 10*s15**2 + 22*s15*s25 + 12*s25**2 - tt*s35 - 2*s15*s35 - s25*s35 + 2*s35&
          &**2 - ss*(2*tt + 25*s15 + 27*s25 + s35)))/(s15*(4*me2 - ss - tt + s25)) + (-32*m&
          &e2**3 + me2**2*(44*ss - 26*s15 - 28*s25) - me2*(26*ss**2 + 2*tt**2 - 26*ss*s15 +&
          & 10*s15**2 - 4*tt*s25 + 10*s25**2 - 3*tt*s35 + 4*s25*s35 + 3*s35**2 - 4*ss*(-tt &
          &+ 8*s25 + s35) + s15*(-2*tt + 20*s25 + s35)) + (ss - s15 - s25)*(6*ss**2 + tt**2&
          & + s15**2 - tt*s25 + 2*s15*s25 + s25**2 - 2*tt*s35 + s25*s35 + s35**2 - ss*(-2*t&
          &t + 2*s15 + 5*s25 + 2*s35)))/(s25*(tt + s15 - s35)) + (32*me2**3 + me2**2*(-48*s&
          &s + 30*s15 + 4*(7*s25 + s35)) + me2*(24*ss**2 + 2*tt**2 - 30*ss*s15 - 3*tt*s15 +&
          & 13*s15**2 - 5*tt*s25 + 29*s15*s25 + 16*s25**2 - tt*s35 + 2*s25*s35 + 2*s35**2 -&
          & 4*ss*(-tt + 9*s25 + s35)) - (ss - s15 - s25)*(4*ss**2 + tt**2 + s15**2 - 2*tt*s&
          &25 + 2*s25**2 + s15*(-tt + 3*s25 - s35) - 2*tt*s35 + 2*s25*s35 + s35**2 - ss*(-2&
          &*tt + s15 + 4*s25 + 4*s35)))/(tt*s15))*pvd11(1) + 128*ss*Pi**2*((2*me2*(6*me2 + &
          &ss - 2*tt))/(s15*(4*me2 - ss - tt + s25)) + (4*me2**2 + 2*me2*(ss + s15) - (ss -&
          & s15 - s25)*(2*ss + s15 - 2*s35))/(tt*s15) - ((2*me2 + 3*ss - s25)*(2*me2 - ss +&
          & s15 + s25))/(s25*(tt + s15 - s35)) - (me2*(4*me2 - 2*ss + 2*s15 + 2*s25 - s35))&
          &/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) - (2*me2*(6*me2 + 2*ss - 2*tt - s15&
          & - s25 + 2*s35))/(s25*(4*me2 - ss - tt + s35)) + (me2*(4*me2 + 3*s35))/(s35*(4*m&
          &e2 - ss - tt + s35)) + (-4*me2**2 + me2*(4*ss - 3*s15 - 3*s25 - s35) - (ss - s15&
          & - s25)*(ss - s15 - s25 + s35))/((tt + s15 - s35)*(s15 + s25 - s35)) + (4*me2**2&
          & + (-ss + s15 + s25)*s35 + me2*(-2*ss + 3*s15 + 3*s25 + s35))/(tt*s35))*pvd11(2)&
          & + 128*Pi**2*(2*me2 - tt + s35)*((2*me2*(6*me2 + ss - 2*tt))/(s15*(4*me2 - ss - &
          &tt + s25)) + (4*me2**2 + 2*me2*(ss + s15) - (ss - s15 - s25)*(2*ss + s15 - 2*s35&
          &))/(tt*s15) - ((2*me2 + 3*ss - s25)*(2*me2 - ss + s15 + s25))/(s25*(tt + s15 - s&
          &35)) - (me2*(4*me2 - 2*ss + 2*s15 + 2*s25 - s35))/((4*me2 - ss - tt + s25)*(s15 &
          &+ s25 - s35)) - (2*me2*(6*me2 + 2*ss - 2*tt - s15 - s25 + 2*s35))/(s25*(4*me2 - &
          &ss - tt + s35)) + (me2*(4*me2 + 3*s35))/(s35*(4*me2 - ss - tt + s35)) + (-4*me2*&
          &*2 + me2*(4*ss - 3*s15 - 3*s25 - s35) - (ss - s15 - s25)*(ss - s15 - s25 + s35))&
          &/((tt + s15 - s35)*(s15 + s25 - s35)) + (4*me2**2 + (-ss + s15 + s25)*s35 + me2*&
          &(-2*ss + 3*s15 + 3*s25 + s35))/(tt*s35))*pvd11(3) + 128*Pi**2*((2*me2*(6*me2 + s&
          &s - 2*tt))/(4*me2 - ss - tt + s25) + (4*me2**2 + 2*me2*(ss + s15) - (ss - s15 - &
          &s25)*(2*ss + s15 - 2*s35))/tt - (s15*(2*me2 + 3*ss - s25)*(2*me2 - ss + s15 + s2&
          &5))/(s25*(tt + s15 - s35)) - (me2*s15*(4*me2 - 2*ss + 2*s15 + 2*s25 - s35))/((4*&
          &me2 - ss - tt + s25)*(s15 + s25 - s35)) - (2*me2*s15*(6*me2 + 2*ss - 2*tt - s15 &
          &- s25 + 2*s35))/(s25*(4*me2 - ss - tt + s35)) + (me2*s15*(4*me2 + 3*s35))/(s35*(&
          &4*me2 - ss - tt + s35)) - (s15*(4*me2**2 + (ss - s15 - s25)*(ss - s15 - s25 + s3&
          &5) + me2*(-4*ss + 3*s15 + 3*s25 + s35)))/((tt + s15 - s35)*(s15 + s25 - s35)) + &
          &(s15*(4*me2**2 + (-ss + s15 + s25)*s35 + me2*(-2*ss + 3*s15 + 3*s25 + s35)))/(tt&
          &*s35))*pvd11(4) + 512*me2*Pi**2*((2*me2)/(s15*(4*me2 - ss - tt + s25)) + (2*me2 &
          &- ss + s15 + s25)/(tt*s15) - (2*me2 - ss + s15 + s25)/(s25*(tt + s15 - s35)) - (&
          &2*me2)/(s25*(4*me2 - ss - tt + s35)))*pvd11(5) + 128*ss**2*Pi**2*((2*me2)/(s15*(&
          &4*me2 - ss - tt + s25)) + (2*me2 - ss + s15 + s25)/(tt*s15) - (2*me2 - ss + s15 &
          &+ s25)/(s25*(tt + s15 - s35)) - (2*me2)/(s25*(4*me2 - ss - tt + s35)))*pvd11(6) &
          &+ 256*ss*Pi**2*(2*me2 - tt + s35)*((2*me2)/(s15*(4*me2 - ss - tt + s25)) + (2*me&
          &2 - ss + s15 + s25)/(tt*s15) - (2*me2 - ss + s15 + s25)/(s25*(tt + s15 - s35)) -&
          & (2*me2)/(s25*(4*me2 - ss - tt + s35)))*pvd11(7) + 256*ss*Pi**2*((2*me2)/(4*me2 &
          &- ss - tt + s25) + (2*me2 - ss + s15 + s25)/tt - (s15*(2*me2 - ss + s15 + s25))/&
          &(s25*(tt + s15 - s35)) - (2*me2*s15)/(s25*(4*me2 - ss - tt + s35)))*pvd11(8) + 1&
          &28*Pi**2*(2*me2 - tt + s35)**2*((2*me2)/(s15*(4*me2 - ss - tt + s25)) + (2*me2 -&
          & ss + s15 + s25)/(tt*s15) - (2*me2 - ss + s15 + s25)/(s25*(tt + s15 - s35)) - (2&
          &*me2)/(s25*(4*me2 - ss - tt + s35)))*pvd11(9) + 256*Pi**2*(2*me2 - tt + s35)*((2&
          &*me2)/(4*me2 - ss - tt + s25) + (2*me2 - ss + s15 + s25)/tt - (s15*(2*me2 - ss +&
          & s15 + s25))/(s25*(tt + s15 - s35)) - (2*me2*s15)/(s25*(4*me2 - ss - tt + s35)))&
          &*pvd11(10) + 128*Pi**2*s15*((2*me2)/(4*me2 - ss - tt + s25) + (2*me2 - ss + s15 &
          &+ s25)/tt - (s15*(2*me2 - ss + s15 + s25))/(s25*(tt + s15 - s35)) - (2*me2*s15)/&
          &(s25*(4*me2 - ss - tt + s35)))*pvd11(11) + 128*Pi**2*((16*me2**3 - 4*ss**3 + s25&
          &*(s15 + s25)**2 + ss**2*(5*s15 + 8*s25) - ss*(s15**2 + 6*s15*s25 + 5*s25**2) + m&
          &e2**2*(-32*ss + 2*tt + 6*s15 + 24*s25 - 6*s35) + me2*(20*ss**2 + tt**2 + tt*s15 &
          &+ 2*s15**2 + 3*tt*s25 + 13*s15*s25 + 10*s25**2 - 2*ss*(tt + 8*s15 + 14*s25 - s35&
          &) + tt*s35 - 2*s25*s35))/(s25*(tt + s15 - s35)) + (24*me2**3 + ss*(ss - s15 - s2&
          &5)*(s15 + s25 - 2*s35) - 2*me2**2*(8*ss + 5*tt - 5*s15 - 12*s25 + s35) + me2*(2*&
          &ss**2 + 3*tt**2 + 4*tt*s15 + 5*s15**2 + tt*s25 + 11*s15*s25 + 6*s25**2 - ss*(tt &
          &+ 9*s15 + 10*s25 - 6*s35) - tt*s35 - 5*s15*s35 - 2*s25*s35))/(s35*(4*me2 - ss - &
          &tt + s35)) + (24*me2**3 - (ss - s25)*(2*ss**2 - 3*ss*(s15 + s25) + (s15 + s25)**&
          &2) - 2*me2**2*(20*ss + tt - 6*s15 - 22*s25 + s35) + me2*(18*ss**2 + tt**2 - 4*tt&
          &*s25 + 10*s15*s25 + 12*s25**2 - ss*(2*tt + 13*s15 + 30*s25 - 3*s35) + tt*s35 + s&
          &25*s35))/(s25*(4*me2 - ss - tt + s35)) + (16*me2**3 - 2*me2**2*(8*ss + tt - 8*s1&
          &5 - 8*s25) + me2*(4*ss**2 + tt**2 + 4*s15**2 - 3*tt*s25 + 4*s25**2 - 2*ss*(-2*tt&
          & + 5*s15 + 6*s25) + s15*(-tt + 8*s25 - 2*s35) - 2*tt*s35 + 4*s25*s35) + (ss - s1&
          &5 - s25)*(ss*(-tt + s15 + 2*s25) + s25*(tt - 2*s35) + tt*(-tt + s35)))/(tt*s35) &
          &+ (-24*me2**3 + 2*me2**2*(8*ss + tt + 3*s15 - 14*s25) + ss**2*(s15 - s25) - s25*&
          &(s15 + s25)**2 + ss*(-s15**2 + s15*s25 + 2*s25**2) + me2*(-2*ss**2 - tt**2 - 3*s&
          &s*s15 + s15**2 + tt*s25 - 10*s25**2 + 2*ss*(tt + 6*s25) - 2*tt*s35 + s15*(-3*tt &
          &- 8*s25 + s35)))/(s15*(4*me2 - ss - tt + s25)) - (16*me2**3 + 2*me2**2*(-8*ss + &
          &tt + 4*s15 + 12*s25) + (ss - s15 - s25)*(tt**2 + 2*tt*s15 - 3*s15*s25 - 2*s25**2&
          & + ss*(s15 + 2*(tt + s25)) - tt*s35) + me2*(4*ss**2 + tt**2 + 3*s15**2 + tt*s25 &
          &+ 12*s25**2 - 2*ss*(tt + 3*s15 + 8*s25) + 2*tt*s35 + s15*(-tt + 16*s25 + s35)))/&
          &(tt*s15) + (16*me2**3 + 2*me2**2*(-8*ss + tt + 9*s15 + 8*s25 + 3*s35) + (ss - s1&
          &5 - s25)*(-((-tt + s25)*(tt + 2*s15 + s25 - s35)) + ss*(tt + s15 + s35)) + me2*(&
          &4*ss**2 + tt**2 + 5*s15**2 - tt*s25 + 6*s25**2 + tt*s35 - 4*ss*(2*s15 + 2*s25 + &
          &s35) + s15*(-tt + 11*s25 + 3*s35)))/((tt + s15 - s35)*(s15 + s25 - s35)) + (24*m&
          &e2**3 - ss*(ss - s15 - s25)*(s15 + s25 - 2*s35) + 2*me2**2*(-8*ss + tt + 6*s15 +&
          & 4*s35) + me2*(2*ss**2 - tt**2 + 2*s15**2 + 3*tt*s25 + 3*s15*s25 + 4*s15*s35 - s&
          &s*(tt + 3*s15 + 8*s35)))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)))*pvd2(1) + &
          &128*(-2*me2 + ss)*Pi**2*((-4*me2**2 + me2*(4*ss - 3*s15 - 4*s25) - (ss + tt - s2&
          &5)*(ss - s15 - s25))/(tt*s15) - (me2*(4*me2 - 2*ss + s15 + 2*s25))/(s15*(4*me2 -&
          & ss - tt + s25)) + (-2*me2**2 + 2*me2*ss + ss*(-ss + s15 + s25))/(s25*(tt + s15 &
          &- s35)) + (2*me2*(2*me2 - ss + s15))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35))&
          & + (2*me2*(me2 + tt + s15))/((tt + s15 - s35)*(s15 + s25 - s35)) + (4*me2**2 + (&
          &ss + tt)*(ss - s15 - s25) + me2*(-4*ss - 2*tt + s15 + 2*s25))/(tt*s35) - (me2*(2&
          &*me2 - 3*ss + s15 + s25))/(s25*(4*me2 - ss - tt + s35)) + (2*me2*(3*me2 - ss - t&
          &t + s25))/(s35*(4*me2 - ss - tt + s35)))*pvd2(2) + 128*Pi**2*(-tt + s35)*((-4*me&
          &2**2 + me2*(4*ss - 3*s15 - 4*s25) - (ss + tt - s25)*(ss - s15 - s25))/(tt*s15) -&
          & (me2*(4*me2 - 2*ss + s15 + 2*s25))/(s15*(4*me2 - ss - tt + s25)) + (-2*me2**2 +&
          & 2*me2*ss + ss*(-ss + s15 + s25))/(s25*(tt + s15 - s35)) + (2*me2*(2*me2 - ss + &
          &s15))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + (2*me2*(me2 + tt + s15))/((t&
          &t + s15 - s35)*(s15 + s25 - s35)) + (4*me2**2 + (ss + tt)*(ss - s15 - s25) + me2&
          &*(-4*ss - 2*tt + s15 + 2*s25))/(tt*s35) - (me2*(2*me2 - 3*ss + s15 + s25))/(s25*&
          &(4*me2 - ss - tt + s35)) + (2*me2*(3*me2 - ss - tt + s25))/(s35*(4*me2 - ss - tt&
          & + s35)))*pvd2(3) + 256*Pi**2*(me2 - s15/2.)*((me2*(4*me2 - 2*ss + s15 + 2*s25))&
          &/(s15*(4*me2 - ss - tt + s25)) + (4*me2**2 + (ss + tt - s25)*(ss - s15 - s25) + &
          &me2*(-4*ss + 3*s15 + 4*s25))/(tt*s15) + (2*me2**2 - 2*me2*ss + ss*(ss - s15 - s2&
          &5))/(s25*(tt + s15 - s35)) - (2*me2*(2*me2 - ss + s15))/((4*me2 - ss - tt + s25)&
          &*(s15 + s25 - s35)) - (2*me2*(me2 + tt + s15))/((tt + s15 - s35)*(s15 + s25 - s3&
          &5)) + (-4*me2**2 + me2*(4*ss + 2*tt - s15 - 2*s25) - (ss + tt)*(ss - s15 - s25))&
          &/(tt*s35) + (me2*(2*me2 - 3*ss + s15 + s25))/(s25*(4*me2 - ss - tt + s35)) - (2*&
          &me2*(3*me2 - ss - tt + s25))/(s35*(4*me2 - ss - tt + s35)))*pvd2(4) + 512*Pi**2*&
          &((me2*(4*me2 - 2*ss + s15 + 2*s25))/(s15*(4*me2 - ss - tt + s25)) + (4*me2**2 + &
          &(ss + tt - s25)*(ss - s15 - s25) + me2*(-4*ss + 3*s15 + 4*s25))/(tt*s15) + (2*me&
          &2**2 - 2*me2*ss + ss*(ss - s15 - s25))/(s25*(tt + s15 - s35)) - (2*me2*(2*me2 - &
          &ss + s15))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) - (2*me2*(me2 + tt + s15)&
          &)/((tt + s15 - s35)*(s15 + s25 - s35)) + (-4*me2**2 + me2*(4*ss + 2*tt - s15 - 2&
          &*s25) - (ss + tt)*(ss - s15 - s25))/(tt*s35) + (me2*(2*me2 - 3*ss + s15 + s25))/&
          &(s25*(4*me2 - ss - tt + s35)) - (2*me2*(3*me2 - ss - tt + s25))/(s35*(4*me2 - ss&
          & - tt + s35)))*pvd2(5) + 128*me2*Pi**2*((me2*(4*me2 - 2*ss + s15 + 2*s25))/(s15*&
          &(4*me2 - ss - tt + s25)) + (4*me2**2 + (ss + tt - s25)*(ss - s15 - s25) + me2*(-&
          &4*ss + 3*s15 + 4*s25))/(tt*s15) + (2*me2**2 - 2*me2*ss + ss*(ss - s15 - s25))/(s&
          &25*(tt + s15 - s35)) - (2*me2*(2*me2 - ss + s15))/((4*me2 - ss - tt + s25)*(s15 &
          &+ s25 - s35)) - (2*me2*(me2 + tt + s15))/((tt + s15 - s35)*(s15 + s25 - s35)) + &
          &(-4*me2**2 + me2*(4*ss + 2*tt - s15 - 2*s25) - (ss + tt)*(ss - s15 - s25))/(tt*s&
          &35) + (me2*(2*me2 - 3*ss + s15 + s25))/(s25*(4*me2 - ss - tt + s35)) - (2*me2*(3&
          &*me2 - ss - tt + s25))/(s35*(4*me2 - ss - tt + s35)))*pvd2(6) + 128*tt*Pi**2*((m&
          &e2*(4*me2 - 2*ss + s15 + 2*s25))/(s15*(4*me2 - ss - tt + s25)) + (4*me2**2 + (ss&
          & + tt - s25)*(ss - s15 - s25) + me2*(-4*ss + 3*s15 + 4*s25))/(tt*s15) + (2*me2**&
          &2 - 2*me2*ss + ss*(ss - s15 - s25))/(s25*(tt + s15 - s35)) - (2*me2*(2*me2 - ss &
          &+ s15))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) - (2*me2*(me2 + tt + s15))/(&
          &(tt + s15 - s35)*(s15 + s25 - s35)) + (-4*me2**2 + me2*(4*ss + 2*tt - s15 - 2*s2&
          &5) - (ss + tt)*(ss - s15 - s25))/(tt*s35) + (me2*(2*me2 - 3*ss + s15 + s25))/(s2&
          &5*(4*me2 - ss - tt + s35)) - (2*me2*(3*me2 - ss - tt + s25))/(s35*(4*me2 - ss - &
          &tt + s35)))*pvd2(7) + 128*Pi**2*(-2*me2 + ss - s25)*((-4*me2**2 + me2*(4*ss - 3*&
          &s15 - 4*s25) - (ss + tt - s25)*(ss - s15 - s25))/(tt*s15) - (me2*(4*me2 - 2*ss +&
          & s15 + 2*s25))/(s15*(4*me2 - ss - tt + s25)) + (-2*me2**2 + 2*me2*ss + ss*(-ss +&
          & s15 + s25))/(s25*(tt + s15 - s35)) + (2*me2*(2*me2 - ss + s15))/((4*me2 - ss - &
          &tt + s25)*(s15 + s25 - s35)) + (2*me2*(me2 + tt + s15))/((tt + s15 - s35)*(s15 +&
          & s25 - s35)) + (4*me2**2 + (ss + tt)*(ss - s15 - s25) + me2*(-4*ss - 2*tt + s15 &
          &+ 2*s25))/(tt*s35) - (me2*(2*me2 - 3*ss + s15 + s25))/(s25*(4*me2 - ss - tt + s3&
          &5)) + (2*me2*(3*me2 - ss - tt + s25))/(s35*(4*me2 - ss - tt + s35)))*pvd2(8) + 1&
          &28*Pi**2*((me2*tt*(4*me2 - 2*ss + s15 + 2*s25))/(s15*(4*me2 - ss - tt + s25)) + &
          &(4*me2**2 + (ss + tt - s25)*(ss - s15 - s25) + me2*(-4*ss + 3*s15 + 4*s25))/s15 &
          &+ (tt*(2*me2**2 - 2*me2*ss + ss*(ss - s15 - s25)))/(s25*(tt + s15 - s35)) - (2*m&
          &e2*tt*(2*me2 - ss + s15))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) - (2*me2*t&
          &t*(me2 + tt + s15))/((tt + s15 - s35)*(s15 + s25 - s35)) + (-4*me2**2 + me2*(4*s&
          &s + 2*tt - s15 - 2*s25) - (ss + tt)*(ss - s15 - s25))/s35 + (me2*tt*(2*me2 - 3*s&
          &s + s15 + s25))/(s25*(4*me2 - ss - tt + s35)) - (2*me2*tt*(3*me2 - ss - tt + s25&
          &))/(s35*(4*me2 - ss - tt + s35)))*pvd2(9) + 128*Pi**2*(-tt + s15)*((-4*me2**2 + &
          &me2*(4*ss - 3*s15 - 4*s25) - (ss + tt - s25)*(ss - s15 - s25))/(tt*s15) - (me2*(&
          &4*me2 - 2*ss + s15 + 2*s25))/(s15*(4*me2 - ss - tt + s25)) + (-2*me2**2 + 2*me2*&
          &ss + ss*(-ss + s15 + s25))/(s25*(tt + s15 - s35)) + (2*me2*(2*me2 - ss + s15))/(&
          &(4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + (2*me2*(me2 + tt + s15))/((tt + s15&
          & - s35)*(s15 + s25 - s35)) + (4*me2**2 + (ss + tt)*(ss - s15 - s25) + me2*(-4*ss&
          & - 2*tt + s15 + 2*s25))/(tt*s35) - (me2*(2*me2 - 3*ss + s15 + s25))/(s25*(4*me2 &
          &- ss - tt + s35)) + (2*me2*(3*me2 - ss - tt + s25))/(s35*(4*me2 - ss - tt + s35)&
          &))*pvd2(10) + 128*Pi**2*(me2 - s15)*((me2*(4*me2 - 2*ss + s15 + 2*s25))/(s15*(4*&
          &me2 - ss - tt + s25)) + (4*me2**2 + (ss + tt - s25)*(ss - s15 - s25) + me2*(-4*s&
          &s + 3*s15 + 4*s25))/(tt*s15) + (2*me2**2 - 2*me2*ss + ss*(ss - s15 - s25))/(s25*&
          &(tt + s15 - s35)) - (2*me2*(2*me2 - ss + s15))/((4*me2 - ss - tt + s25)*(s15 + s&
          &25 - s35)) - (2*me2*(me2 + tt + s15))/((tt + s15 - s35)*(s15 + s25 - s35)) + (-4&
          &*me2**2 + me2*(4*ss + 2*tt - s15 - 2*s25) - (ss + tt)*(ss - s15 - s25))/(tt*s35)&
          & + (me2*(2*me2 - 3*ss + s15 + s25))/(s25*(4*me2 - ss - tt + s35)) - (2*me2*(3*me&
          &2 - ss - tt + s25))/(s35*(4*me2 - ss - tt + s35)))*pvd2(11) + 128*Pi**2*((-16*me&
          &2**3 + 2*me2**2*(8*ss + tt) + ss*(tt**2 - 2*s25**2 + s15*(tt - 2*s25 - s35) + ss&
          &*(tt + 2*s15 + 2*s25 - s35) - tt*s35 + s25*s35) - me2*(4*ss**2 + tt**2 - tt*s25 &
          &- 4*s25**2 + s15*(tt - 4*s25 - 4*s35) + ss*(4*tt + 4*s15 + 4*s25 - 2*s35) - 2*tt&
          &*s35 + 2*s25*s35))/(tt*s15) + (-24*me2**3 + ss*(ss - s15 - s25)*(s15 - s25) + 2*&
          &me2**2*(8*ss - tt - 4*s15 - 8*s25 + 6*s35) + me2*(-2*ss**2 + tt**2 + 6*s15**2 + &
          &2*tt*s25 - 2*s25**2 + ss*(tt - 4*s15 + 4*s25 - 3*s35) + 2*s15*(tt + 2*s25 - 2*s3&
          &5) - 3*tt*s35 + s35**2))/(s25*(4*me2 - ss - tt + s35)) + (24*me2**3 - 2*me2**2*(&
          &8*ss + tt + 6*s15 + 6*s25 - 17*s35) - ss*(ss*(s15 + s25 - 2*s35) + (s15 + s25)*s&
          &35) + me2*(2*ss**2 + tt**2 + tt*s15 + 3*tt*s25 + ss*(-2*tt + 8*s15 + 8*s25 - 15*&
          &s35) - 4*tt*s35 + 2*s15*s35 + 3*s25*s35 + s35**2))/(s35*(4*me2 - ss - tt + s35))&
          & + (-16*me2**3 + 2*me2**2*(8*ss - tt + 3*s15 + s35) - me2*(4*ss**2 + tt**2 + 4*s&
          &s*s15 - 2*s15**2 + tt*s25 + 2*s25**2 + 2*s15*s35 - s25*s35) + ss*(-tt**2 - s15**&
          &2 + tt*s25 + s25**2 + 2*tt*s35 - s25*s35 - s35**2 + ss*(-tt + s15 + s35)))/(s25*&
          &(tt + s15 - s35)) + (-24*me2**3 + ss*(s15 - s25)*(-ss + s15 + s25) + 2*me2**2*(8&
          &*ss + 5*tt + 3*s15 + 4*s25 - 7*s35) + me2*(-2*ss**2 - 3*tt**2 - tt*s15 - 2*s15**&
          &2 + 2*s25**2 + 3*tt*s35 + 3*s15*s35 + ss*(tt - 6*s25 + s35)))/(s15*(4*me2 - ss -&
          & tt + s25)) + (16*me2**3 - 2*me2**2*(8*ss - tt + 4*s15 + 4*s25 - 8*s35) + me2*(4&
          &*ss**2 + tt**2 - tt*s15 + tt*s25 + 2*ss*(-tt + 4*s15 + 4*s25 - 5*s35) + 2*tt*s35&
          & + 3*s15*s35 + 2*s25*s35 - s35**2) + ss*(tt*(tt - s15 - 2*s25) - 2*tt*s35 + s35*&
          &*2 + ss*(2*tt - 2*s15 - 2*s25 + s35)))/(tt*s35) + (-16*me2**3 + 2*me2**2*(16*ss &
          &- tt - 7*s15 - 4*s25 - 9*s35) + ss*(4*ss**2 + (s15 + s25)*(s15 + s25 + 2*s35) - &
          &ss*(4*s15 + 4*s25 + 3*s35)) + me2*(-20*ss**2 - tt**2 + 2*tt*s15 - 2*s15**2 + tt*&
          &s25 - 4*s15*s25 - 2*s25**2 - 2*tt*s35 - 7*s15*s35 - 5*s25*s35 + s35**2 + 2*ss*(t&
          &t + 7*s15 + 6*s25 + 6*s35)))/((tt + s15 - s35)*(s15 + s25 - s35)) + (-24*me2**3 &
          &+ 2*me2**2*(20*ss + tt + s15 + 2*s25 - 16*s35) + ss*(2*ss - s15 - s25)*(ss - s35&
          &) - me2*(18*ss**2 + tt**2 + 5*tt*s15 + 4*s15**2 + 6*tt*s25 + 4*s15*s25 - 4*tt*s3&
          &5 + 2*s15*s35 + 3*s25*s35 + 2*s35**2 - ss*(2*tt + 9*s15 + 6*s25 + 17*s35)))/((4*&
          &me2 - ss - tt + s25)*(s15 + s25 - s35)))*pvd4(1) + 128*Pi**2*((2*me2*s15*(me2 + &
          &tt - s35))/(s25*(tt + s15 - s35)) - (me2*s15*(2*me2 - 3*ss + 2*(s15 + s25)))/((4&
          &*me2 - ss - tt + s25)*(s15 + s25 - s35)) - (s15*(2*me2**2 + ss*(ss - s15 - s25) &
          &+ 2*me2*(-ss + s15 + s25)))/((tt + s15 - s35)*(s15 + s25 - s35)) + (2*me2*(3*me2&
          & - ss - tt + s35))/(4*me2 - ss - tt + s25) + (2*me2*s15*(2*me2 - ss + s15 + s25 &
          &- s35))/(s25*(4*me2 - ss - tt + s35)) - (me2*s15*(4*me2 - 2*ss + s35))/(s35*(4*m&
          &e2 - ss - tt + s35)) - (s15*(4*me2**2 + ss*(ss + tt - s35) + me2*(-4*ss + s35)))&
          &/(tt*s35) + (4*me2**2 + ss*(ss + tt - s15 - s25) + me2*(-4*ss - 2*tt + 2*s15 + 2&
          &*s25 + s35))/tt)*pvd4(2) + 128*Pi**2*(s15 + s25)*((2*me2*(me2 + tt - s35))/(s25*&
          &(tt + s15 - s35)) - (me2*(2*me2 - 3*ss + 2*(s15 + s25)))/((4*me2 - ss - tt + s25&
          &)*(s15 + s25 - s35)) + (-2*me2**2 + 2*me2*(ss - s15 - s25) + ss*(-ss + s15 + s25&
          &))/((tt + s15 - s35)*(s15 + s25 - s35)) + (-4*me2**2 + me2*(4*ss - s35) - ss*(ss&
          & + tt - s35))/(tt*s35) + (2*me2*(3*me2 - ss - tt + s35))/(s15*(4*me2 - ss - tt +&
          & s25)) + (2*me2*(2*me2 - ss + s15 + s25 - s35))/(s25*(4*me2 - ss - tt + s35)) - &
          &(me2*(4*me2 - 2*ss + s35))/(s35*(4*me2 - ss - tt + s35)) + (4*me2**2 + ss*(ss + &
          &tt - s15 - s25) + me2*(-4*ss - 2*tt + 2*s15 + 2*s25 + s35))/(tt*s15))*pvd4(3) + &
          &128*Pi**2*s35*((2*me2*(me2 + tt - s35))/(s25*(tt + s15 - s35)) - (me2*(2*me2 - 3&
          &*ss + 2*(s15 + s25)))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + (-2*me2**2 +&
          & 2*me2*(ss - s15 - s25) + ss*(-ss + s15 + s25))/((tt + s15 - s35)*(s15 + s25 - s&
          &35)) + (-4*me2**2 + me2*(4*ss - s35) - ss*(ss + tt - s35))/(tt*s35) + (2*me2*(3*&
          &me2 - ss - tt + s35))/(s15*(4*me2 - ss - tt + s25)) + (2*me2*(2*me2 - ss + s15 +&
          & s25 - s35))/(s25*(4*me2 - ss - tt + s35)) - (me2*(4*me2 - 2*ss + s35))/(s35*(4*&
          &me2 - ss - tt + s35)) + (4*me2**2 + ss*(ss + tt - s15 - s25) + me2*(-4*ss - 2*tt&
          & + 2*s15 + 2*s25 + s35))/(tt*s15))*pvd4(4) + 128*Pi**2*(8*me2**3*(-((1/tt + 3/(4&
          &*me2 - ss - tt + s25))/s15) + 6/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + 4/&
          &((tt + s15 - s35)*(s15 + s25 - s35)) - 4/(tt*s35) - 6/(s35*(4*me2 - ss - tt + s3&
          &5)) + (1/(tt + s15 - s35) + 3/(4*me2 - ss - tt + s35))/s25) + 2*me2**2*(4/tt + 3&
          &/(4*me2 - ss - tt + s25) + (4*ss)/(s25*(-4*me2 + ss + tt - s35)) - 3/(tt + s15 -&
          & s35) - (22*ss)/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) - (4*tt)/((4*me2 - s&
          &s - tt + s25)*(s15 + s25 - s35)) + (9*s25)/((4*me2 - ss - tt + s25)*(s15 + s25 -&
          & s35)) - (22*ss)/((tt + s15 - s35)*(s15 + s25 - s35)) + (8*s25)/((tt + s15 - s35&
          &)*(s15 + s25 - s35)) + (24*ss)/(tt*s35) - (10*s25)/(tt*s35) + (5*s35)/((4*me2 - &
          &ss - tt + s25)*(s15 + s25 - s35)) + (3*s35)/((tt + s15 - s35)*(s15 + s25 - s35))&
          & - 4/(4*me2 - ss - tt + s35) - (4*tt)/(s25*(4*me2 - ss - tt + s35)) + (28*ss)/(s&
          &35*(4*me2 - ss - tt + s35)) + (4*tt)/(s35*(4*me2 - ss - tt + s35)) - (9*s25)/(s3&
          &5*(4*me2 - ss - tt + s35)) + (4*s35)/(s25*(4*me2 - ss - tt + s35)) + (2*ss**2 + &
          &4*tt**2 - 7*ss*s25 - 8*tt*s25 + 5*s25**2 + 2*ss*s35 - 2*s25*s35 - 4*me2*(2*ss - &
          &5*s25 + 2*s35))/(tt*s15*(4*me2 - ss - tt + s25)) + s15*((1/(-4*me2 + ss + tt - s&
          &35) - 5/(tt + s15 - s35))/s25 + (5/(4*me2 - ss - tt + s25) + 8/(tt + s15 - s35))&
          &/(s15 + s25 - s35) + (-8/tt - 7/(4*me2 - ss - tt + s35))/s35)) + me2*(-1 + (2*ss&
          &)/tt - (7*ss)/(-4*me2 + ss + tt - s25) - (2*s25)/tt - (3*tt)/(4*me2 - ss - tt + &
          &s25) + (2*ss**2)/(s25*(-4*me2 + ss + tt - s35)) - (4*ss*tt)/(s25*(-4*me2 + ss + &
          &tt - s35)) + (12*ss)/(tt + s15 - s35) + (3*tt)/(tt + s15 - s35) - (6*ss**2)/(s25&
          &*(tt + s15 - s35)) + (22*ss**2)/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) - (6&
          &*ss*tt)/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + (2*tt**2)/((4*me2 - ss - t&
          &t + s25)*(s15 + s25 - s35)) - (17*ss*s25)/((4*me2 - ss - tt + s25)*(s15 + s25 - &
          &s35)) + (3*tt*s25)/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + (5*s25**2)/((4*&
          &me2 - ss - tt + s25)*(s15 + s25 - s35)) + (26*ss**2)/((tt + s15 - s35)*(s15 + s2&
          &5 - s35)) + (4*ss*tt)/((tt + s15 - s35)*(s15 + s25 - s35)) + (2*tt**2)/((tt + s1&
          &5 - s35)*(s15 + s25 - s35)) - (20*ss*s25)/((tt + s15 - s35)*(s15 + s25 - s35)) +&
          & (4*s25**2)/((tt + s15 - s35)*(s15 + s25 - s35)) - (4*ss)/s35 - (24*ss**2)/(tt*s&
          &35) - (2*tt)/s35 - s25/s35 + (12*ss*s25)/(tt*s35) - (4*s25**2)/(tt*s35) + (4*ss*&
          &s35)/(s25*(-4*me2 + ss + tt - s35)) - (2*s35)/(tt + s15 - s35) - (4*ss*s35)/(s25&
          &*(tt + s15 - s35)) + (4*ss*s35)/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) - (4&
          &*tt*s35)/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) - (2*s25*s35)/((4*me2 - ss &
          &- tt + s25)*(s15 + s25 - s35)) - (2*ss*s35)/((tt + s15 - s35)*(s15 + s25 - s35))&
          & - (2*tt*s35)/((tt + s15 - s35)*(s15 + s25 - s35)) + (2*s25*s35)/((tt + s15 - s3&
          &5)*(s15 + s25 - s35)) + (2*s35**2)/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) +&
          & ss/(4*me2 - ss - tt + s35) + (3*tt)/(4*me2 - ss - tt + s35) + (2*s25)/(4*me2 - &
          &ss - tt + s35) - (20*ss**2)/(s35*(4*me2 - ss - tt + s35)) + (2*ss*tt)/(s35*(4*me&
          &2 - ss - tt + s35)) - (2*tt**2)/(s35*(4*me2 - ss - tt + s35)) + (13*ss*s25)/(s35&
          &*(4*me2 - ss - tt + s35)) - (2*tt*s25)/(s35*(4*me2 - ss - tt + s35)) - (5*s25**2&
          &)/(s35*(4*me2 - ss - tt + s35)) - (2*s35)/(4*me2 - ss - tt + s35) + ((2*ss*(2*ss&
          & + s35))/tt + s25*(-2 - (4*ss)/tt - (3*ss)/(-4*me2 + ss + tt - s25) + s35/tt))/s&
          &15 + s15**2*((8/(4*me2 - ss - tt + s25) + 7/(tt + s15 - s35))/(s15 + s25 - s35) &
          &- 2/(s25*(4*me2 - ss - tt + s35)) + (-4/tt - 5/(4*me2 - ss - tt + s35))/s35) + s&
          &15*(-2/tt + 1/(-4*me2 + ss + tt - s25) - (3*ss)/(s25*(-4*me2 + ss + tt - s35)) -&
          & 1/(tt + s15 - s35) + (10*ss)/(s25*(tt + s15 - s35)) + (2*tt)/(s25*(tt + s15 - s&
          &35)) - (22*ss)/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + (6*tt)/((4*me2 - ss&
          & - tt + s25)*(s15 + s25 - s35)) + (10*s25)/((4*me2 - ss - tt + s25)*(s15 + s25 -&
          & s35)) - (16*ss)/((tt + s15 - s35)*(s15 + s25 - s35)) + (3*tt)/((tt + s15 - s35)&
          &*(s15 + s25 - s35)) + (8*s25)/((tt + s15 - s35)*(s15 + s25 - s35)) + 1/(4*me2 - &
          &ss - tt + s35) + s35*(-((4/(4*me2 - ss - tt + s25) + 1/(tt + s15 - s35))/(s15 + &
          &s25 - s35)) + 1/(s25*(4*me2 - ss - tt + s35))) - (8*ss**2 + tt**2 + 2*tt*s25 + 8&
          &*me2*(-4*ss + tt + 3*s25) + 2*tt*s35 + 6*s25*s35 - 2*ss*(3*tt + 3*s25 + 4*s35))/&
          &(tt*s35*(4*me2 - ss - tt + s35)))) + ss*(-(tt/(tt + s15 - s35)) - s25**2/((4*me2&
          & - ss - tt + s25)*(s15 + s25 - s35)) - tt**2/((tt + s15 - s35)*(s15 + s25 - s35)&
          &) + (tt*s25)/((tt + s15 - s35)*(s15 + s25 - s35)) - (2*s25**2)/((tt + s15 - s35)&
          &*(s15 + s25 - s35)) + tt/s35 + (2*s25**2)/(tt*s35) + s35/(tt + s15 - s35) + (tt*&
          &s35)/((tt + s15 - s35)*(s15 + s25 - s35)) - (s25*s35)/((tt + s15 - s35)*(s15 + s&
          &25 - s35)) + s25**2/(s35*(4*me2 - ss - tt + s35)) + s15**2*((1/(-4*me2 + ss + tt&
          & - s25) - 2/(tt + s15 - s35))/(s15 + s25 - s35) + (1/tt + 1/(4*me2 - ss - tt + s&
          &35))/s35) + 2*ss**2*(1/(s25*(tt + s15 - s35)) + (1/(-4*me2 + ss + tt - s25) - 3/&
          &(tt + s15 - s35))/(s15 + s25 - s35) + (2/tt + 1/(4*me2 - ss - tt + s35))/s35) + &
          &ss*(-2/tt - 3/(tt + s15 - s35) - (4*s15)/(s25*(tt + s15 - s35)) + (2*s25)/((4*me&
          &2 - ss - tt + s25)*(s15 + s25 - s35)) + (s15*(2/(4*me2 - ss - tt + s25) + 5/(tt &
          &+ s15 - s35)))/(s15 + s25 - s35) - (2*tt)/((tt + s15 - s35)*(s15 + s25 - s35)) +&
          & (7*s25)/((tt + s15 - s35)*(s15 + s25 - s35)) + s35/(s25*(tt + s15 - s35)) + (2 &
          &- (4*s25)/tt - (2*s15)/(4*me2 - ss - tt + s35) - (2*s25)/(4*me2 - ss - tt + s35)&
          &)/s35) + s15*((-2*tt)/(s25*(tt + s15 - s35)) + (2*s25*(1/(-4*me2 + ss + tt - s25&
          &) - 1/(tt + s15 - s35)))/(s15 + s25 - s35) + s35/(s25*(tt + s15 - s35)) - (tt - &
          &s15 + s35)/((tt + s15 - s35)*(s15 + s25 - s35)) + (2 + 2*s25*(1/tt + 1/(4*me2 - &
          &ss - tt + s35)))/s35)))*pvd6(1) + (128*Pi**2*(2*me2 - tt + s35)*(me2*tt*s15*(4*m&
          &e2 - 2*ss + s15)*(4*me2 - ss - tt + s25)*(tt + s15 - s35)*(s15 + s25 - s35)*s35 &
          &+ tt*s15*(4*me2 - ss - tt + s25)*(4*me2**2 + ss*(ss - s15) + me2*(-4*ss + s25))*&
          &(s15 + s25 - s35)*s35*(4*me2 - ss - tt + s35) - me2*tt*(4*me2 - 3*s15)*s25*(tt +&
          & s15 - s35)*(s15 + s25 - s35)*s35*(4*me2 - ss - tt + s35) + s25*(4*me2 - ss - tt&
          & + s25)*(-4*me2**2 - ss*s15 + me2*(2*ss + 2*s15 + s25))*(tt + s15 - s35)*(s15 + &
          &s25 - s35)*s35*(4*me2 - ss - tt + s35) + (2*me2 - ss)*tt*s15*s25*(4*me2 - ss - t&
          &t + s25)*s35*(4*me2 - ss - tt + s35)*(2*me2 + 3*ss - 2*s15 - 2*s25 + s35) + 2*me&
          &2*tt*s15*s25*(tt + s15 - s35)*s35*(4*me2 - ss - tt + s35)*(6*me2 + 2*ss - 2*tt -&
          & 3*s15 - s25 + 2*s35) - 2*me2*tt*s15*s25*(4*me2 - ss - tt + s25)*(tt + s15 - s35&
          &)*(s15 + s25 - s35)*(6*me2 + ss - 2*tt - s15 - s25 + 2*s35) + s15*s25*(4*me2 - s&
          &s - tt + s25)*(tt + s15 - s35)*(s15 + s25 - s35)*(4*me2 - ss - tt + s35)*(-4*me2&
          &**2 + ss*(2*ss - 2*s25 + s35) - 2*me2*(ss - s15 - s25 + s35)))*pvd6(2))/(tt*s15*&
          &s25*(4*me2 - ss - tt + s25)*(tt + s15 - s35)*(s15 + s25 - s35)*s35*(4*me2 - ss -&
          & tt + s35)) + 128*Pi**2*(ss + s35)*((-4*me2**2 + 3*me2*s15)/(s15*(4*me2 - ss - t&
          &t + s25)) + (-4*me2**2 - ss*s15 + me2*(2*ss + 2*s15 + s25))/(tt*s15) + (4*me2**2&
          & + ss*(ss - s15) + me2*(-4*ss + s25))/(s25*(tt + s15 - s35)) + (me2*(4*me2 - 2*s&
          &s + s15))/(s25*(4*me2 - ss - tt + s35)) + ((2*me2 - ss)*(2*me2 + 3*ss - 2*s15 - &
          &2*s25 + s35))/((tt + s15 - s35)*(s15 + s25 - s35)) + (2*me2*(6*me2 + 2*ss - 2*tt&
          & - 3*s15 - s25 + 2*s35))/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) - (2*me2*(6&
          &*me2 + ss - 2*tt - s15 - s25 + 2*s35))/(s35*(4*me2 - ss - tt + s35)) + (-4*me2**&
          &2 + ss*(2*ss - 2*s25 + s35) - 2*me2*(ss - s15 - s25 + s35))/(tt*s35))*pvd6(3) + &
          &128*Pi**2*(((-4*me2**2 - ss*s15 + me2*(2*ss + 2*s15 + s25))*s35)/(tt*s15) + ((4*&
          &me2**2 + ss*(ss - s15) + me2*(-4*ss + s25))*s35)/(s25*(tt + s15 - s35)) + (me2*(&
          &4*me2 - 2*ss + s15)*s35)/(s25*(4*me2 - ss - tt + s35)) + ((2*me2 - ss)*s35*(2*me&
          &2 + 3*ss - 2*s15 - 2*s25 + s35))/((tt + s15 - s35)*(s15 + s25 - s35)) + (2*me2*s&
          &35*(6*me2 + 2*ss - 2*tt - 3*s15 - s25 + 2*s35))/((4*me2 - ss - tt + s25)*(s15 + &
          &s25 - s35)) - (2*me2*(6*me2 + ss - 2*tt - s15 - s25 + 2*s35))/(4*me2 - ss - tt +&
          & s35) + (-4*me2**2*s35 + 3*me2*s15*s35)/(s15*(4*me2 - ss - tt + s25)) + (-4*me2*&
          &*2 + ss*(2*ss - 2*s25 + s35) - 2*me2*(ss - s15 - s25 + s35))/tt)*pvd6(4) + 512*P&
          &i**2*(me2 + s35)*((2*me2)/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + (2*me2 -&
          & ss)/((tt + s15 - s35)*(s15 + s25 - s35)) + (-2*me2 + ss)/(tt*s35) - (2*me2)/(s3&
          &5*(4*me2 - ss - tt + s35)))*pvd6(5) + 128*Pi**2*(2*me2 - tt + s35)**2*((2*me2)/(&
          &(4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + (2*me2 - ss)/((tt + s15 - s35)*(s15&
          & + s25 - s35)) + (-2*me2 + ss)/(tt*s35) - (2*me2)/(s35*(4*me2 - ss - tt + s35)))&
          &*pvd6(6) + 256*Pi**2*(ss + s35)*(2*me2 - tt + s35)*((2*me2)/((4*me2 - ss - tt + &
          &s25)*(s15 + s25 - s35)) + (2*me2 - ss)/((tt + s15 - s35)*(s15 + s25 - s35)) + (-&
          &2*me2 + ss)/(tt*s35) - (2*me2)/(s35*(4*me2 - ss - tt + s35)))*pvd6(7) + 256*Pi**&
          &2*s35*(2*me2 - tt + s35)*((2*me2)/((4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + &
          &(2*me2 - ss)/((tt + s15 - s35)*(s15 + s25 - s35)) + (-2*me2 + ss)/(tt*s35) - (2*&
          &me2)/(s35*(4*me2 - ss - tt + s35)))*pvd6(8) + 128*Pi**2*(ss + s35)**2*((2*me2)/(&
          &(4*me2 - ss - tt + s25)*(s15 + s25 - s35)) + (2*me2 - ss)/((tt + s15 - s35)*(s15&
          & + s25 - s35)) + (-2*me2 + ss)/(tt*s35) - (2*me2)/(s35*(4*me2 - ss - tt + s35)))&
          &*pvd6(9) + 256*Pi**2*(ss + s35)*(2*me2*(-(1/tt) + 1/(-4*me2 + ss + tt - s35) + (&
          &(1/(4*me2 - ss - tt + s25) + 1/(tt + s15 - s35))*s35)/(s15 + s25 - s35)) + (ss*(&
          &s15**2 + s15*(tt + s25 - 2*s35) + s25*(tt - s35) + s35*(-2*tt + s35)))/(tt*(tt +&
          & s15 - s35)*(s15 + s25 - s35)))*pvd6(10) + 128*Pi**2*s35*(ss*(1/tt - s35/((tt + &
          &s15 - s35)*(s15 + s25 - s35))) + me2*(-2/tt + (2*(1/(4*me2 - ss - tt + s25) + 1/&
          &(tt + s15 - s35))*s35)/(s15 + s25 - s35) - 2/(4*me2 - ss - tt + s35)))*pvd6(11) &
          &+ 128*Pi**2*((-64*me2**4 + 16*me2**3*(6*ss - 3*s15 - 4*s25) - 2*me2**2*(24*ss**2&
          & + 2*tt**2 - 5*tt*s15 + 6*s15**2 - 4*tt*s25 + 16*s15*s25 + 8*s25**2 - 4*ss*(-tt &
          &+ 6*s15 + 8*s25)) - ss*s15*(ss - s15 - s25)*(-tt + s15 + 2*s25 - s35) + me2*(8*s&
          &s**3 - 2*s15**3 + tt**2*s25 - 4*ss**2*(-tt + 3*s15 + 4*s25) + 2*ss*(tt**2 + 4*s1&
          &5**2 - 2*tt*s25 + 4*s25**2 + s15*(-4*tt + 10*s25 - s35)) + 3*s15**2*(tt - 2*s25 &
          &+ s35) + s15*(tt**2 + 3*tt*s25 - 4*s25**2 + 2*s25*s35)))/(tt*s15) + (64*me2**4 +&
          & ss*(ss - s15 - s25)*(tt + s15 + 2*s25 - s35)*s35 + 16*me2**3*(-6*ss + 2*s15 + 2&
          &*s25 + s35) + 2*me2**2*(24*ss**2 + 2*tt**2 + tt*s35 + 8*s15*s35 + 8*s25*s35 - 2*&
          &s35**2 - 4*ss*(-tt + 4*s15 + 4*s25 + 2*s35)) + me2*(-8*ss**3 + 4*s15**2*s35 + 4*&
          &ss**2*(-tt + 2*s15 + 2*s25 + s35) + s25*(3*tt**2 + tt*s35 + 4*s25*s35 - 2*s35**2&
          &) - 2*ss*(tt**2 - 2*tt*s15 - 2*tt*s25 + 2*tt*s35 + 5*s15*s35 + 6*s25*s35 - 2*s35&
          &**2) + s15*(3*tt**2 + tt*s35 + 8*s25*s35 - s35**2)))/(tt*s35) + (96*me2**4 - 8*m&
          &e2**3*(14*ss + 2*tt - 6*s15 - 6*s25 - 3*s35) + ss*(ss - s15 - s25)*(s15 + s25)*s&
          &35 + 2*me2**2*(20*ss**2 + 2*tt**2 + 5*tt*s25 - tt*s35 + 8*s25*s35 + 5*s15*(tt + &
          &3*s35) - 2*ss*(tt + 8*s15 + 8*s25 + 6*s35)) + me2*(-4*ss**3 + 3*s15**2*(tt + s35&
          &) + s25**2*(3*tt + 2*s35) + ss**2*(4*s15 + 4*s25 + 6*s35) - ss*(3*tt*s15 + 3*tt*&
          &s25 + 11*s15*s35 + 10*s25*s35) + s15*(6*tt*s25 - 3*tt*s35 + 5*s25*s35 + s35**2))&
          &)/(s35*(4*me2 - ss - tt + s35)) + (-96*me2**4 + 8*me2**3*(14*ss + 2*tt - 5*s15 -&
          & 8*s25) - ss*s15*(ss - s15 - s25)*(s15 + s25) + me2**2*(-40*ss**2 - 4*tt**2 + 6*&
          &tt*s25 - 8*s25**2 + 4*ss*(tt + 6*s15 + 12*s25) - 2*s15*(-2*tt + 4*s25 + 7*s35)) &
          &+ me2*(4*ss**3 - 2*ss**2*(s15 + 4*s25) + s15*(-2*s15**2 - 2*s25**2 + 3*tt*s35 + &
          &s15*(-4*s25 + s35)) + ss*(2*s15**2 + s25*(-3*tt + 4*s25) + s15*(-3*tt + 6*s25 + &
          &s35))))/(s15*(4*me2 - ss - tt + s25)) + (64*me2**4 - 16*me2**3*(8*ss - 4*s15 - 5&
          &*s25) + 2*me2**2*(48*ss**2 + 10*s15**2 - 4*ss*(-tt + 11*s15 + 14*s25 + s35) + s1&
          &5*(-3*tt + 22*s25 + 3*s35) + 2*(tt**2 - 2*tt*s25 + 6*s25**2 - tt*s35 + s25*s35 +&
          & s35**2)) + me2*(-32*ss**3 + 2*s15**3 + 4*s15**2*s25 + ss**2*(-8*tt + 40*s15 + 5&
          &2*s25 + 8*s35) + s15*(2*s25**2 - 2*tt*s35 + s25*(-2*tt + s35)) - 4*ss*(4*s15**2 &
          &+ 5*s25**2 + 2*s25*(-tt + s35) + (-tt + s35)**2 + s15*(-tt + 9*s25 + s35)) + s25&
          &*(2*s25*(-tt + s35) + s35*(-3*tt + 2*s35))) + ss*(4*ss**3 - s15**3 - 2*s15**2*s2&
          &5 - s25*(-tt + s35)*(-tt + s25 + s35) - 2*ss**2*(-tt + 3*s15 + 4*s25 + s35) - s1&
          &5*(s25**2 + s25*(-tt + s35) + (-tt + s35)**2) + ss*(3*s15**2 + 4*s25**2 + 3*s25*&
          &(-tt + s35) + (-tt + s35)**2 + s15*(-2*tt + 7*s25 + 2*s35))))/(s25*(tt + s15 - s&
          &35)) + (-96*me2**4 + 8*me2**3*(20*ss + 2*tt - 7*s15 - 9*s25 - 5*s35) - ss*(ss - &
          &s15 - s25)*(2*ss**2 - 2*ss*(s15 + s25) + (s15 + s25)*s35) - 2*me2**2*(48*ss**2 +&
          & 2*tt**2 + 4*s15**2 + 3*tt*s25 + 6*s25**2 - 3*tt*s35 + 6*s25*s35 + 2*s35**2 - 2*&
          &ss*(-tt + 21*s15 + 22*s25 + 7*s35) + s15*(5*tt + 8*s25 + 10*s35)) + me2*(24*ss**&
          &3 - 2*s15**3 - s15**2*(5*tt + 2*s25 + 2*s35) - 2*ss**2*(tt + 18*s15 + 17*s25 + 2&
          &*s35) - s25*(tt**2 + 3*tt*s25 + 2*s25*s35) - s15*(tt**2 + 8*tt*s25 - 2*tt*s35 + &
          &5*s25*s35 + 2*s35**2) + ss*(2*tt**2 + 14*s15**2 + 5*tt*s25 + 10*s25**2 - 2*tt*s3&
          &5 + 8*s25*s35 + 2*s35**2 + s15*(7*tt + 22*s25 + 9*s35))))/((4*me2 - ss - tt + s2&
          &5)*(s15 + s25 - s35)) + (-64*me2**4 + 16*me2**3*(8*ss - 3*s15 - 3*s25 - s35) - s&
          &s*(ss - s15 - s25)*(4*ss**2 + tt**2 - tt*s25 - tt*s35 + 3*s25*s35 - 2*ss*(-tt + &
          &s15 + 2*s25 + s35) + s15*(tt - s25 + 2*s35)) - 2*me2**2*(48*ss**2 + 2*tt**2 + 4*&
          &s15**2 + 4*s25**2 - tt*s35 + 10*s25*s35 - 4*ss*(-tt + 9*s15 + 10*s25 + 3*s35) + &
          &s15*(2*tt + 6*s25 + 11*s35)) + me2*(32*ss**3 + s15**2*(-3*tt + 2*s25 - 7*s35) - &
          &4*ss**2*(-2*tt + 9*s15 + 11*s25 + 3*s35) + 2*s25*(s25*(tt - 3*s35) + tt*(-2*tt +&
          & s35)) + 4*ss*(tt**2 + 2*s15**2 - 2*tt*s25 + 3*s25**2 - tt*s35 + 5*s25*s35 + 4*s&
          &15*(s25 + s35)) + s15*(-4*tt**2 + 2*s25**2 + tt*s35 + s35**2 - s25*(tt + 11*s35)&
          &)))/((tt + s15 - s35)*(s15 + s25 - s35)) + (96*me2**4 + ss*(2*ss**3 - 4*ss**2*(s&
          &15 + s25) - s15*(s15 + s25)**2 + ss*(3*s15**2 + 5*s15*s25 + 2*s25**2)) - 8*me2**&
          &3*(20*ss + 2*tt - 6*s15 - 11*s25 - 2*s35) + 2*me2**2*(48*ss**2 + 2*tt**2 + 4*s15&
          &**2 - 5*tt*s25 + 10*s25**2 - 2*tt*s35 - 2*s25*s35 + 2*s35**2 + 2*s15*(-tt + 6*s2&
          &5 + s35) - 2*ss*(-tt + 19*s15 + 26*s25 + s35)) + me2*(-24*ss**3 + 4*s15**3 + tt*&
          &*2*s25 + 2*s15**2*(tt + 3*s25 - s35) + 2*ss**2*(tt + 17*s15 + 19*s25 - s35) + s1&
          &5*(2*tt*s25 + 2*s25**2 + (-tt + s35)**2) + ss*(-16*s15**2 - 14*s25**2 + s15*(-tt&
          & - 28*s25 + s35) + s25*(tt + 2*s35) - 2*(tt**2 - tt*s35 + s35**2))))/(s25*(4*me2&
          & - ss - tt + s35)))*pve1(1))/(16.*Pi**2)

          !print*,penta

  END FUNCTION PENTA

  !SUBROUTINE PV_EVALUATE(ss,tt,s25,s35,s15,m)
  !implicit none
  !real(kind=prec),intent(in) :: ss,tt,s15,s25,s35,m
  !complex(kind=prec) :: uvdump(24), pv(281)
  !complex(kind=prec),parameter :: cz = (0._prec, 0._prec)
  !me2 = m**2

  !call A_cll(pv(1:1), uvdump(1:1), cmplx(me2), 0)
  !call B_cll(pv(2:3), uvdump(1:2), cz, cmplx(me2), cmplx(me2), 1)
  !call B_cll(pv(4:7), uvdump(1:4), cmplx(me2), cz, cmplx(me2), 2)
  !call B_cll(pv(8:9), uvdump(1:2), cmplx(me2), cmplx(me2), cz, 1)
  !call B_cll(pv(10:10), uvdump(1:1), cmplx(ss), cmplx(me2), cmplx(me2), 0)
  !call B_cll(pv(11:14), uvdump(1:4), cmplx(me2 - s15), cz, cmplx(me2), 2)
  !call B_cll(pv(15:16), uvdump(1:2), cmplx(me2 - s15), cmplx(me2), cz, 1)
  !call B_cll(pv(17:17), uvdump(1:1), cmplx(ss - s15 - s25), cmplx(me2), cmplx(me2), 0)
  !call B_cll(pv(18:18), uvdump(1:1), cmplx(me2 + s35), cmplx(me2), cz, 0)
  !call B_cll(pv(19:19), uvdump(1:1), cmplx(4*me2 - ss + s25 - tt), cmplx(me2), cmplx(me2), 0)
  !call B_cll(pv(20:20), uvdump(1:1), cmplx(4*me2 - ss + s35 - tt), cmplx(me2), cmplx(me2), 0)
  !call B_cll(pv(21:21), uvdump(1:1), cmplx(tt), cz, cz, 0)
  !call B_cll(pv(22:23), uvdump(1:2), cmplx(tt), cmplx(me2), cmplx(me2), 1)
  !call B_cll(pv(24:25), uvdump(1:2), cmplx(s15 - s35 + tt), cmplx(me2), cmplx(me2), 1)
  !call C_cll(pv(26:38), uvdump(1:13),&
  !    cmplx(me2), cz, cmplx(me2 - s15), &
  !    cz, cmplx(me2), cmplx(me2), 3)
  !call C_cll(pv(39:41), uvdump(1:3),&
  !    cmplx(me2), cmplx(me2), cmplx(ss),&
  !    cmplx(me2), cz, cmplx(me2), 1)
  !call C_cll(pv(42:44), uvdump(1:3),&
  !!    cmplx(me2), cmplx(me2), cmplx(ss - s15 - s25),&
  !    cmplx(me2), cz, cmplx(me2), 1)
  !call C_cll(pv(45:47), uvdump(1:3),&
  !    cmplx(me2), cmplx(me2), cmplx(4*me2 - ss + s25 - tt),&
  !    cmplx(me2), cz, cmplx(me2), 1)
  !call C_cll(pv(48:50), uvdump(1:3),&
  !    cmplx(me2), cmplx(me2), cmplx(4*me2 - ss + s35 - tt),&
  !    cmplx(me2), cz, cmplx(me2), 1)
  !call C_cll(pv(51:57), uvdump(1:7),&
  !    cmplx(me2), cmplx(me2), cmplx(tt),&
  !    cz, cmplx(me2), cz, 2)
  !call C_cll(pv(58:64), uvdump(1:7),&
  !    cmplx(me2), cmplx(me2), cmplx(tt),&
  !    cmplx(me2), cz, cmplx(me2), 2)
  !call C_cll(pv(65:71), uvdump(1:7),&
  !    cmplx(me2), cmplx(me2), cmplx(s15 - s35 + tt),&
  !   cmplx(me2), cz, cmplx(me2), 2)
  !call C_cll(pv(72:78), uvdump(1:7),&
  !    cmplx(me2), cmplx(me2 - s15), cz,&
  !    cmplx(me2), cz, cmplx(me2), 2)
  !call C_cll(pv(79:85), uvdump(1:7),&
  !    cmplx(me2), cmplx(me2 - s15),&
  !    cmplx(tt), cmplx(me2), cz, cmplx(me2), 2)
  !call C_cll(pv(86:92), uvdump(1:7),&
  !    cmplx(me2), cmplx(ss - s15 - s25),&
  !    cmplx(me2 - s15), cz, cmplx(me2), cmplx(me2), 2)
  !call C_cll(pv(93:99), uvdump(1:7),&
  !    cmplx(me2), cmplx(4*me2 - ss + s25 - tt), cmplx(me2 - s15),&
  !    cz, cmplx(me2), cmplx(me2), 2)
  !call C_cll(pv(100:102), uvdump(1:3),&
  !    cmplx(me2), cmplx(tt), cmplx(me2 + s35),&
  !    cmplx(me2), cz, cz, 1)
  !call C_cll(pv(103:105), uvdump(1:3),&
  !    cmplx(ss), cmplx(me2), cmplx(me2 + s35),&
  !    cmplx(me2), cmplx(me2), cz, 1)
  !call C_cll(pv(106:108), uvdump(1:3),&
  !    cmplx(ss), cmplx(ss - s15 - s25),&
  !    cz, cmplx(me2), cmplx(me2), cmplx(me2), 1)
  !call C_cll(pv(109:111), uvdump(1:3),&
  !    cmplx(me2 - s15), cmplx(me2),&
  !    cmplx(tt), cz, cmplx(me2), cz, 1)
  !call C_cll(pv(112:118), uvdump(1:7),&
  !    cmplx(ss - s15 - s25), cmplx(me2),&
  !    cmplx(me2), cmplx(me2), cmplx(me2), cz, 2)
  !call C_cll(pv(119:121), uvdump(1:3),&
  !    cmplx(me2 + s35), cmplx(me2), cz,&
  !    cmplx(me2), cz, cmplx(me2), 1)
  !call C_cll(pv(122:128), uvdump(1:7),&
  !    cmplx(4*me2 - ss + s25 - tt), cmplx(me2), cmplx(me2),&
   !   cmplx(me2), cmplx(me2), cz, 2)
  !!call C_cll(pv(129:131), uvdump(1:3),&
  !    cmplx(4*me2 - ss + s35 - tt), cmplx(me2), cmplx(me2 + s35),&
  !    cmplx(me2), cmplx(me2), cz, 1)
  !call C_cll(pv(132:134), uvdump(1:3),&
  !    cmplx(4*me2 - ss + s35 - tt), cmplx(4*me2 - ss + s25 - tt), cz,&
  !    cmplx(me2), cmplx(me2), cmplx(me2), 1)
  !call C_cll(pv(135:141), uvdump(1:7),&
  !    cmplx(tt), cmplx(me2), cmplx(me2 - s15),&
  !    cz, cz, cmplx(me2), 2)
  !call C_cll(pv(142:148), uvdump(1:7),&
  !    cmplx(s15 - s35 + tt), cz, cmplx(tt),&
  !    cmplx(me2), cmplx(me2), cmplx(me2), 2)
  !call D_cll(pv(149:172), uvdump(1:24),&
  !    cmplx(me2), cmplx(me2), cz, cmplx(tt), cmplx(s15 - s35 + tt), cmplx(me2 - s15), cmplx(me2),&
  !    cz, cmplx(me2), cmplx(me2), 3)
  !call D_cll(pv(173:183), uvdump(1:11),&
  !    cmplx(me2), cmplx(me2), cmplx(me2), cmplx(me2 - s15), cmplx(tt), cmplx(ss - s15 - s25), cz,&
  !    cmplx(me2), cz, cmplx(me2), 2)
  !call D_cll(pv(184:194), uvdump(1:11),&
  !    cmplx(me2), cmplx(me2), cmplx(me2), cmplx(me2 - s15), cmplx(tt), cmplx(4*me2 - ss + s25 - tt), cz,&
  !    cmplx(me2), cz, cmplx(me2), 2)
  !call D_cll(pv(195:198), uvdump(1:4),&
  !    cmplx(me2), cmplx(me2), cmplx(me2), cmplx(me2 + s35), cmplx(ss), cmplx(tt), cmplx(me2),&
  !    cz, cmplx(me2), cz, 1)
  !call D_cll(pv(199:202), uvdump(1:4),&
  !    cmplx(me2), cmplx(me2), cmplx(me2), cmplx(me2 + s35), cmplx(4*me2 - ss + s35 - tt), cmplx(tt), cmplx(me2),&
  !    cz, cmplx(me2), cz, 1)
  !call D_cll(pv(203:213), uvdump(1:11),&
  !    cmplx(me2), cmplx(me2), cmplx(ss - s15 - s25), cz, cmplx(ss), cmplx(me2 - s15), cmplx(me2),&
  !    cz, cmplx(me2), cmplx(me2), 2)
  !call D_cll(pv(214:224), uvdump(1:11),&
  !    cmplx(me2), cmplx(me2), cmplx(4*me2 - ss + s25 - tt), cz, cmplx(4*me2 - ss + s35 - tt), cmplx(me2 - s15), cmplx(me2),&
  !    cz, cmplx(me2), cmplx(me2), 2)
  !call D_cll(pv(225:235), uvdump(1:11),&
  !    cmplx(me2), cmplx(ss - s15 - s25), cmplx(me2), cmplx(tt), cmplx(me2 - s15), cmplx(me2), cz,&
  !    cmplx(me2), cmplx(me2), cz, 2)
  !call D_cll(pv(236:246), uvdump(1:11),&
  !    cmplx(me2), cmplx(4*me2 - ss + s25 - tt), cmplx(me2), cmplx(tt), cmplx(me2 - s15), cmplx(me2), cz,&
  !    cmplx(me2), cmplx(me2), cz, 2)
  !call D_cll(pv(247:257), uvdump(1:11),&
  !    cmplx(me2), cmplx(tt), cmplx(me2), cz, cmplx(me2 + s35), cmplx(me2 - s15), cmplx(me2),&
  !    cz, cz, cmplx(me2), 2)
  !call D_cll(pv(258:268), uvdump(1:11),&
  !    cmplx(ss), cmplx(me2), cmplx(me2), cz, cmplx(me2 + s35), cmplx(ss - s15 - s25), cmplx(me2),&
  !    cmplx(me2), cz, cmplx(me2), 2)
  !call D_cll(pv(269:279), uvdump(1:11),&
  !    cmplx(4*me2 - ss + s35 - tt), cmplx(me2), cmplx(me2), cz, cmplx(me2 + s35), cmplx(4*me2 - ss + s25 - tt), cmplx(me2),&
  !    cmplx(me2), cz, cmplx(me2), 2)
  !call E_cll(pv(280:280), uvdump(1:1),&
  !    cmplx(me2), cmplx(me2), cmplx(me2), cmplx(me2), cz, cmplx(ss), cmplx(tt), cmplx(ss - s15 - s25),&
  !    cmplx(me2 + s35), cmplx(me2 - s15), cmplx(me2),&
  !    cz, cmplx(me2), cz, cmplx(me2), 0)
  !call E_cll(pv(281:281), uvdump(1:1),&
  !    cmplx(me2), cmplx(me2), cmplx(me2), cmplx(me2), cz, cmplx(4*me2 - ss + s35 - tt), cmplx(tt),&
  !    cmplx(4*me2 - ss + s25 - tt), cmplx(me2 + s35), cmplx(me2 - s15),&
  !    cmplx(me2), cz, cmplx(me2), cz, cmplx(me2), 0)


  !pva1 = real(pv(1:1), prec)
  !pvb1 = real(pv(2:3), prec)
  !pvb2 = real(pv(4:7), prec)
  !pvb3 = real(pv(8:9), prec)
  !pvb4 = real(pv(10:10), prec)
  !pvb5 = real(pv(11:14), prec)
  !pvb6 = real(pv(15:16), prec)
  !pvb7 = real(pv(17:17), prec)
  !pvb8 = real(pv(18:18), prec)
  !pvb9 = real(pv(19:19), prec)
  !pvb10 = real(pv(20:20), prec)
  !pvb11 = real(pv(21:21), prec)
  !pvb12 = real(pv(22:23), prec)
  !pvb13 = real(pv(24:25), prec)
  !pvc1 = real(pv(26:38), prec)
  !pvc2 = real(pv(39:41), prec)
  !pvc3 = real(pv(42:44), prec)
  !pvc4 = real(pv(45:47), prec)
  !pvc5 = real(pv(48:50), prec)
  !pvc6 = real(pv(51:57), prec)
  !pvc7 = real(pv(58:64), prec)
  !pvc8 = real(pv(65:71), prec)
  !pvc9 = real(pv(72:78), prec)
  !pvc10 = real(pv(79:85), prec)
  !pvc11 = real(pv(86:92), prec)
  !pvc12 = real(pv(93:99), prec)
  !pvc13 = real(pv(100:102), prec)
  !pvc14 = real(pv(103:105), prec)
  !pvc15 = real(pv(106:108), prec)
  !pvc16 = real(pv(109:111), prec)
  !pvc17 = real(pv(112:118), prec)
  !pvc18 = real(pv(119:121), prec)
  !pvc19 = real(pv(122:128), prec)
  !pvc20 = real(pv(129:131), prec)
  !pvc21 = real(pv(132:134), prec)
  !pvc22 = real(pv(135:141), prec)
  !pvc23 = real(pv(142:148), prec)
  !pvd1 = real(pv(149:172), prec)
  !pvd2 = real(pv(173:183), prec)
  !pvd3 = real(pv(184:194), prec)
  !pvd4 = real(pv(195:198), prec)
  !pvd5 = real(pv(199:202), prec)
  !pvd6 = real(pv(203:213), prec)
  !pvd7 = real(pv(214:224), prec)
  !pvd8 = real(pv(225:235), prec)
  !pvd9 = real(pv(236:246), prec)
  !pvd10 = real(pv(247:257), prec)
  !pvd11 = real(pv(258:268), prec)
  !pvd12 = real(pv(269:279), prec)
  !pve1 = real(pv(280:280), prec)
  !pve2 = real(pv(281:281), prec)
  !pve1 = ((s15*s35*(tt*s15 + tt*s25 - s25*s35 + ss*(-2*tt - s15 + s35)) + 2*me2*(tt*s25*s&
  !        &35 + s15**2*(tt + s35) + s15*(tt*s25 + tt*s35 - s35**2)))*pvd10(1) + (-((ss - s1&
  !        &5 - s25)*s35*(-(tt*s15) + ss*(s15 - s35) + s25*(-tt + s35))) + me2*(4*ss*(s15 - &
  !        &s35)*s35 - 2*(s15 + s25)*(tt*s25 - 2*s35**2 + s15*(tt + s35))))*pvd11(1) - (tt*(&
  !        &ss - s15 - s25)*(tt*s15 + s25*(tt - s35) + ss*(s15 + s35)) + 2*me2*(2*tt*(-ss + &
  !        &s25)*s35 + s15**2*(tt + s35) + tt*s15*(-2*ss + s25 + 2*s35)))*pvd2(1) + (ss*tt*(&
  !        &tt*s15 + tt*s25 - 2*s15*s35 - s25*s35 + ss*(s15 + s35)) + 2*me2*(tt*(-2*ss + s25&
  !        &)*s35 + s15*(-2*ss*tt + tt*s35 + s35**2)))*pvd4(1) + (ss*s15*(tt*s15 + ss*(s15 -&
  !        & s35) + s25*(tt + s35)) - 2*me2*(2*ss*s15*(s15 - s35) + (s15 + s25)*(tt*s25 + s1&
  !        &5*(tt + s35))))*pvd6(1))/(2*ss*tt*s15*(-ss + s15 + s25)*s35 - 2*me2*(tt**2*s25**&
  !        &2 + s15**2*(tt + s35)**2 + 2*tt*s15*(-2*ss*s35 + s25*(tt + s35))))
  !pve2 = ((s15*s35*(tt*(2*tt + s15 - 2*s35) + ss*(2*tt + s15 - s35) + s25*(-tt + s35)) - &
  !        &2*me2*(s15**2*s35 + tt*(s25 - s35)*s35 + s15*(tt*s25 + 3*tt*s35 - s35**2)))*pvd1&
  !        &0(1) + ((ss + tt - s25)*s35*(-(tt*s15) + s25*(tt - s35) + ss*(-s15 + s35)) + 2*m&
  !        &e2*(-(tt*s25**2) + 2*ss*(s15 - s35)*s35 - tt*s35**2 + 2*s25*s35**2 + s15*s35*(2*&
  !        &tt - s25 + s35)))*pvd12(1) - (tt*(ss + tt - s25)*(s15*(tt - 2*s35) + s25*(tt - s&
  !        &35) + ss*(s15 + s35)) + 2*me2*(s15**2*s35 + tt*s15*(-2*ss - 2*tt + s25 + 3*s35) &
  !        &- 2*tt*(s25*(tt - s35) + ss*s35)))*pvd3(1) + (tt*(ss + tt - s35)*(tt*s15 + s25*(&
  !        &tt - s35) + ss*(s15 + s35)) + 2*me2*(s15*(-2*tt*(ss + tt) + s35**2) + tt*(-2*tt*&
  !        &s25 - 2*ss*s35 + s25*s35 + s35**2)))*pvd5(1) + (s15*(ss + tt - s35)*(tt*(s15 - 2&
  !        &*s35) + ss*(s15 - s35) + s25*(tt + s35)) - 2*me2*(2*tt*s15**2 + 2*ss*s15*(s15 - &
  !        &s35) + tt*(s25 - s35)**2 + s15*(2*tt*s25 - 4*tt*s35 + s25*s35 + s35**2)))*pvd7(1&
  !        &))/(-2*tt*s15*(ss + tt - s25)*(ss + tt - s35)*s35 - 2*me2*(tt**2*(s25 - s35)**2 &
  !        &+ s15**2*s35**2 + 2*tt*s15*s35*(-2*ss - 2*tt + s25 + s35)))
  !END SUBROUTINE PV_EVALUATE




                          !!!!!!!!!!!!!!!!!!!!!!
                              END MODULE ee_ee2eegl_coll
                          !!!!!!!!!!!!!!!!!!!!!!
