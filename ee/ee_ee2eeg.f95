
                          !!!!!!!!!!!!!!!!!!!!!!
                            MODULE ee_EE2EEG
                          !!!!!!!!!!!!!!!!!!!!!!
    use functions
    use collier
    implicit none


  contains


  FUNCTION EE2EEG(p1,p2,p3,p4,p5)
    !! e-(p1) e-(p2) -> e-(p3) e-(p4) g(p5)
    real(kind=prec) :: p1(4),p2(4),p3(4),p4(4),p5(4)
    real(kind=prec) :: ee2eeg

    ee2eeg = ee2eeg_tr_tchannel_up(p1,p2,p3,p4,p5) + ee2eeg_bx_tchannel_up(p1,p2,p3,p4,p5) &
           + ee2eeg_tr_tchannel_up(p1,p2,p4,p3,p5) + ee2eeg_bx_tchannel_up(p1,p2,p4,p3,p5) &
           + ee2eeg_tr_tchannel_up(p2,p1,p4,p3,p5) + ee2eeg_bx_tchannel_up(p2,p1,p4,p3,p5) &
           + ee2eeg_tr_tchannel_up(p2,p1,p3,p4,p5) + ee2eeg_bx_tchannel_up(p2,p1,p3,p4,p5)


  END FUNCTION


  FUNCTION EE2EEG_TR_TCHANNEL_UP(p1,p2,p3,p4,p5)
    real(kind=prec) :: ee2eeg_tr_tchannel_up
    real(kind=prec) :: p1(4),p2(4),p3(4),p4(4),p5(4)
    real(kind=prec) :: ss,tt,s15,s25,s35,m2

    ss = sq(p1+p2); tt = sq(p2-p4)
    s15 = s(p1,p5); s25 = s(p2,p5)
    s35 = s(p3,p5); m2 = sq(p1)

    ee2eeg_tr_tchannel_up = (-128*m2**4*(s15 - s35)**2*(s15 + s25 - s35) - 16*m2**3*(2*s15**3*(s25 - 5*ss - &
                        &tt) + (s25 - s35)*s35**2*(10*s25 - 10*ss + tt) + 2*s15**2*(s25**2 - s25*(7*s35 +&
                        & 5*ss + tt) + s35*(15*ss + 4*tt)) + s15*s35*(-12*s25**2 - 5*s35*(6*ss + tt) + s2&
                        &5*(22*s35 + 20*ss + 9*tt))) - 8*m2**2*(2*s15**4*s35 + 2*s15**3*(-s35**2 + s25*(s&
                        &35 - 2*ss) + 6*s35*tt + (2*ss + tt)**2) + 2*(s25 - s35)*s35**2*(4*s25**2 + 4*ss*&
                        &*2 + 2*ss*tt + tt**2 - 2*s25*(4*ss + tt)) + s15*s35*(-4*s25**3 + s25**2*(12*s35 &
                        &+ 20*ss + 19*tt) + s25*(2*s35**2 - 36*s35*ss - 16*ss**2 - 23*s35*tt - 32*ss*tt -&
                        & 5*tt**2) + s35*(-2*s35**2 + 24*ss**2 + 7*s35*tt + 29*ss*tt + 4*tt**2)) + s15**2&
                        &*(-4*s25**2*(s35 + ss) + s35*(2*s35**2 - 24*ss**2 - 16*s35*tt - 33*ss*tt - 7*tt*&
                        &*2) + 2*s25*((2*ss + tt)**2 + 3*s35*(4*ss + 5*tt)))) + s15*s35*tt*(-4*s25**4 + s&
                        &15**3*(-2*s25 + ss + tt) + s25**3*(3*s35 + 12*ss + 7*tt) - s25**2*(2*s35**2 + 5*&
                        &s35*ss + 16*ss**2 + 2*s35*tt + 18*ss*tt + 8*tt**2) - 2*s35*(3*ss**3 + 6*ss**2*tt&
                        & + 6*ss*tt**2 + 2*tt**3 + s35**2*(ss + tt) - 2*s35*(ss + tt)**2) + 2*s25*(s35**3&
                        & + 4*ss**3 + 7*ss**2*tt + 6*ss*tt**2 + 2*tt**3 - s35**2*(ss + tt) + 2*s35*(2*ss*&
                        &*2 + 2*ss*tt + tt**2)) + s15**2*(-6*s25**2 + s25*(s35 + 8*ss + 3*tt) - ss*(s35 +&
                        & 2*(ss + tt))) + s15*(-8*s25**3 + s25**2*(2*s35 + 19*ss + 9*tt) - 2*s25*(s35**2 &
                        &+ 9*ss**2 - 2*s35*tt + 10*ss*tt + 4*tt**2) + 2*(3*ss**3 + 7*ss**2*tt + 6*ss*tt**&
                        &2 + 2*tt**3 + s35**2*(ss + tt) - s35*(ss**2 + 4*ss*tt + 2*tt**2)))) - 2*m2*(2*s1&
                        &5**4*s35*(s25 - ss) + 2*(s25 - s35)*s35**2*(2*s25**3 - 2*ss**3 - 3*ss**2*tt - 3*&
                        &ss*tt**2 - tt**3 - 3*s25**2*(2*ss + tt) + 3*s25*(2*ss**2 + 2*ss*tt + tt**2)) + s&
                        &15**3*(2*s25**2*s35 + s35**2*(2*ss + 3*tt) - 3*s35*tt*(5*ss + 3*tt) + s25*(-2*s3&
                        &5**2 - 2*s35*ss + 4*ss**2 + 20*s35*tt + 4*ss*tt + 2*tt**2) - 2*(2*ss**3 + 4*ss**&
                        &2*tt + 3*ss*tt**2 + tt**3)) + s15*s35*(4*s25**3*(s35 + 2*ss + 5*tt) + s25**2*(2*&
                        &s35**2 - 20*s35*ss - 16*ss**2 - 27*s35*tt - 58*ss*tt - 19*tt**2) + 2*s25*(-s35**&
                        &3 + 4*ss**3 - s35**2*(ss - 5*tt) + 23*ss**2*tt + 18*ss*tt**2 + 6*tt**3 + s35*(14&
                        &*ss**2 + 27*ss*tt + 8*tt**2)) + s35*(2*s35**2*(ss - tt) + s35*tt*(ss + 4*tt) - 2&
                        &*(6*ss**3 + 21*ss**2*tt + 17*ss*tt**2 + 7*tt**3))) + s15**2*(s25**2*(8*s35*ss + &
                        &4*ss**2 + 39*s35*tt + 4*ss*tt + 2*tt**2) + 2*s25*(s35**3 - 2*ss**3 - 9*s35**2*tt&
                        & - 4*ss**2*tt - 3*ss*tt**2 - tt**3 - s35*(10*ss**2 + 39*ss*tt + 15*tt**2)) + s35&
                        &*(-(s35**2*(2*ss + tt)) + 2*s35*tt*(6*ss + tt) + 2*(6*ss**3 + 22*ss**2*tt + 21*s&
                        &s*tt**2 + 7*tt**3)))))/(s15**2*(s15 + s25 - s35)*s35**2*(4*m2 + s25 - ss - tt)*t&
                        &t**2)

    ee2eeg_tr_tchannel_up = 128*pi**3*alpha**3*ee2eeg_tr_tchannel_up

  END FUNCTION



  FUNCTION EE2EEG_BX_TCHANNEL_UP(p1,p2,p3,p4,p5)
    real(kind=prec) :: p1(4),p2(4),p3(4),p4(4),p5(4)
    real(kind=prec) :: ee2eeg_bx_tchannel_up
    real(kind=prec) :: ss,tt,s15,s25,s35,m2

    ss = sq(p1+p2); tt = sq(p2-p4)
    s15 = s(p1,p5); s25 = s(p2,p5)
    s35 = s(p3,p5); m2 = sq(p1)

    ee2eeg_bx_tchannel_up = -((128*m2**4*(s15 - s35)**2*s35 + 16*m2**3*(s15**3*(3*s25 + 7*s35) - s35*(s35**2&
                        &*(s35 + 14*ss - tt) + 4*s25*s35*(-3*s35 + tt) + s25**2*(s35 + 3*tt)) + s15**2*(3&
                        &*s25**2 + s25*(2*s35 + 3*tt) - s35*(19*s35 + 14*ss + 3*tt)) + s15*(s35**2*(13*s3&
                        &5 + 28*ss + 2*tt) + s25**2*(-2*s35 + 3*tt) - s25*s35*(17*s35 + 7*tt))) + 8*m2**2&
                        &*(s15**4*s35 + s15**3*(s25*(6*s35 - 4*ss) - s35*(4*s35 + 15*ss + 9*tt)) + s15**2&
                        &*(s25**2*(3*s35 - 4*ss) - s25*(25*s35**2 + 11*s35*ss + 12*s35*tt + 4*ss*tt) + s3&
                        &5*(-s35**2 + 35*s35*ss + 18*ss**2 + 12*s35*tt + 13*ss*tt + tt**2)) - s15*(3*s25*&
                        &*3*s35 + s25**2*(15*s35**2 - 2*s35*ss + 4*s35*tt + 4*ss*tt) - s25*s35*(15*s35**2&
                        & + 44*s35*ss + 11*ss*tt + 2*tt**2) + s35**2*(-4*s35**2 + 19*s35*ss + 36*ss**2 + &
                        &2*s35*tt + 16*ss*tt + 3*tt**2)) - s35*(s25**3*(s35 + 3*tt) + s25*s35*(29*s35*ss &
                        &+ 3*s35*tt - 13*ss*tt + tt**2) - s25**2*(12*s35**2 + 2*s35*ss - 7*s35*tt + 4*ss*&
                        &tt + 3*tt**2) + s35**2*(-18*ss**2 - 3*ss*tt - 2*tt**2 + s35*(ss + tt)))) + s35*(&
                        &s25**4*(s35 - tt)**2 - s15**5*(ss + tt) + s25**3*(s35 - tt)*(s35**2 + 2*ss*tt - &
                        &s35*(5*ss + tt)) + s15**4*(3*ss*(ss + tt) + s35*(2*ss + tt) - s25*(s35 + 3*ss + &
                        &2*tt)) + s25**2*(2*ss**2*tt**2 - s35**3*(3*ss + 2*tt) - 2*s35*tt*(5*ss**2 + 2*ss&
                        &*tt + tt**2) + s35**2*(10*ss**2 + 7*ss*tt + 4*tt**2)) + s35**2*ss*(-s35**3 + 3*s&
                        &35**2*(ss + tt) - 4*s35*(ss + tt)**2 + 2*(2*ss**3 + 3*ss**2*tt + 3*ss*tt**2 + tt&
                        &**3)) + s25*s35*(s35**4 - s35**3*(3*ss + 4*tt) + s35**2*(6*ss**2 + 13*ss*tt + 7*&
                        &tt**2) + 2*tt*(3*ss**3 + 3*ss**2*tt + 3*ss*tt**2 + tt**3) - 2*s35*(5*ss**3 + 7*s&
                        &s**2*tt + 8*ss*tt**2 + 3*tt**3)) + s15**3*(-3*s25**2*(s35 + ss) + s25*(s35**2 + &
                        &9*s35*ss + 6*ss**2 + 4*ss*tt + tt**2) - ss*(s35**2 + 4*ss*(ss + tt) + 3*s35*(2*s&
                        &s + tt))) + s15**2*(-(s25**3*(3*s35 + ss - 2*tt)) - s35**3*(ss + tt) + 3*s35**2*&
                        &(2*ss**2 + 2*ss*tt + tt**2) + s25**2*(3*s35**2 + 11*s35*ss + 4*ss**2 - 6*s35*tt &
                        &+ ss*tt + 4*tt**2) + 4*s35*(ss**3 - 2*ss**2*tt - 3*ss*tt**2 - tt**3) + 2*(2*ss**&
                        &4 + 5*ss**3*tt + 6*ss**2*tt**2 + 4*ss*tt**3 + tt**4) - 2*s25*(3*s35**2*ss + 3*ss&
                        &**3 + 4*ss**2*tt + 4*ss*tt**2 + tt**3 + s35*(7*ss**2 - 3*ss*tt - tt**2))) + s15*&
                        &(s25**4*(-s35 + tt) + s25**3*(2*s35**2 + 6*s35*(ss - tt) + tt*(-ss + 4*tt)) - s2&
                        &5**2*(s35**2*(5*ss - 3*tt) + 2*tt**2*(4*ss + tt) + s35*(14*ss**2 - 6*ss*tt + tt*&
                        &*2)) + s25*(-s35**4 + s35**3*(3*ss + 2*tt) + s35**2*(2*ss**2 - 11*ss*tt - 3*tt**&
                        &2) + 2*s35*ss*(8*ss**2 + 3*ss*tt + 3*tt**2) + 2*tt*(ss**3 + 4*ss**2*tt + 3*ss*tt&
                        &**2 + tt**3)) + s35*(s35**3*(2*ss + tt) - 2*(2*ss + tt)**2*(ss**2 + ss*tt + tt**&
                        &2) - 3*s35**2*(2*ss**2 + 3*ss*tt + tt**2) + 4*s35*(ss**3 + 5*ss**2*tt + 4*ss*tt*&
                        &*2 + tt**3)))) + 2*m2*(2*s15**5*s35 - s15**4*s35*(-5*s25 + 9*s35 + 8*ss + 4*tt) &
                        &+ s15**3*(4*s25**2*s35 - s25*(20*s35**2 + 21*s35*ss - 2*ss**2 + 6*s35*tt) + s35*&
                        &(13*s35**2 + 20*s35*ss + 20*ss**2 - 2*s35*tt + 17*ss*tt + tt**2)) + s15*(2*s25**&
                        &3*s35*(-7*s35 + ss) + s25**2*(18*s35**3 + 2*s35**2*(24*ss - 5*tt) + 2*ss**2*tt +&
                        & s35*tt*(2*ss + 13*tt)) + s35**2*(5*s35**3 + 40*ss**3 + s35**2*(8*ss - 2*tt) + 4&
                        &4*ss**2*tt + 28*ss*tt**2 + 8*tt**3 + s35*(8*ss**2 - 25*ss*tt - 8*tt**2)) - s25*s&
                        &35*(8*s35**3 + 3*s35**2*(11*ss - 4*tt) + 2*tt*(5*ss**2 + 8*ss*tt + 2*tt**2) + 2*&
                        &s35*(35*ss**2 + 2*ss*tt + 6*tt**2))) + s15**2*(s25**3*s35 - s25**2*(22*s35**2 + &
                        &14*s35*ss - 2*ss**2 + 3*s35*tt) - s35*(11*s35**3 + 20*ss**3 + 2*s35**2*(7*ss - 5&
                        &*tt) + 30*ss**2*tt + 18*ss*tt**2 + 6*tt**3 + s35*(36*ss**2 + 5*ss*tt - 3*tt**2))&
                        & + s25*(15*s35**3 + s35**2*(63*ss - 10*tt) + 2*ss**2*tt + s35*(24*ss**2 + 25*ss*&
                        &tt + 13*tt**2))) + s35*(s25**3*(9*s35**2 + 2*s35*(ss - 6*tt) + tt*(2*ss + 3*tt))&
                        & - s25**2*(2*ss*tt*(ss + 4*tt) + s35**2*(34*ss + 5*tt) + s35*(2*ss**2 - 30*ss*tt&
                        & - 5*tt**2)) + s25*s35*(4*s35**3 - 5*s35**2*(ss + 2*tt) - 4*tt*(6*ss**2 + 2*ss*t&
                        &t + tt**2) + s35*(44*ss**2 + 21*ss*tt + 10*tt**2)) + s35**2*(-2*s35**2*(3*ss + t&
                        &t) + s35*(8*ss**2 + 13*ss*tt + 4*tt**2) - 2*(10*ss**3 + 7*ss**2*tt + 5*ss*tt**2 &
                        &+ tt**3)))))/(s15*s25*(s15 + s25 - s35)*s35**2*(4*m2 + s35 - ss - tt)*tt*(s15 - &
                        &s35 + tt)))


    ee2eeg_bx_tchannel_up = 128*pi**3*alpha**3*ee2eeg_bx_tchannel_up

  END FUNCTION







                          !!!!!!!!!!!!!!!!!!!!!!
                          END MODULE ee_EE2EEG
                          !!!!!!!!!!!!!!!!!!!!!!
