                          !!!!!!!!!!!!!!!!!!!!!!
                            MODULE ee_twoloop
                          !!!!!!!!!!!!!!!!!!!!!!
    use functions
    implicit none
    real(kind=prec), parameter :: zeta(2:5) = (/1.6449340668482264365, &
                                                1.2020569031595942854, &
                                                1.0823232337111381915, &
                                                1.0369277551433699263 /)
    real(kind=prec), parameter :: oopisq = 1/pi/pi

    complex(kind=prec) Hc1(0:+1)
    complex(kind=prec) Hc2(0:+1,0:+1)
    complex(kind=prec) Hc3(0:+1,0:+1,0:+1)
    complex(kind=prec) Hc4(0:+1,0:+1,0:+1,0:+1)

    real(kind=prec) :: tm6xm4x2, om2xp4x2, born, om2x
    real(kind=prec) :: xpow(-2:4), omx(-2:2)

    integer :: prescription = +1
  contains

                          !!!!!!!!!!!!!!!!!!!!!!
                          !   WARNING WARNING  !
                          !!!!!!!!!!!!!!!!!!!!!!
  ! Factors of alpha (and potentially pi) are missing!!

                          !!!!!!!!!!!!!!!!!!!!!!
                          !      ONE LOOP      !
                          !!!!!!!!!!!!!!!!!!!!!!

  FUNCTION EE2MMl0(p1, p2, p3, p4, doub, sing, lin, quad, log_prescription)
  implicit none
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4)
  real(kind=prec) :: ee2mml0
  complex(kind=prec) :: lo, d, s, l, f
  real(kind=prec), optional :: doub, sing, lin, quad
  integer, optional :: log_prescription
  integer :: t_log_prescription = +1
  real(kind=prec) :: tt, ss
  ss = sq(p1+p2)
  tt = sq(p1-p3)
  call init_hpls(-tt/ss)

  if (present(log_prescription)) t_log_prescription = log_prescription
  if( (t_log_prescription == -1).and.(prescription == -1)) then
    ! In the 1/x bit of Moller, there's a factor (-1)^2ep missing
    lo = (log(cmplx(ss / musq)) + 2*imag*pi)
  else
    lo = (log(cmplx(ss / musq)))
  endif

  d = eemma1ed()
  s = eemma1es()
  f = eemma1ef()
  l = eemma1e1()


  if(present(doub)) doub = real(d)
  if(present(sing)) sing = real(s - lo*d)
  ee2mml0 = real(f - lo*s + lo**2*d/2.)
  if(present(lin )) lin  = real(l          - lo*f + lo**2*s/2. - lo**3*d/6)
  if(present(quad)) quad = real(eemma1e2() - lo*l + lo**2*f/2. - lo**3*s/6 + lo**4*d/24.)
  END FUNCTION

  FUNCTION EEexchL0(p1, p2, p3, p4, doub, sing, lin, quad)
  implicit none
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4)
  real(kind=prec) :: eeexchl0
  complex(kind=prec) :: lo, d, s, l, f
  real(kind=prec), optional :: doub, sing, lin, quad
  real(kind=prec) :: tt, ss
  ss = sq(p1+p2)
  tt = sq(p1-p3)
  call init_hpls(-tt/ss)
  lo = log(cmplx(ss / musq))

  d = excha1ed()
  s = excha1es()
  f = excha1ef()
  l = excha1e1()


  if(present(doub)) doub = real(d)
  if(present(sing)) sing = real(s - lo*d)
  eeexchl0 = real(f - lo*s + lo**2*d/2.)
  if(present(lin )) lin  = real(l          - lo*f + lo**2*s/2. - lo**3*d/6)
  if(present(quad)) quad = real(excha1e2() - lo*l + lo**2*f/2. - lo**3*s/6 + lo**4*d/24.)
  END FUNCTION




  FUNCTION EM2EMl0(p1, p2, p3, p4, doub, sing, lin, quad)
  implicit none
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4)
  real(kind=prec) :: em2eml0
  real(kind=prec), optional :: doub, sing, lin, quad
  em2eml0 = ee2mml0(p1,-p3,-p2,p4, doub, sing, lin, quad)
  END FUNCTION

  FUNCTION EB2EBl0(p1, p2, p3, p4, doub, sing, lin, quad)
  implicit none
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4)
  real(kind=prec) :: eb2ebl0
  real(kind=prec) :: d, s, l, q
  real(kind=prec), optional :: doub, sing, lin, quad

  eb2ebl0 = ee2mml0(p1, p2, p3,p4, d, s, l, q, 1)
  if(present(doub)) doub = d
  if(present(sing)) sing = s
  if(present(lin )) lin  = l
  if(present(quad)) quad = q

  eb2ebl0 = eb2ebl0 + ee2mml0(p1,-p3,-p2,p4, d, s, l, q, -1)
  if(present(doub)) doub = doub + d
  if(present(sing)) sing = sing + s
  if(present(lin )) lin  = lin  + l
  if(present(quad)) quad = quad + q

  eb2ebl0 = eb2ebl0 + eeexchl0(p1,p2,p3,p4, d, s, l, q)
  if(present(doub)) doub = doub + d
  if(present(sing)) sing = sing + s
  if(present(lin )) lin  = lin  + l
  if(present(quad)) quad = quad + q
  END FUNCTION

  FUNCTION EE2EEl0(p1, p2, p3, p4, doub, sing, lin, quad)
  implicit none
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4)
  real(kind=prec) :: ee2eel0
  real(kind=prec), optional :: doub, sing, lin, quad
  prescription = -1
  ee2eel0 = eb2ebl0(p1,-p3,p4,-p2, doub, sing, lin, quad)
  prescription = +1
  END FUNCTION



                          !!!!!!!!!!!!!!!!!!!!!!
                          !      TWO LOOP      !
                          !!!!!!!!!!!!!!!!!!!!!!

  FUNCTION EE2MMll0(p1, p2, p3, p4, quad, trip, doub, sing, log_prescription)
  implicit none
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4)
  real(kind=prec) :: ee2mmll0
  complex(kind=prec) :: l, q, t, d, s
  real(kind=prec), optional :: quad, trip, doub, sing
  integer, optional :: log_prescription
  integer :: t_log_prescription = +1
  real(kind=prec) :: tt, ss
  ss = sq(p1+p2)
  tt = sq(p1-p3)
  call init_hpls(-tt/ss)

  if (present(log_prescription)) t_log_prescription = log_prescription
  if( (t_log_prescription == -1).and.(prescription == -1)) then
    ! In the 1/x bit of Moller, there's a factor (-1)^4ep missing
    l = (2*log(cmplx(ss / musq)) + 4*imag*pi)
  else
    l = (2*log(cmplx(ss / musq)))
  endif

  q = eemma2eq()
  t = eemma2et()
  d = eemma2ed()
  s = eemma2es()

  ee2mmll0 = real(eemma2ef() - l*s + l**2*d/2. - l**3*t/6 + l**4*q/24.  - l*q/8.)

  if(present(quad)) quad = real(q)
  if(present(trip)) trip = real(t - l*q)
  if(present(doub)) doub = real(d - l*t + l**2*q/2.)
  if(present(sing)) sing = real(s - l*d + l**2*t/2. - l**3*q/6)

  END FUNCTION

  FUNCTION EEexchLL0(p1, p2, p3, p4, quad, trip, doub, sing)
  implicit none
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4)
  real(kind=prec) :: eeexchll0
  complex(kind=prec) :: l, q, t, d, s
  real(kind=prec), optional :: quad, trip, doub, sing
  real(kind=prec) :: tt, ss
  ss = sq(p1+p2)
  tt = sq(p1-p3)
  call init_hpls(-tt/ss)
  l = 2*log(cmplx(ss / musq))

  q = excha2eq()
  t = excha2et()
  d = excha2ed()
  s = excha2es()

  eeexchll0 = real(excha2ef() - l*s + l**2*d/2. - l**3*t/6 + l**4*q/24.  - l*q/8.)

  if(present(quad)) quad = real(q)
  if(present(trip)) trip = real(t - l*q)
  if(present(doub)) doub = real(d - l*t + l**2*q/2.)
  if(present(sing)) sing = real(s - l*d + l**2*t/2. - l**3*q/6)

  END FUNCTION




  FUNCTION EM2EMll0(p1, p2, p3, p4, quad, trip, doub, sing)
  implicit none
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4)
  real(kind=prec) :: em2emll0
  real(kind=prec), optional :: quad, trip, doub, sing
  em2emll0 = ee2mmll0(p1,-p3,-p2,p4, quad, trip, doub, sing)
  END FUNCTION

  FUNCTION EB2EBll0(p1, p2, p3, p4, quad, trip, doub, sing)
  implicit none
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4)
  real(kind=prec) :: eb2ebll0
  real(kind=prec), optional :: quad, trip, doub, sing
  real(kind=prec) :: q, t, d, s

  eb2ebll0 = ee2mmll0(p1, p2, p3,p4, q, t, d, s, 1)
  if(present(quad)) quad = q
  if(present(trip)) trip = t
  if(present(doub)) doub = d
  if(present(sing)) sing = s

  eb2ebll0 = eb2ebll0 + ee2mmll0(p1,-p3,-p2,p4, q, t, d, s, -1)
  if(present(quad)) quad = quad + q
  if(present(trip)) trip = trip + t
  if(present(doub)) doub = doub + d
  if(present(sing)) sing = sing + s

  eb2ebll0 = eb2ebll0 + eeexchll0(p1,p2,p3,p4, q, t, d, s)
  if(present(quad)) quad = quad + q
  if(present(trip)) trip = trip + t
  if(present(doub)) doub = doub + d
  if(present(sing)) sing = sing + s
  END FUNCTION

  FUNCTION EE2EEll0(p1, p2, p3, p4, quad, trip, doub, sing)
  implicit none
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4)
  real(kind=prec) :: ee2eell0
  real(kind=prec), optional :: quad, trip, doub, sing
  prescription = -1
  ee2eell0 = eb2ebll0(p1,-p3,p4,-p2, quad, trip, doub, sing)
  prescription = +1
  END FUNCTION



  SUBROUTINE INIT_HPLS(x)
  real(kind=prec), intent(in) :: x
  integer, parameter :: n1 =  0
  integer, parameter :: n2 = +1
  integer, parameter :: nw =  4

  real(kind=8) Hi1(0:+1)
  real(kind=8) Hi2(0:+1,0:+1)
  real(kind=8) Hi3(0:+1,0:+1,0:+1)
  real(kind=8) Hi4(0:+1,0:+1,0:+1,0:+1)
  real(kind=8) Hr1(0:+1)
  real(kind=8) Hr2(0:+1,0:+1)
  real(kind=8) Hr3(0:+1,0:+1,0:+1)
  real(kind=8) Hr4(0:+1,0:+1,0:+1,0:+1)

  call hplog(x, nw, Hc1, Hc2, Hc3, Hc4, &
                    Hr1, Hr2, Hr3, Hr4, &
                    Hi1, Hi2, Hi3, Hi4, n1, n2)

  if(prescription == -1) then
    Hc1 = conjg(Hc1)
    Hc2 = conjg(Hc2)
    Hc3 = conjg(Hc3)
    Hc4 = conjg(Hc4)
  endif

  xpow = x**(/-2,-1,0,1,2,3,4/)
  om2x = (1 - 2*x)
  omx = (1 - x)**(/-2,-1,0,1,2/)

  born = 1 - 2*x + 2*xpow(2)
  tm6xm4x2 = 3 - 6*x + 4*xpow(2)
  om2xp4x2 = 1 - 2*x + 4*xpow(2)

  END SUBROUTINE



                          !!!!!!!!!!!!!!!!!!!!!!
                          !   WARNING WARNING  !
                          !!!!!!!!!!!!!!!!!!!!!!
  ! These functions claim to produce the complex part of the matrix element.
  ! They *do not*!! They produce the complex part necessary for xing but
  ! nothing more!!

  FUNCTION EEMMA0EF()
  complex(kind=prec) :: eemmA0EF
  eemmA0EF = ReemmA0EF()
  END FUNCTION  EEMMA0EF

  FUNCTION EEMMA1ED()
  complex(kind=prec) :: eemmA1ED
  eemmA1ED = ReemmA1ED()
  END FUNCTION  EEMMA1ED

  FUNCTION EEMMA1ES()
  complex(kind=prec) :: eemmA1ES
  eemmA1ES = ReemmA1ES() + imag * IeemmA1ES()
  END FUNCTION  EEMMA1ES

  FUNCTION EEMMA1EF()
  complex(kind=prec) :: eemmA1EF
  eemmA1EF = ReemmA1EF() + imag * IeemmA1EF()
  END FUNCTION  EEMMA1EF

  FUNCTION EEMMA1E1()
  complex(kind=prec) :: eemmA1E1
  eemmA1E1 = ReemmA1E1() + imag * IeemmA1E1()
  END FUNCTION  EEMMA1E1

  FUNCTION EEMMA1E2()
  complex(kind=prec) :: eemmA1E2
  eemmA1E2 = ReemmA1E2() + imag * IeemmA1E2()
  END FUNCTION  EEMMA1E2

  FUNCTION EEMMA2EQ()
  complex(kind=prec) :: eemmA2EQ
  eemmA2EQ = ReemmA2EQ()
  END FUNCTION  EEMMA2EQ

  FUNCTION EEMMA2ET()
  complex(kind=prec) :: eemmA2ET
  eemmA2ET = ReemmA2ET() + imag * IeemmA2ET()
  END FUNCTION  EEMMA2ET

  FUNCTION EEMMA2ED()
  complex(kind=prec) :: eemmA2ED
  eemmA2ED = ReemmA2ED() + imag * IeemmA2ED()
  END FUNCTION  EEMMA2ED

  FUNCTION EEMMA2ES()
  complex(kind=prec) :: eemmA2ES
  eemmA2ES = ReemmA2ES() + imag * IeemmA2ES()
  END FUNCTION  EEMMA2ES

  FUNCTION EEMMA2EF()
  complex(kind=prec) :: eemmA2EF
  eemmA2EF = ReemmA2EF() + imag * IeemmA2EF()
  END FUNCTION  EEMMA2EF

  FUNCTION EXCHA0EF()
  complex(kind=prec) :: exchA0EF
  exchA0EF = RexchA0EF()
  END FUNCTION  EXCHA0EF

  FUNCTION EXCHA1ED()
  complex(kind=prec) :: exchA1ED
  exchA1ED = RexchA1ED()
  END FUNCTION  EXCHA1ED

  FUNCTION EXCHA1ES()
  complex(kind=prec) :: exchA1ES
  exchA1ES = RexchA1ES() + imag * IexchA1ES()
  END FUNCTION  EXCHA1ES

  FUNCTION EXCHA1EF()
  complex(kind=prec) :: exchA1EF
  exchA1EF = RexchA1EF() + imag * IexchA1EF()
  END FUNCTION  EXCHA1EF

  FUNCTION EXCHA1E1()
  complex(kind=prec) :: exchA1E1
  exchA1E1 = RexchA1E1() + imag * IexchA1E1()
  END FUNCTION  EXCHA1E1

  FUNCTION EXCHA1E2()
  complex(kind=prec) :: exchA1E2
  exchA1E2 = RexchA1E2() + imag * IexchA1E2()
  END FUNCTION  EXCHA1E2

  FUNCTION EXCHA2EQ()
  complex(kind=prec) :: exchA2EQ
  exchA2EQ = RexchA2EQ()
  END FUNCTION  EXCHA2EQ

  FUNCTION EXCHA2ET()
  complex(kind=prec) :: exchA2ET
  exchA2ET = RexchA2ET() + imag * IexchA2ET()
  END FUNCTION  EXCHA2ET

  FUNCTION EXCHA2ED()
  complex(kind=prec) :: exchA2ED
  exchA2ED = RexchA2ED() + imag * IexchA2ED()
  END FUNCTION  EXCHA2ED

  FUNCTION EXCHA2ES()
  complex(kind=prec) :: exchA2ES
  exchA2ES = RexchA2ES() + imag * IexchA2ES()
  END FUNCTION  EXCHA2ES

  FUNCTION EXCHA2EF()
  complex(kind=prec) :: exchA2EF
  exchA2EF = RexchA2EF() + imag * IexchA2EF()
  END FUNCTION  EXCHA2EF

  FUNCTION ReemmA0EF()
  implicit none
  complex(kind=prec) :: ReemmA0EF

  ReemmA0EF = born

  ReemmA0EF = 32 * pi**2 * ReemmA0EF

  END FUNCTION ReemmA0EF


  FUNCTION ReemmA1ED()
  implicit none
  complex(kind=prec) :: ReemmA1ED

  ReemmA1ED = -2*born

  ReemmA1ED = 32 * pi**1 * ReemmA1ED

  END FUNCTION ReemmA1ED


  FUNCTION ReemmA1ES()
  implicit none
  complex(kind=prec) :: ReemmA1ES

  ReemmA1ES = -3*born
  ReemmA1ES = ReemmA1ES + Hc1(0) * (2*born)
  ReemmA1ES = ReemmA1ES + Hc1(1) * (2*born)

  ReemmA1ES = 32 * pi**1 * ReemmA1ES

  END FUNCTION ReemmA1ES


  FUNCTION ReemmA1EF()
  implicit none
  complex(kind=prec) :: ReemmA1EF

  ReemmA1EF = -7*born
  ReemmA1EF = ReemmA1EF + Hc1(0) * (omx(1))
  ReemmA1EF = ReemmA1EF + Hc1(1) * (xpow(1))
  ReemmA1EF = ReemmA1EF + Hc2(0,0) * (-om2x)
  ReemmA1EF = ReemmA1EF + Hc2(1,1) * (-om2x)
  ReemmA1EF = ReemmA1EF + zeta(2) * (6*born)

  ReemmA1EF = 32 * pi**1 * ReemmA1EF

  END FUNCTION ReemmA1EF


  FUNCTION ReemmA1E1()
  implicit none
  complex(kind=prec) :: ReemmA1E1

  ReemmA1E1 = -15*born
  ReemmA1E1 = ReemmA1E1 + Hc1(0) * (2*omx(1))
  ReemmA1E1 = ReemmA1E1 + Hc1(0)*zeta(2) * (-3*om2xp4x2)
  ReemmA1E1 = ReemmA1E1 + Hc1(1) * (2*xpow(1))
  ReemmA1E1 = ReemmA1E1 + Hc1(1)*zeta(2) * (-3*tm6xm4x2)
  ReemmA1E1 = ReemmA1E1 + Hc2(0,0) * (-(omx(-1)*(1 - 3*xpow(1) + xpow(2))))
  ReemmA1E1 = ReemmA1E1 + Hc2(1,1) * (1 - xpow(-1) + xpow(1))
  ReemmA1E1 = ReemmA1E1 + Hc3(0,0,0) * (-4*xpow(2))
  ReemmA1E1 = ReemmA1E1 + Hc3(0,1,1) * (-tm6xm4x2)
  ReemmA1E1 = ReemmA1E1 + Hc3(1,0,0) * (-om2xp4x2)
  ReemmA1E1 = ReemmA1E1 + Hc3(1,1,1) * (-4*omx(2))
  ReemmA1E1 = ReemmA1E1 + zeta(2) * (6*(1 - 2*xpow(1) + 3*xpow(2)))
  ReemmA1E1 = ReemmA1E1 + zeta(3) * (5 - 10*xpow(1) + 12*xpow(2))

  ReemmA1E1 = 32 * pi**1 * ReemmA1E1

  END FUNCTION ReemmA1E1


  FUNCTION ReemmA1E2()
  implicit none
  complex(kind=prec) :: ReemmA1E2

  ReemmA1E2 = -31*born
  ReemmA1E2 = ReemmA1E2 + Hc1(0) * (4*omx(1))
  ReemmA1E2 = ReemmA1E2 + Hc1(0)*zeta(2) * (-3*omx(-1)*xpow(1))
  ReemmA1E2 = ReemmA1E2 + Hc1(0)*zeta(3) * (-5 + 10*xpow(1) - 12*xpow(2))
  ReemmA1E2 = ReemmA1E2 + Hc1(1) * (4*xpow(1))
  ReemmA1E2 = ReemmA1E2 + Hc1(1)*zeta(2) * (-3*(-1 + xpow(-1)))
  ReemmA1E2 = ReemmA1E2 + Hc1(1)*zeta(3) * (-5 + 10*xpow(1) - 12*xpow(2))
  ReemmA1E2 = ReemmA1E2 + Hc2(0,0) * (-2*omx(-1)*(1 - 3*xpow(1) + xpow(2)))
  ReemmA1E2 = ReemmA1E2 + Hc2(0,0)*zeta(2) * (3*om2xp4x2)
  ReemmA1E2 = ReemmA1E2 + Hc2(0,1)*zeta(2) * (-3*tm6xm4x2)
  ReemmA1E2 = ReemmA1E2 + Hc2(1,0)*zeta(2) * (3*om2xp4x2)
  ReemmA1E2 = ReemmA1E2 + Hc2(1,1) * (-2*(-1 + xpow(-1) - xpow(1)))
  ReemmA1E2 = ReemmA1E2 + Hc2(1,1)*zeta(2) * (-3*tm6xm4x2)
  ReemmA1E2 = ReemmA1E2 + Hc3(0,0,0) * (omx(-1)*(1 - 4*xpow(1) + xpow(2)))
  ReemmA1E2 = ReemmA1E2 + Hc3(0,1,1) * (1 - xpow(-1))
  ReemmA1E2 = ReemmA1E2 + Hc3(1,0,0) * (-(omx(-1)*xpow(1)))
  ReemmA1E2 = ReemmA1E2 + Hc3(1,1,1) * (2 - 2*xpow(-1) + xpow(1))
  ReemmA1E2 = ReemmA1E2 + Hc4(0,0,0,0) * (1 - 2*xpow(1) + 8*xpow(2))
  ReemmA1E2 = ReemmA1E2 + Hc4(0,0,1,1) * (-tm6xm4x2)
  ReemmA1E2 = ReemmA1E2 + Hc4(0,1,0,0) * (om2xp4x2)
  ReemmA1E2 = ReemmA1E2 + Hc4(0,1,1,1) * (-2*tm6xm4x2)
  ReemmA1E2 = ReemmA1E2 + Hc4(1,0,0,0) * (2*om2xp4x2)
  ReemmA1E2 = ReemmA1E2 + Hc4(1,0,1,1) * (-tm6xm4x2)
  ReemmA1E2 = ReemmA1E2 + Hc4(1,1,0,0) * (om2xp4x2)
  ReemmA1E2 = ReemmA1E2 + Hc4(1,1,1,1) * (-7 + 14*xpow(1) - 8*xpow(2))
  ReemmA1E2 = ReemmA1E2 + zeta(2) * (3*(5 - 10*xpow(1) + 14*xpow(2)))
  ReemmA1E2 = ReemmA1E2 + zeta(3) * (omx(-1)*(6 - 17*xpow(1) + 24*xpow(2) - 12*xpow(3)))
  ReemmA1E2 = ReemmA1E2 + zeta(4) * ((3*(7 - 14*xpow(1) + 32*xpow(2)))/4.)

  ReemmA1E2 = 32 * pi**1 * ReemmA1E2

  END FUNCTION ReemmA1E2


  FUNCTION ReemmA2EQ()
  implicit none
  complex(kind=prec) :: ReemmA2EQ

  ReemmA2EQ = 2*born

  ReemmA2EQ = 32 * pi**0 * ReemmA2EQ

  END FUNCTION ReemmA2EQ


  FUNCTION ReemmA2ET()
  implicit none
  complex(kind=prec) :: ReemmA2ET

  ReemmA2ET = 6*born
  ReemmA2ET = ReemmA2ET + Hc1(0) * (-4*born)
  ReemmA2ET = ReemmA2ET + Hc1(1) * (-4*born)

  ReemmA2ET = 32 * pi**0 * ReemmA2ET

  END FUNCTION ReemmA2ET


  FUNCTION ReemmA2ED()
  implicit none
  complex(kind=prec) :: ReemmA2ED

  ReemmA2ED = (37*born)/2.
  ReemmA2ED = ReemmA2ED + Hc1(0) * (-2*(4 - 7*xpow(1) + 6*xpow(2)))
  ReemmA2ED = ReemmA2ED + Hc1(1) * (-2*(3 - 5*xpow(1) + 6*xpow(2)))
  ReemmA2ED = ReemmA2ED + Hc2(0,0) * (2*tm6xm4x2)
  ReemmA2ED = ReemmA2ED + Hc2(0,1) * (4*born)
  ReemmA2ED = ReemmA2ED + Hc2(1,0) * (4*born)
  ReemmA2ED = ReemmA2ED + Hc2(1,1) * (2*tm6xm4x2)
  ReemmA2ED = ReemmA2ED + zeta(2) * (-12*born)

  ReemmA2ED = 32 * pi**0 * ReemmA2ED

  END FUNCTION ReemmA2ED


  FUNCTION ReemmA2ES()
  implicit none
  complex(kind=prec) :: ReemmA2ES

  ReemmA2ES = (809*born)/16.
  ReemmA2ES = ReemmA2ES + Hc1(0) * (-7*(3 - 5*xpow(1) + 4*xpow(2)))
  ReemmA2ES = ReemmA2ES + Hc1(0)*zeta(2) * (6*(3 - 6*xpow(1) + 8*xpow(2)))
  ReemmA2ES = ReemmA2ES + Hc1(1) * (-7*(2 - 3*xpow(1) + 4*xpow(2)))
  ReemmA2ES = ReemmA2ES + Hc1(1)*zeta(2) * (6*(5 - 10*xpow(1) + 8*xpow(2)))
  ReemmA2ES = ReemmA2ES + Hc2(0,0) * (omx(-1)*(9 - 23*xpow(1) + 12*xpow(2)))
  ReemmA2ES = ReemmA2ES + Hc2(0,1) * (2)
  ReemmA2ES = ReemmA2ES + Hc2(1,0) * (2)
  ReemmA2ES = ReemmA2ES + Hc2(1,1) * (1 + 2*xpow(-1) - 4*xpow(1))
  ReemmA2ES = ReemmA2ES + Hc3(0,0,0) * (-2*(3 - 6*xpow(1) - 4*xpow(2)))
  ReemmA2ES = ReemmA2ES + Hc3(0,0,1) * (-2*om2x)
  ReemmA2ES = ReemmA2ES + Hc3(0,1,0) * (-2*om2x)
  ReemmA2ES = ReemmA2ES + Hc3(0,1,1) * (4*born)
  ReemmA2ES = ReemmA2ES + Hc3(1,0,0) * (8*xpow(2))
  ReemmA2ES = ReemmA2ES + Hc3(1,0,1) * (-2*om2x)
  ReemmA2ES = ReemmA2ES + Hc3(1,1,0) * (-2*om2x)
  ReemmA2ES = ReemmA2ES + Hc3(1,1,1) * (2*om2xp4x2)
  ReemmA2ES = ReemmA2ES + zeta(2) * ((-3*(19 - 38*xpow(1) + 46*xpow(2)))/2.)
  ReemmA2ES = ReemmA2ES + zeta(3) * (-13 + 26*xpow(1) - 30*xpow(2))

  ReemmA2ES = 32 * pi**0 * ReemmA2ES

  END FUNCTION ReemmA2ES


  FUNCTION ReemmA2EF()
  implicit none
  complex(kind=prec) :: ReemmA2EF

  ReemmA2EF = (4183*born)/32.
  ReemmA2EF = ReemmA2EF + Hc1(0) * (-53 + 83*xpow(1) - 60*xpow(2))
  ReemmA2EF = ReemmA2EF + Hc1(0)*zeta(2) * (omx(-1)*(29 - 75*xpow(1) + 127*xpow(2) - 72*xpow(3)))
  ReemmA2EF = ReemmA2EF + Hc1(0)*zeta(3) * (24*born)
  ReemmA2EF = ReemmA2EF + Hc1(1) * (-30 + 37*xpow(1) - 60*xpow(2))
  ReemmA2EF = ReemmA2EF + Hc1(1)*zeta(2) * (49 + 3*xpow(-1) - 87*xpow(1) + 72*xpow(2))
  ReemmA2EF = ReemmA2EF + Hc1(1)*zeta(3) * (3*(7 - 14*xpow(1) + 16*xpow(2)))
  ReemmA2EF = ReemmA2EF + Hc2(0,0) * ((5*om2x*omx(-1)*(9 - 7*xpow(1)))/2.)
  ReemmA2EF = ReemmA2EF + Hc2(0,0)*zeta(2) * (-(omx(-2)*(28 - 112*xpow(1) + 223*xpow(2) - 222*xpow(3) + 80*xpow(4))))
  ReemmA2EF = ReemmA2EF + Hc2(0,1) * (6)
  ReemmA2EF = ReemmA2EF + Hc2(0,1)*zeta(2) * (-2*(3 - 6*xpow(1) + 8*xpow(2)))
  ReemmA2EF = ReemmA2EF + Hc2(1,0) * (6)
  ReemmA2EF = ReemmA2EF + Hc2(1,0)*zeta(2) * (-10*(3 - 6*xpow(1) + 8*xpow(2)))
  ReemmA2EF = ReemmA2EF + Hc2(1,1) * ((-7 + 18*xpow(-1) - 6*xpow(1))/2.)
  ReemmA2EF = ReemmA2EF + Hc2(1,1)*zeta(2) * (3 + 3*xpow(-2) - 12*xpow(-1) + 18*xpow(1) - 16*xpow(2))
  ReemmA2EF = ReemmA2EF + Hc3(0,0,0) * (-(omx(-1)*(21 - 62*xpow(1) - 6*xpow(2) + 24*xpow(3)))/2.)
  ReemmA2EF = ReemmA2EF + Hc3(0,0,1) * (-(omx(-1)*(1 - 3*xpow(1))))
  ReemmA2EF = ReemmA2EF + Hc3(0,1,0) * (-(omx(-1)*(1 - 3*xpow(1))))
  ReemmA2EF = ReemmA2EF + Hc3(0,1,1) * (8 - 17*xpow(1) + 12*xpow(2))
  ReemmA2EF = ReemmA2EF + Hc3(1,0,0) * (omx(-1)*(1 + 15*xpow(2) - 12*xpow(3)))
  ReemmA2EF = ReemmA2EF + Hc3(1,0,1) * (3 - 2*xpow(-1))
  ReemmA2EF = ReemmA2EF + Hc3(1,1,0) * (3 - 2*xpow(-1))
  ReemmA2EF = ReemmA2EF + Hc3(1,1,1) * ((26 - 7*xpow(-1) - 34*xpow(1) + 24*xpow(2))/2.)
  ReemmA2EF = ReemmA2EF + Hc4(0,0,0,0) * ((omx(-2)*(1 - 4*xpow(1) - 102*xpow(2) + 212*xpow(3) - 104*xpow(4)))/2.)
  ReemmA2EF = ReemmA2EF + Hc4(0,0,0,1) * (-8*xpow(2))
  ReemmA2EF = ReemmA2EF + Hc4(0,0,1,0) * (-8*xpow(2))
  ReemmA2EF = ReemmA2EF + Hc4(0,0,1,1) * (-2*tm6xm4x2)
  ReemmA2EF = ReemmA2EF + Hc4(0,1,0,0) * (-3*(1 - 2*xpow(1) + 8*xpow(2)))
  ReemmA2EF = ReemmA2EF + Hc4(0,1,0,1) * (-8*omx(2))
  ReemmA2EF = ReemmA2EF + Hc4(0,1,1,0) * (-8*omx(2))
  ReemmA2EF = ReemmA2EF + Hc4(0,1,1,1) * (-2*(11 - 22*xpow(1) + 14*xpow(2)))
  ReemmA2EF = ReemmA2EF + Hc4(1,0,0,0) * (-2*(5 - 10*xpow(1) + 26*xpow(2)))
  ReemmA2EF = ReemmA2EF + Hc4(1,0,0,1) * (-8*xpow(2))
  ReemmA2EF = ReemmA2EF + Hc4(1,0,1,0) * (-8*xpow(2))
  ReemmA2EF = ReemmA2EF + Hc4(1,0,1,1) * (-7 + 14*xpow(1) - 8*xpow(2))
  ReemmA2EF = ReemmA2EF + Hc4(1,1,0,0) * (-6*om2xp4x2)
  ReemmA2EF = ReemmA2EF + Hc4(1,1,0,1) * (-8*omx(2))
  ReemmA2EF = ReemmA2EF + Hc4(1,1,1,0) * (-8*omx(2))
  ReemmA2EF = ReemmA2EF + Hc4(1,1,1,1) * ((-46 + 3*xpow(-2) - 12*xpow(-1) + 116*xpow(1) - 56*xpow(2))/2.)
  ReemmA2EF = ReemmA2EF + zeta(2) * ((-297 + 634*xpow(1) - 834*xpow(2))/4.)
  ReemmA2EF = ReemmA2EF + zeta(3) * (-(omx(-1)*(77 - 217*xpow(1) + 294*xpow(2) - 150*xpow(3)))/2.)
  ReemmA2EF = ReemmA2EF + zeta(4) * (17 - 34*xpow(1) - 9*xpow(2))

  ReemmA2EF = 32 * pi**0 * ReemmA2EF

  END FUNCTION ReemmA2EF


  FUNCTION RexchA0EF()
  implicit none
  complex(kind=prec) :: RexchA0EF

  RexchA0EF = -2*omx(2)*xpow(-1)

  RexchA0EF = 32 * pi**2 * RexchA0EF

  END FUNCTION RexchA0EF


  FUNCTION RexchA1ED()
  implicit none
  complex(kind=prec) :: RexchA1ED

  RexchA1ED = 4*omx(2)*xpow(-1)

  RexchA1ED = 32 * pi**1 * RexchA1ED

  END FUNCTION RexchA1ED


  FUNCTION RexchA1ES()
  implicit none
  complex(kind=prec) :: RexchA1ES

  RexchA1ES = 6*omx(2)*xpow(-1)
  RexchA1ES = RexchA1ES + Hc1(0) * (-4*omx(2)*xpow(-1))
  RexchA1ES = RexchA1ES + Hc1(1) * (-4*omx(2)*xpow(-1))

  RexchA1ES = 32 * pi**1 * RexchA1ES

  END FUNCTION RexchA1ES


  FUNCTION RexchA1EF()
  implicit none
  complex(kind=prec) :: RexchA1EF

  RexchA1EF = 14*omx(2)*xpow(-1)
  RexchA1EF = RexchA1EF + Hc1(0) * (-2*(-1 + xpow(-1))*(2 - xpow(1)))
  RexchA1EF = RexchA1EF + Hc2(0,0) * (-8 + 3*xpow(-1) + 3*xpow(1))
  RexchA1EF = RexchA1EF + Hc2(0,1) * (2*omx(2)*xpow(-1))
  RexchA1EF = RexchA1EF + Hc2(1,0) * (2*omx(2)*xpow(-1))
  RexchA1EF = RexchA1EF + zeta(2) * (-6*omx(2)*xpow(-1))

  RexchA1EF = 32 * pi**1 * RexchA1EF

  END FUNCTION RexchA1EF


  FUNCTION RexchA1E1()
  implicit none
  complex(kind=prec) :: RexchA1E1

  RexchA1E1 = 30*omx(2)*xpow(-1)
  RexchA1E1 = RexchA1E1 + Hc1(0) * (-(omx(1)*(-5 + 9*xpow(-1))))
  RexchA1E1 = RexchA1E1 + Hc1(0)*zeta(2) * (3*(xpow(-1) + xpow(1)))
  RexchA1E1 = RexchA1E1 + Hc1(1)*zeta(2) * (12*omx(2)*xpow(-1))
  RexchA1E1 = RexchA1E1 + Hc2(0,0) * (-5 + 4*xpow(-1) + 2*xpow(1))
  RexchA1E1 = RexchA1E1 + Hc3(0,0,0) * (-2*(-4 + xpow(-1) + xpow(1)))
  RexchA1E1 = RexchA1E1 + Hc3(0,0,1) * (-2*omx(2)*xpow(-1))
  RexchA1E1 = RexchA1E1 + Hc3(0,1,0) * (-2*omx(2)*xpow(-1))
  RexchA1E1 = RexchA1E1 + Hc3(0,1,1) * (2*omx(2)*xpow(-1))
  RexchA1E1 = RexchA1E1 + Hc3(1,0,0) * (-4 + 3*xpow(-1) + 3*xpow(1))
  RexchA1E1 = RexchA1E1 + Hc3(1,0,1) * (2*omx(2)*xpow(-1))
  RexchA1E1 = RexchA1E1 + Hc3(1,1,0) * (2*omx(2)*xpow(-1))
  RexchA1E1 = RexchA1E1 + Hc3(1,1,1) * (4*omx(2)*xpow(-1))
  RexchA1E1 = RexchA1E1 + zeta(2) * (-6*omx(1)*(-2 + xpow(-1)))
  RexchA1E1 = RexchA1E1 + zeta(3) * (16 - 9*xpow(-1) - 9*xpow(1))

  RexchA1E1 = 32 * pi**1 * RexchA1E1

  END FUNCTION RexchA1E1


  FUNCTION RexchA1E2()
  implicit none
  complex(kind=prec) :: RexchA1E2

  RexchA1E2 = 62*omx(2)*xpow(-1)
  RexchA1E2 = RexchA1E2 + Hc1(0) * (-(omx(1)*(-11 + 19*xpow(-1))))
  RexchA1E2 = RexchA1E2 + Hc1(0)*zeta(2) * (-3)
  RexchA1E2 = RexchA1E2 + Hc1(0)*zeta(3) * (-16 + 9*xpow(-1) + 9*xpow(1))
  RexchA1E2 = RexchA1E2 + Hc1(1)*zeta(3) * (-24 + 13*xpow(-1) + 13*xpow(1))
  RexchA1E2 = RexchA1E2 + Hc2(0,0) * (-12 + 9*xpow(-1) + 5*xpow(1))
  RexchA1E2 = RexchA1E2 + Hc2(0,0)*zeta(2) * (-3*(xpow(-1) + xpow(1)))
  RexchA1E2 = RexchA1E2 + Hc2(1,0)*zeta(2) * (-3*(-8 + 5*xpow(-1) + 5*xpow(1)))
  RexchA1E2 = RexchA1E2 + Hc2(1,1)*zeta(2) * (12*omx(2)*xpow(-1))
  RexchA1E2 = RexchA1E2 + Hc3(0,0,0) * (-2*(-2 + 2*xpow(-1) + xpow(1)))
  RexchA1E2 = RexchA1E2 + Hc3(1,0,0) * (-1)
  RexchA1E2 = RexchA1E2 + Hc4(0,0,0,0) * (-8 + xpow(-1) + xpow(1))
  RexchA1E2 = RexchA1E2 + Hc4(0,0,0,1) * (2*omx(2)*xpow(-1))
  RexchA1E2 = RexchA1E2 + Hc4(0,0,1,0) * (2*omx(2)*xpow(-1))
  RexchA1E2 = RexchA1E2 + Hc4(0,0,1,1) * (2*omx(2)*xpow(-1))
  RexchA1E2 = RexchA1E2 + Hc4(0,1,0,0) * (4 - 3*xpow(-1) - 3*xpow(1))
  RexchA1E2 = RexchA1E2 + Hc4(0,1,0,1) * (-2*omx(2)*xpow(-1))
  RexchA1E2 = RexchA1E2 + Hc4(0,1,1,0) * (-2*omx(2)*xpow(-1))
  RexchA1E2 = RexchA1E2 + Hc4(0,1,1,1) * (2*omx(2)*xpow(-1))
  RexchA1E2 = RexchA1E2 + Hc4(1,0,0,0) * (-4*(-3 + 2*xpow(-1) + 2*xpow(1)))
  RexchA1E2 = RexchA1E2 + Hc4(1,0,0,1) * (-2*omx(2)*xpow(-1))
  RexchA1E2 = RexchA1E2 + Hc4(1,0,1,0) * (-2*omx(2)*xpow(-1))
  RexchA1E2 = RexchA1E2 + Hc4(1,0,1,1) * (2*omx(2)*xpow(-1))
  RexchA1E2 = RexchA1E2 + Hc4(1,1,0,0) * (4 - 3*xpow(-1) - 3*xpow(1))
  RexchA1E2 = RexchA1E2 + Hc4(1,1,0,1) * (2*omx(2)*xpow(-1))
  RexchA1E2 = RexchA1E2 + Hc4(1,1,1,0) * (2*omx(2)*xpow(-1))
  RexchA1E2 = RexchA1E2 + Hc4(1,1,1,1) * (8*omx(2)*xpow(-1))
  RexchA1E2 = RexchA1E2 + zeta(2) * (-3*omx(1)*(-9 + 5*xpow(-1)))
  RexchA1E2 = RexchA1E2 + zeta(3) * (-((-4 + 3*xpow(-1))*(4 - 3*xpow(1))))
  RexchA1E2 = RexchA1E2 + zeta(4) * ((-9*(-4 + 5*xpow(-1) + 5*xpow(1)))/4.)

  RexchA1E2 = 32 * pi**1 * RexchA1E2

  END FUNCTION RexchA1E2


  FUNCTION RexchA2EQ()
  implicit none
  complex(kind=prec) :: RexchA2EQ

  RexchA2EQ = -4*omx(2)*xpow(-1)

  RexchA2EQ = 32 * pi**0 * RexchA2EQ

  END FUNCTION RexchA2EQ


  FUNCTION RexchA2ET()
  implicit none
  complex(kind=prec) :: RexchA2ET

  RexchA2ET = -12*omx(2)*xpow(-1)
  RexchA2ET = RexchA2ET + Hc1(0) * (8*omx(2)*xpow(-1))
  RexchA2ET = RexchA2ET + Hc1(1) * (8*omx(2)*xpow(-1))

  RexchA2ET = 32 * pi**0 * RexchA2ET

  END FUNCTION RexchA2ET


  FUNCTION RexchA2ED()
  implicit none
  complex(kind=prec) :: RexchA2ED

  RexchA2ED = -37*omx(2)*xpow(-1)
  RexchA2ED = RexchA2ED + Hc1(0) * (4*omx(1)*(-4 + 5*xpow(-1)))
  RexchA2ED = RexchA2ED + Hc1(1) * (12*omx(2)*xpow(-1))
  RexchA2ED = RexchA2ED + Hc2(0,0) * (-2*(-16 + 7*xpow(-1) + 7*xpow(1)))
  RexchA2ED = RexchA2ED + Hc2(0,1) * (-12*omx(2)*xpow(-1))
  RexchA2ED = RexchA2ED + Hc2(1,0) * (-12*omx(2)*xpow(-1))
  RexchA2ED = RexchA2ED + Hc2(1,1) * (-8*omx(2)*xpow(-1))
  RexchA2ED = RexchA2ED + zeta(2) * (12*omx(2)*xpow(-1))

  RexchA2ED = 32 * pi**0 * RexchA2ED

  END FUNCTION RexchA2ED


  FUNCTION RexchA2ES()
  implicit none
  complex(kind=prec) :: RexchA2ES

  RexchA2ES = (-809*omx(2)*xpow(-1))/8.
  RexchA2ES = RexchA2ES + Hc1(0) * (2*omx(1)*(-22 + 29*xpow(-1)))
  RexchA2ES = RexchA2ES + Hc1(0)*zeta(2) * (-6*(-4 + 3*xpow(-1) + 3*xpow(1)))
  RexchA2ES = RexchA2ES + Hc1(1) * (28*omx(2)*xpow(-1))
  RexchA2ES = RexchA2ES + Hc1(1)*zeta(2) * (-36*omx(2)*xpow(-1))
  RexchA2ES = RexchA2ES + Hc2(0,0) * (58 - 33*xpow(-1) - 21*xpow(1))
  RexchA2ES = RexchA2ES + Hc2(0,1) * (-2*omx(1)*(-5 + 7*xpow(-1)))
  RexchA2ES = RexchA2ES + Hc2(1,0) * (-2*omx(1)*(-5 + 7*xpow(-1)))
  RexchA2ES = RexchA2ES + Hc3(0,0,0) * (2*(-32 + 11*xpow(-1) + 11*xpow(1)))
  RexchA2ES = RexchA2ES + Hc3(0,0,1) * (2*(-20 + 9*xpow(-1) + 9*xpow(1)))
  RexchA2ES = RexchA2ES + Hc3(0,1,0) * (2*(-20 + 9*xpow(-1) + 9*xpow(1)))
  RexchA2ES = RexchA2ES + Hc3(0,1,1) * (4*omx(2)*xpow(-1))
  RexchA2ES = RexchA2ES + Hc3(1,0,0) * (8*(-3 + xpow(-1) + xpow(1)))
  RexchA2ES = RexchA2ES + Hc3(1,0,1) * (4*omx(2)*xpow(-1))
  RexchA2ES = RexchA2ES + Hc3(1,1,0) * (4*omx(2)*xpow(-1))
  RexchA2ES = RexchA2ES + Hc3(1,1,1) * (-8*omx(2)*xpow(-1))
  RexchA2ES = RexchA2ES + zeta(2) * (3*omx(1)*(-13 + 9*xpow(-1)))
  RexchA2ES = RexchA2ES + zeta(3) * (4*(-11 + 6*xpow(-1) + 6*xpow(1)))

  RexchA2ES = 32 * pi**0 * RexchA2ES

  END FUNCTION RexchA2ES


  FUNCTION RexchA2EF()
  implicit none
  complex(kind=prec) :: RexchA2EF

  RexchA2EF = (-4183*omx(2)*xpow(-1))/16.
  RexchA2EF = RexchA2EF + Hc1(0) * ((omx(1)*(-869 + 1237*xpow(-1)))/8.)
  RexchA2EF = RexchA2EF + Hc1(0)*zeta(2) * (-(omx(-1)*(-78 + 26*xpow(-1) + 99*xpow(1) - 41*xpow(2))))
  RexchA2EF = RexchA2EF + Hc1(0)*zeta(3) * (-2*(-40 + 23*xpow(-1) + 20*xpow(1)))
  RexchA2EF = RexchA2EF + Hc1(1) * (60*omx(2)*xpow(-1))
  RexchA2EF = RexchA2EF + Hc1(1)*zeta(2) * (-2*omx(1)*(-35 + 32*xpow(-1)))
  RexchA2EF = RexchA2EF + Hc1(1)*zeta(3) * (-2*(-44 + 23*xpow(-1) + 23*xpow(1)))
  RexchA2EF = RexchA2EF + Hc2(0,0) * ((322 - 189*xpow(-1) - 101*xpow(1) - 4*xpow(2))/2.)
  RexchA2EF = RexchA2EF + Hc2(0,0)*zeta(2) * (omx(-2)*(-98 + 34*xpow(-1) + 121*xpow(1) - 96*xpow(2) + 33*xpow(3)))
  RexchA2EF = RexchA2EF + Hc2(0,1) * (-2*(-1 + xpow(-1))*(17 - 10*xpow(1) - xpow(2)))
  RexchA2EF = RexchA2EF + Hc2(0,1)*zeta(2) * (2*(-32 + 15*xpow(-1) + 20*xpow(1)))
  RexchA2EF = RexchA2EF + Hc2(1,0) * (-2*(-1 + xpow(-1))*(17 - 10*xpow(1) - xpow(2)))
  RexchA2EF = RexchA2EF + Hc2(1,0)*zeta(2) * (2*(-62 + 37*xpow(-1) + 40*xpow(1)))
  RexchA2EF = RexchA2EF + Hc2(1,1) * (-2*omx(2)*(1 + xpow(-2)))
  RexchA2EF = RexchA2EF + Hc2(1,1)*zeta(2) * (46*omx(2)*xpow(-1))
  RexchA2EF = RexchA2EF + Hc3(0,0,0) * ((omx(-1)*(-316 + 111*xpow(-1) + 264*xpow(1) - 65*xpow(2)))/2.)
  RexchA2EF = RexchA2EF + Hc3(0,0,1) * (-34 + 21*xpow(-1) + 15*xpow(1))
  RexchA2EF = RexchA2EF + Hc3(0,1,0) * (-34 + 21*xpow(-1) + 15*xpow(1))
  RexchA2EF = RexchA2EF + Hc3(0,1,1) * (-(omx(1)*(-5 + 7*xpow(-1))))
  RexchA2EF = RexchA2EF + Hc3(1,0,0) * (-14 + 7*xpow(-1) + xpow(1))
  RexchA2EF = RexchA2EF + Hc3(1,0,1) * (-(omx(1)*(-5 + 7*xpow(-1))))
  RexchA2EF = RexchA2EF + Hc3(1,1,0) * (-(omx(1)*(-5 + 7*xpow(-1))))
  RexchA2EF = RexchA2EF + Hc3(1,1,1) * (-12*omx(2)*xpow(-1))
  RexchA2EF = RexchA2EF + Hc4(0,0,0,0) * (-(omx(-2)*(-182 + 31*xpow(-1) + 300*xpow(1) - 172*xpow(2) + 26*xpow(3))))
  RexchA2EF = RexchA2EF + Hc4(0,0,0,1) * (70 - 29*xpow(-1) - 28*xpow(1))
  RexchA2EF = RexchA2EF + Hc4(0,0,1,0) * (70 - 29*xpow(-1) - 28*xpow(1))
  RexchA2EF = RexchA2EF + Hc4(0,0,1,1) * (12 - 7*xpow(-1) - 7*xpow(1))
  RexchA2EF = RexchA2EF + Hc4(0,1,0,0) * (34 - 10*xpow(-1) - 5*xpow(1))
  RexchA2EF = RexchA2EF + Hc4(0,1,0,1) * (-4 + xpow(-1) + xpow(1))
  RexchA2EF = RexchA2EF + Hc4(0,1,1,0) * (-4 + xpow(-1) + xpow(1))
  RexchA2EF = RexchA2EF + Hc4(0,1,1,1) * (22*omx(2)*xpow(-1))
  RexchA2EF = RexchA2EF + Hc4(1,0,0,0) * (-18 + 25*xpow(-1) + 28*xpow(1))
  RexchA2EF = RexchA2EF + Hc4(1,0,0,1) * (3*(-8 + 5*xpow(-1) + 5*xpow(1)))
  RexchA2EF = RexchA2EF + Hc4(1,0,1,0) * (3*(-8 + 5*xpow(-1) + 5*xpow(1)))
  RexchA2EF = RexchA2EF + Hc4(1,0,1,1) * (22*omx(2)*xpow(-1))
  RexchA2EF = RexchA2EF + Hc4(1,1,0,0) * (-48 + 29*xpow(-1) + 29*xpow(1))
  RexchA2EF = RexchA2EF + Hc4(1,1,0,1) * (22*omx(2)*xpow(-1))
  RexchA2EF = RexchA2EF + Hc4(1,1,1,0) * (22*omx(2)*xpow(-1))
  RexchA2EF = RexchA2EF + Hc4(1,1,1,1) * (28*omx(2)*xpow(-1))
  RexchA2EF = RexchA2EF + zeta(2) * (3*(-71 + 25*xpow(-1) + 47*xpow(1) - 2*xpow(2)))
  RexchA2EF = RexchA2EF + zeta(3) * (2*(-66 + 35*xpow(-1) + 35*xpow(1)))
  RexchA2EF = RexchA2EF + zeta(4) * (37 + 23*xpow(-1) + 3*xpow(1))

  RexchA2EF = 32 * pi**0 * RexchA2EF

  END FUNCTION RexchA2EF


  FUNCTION IeemmA1ES()
  implicit none
  complex(kind=prec) :: IeemmA1ES

  IeemmA1ES = -2*born

  IeemmA1ES = 32 * pi**2 * IeemmA1ES

  END FUNCTION IeemmA1ES


  FUNCTION IeemmA1EF()
  implicit none
  complex(kind=prec) :: IeemmA1EF

  IeemmA1EF = -2*(1 - 2*xpow(1) + 3*xpow(2))
  IeemmA1EF = IeemmA1EF + Hc1(0) * (om2xp4x2)
  IeemmA1EF = IeemmA1EF + Hc1(1) * (tm6xm4x2)

  IeemmA1EF = 32 * pi**2 * IeemmA1EF

  END FUNCTION IeemmA1EF


  FUNCTION IeemmA1E1()
  implicit none
  complex(kind=prec) :: IeemmA1E1

  IeemmA1E1 = -5 + 10*xpow(1) - 14*xpow(2)
  IeemmA1E1 = IeemmA1E1 + Hc1(0) * (omx(-1)*xpow(1))
  IeemmA1E1 = IeemmA1E1 + Hc1(1) * (-1 + xpow(-1))
  IeemmA1E1 = IeemmA1E1 + Hc2(0,0) * (-om2xp4x2)
  IeemmA1E1 = IeemmA1E1 + Hc2(0,1) * (tm6xm4x2)
  IeemmA1E1 = IeemmA1E1 + Hc2(1,0) * (-om2xp4x2)
  IeemmA1E1 = IeemmA1E1 + Hc2(1,1) * (tm6xm4x2)
  IeemmA1E1 = IeemmA1E1 + zeta(2) * (om2x)

  IeemmA1E1 = 32 * pi**2 * IeemmA1E1

  END FUNCTION IeemmA1E1


  FUNCTION IeemmA1E2()
  implicit none
  complex(kind=prec) :: IeemmA1E2

  IeemmA1E2 = -11 + 22*xpow(1) - 30*xpow(2)
  IeemmA1E2 = IeemmA1E2 + Hc1(0) * (2*omx(-1)*xpow(1))
  IeemmA1E2 = IeemmA1E2 + Hc1(1) * (2*(-1 + xpow(-1)))
  IeemmA1E2 = IeemmA1E2 + Hc1(1)*zeta(2) * (-2*om2x)
  IeemmA1E2 = IeemmA1E2 + Hc2(0,0) * (-(omx(-1)*xpow(1)))
  IeemmA1E2 = IeemmA1E2 + Hc2(0,1) * (-1 + xpow(-1))
  IeemmA1E2 = IeemmA1E2 + Hc2(1,0) * (-(omx(-1)*xpow(1)))
  IeemmA1E2 = IeemmA1E2 + Hc2(1,1) * (-1 + xpow(-1))
  IeemmA1E2 = IeemmA1E2 + Hc3(0,0,0) * (om2xp4x2)
  IeemmA1E2 = IeemmA1E2 + Hc3(0,0,1) * (tm6xm4x2)
  IeemmA1E2 = IeemmA1E2 + Hc3(0,1,0) * (om2xp4x2)
  IeemmA1E2 = IeemmA1E2 + Hc3(0,1,1) * (tm6xm4x2)
  IeemmA1E2 = IeemmA1E2 + Hc3(1,0,0) * (om2xp4x2)
  IeemmA1E2 = IeemmA1E2 + Hc3(1,0,1) * (tm6xm4x2)
  IeemmA1E2 = IeemmA1E2 + Hc3(1,1,0) * (om2xp4x2)
  IeemmA1E2 = IeemmA1E2 + Hc3(1,1,1) * (tm6xm4x2)
  IeemmA1E2 = IeemmA1E2 + zeta(2) * (born*omx(-1)*(2 - 3*xpow(1)))
  IeemmA1E2 = IeemmA1E2 + zeta(3) * (4*born)

  IeemmA1E2 = 32 * pi**2 * IeemmA1E2

  END FUNCTION IeemmA1E2


  FUNCTION IeemmA2ET()
  implicit none
  complex(kind=prec) :: IeemmA2ET

  IeemmA2ET = 2*born

  IeemmA2ET = 32 * pi**1 * IeemmA2ET

  END FUNCTION IeemmA2ET


  FUNCTION IeemmA2ED()
  implicit none
  complex(kind=prec) :: IeemmA2ED

  IeemmA2ED = 5 - 14*xpow(1) + 12*xpow(2)
  IeemmA2ED = IeemmA2ED + Hc1(0) * (-tm6xm4x2)
  IeemmA2ED = IeemmA2ED + Hc1(1) * (-2*(7 - 14*xpow(1) + 12*xpow(2)))

  IeemmA2ED = 32 * pi**1 * IeemmA2ED

  END FUNCTION IeemmA2ED


  FUNCTION IeemmA2ES()
  implicit none
  complex(kind=prec) :: IeemmA2ES

  IeemmA2ES = (31 - 86*xpow(1) + 82*xpow(2))/2.
  IeemmA2ES = IeemmA2ES + Hc1(0) * ((-9 + 26*xpow(1) - 24*xpow(2))/2.)
  IeemmA2ES = IeemmA2ES + Hc1(1) * ((-29 - 4*xpow(-1) + 14*xpow(1) - 24*xpow(2))/2.)
  IeemmA2ES = IeemmA2ES + Hc2(0,0) * (tm6xm4x2)
  IeemmA2ES = IeemmA2ES + Hc2(0,1) * (8*(1 - 2*xpow(1) + 3*xpow(2)))
  IeemmA2ES = IeemmA2ES + Hc2(1,0) * (om2xp4x2)
  IeemmA2ES = IeemmA2ES + Hc2(1,1) * (-2*(3 - 6*xpow(1) + 8*xpow(2)))
  IeemmA2ES = IeemmA2ES + oopisq*Hc3(1,1,0) * (3*(9 - 46*xpow(1) + 48*xpow(2)))
  IeemmA2ES = IeemmA2ES + zeta(2) * (-9 + 18*xpow(1) - 20*xpow(2))

  IeemmA2ES = 32 * pi**1 * IeemmA2ES

  END FUNCTION IeemmA2ES


  FUNCTION IeemmA2EF()
  implicit none
  complex(kind=prec) :: IeemmA2EF

  IeemmA2EF = (329 - 986*xpow(1) + 890*xpow(2))/8.
  IeemmA2EF = IeemmA2EF + Hc1(0) * (-(omx(-1)*(31 - 95*xpow(1) + 124*xpow(2) - 64*xpow(3)))/2.)
  IeemmA2EF = IeemmA2EF + Hc1(0)*zeta(2) * (2*(5 - 10*xpow(1) + 14*xpow(2)))
  IeemmA2EF = IeemmA2EF + Hc1(1) * ((-79 - 18*xpow(-1) + 120*xpow(1) - 170*xpow(2))/2.)
  IeemmA2EF = IeemmA2EF + Hc1(1)*zeta(2) * (-5 - 2*xpow(-2) + 8*xpow(-1) - 6*xpow(1) + 28*xpow(2))
  IeemmA2EF = IeemmA2EF + Hc2(0,0) * ((5 - 14*xpow(1))/2.)
  IeemmA2EF = IeemmA2EF + Hc2(0,1) * ((omx(-1)*(25 - 19*xpow(1) - 2*xpow(2)))/2.)
  IeemmA2EF = IeemmA2EF + Hc2(1,0) * ((-5*om2x)/2.)
  IeemmA2EF = IeemmA2EF + Hc2(1,1) * ((-29 + 4*xpow(-1) - 22*xpow(1))/2.)
  IeemmA2EF = IeemmA2EF + Hc3(0,0,0) * (-4*omx(2))
  IeemmA2EF = IeemmA2EF + Hc3(0,0,1) * (-2*(3 - 6*xpow(1) + 8*xpow(2)))
  IeemmA2EF = IeemmA2EF + Hc3(0,1,0) * ((1 - 4*xpow(1))*(1 + 2*xpow(1)))
  IeemmA2EF = IeemmA2EF + Hc3(0,1,1) * (4*(8 - 16*xpow(1) + 17*xpow(2)))
  IeemmA2EF = IeemmA2EF + Hc3(1,0,0) * (-3 + 6*xpow(1) - 8*xpow(2))
  IeemmA2EF = IeemmA2EF + Hc3(1,0,1) * (-7 + 14*xpow(1) - 16*xpow(2))
  IeemmA2EF = IeemmA2EF + Hc3(1,1,0) * (-5 + 10*xpow(1) - 12*xpow(2))
  IeemmA2EF = IeemmA2EF + Hc3(1,1,1) * ((-6 - xpow(-2) + 4*xpow(-1) + 4*xpow(1) - 24*xpow(2))/2.)
  IeemmA2EF = IeemmA2EF + oopisq*Hc4(0,1,1,0) * (15*(3 - 4*xpow(1)))
  IeemmA2EF = IeemmA2EF + oopisq*Hc4(1,1,0,0) * (3*(11 + xpow(-1) - 92*xpow(1) + 96*xpow(2)))
  IeemmA2EF = IeemmA2EF + oopisq*Hc4(1,1,0,1) * (6*(9 - 46*xpow(1) + 48*xpow(2)))
  IeemmA2EF = IeemmA2EF + oopisq*Hc4(1,1,1,0) * (-3*(-3 + xpow(-1))*(1 - 40*xpow(1) + 64*xpow(2)))
  IeemmA2EF = IeemmA2EF + zeta(2) * ((-13 + 64*xpow(1) - 84*xpow(2))/2.)
  IeemmA2EF = IeemmA2EF + zeta(3) * (-4*(4 - 8*xpow(1) + 9*xpow(2)))

  IeemmA2EF = 32 * pi**1 * IeemmA2EF

  END FUNCTION IeemmA2EF


  FUNCTION IexchA1ES()
  implicit none
  complex(kind=prec) :: IexchA1ES

  IexchA1ES = -4*(2*xpow(-2) - xpow(-1))*(1 - xpow(1) + xpow(2))

  IexchA1ES = 32 * pi**2 * IexchA1ES

  END FUNCTION IexchA1ES


  FUNCTION IexchA1EF()
  implicit none
  complex(kind=prec) :: IexchA1EF

  IexchA1EF = 4*omx(2)*xpow(-1)
  IexchA1EF = IexchA1EF + Hc1(0) * (8 + 8*xpow(-2) - 11*xpow(-1) - 3*xpow(1))
  IexchA1EF = IexchA1EF + Hc1(1) * (-2*omx(2)*xpow(-1))

  IexchA1EF = 32 * pi**2 * IexchA1EF

  END FUNCTION IexchA1EF


  FUNCTION IexchA1E1()
  implicit none
  complex(kind=prec) :: IexchA1E1

  IexchA1E1 = 9*omx(2)*xpow(-1)
  IexchA1E1 = IexchA1E1 + Hc1(0) * (-3*xpow(-2)*(2 - xpow(1))**2)
  IexchA1E1 = IexchA1E1 + Hc2(0,0) * (-((xpow(-2) - xpow(-1))*(8 - 7*xpow(1) + 3*xpow(2))))
  IexchA1E1 = IexchA1E1 + Hc2(0,1) * (-2*omx(2)*xpow(-1))
  IexchA1E1 = IexchA1E1 + Hc2(1,0) * (-2 + 8*xpow(-2) - xpow(-1) + 3*xpow(1))
  IexchA1E1 = IexchA1E1 + Hc2(1,1) * (-2*omx(2)*xpow(-1))
  IexchA1E1 = IexchA1E1 + zeta(2) * (-((-1 + xpow(-1))*(5 - xpow(1))))

  IexchA1E1 = 32 * pi**2 * IexchA1E1

  END FUNCTION IexchA1E1


  FUNCTION IexchA1E2()
  implicit none
  complex(kind=prec) :: IexchA1E2

  IexchA1E2 = 19*omx(2)*xpow(-1)
  IexchA1E2 = IexchA1E2 + Hc1(0) * (-4*(2 + 7*xpow(-2) - 7*xpow(-1)))
  IexchA1E2 = IexchA1E2 + Hc1(0)*zeta(2) * (-4*(5 + 4*xpow(-2) - 10*xpow(-1)))
  IexchA1E2 = IexchA1E2 + Hc1(1)*zeta(2) * (2 + 8*xpow(-2) - 5*xpow(-1) - xpow(1))
  IexchA1E2 = IexchA1E2 + Hc2(0,0) * (9 + 24*xpow(-2) - 20*xpow(-1) - 2*xpow(1))
  IexchA1E2 = IexchA1E2 + Hc2(1,0) * (1 + 4*xpow(-1) - 2*xpow(1))
  IexchA1E2 = IexchA1E2 + Hc3(0,0,0) * (8 + 8*xpow(-2) - 11*xpow(-1) - 3*xpow(1))
  IexchA1E2 = IexchA1E2 + Hc3(0,0,1) * (-2*omx(2)*xpow(-1))
  IexchA1E2 = IexchA1E2 + Hc3(0,1,0) * (-2 - 8*xpow(-2) + 9*xpow(-1) - 3*xpow(1))
  IexchA1E2 = IexchA1E2 + Hc3(0,1,1) * (-2*omx(2)*xpow(-1))
  IexchA1E2 = IexchA1E2 + Hc3(1,0,0) * (-10 - 24*xpow(-2) + 25*xpow(-1) - 3*xpow(1))
  IexchA1E2 = IexchA1E2 + Hc3(1,0,1) * (-2*omx(2)*xpow(-1))
  IexchA1E2 = IexchA1E2 + Hc3(1,1,0) * (-((xpow(-2) - xpow(-1))*(8 - 9*xpow(1) - 3*xpow(2))))
  IexchA1E2 = IexchA1E2 + Hc3(1,1,1) * (-2*omx(2)*xpow(-1))
  IexchA1E2 = IexchA1E2 + zeta(2) * (-(omx(-1)*(-13 + 4*xpow(-1) + 11*xpow(1) - 4*xpow(2))))
  IexchA1E2 = IexchA1E2 + zeta(3) * (8*(2*xpow(-2) - xpow(-1))*(1 - xpow(1) + xpow(2)))

  IexchA1E2 = 32 * pi**2 * IexchA1E2

  END FUNCTION IexchA1E2


  FUNCTION IexchA2ET()
  implicit none
  complex(kind=prec) :: IexchA2ET

  IexchA2ET = -4*omx(2)*xpow(-1)

  IexchA2ET = 32 * pi**1 * IexchA2ET

  END FUNCTION IexchA2ET


  FUNCTION IexchA2ED()
  implicit none
  complex(kind=prec) :: IexchA2ED

  IexchA2ED = -2*(-11 + 2*xpow(-1) + 3*xpow(1))
  IexchA2ED = IexchA2ED + Hc1(0) * (2*(1 + 6*xpow(-2) - 6*xpow(-1) + 2*xpow(1) - 2*xpow(2)))
  IexchA2ED = IexchA2ED + Hc1(1) * (6*omx(2)*xpow(-1))

  IexchA2ED = 32 * pi**1 * IexchA2ED

  END FUNCTION IexchA2ED


  FUNCTION IexchA2ES()
  implicit none
  complex(kind=prec) :: IexchA2ES

  IexchA2ES = 58 - 12*xpow(-2) - 3*xpow(-1) - 21*xpow(1)
  IexchA2ES = IexchA2ES + Hc1(0) * ((45 + 144*xpow(-2) - 110*xpow(-1) + 4*xpow(1))/2.)
  IexchA2ES = IexchA2ES + Hc1(1) * (11 + xpow(-1) + xpow(1))
  IexchA2ES = IexchA2ES + Hc2(0,0) * (27 + 72*xpow(-2) - 50*xpow(-1) - 8*xpow(1) + 12*xpow(2))
  IexchA2ES = IexchA2ES + Hc2(0,1) * (8 - 5*xpow(-1) - 5*xpow(1))
  IexchA2ES = IexchA2ES + Hc2(1,0) * (2*(33 + 22*xpow(-2) - 35*xpow(-1) - 27*xpow(1) + 22*xpow(2)))
  IexchA2ES = IexchA2ES + Hc2(1,1) * (-2*omx(2)*xpow(-1))
  IexchA2ES = IexchA2ES + oopisq*Hc3(0,0,0) * (-3*(9 + 48*xpow(-2) - 46*xpow(-1)))
  IexchA2ES = IexchA2ES + oopisq*Hc3(0,1,0) * (-3*(9 + 48*xpow(-2) - 46*xpow(-1)))
  IexchA2ES = IexchA2ES + oopisq*Hc3(1,0,0) * (-3*(9 + 48*xpow(-2) - 46*xpow(-1)))
  IexchA2ES = IexchA2ES + oopisq*Hc3(1,1,0) * (-6*(9 + 24*xpow(-2) - 23*xpow(-1) - 23*xpow(1) + 24*xpow(2)))
  IexchA2ES = IexchA2ES + zeta(2) * (-16 + 7*xpow(-1) + 7*xpow(1))

  IexchA2ES = 32 * pi**1 * IexchA2ES

  END FUNCTION IexchA2ES


  FUNCTION IexchA2EF()
  implicit none
  complex(kind=prec) :: IexchA2EF

  IexchA2EF = (716 - 96*xpow(-2) - 85*xpow(-1) - 223*xpow(1))/4.
  IexchA2EF = IexchA2EF + Hc1(1) * ((-7 - 20*xpow(-2) + 52*xpow(-1) + 82*xpow(1) - 40*xpow(2))/2.)
  IexchA2EF = IexchA2EF + Hc1(1)*zeta(2) * (-24*omx(2)*xpow(-1))
  IexchA2EF = IexchA2EF + Hc2(0,0) * (-(omx(-1)*(896 + 768*xpow(-2) - 1441*xpow(-1) - 229*xpow(1) + 30*xpow(2)))/2.)
  IexchA2EF = IexchA2EF + Hc2(0,1) * (2*omx(-1)*(-7 + 4*xpow(-1) + 7*xpow(1) - 6*xpow(2)))
  IexchA2EF = IexchA2EF + Hc2(1,0) * ((-129 - 480*xpow(-2) + 488*xpow(-1) + 69*xpow(1))/2.)
  IexchA2EF = IexchA2EF + Hc2(1,1) * (25 + 2*xpow(-1) + 10*xpow(1))
  IexchA2EF = IexchA2EF + Hc3(0,0,0) * ((-247 - 672*xpow(-2) + 476*xpow(-1) + 44*xpow(1) - 48*xpow(2))/2.)
  IexchA2EF = IexchA2EF + Hc3(0,0,1) * (-8 + 7*xpow(-1) + 5*xpow(1))
  IexchA2EF = IexchA2EF + Hc3(0,1,0) * (21 + 156*xpow(-2) - 115*xpow(-1) + 54*xpow(1) - 48*xpow(2))
  IexchA2EF = IexchA2EF + Hc3(0,1,1) * (20 - 11*xpow(-1) - 11*xpow(1))
  IexchA2EF = IexchA2EF + Hc3(1,0,0) * (-115 - 96*xpow(-2) + 120*xpow(-1) + 99*xpow(1) - 84*xpow(2))
  IexchA2EF = IexchA2EF + Hc3(1,0,1) * (-2*(-4 + xpow(-1) + xpow(1)))
  IexchA2EF = IexchA2EF + Hc3(1,1,0) * (134 + 228*xpow(-2) - 251*xpow(-1) - 27*xpow(1) + 36*xpow(2))
  IexchA2EF = IexchA2EF + Hc3(1,1,1) * (-14*omx(2)*xpow(-1))
  IexchA2EF = IexchA2EF + oopisq*Hc1(0)*zeta(2) * (3*(264 + 690*xpow(-2) - 597*xpow(-1) + xpow(1) + 10*xpow(2)))
  IexchA2EF = IexchA2EF + oopisq*Hc1(0)*zeta(3) * (-3*(80 + 384*xpow(-2) - 388*xpow(-1) + xpow(1)))
  IexchA2EF = IexchA2EF + oopisq*Hc1(0)*zeta(4) * (15*(211 + 776*xpow(-2) - 374*xpow(-1) - 15*xpow(1) + 20*xpow(2)))
  IexchA2EF = IexchA2EF + oopisq*Hc4(0,0,0,0) * (3*(109 + 576*xpow(-2) - 572*xpow(-1) + 2*xpow(1)))
  IexchA2EF = IexchA2EF + oopisq*Hc4(0,0,1,0) * (-3*(-1 + 2*xpow(-1))*(10 + xpow(1)))
  IexchA2EF = IexchA2EF + oopisq*Hc4(0,1,0,0) * (3*(91 + 480*xpow(-2) - 480*xpow(-1) + 2*xpow(1)))
  IexchA2EF = IexchA2EF + oopisq*Hc4(0,1,1,0) * (-3*(25 + 96*xpow(-2) - 72*xpow(-1) - 21*xpow(1)))
  IexchA2EF = IexchA2EF + oopisq*Hc4(1,0,0,0) * (6*(38 + 240*xpow(-2) - 230*xpow(-1) + xpow(1)))
  IexchA2EF = IexchA2EF + oopisq*Hc4(1,0,1,0) * (-3*(25 + 96*xpow(-2) - 92*xpow(-1) - xpow(1)))
  IexchA2EF = IexchA2EF + oopisq*Hc4(1,1,0,0) * (3*(47 + 384*xpow(-2) - 369*xpow(-1) + 94*xpow(1) - 96*xpow(2)))
  IexchA2EF = IexchA2EF + oopisq*Hc4(1,1,1,0) * (-3*(86 + 192*xpow(-2) - 185*xpow(-1) - 185*xpow(1) + 192*xpow(2)))
  IexchA2EF = IexchA2EF + zeta(2) * (-(omx(-1)*(65 + 11*xpow(-1) - 117*xpow(1) + 33*xpow(2)))/2.)
  IexchA2EF = IexchA2EF + zeta(3) * (-56 + 29*xpow(-1) + 29*xpow(1))

  IexchA2EF = 32 * pi**1 * IexchA2EF

  END FUNCTION IexchA2EF
                          !!!!!!!!!!!!!!!!!!!!!!
                           END MODULE ee_twoloop
                          !!!!!!!!!!!!!!!!!!!!!!
