
                          !!!!!!!!!!!!!!!!!!!!!!
                            MODULE ee_EE2EE_A
                          !!!!!!!!!!!!!!!!!!!!!!
    use functions
    use collier
    use ee_ee2eel
    use ee_ee2eeg
    implicit none


  contains


  FUNCTION EE2EE_ALTR(p1,p2,p3,p4,pole)
   real(kind=prec) :: p1(4),p2(4),p3(4),p4(4)
   real(kind=prec), optional :: pole
   real(kind=prec) :: ee2ee_altr
   real(kind=prec) :: t, u
   complex(kind=prec) :: pole_tt, pole_tu, pole_uu, pole_ut

   t = sq(p1-p3); u = sq(p1-p4)

   if(present(pole)) then
       ee2ee_altr = 4*real(deltalph_0_tot_cplx(t)*ee2eel_tr_tt_cplx(p1,p2,p3,p4,pole_tt))&
                  + 4*real(deltalph_0_tot_cplx(u)*ee2eel_tr_tt_cplx(p1,p2,p4,p3,pole_uu))&
                  + 2*real(deltalph_0_tot_cplx(t)*ee2eel_tr_tu_cplx(p1,p2,p3,p4,pole_tu))&
                  + 2*real(deltalph_0_tot_cplx(u)*ee2eel_tr_tu_cplx(p1,p2,p4,p3,pole_ut))&
                  + 2*real(deltalph_0_tot_cplx(u)*ee2eel_tr_tu_cplx(p1,p2,p3,p4))&
                  + 2*real(deltalph_0_tot_cplx(t)*ee2eel_tr_tu_cplx(p1,p2,p4,p3))

       pole = 4*real(deltalph_0_tot_cplx(t)*pole_tt)&
            + 4*real(deltalph_0_tot_cplx(u)*pole_uu)&
            + 2*real(deltalph_0_tot_cplx(t)*pole_tu)&
            + 2*real(deltalph_0_tot_cplx(u)*pole_ut)&
            + 2*real(deltalph_0_tot_cplx(u)*pole_tu)&
            + 2*real(deltalph_0_tot_cplx(t)*pole_ut)
   else
       ee2ee_altr = 4*real(deltalph_0_tot_cplx(t)*ee2eel_tr_tt_cplx(p1,p2,p3,p4))&
                  + 4*real(deltalph_0_tot_cplx(u)*ee2eel_tr_tt_cplx(p1,p2,p4,p3))&
                  + 2*real(deltalph_0_tot_cplx(t)*ee2eel_tr_tu_cplx(p1,p2,p3,p4))&
                  + 2*real(deltalph_0_tot_cplx(u)*ee2eel_tr_tu_cplx(p1,p2,p4,p3))&
                  + 2*real(deltalph_0_tot_cplx(u)*ee2eel_tr_tu_cplx(p1,p2,p3,p4))&
                  + 2*real(deltalph_0_tot_cplx(t)*ee2eel_tr_tu_cplx(p1,p2,p4,p3))
   end if
  END FUNCTION


  FUNCTION EE2EEG_ATR(p1,p2,p3,p4,p5)
   real(kind=prec) :: p1(4),p2(4),p3(4),p4(4),p5(4)
   real(kind=prec) :: ee2eeg_atr

   ee2eeg_atr = 2*deltalph_0_tot(sq(p2-p4))*ee2eeg_tr_tchannel_up(p1,p2,p3,p4,p5) &
              + 2*deltalph_0_tot(sq(p1-p3))*ee2eeg_tr_tchannel_up(p2,p1,p4,p3,p5) &
              + 2*deltalph_0_tot(sq(p2-p3))*ee2eeg_tr_tchannel_up(p1,p2,p4,p3,p5) &
              + 2*deltalph_0_tot(sq(p1-p4))*ee2eeg_tr_tchannel_up(p2,p1,p3,p4,p5)

  END FUNCTION


  FUNCTION EE2EE_ALBX(p1,p2,p3,p4,pole)
   real(kind=prec) :: p1(4),p2(4),p3(4),p4(4)
   real(kind=prec), optional :: pole
   real(kind=prec) :: ee2ee_albx
   real(kind=prec) :: t, u
   complex(kind=prec) :: pole_tt, pole_tu, pole_uu, pole_ut, pole_nft, pole_nfu

   t = sq(p1-p3); u = sq(p1-p4)

   if(present(pole)) then
        ee2ee_albx = 2*real(deltalph_0_tot_cplx(t)*ee2eel_bx_tt_cplx(p1,p2,p3,p4,pole_tt))&
                   + 2*real(deltalph_0_tot_cplx(u)*ee2eel_bx_tt_cplx(p1,p2,p4,p3,pole_uu))&
                   + 2*real(deltalph_0_tot_cplx(u)*ee2eel_bx_tu_cplx(p1,p2,p3,p4,pole_tu))&
                   + 2*real(deltalph_0_tot_cplx(t)*ee2eel_bx_tu_cplx(p1,p2,p4,p3,pole_ut))&
                   + real(deltalph_0_tot_cplx(t)*ee2eel_nfbx_sin_t_cplx(p1,p2,p3,p4,pole_nft))&
                   + real(deltalph_0_tot_cplx(u)*ee2eel_nfbx_sin_t_cplx(p1,p2,p4,p3,pole_nfu))

        pole = 2*real(deltalph_0_tot_cplx(t)*pole_tt)&
             + 2*real(deltalph_0_tot_cplx(u)*pole_uu)&
             + 2*real(deltalph_0_tot_cplx(u)*pole_tu)&
             + 2*real(deltalph_0_tot_cplx(t)*pole_ut)&
             + real(deltalph_0_tot_cplx(t)*pole_nft)&
             + real(deltalph_0_tot_cplx(u)*pole_nfu)
    else
        ee2ee_albx = 2*real(deltalph_0_tot_cplx(t)*ee2eel_bx_tt_cplx(p1,p2,p3,p4))&
                   + 2*real(deltalph_0_tot_cplx(u)*ee2eel_bx_tt_cplx(p1,p2,p4,p3))&
                   + 2*real(deltalph_0_tot_cplx(u)*ee2eel_bx_tu_cplx(p1,p2,p3,p4))&
                   + 2*real(deltalph_0_tot_cplx(t)*ee2eel_bx_tu_cplx(p1,p2,p4,p3))&
                   + real(deltalph_0_tot_cplx(t)*ee2eel_nfbx_sin_t_cplx(p1,p2,p3,p4))&
                   + real(deltalph_0_tot_cplx(u)*ee2eel_nfbx_sin_t_cplx(p1,p2,p4,p3))
   end if
  END FUNCTION


  FUNCTION EE2EEG_ABX(p1,p2,p3,p4,p5)
   real(kind=prec) :: p1(4),p2(4),p3(4),p4(4),p5(4)
   real(kind=prec) :: ee2eeg_abx

   ee2eeg_abx = 2*deltalph_0_tot(sq(p2-p4))*ee2eeg_bx_tchannel_up(p1,p2,p3,p4,p5) &
              + 2*deltalph_0_tot(sq(p1-p3))*ee2eeg_bx_tchannel_up(p2,p1,p4,p3,p5) &
              + 2*deltalph_0_tot(sq(p2-p3))*ee2eeg_bx_tchannel_up(p1,p2,p4,p3,p5) &
              + 2*deltalph_0_tot(sq(p1-p4))*ee2eeg_bx_tchannel_up(p2,p1,p3,p4,p5)

  END FUNCTION



  FUNCTION EE2EEL_NFBX_SIN_T_CPLX(p1,p2,p3,p4,pole)
    complex(kind=prec) :: ee2eel_nfbx_sin_t_cplx
    real(kind=prec),intent(in) :: p1(4),p2(4),p3(4),p4(4)
    complex(kind=prec), optional :: pole
    real(kind=prec) :: ss, tt, m2, m, lgmu
    complex(kind=prec) :: lg,discb_1,discb_2,scalarc0ir6_1,scalarc0ir6_2
    complex(kind=prec) :: pva,pvb_1,pvb_2,pvb_3,pvc_1,pvc_2,pvd_1,pvd_2

    ss = sq(p1+p2); tt = sq(p1-p3)
    m2 = sq(p1); m = sqrt(m2)
    lgmu = log(musq/m2)

    lg = log(cmplx(-(musq/tt)))
    discb_1 = discb_cplx(ss,m)
    discb_2 = discb_cplx(4*m2 - ss - tt,m)
    scalarc0ir6_1 = scalarc0ir6_cplx(ss,m)
    scalarc0ir6_2 = scalarc0ir6_cplx(4*m2 - ss - tt,m)

    pva = (1 + lgmu)*m2
    pvb_1 = 2 + discb_1 + lgmu
    pvb_2 = 2 + discb_2 + lgmu
    pvb_3 = 2 + lg
    pvc_1 = scalarc0ir6_1 - (discb_1*lgmu)/(4*m2 - ss)
    pvc_2 = scalarc0ir6_2 - (discb_2*lgmu)/(ss + tt)

    call SetMuIR2_cll(musq)

    call D0_cll(pvd_1,cmplx(m2),cmplx(m2),cmplx(m2),cmplx(&
        m2),cmplx(4*m2 - ss - tt),cmplx(tt),cmplx(m2),cmplx(0),cmplx(m2),cmplx(-tt))
    call D0_cll(pvd_2,cmplx(m2),cmplx(ss),cmplx(m2),cmplx(&
        tt),cmplx(m2),cmplx(m2),cmplx(0),cmplx(m2),cmplx(m2),cmplx(-tt))

    ee2eel_nfbx_sin_t_cplx = ((2*pvb_2*(2*m2 - ss)*(4*m2*ss - (ss + tt)**2))/(ss + tt) + 4*pvb_3*(8*m2**2 + s&
                           &s*(ss + tt) - m2*(6*ss + tt)) + (2*pvb_1*(32*m2**3 + 16*m2**2*(-2*ss + tt) - ss*&
                           &*2*(ss + tt) + 2*m2*(5*ss**2 - 2*tt**2)))/(4*m2 - ss) + pvc_2*(64*m2**3 + 20*m2*&
                           &ss*(ss + tt) - 8*m2**2*(8*ss + 3*tt) - ss*(2*ss**2 + 3*ss*tt + tt**2)) + pvc_1*(&
                           &16*m2**2*(ss + tt) - 4*m2*(3*ss**2 + 2*ss*tt + tt**2) + ss*(2*ss**2 + 3*ss*tt + &
                           &tt**2)) - (8*(-(ss**2*(ss + tt)**2) + 8*m2**3*(4*ss + 3*tt) - 2*m2**2*ss*(16*ss &
                           &+ 13*tt) + m2*(10*ss**3 + 13*ss**2*tt + 3*ss*tt**2 - tt**3)))/((4*m2 - ss)*(ss +&
                           & tt)) - (8*pva*(-(ss**2*(ss + tt)**2) + 8*m2**3*(4*ss + 3*tt) - 2*m2**2*ss*(16*s&
                           &s + 13*tt) + m2*(10*ss**3 + 13*ss**2*tt + 3*ss*tt**2 - tt**3)))/(m2*(4*m2 - ss)*&
                           &(ss + tt)) + 2*pvd_2*(128*m2**4 + 16*m2**3*(-14*ss + tt) + 8*m2**2*ss*(18*ss + t&
                           &t) - 4*m2*ss*(10*ss**2 + 4*ss*tt + 3*tt**2) + ss*(4*ss**3 + 4*ss**2*tt + 3*ss*tt&
                           &**2 + tt**3)) + 2*pvd_1*(128*m2**4 + 4*ss**4 + 12*ss**3*tt + 15*ss**2*tt**2 + 9*&
                           &ss*tt**3 + 2*tt**4 - 112*m2**3*(2*ss + tt) + 8*m2**2*(18*ss**2 + 21*ss*tt + 4*tt&
                           &**2) - 4*m2*(10*ss**3 + 20*ss**2*tt + 14*ss*tt**2 + 3*tt**3)))/(2.*tt*(-4*m2 + s&
                           &s + tt))

    ee2eel_nfbx_sin_t_cplx = 64*alpha**3*pi*ee2eel_nfbx_sin_t_cplx

    if (present(pole)) then
      pole = -(((discb_2*(4*m2 - ss)*(2*m2 - ss - tt) + discb_1*(2*m2 - ss)*(ss + tt))*(32*m2&
       &**3 - 2*ss**3 - 3*ss**2*tt - 3*ss*tt**2 - tt**3 + m2**2*(-40*ss + 4*tt) + 4*m2*(&
       &4*ss**2 + 2*ss*tt + tt**2)))/((4*m2 - ss)*tt**2*(ss + tt)*(-4*m2 + ss + tt)))

      pole = 64*alpha**3*pi*pole
    end if
  END FUNCTION


                           !!!!!!!!!!!!!!!!!!!!!!
                          END MODULE ee_EE2EE_A
                          !!!!!!!!!!!!!!!!!!!!!!
