
                          !!!!!!!!!!!!!!!!!!!!!!
                            MODULE ee_twoloop2
                          !!!!!!!!!!!!!!!!!!!!!!
    use functions
    implicit none
    real(kind=prec), parameter :: zeta(2:5) = (/1.6449340668482264365, &
                                                1.2020569031595942854, &
                                                1.0823232337111381915, &
                                                1.0369277551433699263 /)
    real(kind=prec), parameter :: oopi = 1/pi

    complex(kind=prec),parameter :: imag2 = (0, pi)
    complex(kind=prec),parameter :: imag3 = (0, oopi)
    integer, parameter :: fac(1:7) = (/1, 2, 6, 24, 120, 720, 5040 /)

contains


  SUBROUTINE BHABHA(ss, tt, tree, ol, tl, prescription)
  implicit none
  real(kind=prec), intent(in) :: ss, tt
  integer, optional, intent(in) :: prescription
  complex(kind=prec), optional, intent(out) :: tree, ol(:), tl(:)
  complex(kind=prec) :: eemm(0:2,-4:2), exch(0:2,-4:2), ans(0:2,-4:2)
  complex(kind=prec) :: lo
  integer :: mask, lans1, presc

  mask = 0
  lans1 = 0
  if(present(tree)) then
    mask = ibset(mask, 0)
  endif
  if(present(ol  )) then
    mask = ibset(mask, 1)
    lans1 = size(ol) - 3
  endif
  if(present(tl  )) then
    mask = ibset(mask, 2)
  endif

  presc = 1
  if(present(prescription)) presc = prescription
  lo = -log(cmplx(ss/musq))

  eemm = 0.
  exch = 0.
  select case(mask)
    case(1) ! 001
      call twoloop(-tt/ss, lo,                           &
        eemmA0=eemm(0, 0      ), exchA0=exch(0, 0      ),&
        prescription=presc)
    case(2) ! 010
      call twoloop(-tt/ss, lo,                           &
        eemmA1=eemm(1,-2:lans1), exchA1=exch(1,-2:lans1),&
        prescription=presc)
    case(4) ! 100
      call twoloop(-tt/ss, lo,                           &
        eemmA2=eemm(2,-4:0    ), exchA2=exch(2,-4:0    ),&
        prescription=presc)

    case(3) ! 011
      call twoloop(-tt/ss, lo,                           &
        eemmA0=eemm(0, 0      ), exchA0=exch(0, 0      ),&
        eemmA1=eemm(1,-2:lans1), exchA1=exch(1,-2:lans1),&
        prescription=presc)
    case(5) ! 101
      call twoloop(-tt/ss, lo,                           &
        eemmA0=eemm(0, 0      ), exchA0=exch(0, 0      ),&
        eemmA2=eemm(2,-4:0    ), exchA2=exch(2,-4:0    ),&
        prescription=presc)
    case(6) ! 110
      call twoloop(-tt/ss, lo,                           &
        eemmA1=eemm(1,-2:lans1), exchA1=exch(1,-2:lans1),&
        eemmA2=eemm(2,-4:0    ), exchA2=exch(2,-4:0    ),&
        prescription=presc)
    case(7) ! 111
      call twoloop(-tt/ss, lo,                           &
        eemmA0=eemm(0, 0      ), exchA0=exch(0, 0      ),&
        eemmA1=eemm(1,-2:lans1), exchA1=exch(1,-2:lans1),&
        eemmA2=eemm(2,-4:0    ), exchA2=exch(2,-4:0    ),&
        prescription=presc)
  end select

  ans = eemm + exch

  if(presc.eq.+1) then
    lo = -log(cmplx(tt/musq))
  else
    lo = conjg(-log(cmplx(tt/musq)) + 4*pi*imag)
  endif


  eemm = 0.
  select case(mask)
    case(1) ! 001
      call twoloop(-ss/tt, lo,                           &
        eemmA0=eemm(0, 0      )                         ,&
        prescription=presc)
    case(2) ! 010
      call twoloop(-ss/tt, lo,                           &
        eemmA1=eemm(1,-2:lans1)                         ,&
        prescription=presc)
    case(4) ! 100
      call twoloop(-ss/tt, lo,                           &
        eemmA2=eemm(2,-4:0    )                         ,&
        prescription=presc)

    case(3) ! 011
      call twoloop(-ss/tt, lo,                           &
        eemmA0=eemm(0, 0      )                         ,&
        eemmA1=eemm(1,-2:lans1)                         ,&
        prescription=presc)
    case(5) ! 101
      call twoloop(-ss/tt, lo,                           &
        eemmA0=eemm(0, 0      )                         ,&
        eemmA2=eemm(2,-4:0    )                         ,&
        prescription=presc)
    case(6) ! 110
      call twoloop(-ss/tt, lo,                           &
        eemmA1=eemm(1,-2:lans1)                         ,&
        eemmA2=eemm(2,-4:0    )                         ,&
        prescription=presc)
    case(7) ! 111
      call twoloop(-ss/tt, lo,                           &
        eemmA0=eemm(0, 0      )                         ,&
        eemmA1=eemm(1,-2:lans1)                         ,&
        eemmA2=eemm(2,-4:0    )                         ,&
        prescription=presc)
  end select

  ans = ans + eemm

  if(present(tree)) then
    tree = ans(0,0)
  endif
  if(present(ol  )) then
    ol = ans(1, -2:lans1)
  endif
  if(present(tl  )) then
    tl = ans(2,-4:0)
  endif
  END SUBROUTINE




  SUBROUTINE GETHPLS(x, Hc1, Hc2, Hc3, Hc4, prescription)
  implicit none
  integer, parameter :: n1 = 0
  integer, parameter :: n2 = +1
  integer, parameter :: nw =  4
  integer, optional, intent(in) :: prescription
  complex(kind=prec) Hc1(0:+1)
  complex(kind=prec) Hc2(0:+1,0:+1)
  complex(kind=prec) Hc3(0:+1,0:+1,0:+1)
  complex(kind=prec) Hc4(0:+1,0:+1,0:+1,0:+1)

  real(kind=8) Hi1(0:+1)
  real(kind=8) Hi2(0:+1,0:+1)
  real(kind=8) Hi3(0:+1,0:+1,0:+1)
  real(kind=8) Hi4(0:+1,0:+1,0:+1,0:+1)

  real(kind=8) Hr1(0:+1)
  real(kind=8) Hr2(0:+1,0:+1)
  real(kind=8) Hr3(0:+1,0:+1,0:+1)
  real(kind=8) Hr4(0:+1,0:+1,0:+1,0:+1)
  real(kind=prec), intent(in) :: x
  call hplog(x, nw, Hc1, Hc2, Hc3, Hc4, &
                    Hr1, Hr2, Hr3, Hr4, &
                    Hi1, Hi2, Hi3, Hi4, n1, n2)
  if(present(prescription)) then
  if(prescription == -1)then
    Hc1 = conjg(Hc1)
    Hc2 = conjg(Hc2)
    Hc3 = conjg(Hc3)
    Hc4 = conjg(Hc4)
  endif
  endif


  END SUBROUTINE


  SUBROUTINE TWOLOOP(x, Ls, eemmA0, eemmA1, eemmA2, exchA0, exchA1, exchA2, prescription)
  implicit none
  real(kind=prec), intent(in) :: x
  complex(kind=prec), intent(in) :: Ls
  integer, intent(in), optional :: prescription
  complex(kind=prec), optional, intent(out) :: eemmA0, exchA0
  complex(kind=prec), optional, intent(out) :: eemmA1(:), eemmA2(:)
  complex(kind=prec), optional, intent(out) :: exchA1(:), exchA2(:)
  complex(kind=prec) :: TeemmA1(-2:2), TeemmA2(-4:0), TexchA1(-2:2), TexchA2(-4:0), Lbit(-4:2)

  real(kind=prec) :: born, om2x, tm6xm4x2, om2xp4x2
  real(kind=prec) :: omx(-2:2), xpow(-2:5)

  integer i, j
  complex(kind=prec) Hc1(0:+1)
  complex(kind=prec) Hc2(0:+1,0:+1)
  complex(kind=prec) Hc3(0:+1,0:+1,0:+1)
  complex(kind=prec) Hc4(0:+1,0:+1,0:+1,0:+1)

  call gethpls(x, Hc1, Hc2, Hc3, Hc4, prescription)


  xpow = x ** (/-2,-1,0,1,2,3,4,5/)
  omx = (1 - xpow(1))**(/-2,-1,0,1,2/)
  born = 1 - 2*xpow(1) + 2*xpow(2)
  tm6xm4x2 = 3 - 6*xpow(1) + 4*xpow(2)
  tm6xm4x2 = 3 - 6*xpow(1) + 4*xpow(2)
  om2xp4x2 = 1 - 2*xpow(1) + 4*xpow(2)
  om2x = 1 - 2*xpow(1)
  if(present(eemmA0)) then
    eemmA0 = born
    eemmA0 = 32*pi**2*eemmA0
  endif

  if(present(eemmA1)) then
    TeemmA1(-2) = -2*born

    TeemmA1(-1) = -(born*(3 + 2*imag2))
    TeemmA1(-1) = TeemmA1(-1) + Hc1(0) * (2*born)
    TeemmA1(-1) = TeemmA1(-1) + Hc1(1) * (2*born)

    TeemmA1(0) = -7 - 2*imag2 + 14*xpow(1) + 4*imag2*xpow(1) - 14*xpow(2) - &
                 6*imag2*xpow(2)
    TeemmA1(0) = TeemmA1(0) + Hc1(0) * (1 + imag2 - xpow(1) - 2*imag2*xpow(1) + &
                 4*imag2*xpow(2))
    TeemmA1(0) = TeemmA1(0) + Hc1(1) * (3*imag2 + xpow(1) - 6*imag2*xpow(1) + &
                 4*imag2*xpow(2))
    TeemmA1(0) = TeemmA1(0) + Hc2(0,0) * (-om2x)
    TeemmA1(0) = TeemmA1(0) + Hc2(1,1) * (-om2x)
    TeemmA1(0) = TeemmA1(0) + zeta(2) * (6*born)
    if(size(eemmA1) > 3) then
      TeemmA1(1) = -15 - 5*imag2 + 30*xpow(1) + 10*imag2*xpow(1) - 30*xpow(2) - &
                   14*imag2*xpow(2)
      TeemmA1(1) = TeemmA1(1) + Hc1(0) * (omx(-1)*(2 - 4*xpow(1) + &
                   imag2*xpow(1) + 2*xpow(2)))
      TeemmA1(1) = TeemmA1(1) + Hc1(0)*zeta(2) * (-3*om2xp4x2)
      TeemmA1(1) = TeemmA1(1) + Hc1(1) * (-imag2 + imag2*xpow(-1) + 2*xpow(1))
      TeemmA1(1) = TeemmA1(1) + Hc1(1)*zeta(2) * (-3*tm6xm4x2)
      TeemmA1(1) = TeemmA1(1) + Hc2(0,0) * (omx(-1)*(-1 - imag2 + 3*xpow(1) + &
                   3*imag2*xpow(1) - xpow(2) - 6*imag2*xpow(2) + 4*imag2*xpow(3)))
      TeemmA1(1) = TeemmA1(1) + Hc2(0,1) * (imag2*tm6xm4x2)
      TeemmA1(1) = TeemmA1(1) + Hc2(1,0) * (-(imag2*om2xp4x2))
      TeemmA1(1) = TeemmA1(1) + Hc2(1,1) * (3*imag2 - omx(1)*xpow(-1) + xpow(1) &
                   - 6*imag2*xpow(1) + 4*imag2*xpow(2))
      TeemmA1(1) = TeemmA1(1) + Hc3(0,0,0) * (-4*xpow(2))
      TeemmA1(1) = TeemmA1(1) + Hc3(0,1,1) * (-tm6xm4x2)
      TeemmA1(1) = TeemmA1(1) + Hc3(1,0,0) * (-om2x - 4*xpow(2))
      TeemmA1(1) = TeemmA1(1) + Hc3(1,1,1) * (-4*omx(2))
      TeemmA1(1) = TeemmA1(1) + zeta(2) * (6 + imag2 - 12*xpow(1) - &
                   2*imag2*xpow(1) + 18*xpow(2))
      TeemmA1(1) = TeemmA1(1) + zeta(3) * (5 - 10*xpow(1) + 12*xpow(2))

      TeemmA1(2) = -31 - 11*imag2 + 62*xpow(1) + 22*imag2*xpow(1) - 62*xpow(2) &
                   - 30*imag2*xpow(2)
      TeemmA1(2) = TeemmA1(2) + Hc1(0) * (2*omx(-1)*(2 - 4*xpow(1) + &
                   imag2*xpow(1) + 2*xpow(2)))
      TeemmA1(2) = TeemmA1(2) + Hc1(0)*zeta(2) * (-3*omx(-1)*xpow(1))
      TeemmA1(2) = TeemmA1(2) + Hc1(0)*zeta(3) * (-5 + 10*xpow(1) - 12*xpow(2))
      TeemmA1(2) = TeemmA1(2) + Hc1(1) * (-2*(imag2 - imag2*xpow(-1) - &
                   2*xpow(1)))
      TeemmA1(2) = TeemmA1(2) + Hc1(1)*zeta(2) * (3 - 2*imag2 - 3*xpow(-1) + &
                   4*imag2*xpow(1))
      TeemmA1(2) = TeemmA1(2) + Hc1(1)*zeta(3) * (-5 + 10*xpow(1) - 12*xpow(2))
      TeemmA1(2) = TeemmA1(2) + Hc2(0,0) * (-(omx(-1)*(2 - 6*xpow(1) + &
                   imag2*xpow(1) + 2*xpow(2))))
      TeemmA1(2) = TeemmA1(2) + Hc2(0,0)*zeta(2) * (3*om2xp4x2)
      TeemmA1(2) = TeemmA1(2) + Hc2(0,1) * (imag2*omx(1)*xpow(-1))
      TeemmA1(2) = TeemmA1(2) + Hc2(0,1)*zeta(2) * (-3*tm6xm4x2)
      TeemmA1(2) = TeemmA1(2) + Hc2(1,0) * (-(imag2*omx(-1)*xpow(1)))
      TeemmA1(2) = TeemmA1(2) + Hc2(1,0)*zeta(2) * (3*om2xp4x2)
      TeemmA1(2) = TeemmA1(2) + Hc2(1,1) * (2 - imag2 - 2*xpow(-1) + &
                   imag2*xpow(-1) + 2*xpow(1))
      TeemmA1(2) = TeemmA1(2) + Hc2(1,1)*zeta(2) * (-3*tm6xm4x2)
      TeemmA1(2) = TeemmA1(2) + Hc3(0,0,0) * (-(omx(-1)*(-1 - imag2 + 4*xpow(1) &
                   + 3*imag2*xpow(1) - xpow(2) - 6*imag2*xpow(2) + 4*imag2*xpow(3))))
      TeemmA1(2) = TeemmA1(2) + Hc3(0,0,1) * (imag2*tm6xm4x2)
      TeemmA1(2) = TeemmA1(2) + Hc3(0,1,0) * (imag2*om2xp4x2)
      TeemmA1(2) = TeemmA1(2) + Hc3(0,1,1) * (3*imag2 - omx(1)*xpow(-1) - &
                   6*imag2*xpow(1) + 4*imag2*xpow(2))
      TeemmA1(2) = TeemmA1(2) + Hc3(1,0,0) * (-(omx(-1)*(-imag2 + xpow(1) + &
                   3*imag2*xpow(1) - 6*imag2*xpow(2) + 4*imag2*xpow(3))))
      TeemmA1(2) = TeemmA1(2) + Hc3(1,0,1) * (imag2*tm6xm4x2)
      TeemmA1(2) = TeemmA1(2) + Hc3(1,1,0) * (imag2*om2xp4x2)
      TeemmA1(2) = TeemmA1(2) + Hc3(1,1,1) * (2 + 3*imag2 - 2*xpow(-1) + &
                   xpow(1) - 6*imag2*xpow(1) + 4*imag2*xpow(2))
      TeemmA1(2) = TeemmA1(2) + Hc4(0,0,0,0) * (1 - 2*xpow(1) + 8*xpow(2))
      TeemmA1(2) = TeemmA1(2) + Hc4(0,0,1,1) * (-tm6xm4x2)
      TeemmA1(2) = TeemmA1(2) + Hc4(0,1,0,0) * (om2xp4x2)
      TeemmA1(2) = TeemmA1(2) + Hc4(0,1,1,1) * (-2*tm6xm4x2)
      TeemmA1(2) = TeemmA1(2) + Hc4(1,0,0,0) * (2*om2xp4x2)
      TeemmA1(2) = TeemmA1(2) + Hc4(1,0,1,1) * (-tm6xm4x2)
      TeemmA1(2) = TeemmA1(2) + Hc4(1,1,0,0) * (om2xp4x2)
      TeemmA1(2) = TeemmA1(2) + Hc4(1,1,1,1) * (-7 + 14*xpow(1) - 8*xpow(2))
      TeemmA1(2) = TeemmA1(2) + zeta(2) * (-(omx(-1)*(-15 - 2*imag2 + &
                   45*xpow(1) + 7*imag2*xpow(1) - 72*xpow(2) - 10*imag2*xpow(2) + 42*xpow(3) + &
                   6*imag2*xpow(3))))
      TeemmA1(2) = TeemmA1(2) + zeta(3) * (-(omx(-1)*(-6 - 4*imag2 + 17*xpow(1) &
                   + 12*imag2*xpow(1) - 24*xpow(2) - 16*imag2*xpow(2) + 12*xpow(3) + &
                   8*imag2*xpow(3))))
      TeemmA1(2) = TeemmA1(2) + zeta(4) * ((3*(7 - 14*xpow(1) + &
                   32*xpow(2)))/4.)
    endif
    Lbit = 0.
    do i=-2,2
      do j=1,i+2
        Lbit(i) = Lbit(i) + (1*Ls)**j / fac(j) * TeemmA1(i-j)
      enddo
    enddo
    select case(size(eemmA1))
      case(5)
        eemmA1 = TeemmA1(-2:2) + Lbit(-2:2)
      case(4)
        eemmA1 = TeemmA1(-2:1) + Lbit(-2:1)
      case(3)
        eemmA1 = TeemmA1(-2:1) + Lbit(-2:1)
      case(1)
        eemmA1 = TeemmA1(0:0) + Lbit(0:0)
      case default
        call crash("ee_twoloop")
    end select

    eemmA1 = 32*pi*eemmA1
  endif

  if(present(eemmA2)) then
    TeemmA2(-4) = 2*born

    TeemmA2(-3) = 2*born*(3 + imag2)
    TeemmA2(-3) = TeemmA2(-3) + Hc1(0) * (-4*born)
    TeemmA2(-3) = TeemmA2(-3) + Hc1(1) * (-4*born)

    TeemmA2(-2) = (37 + 10*imag2 - 74*xpow(1) - 28*imag2*xpow(1) + 74*xpow(2) + &
                  24*imag2*xpow(2))/2.
    TeemmA2(-2) = TeemmA2(-2) + Hc1(0) * (-8 - 3*imag2 + 14*xpow(1) + &
                  6*imag2*xpow(1) - 12*xpow(2) - 4*imag2*xpow(2))
    TeemmA2(-2) = TeemmA2(-2) + Hc1(1) * (-2*(3 + 7*imag2 - 5*xpow(1) - &
                  14*imag2*xpow(1) + 6*xpow(2) + 12*imag2*xpow(2)))
    TeemmA2(-2) = TeemmA2(-2) + Hc2(0,0) * (2*tm6xm4x2)
    TeemmA2(-2) = TeemmA2(-2) + Hc2(0,1) * (4*born)
    TeemmA2(-2) = TeemmA2(-2) + Hc2(1,0) * (4*born)
    TeemmA2(-2) = TeemmA2(-2) + Hc2(1,1) * (2*tm6xm4x2)
    TeemmA2(-2) = TeemmA2(-2) + zeta(2) * (-12*born)

    TeemmA2(-1) = (809 + 248*imag2 - 1618*xpow(1) - 688*imag2*xpow(1) + &
                  1618*xpow(2) + 656*imag2*xpow(2))/16.
    TeemmA2(-1) = TeemmA2(-1) + Hc1(0) * ((-42 - 9*imag2 + 70*xpow(1) + &
                  26*imag2*xpow(1) - 56*xpow(2) - 24*imag2*xpow(2))/2.)
    TeemmA2(-1) = TeemmA2(-1) + Hc1(0)*zeta(2) * (6*(3 - 6*xpow(1) + &
                  8*xpow(2)))
    TeemmA2(-1) = TeemmA2(-1) + Hc1(1) * ((-28 - 29*imag2 - 4*imag2*xpow(-1) + &
                  42*xpow(1) + 14*imag2*xpow(1) - 56*xpow(2) - 24*imag2*xpow(2))/2.)
    TeemmA2(-1) = TeemmA2(-1) + Hc1(1)*zeta(2) * (6*(5 - 10*xpow(1) + &
                  8*xpow(2)))
    TeemmA2(-1) = TeemmA2(-1) + Hc2(0,0) * (-(omx(-1)*(-9 - 3*imag2 + &
                  23*xpow(1) + 9*imag2*xpow(1) - 12*xpow(2) - 10*imag2*xpow(2) + &
                  4*imag2*xpow(3))))
    TeemmA2(-1) = TeemmA2(-1) + Hc2(0,1) * (2*(1 + 4*imag2 - 8*imag2*xpow(1) + &
                  12*imag2*xpow(2)))
    TeemmA2(-1) = TeemmA2(-1) + Hc2(1,0) * (2 + imag2 - 2*imag2*xpow(1) + &
                  4*imag2*xpow(2))
    TeemmA2(-1) = TeemmA2(-1) + Hc2(1,1) * (1 - 6*imag2 + 2*xpow(-1) - &
                  4*xpow(1) + 12*imag2*xpow(1) - 16*imag2*xpow(2))
    TeemmA2(-1) = TeemmA2(-1) + Hc3(0,0,0) * (2*(-3 + 6*xpow(1) + 4*xpow(2)))
    TeemmA2(-1) = TeemmA2(-1) + Hc3(0,0,1) * (-2*om2x)
    TeemmA2(-1) = TeemmA2(-1) + Hc3(0,1,0) * (-2*om2x)
    TeemmA2(-1) = TeemmA2(-1) + Hc3(0,1,1) * (4*born)
    TeemmA2(-1) = TeemmA2(-1) + Hc3(1,0,0) * (8*xpow(2))
    TeemmA2(-1) = TeemmA2(-1) + Hc3(1,0,1) * (-2*om2x)
    TeemmA2(-1) = TeemmA2(-1) + Hc3(1,1,0) * (-2 + 27*imag3 + 4*xpow(1) - &
                  138*imag3*xpow(1) + 144*imag3*xpow(2))
    TeemmA2(-1) = TeemmA2(-1) + Hc3(1,1,1) * (2*om2xp4x2)
    TeemmA2(-1) = TeemmA2(-1) + zeta(2) * ((-57 - 18*imag2 + 114*xpow(1) + &
                  36*imag2*xpow(1) - 138*xpow(2) - 40*imag2*xpow(2))/2.)
    TeemmA2(-1) = TeemmA2(-1) + zeta(3) * (-13 + 26*xpow(1) - 30*xpow(2))

    TeemmA2(0) = (4183 + 1316*imag2 - 8366*xpow(1) - 3944*imag2*xpow(1) + &
                 8366*xpow(2) + 3560*imag2*xpow(2))/32.
    TeemmA2(0) = TeemmA2(0) + Hc1(0) * ((omx(-1)*(-106 - 31*imag2 + 272*xpow(1) &
                 + 95*imag2*xpow(1) - 286*xpow(2) - 124*imag2*xpow(2) + 120*xpow(3) + &
                 64*imag2*xpow(3)))/2.)
    TeemmA2(0) = TeemmA2(0) + Hc1(0)*zeta(2) * (-(omx(-1)*(-29 - 10*imag2 + &
                 75*xpow(1) + 30*imag2*xpow(1) - 127*xpow(2) - 48*imag2*xpow(2) + 72*xpow(3) + &
                 28*imag2*xpow(3))))
    TeemmA2(0) = TeemmA2(0) + Hc1(0)*zeta(3) * (24*born)
    TeemmA2(0) = TeemmA2(0) + Hc1(1) * ((-60 - 79*imag2 - 18*imag2*xpow(-1) + &
                 74*xpow(1) + 120*imag2*xpow(1) - 120*xpow(2) - 170*imag2*xpow(2))/2.)
    TeemmA2(0) = TeemmA2(0) + Hc1(1)*zeta(2) * (49 - 5*imag2 - 2*imag2*xpow(-2) &
                 + 3*xpow(-1) + 8*imag2*xpow(-1) - 87*xpow(1) - 6*imag2*xpow(1) + 72*xpow(2) + &
                 28*imag2*xpow(2))
    TeemmA2(0) = TeemmA2(0) + Hc1(1)*zeta(3) * (3*(7 - 14*xpow(1) + &
                 16*xpow(2)))
    TeemmA2(0) = TeemmA2(0) + Hc2(0,0) * ((omx(-1)*(45 + 5*imag2 - 125*xpow(1) &
                 - 19*imag2*xpow(1) + 70*xpow(2) + 14*imag2*xpow(2)))/2.)
    TeemmA2(0) = TeemmA2(0) + Hc2(0,0)*zeta(2) * (-(omx(-2)*(28 - 112*xpow(1) + &
                 223*xpow(2) - 222*xpow(3) + 80*xpow(4))))
    TeemmA2(0) = TeemmA2(0) + Hc2(0,1) * (-(omx(-1)*(-12 - 25*imag2 + &
                 12*xpow(1) + 19*imag2*xpow(1) + 2*imag2*xpow(2)))/2.)
    TeemmA2(0) = TeemmA2(0) + Hc2(0,1)*zeta(2) * (-2*(3 - 6*xpow(1) + &
                 8*xpow(2)))
    TeemmA2(0) = TeemmA2(0) + Hc2(1,0) * ((12 - 5*imag2 + 10*imag2*xpow(1))/2.)
    TeemmA2(0) = TeemmA2(0) + Hc2(1,0)*zeta(2) * (-10*(3 - 6*xpow(1) + &
                 8*xpow(2)))
    TeemmA2(0) = TeemmA2(0) + Hc2(1,1) * ((-7 - 29*imag2 + 18*xpow(-1) + &
                 4*imag2*xpow(-1) - 6*xpow(1) - 22*imag2*xpow(1))/2.)
    TeemmA2(0) = TeemmA2(0) + Hc2(1,1)*zeta(2) * (3 + 3*xpow(-2) - 12*xpow(-1) &
                 + 18*xpow(1) - 16*xpow(2))
    TeemmA2(0) = TeemmA2(0) + Hc3(0,0,0) * ((omx(-1)*(-21 - 8*imag2 + &
                 62*xpow(1) + 24*imag2*xpow(1) + 6*xpow(2) - 24*imag2*xpow(2) - 24*xpow(3) + &
                 8*imag2*xpow(3)))/2.)
    TeemmA2(0) = TeemmA2(0) + Hc3(0,0,1) * (omx(-1)*(-1 - 6*imag2 + 3*xpow(1) + &
                 18*imag2*xpow(1) - 28*imag2*xpow(2) + 16*imag2*xpow(3)))
    TeemmA2(0) = TeemmA2(0) + Hc3(0,1,0) * (omx(-1)*(-1 + imag2 + 3*xpow(1) - &
                 3*imag2*xpow(1) - 6*imag2*xpow(2) + 8*imag2*xpow(3)))
    TeemmA2(0) = TeemmA2(0) + Hc3(0,1,1) * (8 + 32*imag2 - 17*xpow(1) - &
                 64*imag2*xpow(1) + 12*xpow(2) + 68*imag2*xpow(2))
    TeemmA2(0) = TeemmA2(0) + Hc3(1,0,0) * (omx(-1)*(1 - 3*imag2 + &
                 9*imag2*xpow(1) + 15*xpow(2) - 14*imag2*xpow(2) - 12*xpow(3) + &
                 8*imag2*xpow(3)))
    TeemmA2(0) = TeemmA2(0) + Hc3(1,0,1) * (3 - 7*imag2 - 2*xpow(-1) + &
                 14*imag2*xpow(1) - 16*imag2*xpow(2))
    TeemmA2(0) = TeemmA2(0) + Hc3(1,1,0) * (3 - 5*imag2 - 2*xpow(-1) + &
                 10*imag2*xpow(1) - 12*imag2*xpow(2))
    TeemmA2(0) = TeemmA2(0) + Hc3(1,1,1) * ((26 - 6*imag2 - imag2*xpow(-2) - &
                 7*xpow(-1) + 4*imag2*xpow(-1) - 34*xpow(1) + 4*imag2*xpow(1) + 24*xpow(2) - &
                 24*imag2*xpow(2))/2.)
    TeemmA2(0) = TeemmA2(0) + Hc4(0,0,0,0) * (-(omx(-2)*(-1 + 4*xpow(1) + &
                 102*xpow(2) - 212*xpow(3) + 104*xpow(4)))/2.)
    TeemmA2(0) = TeemmA2(0) + Hc4(0,0,0,1) * (-8*xpow(2))
    TeemmA2(0) = TeemmA2(0) + Hc4(0,0,1,0) * (-8*xpow(2))
    TeemmA2(0) = TeemmA2(0) + Hc4(0,0,1,1) * (-2*tm6xm4x2)
    TeemmA2(0) = TeemmA2(0) + Hc4(0,1,0,0) * (-3*(1 - 2*xpow(1) + 8*xpow(2)))
    TeemmA2(0) = TeemmA2(0) + Hc4(0,1,0,1) * (-8*omx(2))
    TeemmA2(0) = TeemmA2(0) + Hc4(0,1,1,0) * (-8 + 45*imag3 + 16*xpow(1) - &
                 60*imag3*xpow(1) - 8*xpow(2))
    TeemmA2(0) = TeemmA2(0) + Hc4(0,1,1,1) * (-2*(11 - 22*xpow(1) + &
                 14*xpow(2)))
    TeemmA2(0) = TeemmA2(0) + Hc4(1,0,0,0) * (-2*(5 - 10*xpow(1) + 26*xpow(2)))
    TeemmA2(0) = TeemmA2(0) + Hc4(1,0,0,1) * (-8*xpow(2))
    TeemmA2(0) = TeemmA2(0) + Hc4(1,0,1,0) * (-8*xpow(2))
    TeemmA2(0) = TeemmA2(0) + Hc4(1,0,1,1) * (-7 + 14*xpow(1) - 8*xpow(2))
    TeemmA2(0) = TeemmA2(0) + Hc4(1,1,0,0) * (3*(-2 + 11*imag3 + imag3*xpow(-1) &
                 + 4*xpow(1) - 92*imag3*xpow(1) - 8*xpow(2) + 96*imag3*xpow(2)))
    TeemmA2(0) = TeemmA2(0) + Hc4(1,1,0,1) * (2*(-4 + 27*imag3 + 8*xpow(1) - &
                 138*imag3*xpow(1) - 4*xpow(2) + 144*imag3*xpow(2)))
    TeemmA2(0) = TeemmA2(0) + Hc4(1,1,1,0) * (-8 + 129*imag3 - 3*imag3*xpow(-1) &
                 + 16*xpow(1) - 552*imag3*xpow(1) - 8*xpow(2) + 576*imag3*xpow(2))
    TeemmA2(0) = TeemmA2(0) + Hc4(1,1,1,1) * ((-46 + 3*xpow(-2) - 12*xpow(-1) + &
                 116*xpow(1) - 56*xpow(2))/2.)
    TeemmA2(0) = TeemmA2(0) + zeta(2) * ((-297 - 26*imag2 + 634*xpow(1) + &
                 128*imag2*xpow(1) - 834*xpow(2) - 168*imag2*xpow(2))/4.)
    TeemmA2(0) = TeemmA2(0) + zeta(3) * ((omx(-1)*(-77 - 32*imag2 + 217*xpow(1) &
                 + 96*imag2*xpow(1) - 294*xpow(2) - 136*imag2*xpow(2) + 150*xpow(3) + &
                 72*imag2*xpow(3)))/2.)
    TeemmA2(0) = TeemmA2(0) + zeta(4) * (17 - 34*xpow(1) - 9*xpow(2))
    Lbit = 0.
    do i=-4,0
      do j=1,i+4
        Lbit(i) = Lbit(i) + (2*Ls)**j / fac(j) * TeemmA2(i-j)
      enddo
    enddo
    Lbit(0) = Lbit(0) + Ls * TeemmA2(-4) / 4.
    select case(size(eemmA2))
      case(5)
        eemmA2 = TeemmA2(-4:0) + Lbit(-4:0)
      case(1)
        eemmA2 = TeemmA2(0:0) + Lbit(0:0)
      case default
        call crash("ee_twoloop")
    end select
    eemmA2 = 32*eemmA2
  endif
  if(present(exchA0)) then
    exchA0 = 2*xpow(-1)*(-om2x - xpow(2))
    exchA0 = 32*pi**2*exchA0
  endif

  if(present(exchA1)) then
    TexchA1(-2) = 4*omx(2)*xpow(-1)

    TexchA1(-1) = 2*(-6 - 6*imag2 - 4*imag2*xpow(-2) + 3*xpow(-1) + &
                  6*imag2*xpow(-1) + 3*xpow(1) + 2*imag2*xpow(1))
    TexchA1(-1) = TexchA1(-1) + Hc1(0) * (-4*omx(2)*xpow(-1))
    TexchA1(-1) = TexchA1(-1) + Hc1(1) * (-4*omx(2)*xpow(-1))

    TexchA1(0) = 2*(7 + 2*imag2)*omx(2)*xpow(-1)
    TexchA1(0) = TexchA1(0) + Hc1(0) * (6 + 8*imag2 + 8*imag2*xpow(-2) - &
                 4*xpow(-1) - 11*imag2*xpow(-1) - 2*xpow(1) - 3*imag2*xpow(1))
    TexchA1(0) = TexchA1(0) + Hc1(1) * (-2*imag2*omx(2)*xpow(-1))
    TexchA1(0) = TexchA1(0) + Hc2(0,0) * (-8 + 3*xpow(-1) + 3*xpow(1))
    TexchA1(0) = TexchA1(0) + Hc2(0,1) * (2*omx(2)*xpow(-1))
    TexchA1(0) = TexchA1(0) + Hc2(1,0) * (2*omx(2)*xpow(-1))
    TexchA1(0) = TexchA1(0) + zeta(2) * (-6*omx(2)*xpow(-1))
    if(size(exchA1) > 3) then
      TexchA1(1) = 3*(10 + 3*imag2)*omx(2)*xpow(-1)
      TexchA1(1) = TexchA1(1) + Hc1(0) * (14 - 3*imag2 - 12*imag2*xpow(-2) - &
                   9*xpow(-1) + 12*imag2*xpow(-1) - 5*xpow(1))
      TexchA1(1) = TexchA1(1) + Hc1(0)*zeta(2) * (3*(xpow(-1) + xpow(1)))
      TexchA1(1) = TexchA1(1) + Hc1(1)*zeta(2) * (12*omx(2)*xpow(-1))
      TexchA1(1) = TexchA1(1) + Hc2(0,0) * (-5 - 10*imag2 - 8*imag2*xpow(-2) + &
                   4*xpow(-1) + 15*imag2*xpow(-1) + 2*xpow(1) + 3*imag2*xpow(1))
      TexchA1(1) = TexchA1(1) + Hc2(0,1) * (-2*imag2*omx(2)*xpow(-1))
      TexchA1(1) = TexchA1(1) + Hc2(1,0) * (imag2*xpow(-2)*(8 - xpow(1) - &
                   2*xpow(2) + 3*xpow(3)))
      TexchA1(1) = TexchA1(1) + Hc2(1,1) * (-2*imag2*omx(2)*xpow(-1))
      TexchA1(1) = TexchA1(1) + Hc3(0,0,0) * (-2*(-4 + xpow(-1) + xpow(1)))
      TexchA1(1) = TexchA1(1) + Hc3(0,0,1) * (-2*omx(2)*xpow(-1))
      TexchA1(1) = TexchA1(1) + Hc3(0,1,0) * (-2*omx(2)*xpow(-1))
      TexchA1(1) = TexchA1(1) + Hc3(0,1,1) * (2*omx(2)*xpow(-1))
      TexchA1(1) = TexchA1(1) + Hc3(1,0,0) * (-4 + 3*xpow(-1) + 3*xpow(1))
      TexchA1(1) = TexchA1(1) + Hc3(1,0,1) * (2*omx(2)*xpow(-1))
      TexchA1(1) = TexchA1(1) + Hc3(1,1,0) * (2*omx(2)*xpow(-1))
      TexchA1(1) = TexchA1(1) + Hc3(1,1,1) * (4*omx(2)*xpow(-1))
      TexchA1(1) = TexchA1(1) + zeta(2) * (omx(1)*xpow(-1)*(-6 - 5*imag2 + &
                   12*xpow(1) + imag2*xpow(1)))
      TexchA1(1) = TexchA1(1) + zeta(3) * (16 - 9*xpow(-1) - 9*xpow(1))

      TexchA1(2) = (62 + 19*imag2)*omx(2)*xpow(-1)
      TexchA1(2) = TexchA1(2) + Hc1(0) * (30 - 8*imag2 - 28*imag2*xpow(-2) - &
                   19*xpow(-1) + 28*imag2*xpow(-1) - 11*xpow(1))
      TexchA1(2) = TexchA1(2) + Hc1(0)*zeta(2) * (-3 - 20*imag2 - &
                   16*imag2*xpow(-2) + 40*imag2*xpow(-1))
      TexchA1(2) = TexchA1(2) + Hc1(0)*zeta(3) * (-16 + 9*xpow(-1) + 9*xpow(1))
      TexchA1(2) = TexchA1(2) + Hc1(1)*zeta(2) * (-(imag2*xpow(-2)*(-8 + &
                   5*xpow(1) - 2*xpow(2) + xpow(3))))
      TexchA1(2) = TexchA1(2) + Hc1(1)*zeta(3) * (-24 + 13*xpow(-1) + &
                   13*xpow(1))
      TexchA1(2) = TexchA1(2) + Hc2(0,0) * (-12 + 9*imag2 + 24*imag2*xpow(-2) + &
                   9*xpow(-1) - 20*imag2*xpow(-1) + 5*xpow(1) - 2*imag2*xpow(1))
      TexchA1(2) = TexchA1(2) + Hc2(0,0)*zeta(2) * (-3*(xpow(-1) + xpow(1)))
      TexchA1(2) = TexchA1(2) + Hc2(1,0) * (-(imag2*xpow(-1)*(-4 - xpow(1) + &
                   2*xpow(2))))
      TexchA1(2) = TexchA1(2) + Hc2(1,0)*zeta(2) * (-3*(-8 + 5*xpow(-1) + &
                   5*xpow(1)))
      TexchA1(2) = TexchA1(2) + Hc2(1,1)*zeta(2) * (12*omx(2)*xpow(-1))
      TexchA1(2) = TexchA1(2) + Hc3(0,0,0) * (4 + 8*imag2 + 8*imag2*xpow(-2) - &
                   4*xpow(-1) - 11*imag2*xpow(-1) - 2*xpow(1) - 3*imag2*xpow(1))
      TexchA1(2) = TexchA1(2) + Hc3(0,0,1) * (-2*imag2*omx(2)*xpow(-1))
      TexchA1(2) = TexchA1(2) + Hc3(0,1,0) * (-(imag2*xpow(-2)*(8 - 9*xpow(1) + &
                   2*xpow(2) + 3*xpow(3))))
      TexchA1(2) = TexchA1(2) + Hc3(0,1,1) * (-2*imag2*omx(2)*xpow(-1))
      TexchA1(2) = TexchA1(2) + Hc3(1,0,0) * (-1 - 10*imag2 - 24*imag2*xpow(-2) &
                   + 25*imag2*xpow(-1) - 3*imag2*xpow(1))
      TexchA1(2) = TexchA1(2) + Hc3(1,0,1) * (-2*imag2*omx(2)*xpow(-1))
      TexchA1(2) = TexchA1(2) + Hc3(1,1,0) * (imag2*omx(1)*xpow(-2)*(-8 + &
                   9*xpow(1) + 3*xpow(2)))
      TexchA1(2) = TexchA1(2) + Hc3(1,1,1) * (-2*imag2*omx(2)*xpow(-1))
      TexchA1(2) = TexchA1(2) + Hc4(0,0,0,0) * (-8 + xpow(-1) + xpow(1))
      TexchA1(2) = TexchA1(2) + Hc4(0,0,0,1) * (2*omx(2)*xpow(-1))
      TexchA1(2) = TexchA1(2) + Hc4(0,0,1,0) * (2*omx(2)*xpow(-1))
      TexchA1(2) = TexchA1(2) + Hc4(0,0,1,1) * (2*omx(2)*xpow(-1))
      TexchA1(2) = TexchA1(2) + Hc4(0,1,0,0) * (4 - 3*xpow(-1) - 3*xpow(1))
      TexchA1(2) = TexchA1(2) + Hc4(0,1,0,1) * (-2*omx(2)*xpow(-1))
      TexchA1(2) = TexchA1(2) + Hc4(0,1,1,0) * (-2*omx(2)*xpow(-1))
      TexchA1(2) = TexchA1(2) + Hc4(0,1,1,1) * (2*omx(2)*xpow(-1))
      TexchA1(2) = TexchA1(2) + Hc4(1,0,0,0) * (-4*(-3 + 2*xpow(-1) + &
                   2*xpow(1)))
      TexchA1(2) = TexchA1(2) + Hc4(1,0,0,1) * (-2*omx(2)*xpow(-1))
      TexchA1(2) = TexchA1(2) + Hc4(1,0,1,0) * (-2*omx(2)*xpow(-1))
      TexchA1(2) = TexchA1(2) + Hc4(1,0,1,1) * (2*omx(2)*xpow(-1))
      TexchA1(2) = TexchA1(2) + Hc4(1,1,0,0) * (4 - 3*xpow(-1) - 3*xpow(1))
      TexchA1(2) = TexchA1(2) + Hc4(1,1,0,1) * (2*omx(2)*xpow(-1))
      TexchA1(2) = TexchA1(2) + Hc4(1,1,1,0) * (2*omx(2)*xpow(-1))
      TexchA1(2) = TexchA1(2) + Hc4(1,1,1,1) * (8*omx(2)*xpow(-1))
      TexchA1(2) = TexchA1(2) + zeta(2) * (omx(-1)*xpow(-1)*(-15 - 4*imag2 + &
                   57*xpow(1) + 13*imag2*xpow(1) - 69*xpow(2) - 11*imag2*xpow(2) + 27*xpow(3) + &
                   4*imag2*xpow(3)))
      TexchA1(2) = TexchA1(2) + zeta(3) * (25 + 24*imag2 + 16*imag2*xpow(-2) - &
                   12*xpow(-1) - 24*imag2*xpow(-1) - 12*xpow(1) - 8*imag2*xpow(1))
      TexchA1(2) = TexchA1(2) + zeta(4) * ((-9*(-4 + 5*xpow(-1) + &
                   5*xpow(1)))/4.)
    endif
    Lbit = 0.
    do i=-2,2
      do j=1,i+2
        Lbit(i) = Lbit(i) + (1*Ls)**j / fac(j) * TexchA1(i-j)
      enddo
    enddo
    select case(size(exchA1))
      case(5)
        exchA1 = TexchA1(-2:2) + Lbit(-2:2)
      case(4)
        exchA1 = TexchA1(-2:1) + Lbit(-2:1)
      case(3)
        exchA1 = TexchA1(-2:1) + Lbit(-2:1)
      case(1)
        exchA1 = TexchA1(0:0) + Lbit(0:0)
      case default
        call crash("ee_twoloop")
    end select
    exchA1 = 32*pi*exchA1
  endif

  if(present(exchA2)) then
    TexchA2(-4) = -4*omx(2)*xpow(-1)

    TexchA2(-3) = -4*(3 + imag2)*omx(2)*xpow(-1)
    TexchA2(-3) = TexchA2(-3) + Hc1(0) * (8*omx(2)*xpow(-1))
    TexchA2(-3) = TexchA2(-3) + Hc1(1) * (8*omx(2)*xpow(-1))

    TexchA2(-2) = 74 + 22*imag2 - 37*xpow(-1) - 4*imag2*xpow(-1) - 37*xpow(1) - &
                  6*imag2*xpow(1)
    TexchA2(-2) = TexchA2(-2) + Hc1(0) * (-2*(18 - imag2 - 6*imag2*xpow(-2) - &
                  10*xpow(-1) + 6*imag2*xpow(-1) - 8*xpow(1) - 2*imag2*xpow(1) + &
                  2*imag2*xpow(2)))
    TexchA2(-2) = TexchA2(-2) + Hc1(1) * (6*(2 + imag2)*omx(2)*xpow(-1))
    TexchA2(-2) = TexchA2(-2) + Hc2(0,0) * (-2*(-16 + 7*xpow(-1) + 7*xpow(1)))
    TexchA2(-2) = TexchA2(-2) + Hc2(0,1) * (-12*omx(2)*xpow(-1))
    TexchA2(-2) = TexchA2(-2) + Hc2(1,0) * (-12*omx(2)*xpow(-1))
    TexchA2(-2) = TexchA2(-2) + Hc2(1,1) * (-8*omx(2)*xpow(-1))
    TexchA2(-2) = TexchA2(-2) + zeta(2) * (12*omx(2)*xpow(-1))

    TexchA2(-1) = (1618 + 464*imag2 - 96*imag2*xpow(-2) - 809*xpow(-1) - &
                  24*imag2*xpow(-1) - 809*xpow(1) - 168*imag2*xpow(1))/8.
    TexchA2(-1) = TexchA2(-1) + Hc1(0) * ((-204 + 45*imag2 + 144*imag2*xpow(-2) &
                  + 116*xpow(-1) - 110*imag2*xpow(-1) + 88*xpow(1) + 4*imag2*xpow(1))/2.)
    TexchA2(-1) = TexchA2(-1) + Hc1(0)*zeta(2) * (-6*(-4 + 3*xpow(-1) + &
                  3*xpow(1)))
    TexchA2(-1) = TexchA2(-1) + Hc1(1) * (-56 + 11*imag2 + 28*xpow(-1) + &
                  imag2*xpow(-1) + 28*xpow(1) + imag2*xpow(1))
    TexchA2(-1) = TexchA2(-1) + Hc1(1)*zeta(2) * (-36*omx(2)*xpow(-1))
    TexchA2(-1) = TexchA2(-1) + Hc2(0,0) * (58 + 27*imag2 + 72*imag2*xpow(-2) - &
                  33*xpow(-1) - 50*imag2*xpow(-1) - 21*xpow(1) - 8*imag2*xpow(1) + &
                  12*imag2*xpow(2))
    TexchA2(-1) = TexchA2(-1) + Hc2(0,1) * (24 + 8*imag2 - 14*xpow(-1) - &
                  5*imag2*xpow(-1) - 10*xpow(1) - 5*imag2*xpow(1))
    TexchA2(-1) = TexchA2(-1) + Hc2(1,0) * (2*(12 + 33*imag2 + &
                  22*imag2*xpow(-2) - 7*xpow(-1) - 35*imag2*xpow(-1) - 5*xpow(1) - &
                  27*imag2*xpow(1) + 22*imag2*xpow(2)))
    TexchA2(-1) = TexchA2(-1) + Hc2(1,1) * (-2*imag2*omx(2)*xpow(-1))
    TexchA2(-1) = TexchA2(-1) + Hc3(0,0,0) * (-64 - 27*imag3 - &
                  144*imag3*xpow(-2) + 22*xpow(-1) + 138*imag3*xpow(-1) + 22*xpow(1))
    TexchA2(-1) = TexchA2(-1) + Hc3(0,0,1) * (2*(-20 + 9*xpow(-1) + 9*xpow(1)))
    TexchA2(-1) = TexchA2(-1) + Hc3(0,1,0) * (-40 - 27*imag3 - &
                  144*imag3*xpow(-2) + 18*xpow(-1) + 138*imag3*xpow(-1) + 18*xpow(1))
    TexchA2(-1) = TexchA2(-1) + Hc3(0,1,1) * (4*omx(2)*xpow(-1))
    TexchA2(-1) = TexchA2(-1) + Hc3(1,0,0) * (-24 - 27*imag3 - &
                  144*imag3*xpow(-2) + 8*xpow(-1) + 138*imag3*xpow(-1) + 8*xpow(1))
    TexchA2(-1) = TexchA2(-1) + Hc3(1,0,1) * (4*omx(2)*xpow(-1))
    TexchA2(-1) = TexchA2(-1) + Hc3(1,1,0) * (-2*(4 + 27*imag3 + &
                  72*imag3*xpow(-2) - 2*xpow(-1) - 69*imag3*xpow(-1) - 2*xpow(1) - &
                  69*imag3*xpow(1) + 72*imag3*xpow(2)))
    TexchA2(-1) = TexchA2(-1) + Hc3(1,1,1) * (-8*omx(2)*xpow(-1))
    TexchA2(-1) = TexchA2(-1) + zeta(2) * (-66 - 16*imag2 + 27*xpow(-1) + &
                  7*imag2*xpow(-1) + 39*xpow(1) + 7*imag2*xpow(1))
    TexchA2(-1) = TexchA2(-1) + zeta(3) * (4*(-11 + 6*xpow(-1) + 6*xpow(1)))

    TexchA2(0) = (8366 + 2864*imag2 - 384*imag2*xpow(-2) - 4183*xpow(-1) - &
                 340*imag2*xpow(-1) - 4183*xpow(1) - 892*imag2*xpow(1))/16.
    TexchA2(0) = TexchA2(0) + Hc1(0) * ((-2106 + 1056*imag2 + &
                 2760*imag2*xpow(-2) + 1237*xpow(-1) - 2388*imag2*xpow(-1) + 869*xpow(1) + &
                 4*imag2*xpow(1) + 40*imag2*xpow(2))/8.)
    TexchA2(0) = TexchA2(0) + Hc1(0)*zeta(2) * (-(omx(-1)*xpow(-2)*(-776*imag2 &
                 + 26*xpow(1) + 1150*imag2*xpow(1) - 78*xpow(2) - 585*imag2*xpow(2) + 99*xpow(3) &
                 + 226*imag2*xpow(3) - 41*xpow(4) - 35*imag2*xpow(4) + 20*imag2*xpow(5))))
    TexchA2(0) = TexchA2(0) + Hc1(0)*zeta(3) * (80 - 240*imag3 - &
                 1152*imag3*xpow(-2) - 46*xpow(-1) + 1164*imag3*xpow(-1) - 40*xpow(1) - &
                 3*imag3*xpow(1))
    TexchA2(0) = TexchA2(0) + Hc1(1) * ((-240 - 7*imag2 - 20*imag2*xpow(-2) + &
                 120*xpow(-1) + 52*imag2*xpow(-1) + 120*xpow(1) + 82*imag2*xpow(1) - &
                 40*imag2*xpow(2))/2.)
    TexchA2(0) = TexchA2(0) + Hc1(1)*zeta(2) * (2*omx(1)*xpow(-1)*(-32 - &
                 12*imag2 + 35*xpow(1) + 12*imag2*xpow(1)))
    TexchA2(0) = TexchA2(0) + Hc1(1)*zeta(3) * (-2*(-44 + 23*xpow(-1) + &
                 23*xpow(1)))
    TexchA2(0) = TexchA2(0) + Hc2(0,0) * ((omx(-1)*xpow(-2)*(-768*imag2 - &
                 189*xpow(1) + 1441*imag2*xpow(1) + 511*xpow(2) - 896*imag2*xpow(2) - &
                 423*xpow(3) + 229*imag2*xpow(3) + 97*xpow(4) - 30*imag2*xpow(4) + &
                 4*xpow(5)))/2.)
    TexchA2(0) = TexchA2(0) + Hc2(0,0)*zeta(2) * (omx(-2)*xpow(-1)*(34 - &
                 98*xpow(1) + 121*xpow(2) - 96*xpow(3) + 33*xpow(4)))
    TexchA2(0) = TexchA2(0) + Hc2(0,1) * (2*omx(-1)*xpow(-1)*(-17 + 4*imag2 + &
                 44*xpow(1) - 7*imag2*xpow(1) - 36*xpow(2) + 7*imag2*xpow(2) + 8*xpow(3) - &
                 6*imag2*xpow(3) + xpow(4)))
    TexchA2(0) = TexchA2(0) + Hc2(0,1)*zeta(2) * (2*(-32 + 15*xpow(-1) + &
                 20*xpow(1)))
    TexchA2(0) = TexchA2(0) + Hc2(1,0) * ((108 - 129*imag2 - 480*imag2*xpow(-2) &
                 - 68*xpow(-1) + 488*imag2*xpow(-1) - 36*xpow(1) + 69*imag2*xpow(1) - &
                 4*xpow(2))/2.)
    TexchA2(0) = TexchA2(0) + Hc2(1,0)*zeta(2) * (2*(-62 + 37*xpow(-1) + &
                 40*xpow(1)))
    TexchA2(0) = TexchA2(0) + Hc2(1,1) * (-4 + 25*imag2 - 2*xpow(-2) + &
                 4*xpow(-1) + 2*imag2*xpow(-1) + 4*xpow(1) + 10*imag2*xpow(1) - 2*xpow(2))
    TexchA2(0) = TexchA2(0) + Hc2(1,1)*zeta(2) * (46*omx(2)*xpow(-1))
    TexchA2(0) = TexchA2(0) + Hc3(0,0,0) * ((omx(-1)*xpow(-2)*(-672*imag2 + &
                 111*xpow(1) + 1148*imag2*xpow(1) - 316*xpow(2) - 723*imag2*xpow(2) + &
                 264*xpow(3) + 291*imag2*xpow(3) - 65*xpow(4) - 92*imag2*xpow(4) + &
                 48*imag2*xpow(5)))/2.)
    TexchA2(0) = TexchA2(0) + Hc3(0,0,1) * (-34 - 8*imag2 + 21*xpow(-1) + &
                 7*imag2*xpow(-1) + 15*xpow(1) + 5*imag2*xpow(1))
    TexchA2(0) = TexchA2(0) + Hc3(0,1,0) * (-34 + 21*imag2 + 156*imag2*xpow(-2) &
                 + 21*xpow(-1) - 115*imag2*xpow(-1) + 15*xpow(1) + 54*imag2*xpow(1) - &
                 48*imag2*xpow(2))
    TexchA2(0) = TexchA2(0) + Hc3(0,1,1) * (12 + 20*imag2 - 7*xpow(-1) - &
                 11*imag2*xpow(-1) - 5*xpow(1) - 11*imag2*xpow(1))
    TexchA2(0) = TexchA2(0) + Hc3(1,0,0) * (-14 - 115*imag2 - 96*imag2*xpow(-2) &
                 + 7*xpow(-1) + 120*imag2*xpow(-1) + xpow(1) + 99*imag2*xpow(1) - &
                 84*imag2*xpow(2))
    TexchA2(0) = TexchA2(0) + Hc3(1,0,1) * (12 + 8*imag2 - 7*xpow(-1) - &
                 2*imag2*xpow(-1) - 5*xpow(1) - 2*imag2*xpow(1))
    TexchA2(0) = TexchA2(0) + Hc3(1,1,0) * (12 + 134*imag2 + 228*imag2*xpow(-2) &
                 - 7*xpow(-1) - 251*imag2*xpow(-1) - 5*xpow(1) - 27*imag2*xpow(1) + &
                 36*imag2*xpow(2))
    TexchA2(0) = TexchA2(0) + Hc3(1,1,1) * (-2*(6 + 7*imag2)*omx(2)*xpow(-1))
    TexchA2(0) = TexchA2(0) + Hc4(0,0,0,0) * (omx(-2)*xpow(-2)*(1728*imag3 - &
                 31*xpow(1) - 5172*imag3*xpow(1) + 182*xpow(2) + 5487*imag3*xpow(2) - &
                 300*xpow(3) - 2364*imag3*xpow(3) + 172*xpow(4) + 315*imag3*xpow(4) - 26*xpow(5) &
                 + 6*imag3*xpow(5)))
    TexchA2(0) = TexchA2(0) + Hc4(0,0,0,1) * (70 - 29*xpow(-1) - 28*xpow(1))
    TexchA2(0) = TexchA2(0) + Hc4(0,0,1,0) * (70 + 24*imag3 - 29*xpow(-1) - &
                 60*imag3*xpow(-1) - 28*xpow(1) + 3*imag3*xpow(1))
    TexchA2(0) = TexchA2(0) + Hc4(0,0,1,1) * (12 - 7*xpow(-1) - 7*xpow(1))
    TexchA2(0) = TexchA2(0) + Hc4(0,1,0,0) * (34 + 273*imag3 + &
                 1440*imag3*xpow(-2) - 10*xpow(-1) - 1440*imag3*xpow(-1) - 5*xpow(1) + &
                 6*imag3*xpow(1))
    TexchA2(0) = TexchA2(0) + Hc4(0,1,0,1) * (-4 + xpow(-1) + xpow(1))
    TexchA2(0) = TexchA2(0) + Hc4(0,1,1,0) * (-4 - 75*imag3 - &
                 288*imag3*xpow(-2) + xpow(-1) + 216*imag3*xpow(-1) + xpow(1) + &
                 63*imag3*xpow(1))
    TexchA2(0) = TexchA2(0) + Hc4(0,1,1,1) * (22*omx(2)*xpow(-1))
    TexchA2(0) = TexchA2(0) + Hc4(1,0,0,0) * (-18 + 228*imag3 + &
                 1440*imag3*xpow(-2) + 25*xpow(-1) - 1380*imag3*xpow(-1) + 28*xpow(1) + &
                 6*imag3*xpow(1))
    TexchA2(0) = TexchA2(0) + Hc4(1,0,0,1) * (3*(-8 + 5*xpow(-1) + 5*xpow(1)))
    TexchA2(0) = TexchA2(0) + Hc4(1,0,1,0) * (3*(-8 - 25*imag3 - &
                 96*imag3*xpow(-2) + 5*xpow(-1) + 92*imag3*xpow(-1) + 5*xpow(1) + &
                 imag3*xpow(1)))
    TexchA2(0) = TexchA2(0) + Hc4(1,0,1,1) * (22*omx(2)*xpow(-1))
    TexchA2(0) = TexchA2(0) + Hc4(1,1,0,0) * (-48 + 141*imag3 + &
                 1152*imag3*xpow(-2) + 29*xpow(-1) - 1107*imag3*xpow(-1) + 29*xpow(1) + &
                 282*imag3*xpow(1) - 288*imag3*xpow(2))
    TexchA2(0) = TexchA2(0) + Hc4(1,1,0,1) * (22*omx(2)*xpow(-1))
    TexchA2(0) = TexchA2(0) + Hc4(1,1,1,0) * (-44 - 258*imag3 - &
                 576*imag3*xpow(-2) + 22*xpow(-1) + 555*imag3*xpow(-1) + 22*xpow(1) + &
                 555*imag3*xpow(1) - 576*imag3*xpow(2))
    TexchA2(0) = TexchA2(0) + Hc4(1,1,1,1) * (28*omx(2)*xpow(-1))
    TexchA2(0) = TexchA2(0) + zeta(2) * ((omx(-1)*xpow(-1)*(150 - 11*imag2 - &
                 576*xpow(1) - 65*imag2*xpow(1) + 708*xpow(2) + 117*imag2*xpow(2) - 294*xpow(3) &
                 - 33*imag2*xpow(3) + 12*xpow(4)))/2.)
    TexchA2(0) = TexchA2(0) + zeta(3) * (-132 - 56*imag2 + 70*xpow(-1) + &
                 29*imag2*xpow(-1) + 70*xpow(1) + 29*imag2*xpow(1))
    TexchA2(0) = TexchA2(0) + zeta(4) * (37 + 23*xpow(-1) + 3*xpow(1))
    Lbit = 0.
    do i=-4,0
      do j=1,i+4
        Lbit(i) = Lbit(i) + (2*Ls)**j / fac(j) * TexchA2(i-j)
      enddo
    enddo
    Lbit(0) = Lbit(0) + Ls * TexchA2(-4) / 4.
    select case(size(exchA2))
      case(5)
        exchA2 = TexchA2(-4:0) + Lbit(-4:0)
      case(1)
        exchA2 = TexchA2(0:0) + Lbit(0:0)
      case default
        call crash("ee_twoloop")
    end select
    exchA2 = 32*exchA2
  endif

  END SUBROUTINE TWOLOOP
                          !!!!!!!!!!!!!!!!!!!!!!
                           END MODULE ee_twoloop2
                          !!!!!!!!!!!!!!!!!!!!!!


