FROM yulrich/mcmule-pre:1.0.0 AS buildpre
RUN apk add cmake && \
    pip3 install meson ninja && \
    apk del cmake

RUN pip3 install git+https://gitlab.com/mule-tools/pymule.git@meson
WORKDIR /monte-carlo

COPY hplog/ hplog/
COPY tools tools/
COPY subprojects/ subprojects/


COPY meson.build meson.build
COPY meson_options.txt meson_options.txt
COPY pre/meson.build pre/meson.build
COPY pre/mcmule.h.in pre/mcmule.h.in
COPY misc/meson.build misc/meson.build
COPY mue/meson.build mue/meson.build
COPY mudec/meson.build mudec/meson.build
COPY mudecrare/meson.build mudecrare/meson.build
COPY ee/meson.build ee/meson.build
COPY src/meson.build src/meson.build
COPY examples/meson.build examples/meson.build

RUN touch \
    pre/olinterface.f95 \
    pre/global_def.f95 \
    pre/functions.f95 \
    pre/ff_heavyquark.f95 \
    pre/phase_space.f95 \
    pre/massification.f95 \
    pre/mcmule.h.in \
    \
    misc/misc_ee2nngg.f95 \
    misc/misc_ee2nngl.f95 \
    misc/misc_mat_el.f95 \
    misc/misc.f95 \
    misc/misc_test.f95 \
    \
    mue/mue_mp2mpgg.opt.f95 \
    mue/mue_mp2mpgl_coll.opt.f95 \
    mue/mue_mp2mpgl_pvred.opt.f95 \
    mue/mue_mp2mpgl_fullcoll.opt.f95 \
    mue/mue_mp2mp_nuc.f95 \
    mue/mue_mp2mp_mono.f95 \
    mue/mue_em2emggeeee.f95 \
    mue/mue_em2emeeee.f95 \
    mue/mue_em2emgleeee.f95 \
    mue/mue_heavy_quark.f95 \
    mue/mue_pepezmml.f95 \
    mue/mue_pepe2mmgl.f95 \
    mue/mue_pepe2mmgl_coll.f95 \
    mue/mue_pepe2mm.f95 \
    mue/mue_mp2mp_nf_formfac_el.f95 \
    mue/mue_mp2mp_nf_formfac_mu.f95 \
    mue/mue_mp2mpgg_nts.f95 \
    mue/mue_em2em_nfem.f95 \
    mue/mue_em2em_mless.f95 \
    mue/mue_em2emgl_lbk.f95 \
    mue/mue_em2emll0.f95 \
    mue/mue_mat_el.f95 \
    mue/mue.f95 \
    mue/mue_test.f95 \
    \
    mudec/mudec_pm2ennglav.flex.opt.f95 \
    mudec/mudec_pm2enngg.f95 \
    mudec/mudec_pm2ennggav.f95 \
    mudec/mudec_h2lff.f95 \
    mudec/mudec_h2lff_full.f95 \
    mudec/mudec_h20ff.f95 \
    mudec/mudec_TLEPS0.f95 \
    mudec/mudec_TLEPS1.f95 \
    mudec/mudec_m2enn_nf_formfac.f95 \
    mudec/mudec_m2enn_nf_kernels.f95 \
    mudec/mudec_mat_el.f95 \
    mudec/mudec.f95 \
    mudec/mudec_test.f95 \
    \
    mudecrare/mudecrare_1l_onetrace.opt.f95 \
    mudecrare/mudecrare_1l_twotrace.opt.f95 \
    mudecrare/mudecrare_pm2enneeg_one.opt.f95 \
    mudecrare/mudecrare_pm2enneeg_two.opt.f95 \
    mudecrare/mudecrare_pm2ennee.f95 \
    mudecrare/mudecrare_all_amps.f95 \
    mudecrare/mudecrare_mat_el.f95 \
    mudecrare/mudecrare.f95 \
    mudecrare/mudecrare_test.f95 \
    \
    ee/ee_ee2eegg.opt.f95 \
    ee/ee_ee2eegl.opt.f95 \
    ee/ee_ee2eel.f95 \
    ee/ee_ee2eeg.f95 \
    ee/ee_ee2ee_a.f95 \
    ee/ee_ee2eegl_coll.f95 \
    ee/ee_twoloop2.f95 \
    ee/ee_ee2ee_nfbx.f95 \
    ee/ee_ee2eegl_nts.f95 \
    ee/ee_mat_el.f95 \
    ee/ee.f95 \
    ee/ee_test.f95 \
    \
    examples/main.c \
    examples/main.f95 \
    examples/shared_user.c \
    examples/shared_user.f95 \
    src/func.c \
    src/mat_el.f95 \
    src/user_dummy.f95 \
    src/integrands.f95 \
    src/vegas_m.f95 \
    src/testtools.f95 \
    src/test.f95 \
    src/user.f95 \
    src/mcmule_header.f95 \
    src/mcmule.f95


RUN pymule meson setup build -Duser=user.f95
RUN ninja -C build lib.stamp

COPY pre pre
COPY mue/charge_config.h mue/
RUN ninja -C build pre/libmcmule_pre.a && \
    cp build/.ninja_log build/.ninja_log_pre

FROM buildpre as buildmisc
COPY misc misc
RUN ninja -C build misc/libmisc.a && ninja -C build -t recompact && \
    mv build/.ninja_log build/.ninja_log_misc && \
    mv build/.ninja_deps build/.ninja_deps_misc


FROM buildpre as buildmue
COPY mue mue
RUN ninja -C build mue/libmue.a && ninja -C build -t recompact && \
    mv build/.ninja_log build/.ninja_log_mue && \
    mv build/.ninja_deps build/.ninja_deps_mue



FROM buildpre as buildmudec
COPY mudec mudec
RUN ninja -C build mudec/libmudec.a  && ninja -C build -t recompact && \
    mv build/.ninja_log build/.ninja_log_mudec && \
    mv build/.ninja_deps build/.ninja_deps_mudec


FROM buildpre as buildmudecrare
COPY mudecrare mudecrare
RUN ninja -C build mudecrare/libmudecrare.a -j1 && ninja -C build -t recompact && \
    mv build/.ninja_log build/.ninja_log_mudecrare && \
    mv build/.ninja_deps build/.ninja_deps_mudecrare


FROM buildpre as buildee
COPY ee ee
RUN ninja -C build ee/libee.a -j1  && ninja -C build -t recompact && \
    mv build/.ninja_log build/.ninja_log_ee && \
    mv build/.ninja_deps build/.ninja_deps_ee


FROM buildpre as buildpost
LABEL maintainer="yannick.ulrich@psi.ch"
LABEL version="1.0"
LABEL description="The full McMule suite"

COPY out/ out/
COPY .git/ .git/
COPY examples/main.c examples/main.f95 examples/shared_user.c examples/shared_user.f95 examples/
COPY src/func.c src/include-user.f95 src/user_dummy.f95 src/integrands.f95 src/lab.f95 src/mcmule_header.f95 src/mcmule.f95 src/testtools.f95 src/test.f95 src/vegas_m.f95 src/mat_el.f95 src/
COPY .dockerignore .gitattributes .gitignore .gitlab-ci.yml .gitmodules Dockerfile LICENSE README.md ./

COPY misc/ misc/
COPY mue/ mue/
COPY mudec/ mudec/
COPY mudecrare/ mudecrare/
COPY ee/ ee/

COPY --from=buildmisc /monte-carlo/build build
COPY --from=buildmue /monte-carlo/build build
COPY --from=buildmudec /monte-carlo/build build
COPY --from=buildmudecrare /monte-carlo/build build
COPY --from=buildee /monte-carlo/build build

RUN mv build/.ninja_deps build/.ninja_deps_pre && \
    python3 tools/merge.py pre misc mue mudec mudecrare ee

RUN for group in pre misc mue mudec mudecrare ee; do \
        cat build/.ninja_log_$group | grep "\t$group" >> build/.ninja_log ; \
    done

RUN cp tools/user-default.f95 src/user.f95
RUN ninja -C build src/tests
RUN mkdir build/out
WORKDIR /monte-carlo/build/
