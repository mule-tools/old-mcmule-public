                          !!!!!!!!!!!!!!!!!!!!!!
                          MODULE mue_em2emgl_lbk
                          !!!!!!!!!!!!!!!!!!!!!!
  use functions
  use collier
  implicit none

  real(kind=prec) ::  me2
  real(kind=prec) ::  mm2
  real(kind=prec) ::  ss
  real(kind=prec) ::  tt
  real(kind=prec) ::  s15
  real(kind=prec) ::  s25
  real(kind=prec) ::  s35
  real(kind=prec) :: logmume, logmumm, logmm, logt, logbeme, logbemm, logbbeme, logbbemm
  real(kind=prec) :: discbtme, discbtmm, discbs, discbu, discbslogt, discbulogt
  real(kind=prec) :: scalarc0ir6tme, scalarc0ir6tmm, scalarc0ir6s, scalarc0ir6u
  real(kind=prec) :: scalarc0tme, scalarc0tmm
  complex(kind=prec) :: logt_c, logbeme_c, logbemm_c, logbbeme_c, logbbemm_c
  complex(kind=prec) :: discbtme_c, discbtmm_c, discbs_c, discbu_c
  complex(kind=prec) :: scalarc0ir6tme_c, scalarc0ir6tmm_c, scalarc0ir6s_c, scalarc0ir6u_c
  complex(kind=prec) :: scalarc0tme_c, scalarc0tmm_c
  complex(kind=prec) :: c_discbtme_c, c_discbtmm_c, c_discbs_c, c_discbu_c
  complex(kind=prec) :: c_scalarc0ir6tme_c, c_scalarc0ir6tmm_c, c_scalarc0ir6s_c, c_scalarc0ir6u_c
  complex(kind=prec) :: c_scalarc0tme_c, c_scalarc0tmm_c
  contains


  FUNCTION EM2EMGL_LBK(p1, p2, p3, p4, p5,pole)
   !! e-(p1) m-(p2) -> e-(p3) m-(p4) g(p5)
   !! for massive electrons
  implicit none
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4), p5(4)
  real(kind=prec), optional :: pole
  real(kind=prec) :: em2emgl_lbk
  real(kind=prec) :: lbk_lp, lbk_nlp, lbk_lp_pole, lbk_nlp_pole
#include "charge_config.h"


  ss = sq(p1+p2); tt = sq(p2-p4)
  me2 = sq(p1); mm2 = sq(p2)
  s15 = s(p1,p5); s25 = s(p2,p5); s35 = s(p3,p5)

  call init()
  lbk_lp =    Qe**4*Qmu**0*lbk_lp_qe4(me2,mm2,ss,tt,s15,s25,s35)     &
       &    + Qe**3*Qmu**1*lbk_lp_qe3qm(me2,mm2,ss,tt,s15,s25,s35)   &
       &    + Qe**2*Qmu**2*lbk_lp_qe2qm2(me2,mm2,ss,tt,s15,s25,s35)  &
       &    + Qe**1*Qmu**3*lbk_lp_qeqm3(me2,mm2,ss,tt,s15,s25,s35)   &
       &    + Qe**0*Qmu**4*lbk_lp_qm4(me2,mm2,ss,tt,s15,s25,s35)
  lbk_nlp =   Qe**4*Qmu**0*lbk_nlp_qe4(me2,mm2,ss,tt,s15,s25,s35)    &
       &    + Qe**3*Qmu**1*lbk_nlp_qe3qm(me2,mm2,ss,tt,s15,s25,s35)  &
       &    + Qe**2*Qmu**2*lbk_nlp_qe2qm2(me2,mm2,ss,tt,s15,s25,s35) &
       &    + Qe**1*Qmu**3*lbk_nlp_qeqm3(me2,mm2,ss,tt,s15,s25,s35)  &
       &    + Qe**0*Qmu**4*lbk_nlp_qm4(me2,mm2,ss,tt,s15,s25,s35)

  em2emgl_lbk = lbk_lp + lbk_nlp
  em2emgl_lbk = 2.**4 * Pi**2 * alpha**4 * em2emgl_lbk

  if(present(pole)) then
    lbk_lp_pole =    Qe**4*Qmu**0*lbk_lp_ep_qe4(me2,mm2,ss,tt,s15,s25,s35)     &
        &          + Qe**3*Qmu**1*lbk_lp_ep_qe3qm(me2,mm2,ss,tt,s15,s25,s35)   &
        &          + Qe**2*Qmu**2*lbk_lp_ep_qe2qm2(me2,mm2,ss,tt,s15,s25,s35)  &
        &          + Qe**1*Qmu**3*lbk_lp_ep_qeqm3(me2,mm2,ss,tt,s15,s25,s35)   &
        &          + Qe**0*Qmu**4*lbk_lp_ep_qm4(me2,mm2,ss,tt,s15,s25,s35)
    lbk_nlp_pole =   Qe**4*Qmu**0*lbk_nlp_ep_qe4(me2,mm2,ss,tt,s15,s25,s35)    &
        &          + Qe**3*Qmu**1*lbk_nlp_ep_qe3qm(me2,mm2,ss,tt,s15,s25,s35)  &
        &          + Qe**2*Qmu**2*lbk_nlp_ep_qe2qm2(me2,mm2,ss,tt,s15,s25,s35) &
        &          + Qe**1*Qmu**3*lbk_nlp_ep_qeqm3(me2,mm2,ss,tt,s15,s25,s35) &
        &          + Qe**0*Qmu**4*lbk_nlp_ep_qm4(me2,mm2,ss,tt,s15,s25,s35)

    pole = lbk_lp_pole + lbk_nlp_pole
    pole = 2.**4 * Pi**2 * alpha**4 * pole
  endif

  END FUNCTION

  FUNCTION EM2EMGL_LBK_EEEE(p1, p2, p3, p4, p5,pole)
   !! e-(p1) m-(p2) -> e-(p3) m-(p4) g(p5)
   !! for massive electrons
  implicit none
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4), p5(4)
  real(kind=prec), optional :: pole
  real(kind=prec) :: em2emgl_lbk_eeee
  real(kind=prec) :: lbk_lp, lbk_nlp, lbk_lp_pole, lbk_nlp_pole


  ss = sq(p1+p2); tt = sq(p2-p4)
  me2 = sq(p1); mm2 = sq(p2)
  s15 = s(p1,p5); s25 = s(p2,p5); s35 = s(p3,p5)

  call init()
  lbk_lp =    lbk_lp_qe4(me2,mm2,ss,tt,s15,s25,s35)
  lbk_nlp =   lbk_nlp_qe4(me2,mm2,ss,tt,s15,s25,s35)

  em2emgl_lbk_eeee = lbk_lp + lbk_nlp
  em2emgl_lbk_eeee = 2.**4 * Pi**2 * alpha**4 * em2emgl_lbk_eeee

  if(present(pole)) then
    lbk_lp_pole =    lbk_lp_ep_qe4(me2,mm2,ss,tt,s15,s25,s35)
    lbk_nlp_pole =   lbk_nlp_ep_qe4(me2,mm2,ss,tt,s15,s25,s35)

    pole = lbk_lp_pole + lbk_nlp_pole
    pole = 2.**4 * Pi**2 * alpha**4 * pole
  endif

  END FUNCTION

  FUNCTION EM2EMGL_LBK11_EEEE(p1, p2, p3, p4, p5,pole)
   !! e-(p1) m-(p2) -> e-(p3) m-(p4) g(p5)
   !! for massive electrons
  implicit none
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4), p5(4)
  real(kind=prec), optional :: pole
  real(kind=prec) :: em2emgl_lbk11_eeee
  real(kind=prec) :: lbk11_lp, lbk11_nlp, lbk11_lp_pole, lbk11_nlp_pole


  ss = sq(p1+p2); tt = sq(p2-p4)
  me2 = sq(p1); mm2 = sq(p2)
  s15 = s(p1,p5); s25 = s(p2,p5); s35 = s(p3,p5)

  call init()
  lbk11_lp =    lbk11_lp_qe4(me2,mm2,ss,tt,s15,s25,s35)
  lbk11_nlp =   lbk11_nlp_qe4(me2,mm2,ss,tt,s15,s25,s35)

  em2emgl_lbk11_eeee = lbk11_lp + lbk11_nlp
  em2emgl_lbk11_eeee = 2.**4 * Pi**2 * alpha**4 * em2emgl_lbk11_eeee

  if(present(pole)) then
    lbk11_lp_pole =    lbk11_lp_ep_qe4(me2,mm2,ss,tt,s15,s25,s35)
    lbk11_nlp_pole =   lbk11_nlp_ep_qe4(me2,mm2,ss,tt,s15,s25,s35)

    pole = lbk11_lp_pole + lbk11_nlp_pole
    pole = 2.**4 * Pi**2 * alpha**4 * pole
  endif

  END FUNCTION

  FUNCTION MP2MPGL_LBK(p1, p2, p3, p4, p5,pole)
   !! e-(p1) m-(p2) -> e-(p3) m-(p4) g(p5)
   !! for massive electrons
  implicit none
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4), p5(4)
  real(kind=prec), optional :: pole
  real(kind=prec) :: mp2mpgl_lbk
  real(kind=prec) :: lbk11_lp, lbk11_nlp, lbk11_lp_pole, lbk11_nlp_pole
  real(kind=prec) :: lbk12_lp, lbk12_nlp, lbk12_lp_pole, lbk12_nlp_pole
  real(kind=prec) :: lbk22_lp, lbk22_nlp, lbk22_lp_pole, lbk22_nlp_pole
  real(kind=prec) :: tau,Ge,Gm,F1p,F2p


  ss = sq(p1+p2); tt = sq(p2-p4)
  me2 = sq(p1); mm2 = sq(p2); tau = -tt/4/mm2
  s15 = s(p1,p5); s25 = s(p2,p5); s35 = s(p3,p5)
  Ge = sachs_gel(-tt); Gm = sachs_gmag(-tt)
  F1p = (Ge+Gm*tau)/(1+tau); F2p = -(Ge-Gm)/(1+tau)

  call init()
  lbk11_lp =    lbk11_lp_qe4(me2,mm2,ss,tt,s15,s25,s35)
  lbk11_nlp =   lbk11_nlp_qe4(me2,mm2,ss,tt,s15,s25,s35)
  lbk12_lp =    lbk12_lp_qe4(me2,mm2,ss,tt,s15,s25,s35)
  lbk12_nlp =   lbk12_nlp_qe4(me2,mm2,ss,tt,s15,s25,s35)
  lbk22_lp =    lbk22_lp_qe4(me2,mm2,ss,tt,s15,s25,s35)
  lbk22_nlp =   lbk22_nlp_qe4(me2,mm2,ss,tt,s15,s25,s35)

  mp2mpgl_lbk =   F1p**2  * (lbk11_lp + lbk11_nlp) &
                + F1p*F2p * (lbk12_lp + lbk12_nlp) &
                + F2p**2  * (lbk22_lp + lbk22_nlp)
  mp2mpgl_lbk = 2.**4 * Pi**2 * alpha**4 * mp2mpgl_lbk

  if(present(pole)) then
    lbk11_lp_pole =    lbk11_lp_ep_qe4(me2,mm2,ss,tt,s15,s25,s35)
    lbk11_nlp_pole =   lbk11_nlp_ep_qe4(me2,mm2,ss,tt,s15,s25,s35)
    lbk12_lp_pole =    lbk12_lp_ep_qe4(me2,mm2,ss,tt,s15,s25,s35)
    lbk12_nlp_pole =   lbk12_nlp_ep_qe4(me2,mm2,ss,tt,s15,s25,s35)
    lbk22_lp_pole =    lbk22_lp_ep_qe4(me2,mm2,ss,tt,s15,s25,s35)
    lbk22_nlp_pole =   lbk22_nlp_ep_qe4(me2,mm2,ss,tt,s15,s25,s35)

    pole =   F1p**2  * (lbk11_lp_pole + lbk11_nlp_pole) &
           + F1p*F2p * (lbk12_lp_pole + lbk12_nlp_pole) &
           + F2p**2  * (lbk22_lp_pole + lbk22_nlp_pole)
    pole = 2.**4 * Pi**2 * alpha**4 * pole
  endif

  END FUNCTION

  FUNCTION EM2EMGL_LBK_MIXD(p1, p2, p3, p4, p5,pole)
   !! e-(p1) m-(p2) -> e-(p3) m-(p4) g(p5)
   !! for massive electrons
  implicit none
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4), p5(4)
  real(kind=prec), optional :: pole
  real(kind=prec) :: em2emgl_lbk_mixd
  real(kind=prec) :: lbk_lp, lbk_nlp, lbk_lp_pole, lbk_nlp_pole
#include "charge_config.h"


  ss = sq(p1+p2); tt = sq(p2-p4)
  me2 = sq(p1); mm2 = sq(p2)
  s15 = s(p1,p5); s25 = s(p2,p5); s35 = s(p3,p5)

  call init()
  lbk_lp =    Qe**3*Qmu**1*lbk_lp_qe3qm(me2,mm2,ss,tt,s15,s25,s35)   &
       &    + Qe**2*Qmu**2*lbk_lp_qe2qm2(me2,mm2,ss,tt,s15,s25,s35)  &
       &    + Qe**1*Qmu**3*lbk_lp_qeqm3(me2,mm2,ss,tt,s15,s25,s35)
  lbk_nlp =   Qe**3*Qmu**1*lbk_nlp_qe3qm(me2,mm2,ss,tt,s15,s25,s35)  &
       &    + Qe**2*Qmu**2*lbk_nlp_qe2qm2(me2,mm2,ss,tt,s15,s25,s35) &
       &    + Qe**1*Qmu**3*lbk_nlp_qeqm3(me2,mm2,ss,tt,s15,s25,s35)

  em2emgl_lbk_mixd = lbk_lp + lbk_nlp
  em2emgl_lbk_mixd = 2.**4 * Pi**2 * alpha**4 * em2emgl_lbk_mixd

  if(present(pole)) then
    lbk_lp_pole =    Qe**3*Qmu**1*lbk_lp_ep_qe3qm(me2,mm2,ss,tt,s15,s25,s35)   &
        &          + Qe**2*Qmu**2*lbk_lp_ep_qe2qm2(me2,mm2,ss,tt,s15,s25,s35)  &
        &          + Qe**1*Qmu**3*lbk_lp_ep_qeqm3(me2,mm2,ss,tt,s15,s25,s35)
    lbk_nlp_pole =   Qe**3*Qmu**1*lbk_nlp_ep_qe3qm(me2,mm2,ss,tt,s15,s25,s35)  &
        &          + Qe**2*Qmu**2*lbk_nlp_ep_qe2qm2(me2,mm2,ss,tt,s15,s25,s35) &
        &          + Qe**1*Qmu**3*lbk_nlp_ep_qeqm3(me2,mm2,ss,tt,s15,s25,s35)

    pole = lbk_lp_pole + lbk_nlp_pole
    pole = 2.**4 * Pi**2 * alpha**4 * pole
  endif

  END FUNCTION

  FUNCTION EM2EMGL_LBK_E3M1(p1, p2, p3, p4, p5,pole)
   !! e-(p1) m-(p2) -> e-(p3) m-(p4) g(p5)
   !! for massive electrons
  implicit none
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4), p5(4)
  real(kind=prec), optional :: pole
  real(kind=prec) :: em2emgl_lbk_e3m1
  real(kind=prec) :: lbk_lp, lbk_nlp, lbk_lp_pole, lbk_nlp_pole
#include "charge_config.h"

  ss = sq(p1+p2); tt = sq(p2-p4)
  me2 = sq(p1); mm2 = sq(p2)
  s15 = s(p1,p5); s25 = s(p2,p5); s35 = s(p3,p5)

  call init()
  lbk_lp =  Qe**3*Qmu**1*lbk_lp_qe3qm(me2,mm2,ss,tt,s15,s25,s35)
  lbk_nlp = Qe**3*Qmu**1*lbk_nlp_qe3qm(me2,mm2,ss,tt,s15,s25,s35)

  em2emgl_lbk_e3m1 = lbk_lp + lbk_nlp
  em2emgl_lbk_e3m1 = 2.**4 * Pi**2 * alpha**4 * em2emgl_lbk_e3m1

  if(present(pole)) then
    lbk_lp_pole =  Qe**3*Qmu**1*lbk_lp_ep_qe3qm(me2,mm2,ss,tt,s15,s25,s35)
    lbk_nlp_pole = Qe**3*Qmu**1*lbk_nlp_ep_qe3qm(me2,mm2,ss,tt,s15,s25,s35)

    pole = lbk_lp_pole + lbk_nlp_pole
    pole = 2.**4 * Pi**2 * alpha**4 * pole
  endif

  END FUNCTION

  FUNCTION EM2EMGL_LBK_E2M2(p1, p2, p3, p4, p5,pole)
   !! e-(p1) m-(p2) -> e-(p3) m-(p4) g(p5)
   !! for massive electrons
  implicit none
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4), p5(4)
  real(kind=prec), optional :: pole
  real(kind=prec) :: em2emgl_lbk_e2m2
  real(kind=prec) :: lbk_lp, lbk_nlp, lbk_lp_pole, lbk_nlp_pole
#include "charge_config.h"

  ss = sq(p1+p2); tt = sq(p2-p4)
  me2 = sq(p1); mm2 = sq(p2)
  s15 = s(p1,p5); s25 = s(p2,p5); s35 = s(p3,p5)

  call init()
  lbk_lp =  Qe**2*Qmu**2*lbk_lp_qe2qm2(me2,mm2,ss,tt,s15,s25,s35)
  lbk_nlp = Qe**2*Qmu**2*lbk_nlp_qe2qm2(me2,mm2,ss,tt,s15,s25,s35)

  em2emgl_lbk_e2m2 = lbk_lp + lbk_nlp
  em2emgl_lbk_e2m2 = 2.**4 * Pi**2 * alpha**4 * em2emgl_lbk_e2m2

  if(present(pole)) then
    lbk_lp_pole =  Qe**2*Qmu**2*lbk_lp_ep_qe2qm2(me2,mm2,ss,tt,s15,s25,s35)
    lbk_nlp_pole = Qe**2*Qmu**2*lbk_nlp_ep_qe2qm2(me2,mm2,ss,tt,s15,s25,s35)

    pole = lbk_lp_pole + lbk_nlp_pole
    pole = 2.**4 * Pi**2 * alpha**4 * pole
  endif

  END FUNCTION

  FUNCTION EM2EMGL_LBK_E1M3(p1, p2, p3, p4, p5,pole)
   !! e-(p1) m-(p2) -> e-(p3) m-(p4) g(p5)
   !! for massive electrons
  implicit none
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4), p5(4)
  real(kind=prec), optional :: pole
  real(kind=prec) :: em2emgl_lbk_e1m3
  real(kind=prec) :: lbk_lp, lbk_nlp, lbk_lp_pole, lbk_nlp_pole
#include "charge_config.h"

  ss = sq(p1+p2); tt = sq(p2-p4)
  me2 = sq(p1); mm2 = sq(p2)
  s15 = s(p1,p5); s25 = s(p2,p5); s35 = s(p3,p5)

  call init()
  lbk_lp =  Qe**1*Qmu**3*lbk_lp_qeqm3(me2,mm2,ss,tt,s15,s25,s35)
  lbk_nlp = Qe**1*Qmu**3*lbk_nlp_qeqm3(me2,mm2,ss,tt,s15,s25,s35)

  em2emgl_lbk_e1m3 = lbk_lp + lbk_nlp
  em2emgl_lbk_e1m3 = 2.**4 * Pi**2 * alpha**4 * em2emgl_lbk_e1m3

  if(present(pole)) then
    lbk_lp_pole =  Qe**1*Qmu**3*lbk_lp_ep_qeqm3(me2,mm2,ss,tt,s15,s25,s35)
    lbk_nlp_pole = Qe**1*Qmu**3*lbk_nlp_ep_qeqm3(me2,mm2,ss,tt,s15,s25,s35)

    pole = lbk_lp_pole + lbk_nlp_pole
    pole = 2.**4 * Pi**2 * alpha**4 * pole
  endif

  END FUNCTION

  FUNCTION EM2EMGL_LS_MIXD(p1, p2, p3, p4, p5,pole)
   !! e-(p1) m-(p2) -> e-(p3) m-(p4) g(p5)
   !! for massive electrons
  implicit none
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4), p5(4)
  real(kind=prec), optional :: pole
  real(kind=prec) :: em2emgl_ls_mixd
  real(kind=prec) :: lbk_lp, lbk_lp_pole


  ss = sq(p1+p2); tt = sq(p2-p4)
  me2 = sq(p1); mm2 = sq(p2)
  s15 = s(p1,p5); s25 = s(p2,p5); s35 = s(p3,p5)

  call init()
  lbk_lp =    lbk_lp_qe3qm(me2,mm2,ss,tt,s15,s25,s35)   &
       &    + lbk_lp_qe2qm2(me2,mm2,ss,tt,s15,s25,s35)  &
       &    + lbk_lp_qeqm3(me2,mm2,ss,tt,s15,s25,s35)

  em2emgl_ls_mixd = lbk_lp
  em2emgl_ls_mixd = 2.**4 * Pi**2 * alpha**4 * em2emgl_ls_mixd

  if(present(pole)) then
    lbk_lp_pole =    lbk_lp_ep_qe3qm(me2,mm2,ss,tt,s15,s25,s35)   &
        &          + lbk_lp_ep_qe2qm2(me2,mm2,ss,tt,s15,s25,s35)  &
        &          + lbk_lp_ep_qeqm3(me2,mm2,ss,tt,s15,s25,s35)

    pole = lbk_lp_pole
    pole = 2.**4 * Pi**2 * alpha**4 * pole
  endif

  END FUNCTION

  FUNCTION EM2EMGL_LS_E3M1(p1, p2, p3, p4, p5,pole)
   !! e-(p1) m-(p2) -> e-(p3) m-(p4) g(p5)
   !! for massive electrons
  implicit none
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4), p5(4)
  real(kind=prec), optional :: pole
  real(kind=prec) :: em2emgl_ls_e3m1
  real(kind=prec) :: lbk_lp, lbk_lp_pole


  ss = sq(p1+p2); tt = sq(p2-p4)
  me2 = sq(p1); mm2 = sq(p2)
  s15 = s(p1,p5); s25 = s(p2,p5); s35 = s(p3,p5)

  call init()
  lbk_lp =    lbk_lp_qe3qm(me2,mm2,ss,tt,s15,s25,s35)

  em2emgl_ls_e3m1 = lbk_lp
  em2emgl_ls_e3m1 = 2.**4 * Pi**2 * alpha**4 * em2emgl_ls_e3m1

  if(present(pole)) then
    lbk_lp_pole =  lbk_lp_ep_qe3qm(me2,mm2,ss,tt,s15,s25,s35)

    pole = lbk_lp_pole
    pole = 2.**4 * Pi**2 * alpha**4 * pole
  endif

  END FUNCTION

  FUNCTION EM2EMGL_LS_E2M2(p1, p2, p3, p4, p5,pole)
   !! e-(p1) m-(p2) -> e-(p3) m-(p4) g(p5)
   !! for massive electrons
  implicit none
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4), p5(4)
  real(kind=prec), optional :: pole
  real(kind=prec) :: em2emgl_ls_e2m2
  real(kind=prec) :: lbk_lp, lbk_lp_pole


  ss = sq(p1+p2); tt = sq(p2-p4)
  me2 = sq(p1); mm2 = sq(p2)
  s15 = s(p1,p5); s25 = s(p2,p5); s35 = s(p3,p5)

  call init()
  lbk_lp =    lbk_lp_qe2qm2(me2,mm2,ss,tt,s15,s25,s35)

  em2emgl_ls_e2m2 = lbk_lp
  em2emgl_ls_e2m2 = 2.**4 * Pi**2 * alpha**4 * em2emgl_ls_e2m2

  if(present(pole)) then
    lbk_lp_pole =  lbk_lp_ep_qe2qm2(me2,mm2,ss,tt,s15,s25,s35)

    pole = lbk_lp_pole
    pole = 2.**4 * Pi**2 * alpha**4 * pole
  endif

  END FUNCTION

  FUNCTION EM2EMGL_LS_E1M3(p1, p2, p3, p4, p5,pole)
   !! e-(p1) m-(p2) -> e-(p3) m-(p4) g(p5)
   !! for massive electrons
  implicit none
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4), p5(4)
  real(kind=prec), optional :: pole
  real(kind=prec) :: em2emgl_ls_e1m3
  real(kind=prec) :: lbk_lp, lbk_lp_pole


  ss = sq(p1+p2); tt = sq(p2-p4)
  me2 = sq(p1); mm2 = sq(p2)
  s15 = s(p1,p5); s25 = s(p2,p5); s35 = s(p3,p5)

  call init()
  lbk_lp =    lbk_lp_qeqm3(me2,mm2,ss,tt,s15,s25,s35)

  em2emgl_ls_e1m3 = lbk_lp
  em2emgl_ls_e1m3 = 2.**4 * Pi**2 * alpha**4 * em2emgl_ls_e1m3

  if(present(pole)) then
    lbk_lp_pole =  lbk_lp_ep_qeqm3(me2,mm2,ss,tt,s15,s25,s35)

    pole = lbk_lp_pole
    pole = 2.**4 * Pi**2 * alpha**4 * pole
  endif

  END FUNCTION

  FUNCTION EM2EMGL_LBK_MMMM(p1, p2, p3, p4, p5,pole)
   !! e-(p1) m-(p2) -> e-(p3) m-(p4) g(p5)
   !! for massive electrons
  implicit none
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4), p5(4)
  real(kind=prec), optional :: pole
  real(kind=prec) :: em2emgl_lbk_mmmm
  real(kind=prec) :: lbk_lp, lbk_nlp, lbk_lp_pole, lbk_nlp_pole


  ss = sq(p1+p2); tt = sq(p2-p4)
  me2 = sq(p1); mm2 = sq(p2)
  s15 = s(p1,p5); s25 = s(p2,p5); s35 = s(p3,p5)

  call init()
  lbk_lp =    lbk_lp_qm4(me2,mm2,ss,tt,s15,s25,s35)
  lbk_nlp =   lbk_nlp_qm4(me2,mm2,ss,tt,s15,s25,s35)

  em2emgl_lbk_mmmm = lbk_lp + lbk_nlp
  em2emgl_lbk_mmmm = 2.**4 * Pi**2 * alpha**4 * em2emgl_lbk_mmmm

  if(present(pole)) then
    lbk_lp_pole =    lbk_lp_ep_qm4(me2,mm2,ss,tt,s15,s25,s35)
    lbk_nlp_pole =   lbk_nlp_ep_qm4(me2,mm2,ss,tt,s15,s25,s35)

    pole = lbk_lp_pole + lbk_nlp_pole
    pole = 2.**4 * Pi**2 * alpha**4 * pole
  endif

  END FUNCTION

  FUNCTION MYLOG(x,branchcut)
   implicit none
   complex(kind=prec) :: mylog
   real(kind=prec) :: x
   integer :: branchcut

   mylog = log(abs(x))
   if(x<0) mylog = mylog + branchcut*imag*pi
  END FUNCTION

  SUBROUTINE INIT()
   implicit none
   real(kind=prec) :: me, mm, uu

   me = sqrt(me2)
   mm = sqrt(mm2)
   uu = 2*me2 + 2*mm**2 - ss - tt

   !! LOGS !!
   logmume = log(musq/me2)
   logmumm = log(musq/mm2)
   logmm   = log(me2/mm2)

   logt_c     = mylog(-me2/tt,1)
   logbeme_c  = mylog(2/(1 + sqrt(1 - 4*me2/tt)), 1)
   logbemm_c  = mylog(2/(1 + sqrt(1 - 4*mm2/tt)), 1)
   logbbeme_c = mylog((-1 + sqrt(1 - 4*me2/tt))/(1 + sqrt(1 - 4*me2/tt)), 1)
   logbbemm_c = mylog((-1 + sqrt(1 - 4*mm2/tt))/(1 + sqrt(1 - 4*mm2/tt)), 1)

   logt     = real(logt_c)
   logbeme  = real(logbeme_c)
   logbemm  = real(logbemm_c)
   logbbeme = real(logbbeme_c)
   logbbemm = real(logbbemm_c)

   !! DISCB !!
   c_discbtme_c = discb_coll(tt,me,me)
   c_discbtmm_c = discb_coll(tt,mm,mm)
   c_discbs_c   = discb_coll(ss,me,mm)
   c_discbu_c   = discb_coll(uu,me,mm)

   discbtme_c = discb_cplx(tt,me)
   discbtmm_c = discb_cplx(tt,mm)
   discbs_c = discb_cplx(ss,me,mm)  !interface enabled
   discbu_c = discb_cplx(uu,me,mm)  !interface enabled

   discbtme   = real(discbtme_c)
   discbtmm   = real(discbtmm_c)
   discbs     = real(discbs_c)
   discbu     = real(discbu_c)
   discbslogt = real(discbs_c*logt_c)
   discbulogt = real(discbu_c*logt_c)

   !! SCALARC0IR6 !!
   c_scalarc0ir6tme_c = scalarc0ir6_coll(tt,me,me)
   c_scalarc0ir6tmm_c = scalarc0ir6_coll(tt,mm,mm)
   c_scalarc0ir6s_c   = scalarc0ir6_coll(ss,me,mm)
   c_scalarc0ir6u_c   = scalarc0ir6_coll(uu,me,mm)

   scalarc0ir6tme_c = scalarc0ir6_cplx(tt,me)
   scalarc0ir6tmm_c = scalarc0ir6_cplx(tt,mm)
   scalarc0ir6s_c   = scalarc0ir6_cplx(ss,me,mm)   !interface enabled
   scalarc0ir6u_c   = scalarc0ir6_cplx(uu,me,mm)   !interface enabled

   scalarc0ir6tme = real(scalarc0ir6tme_c)
   scalarc0ir6tmm = real(scalarc0ir6tmm_c)
   scalarc0ir6s   = real(scalarc0ir6s_c)
   scalarc0ir6u   = real(scalarc0ir6u_c)

   !! SCALARC0 !!
   c_scalarc0tme_c = scalarc0_coll(me2,me2,tt,0.,me,0.)
   c_scalarc0tmm_c = scalarc0_coll(mm2,mm2,tt,0.,mm,0.)

   scalarc0tme_c = scalarc0_cplx(tt,me)
   scalarc0tmm_c = scalarc0_cplx(tt,mm)

   scalarc0tme = real(scalarc0tme_c)
   scalarc0tmm = real(scalarc0tmm_c)

  !write(*,*) "discbtme = ", discbtme, c_discbtme_c, "-8.48174"
  !write(*,*) "discbtmm = ", discbtmm, c_discbtmm_c, "-2.01853"
  !write(*,*) "discbs = ", discbs, c_discbs_c, "-4.52763"
  !write(*,*) "discbu = ", discbu, c_discbu_c, "-10.4767"

  !write(*,*) "c0ir6tme = ", scalarc0ir6tme, c_scalarc0ir6tme_c, "-0.027315"
  !write(*,*) "c0ir6tmm = ", scalarc0ir6tmm, c_scalarc0ir6tmm_c, "-8.11777e-7"
  !write(*,*) "c0ir6s = ", scalarc0ir6s, c_scalarc0ir6s_c, "-0.0000601932"
  !write(*,*) "c0ir6u = ", scalarc0ir6u, c_scalarc0ir6u_c, "-0.000163554"

  !write(*,*) "c0tme = ", scalarc0tme, c_scalarc0tme_c, "-0.0338621"
  !write(*,*) "c0tmm = ", scalarc0tmm, c_scalarc0tmm_c, "-0.00111586"

  END SUBROUTINE

  FUNCTION LBK11_LP_EP_QE4(me2, mm2, ss, tt, s15, s25, s35)
  implicit none
  real LBK11_LP_EP_Qe4
  real(kind=prec) ::  me2
  real(kind=prec) ::  mm2
  real(kind=prec) ::  ss
  real(kind=prec) ::  tt
  real(kind=prec) ::  s15
  real(kind=prec) ::  s25
  real(kind=prec) ::  s35
  real tmp1, tmp2, tmp3, tmp4, tmp5
  real tmp6, tmp7, tmp8, tmp9, tmp10
  real tmp11, tmp12, tmp13, tmp14, tmp15

  tmp1 = 1/s15
  tmp2 = 1/s35
  tmp3 = tmp1**2
  tmp4 = me2**2
  tmp5 = tmp2**2
  tmp6 = 4*me2
  tmp7 = -tt
  tmp8 = tmp6 + tmp7
  tmp9 = 1/tmp8
  tmp10 = tt**(-2)
  tmp11 = mm2**2
  tmp12 = me2**3
  tmp13 = ss**2
  tmp14 = me2**4
  tmp15 = 1/tt
  LBK11_LP_EP_Qe4 = -64*me2*tmp1*tmp2 + 64*ss*tmp1*tmp2 + 256*me2*mm2*ss*tmp1*tmp10*tmp2 - 128*me2*t&
                     &mp1*tmp10*tmp11*tmp2 - 128*tmp1*tmp10*tmp12*tmp2 - 128*me2*tmp1*tmp10*tmp13*tmp2&
                     & + (32*tmp1*tmp2)/tmp15 + 128*me2*mm2*tmp1*tmp15*tmp2 - 256*me2*ss*tmp1*tmp15*tm&
                     &p2 - 128*mm2*ss*tmp1*tmp15*tmp2 + 64*tmp1*tmp11*tmp15*tmp2 + 64*tmp1*tmp13*tmp15&
                     &*tmp2 + 32*me2*tmp3 - 128*me2*mm2*ss*tmp10*tmp3 + 64*me2*tmp10*tmp11*tmp3 + 64*t&
                     &mp10*tmp12*tmp3 + 64*me2*tmp10*tmp13*tmp3 + 64*me2*ss*tmp15*tmp3 - 256*mm2*tmp1*&
                     &tmp10*tmp2*tmp4 + 256*ss*tmp1*tmp10*tmp2*tmp4 + 64*tmp1*tmp15*tmp2*tmp4 + 128*mm&
                     &2*tmp10*tmp3*tmp4 - 128*ss*tmp10*tmp3*tmp4 + 32*me2*tmp5 - 128*me2*mm2*ss*tmp10*&
                     &tmp5 + 64*me2*tmp10*tmp11*tmp5 + 64*tmp10*tmp12*tmp5 + 64*me2*tmp10*tmp13*tmp5 +&
                     & 64*me2*ss*tmp15*tmp5 + 128*mm2*tmp10*tmp4*tmp5 - 128*ss*tmp10*tmp4*tmp5 - 128*d&
                     &iscbtme*me2*mm2*tmp1*tmp2*tmp9 + 384*discbtme*me2*ss*tmp1*tmp2*tmp9 + 128*discbt&
                     &me*mm2*ss*tmp1*tmp2*tmp9 - 64*discbtme*tmp1*tmp11*tmp2*tmp9 - 512*discbtme*mm2*t&
                     &mp1*tmp10*tmp12*tmp2*tmp9 + 512*discbtme*ss*tmp1*tmp10*tmp12*tmp2*tmp9 - 64*disc&
                     &btme*tmp1*tmp13*tmp2*tmp9 - 256*discbtme*tmp1*tmp10*tmp14*tmp2*tmp9 - (32*discbt&
                     &me*tmp1*tmp2*tmp9)/tmp15**2 + (128*discbtme*me2*tmp1*tmp2*tmp9)/tmp15 - (64*disc&
                     &btme*ss*tmp1*tmp2*tmp9)/tmp15 - 512*discbtme*me2*mm2*ss*tmp1*tmp15*tmp2*tmp9 + 2&
                     &56*discbtme*me2*tmp1*tmp11*tmp15*tmp2*tmp9 + 256*discbtme*tmp1*tmp12*tmp15*tmp2*&
                     &tmp9 + 256*discbtme*me2*tmp1*tmp13*tmp15*tmp2*tmp9 - 64*discbtme*me2*ss*tmp3*tmp&
                     &9 + 256*discbtme*mm2*tmp10*tmp12*tmp3*tmp9 - 256*discbtme*ss*tmp10*tmp12*tmp3*tm&
                     &p9 + 128*discbtme*tmp10*tmp14*tmp3*tmp9 - (32*discbtme*me2*tmp3*tmp9)/tmp15 + 12&
                     &8*discbtme*me2*mm2*ss*tmp15*tmp3*tmp9 - 64*discbtme*me2*tmp11*tmp15*tmp3*tmp9 - &
                     &64*discbtme*tmp12*tmp15*tmp3*tmp9 - 64*discbtme*me2*tmp13*tmp15*tmp3*tmp9 - 192*&
                     &discbtme*tmp1*tmp2*tmp4*tmp9 + 512*discbtme*mm2*ss*tmp1*tmp10*tmp2*tmp4*tmp9 - 2&
                     &56*discbtme*tmp1*tmp10*tmp11*tmp2*tmp4*tmp9 - 256*discbtme*tmp1*tmp10*tmp13*tmp2&
                     &*tmp4*tmp9 + 512*discbtme*mm2*tmp1*tmp15*tmp2*tmp4*tmp9 - 768*discbtme*ss*tmp1*t&
                     &mp15*tmp2*tmp4*tmp9 + 64*discbtme*tmp3*tmp4*tmp9 - 256*discbtme*mm2*ss*tmp10*tmp&
                     &3*tmp4*tmp9 + 128*discbtme*tmp10*tmp11*tmp3*tmp4*tmp9 + 128*discbtme*tmp10*tmp13&
                     &*tmp3*tmp4*tmp9 - 128*discbtme*mm2*tmp15*tmp3*tmp4*tmp9 + 256*discbtme*ss*tmp15*&
                     &tmp3*tmp4*tmp9 - 64*discbtme*me2*ss*tmp5*tmp9 + 256*discbtme*mm2*tmp10*tmp12*tmp&
                     &5*tmp9 - 256*discbtme*ss*tmp10*tmp12*tmp5*tmp9 + 128*discbtme*tmp10*tmp14*tmp5*t&
                     &mp9 - (32*discbtme*me2*tmp5*tmp9)/tmp15 + 128*discbtme*me2*mm2*ss*tmp15*tmp5*tmp&
                     &9 - 64*discbtme*me2*tmp11*tmp15*tmp5*tmp9 - 64*discbtme*tmp12*tmp15*tmp5*tmp9 - &
                     &64*discbtme*me2*tmp13*tmp15*tmp5*tmp9 + 64*discbtme*tmp4*tmp5*tmp9 - 256*discbtm&
                     &e*mm2*ss*tmp10*tmp4*tmp5*tmp9 + 128*discbtme*tmp10*tmp11*tmp4*tmp5*tmp9 + 128*di&
                     &scbtme*tmp10*tmp13*tmp4*tmp5*tmp9 - 128*discbtme*mm2*tmp15*tmp4*tmp5*tmp9 + 256*&
                     &discbtme*ss*tmp15*tmp4*tmp5*tmp9

  END FUNCTION LBK11_LP_EP_QE4

  FUNCTION LBK11_LP_QE4(me2, mm2, ss, tt, s15, s25, s35)
  implicit none
  real LBK11_LP_Qe4
  real(kind=prec) ::  me2
  real(kind=prec) ::  mm2
  real(kind=prec) ::  ss
  real(kind=prec) ::  tt
  real(kind=prec) ::  s15
  real(kind=prec) ::  s25
  real(kind=prec) ::  s35
  real tmp1, tmp2, tmp3, tmp4, tmp5
  real tmp6, tmp7, tmp8, tmp9, tmp10
  real tmp11, tmp12, tmp13, tmp14, tmp15
  real tmp16

  tmp1 = s15**(-2)
  tmp2 = s35**(-2)
  tmp3 = 1/s15
  tmp4 = 1/s35
  tmp5 = me2**2
  tmp6 = 4*me2
  tmp7 = -tt
  tmp8 = tmp6 + tmp7
  tmp9 = 1/tmp8
  tmp10 = mm2**2
  tmp11 = ss**2
  tmp12 = me2**3
  tmp13 = tt**(-2)
  tmp14 = me2**4
  tmp15 = 1/tt
  tmp16 = tmp15**(-2)
  LBK11_LP_Qe4 = 64*me2*tmp1 + 32*logmume*me2*tmp1 + 64*me2*scalarc0ir6tme*ss*tmp1 - 256*me2*mm2*&
                  &ss*tmp1*tmp13 - 128*logmume*me2*mm2*ss*tmp1*tmp13 + 128*me2*tmp1*tmp10*tmp13 + 6&
                  &4*logmume*me2*tmp1*tmp10*tmp13 + 128*me2*tmp1*tmp11*tmp13 + 64*logmume*me2*tmp1*&
                  &tmp11*tmp13 + 128*tmp1*tmp12*tmp13 + 64*logmume*tmp1*tmp12*tmp13 - 256*mm2*scala&
                  &rc0ir6tme*tmp1*tmp12*tmp13 + 256*scalarc0ir6tme*ss*tmp1*tmp12*tmp13 - 128*scalar&
                  &c0ir6tme*tmp1*tmp13*tmp14 + (32*me2*scalarc0ir6tme*tmp1)/tmp15 + 128*me2*ss*tmp1&
                  &*tmp15 + 64*logmume*me2*ss*tmp1*tmp15 - 128*me2*mm2*scalarc0ir6tme*ss*tmp1*tmp15&
                  & + 64*me2*scalarc0ir6tme*tmp1*tmp10*tmp15 + 64*me2*scalarc0ir6tme*tmp1*tmp11*tmp&
                  &15 + 64*scalarc0ir6tme*tmp1*tmp12*tmp15 + 64*me2*tmp2 + 32*logmume*me2*tmp2 + 64&
                  &*me2*scalarc0ir6tme*ss*tmp2 - 256*me2*mm2*ss*tmp13*tmp2 - 128*logmume*me2*mm2*ss&
                  &*tmp13*tmp2 + 128*me2*tmp10*tmp13*tmp2 + 64*logmume*me2*tmp10*tmp13*tmp2 + 128*m&
                  &e2*tmp11*tmp13*tmp2 + 64*logmume*me2*tmp11*tmp13*tmp2 + 128*tmp12*tmp13*tmp2 + 6&
                  &4*logmume*tmp12*tmp13*tmp2 - 256*mm2*scalarc0ir6tme*tmp12*tmp13*tmp2 + 256*scala&
                  &rc0ir6tme*ss*tmp12*tmp13*tmp2 - 128*scalarc0ir6tme*tmp13*tmp14*tmp2 + (32*me2*sc&
                  &alarc0ir6tme*tmp2)/tmp15 + 128*me2*ss*tmp15*tmp2 + 64*logmume*me2*ss*tmp15*tmp2 &
                  &- 128*me2*mm2*scalarc0ir6tme*ss*tmp15*tmp2 + 64*me2*scalarc0ir6tme*tmp10*tmp15*t&
                  &mp2 + 64*me2*scalarc0ir6tme*tmp11*tmp15*tmp2 + 64*scalarc0ir6tme*tmp12*tmp15*tmp&
                  &2 - 128*me2*tmp3*tmp4 - 64*logmume*me2*tmp3*tmp4 + 128*me2*mm2*scalarc0ir6tme*tm&
                  &p3*tmp4 + 128*ss*tmp3*tmp4 + 64*logmume*ss*tmp3*tmp4 - 384*me2*scalarc0ir6tme*ss&
                  &*tmp3*tmp4 - 128*mm2*scalarc0ir6tme*ss*tmp3*tmp4 + 64*scalarc0ir6tme*tmp10*tmp3*&
                  &tmp4 + 64*scalarc0ir6tme*tmp11*tmp3*tmp4 + 512*me2*mm2*ss*tmp13*tmp3*tmp4 + 256*&
                  &logmume*me2*mm2*ss*tmp13*tmp3*tmp4 - 256*me2*tmp10*tmp13*tmp3*tmp4 - 128*logmume&
                  &*me2*tmp10*tmp13*tmp3*tmp4 - 256*me2*tmp11*tmp13*tmp3*tmp4 - 128*logmume*me2*tmp&
                  &11*tmp13*tmp3*tmp4 - 256*tmp12*tmp13*tmp3*tmp4 - 128*logmume*tmp12*tmp13*tmp3*tm&
                  &p4 + 512*mm2*scalarc0ir6tme*tmp12*tmp13*tmp3*tmp4 - 512*scalarc0ir6tme*ss*tmp12*&
                  &tmp13*tmp3*tmp4 + 256*scalarc0ir6tme*tmp13*tmp14*tmp3*tmp4 + (64*tmp3*tmp4)/tmp1&
                  &5 + (32*logmume*tmp3*tmp4)/tmp15 - (128*me2*scalarc0ir6tme*tmp3*tmp4)/tmp15 + (6&
                  &4*scalarc0ir6tme*ss*tmp3*tmp4)/tmp15 + 256*me2*mm2*tmp15*tmp3*tmp4 + 128*logmume&
                  &*me2*mm2*tmp15*tmp3*tmp4 - 512*me2*ss*tmp15*tmp3*tmp4 - 256*logmume*me2*ss*tmp15&
                  &*tmp3*tmp4 - 256*mm2*ss*tmp15*tmp3*tmp4 - 128*logmume*mm2*ss*tmp15*tmp3*tmp4 + 5&
                  &12*me2*mm2*scalarc0ir6tme*ss*tmp15*tmp3*tmp4 + 128*tmp10*tmp15*tmp3*tmp4 + 64*lo&
                  &gmume*tmp10*tmp15*tmp3*tmp4 - 256*me2*scalarc0ir6tme*tmp10*tmp15*tmp3*tmp4 + 128&
                  &*tmp11*tmp15*tmp3*tmp4 + 64*logmume*tmp11*tmp15*tmp3*tmp4 - 256*me2*scalarc0ir6t&
                  &me*tmp11*tmp15*tmp3*tmp4 - 256*scalarc0ir6tme*tmp12*tmp15*tmp3*tmp4 + 32*scalarc&
                  &0ir6tme*tmp16*tmp3*tmp4 - 64*scalarc0ir6tme*tmp1*tmp5 + 256*mm2*tmp1*tmp13*tmp5 &
                  &+ 128*logmume*mm2*tmp1*tmp13*tmp5 - 256*ss*tmp1*tmp13*tmp5 - 128*logmume*ss*tmp1&
                  &*tmp13*tmp5 + 256*mm2*scalarc0ir6tme*ss*tmp1*tmp13*tmp5 - 128*scalarc0ir6tme*tmp&
                  &1*tmp10*tmp13*tmp5 - 128*scalarc0ir6tme*tmp1*tmp11*tmp13*tmp5 + 128*mm2*scalarc0&
                  &ir6tme*tmp1*tmp15*tmp5 - 256*scalarc0ir6tme*ss*tmp1*tmp15*tmp5 - 64*scalarc0ir6t&
                  &me*tmp2*tmp5 + 256*mm2*tmp13*tmp2*tmp5 + 128*logmume*mm2*tmp13*tmp2*tmp5 - 256*s&
                  &s*tmp13*tmp2*tmp5 - 128*logmume*ss*tmp13*tmp2*tmp5 + 256*mm2*scalarc0ir6tme*ss*t&
                  &mp13*tmp2*tmp5 - 128*scalarc0ir6tme*tmp10*tmp13*tmp2*tmp5 - 128*scalarc0ir6tme*t&
                  &mp11*tmp13*tmp2*tmp5 + 128*mm2*scalarc0ir6tme*tmp15*tmp2*tmp5 - 256*scalarc0ir6t&
                  &me*ss*tmp15*tmp2*tmp5 + 192*scalarc0ir6tme*tmp3*tmp4*tmp5 - 512*mm2*tmp13*tmp3*t&
                  &mp4*tmp5 - 256*logmume*mm2*tmp13*tmp3*tmp4*tmp5 + 512*ss*tmp13*tmp3*tmp4*tmp5 + &
                  &256*logmume*ss*tmp13*tmp3*tmp4*tmp5 - 512*mm2*scalarc0ir6tme*ss*tmp13*tmp3*tmp4*&
                  &tmp5 + 256*scalarc0ir6tme*tmp10*tmp13*tmp3*tmp4*tmp5 + 256*scalarc0ir6tme*tmp11*&
                  &tmp13*tmp3*tmp4*tmp5 + 128*tmp15*tmp3*tmp4*tmp5 + 64*logmume*tmp15*tmp3*tmp4*tmp&
                  &5 - 512*mm2*scalarc0ir6tme*tmp15*tmp3*tmp4*tmp5 + 768*scalarc0ir6tme*ss*tmp15*tm&
                  &p3*tmp4*tmp5 - 96*discbtme*me2*ss*tmp1*tmp9 - 64*discbtme*logmume*me2*ss*tmp1*tm&
                  &p9 + 512*discbtme*mm2*tmp1*tmp12*tmp13*tmp9 + 256*discbtme*logmume*mm2*tmp1*tmp1&
                  &2*tmp13*tmp9 - 512*discbtme*ss*tmp1*tmp12*tmp13*tmp9 - 256*discbtme*logmume*ss*t&
                  &mp1*tmp12*tmp13*tmp9 + 256*discbtme*tmp1*tmp13*tmp14*tmp9 + 128*discbtme*logmume&
                  &*tmp1*tmp13*tmp14*tmp9 - (48*discbtme*me2*tmp1*tmp9)/tmp15 - (32*discbtme*logmum&
                  &e*me2*tmp1*tmp9)/tmp15 + 192*discbtme*me2*mm2*ss*tmp1*tmp15*tmp9 + 128*discbtme*&
                  &logmume*me2*mm2*ss*tmp1*tmp15*tmp9 - 96*discbtme*me2*tmp1*tmp10*tmp15*tmp9 - 64*&
                  &discbtme*logmume*me2*tmp1*tmp10*tmp15*tmp9 - 96*discbtme*me2*tmp1*tmp11*tmp15*tm&
                  &p9 - 64*discbtme*logmume*me2*tmp1*tmp11*tmp15*tmp9 - 96*discbtme*tmp1*tmp12*tmp1&
                  &5*tmp9 - 64*discbtme*logmume*tmp1*tmp12*tmp15*tmp9 - 96*discbtme*me2*ss*tmp2*tmp&
                  &9 - 64*discbtme*logmume*me2*ss*tmp2*tmp9 + 512*discbtme*mm2*tmp12*tmp13*tmp2*tmp&
                  &9 + 256*discbtme*logmume*mm2*tmp12*tmp13*tmp2*tmp9 - 512*discbtme*ss*tmp12*tmp13&
                  &*tmp2*tmp9 - 256*discbtme*logmume*ss*tmp12*tmp13*tmp2*tmp9 + 256*discbtme*tmp13*&
                  &tmp14*tmp2*tmp9 + 128*discbtme*logmume*tmp13*tmp14*tmp2*tmp9 - (48*discbtme*me2*&
                  &tmp2*tmp9)/tmp15 - (32*discbtme*logmume*me2*tmp2*tmp9)/tmp15 + 192*discbtme*me2*&
                  &mm2*ss*tmp15*tmp2*tmp9 + 128*discbtme*logmume*me2*mm2*ss*tmp15*tmp2*tmp9 - 96*di&
                  &scbtme*me2*tmp10*tmp15*tmp2*tmp9 - 64*discbtme*logmume*me2*tmp10*tmp15*tmp2*tmp9&
                  & - 96*discbtme*me2*tmp11*tmp15*tmp2*tmp9 - 64*discbtme*logmume*me2*tmp11*tmp15*t&
                  &mp2*tmp9 - 96*discbtme*tmp12*tmp15*tmp2*tmp9 - 64*discbtme*logmume*tmp12*tmp15*t&
                  &mp2*tmp9 - 64*discbtme*me2*mm2*tmp3*tmp4*tmp9 - 128*discbtme*logmume*me2*mm2*tmp&
                  &3*tmp4*tmp9 + 640*discbtme*me2*ss*tmp3*tmp4*tmp9 + 384*discbtme*logmume*me2*ss*t&
                  &mp3*tmp4*tmp9 + 192*discbtme*mm2*ss*tmp3*tmp4*tmp9 + 128*discbtme*logmume*mm2*ss&
                  &*tmp3*tmp4*tmp9 - 96*discbtme*tmp10*tmp3*tmp4*tmp9 - 64*discbtme*logmume*tmp10*t&
                  &mp3*tmp4*tmp9 - 96*discbtme*tmp11*tmp3*tmp4*tmp9 - 64*discbtme*logmume*tmp11*tmp&
                  &3*tmp4*tmp9 - 1024*discbtme*mm2*tmp12*tmp13*tmp3*tmp4*tmp9 - 512*discbtme*logmum&
                  &e*mm2*tmp12*tmp13*tmp3*tmp4*tmp9 + 1024*discbtme*ss*tmp12*tmp13*tmp3*tmp4*tmp9 +&
                  & 512*discbtme*logmume*ss*tmp12*tmp13*tmp3*tmp4*tmp9 - 512*discbtme*tmp13*tmp14*t&
                  &mp3*tmp4*tmp9 - 256*discbtme*logmume*tmp13*tmp14*tmp3*tmp4*tmp9 + (288*discbtme*&
                  &me2*tmp3*tmp4*tmp9)/tmp15 + (128*discbtme*logmume*me2*tmp3*tmp4*tmp9)/tmp15 - (9&
                  &6*discbtme*ss*tmp3*tmp4*tmp9)/tmp15 - (64*discbtme*logmume*ss*tmp3*tmp4*tmp9)/tm&
                  &p15 - 896*discbtme*me2*mm2*ss*tmp15*tmp3*tmp4*tmp9 - 512*discbtme*logmume*me2*mm&
                  &2*ss*tmp15*tmp3*tmp4*tmp9 + 448*discbtme*me2*tmp10*tmp15*tmp3*tmp4*tmp9 + 256*di&
                  &scbtme*logmume*me2*tmp10*tmp15*tmp3*tmp4*tmp9 + 448*discbtme*me2*tmp11*tmp15*tmp&
                  &3*tmp4*tmp9 + 256*discbtme*logmume*me2*tmp11*tmp15*tmp3*tmp4*tmp9 + 448*discbtme&
                  &*tmp12*tmp15*tmp3*tmp4*tmp9 + 256*discbtme*logmume*tmp12*tmp15*tmp3*tmp4*tmp9 - &
                  &48*discbtme*tmp16*tmp3*tmp4*tmp9 - 32*discbtme*logmume*tmp16*tmp3*tmp4*tmp9 + 19&
                  &2*discbtme*tmp1*tmp5*tmp9 + 64*discbtme*logmume*tmp1*tmp5*tmp9 - 512*discbtme*mm&
                  &2*ss*tmp1*tmp13*tmp5*tmp9 - 256*discbtme*logmume*mm2*ss*tmp1*tmp13*tmp5*tmp9 + 2&
                  &56*discbtme*tmp1*tmp10*tmp13*tmp5*tmp9 + 128*discbtme*logmume*tmp1*tmp10*tmp13*t&
                  &mp5*tmp9 + 256*discbtme*tmp1*tmp11*tmp13*tmp5*tmp9 + 128*discbtme*logmume*tmp1*t&
                  &mp11*tmp13*tmp5*tmp9 - 64*discbtme*mm2*tmp1*tmp15*tmp5*tmp9 - 128*discbtme*logmu&
                  &me*mm2*tmp1*tmp15*tmp5*tmp9 + 448*discbtme*ss*tmp1*tmp15*tmp5*tmp9 + 256*discbtm&
                  &e*logmume*ss*tmp1*tmp15*tmp5*tmp9 + 192*discbtme*tmp2*tmp5*tmp9 + 64*discbtme*lo&
                  &gmume*tmp2*tmp5*tmp9 - 512*discbtme*mm2*ss*tmp13*tmp2*tmp5*tmp9 - 256*discbtme*l&
                  &ogmume*mm2*ss*tmp13*tmp2*tmp5*tmp9 + 256*discbtme*tmp10*tmp13*tmp2*tmp5*tmp9 + 1&
                  &28*discbtme*logmume*tmp10*tmp13*tmp2*tmp5*tmp9 + 256*discbtme*tmp11*tmp13*tmp2*t&
                  &mp5*tmp9 + 128*discbtme*logmume*tmp11*tmp13*tmp2*tmp5*tmp9 - 64*discbtme*mm2*tmp&
                  &15*tmp2*tmp5*tmp9 - 128*discbtme*logmume*mm2*tmp15*tmp2*tmp5*tmp9 + 448*discbtme&
                  &*ss*tmp15*tmp2*tmp5*tmp9 + 256*discbtme*logmume*ss*tmp15*tmp2*tmp5*tmp9 - 480*di&
                  &scbtme*tmp3*tmp4*tmp5*tmp9 - 192*discbtme*logmume*tmp3*tmp4*tmp5*tmp9 + 1024*dis&
                  &cbtme*mm2*ss*tmp13*tmp3*tmp4*tmp5*tmp9 + 512*discbtme*logmume*mm2*ss*tmp13*tmp3*&
                  &tmp4*tmp5*tmp9 - 512*discbtme*tmp10*tmp13*tmp3*tmp4*tmp5*tmp9 - 256*discbtme*log&
                  &mume*tmp10*tmp13*tmp3*tmp4*tmp5*tmp9 - 512*discbtme*tmp11*tmp13*tmp3*tmp4*tmp5*t&
                  &mp9 - 256*discbtme*logmume*tmp11*tmp13*tmp3*tmp4*tmp5*tmp9 + 640*discbtme*mm2*tm&
                  &p15*tmp3*tmp4*tmp5*tmp9 + 512*discbtme*logmume*mm2*tmp15*tmp3*tmp4*tmp5*tmp9 - 1&
                  &408*discbtme*ss*tmp15*tmp3*tmp4*tmp5*tmp9 - 768*discbtme*logmume*ss*tmp15*tmp3*t&
                  &mp4*tmp5*tmp9

  END FUNCTION LBK11_LP_QE4

  FUNCTION LBK11_NLP_EP_QE4(me2, mm2, ss, tt, s15, s25, s35)
  implicit none
  real LBK11_NLP_EP_Qe4
  real(kind=prec) ::  me2
  real(kind=prec) ::  mm2
  real(kind=prec) ::  ss
  real(kind=prec) ::  tt
  real(kind=prec) ::  s15
  real(kind=prec) ::  s25
  real(kind=prec) ::  s35
  real tmp1, tmp2, tmp3, tmp4, tmp5
  real tmp6, tmp7, tmp8, tmp9, tmp10
  real tmp11, tmp12, tmp13, tmp14, tmp15
  real tmp16, tmp17, tmp18, tmp19, tmp20

  tmp1 = 1/s15
  tmp2 = 4*me2
  tmp3 = -tt
  tmp4 = tmp2 + tmp3
  tmp5 = 1/tmp4
  tmp6 = tmp1**2
  tmp7 = 1/s35
  tmp8 = me2**3
  tmp9 = tt**(-2)
  tmp10 = me2**2
  tmp11 = mm2**2
  tmp12 = ss**2
  tmp13 = 1/tt
  tmp14 = -(1/tmp1)
  tmp15 = -s25
  tmp16 = tmp14 + tmp15 + 1/tmp7
  tmp17 = 1/tmp16
  tmp18 = -4*me2
  tmp19 = 1/tmp13 + tmp18
  tmp20 = 1/tmp19
  LBK11_NLP_EP_Qe4 = -256*me2*tmp17*tmp20 - 128*discbtme*me2*tmp17*tmp20 + 128*ss*tmp17*tmp20 + 128*d&
                      &iscbtme*ss*tmp17*tmp20 - 128*me2*s25*tmp1*tmp17*tmp20 - 64*discbtme*me2*s25*tmp1&
                      &*tmp17*tmp20 + 64*s25*ss*tmp1*tmp17*tmp20 + 64*discbtme*s25*ss*tmp1*tmp17*tmp20 &
                      &+ (64*tmp17*tmp20)/tmp13 + (64*discbtme*tmp17*tmp20)/tmp13 + (32*s25*tmp1*tmp17*&
                      &tmp20)/tmp13 + (32*discbtme*s25*tmp1*tmp17*tmp20)/tmp13 + 256*me2*mm2*tmp13*tmp1&
                      &7*tmp20 + 256*discbtme*me2*mm2*tmp13*tmp17*tmp20 - 768*me2*ss*tmp13*tmp17*tmp20 &
                      &- 512*discbtme*me2*ss*tmp13*tmp17*tmp20 - 256*mm2*ss*tmp13*tmp17*tmp20 - 256*dis&
                      &cbtme*mm2*ss*tmp13*tmp17*tmp20 + 128*me2*mm2*s25*tmp1*tmp13*tmp17*tmp20 + 128*di&
                      &scbtme*me2*mm2*s25*tmp1*tmp13*tmp17*tmp20 - 384*me2*s25*ss*tmp1*tmp13*tmp17*tmp2&
                      &0 - 256*discbtme*me2*s25*ss*tmp1*tmp13*tmp17*tmp20 - 128*mm2*s25*ss*tmp1*tmp13*t&
                      &mp17*tmp20 - 128*discbtme*mm2*s25*ss*tmp1*tmp13*tmp17*tmp20 + 128*tmp10*tmp13*tm&
                      &p17*tmp20 + 128*discbtme*tmp10*tmp13*tmp17*tmp20 + 64*s25*tmp1*tmp10*tmp13*tmp17&
                      &*tmp20 + 64*discbtme*s25*tmp1*tmp10*tmp13*tmp17*tmp20 + 128*tmp11*tmp13*tmp17*tm&
                      &p20 + 128*discbtme*tmp11*tmp13*tmp17*tmp20 + 64*s25*tmp1*tmp11*tmp13*tmp17*tmp20&
                      & + 64*discbtme*s25*tmp1*tmp11*tmp13*tmp17*tmp20 + 128*tmp12*tmp13*tmp17*tmp20 + &
                      &128*discbtme*tmp12*tmp13*tmp17*tmp20 + 64*s25*tmp1*tmp12*tmp13*tmp17*tmp20 + 64*&
                      &discbtme*s25*tmp1*tmp12*tmp13*tmp17*tmp20 + 32*me2*tmp1*tmp5 + 32*discbtme*me2*t&
                      &mp1*tmp5 + 32*mm2*tmp1*tmp5 + 32*discbtme*mm2*tmp1*tmp5 - 32*ss*tmp1*tmp5 - 32*d&
                      &iscbtme*ss*tmp1*tmp5 - 256*me2*mm2*tmp1*tmp13*tmp5 - 192*discbtme*me2*mm2*tmp1*t&
                      &mp13*tmp5 + 256*me2*ss*tmp1*tmp13*tmp5 + 192*discbtme*me2*ss*tmp1*tmp13*tmp5 + 1&
                      &28*mm2*ss*tmp1*tmp13*tmp5 + 128*discbtme*mm2*ss*tmp1*tmp13*tmp5 - 192*tmp1*tmp10&
                      &*tmp13*tmp5 - 128*discbtme*tmp1*tmp10*tmp13*tmp5 - 64*tmp1*tmp11*tmp13*tmp5 - 64&
                      &*discbtme*tmp1*tmp11*tmp13*tmp5 - 64*tmp1*tmp12*tmp13*tmp5 - 64*discbtme*tmp1*tm&
                      &p12*tmp13*tmp5 + 64*me2*s25*tmp5*tmp6 + 64*discbtme*me2*s25*tmp5*tmp6 - 128*me2*&
                      &mm2*s25*tmp13*tmp5*tmp6 - 128*discbtme*me2*mm2*s25*tmp13*tmp5*tmp6 + 128*me2*s25&
                      &*ss*tmp13*tmp5*tmp6 + 128*discbtme*me2*s25*ss*tmp13*tmp5*tmp6 - 384*s25*tmp10*tm&
                      &p13*tmp5*tmp6 - 256*discbtme*s25*tmp10*tmp13*tmp5*tmp6 + (128*me2*tmp1*tmp17*tmp&
                      &20)/tmp7 + (64*discbtme*me2*tmp1*tmp17*tmp20)/tmp7 - (64*ss*tmp1*tmp17*tmp20)/tm&
                      &p7 - (64*discbtme*ss*tmp1*tmp17*tmp20)/tmp7 - (32*tmp1*tmp17*tmp20)/(tmp13*tmp7)&
                      & - (32*discbtme*tmp1*tmp17*tmp20)/(tmp13*tmp7) - (128*me2*mm2*tmp1*tmp13*tmp17*t&
                      &mp20)/tmp7 - (128*discbtme*me2*mm2*tmp1*tmp13*tmp17*tmp20)/tmp7 + (384*me2*ss*tm&
                      &p1*tmp13*tmp17*tmp20)/tmp7 + (256*discbtme*me2*ss*tmp1*tmp13*tmp17*tmp20)/tmp7 +&
                      & (128*mm2*ss*tmp1*tmp13*tmp17*tmp20)/tmp7 + (128*discbtme*mm2*ss*tmp1*tmp13*tmp1&
                      &7*tmp20)/tmp7 - (64*tmp1*tmp10*tmp13*tmp17*tmp20)/tmp7 - (64*discbtme*tmp1*tmp10&
                      &*tmp13*tmp17*tmp20)/tmp7 - (64*tmp1*tmp11*tmp13*tmp17*tmp20)/tmp7 - (64*discbtme&
                      &*tmp1*tmp11*tmp13*tmp17*tmp20)/tmp7 - (64*tmp1*tmp12*tmp13*tmp17*tmp20)/tmp7 - (&
                      &64*discbtme*tmp1*tmp12*tmp13*tmp17*tmp20)/tmp7 + 128*me2*s25*tmp17*tmp20*tmp7 + &
                      &64*discbtme*me2*s25*tmp17*tmp20*tmp7 - 64*s25*ss*tmp17*tmp20*tmp7 - 64*discbtme*&
                      &s25*ss*tmp17*tmp20*tmp7 + (128*me2*tmp17*tmp20*tmp7)/tmp1 + (64*discbtme*me2*tmp&
                      &17*tmp20*tmp7)/tmp1 - (64*ss*tmp17*tmp20*tmp7)/tmp1 - (64*discbtme*ss*tmp17*tmp2&
                      &0*tmp7)/tmp1 - (32*s25*tmp17*tmp20*tmp7)/tmp13 - (32*discbtme*s25*tmp17*tmp20*tm&
                      &p7)/tmp13 - (32*tmp17*tmp20*tmp7)/(tmp1*tmp13) - (32*discbtme*tmp17*tmp20*tmp7)/&
                      &(tmp1*tmp13) - 128*me2*mm2*s25*tmp13*tmp17*tmp20*tmp7 - 128*discbtme*me2*mm2*s25&
                      &*tmp13*tmp17*tmp20*tmp7 + 384*me2*s25*ss*tmp13*tmp17*tmp20*tmp7 + 256*discbtme*m&
                      &e2*s25*ss*tmp13*tmp17*tmp20*tmp7 + 128*mm2*s25*ss*tmp13*tmp17*tmp20*tmp7 + 128*d&
                      &iscbtme*mm2*s25*ss*tmp13*tmp17*tmp20*tmp7 - (128*me2*mm2*tmp13*tmp17*tmp20*tmp7)&
                      &/tmp1 - (128*discbtme*me2*mm2*tmp13*tmp17*tmp20*tmp7)/tmp1 + (384*me2*ss*tmp13*t&
                      &mp17*tmp20*tmp7)/tmp1 + (256*discbtme*me2*ss*tmp13*tmp17*tmp20*tmp7)/tmp1 + (128&
                      &*mm2*ss*tmp13*tmp17*tmp20*tmp7)/tmp1 + (128*discbtme*mm2*ss*tmp13*tmp17*tmp20*tm&
                      &p7)/tmp1 - 64*s25*tmp10*tmp13*tmp17*tmp20*tmp7 - 64*discbtme*s25*tmp10*tmp13*tmp&
                      &17*tmp20*tmp7 - (64*tmp10*tmp13*tmp17*tmp20*tmp7)/tmp1 - (64*discbtme*tmp10*tmp1&
                      &3*tmp17*tmp20*tmp7)/tmp1 - 64*s25*tmp11*tmp13*tmp17*tmp20*tmp7 - 64*discbtme*s25&
                      &*tmp11*tmp13*tmp17*tmp20*tmp7 - (64*tmp11*tmp13*tmp17*tmp20*tmp7)/tmp1 - (64*dis&
                      &cbtme*tmp11*tmp13*tmp17*tmp20*tmp7)/tmp1 - 64*s25*tmp12*tmp13*tmp17*tmp20*tmp7 -&
                      & 64*discbtme*s25*tmp12*tmp13*tmp17*tmp20*tmp7 - (64*tmp12*tmp13*tmp17*tmp20*tmp7&
                      &)/tmp1 - (64*discbtme*tmp12*tmp13*tmp17*tmp20*tmp7)/tmp1 - 224*me2*tmp5*tmp7 - 1&
                      &60*discbtme*me2*tmp5*tmp7 - 96*mm2*tmp5*tmp7 - 96*discbtme*mm2*tmp5*tmp7 + 96*ss&
                      &*tmp5*tmp7 + 96*discbtme*ss*tmp5*tmp7 - 256*me2*s25*tmp1*tmp5*tmp7 - 192*discbtm&
                      &e*me2*s25*tmp1*tmp5*tmp7 - 64*mm2*s25*tmp1*tmp5*tmp7 - 64*discbtme*mm2*s25*tmp1*&
                      &tmp5*tmp7 + 64*s25*ss*tmp1*tmp5*tmp7 + 64*discbtme*s25*ss*tmp1*tmp5*tmp7 + (32*t&
                      &mp5*tmp7)/tmp13 + (32*discbtme*tmp5*tmp7)/tmp13 + (32*s25*tmp1*tmp5*tmp7)/tmp13 &
                      &+ (32*discbtme*s25*tmp1*tmp5*tmp7)/tmp13 + 512*me2*mm2*tmp13*tmp5*tmp7 + 320*dis&
                      &cbtme*me2*mm2*tmp13*tmp5*tmp7 - 512*me2*ss*tmp13*tmp5*tmp7 - 320*discbtme*me2*ss&
                      &*tmp13*tmp5*tmp7 - 128*mm2*ss*tmp13*tmp5*tmp7 - 128*discbtme*mm2*ss*tmp13*tmp5*t&
                      &mp7 + 384*me2*mm2*s25*tmp1*tmp13*tmp5*tmp7 + 256*discbtme*me2*mm2*s25*tmp1*tmp13&
                      &*tmp5*tmp7 - 384*me2*s25*ss*tmp1*tmp13*tmp5*tmp7 - 256*discbtme*me2*s25*ss*tmp1*&
                      &tmp13*tmp5*tmp7 + 448*tmp10*tmp13*tmp5*tmp7 + 256*discbtme*tmp10*tmp13*tmp5*tmp7&
                      & + 640*s25*tmp1*tmp10*tmp13*tmp5*tmp7 + 384*discbtme*s25*tmp1*tmp10*tmp13*tmp5*t&
                      &mp7 + 64*tmp11*tmp13*tmp5*tmp7 + 64*discbtme*tmp11*tmp13*tmp5*tmp7 + 64*tmp12*tm&
                      &p13*tmp5*tmp7 + 64*discbtme*tmp12*tmp13*tmp5*tmp7 + 1024*me2*mm2*ss*tmp17*tmp20*&
                      &tmp9 + 512*discbtme*me2*mm2*ss*tmp17*tmp20*tmp9 + 512*me2*mm2*s25*ss*tmp1*tmp17*&
                      &tmp20*tmp9 + 256*discbtme*me2*mm2*s25*ss*tmp1*tmp17*tmp20*tmp9 - 1024*mm2*tmp10*&
                      &tmp17*tmp20*tmp9 - 512*discbtme*mm2*tmp10*tmp17*tmp20*tmp9 + 1024*ss*tmp10*tmp17&
                      &*tmp20*tmp9 + 512*discbtme*ss*tmp10*tmp17*tmp20*tmp9 - 512*mm2*s25*tmp1*tmp10*tm&
                      &p17*tmp20*tmp9 - 256*discbtme*mm2*s25*tmp1*tmp10*tmp17*tmp20*tmp9 + 512*s25*ss*t&
                      &mp1*tmp10*tmp17*tmp20*tmp9 + 256*discbtme*s25*ss*tmp1*tmp10*tmp17*tmp20*tmp9 - 5&
                      &12*me2*tmp11*tmp17*tmp20*tmp9 - 256*discbtme*me2*tmp11*tmp17*tmp20*tmp9 - 256*me&
                      &2*s25*tmp1*tmp11*tmp17*tmp20*tmp9 - 128*discbtme*me2*s25*tmp1*tmp11*tmp17*tmp20*&
                      &tmp9 - 512*me2*tmp12*tmp17*tmp20*tmp9 - 256*discbtme*me2*tmp12*tmp17*tmp20*tmp9 &
                      &- 256*me2*s25*tmp1*tmp12*tmp17*tmp20*tmp9 - 128*discbtme*me2*s25*tmp1*tmp12*tmp1&
                      &7*tmp20*tmp9 - 512*me2*mm2*ss*tmp1*tmp5*tmp9 - 256*discbtme*me2*mm2*ss*tmp1*tmp5&
                      &*tmp9 + 512*mm2*tmp1*tmp10*tmp5*tmp9 + 256*discbtme*mm2*tmp1*tmp10*tmp5*tmp9 - 5&
                      &12*ss*tmp1*tmp10*tmp5*tmp9 - 256*discbtme*ss*tmp1*tmp10*tmp5*tmp9 + 256*me2*tmp1&
                      &*tmp11*tmp5*tmp9 + 128*discbtme*me2*tmp1*tmp11*tmp5*tmp9 + 256*me2*tmp1*tmp12*tm&
                      &p5*tmp9 + 128*discbtme*me2*tmp1*tmp12*tmp5*tmp9 + 512*mm2*s25*tmp10*tmp5*tmp6*tm&
                      &p9 + 256*discbtme*mm2*s25*tmp10*tmp5*tmp6*tmp9 - 512*s25*ss*tmp10*tmp5*tmp6*tmp9&
                      & - 256*discbtme*s25*ss*tmp10*tmp5*tmp6*tmp9 - (512*me2*mm2*ss*tmp1*tmp17*tmp20*t&
                      &mp9)/tmp7 - (256*discbtme*me2*mm2*ss*tmp1*tmp17*tmp20*tmp9)/tmp7 + (512*mm2*tmp1&
                      &*tmp10*tmp17*tmp20*tmp9)/tmp7 + (256*discbtme*mm2*tmp1*tmp10*tmp17*tmp20*tmp9)/t&
                      &mp7 - (512*ss*tmp1*tmp10*tmp17*tmp20*tmp9)/tmp7 - (256*discbtme*ss*tmp1*tmp10*tm&
                      &p17*tmp20*tmp9)/tmp7 + (256*me2*tmp1*tmp11*tmp17*tmp20*tmp9)/tmp7 + (128*discbtm&
                      &e*me2*tmp1*tmp11*tmp17*tmp20*tmp9)/tmp7 + (256*me2*tmp1*tmp12*tmp17*tmp20*tmp9)/&
                      &tmp7 + (128*discbtme*me2*tmp1*tmp12*tmp17*tmp20*tmp9)/tmp7 - 512*me2*mm2*s25*ss*&
                      &tmp17*tmp20*tmp7*tmp9 - 256*discbtme*me2*mm2*s25*ss*tmp17*tmp20*tmp7*tmp9 - (512&
                      &*me2*mm2*ss*tmp17*tmp20*tmp7*tmp9)/tmp1 - (256*discbtme*me2*mm2*ss*tmp17*tmp20*t&
                      &mp7*tmp9)/tmp1 + 512*mm2*s25*tmp10*tmp17*tmp20*tmp7*tmp9 + 256*discbtme*mm2*s25*&
                      &tmp10*tmp17*tmp20*tmp7*tmp9 - 512*s25*ss*tmp10*tmp17*tmp20*tmp7*tmp9 - 256*discb&
                      &tme*s25*ss*tmp10*tmp17*tmp20*tmp7*tmp9 + (512*mm2*tmp10*tmp17*tmp20*tmp7*tmp9)/t&
                      &mp1 + (256*discbtme*mm2*tmp10*tmp17*tmp20*tmp7*tmp9)/tmp1 - (512*ss*tmp10*tmp17*&
                      &tmp20*tmp7*tmp9)/tmp1 - (256*discbtme*ss*tmp10*tmp17*tmp20*tmp7*tmp9)/tmp1 + 256&
                      &*me2*s25*tmp11*tmp17*tmp20*tmp7*tmp9 + 128*discbtme*me2*s25*tmp11*tmp17*tmp20*tm&
                      &p7*tmp9 + (256*me2*tmp11*tmp17*tmp20*tmp7*tmp9)/tmp1 + (128*discbtme*me2*tmp11*t&
                      &mp17*tmp20*tmp7*tmp9)/tmp1 + 256*me2*s25*tmp12*tmp17*tmp20*tmp7*tmp9 + 128*discb&
                      &tme*me2*s25*tmp12*tmp17*tmp20*tmp7*tmp9 + (256*me2*tmp12*tmp17*tmp20*tmp7*tmp9)/&
                      &tmp1 + (128*discbtme*me2*tmp12*tmp17*tmp20*tmp7*tmp9)/tmp1 + 512*me2*mm2*ss*tmp5&
                      &*tmp7*tmp9 + 256*discbtme*me2*mm2*ss*tmp5*tmp7*tmp9 - 512*mm2*tmp10*tmp5*tmp7*tm&
                      &p9 - 256*discbtme*mm2*tmp10*tmp5*tmp7*tmp9 + 512*ss*tmp10*tmp5*tmp7*tmp9 + 256*d&
                      &iscbtme*ss*tmp10*tmp5*tmp7*tmp9 - 512*mm2*s25*tmp1*tmp10*tmp5*tmp7*tmp9 - 256*di&
                      &scbtme*mm2*s25*tmp1*tmp10*tmp5*tmp7*tmp9 + 512*s25*ss*tmp1*tmp10*tmp5*tmp7*tmp9 &
                      &+ 256*discbtme*s25*ss*tmp1*tmp10*tmp5*tmp7*tmp9 - 256*me2*tmp11*tmp5*tmp7*tmp9 -&
                      & 128*discbtme*me2*tmp11*tmp5*tmp7*tmp9 - 256*me2*tmp12*tmp5*tmp7*tmp9 - 128*disc&
                      &btme*me2*tmp12*tmp5*tmp7*tmp9 - 512*tmp17*tmp20*tmp8*tmp9 - 256*discbtme*tmp17*t&
                      &mp20*tmp8*tmp9 - 256*s25*tmp1*tmp17*tmp20*tmp8*tmp9 - 128*discbtme*s25*tmp1*tmp1&
                      &7*tmp20*tmp8*tmp9 + 256*tmp1*tmp5*tmp8*tmp9 + 128*discbtme*tmp1*tmp5*tmp8*tmp9 +&
                      & 512*s25*tmp5*tmp6*tmp8*tmp9 + 256*discbtme*s25*tmp5*tmp6*tmp8*tmp9 + (256*tmp1*&
                      &tmp17*tmp20*tmp8*tmp9)/tmp7 + (128*discbtme*tmp1*tmp17*tmp20*tmp8*tmp9)/tmp7 + 2&
                      &56*s25*tmp17*tmp20*tmp7*tmp8*tmp9 + 128*discbtme*s25*tmp17*tmp20*tmp7*tmp8*tmp9 &
                      &+ (256*tmp17*tmp20*tmp7*tmp8*tmp9)/tmp1 + (128*discbtme*tmp17*tmp20*tmp7*tmp8*tm&
                      &p9)/tmp1 - 256*tmp5*tmp7*tmp8*tmp9 - 128*discbtme*tmp5*tmp7*tmp8*tmp9 - 512*s25*&
                      &tmp1*tmp5*tmp7*tmp8*tmp9 - 256*discbtme*s25*tmp1*tmp5*tmp7*tmp8*tmp9

  END FUNCTION LBK11_NLP_EP_QE4

  FUNCTION LBK11_NLP_QE4(me2, mm2, ss, tt, s15, s25, s35)
  implicit none
  real LBK11_NLP_Qe4
  real(kind=prec) ::  me2
  real(kind=prec) ::  mm2
  real(kind=prec) ::  ss
  real(kind=prec) ::  tt
  real(kind=prec) ::  s15
  real(kind=prec) ::  s25
  real(kind=prec) ::  s35
  real tmp1, tmp2, tmp3, tmp4, tmp5
  real tmp6, tmp7, tmp8, tmp9, tmp10
  real tmp11

  tmp1 = 1/s15
  tmp2 = 1/s35
  tmp3 = 4*me2
  tmp4 = -tt
  tmp5 = tmp3 + tmp4
  tmp6 = 1/tmp5
  tmp7 = tmp1**2
  tmp8 = me2**2
  tmp9 = tt**(-2)
  tmp10 = me2**3
  tmp11 = 1/tt
  LBK11_NLP_Qe4 = -64*tmp1 - 32*logmume*tmp1 + 32*me2*scalarc0ir6tme*tmp1 - 32*mm2*scalarc0ir6tme*&
                   &tmp1 - 32*scalarc0ir6tme*ss*tmp1 - (32*scalarc0ir6tme*tmp1)/tmp11 - 64*me2*tmp1*&
                   &tmp11 - 32*logmume*me2*tmp1*tmp11 - 64*mm2*tmp1*tmp11 - 32*logmume*mm2*tmp1*tmp1&
                   &1 + 64*me2*mm2*scalarc0ir6tme*tmp1*tmp11 - 64*ss*tmp1*tmp11 - 32*logmume*ss*tmp1&
                   &*tmp11 + 64*me2*scalarc0ir6tme*ss*tmp1*tmp11 + 96*me2*scalarc0ir6tme*tmp2 + 96*m&
                   &m2*scalarc0ir6tme*tmp2 - 32*scalarc0ir6tme*ss*tmp2 - 64*s25*tmp1*tmp2 - 32*logmu&
                   &me*s25*tmp1*tmp2 + 192*me2*s25*scalarc0ir6tme*tmp1*tmp2 + 64*mm2*s25*scalarc0ir6&
                   &tme*tmp1*tmp2 - 64*s25*scalarc0ir6tme*ss*tmp1*tmp2 - (32*s25*scalarc0ir6tme*tmp1&
                   &*tmp2)/tmp11 + 192*me2*tmp11*tmp2 + 96*logmume*me2*tmp11*tmp2 + 192*mm2*tmp11*tm&
                   &p2 + 96*logmume*mm2*tmp11*tmp2 - 192*me2*mm2*scalarc0ir6tme*tmp11*tmp2 - 64*ss*t&
                   &mp11*tmp2 - 32*logmume*ss*tmp11*tmp2 + 64*me2*scalarc0ir6tme*ss*tmp11*tmp2 + 256&
                   &*me2*s25*tmp1*tmp11*tmp2 + 128*logmume*me2*s25*tmp1*tmp11*tmp2 + 128*mm2*s25*tmp&
                   &1*tmp11*tmp2 + 64*logmume*mm2*s25*tmp1*tmp11*tmp2 - 256*me2*mm2*s25*scalarc0ir6t&
                   &me*tmp1*tmp11*tmp2 - 128*s25*ss*tmp1*tmp11*tmp2 - 64*logmume*s25*ss*tmp1*tmp11*t&
                   &mp2 + 256*me2*s25*scalarc0ir6tme*ss*tmp1*tmp11*tmp2 - 144*discbtme*me2*tmp1*tmp6&
                   & - 32*discbtme*logmume*me2*tmp1*tmp6 + 48*discbtme*mm2*tmp1*tmp6 + 32*discbtme*l&
                   &ogmume*mm2*tmp1*tmp6 + 48*discbtme*ss*tmp1*tmp6 + 32*discbtme*logmume*ss*tmp1*tm&
                   &p6 + (48*discbtme*tmp1*tmp6)/tmp11 + (32*discbtme*logmume*tmp1*tmp6)/tmp11 - 256&
                   &*discbtme*me2*mm2*tmp1*tmp11*tmp6 - 64*discbtme*logmume*me2*mm2*tmp1*tmp11*tmp6 &
                   &- 128*discbtme*me2*ss*tmp1*tmp11*tmp6 - 64*discbtme*logmume*me2*ss*tmp1*tmp11*tm&
                   &p6 - 80*discbtme*me2*tmp2*tmp6 - 96*discbtme*logmume*me2*tmp2*tmp6 - 144*discbtm&
                   &e*mm2*tmp2*tmp6 - 96*discbtme*logmume*mm2*tmp2*tmp6 + 48*discbtme*ss*tmp2*tmp6 +&
                   & 32*discbtme*logmume*ss*tmp2*tmp6 - 320*discbtme*me2*s25*tmp1*tmp2*tmp6 - 192*di&
                   &scbtme*logmume*me2*s25*tmp1*tmp2*tmp6 - 96*discbtme*mm2*s25*tmp1*tmp2*tmp6 - 64*&
                   &discbtme*logmume*mm2*s25*tmp1*tmp2*tmp6 + 96*discbtme*s25*ss*tmp1*tmp2*tmp6 + 64&
                   &*discbtme*logmume*s25*ss*tmp1*tmp2*tmp6 + (48*discbtme*s25*tmp1*tmp2*tmp6)/tmp11&
                   & + (32*discbtme*logmume*s25*tmp1*tmp2*tmp6)/tmp11 + 512*discbtme*me2*mm2*tmp11*t&
                   &mp2*tmp6 + 192*discbtme*logmume*me2*mm2*tmp11*tmp2*tmp6 - 128*discbtme*me2*ss*tm&
                   &p11*tmp2*tmp6 - 64*discbtme*logmume*me2*ss*tmp11*tmp2*tmp6 + 448*discbtme*me2*mm&
                   &2*s25*tmp1*tmp11*tmp2*tmp6 + 256*discbtme*logmume*me2*mm2*s25*tmp1*tmp11*tmp2*tm&
                   &p6 - 448*discbtme*me2*s25*ss*tmp1*tmp11*tmp2*tmp6 - 256*discbtme*logmume*me2*s25&
                   &*ss*tmp1*tmp11*tmp2*tmp6 - 64*me2*s25*scalarc0ir6tme*tmp7 - 128*me2*s25*tmp11*tm&
                   &p7 - 64*logmume*me2*s25*tmp11*tmp7 + 128*me2*mm2*s25*scalarc0ir6tme*tmp11*tmp7 -&
                   & 128*me2*s25*scalarc0ir6tme*ss*tmp11*tmp7 + 96*discbtme*me2*s25*tmp6*tmp7 + 64*d&
                   &iscbtme*logmume*me2*s25*tmp6*tmp7 - 192*discbtme*me2*mm2*s25*tmp11*tmp6*tmp7 - 1&
                   &28*discbtme*logmume*me2*mm2*s25*tmp11*tmp6*tmp7 + 192*discbtme*me2*s25*ss*tmp11*&
                   &tmp6*tmp7 + 128*discbtme*logmume*me2*s25*ss*tmp11*tmp6*tmp7 + 64*scalarc0ir6tme*&
                   &tmp1*tmp11*tmp8 - 192*scalarc0ir6tme*tmp11*tmp2*tmp8 - 384*s25*scalarc0ir6tme*tm&
                   &p1*tmp11*tmp2*tmp8 - 128*discbtme*tmp1*tmp11*tmp6*tmp8 - 64*discbtme*logmume*tmp&
                   &1*tmp11*tmp6*tmp8 + 384*discbtme*tmp11*tmp2*tmp6*tmp8 + 192*discbtme*logmume*tmp&
                   &11*tmp2*tmp6*tmp8 + 704*discbtme*s25*tmp1*tmp11*tmp2*tmp6*tmp8 + 384*discbtme*lo&
                   &gmume*s25*tmp1*tmp11*tmp2*tmp6*tmp8 + 256*s25*scalarc0ir6tme*tmp11*tmp7*tmp8 - 4&
                   &48*discbtme*s25*tmp11*tmp6*tmp7*tmp8 - 256*discbtme*logmume*s25*tmp11*tmp6*tmp7*&
                   &tmp8 - 256*me2*mm2*s25*tmp1*tmp2*tmp9 - 128*logmume*me2*mm2*s25*tmp1*tmp2*tmp9 +&
                   & 256*me2*s25*ss*tmp1*tmp2*tmp9 + 128*logmume*me2*s25*ss*tmp1*tmp2*tmp9 + 256*s25&
                   &*scalarc0ir6tme*tmp1*tmp10*tmp2*tmp9 - 512*discbtme*s25*tmp1*tmp10*tmp2*tmp6*tmp&
                   &9 - 256*discbtme*logmume*s25*tmp1*tmp10*tmp2*tmp6*tmp9 + 256*me2*mm2*s25*tmp7*tm&
                   &p9 + 128*logmume*me2*mm2*s25*tmp7*tmp9 - 256*me2*s25*ss*tmp7*tmp9 - 128*logmume*&
                   &me2*s25*ss*tmp7*tmp9 - 256*s25*scalarc0ir6tme*tmp10*tmp7*tmp9 + 512*discbtme*s25&
                   &*tmp10*tmp6*tmp7*tmp9 + 256*discbtme*logmume*s25*tmp10*tmp6*tmp7*tmp9 - 256*s25*&
                   &tmp1*tmp2*tmp8*tmp9 - 128*logmume*s25*tmp1*tmp2*tmp8*tmp9 + 256*mm2*s25*scalarc0&
                   &ir6tme*tmp1*tmp2*tmp8*tmp9 - 256*s25*scalarc0ir6tme*ss*tmp1*tmp2*tmp8*tmp9 - 512&
                   &*discbtme*mm2*s25*tmp1*tmp2*tmp6*tmp8*tmp9 - 256*discbtme*logmume*mm2*s25*tmp1*t&
                   &mp2*tmp6*tmp8*tmp9 + 512*discbtme*s25*ss*tmp1*tmp2*tmp6*tmp8*tmp9 + 256*discbtme&
                   &*logmume*s25*ss*tmp1*tmp2*tmp6*tmp8*tmp9 + 256*s25*tmp7*tmp8*tmp9 + 128*logmume*&
                   &s25*tmp7*tmp8*tmp9 - 256*mm2*s25*scalarc0ir6tme*tmp7*tmp8*tmp9 + 256*s25*scalarc&
                   &0ir6tme*ss*tmp7*tmp8*tmp9 + 512*discbtme*mm2*s25*tmp6*tmp7*tmp8*tmp9 + 256*discb&
                   &tme*logmume*mm2*s25*tmp6*tmp7*tmp8*tmp9 - 512*discbtme*s25*ss*tmp6*tmp7*tmp8*tmp&
                   &9 - 256*discbtme*logmume*s25*ss*tmp6*tmp7*tmp8*tmp9

  END FUNCTION LBK11_NLP_QE4

  FUNCTION LBK12_LP_EP_QE4(me2, mm2, ss, tt, s15, s25, s35)
  implicit none
  real LBK12_LP_EP_Qe4
  real(kind=prec) ::  me2
  real(kind=prec) ::  mm2
  real(kind=prec) ::  ss
  real(kind=prec) ::  tt
  real(kind=prec) ::  s15
  real(kind=prec) ::  s25
  real(kind=prec) ::  s35
  real tmp1, tmp2, tmp3, tmp4, tmp5
  real tmp6, tmp7, tmp8, tmp9, tmp10
  real tmp11, tmp12

  tmp1 = 1/s15
  tmp2 = 1/s35
  tmp3 = 2*me2
  tmp4 = -tt
  tmp5 = tmp3 + tt
  tmp6 = 1/tt
  tmp7 = tmp1**2
  tmp8 = tmp3 + tmp4
  tmp9 = 4*me2
  tmp10 = tmp4 + tmp9
  tmp11 = 1/tmp10
  tmp12 = tmp2**2
  LBK12_LP_EP_Qe4 = 64*tmp1*tmp2*tmp5 + 64*me2*tmp12*tmp5*tmp6 - 128*me2*tmp1*tmp2*tmp5*tmp6 + 64*me&
                     &2*tmp5*tmp6*tmp7 + 64*discbtme*tmp1*tmp11*tmp2*tmp5*tmp8 + 64*discbtme*me2*tmp11&
                     &*tmp12*tmp5*tmp6*tmp8 - 128*discbtme*me2*tmp1*tmp11*tmp2*tmp5*tmp6*tmp8 + 64*dis&
                     &cbtme*me2*tmp11*tmp5*tmp6*tmp7*tmp8

  END FUNCTION LBK12_LP_EP_QE4

  FUNCTION LBK12_LP_QE4(me2, mm2, ss, tt, s15, s25, s35)
  implicit none
  real LBK12_LP_Qe4
  real(kind=prec) ::  me2
  real(kind=prec) ::  mm2
  real(kind=prec) ::  ss
  real(kind=prec) ::  tt
  real(kind=prec) ::  s15
  real(kind=prec) ::  s25
  real(kind=prec) ::  s35
  real tmp1, tmp2, tmp3, tmp4, tmp5
  real tmp6, tmp7, tmp8, tmp9, tmp10
  real tmp11, tmp12, tmp13, tmp14, tmp15
  real tmp16, tmp17, tmp18, tmp19, tmp20

  tmp1 = 1/s15
  tmp2 = 1/s35
  tmp3 = 2*me2
  tmp4 = tmp3 + tt
  tmp5 = -tt
  tmp6 = tmp1**2
  tmp7 = 1/tt
  tmp8 = tmp2**2
  tmp9 = tmp3 + tmp5
  tmp10 = 4*me2
  tmp11 = tmp10 + tmp5
  tmp12 = 1/tmp11
  tmp13 = -2*me2
  tmp14 = tmp13 + 1/tmp7
  tmp15 = me2**2
  tmp16 = 16*tmp15
  tmp17 = (8*me2)/tmp7
  tmp18 = tmp7**(-2)
  tmp19 = -3*tmp18
  tmp20 = tmp16 + tmp17 + tmp19
  LBK12_LP_Qe4 = 32*discbtme*tmp1*tmp12*tmp2*tmp20 + 128*tmp1*tmp2*tmp4 + 64*logmume*tmp1*tmp2*tm&
                  &p4 + 64*scalarc0ir6tme*tmp1*tmp14*tmp2*tmp4 - 64*discbtme*me2*tmp1*tmp12*tmp2*tm&
                  &p20*tmp7 - 256*me2*tmp1*tmp2*tmp4*tmp7 - 128*logmume*me2*tmp1*tmp2*tmp4*tmp7 - 1&
                  &28*me2*scalarc0ir6tme*tmp1*tmp14*tmp2*tmp4*tmp7 + 32*discbtme*me2*tmp12*tmp20*tm&
                  &p6*tmp7 + 128*me2*tmp4*tmp6*tmp7 + 64*logmume*me2*tmp4*tmp6*tmp7 + 64*me2*scalar&
                  &c0ir6tme*tmp14*tmp4*tmp6*tmp7 + 32*discbtme*me2*tmp12*tmp20*tmp7*tmp8 + 128*me2*&
                  &tmp4*tmp7*tmp8 + 64*logmume*me2*tmp4*tmp7*tmp8 + 64*me2*scalarc0ir6tme*tmp14*tmp&
                  &4*tmp7*tmp8 + 64*discbtme*logmume*tmp1*tmp12*tmp2*tmp4*tmp9 - 128*discbtme*logmu&
                  &me*me2*tmp1*tmp12*tmp2*tmp4*tmp7*tmp9 + 64*discbtme*logmume*me2*tmp12*tmp4*tmp6*&
                  &tmp7*tmp9 + 64*discbtme*logmume*me2*tmp12*tmp4*tmp7*tmp8*tmp9

  END FUNCTION LBK12_LP_QE4

  FUNCTION LBK12_NLP_EP_QE4(me2, mm2, ss, tt, s15, s25, s35)
  implicit none
  real LBK12_NLP_EP_Qe4
  real(kind=prec) ::  me2
  real(kind=prec) ::  mm2
  real(kind=prec) ::  ss
  real(kind=prec) ::  tt
  real(kind=prec) ::  s15
  real(kind=prec) ::  s25
  real(kind=prec) ::  s35
  real tmp1, tmp2, tmp3, tmp4, tmp5
  real tmp6, tmp7, tmp8, tmp9, tmp10
  real tmp11, tmp12, tmp13, tmp14, tmp15
  real tmp16

  tmp1 = 1/mm2
  tmp2 = -s15
  tmp3 = -s25
  tmp4 = s35 + tmp2 + tmp3
  tmp5 = 1/tmp4
  tmp6 = tt**(-2)
  tmp7 = -4*me2
  tmp8 = tmp7 + tt
  tmp9 = 1/tmp8
  tmp10 = (8*me2*tt)/tmp1
  tmp11 = 1/tmp6
  tmp12 = (4*tmp11)/tmp1
  tmp13 = tmp10 + tmp12
  tmp14 = 1/s15
  tmp15 = 1/s35
  tmp16 = 1/tt
  LBK12_NLP_EP_Qe4 = 32*tmp1*tmp13*tmp16*tmp5*tmp9 + 32*discbtme*tmp1*tmp13*tmp16*tmp5*tmp9 + 16*s25*&
                      &tmp1*tmp13*tmp14*tmp16*tmp5*tmp9 + 16*discbtme*s25*tmp1*tmp13*tmp14*tmp16*tmp5*t&
                      &mp9 - (16*tmp1*tmp13*tmp14*tmp16*tmp5*tmp9)/tmp15 - (16*discbtme*tmp1*tmp13*tmp1&
                      &4*tmp16*tmp5*tmp9)/tmp15 - 16*s25*tmp1*tmp13*tmp15*tmp16*tmp5*tmp9 - 16*discbtme&
                      &*s25*tmp1*tmp13*tmp15*tmp16*tmp5*tmp9 - (16*tmp1*tmp13*tmp15*tmp16*tmp5*tmp9)/tm&
                      &p14 - (16*discbtme*tmp1*tmp13*tmp15*tmp16*tmp5*tmp9)/tmp14 - 128*me2*tmp1*tmp13*&
                      &tmp5*tmp6*tmp9 - 64*discbtme*me2*tmp1*tmp13*tmp5*tmp6*tmp9 - 64*me2*s25*tmp1*tmp&
                      &13*tmp14*tmp5*tmp6*tmp9 - 32*discbtme*me2*s25*tmp1*tmp13*tmp14*tmp5*tmp6*tmp9 + &
                      &(64*me2*tmp1*tmp13*tmp14*tmp5*tmp6*tmp9)/tmp15 + (32*discbtme*me2*tmp1*tmp13*tmp&
                      &14*tmp5*tmp6*tmp9)/tmp15 + 64*me2*s25*tmp1*tmp13*tmp15*tmp5*tmp6*tmp9 + 32*discb&
                      &tme*me2*s25*tmp1*tmp13*tmp15*tmp5*tmp6*tmp9 + (64*me2*tmp1*tmp13*tmp15*tmp5*tmp6&
                      &*tmp9)/tmp14 + (32*discbtme*me2*tmp1*tmp13*tmp15*tmp5*tmp6*tmp9)/tmp14

  END FUNCTION LBK12_NLP_EP_QE4

  FUNCTION LBK12_NLP_QE4(me2, mm2, ss, tt, s15, s25, s35)
  implicit none
  real LBK12_NLP_Qe4
  real(kind=prec) ::  me2
  real(kind=prec) ::  mm2
  real(kind=prec) ::  ss
  real(kind=prec) ::  tt
  real(kind=prec) ::  s15
  real(kind=prec) ::  s25
  real(kind=prec) ::  s35
  real tmp1, tmp2, tmp3, tmp4, tmp5
  real tmp6, tmp7, tmp8, tmp9, tmp10
  real tmp11

  tmp1 = 1/s15
  tmp2 = -s35
  tmp3 = 1/tmp1 + tmp2
  tmp4 = 1/s35
  tmp5 = 1/tt
  tmp6 = 2*me2
  tmp7 = 1/tmp5 + tmp6
  tmp8 = -(1/tmp5)
  tmp9 = 4*me2
  tmp10 = tmp8 + tmp9
  tmp11 = 1/tmp10
  LBK12_NLP_Qe4 = 32*discbtme*tmp1*tmp11*tmp3*tmp4*(16*me2**2 - 3/tmp5**2 + (8*me2)/tmp5)*tmp5 + 1&
                   &28*tmp1*tmp3*tmp4*tmp5*tmp7 + 64*logmume*tmp1*tmp3*tmp4*tmp5*tmp7 + 64*scalarc0i&
                   &r6tme*tmp1*tmp3*tmp4*(-2*me2 + 1/tmp5)*tmp5*tmp7 + 64*discbtme*logmume*tmp1*tmp1&
                   &1*tmp3*tmp4*tmp5*tmp7*(tmp6 + tmp8)

  END FUNCTION LBK12_NLP_QE4

  FUNCTION LBK22_LP_EP_QE4(me2, mm2, ss, tt, s15, s25, s35)
  implicit none
  real LBK22_LP_EP_Qe4
  real(kind=prec) ::  me2
  real(kind=prec) ::  mm2
  real(kind=prec) ::  ss
  real(kind=prec) ::  tt
  real(kind=prec) ::  s15
  real(kind=prec) ::  s25
  real(kind=prec) ::  s35
  real tmp1, tmp2, tmp3, tmp4, tmp5
  real tmp6, tmp7, tmp8, tmp9, tmp10
  real tmp11, tmp12, tmp13, tmp14, tmp15

  tmp1 = s15**(-2)
  tmp2 = me2**2
  tmp3 = 1/mm2
  tmp4 = s35**(-2)
  tmp5 = 1/s15
  tmp6 = 1/s35
  tmp7 = 4*me2
  tmp8 = -tt
  tmp9 = tmp7 + tmp8
  tmp10 = 1/tmp9
  tmp11 = me2**3
  tmp12 = ss**2
  tmp13 = 1/tt
  tmp14 = me2**4
  tmp15 = tmp13**(-2)
  LBK22_LP_EP_Qe4 = 32*me2*tmp1 - 32*discbtme*me2*ss*tmp1*tmp10 - (32*discbtme*me2*tmp1*tmp10)/tmp13&
                     & + 32*me2*ss*tmp1*tmp13 + 64*discbtme*tmp1*tmp10*tmp11*tmp13 + 32*discbtme*tmp1*&
                     &tmp10*tmp2 + 32*tmp1*tmp13*tmp2 + 64*discbtme*ss*tmp1*tmp10*tmp13*tmp2 + (16*dis&
                     &cbtme*me2*tmp1*tmp10)/tmp3 - (16*me2*tmp1*tmp13)/tmp3 - (32*discbtme*tmp1*tmp10*&
                     &tmp13*tmp2)/tmp3 - 16*me2*ss*tmp1*tmp3 + 48*discbtme*tmp1*tmp10*tmp11*tmp3 + 16*&
                     &discbtme*me2*tmp1*tmp10*tmp12*tmp3 + (16*discbtme*me2*ss*tmp1*tmp10*tmp3)/tmp13 &
                     &- 16*tmp1*tmp11*tmp13*tmp3 + 64*discbtme*ss*tmp1*tmp10*tmp11*tmp13*tmp3 - 16*me2&
                     &*tmp1*tmp12*tmp13*tmp3 - 32*discbtme*tmp1*tmp10*tmp13*tmp14*tmp3 + 16*tmp1*tmp2*&
                     &tmp3 - 64*discbtme*ss*tmp1*tmp10*tmp2*tmp3 - (16*discbtme*tmp1*tmp10*tmp2*tmp3)/&
                     &tmp13 + 32*ss*tmp1*tmp13*tmp2*tmp3 - 32*discbtme*tmp1*tmp10*tmp12*tmp13*tmp2*tmp&
                     &3 + 32*me2*tmp4 - 32*discbtme*me2*ss*tmp10*tmp4 - (32*discbtme*me2*tmp10*tmp4)/t&
                     &mp13 + 32*me2*ss*tmp13*tmp4 + 64*discbtme*tmp10*tmp11*tmp13*tmp4 + 32*discbtme*t&
                     &mp10*tmp2*tmp4 + 32*tmp13*tmp2*tmp4 + 64*discbtme*ss*tmp10*tmp13*tmp2*tmp4 + (16&
                     &*discbtme*me2*tmp10*tmp4)/tmp3 - (16*me2*tmp13*tmp4)/tmp3 - (32*discbtme*tmp10*t&
                     &mp13*tmp2*tmp4)/tmp3 - 16*me2*ss*tmp3*tmp4 + 48*discbtme*tmp10*tmp11*tmp3*tmp4 +&
                     & 16*discbtme*me2*tmp10*tmp12*tmp3*tmp4 + (16*discbtme*me2*ss*tmp10*tmp3*tmp4)/tm&
                     &p13 - 16*tmp11*tmp13*tmp3*tmp4 + 64*discbtme*ss*tmp10*tmp11*tmp13*tmp3*tmp4 - 16&
                     &*me2*tmp12*tmp13*tmp3*tmp4 - 32*discbtme*tmp10*tmp13*tmp14*tmp3*tmp4 + 16*tmp2*t&
                     &mp3*tmp4 - 64*discbtme*ss*tmp10*tmp2*tmp3*tmp4 - (16*discbtme*tmp10*tmp2*tmp3*tm&
                     &p4)/tmp13 + 32*ss*tmp13*tmp2*tmp3*tmp4 - 32*discbtme*tmp10*tmp12*tmp13*tmp2*tmp3&
                     &*tmp4 - 32*me2*tmp5*tmp6 + 32*ss*tmp5*tmp6 + 128*discbtme*me2*ss*tmp10*tmp5*tmp6&
                     & + (32*tmp5*tmp6)/tmp13 + (96*discbtme*me2*tmp10*tmp5*tmp6)/tmp13 - (32*discbtme&
                     &*ss*tmp10*tmp5*tmp6)/tmp13 - 64*me2*ss*tmp13*tmp5*tmp6 - 128*discbtme*tmp10*tmp1&
                     &1*tmp13*tmp5*tmp6 - 32*discbtme*tmp10*tmp15*tmp5*tmp6 - 64*tmp13*tmp2*tmp5*tmp6 &
                     &- 128*discbtme*ss*tmp10*tmp13*tmp2*tmp5*tmp6 - (16*tmp5*tmp6)/tmp3 - (64*discbtm&
                     &e*me2*tmp10*tmp5*tmp6)/tmp3 + (16*discbtme*tmp10*tmp5*tmp6)/(tmp13*tmp3) + (32*m&
                     &e2*tmp13*tmp5*tmp6)/tmp3 + (64*discbtme*tmp10*tmp13*tmp2*tmp5*tmp6)/tmp3 + 64*me&
                     &2*ss*tmp3*tmp5*tmp6 - 128*discbtme*tmp10*tmp11*tmp3*tmp5*tmp6 - 16*tmp12*tmp3*tm&
                     &p5*tmp6 - 64*discbtme*me2*tmp10*tmp12*tmp3*tmp5*tmp6 + (16*me2*tmp3*tmp5*tmp6)/t&
                     &mp13 - (16*ss*tmp3*tmp5*tmp6)/tmp13 - (96*discbtme*me2*ss*tmp10*tmp3*tmp5*tmp6)/&
                     &tmp13 + (16*discbtme*tmp10*tmp12*tmp3*tmp5*tmp6)/tmp13 + 32*tmp11*tmp13*tmp3*tmp&
                     &5*tmp6 - 128*discbtme*ss*tmp10*tmp11*tmp13*tmp3*tmp5*tmp6 + 32*me2*tmp12*tmp13*t&
                     &mp3*tmp5*tmp6 + 64*discbtme*tmp10*tmp13*tmp14*tmp3*tmp5*tmp6 - 16*discbtme*me2*t&
                     &mp10*tmp15*tmp3*tmp5*tmp6 + 16*discbtme*ss*tmp10*tmp15*tmp3*tmp5*tmp6 - 48*tmp2*&
                     &tmp3*tmp5*tmp6 + 192*discbtme*ss*tmp10*tmp2*tmp3*tmp5*tmp6 + (80*discbtme*tmp10*&
                     &tmp2*tmp3*tmp5*tmp6)/tmp13 - 64*ss*tmp13*tmp2*tmp3*tmp5*tmp6 + 64*discbtme*tmp10&
                     &*tmp12*tmp13*tmp2*tmp3*tmp5*tmp6

  END FUNCTION LBK22_LP_EP_QE4

  FUNCTION LBK22_LP_QE4(me2, mm2, ss, tt, s15, s25, s35)
  implicit none
  real LBK22_LP_Qe4
  real(kind=prec) ::  me2
  real(kind=prec) ::  mm2
  real(kind=prec) ::  ss
  real(kind=prec) ::  tt
  real(kind=prec) ::  s15
  real(kind=prec) ::  s25
  real(kind=prec) ::  s35
  real tmp1, tmp2, tmp3, tmp4, tmp5
  real tmp6, tmp7, tmp8, tmp9, tmp10
  real tmp11, tmp12, tmp13, tmp14, tmp15

  tmp1 = s15**(-2)
  tmp2 = me2**2
  tmp3 = 1/mm2
  tmp4 = s35**(-2)
  tmp5 = 1/s15
  tmp6 = 1/s35
  tmp7 = me2**3
  tmp8 = ss**2
  tmp9 = 4*me2
  tmp10 = -tt
  tmp11 = tmp10 + tmp9
  tmp12 = 1/tmp11
  tmp13 = 1/tt
  tmp14 = me2**4
  tmp15 = tmp13**(-2)
  LBK22_LP_Qe4 = 64*me2*tmp1 + 32*logmume*me2*tmp1 + 32*me2*scalarc0ir6tme*ss*tmp1 - 48*discbtme*&
                  &me2*ss*tmp1*tmp12 - 32*discbtme*logmume*me2*ss*tmp1*tmp12 + (32*me2*scalarc0ir6t&
                  &me*tmp1)/tmp13 - (48*discbtme*me2*tmp1*tmp12)/tmp13 - (32*discbtme*logmume*me2*t&
                  &mp1*tmp12)/tmp13 + 64*me2*ss*tmp1*tmp13 + 32*logmume*me2*ss*tmp1*tmp13 - 32*scal&
                  &arc0ir6tme*tmp1*tmp2 + 144*discbtme*tmp1*tmp12*tmp2 + 32*discbtme*logmume*tmp1*t&
                  &mp12*tmp2 + 64*tmp1*tmp13*tmp2 + 32*logmume*tmp1*tmp13*tmp2 - 64*scalarc0ir6tme*&
                  &ss*tmp1*tmp13*tmp2 + 128*discbtme*ss*tmp1*tmp12*tmp13*tmp2 + 64*discbtme*logmume&
                  &*ss*tmp1*tmp12*tmp13*tmp2 - (16*me2*scalarc0ir6tme*tmp1)/tmp3 + (24*discbtme*me2&
                  &*tmp1*tmp12)/tmp3 + (16*discbtme*logmume*me2*tmp1*tmp12)/tmp3 - (32*me2*tmp1*tmp&
                  &13)/tmp3 - (16*logmume*me2*tmp1*tmp13)/tmp3 + (32*scalarc0ir6tme*tmp1*tmp13*tmp2&
                  &)/tmp3 - (64*discbtme*tmp1*tmp12*tmp13*tmp2)/tmp3 - (32*discbtme*logmume*tmp1*tm&
                  &p12*tmp13*tmp2)/tmp3 - 32*me2*ss*tmp1*tmp3 - 16*logmume*me2*ss*tmp1*tmp3 - (16*m&
                  &e2*scalarc0ir6tme*ss*tmp1*tmp3)/tmp13 + (24*discbtme*me2*ss*tmp1*tmp12*tmp3)/tmp&
                  &13 + (16*discbtme*logmume*me2*ss*tmp1*tmp12*tmp3)/tmp13 + 32*scalarc0ir6tme*tmp1&
                  &*tmp13*tmp14*tmp3 - 64*discbtme*tmp1*tmp12*tmp13*tmp14*tmp3 - 32*discbtme*logmum&
                  &e*tmp1*tmp12*tmp13*tmp14*tmp3 + 32*tmp1*tmp2*tmp3 + 16*logmume*tmp1*tmp2*tmp3 + &
                  &64*scalarc0ir6tme*ss*tmp1*tmp2*tmp3 - 112*discbtme*ss*tmp1*tmp12*tmp2*tmp3 - 64*&
                  &discbtme*logmume*ss*tmp1*tmp12*tmp2*tmp3 + (16*scalarc0ir6tme*tmp1*tmp2*tmp3)/tm&
                  &p13 - (16*discbtme*tmp1*tmp12*tmp2*tmp3)/tmp13 - (16*discbtme*logmume*tmp1*tmp12&
                  &*tmp2*tmp3)/tmp13 + 64*ss*tmp1*tmp13*tmp2*tmp3 + 32*logmume*ss*tmp1*tmp13*tmp2*t&
                  &mp3 + 64*me2*tmp4 + 32*logmume*me2*tmp4 + 32*me2*scalarc0ir6tme*ss*tmp4 - 48*dis&
                  &cbtme*me2*ss*tmp12*tmp4 - 32*discbtme*logmume*me2*ss*tmp12*tmp4 + (32*me2*scalar&
                  &c0ir6tme*tmp4)/tmp13 - (48*discbtme*me2*tmp12*tmp4)/tmp13 - (32*discbtme*logmume&
                  &*me2*tmp12*tmp4)/tmp13 + 64*me2*ss*tmp13*tmp4 + 32*logmume*me2*ss*tmp13*tmp4 - 3&
                  &2*scalarc0ir6tme*tmp2*tmp4 + 144*discbtme*tmp12*tmp2*tmp4 + 32*discbtme*logmume*&
                  &tmp12*tmp2*tmp4 + 64*tmp13*tmp2*tmp4 + 32*logmume*tmp13*tmp2*tmp4 - 64*scalarc0i&
                  &r6tme*ss*tmp13*tmp2*tmp4 + 128*discbtme*ss*tmp12*tmp13*tmp2*tmp4 + 64*discbtme*l&
                  &ogmume*ss*tmp12*tmp13*tmp2*tmp4 - (16*me2*scalarc0ir6tme*tmp4)/tmp3 + (24*discbt&
                  &me*me2*tmp12*tmp4)/tmp3 + (16*discbtme*logmume*me2*tmp12*tmp4)/tmp3 - (32*me2*tm&
                  &p13*tmp4)/tmp3 - (16*logmume*me2*tmp13*tmp4)/tmp3 + (32*scalarc0ir6tme*tmp13*tmp&
                  &2*tmp4)/tmp3 - (64*discbtme*tmp12*tmp13*tmp2*tmp4)/tmp3 - (32*discbtme*logmume*t&
                  &mp12*tmp13*tmp2*tmp4)/tmp3 - 32*me2*ss*tmp3*tmp4 - 16*logmume*me2*ss*tmp3*tmp4 -&
                  & (16*me2*scalarc0ir6tme*ss*tmp3*tmp4)/tmp13 + (24*discbtme*me2*ss*tmp12*tmp3*tmp&
                  &4)/tmp13 + (16*discbtme*logmume*me2*ss*tmp12*tmp3*tmp4)/tmp13 + 32*scalarc0ir6tm&
                  &e*tmp13*tmp14*tmp3*tmp4 - 64*discbtme*tmp12*tmp13*tmp14*tmp3*tmp4 - 32*discbtme*&
                  &logmume*tmp12*tmp13*tmp14*tmp3*tmp4 + 32*tmp2*tmp3*tmp4 + 16*logmume*tmp2*tmp3*t&
                  &mp4 + 64*scalarc0ir6tme*ss*tmp2*tmp3*tmp4 - 112*discbtme*ss*tmp12*tmp2*tmp3*tmp4&
                  & - 64*discbtme*logmume*ss*tmp12*tmp2*tmp3*tmp4 + (16*scalarc0ir6tme*tmp2*tmp3*tm&
                  &p4)/tmp13 - (16*discbtme*tmp12*tmp2*tmp3*tmp4)/tmp13 - (16*discbtme*logmume*tmp1&
                  &2*tmp2*tmp3*tmp4)/tmp13 + 64*ss*tmp13*tmp2*tmp3*tmp4 + 32*logmume*ss*tmp13*tmp2*&
                  &tmp3*tmp4 - 64*me2*tmp5*tmp6 - 32*logmume*me2*tmp5*tmp6 + 64*ss*tmp5*tmp6 + 32*l&
                  &ogmume*ss*tmp5*tmp6 - 128*me2*scalarc0ir6tme*ss*tmp5*tmp6 + 224*discbtme*me2*ss*&
                  &tmp12*tmp5*tmp6 + 128*discbtme*logmume*me2*ss*tmp12*tmp5*tmp6 + (64*tmp5*tmp6)/t&
                  &mp13 + (32*logmume*tmp5*tmp6)/tmp13 - (96*me2*scalarc0ir6tme*tmp5*tmp6)/tmp13 + &
                  &(32*scalarc0ir6tme*ss*tmp5*tmp6)/tmp13 + (240*discbtme*me2*tmp12*tmp5*tmp6)/tmp1&
                  &3 + (96*discbtme*logmume*me2*tmp12*tmp5*tmp6)/tmp13 - (48*discbtme*ss*tmp12*tmp5&
                  &*tmp6)/tmp13 - (32*discbtme*logmume*ss*tmp12*tmp5*tmp6)/tmp13 - 128*me2*ss*tmp13&
                  &*tmp5*tmp6 - 64*logmume*me2*ss*tmp13*tmp5*tmp6 + 32*scalarc0ir6tme*tmp15*tmp5*tm&
                  &p6 - 48*discbtme*tmp12*tmp15*tmp5*tmp6 - 32*discbtme*logmume*tmp12*tmp15*tmp5*tm&
                  &p6 - 160*discbtme*tmp12*tmp2*tmp5*tmp6 - 128*tmp13*tmp2*tmp5*tmp6 - 64*logmume*t&
                  &mp13*tmp2*tmp5*tmp6 + 128*scalarc0ir6tme*ss*tmp13*tmp2*tmp5*tmp6 - 256*discbtme*&
                  &ss*tmp12*tmp13*tmp2*tmp5*tmp6 - 128*discbtme*logmume*ss*tmp12*tmp13*tmp2*tmp5*tm&
                  &p6 - (32*tmp5*tmp6)/tmp3 - (16*logmume*tmp5*tmp6)/tmp3 + (64*me2*scalarc0ir6tme*&
                  &tmp5*tmp6)/tmp3 - (112*discbtme*me2*tmp12*tmp5*tmp6)/tmp3 - (64*discbtme*logmume&
                  &*me2*tmp12*tmp5*tmp6)/tmp3 - (16*scalarc0ir6tme*tmp5*tmp6)/(tmp13*tmp3) + (24*di&
                  &scbtme*tmp12*tmp5*tmp6)/(tmp13*tmp3) + (16*discbtme*logmume*tmp12*tmp5*tmp6)/(tm&
                  &p13*tmp3) + (64*me2*tmp13*tmp5*tmp6)/tmp3 + (32*logmume*me2*tmp13*tmp5*tmp6)/tmp&
                  &3 - (64*scalarc0ir6tme*tmp13*tmp2*tmp5*tmp6)/tmp3 + (128*discbtme*tmp12*tmp13*tm&
                  &p2*tmp5*tmp6)/tmp3 + (64*discbtme*logmume*tmp12*tmp13*tmp2*tmp5*tmp6)/tmp3 + 128&
                  &*me2*ss*tmp3*tmp5*tmp6 + 64*logmume*me2*ss*tmp3*tmp5*tmp6 + (32*me2*tmp3*tmp5*tm&
                  &p6)/tmp13 + (16*logmume*me2*tmp3*tmp5*tmp6)/tmp13 - (32*ss*tmp3*tmp5*tmp6)/tmp13&
                  & - (16*logmume*ss*tmp3*tmp5*tmp6)/tmp13 + (96*me2*scalarc0ir6tme*ss*tmp3*tmp5*tm&
                  &p6)/tmp13 - (160*discbtme*me2*ss*tmp12*tmp3*tmp5*tmp6)/tmp13 - (96*discbtme*logm&
                  &ume*me2*ss*tmp12*tmp3*tmp5*tmp6)/tmp13 - 64*scalarc0ir6tme*tmp13*tmp14*tmp3*tmp5&
                  &*tmp6 + 128*discbtme*tmp12*tmp13*tmp14*tmp3*tmp5*tmp6 + 64*discbtme*logmume*tmp1&
                  &2*tmp13*tmp14*tmp3*tmp5*tmp6 + 16*me2*scalarc0ir6tme*tmp15*tmp3*tmp5*tmp6 - 16*s&
                  &calarc0ir6tme*ss*tmp15*tmp3*tmp5*tmp6 - 16*discbtme*me2*tmp12*tmp15*tmp3*tmp5*tm&
                  &p6 - 16*discbtme*logmume*me2*tmp12*tmp15*tmp3*tmp5*tmp6 + 24*discbtme*ss*tmp12*t&
                  &mp15*tmp3*tmp5*tmp6 + 16*discbtme*logmume*ss*tmp12*tmp15*tmp3*tmp5*tmp6 - 96*tmp&
                  &2*tmp3*tmp5*tmp6 - 48*logmume*tmp2*tmp3*tmp5*tmp6 - 192*scalarc0ir6tme*ss*tmp2*t&
                  &mp3*tmp5*tmp6 + 352*discbtme*ss*tmp12*tmp2*tmp3*tmp5*tmp6 + 192*discbtme*logmume&
                  &*ss*tmp12*tmp2*tmp3*tmp5*tmp6 - (80*scalarc0ir6tme*tmp2*tmp3*tmp5*tmp6)/tmp13 + &
                  &(120*discbtme*tmp12*tmp2*tmp3*tmp5*tmp6)/tmp13 + (80*discbtme*logmume*tmp12*tmp2&
                  &*tmp3*tmp5*tmp6)/tmp13 - 128*ss*tmp13*tmp2*tmp3*tmp5*tmp6 - 64*logmume*ss*tmp13*&
                  &tmp2*tmp3*tmp5*tmp6 - 64*scalarc0ir6tme*tmp1*tmp13*tmp7 + 128*discbtme*tmp1*tmp1&
                  &2*tmp13*tmp7 + 64*discbtme*logmume*tmp1*tmp12*tmp13*tmp7 - 48*scalarc0ir6tme*tmp&
                  &1*tmp3*tmp7 + 88*discbtme*tmp1*tmp12*tmp3*tmp7 + 48*discbtme*logmume*tmp1*tmp12*&
                  &tmp3*tmp7 - 32*tmp1*tmp13*tmp3*tmp7 - 16*logmume*tmp1*tmp13*tmp3*tmp7 - 64*scala&
                  &rc0ir6tme*ss*tmp1*tmp13*tmp3*tmp7 + 128*discbtme*ss*tmp1*tmp12*tmp13*tmp3*tmp7 +&
                  & 64*discbtme*logmume*ss*tmp1*tmp12*tmp13*tmp3*tmp7 - 64*scalarc0ir6tme*tmp13*tmp&
                  &4*tmp7 + 128*discbtme*tmp12*tmp13*tmp4*tmp7 + 64*discbtme*logmume*tmp12*tmp13*tm&
                  &p4*tmp7 - 48*scalarc0ir6tme*tmp3*tmp4*tmp7 + 88*discbtme*tmp12*tmp3*tmp4*tmp7 + &
                  &48*discbtme*logmume*tmp12*tmp3*tmp4*tmp7 - 32*tmp13*tmp3*tmp4*tmp7 - 16*logmume*&
                  &tmp13*tmp3*tmp4*tmp7 - 64*scalarc0ir6tme*ss*tmp13*tmp3*tmp4*tmp7 + 128*discbtme*&
                  &ss*tmp12*tmp13*tmp3*tmp4*tmp7 + 64*discbtme*logmume*ss*tmp12*tmp13*tmp3*tmp4*tmp&
                  &7 + 128*scalarc0ir6tme*tmp13*tmp5*tmp6*tmp7 - 256*discbtme*tmp12*tmp13*tmp5*tmp6&
                  &*tmp7 - 128*discbtme*logmume*tmp12*tmp13*tmp5*tmp6*tmp7 + 128*scalarc0ir6tme*tmp&
                  &3*tmp5*tmp6*tmp7 - 240*discbtme*tmp12*tmp3*tmp5*tmp6*tmp7 - 128*discbtme*logmume&
                  &*tmp12*tmp3*tmp5*tmp6*tmp7 + 64*tmp13*tmp3*tmp5*tmp6*tmp7 + 32*logmume*tmp13*tmp&
                  &3*tmp5*tmp6*tmp7 + 128*scalarc0ir6tme*ss*tmp13*tmp3*tmp5*tmp6*tmp7 - 256*discbtm&
                  &e*ss*tmp12*tmp13*tmp3*tmp5*tmp6*tmp7 - 128*discbtme*logmume*ss*tmp12*tmp13*tmp3*&
                  &tmp5*tmp6*tmp7 - 16*me2*scalarc0ir6tme*tmp1*tmp3*tmp8 + 24*discbtme*me2*tmp1*tmp&
                  &12*tmp3*tmp8 + 16*discbtme*logmume*me2*tmp1*tmp12*tmp3*tmp8 - 32*me2*tmp1*tmp13*&
                  &tmp3*tmp8 - 16*logmume*me2*tmp1*tmp13*tmp3*tmp8 + 32*scalarc0ir6tme*tmp1*tmp13*t&
                  &mp2*tmp3*tmp8 - 64*discbtme*tmp1*tmp12*tmp13*tmp2*tmp3*tmp8 - 32*discbtme*logmum&
                  &e*tmp1*tmp12*tmp13*tmp2*tmp3*tmp8 - 16*me2*scalarc0ir6tme*tmp3*tmp4*tmp8 + 24*di&
                  &scbtme*me2*tmp12*tmp3*tmp4*tmp8 + 16*discbtme*logmume*me2*tmp12*tmp3*tmp4*tmp8 -&
                  & 32*me2*tmp13*tmp3*tmp4*tmp8 - 16*logmume*me2*tmp13*tmp3*tmp4*tmp8 + 32*scalarc0&
                  &ir6tme*tmp13*tmp2*tmp3*tmp4*tmp8 - 64*discbtme*tmp12*tmp13*tmp2*tmp3*tmp4*tmp8 -&
                  & 32*discbtme*logmume*tmp12*tmp13*tmp2*tmp3*tmp4*tmp8 - 32*tmp3*tmp5*tmp6*tmp8 - &
                  &16*logmume*tmp3*tmp5*tmp6*tmp8 + 64*me2*scalarc0ir6tme*tmp3*tmp5*tmp6*tmp8 - 112&
                  &*discbtme*me2*tmp12*tmp3*tmp5*tmp6*tmp8 - 64*discbtme*logmume*me2*tmp12*tmp3*tmp&
                  &5*tmp6*tmp8 - (16*scalarc0ir6tme*tmp3*tmp5*tmp6*tmp8)/tmp13 + (24*discbtme*tmp12&
                  &*tmp3*tmp5*tmp6*tmp8)/tmp13 + (16*discbtme*logmume*tmp12*tmp3*tmp5*tmp6*tmp8)/tm&
                  &p13 + 64*me2*tmp13*tmp3*tmp5*tmp6*tmp8 + 32*logmume*me2*tmp13*tmp3*tmp5*tmp6*tmp&
                  &8 - 64*scalarc0ir6tme*tmp13*tmp2*tmp3*tmp5*tmp6*tmp8 + 128*discbtme*tmp12*tmp13*&
                  &tmp2*tmp3*tmp5*tmp6*tmp8 + 64*discbtme*logmume*tmp12*tmp13*tmp2*tmp3*tmp5*tmp6*t&
                  &mp8

  END FUNCTION LBK22_LP_QE4

  FUNCTION LBK22_NLP_EP_QE4(me2, mm2, ss, tt, s15, s25, s35)
  implicit none
  real LBK22_NLP_EP_Qe4
  real(kind=prec) ::  me2
  real(kind=prec) ::  mm2
  real(kind=prec) ::  ss
  real(kind=prec) ::  tt
  real(kind=prec) ::  s15
  real(kind=prec) ::  s25
  real(kind=prec) ::  s35
  real tmp1, tmp2, tmp3, tmp4, tmp5
  real tmp6, tmp7, tmp8, tmp9, tmp10
  real tmp11, tmp12, tmp13, tmp14, tmp15
  real tmp16, tmp17, tmp18, tmp19, tmp20

  tmp1 = 1/s15
  tmp2 = 4*me2
  tmp3 = -tt
  tmp4 = tmp2 + tmp3
  tmp5 = 1/tmp4
  tmp6 = me2**2
  tmp7 = 1/mm2
  tmp8 = tmp1**2
  tmp9 = 1/s35
  tmp10 = ss**2
  tmp11 = 1/tt
  tmp12 = me2**3
  tmp13 = tmp11**(-2)
  tmp14 = -(1/tmp1)
  tmp15 = -s25
  tmp16 = tmp14 + tmp15 + 1/tmp9
  tmp17 = 1/tmp16
  tmp18 = -4*me2
  tmp19 = 1/tmp11 + tmp18
  tmp20 = 1/tmp19
  LBK22_NLP_EP_Qe4 = -192*me2*tmp17*tmp20 - 64*discbtme*me2*tmp17*tmp20 + 64*ss*tmp17*tmp20 + 64*disc&
                      &btme*ss*tmp17*tmp20 - 96*me2*s25*tmp1*tmp17*tmp20 - 32*discbtme*me2*s25*tmp1*tmp&
                      &17*tmp20 + 32*s25*ss*tmp1*tmp17*tmp20 + 32*discbtme*s25*ss*tmp1*tmp17*tmp20 + (6&
                      &4*tmp17*tmp20)/tmp11 + (64*discbtme*tmp17*tmp20)/tmp11 + (32*s25*tmp1*tmp17*tmp2&
                      &0)/tmp11 + (32*discbtme*s25*tmp1*tmp17*tmp20)/tmp11 - 256*me2*ss*tmp11*tmp17*tmp&
                      &20 - 128*discbtme*me2*ss*tmp11*tmp17*tmp20 - 128*me2*s25*ss*tmp1*tmp11*tmp17*tmp&
                      &20 - 64*discbtme*me2*s25*ss*tmp1*tmp11*tmp17*tmp20 + 64*me2*tmp1*tmp5 + 48*discb&
                      &tme*me2*tmp1*tmp5 - 32*ss*tmp1*tmp5 - 32*discbtme*ss*tmp1*tmp5 - (8*tmp1*tmp5)/t&
                      &mp11 - (8*discbtme*tmp1*tmp5)/tmp11 + 128*me2*ss*tmp1*tmp11*tmp5 + 64*discbtme*m&
                      &e2*ss*tmp1*tmp11*tmp5 - 256*tmp11*tmp17*tmp20*tmp6 - 128*discbtme*tmp11*tmp17*tm&
                      &p20*tmp6 - 128*s25*tmp1*tmp11*tmp17*tmp20*tmp6 - 64*discbtme*s25*tmp1*tmp11*tmp1&
                      &7*tmp20*tmp6 - 128*tmp1*tmp11*tmp5*tmp6 - 64*discbtme*tmp1*tmp11*tmp5*tmp6 - (32&
                      &*tmp17*tmp20)/tmp7 - (32*discbtme*tmp17*tmp20)/tmp7 - (16*s25*tmp1*tmp17*tmp20)/&
                      &tmp7 - (16*discbtme*s25*tmp1*tmp17*tmp20)/tmp7 + (128*me2*tmp11*tmp17*tmp20)/tmp&
                      &7 + (64*discbtme*me2*tmp11*tmp17*tmp20)/tmp7 + (64*me2*s25*tmp1*tmp11*tmp17*tmp2&
                      &0)/tmp7 + (32*discbtme*me2*s25*tmp1*tmp11*tmp17*tmp20)/tmp7 + (16*tmp1*tmp5)/tmp&
                      &7 + (16*discbtme*tmp1*tmp5)/tmp7 - (64*me2*tmp1*tmp11*tmp5)/tmp7 - (32*discbtme*&
                      &me2*tmp1*tmp11*tmp5)/tmp7 + 192*me2*ss*tmp17*tmp20*tmp7 + 128*discbtme*me2*ss*tm&
                      &p17*tmp20*tmp7 + 96*me2*s25*ss*tmp1*tmp17*tmp20*tmp7 + 64*discbtme*me2*s25*ss*tm&
                      &p1*tmp17*tmp20*tmp7 - 32*tmp10*tmp17*tmp20*tmp7 - 32*discbtme*tmp10*tmp17*tmp20*&
                      &tmp7 - 16*s25*tmp1*tmp10*tmp17*tmp20*tmp7 - 16*discbtme*s25*tmp1*tmp10*tmp17*tmp&
                      &20*tmp7 + (32*me2*tmp17*tmp20*tmp7)/tmp11 + (32*discbtme*me2*tmp17*tmp20*tmp7)/t&
                      &mp11 - (32*ss*tmp17*tmp20*tmp7)/tmp11 - (32*discbtme*ss*tmp17*tmp20*tmp7)/tmp11 &
                      &+ (16*me2*s25*tmp1*tmp17*tmp20*tmp7)/tmp11 + (16*discbtme*me2*s25*tmp1*tmp17*tmp&
                      &20*tmp7)/tmp11 - (16*s25*ss*tmp1*tmp17*tmp20*tmp7)/tmp11 - (16*discbtme*s25*ss*t&
                      &mp1*tmp17*tmp20*tmp7)/tmp11 + 128*me2*tmp10*tmp11*tmp17*tmp20*tmp7 + 64*discbtme&
                      &*me2*tmp10*tmp11*tmp17*tmp20*tmp7 + 64*me2*s25*tmp1*tmp10*tmp11*tmp17*tmp20*tmp7&
                      & + 32*discbtme*me2*s25*tmp1*tmp10*tmp11*tmp17*tmp20*tmp7 + 128*tmp11*tmp12*tmp17&
                      &*tmp20*tmp7 + 64*discbtme*tmp11*tmp12*tmp17*tmp20*tmp7 + 64*s25*tmp1*tmp11*tmp12&
                      &*tmp17*tmp20*tmp7 + 32*discbtme*s25*tmp1*tmp11*tmp12*tmp17*tmp20*tmp7 - 64*me2*s&
                      &s*tmp1*tmp5*tmp7 - 48*discbtme*me2*ss*tmp1*tmp5*tmp7 + 16*tmp1*tmp10*tmp5*tmp7 +&
                      & 16*discbtme*tmp1*tmp10*tmp5*tmp7 - (8*me2*tmp1*tmp5*tmp7)/tmp11 - (8*discbtme*m&
                      &e2*tmp1*tmp5*tmp7)/tmp11 + (8*ss*tmp1*tmp5*tmp7)/tmp11 + (8*discbtme*ss*tmp1*tmp&
                      &5*tmp7)/tmp11 - 64*me2*tmp1*tmp10*tmp11*tmp5*tmp7 - 32*discbtme*me2*tmp1*tmp10*t&
                      &mp11*tmp5*tmp7 - 64*tmp1*tmp11*tmp12*tmp5*tmp7 - 32*discbtme*tmp1*tmp11*tmp12*tm&
                      &p5*tmp7 - 160*tmp17*tmp20*tmp6*tmp7 - 96*discbtme*tmp17*tmp20*tmp6*tmp7 - 80*s25&
                      &*tmp1*tmp17*tmp20*tmp6*tmp7 - 48*discbtme*s25*tmp1*tmp17*tmp20*tmp6*tmp7 - 256*s&
                      &s*tmp11*tmp17*tmp20*tmp6*tmp7 - 128*discbtme*ss*tmp11*tmp17*tmp20*tmp6*tmp7 - 12&
                      &8*s25*ss*tmp1*tmp11*tmp17*tmp20*tmp6*tmp7 - 64*discbtme*s25*ss*tmp1*tmp11*tmp17*&
                      &tmp20*tmp6*tmp7 + 48*tmp1*tmp5*tmp6*tmp7 + 32*discbtme*tmp1*tmp5*tmp6*tmp7 + 128&
                      &*ss*tmp1*tmp11*tmp5*tmp6*tmp7 + 64*discbtme*ss*tmp1*tmp11*tmp5*tmp6*tmp7 + 32*me&
                      &2*s25*tmp5*tmp8 + 32*discbtme*me2*s25*tmp5*tmp8 - 128*s25*tmp11*tmp5*tmp6*tmp8 -&
                      & 64*discbtme*s25*tmp11*tmp5*tmp6*tmp8 - 32*me2*s25*ss*tmp5*tmp7*tmp8 - 32*discbt&
                      &me*me2*s25*ss*tmp5*tmp7*tmp8 - (16*me2*s25*tmp5*tmp7*tmp8)/tmp11 - (16*discbtme*&
                      &me2*s25*tmp5*tmp7*tmp8)/tmp11 - 128*s25*tmp11*tmp12*tmp5*tmp7*tmp8 - 64*discbtme&
                      &*s25*tmp11*tmp12*tmp5*tmp7*tmp8 + 96*s25*tmp5*tmp6*tmp7*tmp8 + 64*discbtme*s25*t&
                      &mp5*tmp6*tmp7*tmp8 + 128*s25*ss*tmp11*tmp5*tmp6*tmp7*tmp8 + 64*discbtme*s25*ss*t&
                      &mp11*tmp5*tmp6*tmp7*tmp8 + (96*me2*tmp1*tmp17*tmp20)/tmp9 + (32*discbtme*me2*tmp&
                      &1*tmp17*tmp20)/tmp9 - (32*ss*tmp1*tmp17*tmp20)/tmp9 - (32*discbtme*ss*tmp1*tmp17&
                      &*tmp20)/tmp9 - (32*tmp1*tmp17*tmp20)/(tmp11*tmp9) - (32*discbtme*tmp1*tmp17*tmp2&
                      &0)/(tmp11*tmp9) + (128*me2*ss*tmp1*tmp11*tmp17*tmp20)/tmp9 + (64*discbtme*me2*ss&
                      &*tmp1*tmp11*tmp17*tmp20)/tmp9 + (128*tmp1*tmp11*tmp17*tmp20*tmp6)/tmp9 + (64*dis&
                      &cbtme*tmp1*tmp11*tmp17*tmp20*tmp6)/tmp9 + (16*tmp1*tmp17*tmp20)/(tmp7*tmp9) + (1&
                      &6*discbtme*tmp1*tmp17*tmp20)/(tmp7*tmp9) - (64*me2*tmp1*tmp11*tmp17*tmp20)/(tmp7&
                      &*tmp9) - (32*discbtme*me2*tmp1*tmp11*tmp17*tmp20)/(tmp7*tmp9) - (96*me2*ss*tmp1*&
                      &tmp17*tmp20*tmp7)/tmp9 - (64*discbtme*me2*ss*tmp1*tmp17*tmp20*tmp7)/tmp9 + (16*t&
                      &mp1*tmp10*tmp17*tmp20*tmp7)/tmp9 + (16*discbtme*tmp1*tmp10*tmp17*tmp20*tmp7)/tmp&
                      &9 - (16*me2*tmp1*tmp17*tmp20*tmp7)/(tmp11*tmp9) - (16*discbtme*me2*tmp1*tmp17*tm&
                      &p20*tmp7)/(tmp11*tmp9) + (16*ss*tmp1*tmp17*tmp20*tmp7)/(tmp11*tmp9) + (16*discbt&
                      &me*ss*tmp1*tmp17*tmp20*tmp7)/(tmp11*tmp9) - (64*me2*tmp1*tmp10*tmp11*tmp17*tmp20&
                      &*tmp7)/tmp9 - (32*discbtme*me2*tmp1*tmp10*tmp11*tmp17*tmp20*tmp7)/tmp9 - (64*tmp&
                      &1*tmp11*tmp12*tmp17*tmp20*tmp7)/tmp9 - (32*discbtme*tmp1*tmp11*tmp12*tmp17*tmp20&
                      &*tmp7)/tmp9 + (80*tmp1*tmp17*tmp20*tmp6*tmp7)/tmp9 + (48*discbtme*tmp1*tmp17*tmp&
                      &20*tmp6*tmp7)/tmp9 + (128*ss*tmp1*tmp11*tmp17*tmp20*tmp6*tmp7)/tmp9 + (64*discbt&
                      &me*ss*tmp1*tmp11*tmp17*tmp20*tmp6*tmp7)/tmp9 + 96*me2*s25*tmp17*tmp20*tmp9 + 32*&
                      &discbtme*me2*s25*tmp17*tmp20*tmp9 - 32*s25*ss*tmp17*tmp20*tmp9 - 32*discbtme*s25&
                      &*ss*tmp17*tmp20*tmp9 + (96*me2*tmp17*tmp20*tmp9)/tmp1 + (32*discbtme*me2*tmp17*t&
                      &mp20*tmp9)/tmp1 - (32*ss*tmp17*tmp20*tmp9)/tmp1 - (32*discbtme*ss*tmp17*tmp20*tm&
                      &p9)/tmp1 - (32*s25*tmp17*tmp20*tmp9)/tmp11 - (32*discbtme*s25*tmp17*tmp20*tmp9)/&
                      &tmp11 - (32*tmp17*tmp20*tmp9)/(tmp1*tmp11) - (32*discbtme*tmp17*tmp20*tmp9)/(tmp&
                      &1*tmp11) + 128*me2*s25*ss*tmp11*tmp17*tmp20*tmp9 + 64*discbtme*me2*s25*ss*tmp11*&
                      &tmp17*tmp20*tmp9 + (128*me2*ss*tmp11*tmp17*tmp20*tmp9)/tmp1 + (64*discbtme*me2*s&
                      &s*tmp11*tmp17*tmp20*tmp9)/tmp1 - 128*me2*tmp5*tmp9 - 80*discbtme*me2*tmp5*tmp9 +&
                      & 32*ss*tmp5*tmp9 + 32*discbtme*ss*tmp5*tmp9 - 96*me2*s25*tmp1*tmp5*tmp9 - 64*dis&
                      &cbtme*me2*s25*tmp1*tmp5*tmp9 + (24*tmp5*tmp9)/tmp11 + (24*discbtme*tmp5*tmp9)/tm&
                      &p11 + (16*s25*tmp1*tmp5*tmp9)/tmp11 + (16*discbtme*s25*tmp1*tmp5*tmp9)/tmp11 - 1&
                      &28*me2*ss*tmp11*tmp5*tmp9 - 64*discbtme*me2*ss*tmp11*tmp5*tmp9 + 128*s25*tmp11*t&
                      &mp17*tmp20*tmp6*tmp9 + 64*discbtme*s25*tmp11*tmp17*tmp20*tmp6*tmp9 + (128*tmp11*&
                      &tmp17*tmp20*tmp6*tmp9)/tmp1 + (64*discbtme*tmp11*tmp17*tmp20*tmp6*tmp9)/tmp1 + 1&
                      &28*tmp11*tmp5*tmp6*tmp9 + 64*discbtme*tmp11*tmp5*tmp6*tmp9 + 128*s25*tmp1*tmp11*&
                      &tmp5*tmp6*tmp9 + 64*discbtme*s25*tmp1*tmp11*tmp5*tmp6*tmp9 + (16*s25*tmp17*tmp20&
                      &*tmp9)/tmp7 + (16*discbtme*s25*tmp17*tmp20*tmp9)/tmp7 + (16*tmp17*tmp20*tmp9)/(t&
                      &mp1*tmp7) + (16*discbtme*tmp17*tmp20*tmp9)/(tmp1*tmp7) - (64*me2*s25*tmp11*tmp17&
                      &*tmp20*tmp9)/tmp7 - (32*discbtme*me2*s25*tmp11*tmp17*tmp20*tmp9)/tmp7 - (64*me2*&
                      &tmp11*tmp17*tmp20*tmp9)/(tmp1*tmp7) - (32*discbtme*me2*tmp11*tmp17*tmp20*tmp9)/(&
                      &tmp1*tmp7) - (16*tmp5*tmp9)/tmp7 - (16*discbtme*tmp5*tmp9)/tmp7 + (64*me2*tmp11*&
                      &tmp5*tmp9)/tmp7 + (32*discbtme*me2*tmp11*tmp5*tmp9)/tmp7 - 96*me2*s25*ss*tmp17*t&
                      &mp20*tmp7*tmp9 - 64*discbtme*me2*s25*ss*tmp17*tmp20*tmp7*tmp9 - (96*me2*ss*tmp17&
                      &*tmp20*tmp7*tmp9)/tmp1 - (64*discbtme*me2*ss*tmp17*tmp20*tmp7*tmp9)/tmp1 + 16*s2&
                      &5*tmp10*tmp17*tmp20*tmp7*tmp9 + 16*discbtme*s25*tmp10*tmp17*tmp20*tmp7*tmp9 + (1&
                      &6*tmp10*tmp17*tmp20*tmp7*tmp9)/tmp1 + (16*discbtme*tmp10*tmp17*tmp20*tmp7*tmp9)/&
                      &tmp1 - (16*me2*s25*tmp17*tmp20*tmp7*tmp9)/tmp11 - (16*discbtme*me2*s25*tmp17*tmp&
                      &20*tmp7*tmp9)/tmp11 + (16*s25*ss*tmp17*tmp20*tmp7*tmp9)/tmp11 + (16*discbtme*s25&
                      &*ss*tmp17*tmp20*tmp7*tmp9)/tmp11 - (16*me2*tmp17*tmp20*tmp7*tmp9)/(tmp1*tmp11) -&
                      & (16*discbtme*me2*tmp17*tmp20*tmp7*tmp9)/(tmp1*tmp11) + (16*ss*tmp17*tmp20*tmp7*&
                      &tmp9)/(tmp1*tmp11) + (16*discbtme*ss*tmp17*tmp20*tmp7*tmp9)/(tmp1*tmp11) - 64*me&
                      &2*s25*tmp10*tmp11*tmp17*tmp20*tmp7*tmp9 - 32*discbtme*me2*s25*tmp10*tmp11*tmp17*&
                      &tmp20*tmp7*tmp9 - (64*me2*tmp10*tmp11*tmp17*tmp20*tmp7*tmp9)/tmp1 - (32*discbtme&
                      &*me2*tmp10*tmp11*tmp17*tmp20*tmp7*tmp9)/tmp1 - 64*s25*tmp11*tmp12*tmp17*tmp20*tm&
                      &p7*tmp9 - 32*discbtme*s25*tmp11*tmp12*tmp17*tmp20*tmp7*tmp9 - (64*tmp11*tmp12*tm&
                      &p17*tmp20*tmp7*tmp9)/tmp1 - (32*discbtme*tmp11*tmp12*tmp17*tmp20*tmp7*tmp9)/tmp1&
                      & + 128*me2*ss*tmp5*tmp7*tmp9 + 80*discbtme*me2*ss*tmp5*tmp7*tmp9 + 96*me2*s25*ss&
                      &*tmp1*tmp5*tmp7*tmp9 + 64*discbtme*me2*s25*ss*tmp1*tmp5*tmp7*tmp9 - 16*tmp10*tmp&
                      &5*tmp7*tmp9 - 16*discbtme*tmp10*tmp5*tmp7*tmp9 + (56*me2*tmp5*tmp7*tmp9)/tmp11 +&
                      & (40*discbtme*me2*tmp5*tmp7*tmp9)/tmp11 - (24*ss*tmp5*tmp7*tmp9)/tmp11 - (24*dis&
                      &cbtme*ss*tmp5*tmp7*tmp9)/tmp11 + (64*me2*s25*tmp1*tmp5*tmp7*tmp9)/tmp11 + (48*di&
                      &scbtme*me2*s25*tmp1*tmp5*tmp7*tmp9)/tmp11 - (16*s25*ss*tmp1*tmp5*tmp7*tmp9)/tmp1&
                      &1 - (16*discbtme*s25*ss*tmp1*tmp5*tmp7*tmp9)/tmp11 + 64*me2*tmp10*tmp11*tmp5*tmp&
                      &7*tmp9 + 32*discbtme*me2*tmp10*tmp11*tmp5*tmp7*tmp9 + 64*tmp11*tmp12*tmp5*tmp7*t&
                      &mp9 + 32*discbtme*tmp11*tmp12*tmp5*tmp7*tmp9 + 128*s25*tmp1*tmp11*tmp12*tmp5*tmp&
                      &7*tmp9 + 64*discbtme*s25*tmp1*tmp11*tmp12*tmp5*tmp7*tmp9 - 8*tmp13*tmp5*tmp7*tmp&
                      &9 - 8*discbtme*tmp13*tmp5*tmp7*tmp9 - 8*s25*tmp1*tmp13*tmp5*tmp7*tmp9 - 8*discbt&
                      &me*s25*tmp1*tmp13*tmp5*tmp7*tmp9 + 80*s25*tmp17*tmp20*tmp6*tmp7*tmp9 + 48*discbt&
                      &me*s25*tmp17*tmp20*tmp6*tmp7*tmp9 + (80*tmp17*tmp20*tmp6*tmp7*tmp9)/tmp1 + (48*d&
                      &iscbtme*tmp17*tmp20*tmp6*tmp7*tmp9)/tmp1 + 128*s25*ss*tmp11*tmp17*tmp20*tmp6*tmp&
                      &7*tmp9 + 64*discbtme*s25*ss*tmp11*tmp17*tmp20*tmp6*tmp7*tmp9 + (128*ss*tmp11*tmp&
                      &17*tmp20*tmp6*tmp7*tmp9)/tmp1 + (64*discbtme*ss*tmp11*tmp17*tmp20*tmp6*tmp7*tmp9&
                      &)/tmp1 - 112*tmp5*tmp6*tmp7*tmp9 - 64*discbtme*tmp5*tmp6*tmp7*tmp9 - 160*s25*tmp&
                      &1*tmp5*tmp6*tmp7*tmp9 - 96*discbtme*s25*tmp1*tmp5*tmp6*tmp7*tmp9 - 128*ss*tmp11*&
                      &tmp5*tmp6*tmp7*tmp9 - 64*discbtme*ss*tmp11*tmp5*tmp6*tmp7*tmp9 - 128*s25*ss*tmp1&
                      &*tmp11*tmp5*tmp6*tmp7*tmp9 - 64*discbtme*s25*ss*tmp1*tmp11*tmp5*tmp6*tmp7*tmp9

  END FUNCTION LBK22_NLP_EP_QE4

  FUNCTION LBK22_NLP_QE4(me2, mm2, ss, tt, s15, s25, s35)
  implicit none
  real LBK22_NLP_Qe4
  real(kind=prec) ::  me2
  real(kind=prec) ::  mm2
  real(kind=prec) ::  ss
  real(kind=prec) ::  tt
  real(kind=prec) ::  s15
  real(kind=prec) ::  s25
  real(kind=prec) ::  s35
  real tmp1, tmp2, tmp3, tmp4, tmp5
  real tmp6, tmp7, tmp8, tmp9, tmp10
  real tmp11, tmp12

  tmp1 = 1/s15
  tmp2 = 1/mm2
  tmp3 = tmp1**2
  tmp4 = 1/s35
  tmp5 = me2**2
  tmp6 = 4*me2
  tmp7 = -tt
  tmp8 = tmp6 + tmp7
  tmp9 = 1/tmp8
  tmp10 = 1/tt
  tmp11 = me2**3
  tmp12 = tmp10**(-2)
  LBK22_NLP_Qe4 = -48*tmp1 - 24*logmume*tmp1 - 16*me2*scalarc0ir6tme*tmp1 - (24*scalarc0ir6tme*tmp&
                   &1)/tmp10 - 128*me2*tmp1*tmp10 - 64*logmume*me2*tmp1*tmp10 - 16*me2*tmp1*tmp2 - 8&
                   &*logmume*me2*tmp1*tmp2 + 16*ss*tmp1*tmp2 + 8*logmume*ss*tmp1*tmp2 - 16*me2*scala&
                   &rc0ir6tme*ss*tmp1*tmp2 - (8*me2*scalarc0ir6tme*tmp1*tmp2)/tmp10 + (8*scalarc0ir6&
                   &tme*ss*tmp1*tmp2)/tmp10 - 32*me2*s25*scalarc0ir6tme*tmp3 - 64*me2*s25*tmp10*tmp3&
                   & - 32*logmume*me2*s25*tmp10*tmp3 + 32*me2*s25*tmp2*tmp3 + 16*logmume*me2*s25*tmp&
                   &2*tmp3 + 32*me2*s25*scalarc0ir6tme*ss*tmp2*tmp3 + (16*me2*s25*scalarc0ir6tme*tmp&
                   &2*tmp3)/tmp10 + 64*me2*s25*ss*tmp10*tmp2*tmp3 + 32*logmume*me2*s25*ss*tmp10*tmp2&
                   &*tmp3 + 64*s25*scalarc0ir6tme*tmp10*tmp11*tmp2*tmp3 + 16*tmp4 + 8*logmume*tmp4 +&
                   & 48*me2*scalarc0ir6tme*tmp4 - 32*s25*tmp1*tmp4 - 16*logmume*s25*tmp1*tmp4 + 64*m&
                   &e2*s25*scalarc0ir6tme*tmp1*tmp4 + (8*scalarc0ir6tme*tmp4)/tmp10 - (16*s25*scalar&
                   &c0ir6tme*tmp1*tmp4)/tmp10 + 128*me2*tmp10*tmp4 + 64*logmume*me2*tmp10*tmp4 + 64*&
                   &me2*s25*tmp1*tmp10*tmp4 + 32*logmume*me2*s25*tmp1*tmp10*tmp4 - 16*me2*tmp2*tmp4 &
                   &- 8*logmume*me2*tmp2*tmp4 + 16*ss*tmp2*tmp4 + 8*logmume*ss*tmp2*tmp4 - 16*me2*sc&
                   &alarc0ir6tme*ss*tmp2*tmp4 - 64*me2*s25*tmp1*tmp2*tmp4 - 32*logmume*me2*s25*tmp1*&
                   &tmp2*tmp4 + 32*s25*ss*tmp1*tmp2*tmp4 + 16*logmume*s25*ss*tmp1*tmp2*tmp4 - 64*me2&
                   &*s25*scalarc0ir6tme*ss*tmp1*tmp2*tmp4 + (16*tmp2*tmp4)/tmp10 + (8*logmume*tmp2*t&
                   &mp4)/tmp10 - (24*me2*scalarc0ir6tme*tmp2*tmp4)/tmp10 + (8*scalarc0ir6tme*ss*tmp2&
                   &*tmp4)/tmp10 + (16*s25*tmp1*tmp2*tmp4)/tmp10 + (8*logmume*s25*tmp1*tmp2*tmp4)/tm&
                   &p10 - (48*me2*s25*scalarc0ir6tme*tmp1*tmp2*tmp4)/tmp10 + (16*s25*scalarc0ir6tme*&
                   &ss*tmp1*tmp2*tmp4)/tmp10 - 64*me2*s25*ss*tmp1*tmp10*tmp2*tmp4 - 32*logmume*me2*s&
                   &25*ss*tmp1*tmp10*tmp2*tmp4 - 64*s25*scalarc0ir6tme*tmp1*tmp10*tmp11*tmp2*tmp4 + &
                   &8*scalarc0ir6tme*tmp12*tmp2*tmp4 + 8*s25*scalarc0ir6tme*tmp1*tmp12*tmp2*tmp4 + 1&
                   &28*scalarc0ir6tme*tmp1*tmp10*tmp5 + 16*scalarc0ir6tme*tmp1*tmp2*tmp5 + 64*s25*sc&
                   &alarc0ir6tme*tmp10*tmp3*tmp5 - 64*s25*scalarc0ir6tme*tmp2*tmp3*tmp5 - 64*s25*tmp&
                   &10*tmp2*tmp3*tmp5 - 32*logmume*s25*tmp10*tmp2*tmp3*tmp5 - 64*s25*scalarc0ir6tme*&
                   &ss*tmp10*tmp2*tmp3*tmp5 - 128*scalarc0ir6tme*tmp10*tmp4*tmp5 - 64*s25*scalarc0ir&
                   &6tme*tmp1*tmp10*tmp4*tmp5 + 16*scalarc0ir6tme*tmp2*tmp4*tmp5 + 96*s25*scalarc0ir&
                   &6tme*tmp1*tmp2*tmp4*tmp5 + 64*s25*tmp1*tmp10*tmp2*tmp4*tmp5 + 32*logmume*s25*tmp&
                   &1*tmp10*tmp2*tmp4*tmp5 + 64*s25*scalarc0ir6tme*ss*tmp1*tmp10*tmp2*tmp4*tmp5 - 64&
                   &*discbtme*me2*tmp1*tmp9 + 16*discbtme*logmume*me2*tmp1*tmp9 + (36*discbtme*tmp1*&
                   &tmp9)/tmp10 + (24*discbtme*logmume*tmp1*tmp9)/tmp10 + 32*discbtme*me2*ss*tmp1*tm&
                   &p2*tmp9 + 16*discbtme*logmume*me2*ss*tmp1*tmp2*tmp9 + (8*discbtme*logmume*me2*tm&
                   &p1*tmp2*tmp9)/tmp10 - (12*discbtme*ss*tmp1*tmp2*tmp9)/tmp10 - (8*discbtme*logmum&
                   &e*ss*tmp1*tmp2*tmp9)/tmp10 + 48*discbtme*me2*s25*tmp3*tmp9 + 32*discbtme*logmume&
                   &*me2*s25*tmp3*tmp9 - 48*discbtme*me2*s25*ss*tmp2*tmp3*tmp9 - 32*discbtme*logmume&
                   &*me2*s25*ss*tmp2*tmp3*tmp9 - (24*discbtme*me2*s25*tmp2*tmp3*tmp9)/tmp10 - (16*di&
                   &scbtme*logmume*me2*s25*tmp2*tmp3*tmp9)/tmp10 - 128*discbtme*s25*tmp10*tmp11*tmp2&
                   &*tmp3*tmp9 - 64*discbtme*logmume*s25*tmp10*tmp11*tmp2*tmp3*tmp9 - 48*discbtme*lo&
                   &gmume*me2*tmp4*tmp9 - 112*discbtme*me2*s25*tmp1*tmp4*tmp9 - 64*discbtme*logmume*&
                   &me2*s25*tmp1*tmp4*tmp9 - (12*discbtme*tmp4*tmp9)/tmp10 - (8*discbtme*logmume*tmp&
                   &4*tmp9)/tmp10 + (24*discbtme*s25*tmp1*tmp4*tmp9)/tmp10 + (16*discbtme*logmume*s2&
                   &5*tmp1*tmp4*tmp9)/tmp10 + 32*discbtme*me2*ss*tmp2*tmp4*tmp9 + 16*discbtme*logmum&
                   &e*me2*ss*tmp2*tmp4*tmp9 + 112*discbtme*me2*s25*ss*tmp1*tmp2*tmp4*tmp9 + 64*discb&
                   &tme*logmume*me2*s25*ss*tmp1*tmp2*tmp4*tmp9 + (52*discbtme*me2*tmp2*tmp4*tmp9)/tm&
                   &p10 + (24*discbtme*logmume*me2*tmp2*tmp4*tmp9)/tmp10 - (12*discbtme*ss*tmp2*tmp4&
                   &*tmp9)/tmp10 - (8*discbtme*logmume*ss*tmp2*tmp4*tmp9)/tmp10 + (80*discbtme*me2*s&
                   &25*tmp1*tmp2*tmp4*tmp9)/tmp10 + (48*discbtme*logmume*me2*s25*tmp1*tmp2*tmp4*tmp9&
                   &)/tmp10 - (24*discbtme*s25*ss*tmp1*tmp2*tmp4*tmp9)/tmp10 - (16*discbtme*logmume*&
                   &s25*ss*tmp1*tmp2*tmp4*tmp9)/tmp10 + 128*discbtme*s25*tmp1*tmp10*tmp11*tmp2*tmp4*&
                   &tmp9 + 64*discbtme*logmume*s25*tmp1*tmp10*tmp11*tmp2*tmp4*tmp9 - 12*discbtme*tmp&
                   &12*tmp2*tmp4*tmp9 - 8*discbtme*logmume*tmp12*tmp2*tmp4*tmp9 - 12*discbtme*s25*tm&
                   &p1*tmp12*tmp2*tmp4*tmp9 - 8*discbtme*logmume*s25*tmp1*tmp12*tmp2*tmp4*tmp9 - 256&
                   &*discbtme*tmp1*tmp10*tmp5*tmp9 - 128*discbtme*logmume*tmp1*tmp10*tmp5*tmp9 - 32*&
                   &discbtme*tmp1*tmp2*tmp5*tmp9 - 16*discbtme*logmume*tmp1*tmp2*tmp5*tmp9 - 128*dis&
                   &cbtme*s25*tmp10*tmp3*tmp5*tmp9 - 64*discbtme*logmume*s25*tmp10*tmp3*tmp5*tmp9 + &
                   &112*discbtme*s25*tmp2*tmp3*tmp5*tmp9 + 64*discbtme*logmume*s25*tmp2*tmp3*tmp5*tm&
                   &p9 + 128*discbtme*s25*ss*tmp10*tmp2*tmp3*tmp5*tmp9 + 64*discbtme*logmume*s25*ss*&
                   &tmp10*tmp2*tmp3*tmp5*tmp9 + 256*discbtme*tmp10*tmp4*tmp5*tmp9 + 128*discbtme*log&
                   &mume*tmp10*tmp4*tmp5*tmp9 + 128*discbtme*s25*tmp1*tmp10*tmp4*tmp5*tmp9 + 64*disc&
                   &btme*logmume*s25*tmp1*tmp10*tmp4*tmp5*tmp9 - 32*discbtme*tmp2*tmp4*tmp5*tmp9 - 1&
                   &6*discbtme*logmume*tmp2*tmp4*tmp5*tmp9 - 176*discbtme*s25*tmp1*tmp2*tmp4*tmp5*tm&
                   &p9 - 96*discbtme*logmume*s25*tmp1*tmp2*tmp4*tmp5*tmp9 - 128*discbtme*s25*ss*tmp1&
                   &*tmp10*tmp2*tmp4*tmp5*tmp9 - 64*discbtme*logmume*s25*ss*tmp1*tmp10*tmp2*tmp4*tmp&
                   &5*tmp9 + (discbtme*tmp1*tmp2*tmp6*tmp9)/tmp10

  END FUNCTION LBK22_NLP_QE4

  FUNCTION LBK_LP_EP_QE3QM(me2, mm2, ss, tt, s15, s25, s35)
  implicit none
  real(kind=prec) ::  me2
  real(kind=prec) ::  mm2
  real(kind=prec) ::  ss
  real(kind=prec) ::  tt
  real(kind=prec) ::  s15
  real(kind=prec) ::  s25
  real(kind=prec) ::  s35
  real LBK_LP_EP_Qe3Qm
  real tmp1, tmp2, tmp3, tmp4, tmp5
  real tmp6, tmp7, tmp8, tmp9, tmp10
  real tmp11, tmp12, tmp13, tmp14, tmp15
  real tmp16, tmp17, tmp18, tmp19, tmp20
  real tmp21, tmp22, tmp23, tmp24, tmp25
  real tmp26, tmp27, tmp28, tmp29, tmp30
  real tmp31, tmp32, tmp33, tmp34, tmp35
  real tmp36, tmp37, tmp38, tmp39, tmp40
  real tmp41, tmp42, tmp43, tmp44

  tmp1 = -ss
  tmp2 = me2 + mm2 + tmp1
  tmp3 = tt**(-2)
  tmp4 = tmp2**2
  tmp5 = 2*tmp4
  tmp6 = 2*ss*tt
  tmp7 = 1/tmp3
  tmp8 = tmp5 + tmp6 + tmp7
  tmp9 = sqrt(me2)
  tmp10 = sqrt(mm2)
  tmp11 = -tmp10
  tmp12 = tmp11 + tmp9
  tmp13 = tmp12**2
  tmp14 = tmp1 + tmp13
  tmp15 = 1/tmp14
  tmp16 = tmp10 + tmp9
  tmp17 = tmp16**2
  tmp18 = tmp1 + tmp17
  tmp19 = 1/tmp18
  tmp20 = 1/s15
  tmp21 = 1/s35
  tmp22 = 1/s25
  tmp23 = -tt
  tmp24 = -(1/tmp21)
  tmp25 = 1/tmp20 + 1/tmp22 + tmp24
  tmp26 = 1/tmp25
  tmp27 = 2*me2
  tmp28 = tmp23 + tmp27
  tmp29 = 4*me2
  tmp30 = tmp23 + tmp29
  tmp31 = 1/tmp30
  tmp32 = tmp20**2
  tmp33 = tmp21**2
  tmp34 = tmp14 + tmp23
  tmp35 = 1/tmp34
  tmp36 = tmp18 + tmp23
  tmp37 = 1/tmp36
  tmp38 = tmp2 + tmp23
  tmp39 = 2*mm2
  tmp40 = tmp1 + tmp28 + tmp39
  tmp41 = 1/tt
  tmp42 = -me2
  tmp43 = -mm2
  tmp44 = ss + 1/tmp41 + tmp42 + tmp43
  LBK_LP_EP_Qe3Qm = -128*discbs*me2*ss*tmp15*tmp19*tmp2*tmp20*tmp21*tmp3*tmp8 - 32*tmp2*tmp20*tmp22*&
                     &tmp3*tmp8 - 32*tmp2*tmp21*tmp26*tmp3*tmp8 - 32*discbtme*tmp2*tmp20*tmp22*tmp28*t&
                     &mp3*tmp31*tmp8 - 32*discbtme*tmp2*tmp21*tmp26*tmp28*tmp3*tmp31*tmp8 + 64*discbs*&
                     &me2*ss*tmp15*tmp19*tmp2*tmp3*tmp32*tmp8 + 64*discbs*me2*ss*tmp15*tmp19*tmp2*tmp3&
                     &*tmp33*tmp8 - 128*discbu*me2*tmp20*tmp21*tmp3*tmp35*tmp37*tmp38*tmp40*tmp8 + 64*&
                     &discbu*me2*tmp3*tmp32*tmp35*tmp37*tmp38*tmp40*tmp8 + 64*discbu*me2*tmp3*tmp33*tm&
                     &p35*tmp37*tmp38*tmp40*tmp8 + 64*discbs*ss*tmp15*tmp19*tmp2*tmp20*tmp21*tmp41*tmp&
                     &8 + 64*discbu*tmp20*tmp21*tmp35*tmp37*tmp38*tmp40*tmp41*tmp8 - 32*tmp21*tmp22*tm&
                     &p3*tmp44*tmp8 - 32*tmp20*tmp26*tmp3*tmp44*tmp8 - 32*discbtme*tmp21*tmp22*tmp28*t&
                     &mp3*tmp31*tmp44*tmp8 - 32*discbtme*tmp20*tmp26*tmp28*tmp3*tmp31*tmp44*tmp8

  END FUNCTION LBK_LP_EP_QE3QM

  FUNCTION LBK_LP_EP_QEQM3(me2, mm2, ss, tt, s15, s25, s35)
  implicit none
  real LBK_LP_EP_QeQm3
  real(kind=prec) ::  me2
  real(kind=prec) ::  mm2
  real(kind=prec) ::  ss
  real(kind=prec) ::  tt
  real(kind=prec) ::  s15
  real(kind=prec) ::  s25
  real(kind=prec) ::  s35
  real tmp1, tmp2, tmp3, tmp4, tmp5
  real tmp6, tmp7, tmp8, tmp9, tmp10
  real tmp11, tmp12, tmp13, tmp14, tmp15
  real tmp16, tmp17, tmp18, tmp19, tmp20
  real tmp21, tmp22, tmp23, tmp24, tmp25
  real tmp26, tmp27, tmp28, tmp29, tmp30
  real tmp31, tmp32, tmp33, tmp34, tmp35
  real tmp36, tmp37, tmp38, tmp39, tmp40
  real tmp41, tmp42, tmp43

  tmp1 = -ss
  tmp2 = me2 + mm2 + tmp1
  tmp3 = tt**(-2)
  tmp4 = tmp2**2
  tmp5 = 2*tmp4
  tmp6 = 2*ss*tt
  tmp7 = 1/tmp3
  tmp8 = tmp5 + tmp6 + tmp7
  tmp9 = sqrt(me2)
  tmp10 = sqrt(mm2)
  tmp11 = -s35
  tmp12 = s15 + s25 + tmp11
  tmp13 = -tmp10
  tmp14 = tmp13 + tmp9
  tmp15 = tmp14**2
  tmp16 = tmp1 + tmp15
  tmp17 = 1/tmp16
  tmp18 = tmp10 + tmp9
  tmp19 = tmp18**2
  tmp20 = tmp1 + tmp19
  tmp21 = 1/tmp20
  tmp22 = 1/s25
  tmp23 = 1/tmp12
  tmp24 = 1/s15
  tmp25 = 2*mm2
  tmp26 = -tt
  tmp27 = tmp25 + tmp26
  tmp28 = 1/s35
  tmp29 = 4*mm2
  tmp30 = tmp26 + tmp29
  tmp31 = 1/tmp30
  tmp32 = tmp22**2
  tmp33 = tmp23**2
  tmp34 = tmp16 + tmp26
  tmp35 = 1/tmp34
  tmp36 = tmp20 + tmp26
  tmp37 = 1/tmp36
  tmp38 = tmp2 + tmp26
  tmp39 = 2*me2
  tmp40 = tmp1 + tmp27 + tmp39
  tmp41 = -me2
  tmp42 = -mm2
  tmp43 = ss + tmp41 + tmp42 + tt
  LBK_LP_EP_QeQm3 = -32*tmp2*tmp22*tmp24*tmp3*tmp8 - 64*discbs*ss*tmp17*tmp2*tmp21*tmp22*tmp23*tmp27&
                     &*tmp3*tmp8 - 32*tmp2*tmp23*tmp28*tmp3*tmp8 - 32*discbtmm*tmp2*tmp22*tmp24*tmp27*&
                     &tmp3*tmp31*tmp8 - 32*discbtmm*tmp2*tmp23*tmp27*tmp28*tmp3*tmp31*tmp8 + 64*discbs&
                     &*mm2*ss*tmp17*tmp2*tmp21*tmp3*tmp32*tmp8 + 64*discbs*mm2*ss*tmp17*tmp2*tmp21*tmp&
                     &3*tmp33*tmp8 - 64*discbu*tmp22*tmp23*tmp27*tmp3*tmp35*tmp37*tmp38*tmp40*tmp8 + 6&
                     &4*discbu*mm2*tmp3*tmp32*tmp35*tmp37*tmp38*tmp40*tmp8 + 64*discbu*mm2*tmp3*tmp33*&
                     &tmp35*tmp37*tmp38*tmp40*tmp8 - 32*tmp23*tmp24*tmp3*tmp43*tmp8 - 32*tmp22*tmp28*t&
                     &mp3*tmp43*tmp8 - 32*discbtmm*tmp23*tmp24*tmp27*tmp3*tmp31*tmp43*tmp8 - 32*discbt&
                     &mm*tmp22*tmp27*tmp28*tmp3*tmp31*tmp43*tmp8

  END FUNCTION LBK_LP_EP_QEQM3

  FUNCTION LBK_LP_EP_QE2QM2(me2, mm2, ss, tt, s15, s25, s35)
  implicit none
  real LBK_LP_EP_Qe2Qm2
  real(kind=prec) ::  me2
  real(kind=prec) ::  mm2
  real(kind=prec) ::  ss
  real(kind=prec) ::  tt
  real(kind=prec) ::  s15
  real(kind=prec) ::  s25
  real(kind=prec) ::  s35
  real tmp1, tmp2, tmp3, tmp4, tmp5
  real tmp6, tmp7, tmp8, tmp9, tmp10
  real tmp11, tmp12, tmp13, tmp14, tmp15
  real tmp16, tmp17, tmp18, tmp19, tmp20
  real tmp21, tmp22, tmp23, tmp24, tmp25
  real tmp26, tmp27, tmp28, tmp29, tmp30
  real tmp31, tmp32, tmp33, tmp34, tmp35
  real tmp36, tmp37, tmp38, tmp39, tmp40
  real tmp41, tmp42, tmp43, tmp44, tmp45
  real tmp46, tmp47, tmp48, tmp49, tmp50

  tmp1 = tt**(-2)
  tmp2 = -ss
  tmp3 = me2 + mm2 + tmp2
  tmp4 = tmp3**2
  tmp5 = 2*tmp4
  tmp6 = 2*ss*tt
  tmp7 = 1/tmp1
  tmp8 = tmp5 + tmp6 + tmp7
  tmp9 = 1/s15
  tmp10 = sqrt(me2)
  tmp11 = sqrt(mm2)
  tmp12 = -s35
  tmp13 = s25 + tmp12 + 1/tmp9
  tmp14 = 1/s35
  tmp15 = -tmp11
  tmp16 = tmp10 + tmp15
  tmp17 = tmp16**2
  tmp18 = tmp17 + tmp2
  tmp19 = 1/tmp18
  tmp20 = tmp10 + tmp11
  tmp21 = tmp20**2
  tmp22 = tmp2 + tmp21
  tmp23 = 1/tmp22
  tmp24 = s25**(-2)
  tmp25 = -tt
  tmp26 = tmp13**(-2)
  tmp27 = 2*me2
  tmp28 = tmp25 + tmp27
  tmp29 = 4*me2
  tmp30 = tmp25 + tmp29
  tmp31 = 1/tmp30
  tmp32 = 1/s25
  tmp33 = 1/tmp13
  tmp34 = 2*mm2
  tmp35 = tmp25 + tmp34
  tmp36 = tmp9**2
  tmp37 = tmp14**2
  tmp38 = 4*mm2
  tmp39 = tmp25 + tmp38
  tmp40 = 1/tmp39
  tmp41 = tmp18 + tmp25
  tmp42 = 1/tmp41
  tmp43 = tmp22 + tmp25
  tmp44 = 1/tmp43
  tmp45 = tmp25 + tmp3
  tmp46 = tmp2 + tmp27 + tmp35
  tmp47 = 1/tt
  tmp48 = -me2
  tmp49 = -mm2
  tmp50 = ss + 1/tmp47 + tmp48 + tmp49
  LBK_LP_EP_Qe2Qm2 = 32*mm2*tmp1*tmp24*tmp8 + 32*mm2*tmp1*tmp26*tmp8 + 32*discbtme*mm2*tmp1*tmp24*tmp&
                      &28*tmp31*tmp8 + 32*discbtme*mm2*tmp1*tmp26*tmp28*tmp31*tmp8 - 32*tmp1*tmp32*tmp3&
                      &3*tmp35*tmp8 - 32*discbtme*tmp1*tmp28*tmp31*tmp32*tmp33*tmp35*tmp8 + 32*me2*tmp1&
                      &*tmp36*tmp8 + 32*me2*tmp1*tmp37*tmp8 - 64*discbs*ss*tmp1*tmp14*tmp19*tmp23*tmp33&
                      &*tmp4*tmp8 + 32*discbtmm*me2*tmp1*tmp35*tmp36*tmp40*tmp8 + 32*discbtmm*me2*tmp1*&
                      &tmp35*tmp37*tmp40*tmp8 - 64*discbu*tmp1*tmp14*tmp3*tmp33*tmp42*tmp44*tmp45*tmp46&
                      &*tmp8 - 64*discbs*ss*tmp1*tmp14*tmp19*tmp23*tmp3*tmp32*tmp50*tmp8 - 64*discbu*tm&
                      &p1*tmp14*tmp32*tmp42*tmp44*tmp45*tmp46*tmp50*tmp8 - 64*me2*tmp1*tmp14*tmp8*tmp9 &
                      &- 64*discbs*ss*tmp1*tmp19*tmp23*tmp32*tmp4*tmp8*tmp9 - 64*discbtmm*me2*tmp1*tmp1&
                      &4*tmp35*tmp40*tmp8*tmp9 - 64*discbu*tmp1*tmp3*tmp32*tmp42*tmp44*tmp45*tmp46*tmp8&
                      &*tmp9 + 32*tmp14*tmp47*tmp8*tmp9 + 32*discbtmm*tmp14*tmp35*tmp40*tmp47*tmp8*tmp9&
                      & - 64*discbs*ss*tmp1*tmp19*tmp23*tmp3*tmp33*tmp50*tmp8*tmp9 - 64*discbu*tmp1*tmp&
                      &33*tmp42*tmp44*tmp45*tmp46*tmp50*tmp8*tmp9

  END FUNCTION LBK_LP_EP_QE2QM2

  FUNCTION LBK_LP_EP_QE4(me2, mm2, ss, tt, s15, s25, s35)
  implicit none
  real LBK_LP_EP_Qe4
  real(kind=prec) ::  me2
  real(kind=prec) ::  mm2
  real(kind=prec) ::  ss
  real(kind=prec) ::  tt
  real(kind=prec) ::  s15
  real(kind=prec) ::  s25
  real(kind=prec) ::  s35
  real tmp1, tmp2, tmp3, tmp4, tmp5
  real tmp6, tmp7, tmp8, tmp9, tmp10
  real tmp11, tmp12, tmp13, tmp14, tmp15
  real tmp16, tmp17, tmp18, tmp19

  tmp1 = tt**(-2)
  tmp2 = -ss
  tmp3 = me2 + mm2 + tmp2
  tmp4 = tmp3**2
  tmp5 = 2*tmp4
  tmp6 = 2*ss*tt
  tmp7 = 1/tmp1
  tmp8 = tmp5 + tmp6 + tmp7
  tmp9 = s15**(-2)
  tmp10 = -tt
  tmp11 = s35**(-2)
  tmp12 = 2*me2
  tmp13 = tmp10 + tmp12
  tmp14 = 4*me2
  tmp15 = tmp10 + tmp14
  tmp16 = 1/tmp15
  tmp17 = 1/s15
  tmp18 = 1/s35
  tmp19 = 1/tt
  LBK_LP_EP_Qe4 = 32*me2*tmp1*tmp11*tmp8 + 32*discbtme*me2*tmp1*tmp11*tmp13*tmp16*tmp8 - 64*me2*tm&
                   &p1*tmp17*tmp18*tmp8 - 64*discbtme*me2*tmp1*tmp13*tmp16*tmp17*tmp18*tmp8 + 32*tmp&
                   &17*tmp18*tmp19*tmp8 + 32*discbtme*tmp13*tmp16*tmp17*tmp18*tmp19*tmp8 + 32*me2*tm&
                   &p1*tmp8*tmp9 + 32*discbtme*me2*tmp1*tmp13*tmp16*tmp8*tmp9

  END FUNCTION LBK_LP_EP_QE4

  FUNCTION LBK_LP_EP_QM4(me2, mm2, ss, tt, s15, s25, s35)
  implicit none
  real LBK_LP_EP_Qm4
  real(kind=prec) ::  me2
  real(kind=prec) ::  mm2
  real(kind=prec) ::  ss
  real(kind=prec) ::  tt
  real(kind=prec) ::  s15
  real(kind=prec) ::  s25
  real(kind=prec) ::  s35
  real tmp1, tmp2, tmp3, tmp4, tmp5
  real tmp6, tmp7, tmp8, tmp9, tmp10
  real tmp11, tmp12, tmp13, tmp14, tmp15
  real tmp16, tmp17, tmp18, tmp19, tmp20

  tmp1 = tt**(-2)
  tmp2 = -ss
  tmp3 = me2 + mm2 + tmp2
  tmp4 = tmp3**2
  tmp5 = 2*tmp4
  tmp6 = 2*ss*tt
  tmp7 = 1/tmp1
  tmp8 = tmp5 + tmp6 + tmp7
  tmp9 = -s35
  tmp10 = s15 + s25 + tmp9
  tmp11 = s25**(-2)
  tmp12 = 2*mm2
  tmp13 = -tt
  tmp14 = tmp12 + tmp13
  tmp15 = tmp10**(-2)
  tmp16 = 4*mm2
  tmp17 = tmp13 + tmp16
  tmp18 = 1/tmp17
  tmp19 = 1/s25
  tmp20 = 1/tmp10
  LBK_LP_EP_Qm4 = 32*mm2*tmp1*tmp11*tmp8 + 32*mm2*tmp1*tmp15*tmp8 + 32*discbtmm*mm2*tmp1*tmp11*tmp&
                   &14*tmp18*tmp8 + 32*discbtmm*mm2*tmp1*tmp14*tmp15*tmp18*tmp8 - 32*tmp1*tmp14*tmp1&
                   &9*tmp20*tmp8 - 32*discbtmm*tmp1*tmp14**2*tmp18*tmp19*tmp20*tmp8

  END FUNCTION LBK_LP_EP_QM4

  FUNCTION LBK_LP_QE3QM(me2, mm2, ss, tt, s15, s25, s35)
  implicit none
  real LBK_LP_Qe3Qm
  real(kind=prec) ::  me2
  real(kind=prec) ::  mm2
  real(kind=prec) ::  ss
  real(kind=prec) ::  tt
  real(kind=prec) ::  s15
  real(kind=prec) ::  s25
  real(kind=prec) ::  s35
  real tmp1, tmp2, tmp3, tmp4, tmp5
  real tmp6, tmp7, tmp8, tmp9, tmp10
  real tmp11, tmp12, tmp13, tmp14, tmp15
  real tmp16, tmp17, tmp18, tmp19, tmp20
  real tmp21, tmp22, tmp23, tmp24, tmp25
  real tmp26, tmp27, tmp28, tmp29, tmp30
  real tmp31, tmp32, tmp33, tmp34, tmp35
  real tmp36, tmp37, tmp38, tmp39, tmp40
  real tmp41, tmp42, tmp43, tmp44, tmp45
  real tmp46, tmp47, tmp48, tmp49, tmp50
  real tmp51, tmp52, tmp53, tmp54, tmp55
  real tmp56, tmp57, tmp58, tmp59, tmp60
  real tmp61, tmp62, tmp63, tmp64, tmp65
  real tmp66, tmp67, tmp68, tmp69, tmp70
  real tmp71, tmp72, tmp73, tmp74, tmp75
  real tmp76, tmp77, tmp78, tmp79, tmp80
  real tmp81, tmp82, tmp83, tmp84, tmp85
  real tmp86, tmp87, tmp88, tmp89, tmp90
  real tmp91, tmp92, tmp93, tmp94, tmp95
  real tmp96, tmp97, tmp98, tmp99, tmp100
  real tmp101, tmp102, tmp103, tmp104, tmp105
  real tmp106, tmp107, tmp108, tmp109, tmp110
  real tmp111, tmp112, tmp113, tmp114, tmp115
  real tmp116, tmp117, tmp118, tmp119, tmp120
  real tmp121, tmp122, tmp123, tmp124, tmp125
  real tmp126, tmp127, tmp128, tmp129, tmp130
  real tmp131, tmp132, tmp133, tmp134, tmp135
  real tmp136, tmp137, tmp138, tmp139, tmp140
  real tmp141, tmp142, tmp143, tmp144, tmp145
  real tmp146, tmp147, tmp148, tmp149, tmp150
  real tmp151

  tmp1 = -tt
  tmp2 = 1/s15
  tmp3 = 1/s35
  tmp4 = 4*me2
  tmp5 = 4*mm2
  tmp6 = -2*ss
  tmp7 = tmp1 + tmp4 + tmp5 + tmp6
  tmp8 = -ss
  tmp9 = sqrt(me2)
  tmp10 = sqrt(mm2)
  tmp11 = me2 + mm2 + tmp1 + tmp8
  tmp12 = 1/tt
  tmp13 = tmp2**2
  tmp14 = -tmp10
  tmp15 = tmp14 + tmp9
  tmp16 = tmp15**2
  tmp17 = tmp1 + tmp16 + tmp8
  tmp18 = 1/tmp17
  tmp19 = tmp10 + tmp9
  tmp20 = tmp19**2
  tmp21 = tmp1 + tmp20 + tmp8
  tmp22 = 1/tmp21
  tmp23 = 2*me2
  tmp24 = 2*mm2
  tmp25 = tmp1 + tmp23 + tmp24 + tmp8
  tmp26 = tmp3**2
  tmp27 = 4*tmp10*tmp9
  tmp28 = tmp1 + tmp4
  tmp29 = 1/tmp28
  tmp30 = tmp1 + tmp27
  tmp31 = me2 + mm2 + tmp8
  tmp32 = 2*tmp31
  tmp33 = tmp1 + tmp32
  tmp34 = 1/tmp12 + tmp27
  tmp35 = -4*mm2
  tmp36 = 1/tmp12 + tmp35
  tmp37 = 1/tmp36
  tmp38 = 2*ss
  tmp39 = 1/tmp12 + tmp38
  tmp40 = me2**2
  tmp41 = mm2 + tmp8
  tmp42 = tmp41**2
  tmp43 = mm2 + ss
  tmp44 = -2*me2*tmp43
  tmp45 = tmp40 + tmp42 + tmp44
  tmp46 = 1/tmp45
  tmp47 = 8*tmp40
  tmp48 = (-8*me2)/tmp12
  tmp49 = tmp12**(-2)
  tmp50 = tmp47 + tmp48 + tmp49
  tmp51 = tmp1 + tmp5
  tmp52 = 1/tmp51
  tmp53 = mm2**2
  tmp54 = 8*tmp53
  tmp55 = (-8*mm2)/tmp12
  tmp56 = tmp49 + tmp54 + tmp55
  tmp57 = 1/s25
  tmp58 = 1/tmp49
  tmp59 = tmp31**2
  tmp60 = 2*tmp59
  tmp61 = tmp38/tmp12
  tmp62 = tmp49 + tmp60 + tmp61
  tmp63 = -(1/tmp3)
  tmp64 = 1/tmp2 + 1/tmp57 + tmp63
  tmp65 = 1/tmp64
  tmp66 = tmp16 + tmp8
  tmp67 = 1/tmp66
  tmp68 = tmp20 + tmp8
  tmp69 = 1/tmp68
  tmp70 = tmp1 + tmp23
  tmp71 = -2*me2
  tmp72 = 1/tmp12 + tmp71
  tmp73 = -me2
  tmp74 = -mm2
  tmp75 = ss + 1/tmp12 + tmp73 + tmp74
  tmp76 = 4*tmp59
  tmp77 = tmp49 + tmp61 + tmp76
  tmp78 = me2 + mm2
  tmp79 = (-4*tmp78)/tmp12
  tmp80 = (6*ss)/tmp12
  tmp81 = 3*tmp49
  tmp82 = tmp76 + tmp79 + tmp80 + tmp81
  tmp83 = 1/ss
  tmp84 = -2*tmp31
  tmp85 = 1/tmp12 + tmp84
  tmp86 = -2*mm2
  tmp87 = tmp72 + 1/tmp83 + tmp86
  tmp88 = 1/tmp87
  tmp89 = -tmp40
  tmp90 = tmp71/tmp83
  tmp91 = tmp42 + tmp89 + tmp90
  tmp92 = tmp5*tmp91
  tmp93 = 3*tmp53
  tmp94 = tmp24/tmp83
  tmp95 = tmp83**(-2)
  tmp96 = tmp24 + 1/tmp83
  tmp97 = tmp71*tmp96
  tmp98 = tmp40 + tmp93 + tmp94 + tmp95 + tmp97
  tmp99 = tmp98/tmp12
  tmp100 = me2 + tmp74 + 1/tmp83
  tmp101 = tmp100*tmp49
  tmp102 = tmp101 + tmp92 + tmp99
  tmp103 = 16*me2*tmp59
  tmp104 = 3*tmp40
  tmp105 = -7/tmp83
  tmp106 = mm2 + tmp105
  tmp107 = tmp106*tmp23
  tmp108 = 3*tmp42
  tmp109 = tmp104 + tmp107 + tmp108
  tmp110 = (-2*tmp109)/tmp12
  tmp111 = tmp23 + tmp8
  tmp112 = 6*tmp111*tmp49
  tmp113 = tmp12**(-3)
  tmp114 = -3*tmp113
  tmp115 = tmp103 + tmp110 + tmp112 + tmp114
  tmp116 = me2**3
  tmp117 = tmp41**3
  tmp118 = -tmp95
  tmp119 = tmp118 + tmp53
  tmp120 = tmp119/tmp12
  tmp121 = -3/tmp83
  tmp122 = 1/tmp12 + tmp121 + tmp74
  tmp123 = tmp122*tmp40
  tmp124 = -3*tmp95
  tmp125 = 1/tmp12 + 1/tmp83
  tmp126 = tmp125*tmp24
  tmp127 = tmp124 + tmp126 + tmp53
  tmp128 = tmp127*tmp73
  tmp129 = tmp116 + tmp117 + tmp120 + tmp123 + tmp128
  tmp130 = 3/tmp83
  tmp131 = tmp125 + tmp74
  tmp132 = tmp131**2
  tmp133 = mm2 + tmp125
  tmp134 = tmp133*tmp71
  tmp135 = tmp132 + tmp134 + tmp40
  tmp136 = 1/tmp135
  tmp137 = -tmp116
  tmp138 = -tmp117
  tmp139 = mm2 + tmp130
  tmp140 = tmp139*tmp40
  tmp141 = tmp74 + 1/tmp83
  tmp142 = (2*tmp141)/(tmp12*tmp83)
  tmp143 = tmp43*tmp49
  tmp144 = -4/tmp12
  tmp145 = tmp144 + 1/tmp83
  tmp146 = tmp145*tmp24
  tmp147 = tmp1 + tmp130
  tmp148 = -(tmp125*tmp147)
  tmp149 = tmp146 + tmp148 + tmp53
  tmp150 = me2*tmp149
  tmp151 = tmp137 + tmp138 + tmp140 + tmp142 + tmp143 + tmp150
  LBK_LP_Qe3Qm = -32*discbu*me2*tmp12*tmp13*tmp136*tmp151 - 32*discbu*me2*tmp12*tmp136*tmp151*tmp&
                  &26 - 32*discbu*tmp136*tmp151*tmp2*tmp3 + 64*discbu*me2*tmp12*tmp136*tmp151*tmp2*&
                  &tmp3 - 32*logt*me2*tmp12*tmp13*tmp29*tmp30*tmp33*tmp34*tmp37 - 32*logt*me2*tmp12&
                  &*tmp26*tmp29*tmp30*tmp33*tmp34*tmp37 - 32*logt*tmp2*tmp29*tmp3*tmp30*tmp33*tmp34&
                  &*tmp37 + 64*logt*me2*tmp12*tmp2*tmp29*tmp3*tmp30*tmp33*tmp34*tmp37 + 32*me2*scal&
                  &arc0ir6s*tmp12*tmp13*tmp31*tmp39 + 32*me2*scalarc0ir6s*tmp12*tmp26*tmp31*tmp39 +&
                  & 32*scalarc0ir6s*tmp2*tmp3*tmp31*tmp39 - 64*me2*scalarc0ir6s*tmp12*tmp2*tmp3*tmp&
                  &31*tmp39 - 32*me2*scalarc0tme*tmp12*tmp13*tmp29*tmp33*tmp50 - 32*me2*scalarc0tme&
                  &*tmp12*tmp26*tmp29*tmp33*tmp50 - 32*scalarc0tme*tmp2*tmp29*tmp3*tmp33*tmp50 + 64&
                  &*me2*scalarc0tme*tmp12*tmp2*tmp29*tmp3*tmp33*tmp50 - 32*me2*scalarc0tmm*tmp12*tm&
                  &p13*tmp33*tmp52*tmp56 - 32*me2*scalarc0tmm*tmp12*tmp26*tmp33*tmp52*tmp56 - 32*sc&
                  &alarc0tmm*tmp2*tmp3*tmp33*tmp52*tmp56 + 64*me2*scalarc0tmm*tmp12*tmp2*tmp3*tmp33&
                  &*tmp52*tmp56 - 16*discbtme*tmp115*tmp2*tmp29*tmp31*tmp57*tmp58 + 64*discbu*logmu&
                  &me*tmp11*tmp12*tmp18*tmp2*tmp22*tmp25*tmp3*tmp62 + 64*discbu*logmume*me2*tmp11*t&
                  &mp13*tmp18*tmp22*tmp25*tmp58*tmp62 + 64*discbu*logmume*me2*tmp11*tmp18*tmp22*tmp&
                  &25*tmp26*tmp58*tmp62 - 128*discbu*logmume*me2*tmp11*tmp18*tmp2*tmp22*tmp25*tmp3*&
                  &tmp58*tmp62 - 64*tmp2*tmp31*tmp57*tmp58*tmp62 - 32*logmume*tmp2*tmp31*tmp57*tmp5&
                  &8*tmp62 - 16*discbtme*tmp115*tmp29*tmp3*tmp31*tmp58*tmp65 - 64*tmp3*tmp31*tmp58*&
                  &tmp62*tmp65 - 32*logmume*tmp3*tmp31*tmp58*tmp62*tmp65 + 32*discbs*me2*tmp12*tmp1&
                  &29*tmp13*tmp67*tmp69 + 32*discbs*me2*tmp12*tmp129*tmp26*tmp67*tmp69 + 32*discbs*&
                  &tmp129*tmp2*tmp3*tmp67*tmp69 - 64*discbs*me2*tmp12*tmp129*tmp2*tmp3*tmp67*tmp69 &
                  &+ 32*me2*scalarc0ir6u*tmp11*tmp12*tmp13*tmp7 + 16*discbu*logmm*me2*tmp11*tmp12*t&
                  &mp13*tmp18*tmp22*tmp25*tmp7 + 32*me2*scalarc0ir6u*tmp11*tmp12*tmp26*tmp7 + 16*di&
                  &scbu*logmm*me2*tmp11*tmp12*tmp18*tmp22*tmp25*tmp26*tmp7 + 32*scalarc0ir6u*tmp11*&
                  &tmp2*tmp3*tmp7 - 64*me2*scalarc0ir6u*tmp11*tmp12*tmp2*tmp3*tmp7 + 16*discbu*logm&
                  &m*tmp11*tmp18*tmp2*tmp22*tmp25*tmp3*tmp7 - 32*discbu*logmm*me2*tmp11*tmp12*tmp18&
                  &*tmp2*tmp22*tmp25*tmp3*tmp7 - 32*discbtme*logmume*tmp2*tmp29*tmp31*tmp57*tmp58*t&
                  &mp62*tmp70 - 32*discbtme*logmume*tmp29*tmp3*tmp31*tmp58*tmp62*tmp65*tmp70 - 32*s&
                  &calarc0ir6tme*tmp2*tmp31*tmp57*tmp58*tmp62*tmp72 - 32*scalarc0ir6tme*tmp3*tmp31*&
                  &tmp58*tmp62*tmp65*tmp72 - 16*discbtme*tmp115*tmp29*tmp3*tmp57*tmp58*tmp75 - 64*t&
                  &mp3*tmp57*tmp58*tmp62*tmp75 - 32*logmume*tmp3*tmp57*tmp58*tmp62*tmp75 - 16*discb&
                  &tme*tmp115*tmp2*tmp29*tmp58*tmp65*tmp75 - 64*tmp2*tmp58*tmp62*tmp65*tmp75 - 32*l&
                  &ogmume*tmp2*tmp58*tmp62*tmp65*tmp75 - 32*discbtme*logmume*tmp29*tmp3*tmp57*tmp58&
                  &*tmp62*tmp70*tmp75 - 32*discbtme*logmume*tmp2*tmp29*tmp58*tmp62*tmp65*tmp70*tmp7&
                  &5 - 32*scalarc0ir6tme*tmp3*tmp57*tmp58*tmp62*tmp72*tmp75 - 32*scalarc0ir6tme*tmp&
                  &2*tmp58*tmp62*tmp65*tmp72*tmp75 + 32*discbulogt*tmp11*tmp12*tmp18*tmp2*tmp22*tmp&
                  &25*tmp3*tmp82 + 32*discbulogt*me2*tmp11*tmp13*tmp18*tmp22*tmp25*tmp58*tmp82 + 32&
                  &*discbulogt*me2*tmp11*tmp18*tmp22*tmp25*tmp26*tmp58*tmp82 - 64*discbulogt*me2*tm&
                  &p11*tmp18*tmp2*tmp22*tmp25*tmp3*tmp58*tmp82 + (16*discbs*logmm*me2*tmp12*tmp13*t&
                  &mp31*tmp39*tmp46)/tmp83 + (16*discbs*logmm*me2*tmp12*tmp26*tmp31*tmp39*tmp46)/tm&
                  &p83 + (16*discbs*logmm*tmp2*tmp3*tmp31*tmp39*tmp46)/tmp83 - (32*discbs*logmm*me2&
                  &*tmp12*tmp2*tmp3*tmp31*tmp39*tmp46)/tmp83 + (64*discbs*logmume*tmp12*tmp2*tmp3*t&
                  &mp31*tmp62*tmp67*tmp69)/tmp83 + (64*discbs*logmume*me2*tmp13*tmp31*tmp58*tmp62*t&
                  &mp67*tmp69)/tmp83 + (64*discbs*logmume*me2*tmp26*tmp31*tmp58*tmp62*tmp67*tmp69)/&
                  &tmp83 - (128*discbs*logmume*me2*tmp2*tmp3*tmp31*tmp58*tmp62*tmp67*tmp69)/tmp83 +&
                  & (32*discbslogt*tmp12*tmp2*tmp3*tmp31*tmp67*tmp69*tmp77)/tmp83 + (32*discbslogt*&
                  &me2*tmp13*tmp31*tmp58*tmp67*tmp69*tmp77)/tmp83 + (32*discbslogt*me2*tmp26*tmp31*&
                  &tmp58*tmp67*tmp69*tmp77)/tmp83 - (64*discbslogt*me2*tmp2*tmp3*tmp31*tmp58*tmp67*&
                  &tmp69*tmp77)/tmp83 + 16*logmm*me2*tmp102*tmp12*tmp13*tmp52*tmp83*tmp85*tmp88 + 1&
                  &6*logmm*me2*tmp102*tmp12*tmp26*tmp52*tmp83*tmp85*tmp88 + 16*logmm*tmp102*tmp2*tm&
                  &p3*tmp52*tmp83*tmp85*tmp88 - 32*logmm*me2*tmp102*tmp12*tmp2*tmp3*tmp52*tmp83*tmp&
                  &85*tmp88

  END FUNCTION LBK_LP_QE3QM

  FUNCTION LBK_LP_QEQM3(me2, mm2, ss, tt, s15, s25, s35)
  implicit none
  real LBK_LP_QeQm3
  real(kind=prec) ::  me2
  real(kind=prec) ::  mm2
  real(kind=prec) ::  ss
  real(kind=prec) ::  tt
  real(kind=prec) ::  s15
  real(kind=prec) ::  s25
  real(kind=prec) ::  s35
  real tmp1, tmp2, tmp3, tmp4, tmp5
  real tmp6, tmp7, tmp8, tmp9, tmp10
  real tmp11, tmp12, tmp13, tmp14, tmp15
  real tmp16, tmp17, tmp18, tmp19, tmp20
  real tmp21, tmp22, tmp23, tmp24, tmp25
  real tmp26, tmp27, tmp28, tmp29, tmp30
  real tmp31, tmp32, tmp33, tmp34, tmp35
  real tmp36, tmp37, tmp38, tmp39, tmp40
  real tmp41, tmp42, tmp43, tmp44, tmp45
  real tmp46, tmp47, tmp48, tmp49, tmp50
  real tmp51, tmp52, tmp53, tmp54, tmp55
  real tmp56, tmp57, tmp58, tmp59, tmp60
  real tmp61, tmp62, tmp63, tmp64, tmp65
  real tmp66, tmp67, tmp68, tmp69, tmp70
  real tmp71, tmp72, tmp73, tmp74, tmp75
  real tmp76, tmp77, tmp78, tmp79, tmp80
  real tmp81, tmp82, tmp83, tmp84, tmp85
  real tmp86, tmp87, tmp88, tmp89, tmp90
  real tmp91, tmp92, tmp93, tmp94, tmp95
  real tmp96, tmp97, tmp98, tmp99, tmp100
  real tmp101, tmp102, tmp103, tmp104, tmp105
  real tmp106, tmp107, tmp108, tmp109, tmp110
  real tmp111, tmp112, tmp113, tmp114, tmp115
  real tmp116, tmp117, tmp118, tmp119, tmp120
  real tmp121, tmp122, tmp123, tmp124, tmp125
  real tmp126, tmp127, tmp128, tmp129, tmp130
  real tmp131, tmp132, tmp133, tmp134, tmp135
  real tmp136, tmp137, tmp138, tmp139, tmp140
  real tmp141, tmp142, tmp143, tmp144, tmp145
  real tmp146, tmp147, tmp148, tmp149, tmp150
  real tmp151

  tmp1 = -tt
  tmp2 = 4*me2
  tmp3 = 4*mm2
  tmp4 = -2*ss
  tmp5 = tmp1 + tmp2 + tmp3 + tmp4
  tmp6 = -ss
  tmp7 = me2 + mm2 + tmp1 + tmp6
  tmp8 = 1/tt
  tmp9 = -s35
  tmp10 = s15 + s25 + tmp9
  tmp11 = s25**(-2)
  tmp12 = sqrt(me2)
  tmp13 = sqrt(mm2)
  tmp14 = 2*mm2
  tmp15 = tmp10**(-2)
  tmp16 = -tmp13
  tmp17 = tmp12 + tmp16
  tmp18 = tmp17**2
  tmp19 = tmp1 + tmp18 + tmp6
  tmp20 = 1/tmp19
  tmp21 = tmp12 + tmp13
  tmp22 = tmp21**2
  tmp23 = tmp1 + tmp22 + tmp6
  tmp24 = 1/tmp23
  tmp25 = 2*me2
  tmp26 = tmp1 + tmp14 + tmp25 + tmp6
  tmp27 = 1/s25
  tmp28 = 1/tmp10
  tmp29 = tmp1 + tmp14
  tmp30 = 4*tmp12*tmp13
  tmp31 = tmp1 + tmp2
  tmp32 = 1/tmp31
  tmp33 = tmp1 + tmp30
  tmp34 = me2 + mm2 + tmp6
  tmp35 = 2*tmp34
  tmp36 = tmp1 + tmp35
  tmp37 = tmp30 + 1/tmp8
  tmp38 = -4*mm2
  tmp39 = tmp38 + 1/tmp8
  tmp40 = 1/tmp39
  tmp41 = 2*ss
  tmp42 = tmp41 + 1/tmp8
  tmp43 = me2**2
  tmp44 = mm2 + tmp6
  tmp45 = tmp44**2
  tmp46 = mm2 + ss
  tmp47 = -2*me2*tmp46
  tmp48 = tmp43 + tmp45 + tmp47
  tmp49 = 1/tmp48
  tmp50 = 8*tmp43
  tmp51 = (-8*me2)/tmp8
  tmp52 = tmp8**(-2)
  tmp53 = tmp50 + tmp51 + tmp52
  tmp54 = tmp1 + tmp3
  tmp55 = 1/tmp54
  tmp56 = mm2**2
  tmp57 = 8*tmp56
  tmp58 = (-8*mm2)/tmp8
  tmp59 = tmp52 + tmp57 + tmp58
  tmp60 = 1/s15
  tmp61 = 1/tmp52
  tmp62 = tmp34**2
  tmp63 = 2*tmp62
  tmp64 = tmp41/tmp8
  tmp65 = tmp52 + tmp63 + tmp64
  tmp66 = 1/s35
  tmp67 = tmp18 + tmp6
  tmp68 = 1/tmp67
  tmp69 = tmp22 + tmp6
  tmp70 = 1/tmp69
  tmp71 = -2*mm2
  tmp72 = tmp71 + 1/tmp8
  tmp73 = -me2
  tmp74 = -mm2
  tmp75 = ss + tmp73 + tmp74 + 1/tmp8
  tmp76 = 4*tmp62
  tmp77 = tmp52 + tmp64 + tmp76
  tmp78 = me2 + mm2
  tmp79 = (-4*tmp78)/tmp8
  tmp80 = (6*ss)/tmp8
  tmp81 = 3*tmp52
  tmp82 = tmp76 + tmp79 + tmp80 + tmp81
  tmp83 = 1/ss
  tmp84 = -2*tmp34
  tmp85 = 1/tmp8 + tmp84
  tmp86 = -2*me2
  tmp87 = tmp72 + 1/tmp83 + tmp86
  tmp88 = 1/tmp87
  tmp89 = -tmp43
  tmp90 = tmp86/tmp83
  tmp91 = tmp45 + tmp89 + tmp90
  tmp92 = tmp3*tmp91
  tmp93 = 3*tmp56
  tmp94 = tmp14/tmp83
  tmp95 = tmp83**(-2)
  tmp96 = tmp14 + 1/tmp83
  tmp97 = tmp86*tmp96
  tmp98 = tmp43 + tmp93 + tmp94 + tmp95 + tmp97
  tmp99 = tmp98/tmp8
  tmp100 = me2 + tmp74 + 1/tmp83
  tmp101 = tmp100*tmp52
  tmp102 = tmp101 + tmp92 + tmp99
  tmp103 = 16*mm2*tmp62
  tmp104 = 3*tmp43
  tmp105 = -3/tmp83
  tmp106 = mm2 + tmp105
  tmp107 = tmp106*tmp25
  tmp108 = (-14*mm2)/tmp83
  tmp109 = 3*tmp95
  tmp110 = tmp104 + tmp107 + tmp108 + tmp109 + tmp93
  tmp111 = (-2*tmp110)/tmp8
  tmp112 = tmp14 + tmp6
  tmp113 = 6*tmp112*tmp52
  tmp114 = tmp8**(-3)
  tmp115 = -3*tmp114
  tmp116 = tmp103 + tmp111 + tmp113 + tmp115
  tmp117 = me2**3
  tmp118 = tmp44**3
  tmp119 = -tmp95
  tmp120 = tmp119 + tmp56
  tmp121 = tmp120/tmp8
  tmp122 = tmp105 + tmp74 + 1/tmp8
  tmp123 = tmp122*tmp43
  tmp124 = -3*tmp95
  tmp125 = 1/tmp8 + 1/tmp83
  tmp126 = tmp125*tmp14
  tmp127 = tmp124 + tmp126 + tmp56
  tmp128 = tmp127*tmp73
  tmp129 = tmp117 + tmp118 + tmp121 + tmp123 + tmp128
  tmp130 = 3/tmp83
  tmp131 = tmp125 + tmp74
  tmp132 = tmp131**2
  tmp133 = mm2 + tmp125
  tmp134 = tmp133*tmp86
  tmp135 = tmp132 + tmp134 + tmp43
  tmp136 = 1/tmp135
  tmp137 = -tmp117
  tmp138 = -tmp118
  tmp139 = mm2 + tmp130
  tmp140 = tmp139*tmp43
  tmp141 = tmp74 + 1/tmp83
  tmp142 = (2*tmp141)/(tmp8*tmp83)
  tmp143 = tmp46*tmp52
  tmp144 = -4/tmp8
  tmp145 = tmp144 + 1/tmp83
  tmp146 = tmp14*tmp145
  tmp147 = tmp1 + tmp130
  tmp148 = -(tmp125*tmp147)
  tmp149 = tmp146 + tmp148 + tmp56
  tmp150 = me2*tmp149
  tmp151 = tmp137 + tmp138 + tmp140 + tmp142 + tmp143 + tmp150
  LBK_LP_QeQm3 = -16*discbtmm*tmp116*tmp27*tmp34*tmp55*tmp60*tmp61 - 64*tmp27*tmp34*tmp60*tmp61*t&
                  &mp65 - 32*logmumm*tmp27*tmp34*tmp60*tmp61*tmp65 - 32*discbtmm*logmumm*tmp27*tmp2&
                  &9*tmp34*tmp55*tmp60*tmp61*tmp65 - 16*discbtmm*tmp116*tmp28*tmp34*tmp55*tmp61*tmp&
                  &66 - 64*tmp28*tmp34*tmp61*tmp65*tmp66 - 32*logmumm*tmp28*tmp34*tmp61*tmp65*tmp66&
                  & - 32*discbtmm*logmumm*tmp28*tmp29*tmp34*tmp55*tmp61*tmp65*tmp66 + 64*discbu*log&
                  &mume*mm2*tmp11*tmp20*tmp24*tmp26*tmp61*tmp65*tmp7 + 64*discbu*logmume*mm2*tmp15*&
                  &tmp20*tmp24*tmp26*tmp61*tmp65*tmp7 - 64*discbu*logmume*tmp20*tmp24*tmp26*tmp27*t&
                  &mp28*tmp29*tmp61*tmp65*tmp7 - 32*scalarc0ir6tmm*tmp27*tmp34*tmp60*tmp61*tmp65*tm&
                  &p72 - 32*scalarc0ir6tmm*tmp28*tmp34*tmp61*tmp65*tmp66*tmp72 - 16*discbtmm*tmp116&
                  &*tmp28*tmp55*tmp60*tmp61*tmp75 - 64*tmp28*tmp60*tmp61*tmp65*tmp75 - 32*logmumm*t&
                  &mp28*tmp60*tmp61*tmp65*tmp75 - 32*discbtmm*logmumm*tmp28*tmp29*tmp55*tmp60*tmp61&
                  &*tmp65*tmp75 - 16*discbtmm*tmp116*tmp27*tmp55*tmp61*tmp66*tmp75 - 64*tmp27*tmp61&
                  &*tmp65*tmp66*tmp75 - 32*logmumm*tmp27*tmp61*tmp65*tmp66*tmp75 - 32*discbtmm*logm&
                  &umm*tmp27*tmp29*tmp55*tmp61*tmp65*tmp66*tmp75 - 32*scalarc0ir6tmm*tmp28*tmp60*tm&
                  &p61*tmp65*tmp72*tmp75 - 32*scalarc0ir6tmm*tmp27*tmp61*tmp65*tmp66*tmp72*tmp75 - &
                  &32*discbu*mm2*tmp11*tmp136*tmp151*tmp8 - 32*discbu*mm2*tmp136*tmp15*tmp151*tmp8 &
                  &+ 32*discbu*tmp136*tmp151*tmp27*tmp28*tmp29*tmp8 - 32*logt*mm2*tmp11*tmp32*tmp33&
                  &*tmp36*tmp37*tmp40*tmp8 - 32*logt*mm2*tmp15*tmp32*tmp33*tmp36*tmp37*tmp40*tmp8 +&
                  & 32*logt*tmp27*tmp28*tmp29*tmp32*tmp33*tmp36*tmp37*tmp40*tmp8 + 32*mm2*scalarc0i&
                  &r6s*tmp11*tmp34*tmp42*tmp8 + 32*mm2*scalarc0ir6s*tmp15*tmp34*tmp42*tmp8 - 32*sca&
                  &larc0ir6s*tmp27*tmp28*tmp29*tmp34*tmp42*tmp8 - 32*mm2*scalarc0tme*tmp11*tmp32*tm&
                  &p36*tmp53*tmp8 - 32*mm2*scalarc0tme*tmp15*tmp32*tmp36*tmp53*tmp8 + 32*scalarc0tm&
                  &e*tmp27*tmp28*tmp29*tmp32*tmp36*tmp53*tmp8 - 32*mm2*scalarc0tmm*tmp11*tmp36*tmp5&
                  &5*tmp59*tmp8 - 32*mm2*scalarc0tmm*tmp15*tmp36*tmp55*tmp59*tmp8 + 32*scalarc0tmm*&
                  &tmp27*tmp28*tmp29*tmp36*tmp55*tmp59*tmp8 + 32*mm2*scalarc0ir6u*tmp11*tmp5*tmp7*t&
                  &mp8 + 32*mm2*scalarc0ir6u*tmp15*tmp5*tmp7*tmp8 + 16*discbu*logmm*mm2*tmp11*tmp20&
                  &*tmp24*tmp26*tmp5*tmp7*tmp8 + 16*discbu*logmm*mm2*tmp15*tmp20*tmp24*tmp26*tmp5*t&
                  &mp7*tmp8 - 32*scalarc0ir6u*tmp27*tmp28*tmp29*tmp5*tmp7*tmp8 - 16*discbu*logmm*tm&
                  &p20*tmp24*tmp26*tmp27*tmp28*tmp29*tmp5*tmp7*tmp8 + 32*discbs*mm2*tmp11*tmp129*tm&
                  &p68*tmp70*tmp8 + 32*discbs*mm2*tmp129*tmp15*tmp68*tmp70*tmp8 - 32*discbs*tmp129*&
                  &tmp27*tmp28*tmp29*tmp68*tmp70*tmp8 + 32*discbulogt*mm2*tmp11*tmp20*tmp24*tmp26*t&
                  &mp61*tmp7*tmp82 + 32*discbulogt*mm2*tmp15*tmp20*tmp24*tmp26*tmp61*tmp7*tmp82 - 3&
                  &2*discbulogt*tmp20*tmp24*tmp26*tmp27*tmp28*tmp29*tmp61*tmp7*tmp82 + (64*discbs*l&
                  &ogmume*mm2*tmp11*tmp34*tmp61*tmp65*tmp68*tmp70)/tmp83 + (64*discbs*logmume*mm2*t&
                  &mp15*tmp34*tmp61*tmp65*tmp68*tmp70)/tmp83 - (64*discbs*logmume*tmp27*tmp28*tmp29&
                  &*tmp34*tmp61*tmp65*tmp68*tmp70)/tmp83 + (32*discbslogt*mm2*tmp11*tmp34*tmp61*tmp&
                  &68*tmp70*tmp77)/tmp83 + (32*discbslogt*mm2*tmp15*tmp34*tmp61*tmp68*tmp70*tmp77)/&
                  &tmp83 - (32*discbslogt*tmp27*tmp28*tmp29*tmp34*tmp61*tmp68*tmp70*tmp77)/tmp83 + &
                  &(16*discbs*logmm*mm2*tmp11*tmp34*tmp42*tmp49*tmp8)/tmp83 + (16*discbs*logmm*mm2*&
                  &tmp15*tmp34*tmp42*tmp49*tmp8)/tmp83 - (16*discbs*logmm*tmp27*tmp28*tmp29*tmp34*t&
                  &mp42*tmp49*tmp8)/tmp83 + 16*logmm*mm2*tmp102*tmp11*tmp55*tmp8*tmp83*tmp85*tmp88 &
                  &+ 16*logmm*mm2*tmp102*tmp15*tmp55*tmp8*tmp83*tmp85*tmp88 - 16*logmm*tmp102*tmp27&
                  &*tmp28*tmp29*tmp55*tmp8*tmp83*tmp85*tmp88

  END FUNCTION LBK_LP_QEQM3

  FUNCTION LBK_LP_QE2QM2(me2, mm2, ss, tt, s15, s25, s35)
  implicit none
  real LBK_LP_Qe2Qm2
  real(kind=prec) ::  me2
  real(kind=prec) ::  mm2
  real(kind=prec) ::  ss
  real(kind=prec) ::  tt
  real(kind=prec) ::  s15
  real(kind=prec) ::  s25
  real(kind=prec) ::  s35
  real tmp1, tmp2, tmp3, tmp4, tmp5
  real tmp6, tmp7, tmp8, tmp9, tmp10
  real tmp11, tmp12, tmp13, tmp14, tmp15
  real tmp16, tmp17, tmp18, tmp19, tmp20
  real tmp21, tmp22, tmp23, tmp24, tmp25
  real tmp26, tmp27, tmp28, tmp29, tmp30
  real tmp31, tmp32, tmp33, tmp34, tmp35
  real tmp36, tmp37, tmp38, tmp39, tmp40
  real tmp41, tmp42, tmp43, tmp44, tmp45
  real tmp46, tmp47, tmp48, tmp49, tmp50
  real tmp51, tmp52, tmp53, tmp54, tmp55
  real tmp56, tmp57, tmp58, tmp59, tmp60
  real tmp61, tmp62, tmp63, tmp64, tmp65
  real tmp66, tmp67, tmp68, tmp69, tmp70
  real tmp71, tmp72, tmp73, tmp74, tmp75
  real tmp76, tmp77, tmp78, tmp79, tmp80
  real tmp81, tmp82, tmp83, tmp84, tmp85
  real tmp86, tmp87, tmp88, tmp89, tmp90
  real tmp91, tmp92, tmp93, tmp94, tmp95
  real tmp96, tmp97, tmp98, tmp99, tmp100
  real tmp101, tmp102, tmp103, tmp104, tmp105
  real tmp106, tmp107, tmp108, tmp109, tmp110
  real tmp111, tmp112, tmp113, tmp114, tmp115
  real tmp116, tmp117, tmp118, tmp119, tmp120
  real tmp121, tmp122, tmp123, tmp124, tmp125
  real tmp126, tmp127, tmp128, tmp129, tmp130
  real tmp131, tmp132, tmp133, tmp134, tmp135
  real tmp136, tmp137, tmp138, tmp139, tmp140
  real tmp141, tmp142, tmp143, tmp144, tmp145
  real tmp146, tmp147, tmp148, tmp149, tmp150
  real tmp151, tmp152, tmp153, tmp154, tmp155
  real tmp156, tmp157, tmp158, tmp159, tmp160
  real tmp161, tmp162, tmp163, tmp164, tmp165

  tmp1 = -ss
  tmp2 = -tt
  tmp3 = me2 + mm2 + tmp1
  tmp4 = 4*me2
  tmp5 = 4*mm2
  tmp6 = -2*ss
  tmp7 = tmp2 + tmp4 + tmp5 + tmp6
  tmp8 = tmp2 + tmp3
  tmp9 = 1/tt
  tmp10 = 1/s15
  tmp11 = 1/s25
  tmp12 = sqrt(me2)
  tmp13 = sqrt(mm2)
  tmp14 = -s35
  tmp15 = 1/tmp10 + 1/tmp11 + tmp14
  tmp16 = 1/tmp15
  tmp17 = 1/s35
  tmp18 = -tmp13
  tmp19 = tmp12 + tmp18
  tmp20 = tmp19**2
  tmp21 = tmp1 + tmp2 + tmp20
  tmp22 = 1/tmp21
  tmp23 = tmp12 + tmp13
  tmp24 = tmp23**2
  tmp25 = tmp1 + tmp2 + tmp24
  tmp26 = 1/tmp25
  tmp27 = 2*me2
  tmp28 = 2*mm2
  tmp29 = tmp1 + tmp2 + tmp27 + tmp28
  tmp30 = 4*tmp12*tmp13
  tmp31 = tmp2 + tmp4
  tmp32 = 1/tmp31
  tmp33 = tmp2 + tmp30
  tmp34 = 2*tmp3
  tmp35 = tmp2 + tmp34
  tmp36 = tmp30 + 1/tmp9
  tmp37 = -4*mm2
  tmp38 = tmp37 + 1/tmp9
  tmp39 = 1/tmp38
  tmp40 = -me2
  tmp41 = -mm2
  tmp42 = ss + tmp40 + tmp41 + 1/tmp9
  tmp43 = tmp3**2
  tmp44 = 2*ss
  tmp45 = tmp44 + 1/tmp9
  tmp46 = me2**2
  tmp47 = mm2 + tmp1
  tmp48 = tmp47**2
  tmp49 = mm2 + ss
  tmp50 = -2*me2*tmp49
  tmp51 = tmp46 + tmp48 + tmp50
  tmp52 = 1/tmp51
  tmp53 = 8*tmp46
  tmp54 = (-8*me2)/tmp9
  tmp55 = tmp9**(-2)
  tmp56 = tmp53 + tmp54 + tmp55
  tmp57 = tmp2 + tmp5
  tmp58 = 1/tmp57
  tmp59 = mm2**2
  tmp60 = 8*tmp59
  tmp61 = (-8*mm2)/tmp9
  tmp62 = tmp55 + tmp60 + tmp61
  tmp63 = tmp10**2
  tmp64 = 1/tmp55
  tmp65 = 2*tmp43
  tmp66 = tmp44/tmp9
  tmp67 = tmp55 + tmp65 + tmp66
  tmp68 = tmp11**2
  tmp69 = tmp16**2
  tmp70 = tmp17**2
  tmp71 = tmp1 + tmp20
  tmp72 = 1/tmp71
  tmp73 = tmp1 + tmp24
  tmp74 = 1/tmp73
  tmp75 = tmp2 + tmp27
  tmp76 = tmp2 + tmp28
  tmp77 = -2*me2
  tmp78 = tmp77 + 1/tmp9
  tmp79 = -2*mm2
  tmp80 = tmp79 + 1/tmp9
  tmp81 = 4*tmp43
  tmp82 = tmp55 + tmp66 + tmp81
  tmp83 = me2 + mm2
  tmp84 = (-4*tmp83)/tmp9
  tmp85 = (6*ss)/tmp9
  tmp86 = 3*tmp55
  tmp87 = tmp81 + tmp84 + tmp85 + tmp86
  tmp88 = 1/ss
  tmp89 = -2*tmp3
  tmp90 = tmp89 + 1/tmp9
  tmp91 = tmp77 + tmp80 + 1/tmp88
  tmp92 = 1/tmp91
  tmp93 = -tmp46
  tmp94 = tmp77/tmp88
  tmp95 = tmp48 + tmp93 + tmp94
  tmp96 = tmp5*tmp95
  tmp97 = 3*tmp59
  tmp98 = tmp28/tmp88
  tmp99 = tmp88**(-2)
  tmp100 = tmp28 + 1/tmp88
  tmp101 = tmp100*tmp77
  tmp102 = tmp101 + tmp46 + tmp97 + tmp98 + tmp99
  tmp103 = tmp102/tmp9
  tmp104 = me2 + tmp41 + 1/tmp88
  tmp105 = tmp104*tmp55
  tmp106 = tmp103 + tmp105 + tmp96
  tmp107 = 16*me2*tmp43
  tmp108 = 3*tmp46
  tmp109 = -7/tmp88
  tmp110 = mm2 + tmp109
  tmp111 = tmp110*tmp27
  tmp112 = 3*tmp48
  tmp113 = tmp108 + tmp111 + tmp112
  tmp114 = (-2*tmp113)/tmp9
  tmp115 = tmp1 + tmp27
  tmp116 = 6*tmp115*tmp55
  tmp117 = tmp9**(-3)
  tmp118 = -3*tmp117
  tmp119 = tmp107 + tmp114 + tmp116 + tmp118
  tmp120 = 16*mm2*tmp43
  tmp121 = -3/tmp88
  tmp122 = mm2 + tmp121
  tmp123 = tmp122*tmp27
  tmp124 = (-14*mm2)/tmp88
  tmp125 = 3*tmp99
  tmp126 = tmp108 + tmp123 + tmp124 + tmp125 + tmp97
  tmp127 = (-2*tmp126)/tmp9
  tmp128 = tmp1 + tmp28
  tmp129 = 6*tmp128*tmp55
  tmp130 = tmp118 + tmp120 + tmp127 + tmp129
  tmp131 = me2**3
  tmp132 = tmp47**3
  tmp133 = -tmp99
  tmp134 = tmp133 + tmp59
  tmp135 = tmp134/tmp9
  tmp136 = tmp121 + tmp41 + 1/tmp9
  tmp137 = tmp136*tmp46
  tmp138 = -3*tmp99
  tmp139 = 1/tmp88 + 1/tmp9
  tmp140 = tmp139*tmp28
  tmp141 = tmp138 + tmp140 + tmp59
  tmp142 = tmp141*tmp40
  tmp143 = tmp131 + tmp132 + tmp135 + tmp137 + tmp142
  tmp144 = 3/tmp88
  tmp145 = tmp139 + tmp41
  tmp146 = tmp145**2
  tmp147 = mm2 + tmp139
  tmp148 = tmp147*tmp77
  tmp149 = tmp146 + tmp148 + tmp46
  tmp150 = 1/tmp149
  tmp151 = -tmp131
  tmp152 = -tmp132
  tmp153 = mm2 + tmp144
  tmp154 = tmp153*tmp46
  tmp155 = tmp41 + 1/tmp88
  tmp156 = (2*tmp155)/(tmp88*tmp9)
  tmp157 = tmp49*tmp55
  tmp158 = -4/tmp9
  tmp159 = tmp158 + 1/tmp88
  tmp160 = tmp159*tmp28
  tmp161 = tmp144 + tmp2
  tmp162 = -(tmp139*tmp161)
  tmp163 = tmp160 + tmp162 + tmp59
  tmp164 = me2*tmp163
  tmp165 = tmp151 + tmp152 + tmp154 + tmp156 + tmp157 + tmp164
  LBK_LP_Qe2Qm2 = -32*discbtmm*me2*tmp10*tmp130*tmp17*tmp58*tmp64 + 16*discbtmm*me2*tmp130*tmp58*t&
                   &mp63*tmp64 - 128*me2*tmp10*tmp17*tmp64*tmp67 - 64*logmumm*me2*tmp10*tmp17*tmp64*&
                   &tmp67 + 64*me2*tmp63*tmp64*tmp67 + 32*logmumm*me2*tmp63*tmp64*tmp67 + 16*discbtm&
                   &e*mm2*tmp119*tmp32*tmp64*tmp68 + 64*mm2*tmp64*tmp67*tmp68 + 32*logmume*mm2*tmp64&
                   &*tmp67*tmp68 + 16*discbtme*mm2*tmp119*tmp32*tmp64*tmp69 + 64*mm2*tmp64*tmp67*tmp&
                   &69 + 32*logmume*mm2*tmp64*tmp67*tmp69 + 16*discbtmm*me2*tmp130*tmp58*tmp64*tmp70&
                   & + 64*me2*tmp64*tmp67*tmp70 + 32*logmumm*me2*tmp64*tmp67*tmp70 + 32*discbtme*log&
                   &mume*mm2*tmp32*tmp64*tmp67*tmp68*tmp75 + 32*discbtme*logmume*mm2*tmp32*tmp64*tmp&
                   &67*tmp69*tmp75 - 16*discbtme*tmp11*tmp119*tmp16*tmp32*tmp64*tmp76 - 64*tmp11*tmp&
                   &16*tmp64*tmp67*tmp76 - 32*logmume*tmp11*tmp16*tmp64*tmp67*tmp76 - 64*discbtmm*lo&
                   &gmumm*me2*tmp10*tmp17*tmp58*tmp64*tmp67*tmp76 + 32*discbtmm*logmumm*me2*tmp58*tm&
                   &p63*tmp64*tmp67*tmp76 + 32*discbtmm*logmumm*me2*tmp58*tmp64*tmp67*tmp70*tmp76 - &
                   &32*discbtme*logmume*tmp11*tmp16*tmp32*tmp64*tmp67*tmp75*tmp76 + 32*mm2*scalarc0i&
                   &r6tme*tmp64*tmp67*tmp68*tmp78 + 32*mm2*scalarc0ir6tme*tmp64*tmp67*tmp69*tmp78 - &
                   &32*scalarc0ir6tme*tmp11*tmp16*tmp64*tmp67*tmp76*tmp78 - 64*discbu*logmume*tmp10*&
                   &tmp11*tmp22*tmp26*tmp29*tmp3*tmp64*tmp67*tmp8 - 64*discbu*logmume*tmp16*tmp17*tm&
                   &p22*tmp26*tmp29*tmp3*tmp64*tmp67*tmp8 - 64*discbu*logmume*tmp10*tmp16*tmp22*tmp2&
                   &6*tmp29*tmp42*tmp64*tmp67*tmp8 - 64*discbu*logmume*tmp11*tmp17*tmp22*tmp26*tmp29&
                   &*tmp42*tmp64*tmp67*tmp8 - 64*me2*scalarc0ir6tmm*tmp10*tmp17*tmp64*tmp67*tmp80 + &
                   &32*me2*scalarc0ir6tmm*tmp63*tmp64*tmp67*tmp80 + 32*me2*scalarc0ir6tmm*tmp64*tmp6&
                   &7*tmp70*tmp80 - 32*discbulogt*tmp10*tmp11*tmp22*tmp26*tmp29*tmp3*tmp64*tmp8*tmp8&
                   &7 - 32*discbulogt*tmp16*tmp17*tmp22*tmp26*tmp29*tmp3*tmp64*tmp8*tmp87 - 32*discb&
                   &ulogt*tmp10*tmp16*tmp22*tmp26*tmp29*tmp42*tmp64*tmp8*tmp87 - 32*discbulogt*tmp11&
                   &*tmp17*tmp22*tmp26*tmp29*tmp42*tmp64*tmp8*tmp87 - (64*discbs*logmume*tmp10*tmp16&
                   &*tmp3*tmp42*tmp64*tmp67*tmp72*tmp74)/tmp88 - (64*discbs*logmume*tmp11*tmp17*tmp3&
                   &*tmp42*tmp64*tmp67*tmp72*tmp74)/tmp88 - (64*discbs*logmume*tmp10*tmp11*tmp43*tmp&
                   &64*tmp67*tmp72*tmp74)/tmp88 - (64*discbs*logmume*tmp16*tmp17*tmp43*tmp64*tmp67*t&
                   &mp72*tmp74)/tmp88 - (32*discbslogt*tmp10*tmp16*tmp3*tmp42*tmp64*tmp72*tmp74*tmp8&
                   &2)/tmp88 - (32*discbslogt*tmp11*tmp17*tmp3*tmp42*tmp64*tmp72*tmp74*tmp82)/tmp88 &
                   &- (32*discbslogt*tmp10*tmp11*tmp43*tmp64*tmp72*tmp74*tmp82)/tmp88 - (32*discbslo&
                   &gt*tmp16*tmp17*tmp43*tmp64*tmp72*tmp74*tmp82)/tmp88 + 32*discbu*tmp10*tmp11*tmp1&
                   &50*tmp165*tmp3*tmp9 + 32*discbu*tmp150*tmp16*tmp165*tmp17*tmp3*tmp9 + 32*logt*tm&
                   &p10*tmp11*tmp3*tmp32*tmp33*tmp35*tmp36*tmp39*tmp9 + 32*logt*tmp16*tmp17*tmp3*tmp&
                   &32*tmp33*tmp35*tmp36*tmp39*tmp9 + 32*discbu*tmp10*tmp150*tmp16*tmp165*tmp42*tmp9&
                   & + 32*discbu*tmp11*tmp150*tmp165*tmp17*tmp42*tmp9 + 32*logt*tmp10*tmp16*tmp32*tm&
                   &p33*tmp35*tmp36*tmp39*tmp42*tmp9 + 32*logt*tmp11*tmp17*tmp32*tmp33*tmp35*tmp36*t&
                   &mp39*tmp42*tmp9 - 32*scalarc0ir6s*tmp10*tmp16*tmp3*tmp42*tmp45*tmp9 - 32*scalarc&
                   &0ir6s*tmp11*tmp17*tmp3*tmp42*tmp45*tmp9 - 32*scalarc0ir6s*tmp10*tmp11*tmp43*tmp4&
                   &5*tmp9 - 32*scalarc0ir6s*tmp16*tmp17*tmp43*tmp45*tmp9 + 32*scalarc0tme*tmp10*tmp&
                   &11*tmp3*tmp32*tmp35*tmp56*tmp9 + 32*scalarc0tme*tmp16*tmp17*tmp3*tmp32*tmp35*tmp&
                   &56*tmp9 + 32*scalarc0tme*tmp10*tmp16*tmp32*tmp35*tmp42*tmp56*tmp9 + 32*scalarc0t&
                   &me*tmp11*tmp17*tmp32*tmp35*tmp42*tmp56*tmp9 + 16*discbtmm*tmp10*tmp130*tmp17*tmp&
                   &58*tmp9 + 32*scalarc0tmm*tmp10*tmp11*tmp3*tmp35*tmp58*tmp62*tmp9 + 32*scalarc0tm&
                   &m*tmp16*tmp17*tmp3*tmp35*tmp58*tmp62*tmp9 + 32*scalarc0tmm*tmp10*tmp16*tmp35*tmp&
                   &42*tmp58*tmp62*tmp9 + 32*scalarc0tmm*tmp11*tmp17*tmp35*tmp42*tmp58*tmp62*tmp9 + &
                   &64*tmp10*tmp17*tmp67*tmp9 + 32*logmumm*tmp10*tmp17*tmp67*tmp9 - 32*discbs*tmp10*&
                   &tmp11*tmp143*tmp3*tmp72*tmp74*tmp9 - 32*discbs*tmp143*tmp16*tmp17*tmp3*tmp72*tmp&
                   &74*tmp9 - 32*discbs*tmp10*tmp143*tmp16*tmp42*tmp72*tmp74*tmp9 - 32*discbs*tmp11*&
                   &tmp143*tmp17*tmp42*tmp72*tmp74*tmp9 + 32*discbtmm*logmumm*tmp10*tmp17*tmp58*tmp6&
                   &7*tmp76*tmp9 - 32*scalarc0ir6u*tmp10*tmp11*tmp3*tmp7*tmp8*tmp9 - 32*scalarc0ir6u&
                   &*tmp16*tmp17*tmp3*tmp7*tmp8*tmp9 - 16*discbu*logmm*tmp10*tmp11*tmp22*tmp26*tmp29&
                   &*tmp3*tmp7*tmp8*tmp9 - 16*discbu*logmm*tmp16*tmp17*tmp22*tmp26*tmp29*tmp3*tmp7*t&
                   &mp8*tmp9 - 32*scalarc0ir6u*tmp10*tmp16*tmp42*tmp7*tmp8*tmp9 - 32*scalarc0ir6u*tm&
                   &p11*tmp17*tmp42*tmp7*tmp8*tmp9 - 16*discbu*logmm*tmp10*tmp16*tmp22*tmp26*tmp29*t&
                   &mp42*tmp7*tmp8*tmp9 - 16*discbu*logmm*tmp11*tmp17*tmp22*tmp26*tmp29*tmp42*tmp7*t&
                   &mp8*tmp9 + 32*scalarc0ir6tmm*tmp10*tmp17*tmp67*tmp80*tmp9 - (16*discbs*logmm*tmp&
                   &10*tmp16*tmp3*tmp42*tmp45*tmp52*tmp9)/tmp88 - (16*discbs*logmm*tmp11*tmp17*tmp3*&
                   &tmp42*tmp45*tmp52*tmp9)/tmp88 - (16*discbs*logmm*tmp10*tmp11*tmp43*tmp45*tmp52*t&
                   &mp9)/tmp88 - (16*discbs*logmm*tmp16*tmp17*tmp43*tmp45*tmp52*tmp9)/tmp88 - 16*log&
                   &mm*tmp10*tmp106*tmp11*tmp3*tmp58*tmp88*tmp9*tmp90*tmp92 - 16*logmm*tmp106*tmp16*&
                   &tmp17*tmp3*tmp58*tmp88*tmp9*tmp90*tmp92 - 16*logmm*tmp10*tmp106*tmp16*tmp42*tmp5&
                   &8*tmp88*tmp9*tmp90*tmp92 - 16*logmm*tmp106*tmp11*tmp17*tmp42*tmp58*tmp88*tmp9*tm&
                   &p90*tmp92

  END FUNCTION LBK_LP_QE2QM2

  FUNCTION LBK_LP_QE4(me2, mm2, ss, tt, s15, s25, s35)
  implicit none
  real LBK_LP_Qe4
  real(kind=prec) ::  me2
  real(kind=prec) ::  mm2
  real(kind=prec) ::  ss
  real(kind=prec) ::  tt
  real(kind=prec) ::  s15
  real(kind=prec) ::  s25
  real(kind=prec) ::  s35
  real tmp1, tmp2, tmp3, tmp4, tmp5
  real tmp6, tmp7, tmp8, tmp9, tmp10
  real tmp11, tmp12, tmp13, tmp14, tmp15
  real tmp16, tmp17, tmp18, tmp19, tmp20
  real tmp21, tmp22, tmp23, tmp24, tmp25
  real tmp26, tmp27, tmp28, tmp29, tmp30
  real tmp31, tmp32, tmp33, tmp34, tmp35
  real tmp36, tmp37

  tmp1 = s15**(-2)
  tmp2 = tt**(-2)
  tmp3 = -ss
  tmp4 = me2 + mm2 + tmp3
  tmp5 = tmp4**2
  tmp6 = 2*tmp5
  tmp7 = 2*ss*tt
  tmp8 = 1/tmp2
  tmp9 = tmp6 + tmp7 + tmp8
  tmp10 = s35**(-2)
  tmp11 = 1/s15
  tmp12 = 1/s35
  tmp13 = -tt
  tmp14 = 2*me2
  tmp15 = tmp13 + tmp14
  tmp16 = 4*me2
  tmp17 = tmp13 + tmp16
  tmp18 = 1/tmp17
  tmp19 = 1/tt
  tmp20 = -2*me2
  tmp21 = 1/tmp19 + tmp20
  tmp22 = 16*me2*tmp5
  tmp23 = me2**2
  tmp24 = 3*tmp23
  tmp25 = -7*ss
  tmp26 = mm2 + tmp25
  tmp27 = tmp14*tmp26
  tmp28 = mm2 + tmp3
  tmp29 = tmp28**2
  tmp30 = 3*tmp29
  tmp31 = tmp24 + tmp27 + tmp30
  tmp32 = (-2*tmp31)/tmp19
  tmp33 = tmp14 + tmp3
  tmp34 = 6*tmp33*tmp8
  tmp35 = tmp19**(-3)
  tmp36 = -3*tmp35
  tmp37 = tmp22 + tmp32 + tmp34 + tmp36
  LBK_LP_Qe4 = 16*discbtme*tmp11*tmp12*tmp18*tmp19*tmp37 + 16*discbtme*me2*tmp1*tmp18*tmp2*tmp3&
                &7 + 16*discbtme*me2*tmp10*tmp18*tmp2*tmp37 - 32*discbtme*me2*tmp11*tmp12*tmp18*t&
                &mp2*tmp37 + 64*tmp11*tmp12*tmp19*tmp9 + 32*logmume*tmp11*tmp12*tmp19*tmp9 + 32*d&
                &iscbtme*logmume*tmp11*tmp12*tmp15*tmp18*tmp19*tmp9 + 64*me2*tmp1*tmp2*tmp9 + 32*&
                &logmume*me2*tmp1*tmp2*tmp9 + 64*me2*tmp10*tmp2*tmp9 + 32*logmume*me2*tmp10*tmp2*&
                &tmp9 - 128*me2*tmp11*tmp12*tmp2*tmp9 - 64*logmume*me2*tmp11*tmp12*tmp2*tmp9 + 32&
                &*discbtme*logmume*me2*tmp1*tmp15*tmp18*tmp2*tmp9 + 32*discbtme*logmume*me2*tmp10&
                &*tmp15*tmp18*tmp2*tmp9 - 64*discbtme*logmume*me2*tmp11*tmp12*tmp15*tmp18*tmp2*tm&
                &p9 + 32*scalarc0ir6tme*tmp11*tmp12*tmp19*tmp21*tmp9 + 32*me2*scalarc0ir6tme*tmp1&
                &*tmp2*tmp21*tmp9 + 32*me2*scalarc0ir6tme*tmp10*tmp2*tmp21*tmp9 - 64*me2*scalarc0&
                &ir6tme*tmp11*tmp12*tmp2*tmp21*tmp9
  END FUNCTION LBK_LP_QE4

  FUNCTION LBK_LP_QM4(me2, mm2, ss, tt, s15, s25, s35)
  implicit none
  real LBK_LP_Qm4
  real(kind=prec) ::  me2
  real(kind=prec) ::  mm2
  real(kind=prec) ::  ss
  real(kind=prec) ::  tt
  real(kind=prec) ::  s15
  real(kind=prec) ::  s25
  real(kind=prec) ::  s35
  real tmp1, tmp2, tmp3, tmp4, tmp5
  real tmp6, tmp7, tmp8, tmp9, tmp10
  real tmp11, tmp12, tmp13, tmp14, tmp15
  real tmp16, tmp17, tmp18, tmp19, tmp20
  real tmp21, tmp22, tmp23, tmp24, tmp25
  real tmp26, tmp27, tmp28, tmp29, tmp30
  real tmp31, tmp32, tmp33, tmp34, tmp35
  real tmp36, tmp37, tmp38, tmp39, tmp40

  tmp1 = s25**(-2)
  tmp2 = tt**(-2)
  tmp3 = -ss
  tmp4 = me2 + mm2 + tmp3
  tmp5 = tmp4**2
  tmp6 = 2*tmp5
  tmp7 = 2*ss*tt
  tmp8 = 1/tmp2
  tmp9 = tmp6 + tmp7 + tmp8
  tmp10 = -s35
  tmp11 = s15 + s25 + tmp10
  tmp12 = tmp11**(-2)
  tmp13 = 1/s25
  tmp14 = 1/tmp11
  tmp15 = 2*mm2
  tmp16 = -tt
  tmp17 = tmp15 + tmp16
  tmp18 = 4*mm2
  tmp19 = tmp16 + tmp18
  tmp20 = 1/tmp19
  tmp21 = -2*mm2
  tmp22 = tmp21 + tt
  tmp23 = 16*mm2*tmp5
  tmp24 = me2**2
  tmp25 = 3*tmp24
  tmp26 = mm2**2
  tmp27 = 3*tmp26
  tmp28 = -3*ss
  tmp29 = mm2 + tmp28
  tmp30 = 2*me2*tmp29
  tmp31 = -14*mm2*ss
  tmp32 = ss**2
  tmp33 = 3*tmp32
  tmp34 = tmp25 + tmp27 + tmp30 + tmp31 + tmp33
  tmp35 = -2*tmp34*tt
  tmp36 = tmp15 + tmp3
  tmp37 = 6*tmp36*tmp8
  tmp38 = tt**3
  tmp39 = -3*tmp38
  tmp40 = tmp23 + tmp35 + tmp37 + tmp39
  LBK_LP_Qm4 = 16*discbtmm*mm2*tmp1*tmp2*tmp20*tmp40 + 16*discbtmm*mm2*tmp12*tmp2*tmp20*tmp40 -&
                & 16*discbtmm*tmp13*tmp14*tmp17*tmp2*tmp20*tmp40 + 64*mm2*tmp1*tmp2*tmp9 + 32*log&
                &mumm*mm2*tmp1*tmp2*tmp9 + 64*mm2*tmp12*tmp2*tmp9 + 32*logmumm*mm2*tmp12*tmp2*tmp&
                &9 - 64*tmp13*tmp14*tmp17*tmp2*tmp9 - 32*logmumm*tmp13*tmp14*tmp17*tmp2*tmp9 + 32&
                &*discbtmm*logmumm*mm2*tmp1*tmp17*tmp2*tmp20*tmp9 + 32*discbtmm*logmumm*mm2*tmp12&
                &*tmp17*tmp2*tmp20*tmp9 - 32*discbtmm*logmumm*tmp13*tmp14*tmp17**2*tmp2*tmp20*tmp&
                &9 + 32*mm2*scalarc0ir6tmm*tmp1*tmp2*tmp22*tmp9 + 32*mm2*scalarc0ir6tmm*tmp12*tmp&
                &2*tmp22*tmp9 - 32*scalarc0ir6tmm*tmp13*tmp14*tmp17*tmp2*tmp22*tmp9

  END FUNCTION LBK_LP_QM4

  FUNCTION LBK_NLP_EP_QE2QM2(me2, mm2, ss, tt, s15, s25, s35)
  implicit none
  real LBK_NLP_EP_Qe2Qm2
  real(kind=prec) ::  me2
  real(kind=prec) ::  mm2
  real(kind=prec) ::  ss
  real(kind=prec) ::  tt
  real(kind=prec) ::  s15
  real(kind=prec) ::  s25
  real(kind=prec) ::  s35
  real tmp1, tmp2, tmp3, tmp4, tmp5
  real tmp6, tmp7, tmp8, tmp9, tmp10
  real tmp11, tmp12, tmp13, tmp14, tmp15
  real tmp16, tmp17, tmp18, tmp19, tmp20
  real tmp21, tmp22, tmp23, tmp24, tmp25
  real tmp26, tmp27, tmp28, tmp29, tmp30
  real tmp31, tmp32, tmp33, tmp34, tmp35
  real tmp36, tmp37, tmp38, tmp39, tmp40
  real tmp41, tmp42, tmp43, tmp44, tmp45
  real tmp46, tmp47, tmp48, tmp49, tmp50
  real tmp51, tmp52, tmp53, tmp54, tmp55
  real tmp56, tmp57, tmp58, tmp59, tmp60
  real tmp61, tmp62, tmp63, tmp64, tmp65
  real tmp66, tmp67, tmp68, tmp69, tmp70
  real tmp71, tmp72, tmp73, tmp74, tmp75
  real tmp76, tmp77, tmp78, tmp79, tmp80
  real tmp81, tmp82, tmp83, tmp84, tmp85
  real tmp86, tmp87, tmp88, tmp89, tmp90
  real tmp91, tmp92, tmp93, tmp94, tmp95
  real tmp96, tmp97, tmp98, tmp99, tmp100
  real tmp101, tmp102, tmp103, tmp104, tmp105
  real tmp106

  tmp1 = -tt
  tmp2 = 1/s25
  tmp3 = -ss
  tmp4 = me2 + mm2 + tmp3
  tmp5 = 4*me2
  tmp6 = tmp1 + tmp5
  tmp7 = 1/tmp6
  tmp8 = 2*me2
  tmp9 = 2*mm2
  tmp10 = -2*ss
  tmp11 = tmp1 + tmp10 + tmp8 + tmp9
  tmp12 = tt**(-2)
  tmp13 = 1/s15
  tmp14 = 4*mm2
  tmp15 = tmp1 + tmp14
  tmp16 = 1/tmp15
  tmp17 = -s35
  tmp18 = 1/tmp13 + tmp17 + 1/tmp2
  tmp19 = 1/tmp18
  tmp20 = tmp1 + tmp4
  tmp21 = 1/s35
  tmp22 = 1/tt
  tmp23 = tmp2**2
  tmp24 = -4*me2
  tmp25 = 1/tmp22 + tmp24
  tmp26 = 1/tmp25
  tmp27 = tmp1 + tmp9
  tmp28 = tmp13**2
  tmp29 = -4*mm2
  tmp30 = 1/tmp22 + tmp29
  tmp31 = 1/tmp30
  tmp32 = tmp1 + tmp8
  tmp33 = sqrt(me2)
  tmp34 = sqrt(mm2)
  tmp35 = -tmp34
  tmp36 = tmp33 + tmp35
  tmp37 = tmp36**2
  tmp38 = tmp3 + tmp37
  tmp39 = 1/tmp38
  tmp40 = tmp33 + tmp34
  tmp41 = tmp40**2
  tmp42 = tmp3 + tmp41
  tmp43 = 1/tmp42
  tmp44 = -4*tmp4
  tmp45 = 2/tmp22
  tmp46 = tmp44 + tmp45
  tmp47 = -me2
  tmp48 = -mm2
  tmp49 = ss + tmp47 + tmp48
  tmp50 = tmp1 + tmp38
  tmp51 = 1/tmp50
  tmp52 = tmp1 + tmp42
  tmp53 = 1/tmp52
  tmp54 = tmp3 + tmp32 + tmp9
  tmp55 = 1/tmp22 + tmp49
  tmp56 = 2*ss
  tmp57 = tmp45 + tmp56
  tmp58 = tmp19**2
  tmp59 = tmp22**3
  tmp60 = tmp4**2
  tmp61 = 2*tmp60
  tmp62 = tmp56/tmp22
  tmp63 = tmp22**(-2)
  tmp64 = tmp61 + tmp62 + tmp63
  tmp65 = tmp43**2
  tmp66 = tmp39**2
  tmp67 = 1/ss
  tmp68 = -tmp67
  tmp69 = me2**2
  tmp70 = -2*me2*mm2
  tmp71 = mm2**2
  tmp72 = (-2*me2)/tmp67
  tmp73 = (-2*mm2)/tmp67
  tmp74 = tmp67**(-2)
  tmp75 = tmp69 + tmp70 + tmp71 + tmp72 + tmp73 + tmp74
  tmp76 = 1/tmp75
  tmp77 = tmp49*tmp76
  tmp78 = tmp68 + tmp77
  tmp79 = tmp7**2
  tmp80 = tmp53**2
  tmp81 = tmp51**2
  tmp82 = 1/tmp13 + 1/tmp2
  tmp83 = -(1/tmp13)
  tmp84 = -(1/tmp2)
  tmp85 = 1/tmp21 + tmp83 + tmp84
  tmp86 = 1/tmp85
  tmp87 = 2*tmp69
  tmp88 = me2*tmp14
  tmp89 = 2*tmp71
  tmp90 = tmp24/tmp67
  tmp91 = tmp29/tmp67
  tmp92 = 2*tmp74
  tmp93 = tmp62 + tmp63 + tmp87 + tmp88 + tmp89 + tmp90 + tmp91 + tmp92
  tmp94 = -tmp22
  tmp95 = -2*me2
  tmp96 = 1/tmp22 + tmp95
  tmp97 = tmp22*tmp26*tmp96
  tmp98 = tmp94 + tmp97
  tmp99 = 1/tmp54
  tmp100 = -tmp99
  tmp101 = tmp95/tmp22
  tmp102 = (-2*mm2)/tmp22
  tmp103 = tmp101 + tmp102 + tmp62 + tmp63 + tmp75
  tmp104 = 1/tmp103
  tmp105 = tmp104*tmp20
  tmp106 = tmp100 + tmp105
  LBK_NLP_EP_Qe2Qm2 = -128*mm2*tmp11*tmp12*tmp16*tmp20*tmp21 - 64*discbtmm*mm2*tmp11*tmp12*tmp16*tmp20&
                       &*tmp21 + 32*tmp11*tmp16*tmp20*tmp21*tmp22 + 32*discbtmm*tmp11*tmp16*tmp20*tmp21*&
                       &tmp22 - (256*me2*mm2*tmp11*tmp12*tmp23*tmp26)/tmp13 - (128*discbtme*me2*mm2*tmp1&
                       &1*tmp12*tmp23*tmp26)/tmp13 + (64*mm2*tmp11*tmp22*tmp23*tmp26)/tmp13 + (64*discbt&
                       &me*mm2*tmp11*tmp22*tmp23*tmp26)/tmp13 + (128*me2*tmp11*tmp12*tmp19*tmp2*tmp26*tm&
                       &p27)/tmp13 + (64*discbtme*me2*tmp11*tmp12*tmp19*tmp2*tmp26*tmp27)/tmp13 - (32*tm&
                       &p11*tmp19*tmp2*tmp22*tmp26*tmp27)/tmp13 - (32*discbtme*tmp11*tmp19*tmp2*tmp22*tm&
                       &p26*tmp27)/tmp13 - (256*me2*mm2*tmp11*tmp12*tmp28*tmp31)/tmp2 - (128*discbtmm*me&
                       &2*mm2*tmp11*tmp12*tmp28*tmp31)/tmp2 + (64*me2*tmp11*tmp22*tmp28*tmp31)/tmp2 + (6&
                       &4*discbtmm*me2*tmp11*tmp22*tmp28*tmp31)/tmp2 + (128*mm2*tmp11*tmp12*tmp13*tmp21*&
                       &tmp31*tmp32)/tmp2 + (64*discbtmm*mm2*tmp11*tmp12*tmp13*tmp21*tmp31*tmp32)/tmp2 -&
                       & (32*tmp11*tmp13*tmp21*tmp22*tmp31*tmp32)/tmp2 - (32*discbtmm*tmp11*tmp13*tmp21*&
                       &tmp22*tmp31*tmp32)/tmp2 + 128*mm2*tmp11*tmp12*tmp13*tmp16*tmp4 + 64*discbtmm*mm2&
                       &*tmp11*tmp12*tmp13*tmp16*tmp4 - 32*tmp11*tmp13*tmp16*tmp22*tmp4 - 32*discbtmm*tm&
                       &p11*tmp13*tmp16*tmp22*tmp4 + 32*mm2*tmp12*tmp19*tmp57 - 32*mm2*tmp12*tmp2*tmp57 &
                       &+ (32*mm2*tmp12*tmp23*tmp57)/tmp19 + 32*tmp12*tmp19*tmp27*tmp57 - 32*tmp12*tmp2*&
                       &tmp27*tmp57 - (32*mm2*tmp12*tmp57*tmp58)/tmp2 - 64*me2*tmp12*tmp13*tmp39*tmp4*tm&
                       &p43*tmp64 + 64*discbs*me2*tmp12*tmp13*tmp39*tmp4*tmp43*tmp64 - 64*mm2*tmp12*tmp2&
                       &*tmp39*tmp4*tmp43*tmp64 + 64*discbs*mm2*tmp12*tmp2*tmp39*tmp4*tmp43*tmp64 + 32*t&
                       &mp12*tmp19*tmp27*tmp39*tmp4*tmp43*tmp64 - 32*discbs*tmp12*tmp19*tmp27*tmp39*tmp4&
                       &*tmp43*tmp64 + 32*tmp12*tmp21*tmp32*tmp39*tmp4*tmp43*tmp64 - 32*discbs*tmp12*tmp&
                       &21*tmp32*tmp39*tmp4*tmp43*tmp64 + 32*tmp12*tmp13*tmp39*tmp4*tmp43*tmp49*tmp64 - &
                       &32*discbs*tmp12*tmp13*tmp39*tmp4*tmp43*tmp49*tmp64 + 32*tmp12*tmp2*tmp39*tmp4*tm&
                       &p43*tmp49*tmp64 - 32*discbs*tmp12*tmp2*tmp39*tmp4*tmp43*tmp49*tmp64 + 64*me2*tmp&
                       &12*tmp13*tmp20*tmp51*tmp53*tmp64 - 64*discbu*me2*tmp12*tmp13*tmp20*tmp51*tmp53*t&
                       &mp64 + 64*mm2*tmp12*tmp2*tmp20*tmp51*tmp53*tmp64 - 64*discbu*mm2*tmp12*tmp2*tmp2&
                       &0*tmp51*tmp53*tmp64 - 32*tmp12*tmp19*tmp20*tmp27*tmp51*tmp53*tmp64 + 32*discbu*t&
                       &mp12*tmp19*tmp20*tmp27*tmp51*tmp53*tmp64 - 32*tmp12*tmp20*tmp21*tmp32*tmp51*tmp5&
                       &3*tmp64 + 32*discbu*tmp12*tmp20*tmp21*tmp32*tmp51*tmp53*tmp64 - 64*tmp12*tmp13*t&
                       &mp20*tmp49*tmp51*tmp53*tmp64 + 64*discbu*tmp12*tmp13*tmp20*tmp49*tmp51*tmp53*tmp&
                       &64 - 32*tmp12*tmp2*tmp20*tmp49*tmp51*tmp53*tmp64 + 32*discbu*tmp12*tmp2*tmp20*tm&
                       &p49*tmp51*tmp53*tmp64 + (32*tmp12*tmp13*tmp2*tmp20*tmp49*tmp51*tmp53*tmp64)/tmp1&
                       &9 - (32*discbu*tmp12*tmp13*tmp2*tmp20*tmp49*tmp51*tmp53*tmp64)/tmp19 + 32*tmp12*&
                       &tmp20*tmp21*tmp49*tmp51*tmp53*tmp64 - 32*discbu*tmp12*tmp20*tmp21*tmp49*tmp51*tm&
                       &p53*tmp64 - (32*tmp12*tmp19*tmp20*tmp21*tmp49*tmp51*tmp53*tmp64)/tmp2 + (32*disc&
                       &bu*tmp12*tmp19*tmp20*tmp21*tmp49*tmp51*tmp53*tmp64)/tmp2 - (32*tmp12*tmp13*tmp19&
                       &*tmp39*tmp4*tmp43*tmp55*tmp64)/tmp2 + (32*discbs*tmp12*tmp13*tmp19*tmp39*tmp4*tm&
                       &p43*tmp55*tmp64)/tmp2 - (32*tmp12*tmp2*tmp21*tmp39*tmp4*tmp43*tmp55*tmp64)/tmp13&
                       & + (32*discbs*tmp12*tmp2*tmp21*tmp39*tmp4*tmp43*tmp55*tmp64)/tmp13 - 32*tmp12*tm&
                       &p13*tmp20*tmp51*tmp53*tmp55*tmp64 + 32*discbu*tmp12*tmp13*tmp20*tmp51*tmp53*tmp5&
                       &5*tmp64 + (64*tmp12*tmp13*tmp19*tmp20*tmp51*tmp53*tmp55*tmp64)/tmp2 - (64*discbu&
                       &*tmp12*tmp13*tmp19*tmp20*tmp51*tmp53*tmp55*tmp64)/tmp2 + 32*tmp12*tmp20*tmp21*tm&
                       &p51*tmp53*tmp55*tmp64 - 32*discbu*tmp12*tmp20*tmp21*tmp51*tmp53*tmp55*tmp64 + (3&
                       &2*tmp12*tmp2*tmp20*tmp21*tmp51*tmp53*tmp55*tmp64)/tmp13 - (32*discbu*tmp12*tmp2*&
                       &tmp20*tmp21*tmp51*tmp53*tmp55*tmp64)/tmp13 - (32*tmp12*tmp2*tmp20*tmp21*tmp51*tm&
                       &p53*tmp55*tmp64)/tmp19 + (32*discbu*tmp12*tmp2*tmp20*tmp21*tmp51*tmp53*tmp55*tmp&
                       &64)/tmp19 - 64*mm2*tmp19*tmp59*tmp64 + 64*mm2*tmp2*tmp59*tmp64 - (64*mm2*tmp23*t&
                       &mp59*tmp64)/tmp19 - 64*tmp19*tmp27*tmp59*tmp64 + 64*tmp2*tmp27*tmp59*tmp64 + (64&
                       &*mm2*tmp58*tmp59*tmp64)/tmp2 + (64*discbs*me2*tmp12*tmp13*tmp39*tmp4*tmp43*tmp46&
                       &)/tmp67 + (64*discbs*mm2*tmp12*tmp2*tmp39*tmp4*tmp43*tmp46)/tmp67 - (32*discbs*t&
                       &mp12*tmp19*tmp27*tmp39*tmp4*tmp43*tmp46)/tmp67 - (32*discbs*tmp12*tmp21*tmp32*tm&
                       &p39*tmp4*tmp43*tmp46)/tmp67 - (32*discbs*tmp12*tmp13*tmp39*tmp4*tmp43*tmp46*tmp4&
                       &9)/tmp67 - (32*discbs*tmp12*tmp2*tmp39*tmp4*tmp43*tmp46*tmp49)/tmp67 + (32*discb&
                       &s*tmp12*tmp13*tmp19*tmp39*tmp4*tmp43*tmp46*tmp55)/(tmp2*tmp67) + (32*discbs*tmp1&
                       &2*tmp2*tmp21*tmp39*tmp4*tmp43*tmp46*tmp55)/(tmp13*tmp67) - (32*discbs*tmp12*tmp1&
                       &3*tmp39*tmp4*tmp43*tmp49*tmp57)/tmp67 + (32*discbs*tmp12*tmp13*tmp2*tmp39*tmp4*t&
                       &mp43*tmp49*tmp57)/(tmp19*tmp67) + (32*discbs*tmp12*tmp21*tmp39*tmp4*tmp43*tmp49*&
                       &tmp57)/tmp67 - (32*discbs*tmp12*tmp19*tmp21*tmp39*tmp4*tmp43*tmp49*tmp57)/(tmp2*&
                       &tmp67) - (32*discbs*tmp12*tmp13*tmp39*tmp4*tmp43*tmp55*tmp57)/tmp67 + (32*discbs&
                       &*tmp12*tmp13*tmp19*tmp39*tmp4*tmp43*tmp55*tmp57)/(tmp2*tmp67) + (32*discbs*tmp12&
                       &*tmp21*tmp39*tmp4*tmp43*tmp55*tmp57)/tmp67 - (32*discbs*tmp12*tmp2*tmp21*tmp39*t&
                       &mp4*tmp43*tmp55*tmp57)/(tmp19*tmp67) - (64*discbs*me2*tmp12*tmp13*tmp39*tmp43*tm&
                       &p64)/tmp67 - (64*discbs*mm2*tmp12*tmp2*tmp39*tmp43*tmp64)/tmp67 + (32*discbs*tmp&
                       &12*tmp19*tmp27*tmp39*tmp43*tmp64)/tmp67 + (32*discbs*tmp12*tmp21*tmp32*tmp39*tmp&
                       &43*tmp64)/tmp67 + (64*discbs*tmp12*tmp13*tmp19*tmp39*tmp4*tmp43*tmp64)/(tmp21*tm&
                       &p67) + (64*discbs*tmp12*tmp21*tmp39*tmp4*tmp43*tmp64)/tmp67 + (32*discbs*tmp12*t&
                       &mp13*tmp39*tmp43*tmp49*tmp64)/tmp67 + (32*discbs*tmp12*tmp2*tmp39*tmp43*tmp49*tm&
                       &p64)/tmp67 - (32*discbs*tmp12*tmp13*tmp19*tmp39*tmp43*tmp55*tmp64)/(tmp2*tmp67) &
                       &- (32*discbs*tmp12*tmp2*tmp21*tmp39*tmp43*tmp55*tmp64)/(tmp13*tmp67) + (64*discb&
                       &s*tmp13*tmp39*tmp4*tmp43*tmp49*tmp59*tmp64)/tmp67 - (64*discbs*tmp13*tmp2*tmp39*&
                       &tmp4*tmp43*tmp49*tmp59*tmp64)/(tmp19*tmp67) - (64*discbs*tmp21*tmp39*tmp4*tmp43*&
                       &tmp49*tmp59*tmp64)/tmp67 + (64*discbs*tmp19*tmp21*tmp39*tmp4*tmp43*tmp49*tmp59*t&
                       &mp64)/(tmp2*tmp67) + (64*discbs*tmp13*tmp39*tmp4*tmp43*tmp55*tmp59*tmp64)/tmp67 &
                       &- (64*discbs*tmp13*tmp19*tmp39*tmp4*tmp43*tmp55*tmp59*tmp64)/(tmp2*tmp67) - (64*&
                       &discbs*tmp21*tmp39*tmp4*tmp43*tmp55*tmp59*tmp64)/tmp67 + (64*discbs*tmp2*tmp21*t&
                       &mp39*tmp4*tmp43*tmp55*tmp59*tmp64)/(tmp19*tmp67) + (64*discbs*me2*tmp12*tmp13*tm&
                       &p39*tmp4*tmp64*tmp65)/tmp67 + (64*discbs*mm2*tmp12*tmp2*tmp39*tmp4*tmp64*tmp65)/&
                       &tmp67 - (32*discbs*tmp12*tmp19*tmp27*tmp39*tmp4*tmp64*tmp65)/tmp67 - (32*discbs*&
                       &tmp12*tmp21*tmp32*tmp39*tmp4*tmp64*tmp65)/tmp67 - (32*discbs*tmp12*tmp13*tmp39*t&
                       &mp4*tmp49*tmp64*tmp65)/tmp67 - (32*discbs*tmp12*tmp2*tmp39*tmp4*tmp49*tmp64*tmp6&
                       &5)/tmp67 + (32*discbs*tmp12*tmp13*tmp19*tmp39*tmp4*tmp55*tmp64*tmp65)/(tmp2*tmp6&
                       &7) + (32*discbs*tmp12*tmp2*tmp21*tmp39*tmp4*tmp55*tmp64*tmp65)/(tmp13*tmp67) + (&
                       &64*discbs*me2*tmp12*tmp13*tmp4*tmp43*tmp64*tmp66)/tmp67 + (64*discbs*mm2*tmp12*t&
                       &mp2*tmp4*tmp43*tmp64*tmp66)/tmp67 - (32*discbs*tmp12*tmp19*tmp27*tmp4*tmp43*tmp6&
                       &4*tmp66)/tmp67 - (32*discbs*tmp12*tmp21*tmp32*tmp4*tmp43*tmp64*tmp66)/tmp67 - (3&
                       &2*discbs*tmp12*tmp13*tmp4*tmp43*tmp49*tmp64*tmp66)/tmp67 - (32*discbs*tmp12*tmp2&
                       &*tmp4*tmp43*tmp49*tmp64*tmp66)/tmp67 + (32*discbs*tmp12*tmp13*tmp19*tmp4*tmp43*t&
                       &mp55*tmp64*tmp66)/(tmp2*tmp67) + (32*discbs*tmp12*tmp2*tmp21*tmp4*tmp43*tmp55*tm&
                       &p64*tmp66)/(tmp13*tmp67) - 128*me2*tmp11*tmp12*tmp19*tmp20*tmp7 - 64*discbtme*me&
                       &2*tmp11*tmp12*tmp19*tmp20*tmp7 + 32*tmp11*tmp19*tmp20*tmp22*tmp7 + 32*discbtme*t&
                       &mp11*tmp19*tmp20*tmp22*tmp7 + 128*me2*tmp11*tmp12*tmp2*tmp4*tmp7 + 64*discbtme*m&
                       &e2*tmp11*tmp12*tmp2*tmp4*tmp7 - 32*tmp11*tmp2*tmp22*tmp4*tmp7 - 32*discbtme*tmp1&
                       &1*tmp2*tmp22*tmp4*tmp7 + 32*discbtme*mm2*tmp12*tmp19*tmp32*tmp57*tmp7 - 32*discb&
                       &tme*mm2*tmp12*tmp2*tmp32*tmp57*tmp7 + (32*discbtme*mm2*tmp12*tmp23*tmp32*tmp57*t&
                       &mp7)/tmp19 + 32*discbtme*tmp12*tmp19*tmp27*tmp32*tmp57*tmp7 - 32*discbtme*tmp12*&
                       &tmp2*tmp27*tmp32*tmp57*tmp7 - (32*discbtme*mm2*tmp12*tmp32*tmp57*tmp58*tmp7)/tmp&
                       &2 - 32*discbtme*mm2*tmp12*tmp19*tmp64*tmp7 + 32*discbtme*mm2*tmp12*tmp2*tmp64*tm&
                       &p7 - (32*discbtme*mm2*tmp12*tmp23*tmp64*tmp7)/tmp19 - 32*discbtme*tmp12*tmp19*tm&
                       &p27*tmp64*tmp7 + 32*discbtme*tmp12*tmp2*tmp27*tmp64*tmp7 + (32*discbtme*mm2*tmp1&
                       &2*tmp58*tmp64*tmp7)/tmp2 - 32*mm2*tmp19*tmp32*tmp59*tmp64*tmp7 - 64*discbtme*mm2&
                       &*tmp19*tmp32*tmp59*tmp64*tmp7 + 32*mm2*tmp2*tmp32*tmp59*tmp64*tmp7 + 64*discbtme&
                       &*mm2*tmp2*tmp32*tmp59*tmp64*tmp7 - (32*mm2*tmp23*tmp32*tmp59*tmp64*tmp7)/tmp19 -&
                       & (64*discbtme*mm2*tmp23*tmp32*tmp59*tmp64*tmp7)/tmp19 - 32*tmp19*tmp27*tmp32*tmp&
                       &59*tmp64*tmp7 - 64*discbtme*tmp19*tmp27*tmp32*tmp59*tmp64*tmp7 + 32*tmp2*tmp27*t&
                       &mp32*tmp59*tmp64*tmp7 + 64*discbtme*tmp2*tmp27*tmp32*tmp59*tmp64*tmp7 + (32*mm2*&
                       &tmp32*tmp58*tmp59*tmp64*tmp7)/tmp2 + (64*discbtme*mm2*tmp32*tmp58*tmp59*tmp64*tm&
                       &p7)/tmp2 + (64*discbs*me2*tmp12*tmp13*tmp39*tmp4*tmp43*tmp64*tmp78)/tmp67 + (64*&
                       &discbs*mm2*tmp12*tmp2*tmp39*tmp4*tmp43*tmp64*tmp78)/tmp67 - (32*discbs*tmp12*tmp&
                       &19*tmp27*tmp39*tmp4*tmp43*tmp64*tmp78)/tmp67 - (32*discbs*tmp12*tmp21*tmp32*tmp3&
                       &9*tmp4*tmp43*tmp64*tmp78)/tmp67 - (32*discbs*tmp12*tmp13*tmp39*tmp4*tmp43*tmp49*&
                       &tmp64*tmp78)/tmp67 - (32*discbs*tmp12*tmp2*tmp39*tmp4*tmp43*tmp49*tmp64*tmp78)/t&
                       &mp67 + (32*discbs*tmp12*tmp13*tmp19*tmp39*tmp4*tmp43*tmp55*tmp64*tmp78)/(tmp2*tm&
                       &p67) + (32*discbs*tmp12*tmp2*tmp21*tmp39*tmp4*tmp43*tmp55*tmp64*tmp78)/(tmp13*tm&
                       &p67) + 32*discbtme*mm2*tmp12*tmp19*tmp32*tmp64*tmp79 - 32*discbtme*mm2*tmp12*tmp&
                       &2*tmp32*tmp64*tmp79 + (32*discbtme*mm2*tmp12*tmp23*tmp32*tmp64*tmp79)/tmp19 + 32&
                       &*discbtme*tmp12*tmp19*tmp27*tmp32*tmp64*tmp79 - 32*discbtme*tmp12*tmp2*tmp27*tmp&
                       &32*tmp64*tmp79 - (32*discbtme*mm2*tmp12*tmp32*tmp58*tmp64*tmp79)/tmp2 - (64*disc&
                       &bs*tmp12*tmp19*tmp21*tmp39*tmp4*tmp43*tmp64*tmp82)/tmp67 - 256*mm2*tmp12*tmp31*t&
                       &mp86*tmp93 - 128*discbtmm*mm2*tmp12*tmp31*tmp86*tmp93 - (128*mm2*tmp12*tmp13*tmp&
                       &31*tmp86*tmp93)/tmp2 - (64*discbtmm*mm2*tmp12*tmp13*tmp31*tmp86*tmp93)/tmp2 + (1&
                       &28*mm2*tmp12*tmp13*tmp31*tmp86*tmp93)/tmp21 + (64*discbtmm*mm2*tmp12*tmp13*tmp31&
                       &*tmp86*tmp93)/tmp21 + (128*mm2*tmp12*tmp21*tmp31*tmp86*tmp93)/tmp13 + (64*discbt&
                       &mm*mm2*tmp12*tmp21*tmp31*tmp86*tmp93)/tmp13 + (128*mm2*tmp12*tmp21*tmp31*tmp86*t&
                       &mp93)/tmp2 + (64*discbtmm*mm2*tmp12*tmp21*tmp31*tmp86*tmp93)/tmp2 + 64*tmp22*tmp&
                       &31*tmp86*tmp93 + 64*discbtmm*tmp22*tmp31*tmp86*tmp93 + (32*tmp13*tmp22*tmp31*tmp&
                       &86*tmp93)/tmp2 + (32*discbtmm*tmp13*tmp22*tmp31*tmp86*tmp93)/tmp2 - (32*tmp13*tm&
                       &p22*tmp31*tmp86*tmp93)/tmp21 - (32*discbtmm*tmp13*tmp22*tmp31*tmp86*tmp93)/tmp21&
                       & - (32*tmp21*tmp22*tmp31*tmp86*tmp93)/tmp13 - (32*discbtmm*tmp21*tmp22*tmp31*tmp&
                       &86*tmp93)/tmp13 - (32*tmp21*tmp22*tmp31*tmp86*tmp93)/tmp2 - (32*discbtmm*tmp21*t&
                       &mp22*tmp31*tmp86*tmp93)/tmp2 + 32*discbtme*mm2*tmp12*tmp19*tmp32*tmp64*tmp7*tmp9&
                       &8 - 32*discbtme*mm2*tmp12*tmp2*tmp32*tmp64*tmp7*tmp98 + (32*discbtme*mm2*tmp12*t&
                       &mp23*tmp32*tmp64*tmp7*tmp98)/tmp19 + 32*discbtme*tmp12*tmp19*tmp27*tmp32*tmp64*t&
                       &mp7*tmp98 - 32*discbtme*tmp12*tmp2*tmp27*tmp32*tmp64*tmp7*tmp98 - (32*discbtme*m&
                       &m2*tmp12*tmp32*tmp58*tmp64*tmp7*tmp98)/tmp2 + (64*discbu*me2*tmp12*tmp13*tmp20*t&
                       &mp46*tmp51*tmp53)/tmp99 + (64*discbu*mm2*tmp12*tmp2*tmp20*tmp46*tmp51*tmp53)/tmp&
                       &99 - (32*discbu*tmp12*tmp19*tmp20*tmp27*tmp46*tmp51*tmp53)/tmp99 - (32*discbu*tm&
                       &p12*tmp20*tmp21*tmp32*tmp46*tmp51*tmp53)/tmp99 - (32*discbu*tmp12*tmp13*tmp20*tm&
                       &p46*tmp49*tmp51*tmp53)/tmp99 - (32*discbu*tmp12*tmp2*tmp20*tmp46*tmp49*tmp51*tmp&
                       &53)/tmp99 + (32*discbu*tmp12*tmp13*tmp19*tmp20*tmp46*tmp51*tmp53*tmp55)/(tmp2*tm&
                       &p99) + (32*discbu*tmp12*tmp2*tmp20*tmp21*tmp46*tmp51*tmp53*tmp55)/(tmp13*tmp99) &
                       &- (32*discbu*tmp12*tmp13*tmp20*tmp49*tmp51*tmp53*tmp57)/tmp99 + (32*discbu*tmp12&
                       &*tmp13*tmp2*tmp20*tmp49*tmp51*tmp53*tmp57)/(tmp19*tmp99) + (32*discbu*tmp12*tmp2&
                       &0*tmp21*tmp49*tmp51*tmp53*tmp57)/tmp99 - (32*discbu*tmp12*tmp19*tmp20*tmp21*tmp4&
                       &9*tmp51*tmp53*tmp57)/(tmp2*tmp99) - (32*discbu*tmp12*tmp13*tmp20*tmp51*tmp53*tmp&
                       &55*tmp57)/tmp99 + (32*discbu*tmp12*tmp13*tmp19*tmp20*tmp51*tmp53*tmp55*tmp57)/(t&
                       &mp2*tmp99) + (32*discbu*tmp12*tmp20*tmp21*tmp51*tmp53*tmp55*tmp57)/tmp99 - (32*d&
                       &iscbu*tmp12*tmp2*tmp20*tmp21*tmp51*tmp53*tmp55*tmp57)/(tmp19*tmp99) - (64*discbu&
                       &*me2*tmp12*tmp13*tmp51*tmp53*tmp64)/tmp99 - (64*discbu*mm2*tmp12*tmp2*tmp51*tmp5&
                       &3*tmp64)/tmp99 - (64*discbu*me2*tmp106*tmp12*tmp13*tmp20*tmp51*tmp53*tmp64)/tmp9&
                       &9 - (64*discbu*mm2*tmp106*tmp12*tmp2*tmp20*tmp51*tmp53*tmp64)/tmp99 + (64*discbu&
                       &*tmp12*tmp13*tmp19*tmp20*tmp51*tmp53*tmp64)/(tmp21*tmp99) + (64*discbu*tmp12*tmp&
                       &20*tmp21*tmp51*tmp53*tmp64)/tmp99 + (32*discbu*tmp12*tmp19*tmp27*tmp51*tmp53*tmp&
                       &64)/tmp99 + (32*discbu*tmp106*tmp12*tmp19*tmp20*tmp27*tmp51*tmp53*tmp64)/tmp99 +&
                       & (32*discbu*tmp12*tmp21*tmp32*tmp51*tmp53*tmp64)/tmp99 + (32*discbu*tmp106*tmp12&
                       &*tmp20*tmp21*tmp32*tmp51*tmp53*tmp64)/tmp99 + (64*discbu*tmp12*tmp13*tmp49*tmp51&
                       &*tmp53*tmp64)/tmp99 + (32*discbu*tmp12*tmp2*tmp49*tmp51*tmp53*tmp64)/tmp99 - (32&
                       &*discbu*tmp12*tmp13*tmp2*tmp49*tmp51*tmp53*tmp64)/(tmp19*tmp99) + (64*discbu*tmp&
                       &106*tmp12*tmp13*tmp20*tmp49*tmp51*tmp53*tmp64)/tmp99 + (32*discbu*tmp106*tmp12*t&
                       &mp2*tmp20*tmp49*tmp51*tmp53*tmp64)/tmp99 - (32*discbu*tmp106*tmp12*tmp13*tmp2*tm&
                       &p20*tmp49*tmp51*tmp53*tmp64)/(tmp19*tmp99) - (32*discbu*tmp12*tmp21*tmp49*tmp51*&
                       &tmp53*tmp64)/tmp99 + (32*discbu*tmp12*tmp19*tmp21*tmp49*tmp51*tmp53*tmp64)/(tmp2&
                       &*tmp99) - (32*discbu*tmp106*tmp12*tmp20*tmp21*tmp49*tmp51*tmp53*tmp64)/tmp99 + (&
                       &32*discbu*tmp106*tmp12*tmp19*tmp20*tmp21*tmp49*tmp51*tmp53*tmp64)/(tmp2*tmp99) +&
                       & (32*discbu*tmp12*tmp13*tmp51*tmp53*tmp55*tmp64)/tmp99 - (64*discbu*tmp12*tmp13*&
                       &tmp19*tmp51*tmp53*tmp55*tmp64)/(tmp2*tmp99) + (32*discbu*tmp106*tmp12*tmp13*tmp2&
                       &0*tmp51*tmp53*tmp55*tmp64)/tmp99 - (64*discbu*tmp106*tmp12*tmp13*tmp19*tmp20*tmp&
                       &51*tmp53*tmp55*tmp64)/(tmp2*tmp99) - (32*discbu*tmp12*tmp21*tmp51*tmp53*tmp55*tm&
                       &p64)/tmp99 - (32*discbu*tmp12*tmp2*tmp21*tmp51*tmp53*tmp55*tmp64)/(tmp13*tmp99) &
                       &+ (32*discbu*tmp12*tmp2*tmp21*tmp51*tmp53*tmp55*tmp64)/(tmp19*tmp99) - (32*discb&
                       &u*tmp106*tmp12*tmp20*tmp21*tmp51*tmp53*tmp55*tmp64)/tmp99 - (32*discbu*tmp106*tm&
                       &p12*tmp2*tmp20*tmp21*tmp51*tmp53*tmp55*tmp64)/(tmp13*tmp99) + (32*discbu*tmp106*&
                       &tmp12*tmp2*tmp20*tmp21*tmp51*tmp53*tmp55*tmp64)/(tmp19*tmp99) + (64*discbu*tmp13&
                       &*tmp20*tmp49*tmp51*tmp53*tmp59*tmp64)/tmp99 - (64*discbu*tmp13*tmp2*tmp20*tmp49*&
                       &tmp51*tmp53*tmp59*tmp64)/(tmp19*tmp99) - (64*discbu*tmp20*tmp21*tmp49*tmp51*tmp5&
                       &3*tmp59*tmp64)/tmp99 + (64*discbu*tmp19*tmp20*tmp21*tmp49*tmp51*tmp53*tmp59*tmp6&
                       &4)/(tmp2*tmp99) + (64*discbu*tmp13*tmp20*tmp51*tmp53*tmp55*tmp59*tmp64)/tmp99 - &
                       &(64*discbu*tmp13*tmp19*tmp20*tmp51*tmp53*tmp55*tmp59*tmp64)/(tmp2*tmp99) - (64*d&
                       &iscbu*tmp20*tmp21*tmp51*tmp53*tmp55*tmp59*tmp64)/tmp99 + (64*discbu*tmp2*tmp20*t&
                       &mp21*tmp51*tmp53*tmp55*tmp59*tmp64)/(tmp19*tmp99) + (64*discbu*me2*tmp12*tmp13*t&
                       &mp20*tmp51*tmp64*tmp80)/tmp99 + (64*discbu*mm2*tmp12*tmp2*tmp20*tmp51*tmp64*tmp8&
                       &0)/tmp99 - (32*discbu*tmp12*tmp19*tmp20*tmp27*tmp51*tmp64*tmp80)/tmp99 - (32*dis&
                       &cbu*tmp12*tmp20*tmp21*tmp32*tmp51*tmp64*tmp80)/tmp99 - (64*discbu*tmp12*tmp13*tm&
                       &p20*tmp49*tmp51*tmp64*tmp80)/tmp99 - (32*discbu*tmp12*tmp2*tmp20*tmp49*tmp51*tmp&
                       &64*tmp80)/tmp99 + (32*discbu*tmp12*tmp13*tmp2*tmp20*tmp49*tmp51*tmp64*tmp80)/(tm&
                       &p19*tmp99) + (32*discbu*tmp12*tmp20*tmp21*tmp49*tmp51*tmp64*tmp80)/tmp99 - (32*d&
                       &iscbu*tmp12*tmp19*tmp20*tmp21*tmp49*tmp51*tmp64*tmp80)/(tmp2*tmp99) - (32*discbu&
                       &*tmp12*tmp13*tmp20*tmp51*tmp55*tmp64*tmp80)/tmp99 + (64*discbu*tmp12*tmp13*tmp19&
                       &*tmp20*tmp51*tmp55*tmp64*tmp80)/(tmp2*tmp99) + (32*discbu*tmp12*tmp20*tmp21*tmp5&
                       &1*tmp55*tmp64*tmp80)/tmp99 + (32*discbu*tmp12*tmp2*tmp20*tmp21*tmp51*tmp55*tmp64&
                       &*tmp80)/(tmp13*tmp99) - (32*discbu*tmp12*tmp2*tmp20*tmp21*tmp51*tmp55*tmp64*tmp8&
                       &0)/(tmp19*tmp99) + (64*discbu*me2*tmp12*tmp13*tmp20*tmp53*tmp64*tmp81)/tmp99 + (&
                       &64*discbu*mm2*tmp12*tmp2*tmp20*tmp53*tmp64*tmp81)/tmp99 - (32*discbu*tmp12*tmp19&
                       &*tmp20*tmp27*tmp53*tmp64*tmp81)/tmp99 - (32*discbu*tmp12*tmp20*tmp21*tmp32*tmp53&
                       &*tmp64*tmp81)/tmp99 - (64*discbu*tmp12*tmp13*tmp20*tmp49*tmp53*tmp64*tmp81)/tmp9&
                       &9 - (32*discbu*tmp12*tmp2*tmp20*tmp49*tmp53*tmp64*tmp81)/tmp99 + (32*discbu*tmp1&
                       &2*tmp13*tmp2*tmp20*tmp49*tmp53*tmp64*tmp81)/(tmp19*tmp99) + (32*discbu*tmp12*tmp&
                       &20*tmp21*tmp49*tmp53*tmp64*tmp81)/tmp99 - (32*discbu*tmp12*tmp19*tmp20*tmp21*tmp&
                       &49*tmp53*tmp64*tmp81)/(tmp2*tmp99) - (32*discbu*tmp12*tmp13*tmp20*tmp53*tmp55*tm&
                       &p64*tmp81)/tmp99 + (64*discbu*tmp12*tmp13*tmp19*tmp20*tmp53*tmp55*tmp64*tmp81)/(&
                       &tmp2*tmp99) + (32*discbu*tmp12*tmp20*tmp21*tmp53*tmp55*tmp64*tmp81)/tmp99 + (32*&
                       &discbu*tmp12*tmp2*tmp20*tmp21*tmp53*tmp55*tmp64*tmp81)/(tmp13*tmp99) - (32*discb&
                       &u*tmp12*tmp2*tmp20*tmp21*tmp53*tmp55*tmp64*tmp81)/(tmp19*tmp99) - (64*discbu*tmp&
                       &12*tmp19*tmp20*tmp21*tmp51*tmp53*tmp64*tmp82)/tmp99

  END FUNCTION LBK_NLP_EP_QE2QM2

  FUNCTION LBK_NLP_EP_QEQM3(me2, mm2, ss, tt, s15, s25, s35)
  implicit none
  real LBK_NLP_EP_QeQm3
  real(kind=prec) ::  me2
  real(kind=prec) ::  mm2
  real(kind=prec) ::  ss
  real(kind=prec) ::  tt
  real(kind=prec) ::  s15
  real(kind=prec) ::  s25
  real(kind=prec) ::  s35
  real tmp1, tmp2, tmp3, tmp4, tmp5
  real tmp6, tmp7, tmp8, tmp9, tmp10
  real tmp11, tmp12, tmp13, tmp14, tmp15
  real tmp16, tmp17, tmp18, tmp19, tmp20
  real tmp21, tmp22, tmp23, tmp24, tmp25
  real tmp26, tmp27, tmp28, tmp29, tmp30
  real tmp31, tmp32, tmp33, tmp34, tmp35
  real tmp36, tmp37, tmp38, tmp39, tmp40
  real tmp41, tmp42, tmp43, tmp44, tmp45
  real tmp46, tmp47, tmp48, tmp49, tmp50
  real tmp51, tmp52, tmp53, tmp54, tmp55
  real tmp56, tmp57, tmp58, tmp59, tmp60
  real tmp61, tmp62, tmp63, tmp64, tmp65
  real tmp66, tmp67, tmp68, tmp69, tmp70
  real tmp71, tmp72, tmp73, tmp74, tmp75
  real tmp76, tmp77, tmp78, tmp79, tmp80
  real tmp81, tmp82, tmp83, tmp84, tmp85
  real tmp86, tmp87, tmp88, tmp89, tmp90
  real tmp91, tmp92, tmp93, tmp94, tmp95
  real tmp96, tmp97, tmp98

  tmp1 = -tt
  tmp2 = mm2**2
  tmp3 = 1/s25
  tmp4 = 4*mm2
  tmp5 = tmp1 + tmp4
  tmp6 = 1/tmp5
  tmp7 = 2*me2
  tmp8 = 2*mm2
  tmp9 = -2*ss
  tmp10 = tmp1 + tmp7 + tmp8 + tmp9
  tmp11 = tt**(-2)
  tmp12 = 1/s15
  tmp13 = -ss
  tmp14 = me2 + mm2 + tmp13
  tmp15 = -s35
  tmp16 = 1/tmp12 + tmp15 + 1/tmp3
  tmp17 = 1/tmp16
  tmp18 = tmp1 + tmp8
  tmp19 = tmp1 + tmp14
  tmp20 = 1/s35
  tmp21 = 1/tt
  tmp22 = -4*mm2
  tmp23 = 1/tmp21 + tmp22
  tmp24 = 1/tmp23
  tmp25 = tmp1 + tmp7
  tmp26 = sqrt(me2)
  tmp27 = sqrt(mm2)
  tmp28 = -tmp27
  tmp29 = tmp26 + tmp28
  tmp30 = tmp29**2
  tmp31 = tmp13 + tmp30
  tmp32 = 1/tmp31
  tmp33 = tmp26 + tmp27
  tmp34 = tmp33**2
  tmp35 = tmp13 + tmp34
  tmp36 = 1/tmp35
  tmp37 = -4*tmp14
  tmp38 = 2/tmp21
  tmp39 = tmp37 + tmp38
  tmp40 = tmp3**2
  tmp41 = -me2
  tmp42 = -mm2
  tmp43 = ss + tmp41 + tmp42
  tmp44 = tmp1 + tmp31
  tmp45 = 1/tmp44
  tmp46 = tmp1 + tmp35
  tmp47 = 1/tmp46
  tmp48 = tmp13 + tmp25 + tmp8
  tmp49 = 1/tmp21 + tmp43
  tmp50 = 2*ss
  tmp51 = tmp38 + tmp50
  tmp52 = tmp17**2
  tmp53 = tmp21**3
  tmp54 = tmp14**2
  tmp55 = 2*tmp54
  tmp56 = tmp50/tmp21
  tmp57 = tmp21**(-2)
  tmp58 = tmp55 + tmp56 + tmp57
  tmp59 = tmp36**2
  tmp60 = tmp32**2
  tmp61 = 1/ss
  tmp62 = -tmp61
  tmp63 = me2**2
  tmp64 = -2*me2*mm2
  tmp65 = (-2*me2)/tmp61
  tmp66 = (-2*mm2)/tmp61
  tmp67 = tmp61**(-2)
  tmp68 = tmp2 + tmp63 + tmp64 + tmp65 + tmp66 + tmp67
  tmp69 = 1/tmp68
  tmp70 = tmp43*tmp69
  tmp71 = tmp62 + tmp70
  tmp72 = tmp6**2
  tmp73 = tmp47**2
  tmp74 = tmp45**2
  tmp75 = -(1/tmp12)
  tmp76 = -(1/tmp3)
  tmp77 = 1/tmp20 + tmp75 + tmp76
  tmp78 = 1/tmp77
  tmp79 = 2*tmp63
  tmp80 = me2*tmp4
  tmp81 = 2*tmp2
  tmp82 = (-4*me2)/tmp61
  tmp83 = tmp22/tmp61
  tmp84 = 2*tmp67
  tmp85 = tmp56 + tmp57 + tmp79 + tmp80 + tmp81 + tmp82 + tmp83 + tmp84
  tmp86 = -tmp21
  tmp87 = -2*mm2
  tmp88 = 1/tmp21 + tmp87
  tmp89 = tmp21*tmp24*tmp88
  tmp90 = tmp86 + tmp89
  tmp91 = 1/tmp48
  tmp92 = -tmp91
  tmp93 = (-2*me2)/tmp21
  tmp94 = tmp87/tmp21
  tmp95 = tmp56 + tmp57 + tmp68 + tmp93 + tmp94
  tmp96 = 1/tmp95
  tmp97 = tmp19*tmp96
  tmp98 = tmp92 + tmp97
  LBK_NLP_EP_QeQm3 = 256*me2*mm2*tmp10*tmp11*tmp12*tmp24 + 128*discbtmm*me2*mm2*tmp10*tmp11*tmp12*tmp&
                      &24 - 64*me2*tmp10*tmp12*tmp21*tmp24 - 64*discbtmm*me2*tmp10*tmp12*tmp21*tmp24 - &
                      &128*mm2*tmp10*tmp11*tmp20*tmp24*tmp25 - 64*discbtmm*mm2*tmp10*tmp11*tmp20*tmp24*&
                      &tmp25 + 32*tmp10*tmp20*tmp21*tmp24*tmp25 + 32*discbtmm*tmp10*tmp20*tmp21*tmp24*t&
                      &mp25 - 16*tmp11*tmp12*tmp43*tmp51 + 16*tmp11*tmp20*tmp43*tmp51 - (16*tmp11*tmp17&
                      &*tmp20*tmp43*tmp51)/tmp3 + (16*tmp11*tmp12*tmp3*tmp43*tmp51)/tmp17 - 16*tmp11*tm&
                      &p12*tmp49*tmp51 + 16*tmp11*tmp20*tmp49*tmp51 + (16*tmp11*tmp12*tmp17*tmp49*tmp51&
                      &)/tmp3 - (16*tmp11*tmp20*tmp3*tmp49*tmp51)/tmp17 - (32*tmp11*tmp14*tmp17*tmp18*t&
                      &mp3*tmp32*tmp36*tmp58)/tmp12 + (32*discbs*tmp11*tmp14*tmp17*tmp18*tmp3*tmp32*tmp&
                      &36*tmp58)/tmp12 + (64*mm2*tmp11*tmp14*tmp32*tmp36*tmp40*tmp58)/tmp12 - (64*discb&
                      &s*mm2*tmp11*tmp14*tmp32*tmp36*tmp40*tmp58)/tmp12 - 32*tmp11*tmp14*tmp3*tmp32*tmp&
                      &36*tmp43*tmp58 + 32*discbs*tmp11*tmp14*tmp3*tmp32*tmp36*tmp43*tmp58 + 64*mm2*tmp&
                      &11*tmp17*tmp19*tmp45*tmp47*tmp58 - 64*discbu*mm2*tmp11*tmp17*tmp19*tmp45*tmp47*t&
                      &mp58 + 64*tmp11*tmp17*tmp18*tmp19*tmp45*tmp47*tmp58 - 64*discbu*tmp11*tmp17*tmp1&
                      &8*tmp19*tmp45*tmp47*tmp58 - 64*mm2*tmp11*tmp19*tmp3*tmp45*tmp47*tmp58 + 64*discb&
                      &u*mm2*tmp11*tmp19*tmp3*tmp45*tmp47*tmp58 - 64*tmp11*tmp18*tmp19*tmp3*tmp45*tmp47&
                      &*tmp58 + 64*discbu*tmp11*tmp18*tmp19*tmp3*tmp45*tmp47*tmp58 + (32*tmp11*tmp17*tm&
                      &p18*tmp19*tmp3*tmp45*tmp47*tmp58)/tmp12 - (32*discbu*tmp11*tmp17*tmp18*tmp19*tmp&
                      &3*tmp45*tmp47*tmp58)/tmp12 - (64*mm2*tmp11*tmp19*tmp40*tmp45*tmp47*tmp58)/tmp12 &
                      &+ (64*discbu*mm2*tmp11*tmp19*tmp40*tmp45*tmp47*tmp58)/tmp12 + (64*mm2*tmp11*tmp1&
                      &9*tmp40*tmp45*tmp47*tmp58)/tmp17 - (64*discbu*mm2*tmp11*tmp19*tmp40*tmp45*tmp47*&
                      &tmp58)/tmp17 + 32*tmp11*tmp19*tmp3*tmp43*tmp45*tmp47*tmp58 - 32*discbu*tmp11*tmp&
                      &19*tmp3*tmp43*tmp45*tmp47*tmp58 + 32*tmp11*tmp14*tmp17*tmp32*tmp36*tmp49*tmp58 -&
                      & 32*discbs*tmp11*tmp14*tmp17*tmp32*tmp36*tmp49*tmp58 - 32*tmp11*tmp17*tmp19*tmp4&
                      &5*tmp47*tmp49*tmp58 + 32*discbu*tmp11*tmp17*tmp19*tmp45*tmp47*tmp49*tmp58 - (64*&
                      &mm2*tmp11*tmp19*tmp45*tmp47*tmp52*tmp58)/tmp3 + (64*discbu*mm2*tmp11*tmp19*tmp45&
                      &*tmp47*tmp52*tmp58)/tmp3 + 32*tmp12*tmp43*tmp53*tmp58 - 32*tmp20*tmp43*tmp53*tmp&
                      &58 + (32*tmp17*tmp20*tmp43*tmp53*tmp58)/tmp3 - (32*tmp12*tmp3*tmp43*tmp53*tmp58)&
                      &/tmp17 + 32*tmp12*tmp49*tmp53*tmp58 - 32*tmp20*tmp49*tmp53*tmp58 - (32*tmp12*tmp&
                      &17*tmp49*tmp53*tmp58)/tmp3 + (32*tmp20*tmp3*tmp49*tmp53*tmp58)/tmp17 - 128*mm2*t&
                      &mp10*tmp11*tmp12*tmp14*tmp6 - 64*discbtmm*mm2*tmp10*tmp11*tmp12*tmp14*tmp6 + 128&
                      &*mm2*tmp10*tmp11*tmp17*tmp18*tmp6 + 64*discbtmm*mm2*tmp10*tmp11*tmp17*tmp18*tmp6&
                      & + 32*tmp10*tmp12*tmp14*tmp21*tmp6 + 32*discbtmm*tmp10*tmp12*tmp14*tmp21*tmp6 - &
                      &32*tmp10*tmp17*tmp18*tmp21*tmp6 - 32*discbtmm*tmp10*tmp17*tmp18*tmp21*tmp6 + (12&
                      &8*mm2*tmp10*tmp11*tmp12*tmp17*tmp19*tmp6)/tmp3 + (64*discbtmm*mm2*tmp10*tmp11*tm&
                      &p12*tmp17*tmp19*tmp6)/tmp3 - (32*tmp10*tmp12*tmp17*tmp19*tmp21*tmp6)/tmp3 - (32*&
                      &discbtmm*tmp10*tmp12*tmp17*tmp19*tmp21*tmp6)/tmp3 - 128*mm2*tmp10*tmp11*tmp14*tm&
                      &p3*tmp6 - 64*discbtmm*mm2*tmp10*tmp11*tmp14*tmp3*tmp6 - 256*tmp10*tmp11*tmp2*tmp&
                      &3*tmp6 - 128*discbtmm*tmp10*tmp11*tmp2*tmp3*tmp6 + (128*mm2*tmp10*tmp11*tmp19*tm&
                      &p20*tmp3*tmp6)/tmp12 + (64*discbtmm*mm2*tmp10*tmp11*tmp19*tmp20*tmp3*tmp6)/tmp12&
                      & + 64*mm2*tmp10*tmp21*tmp3*tmp6 + 64*discbtmm*mm2*tmp10*tmp21*tmp3*tmp6 + 32*tmp&
                      &10*tmp14*tmp21*tmp3*tmp6 + 32*discbtmm*tmp10*tmp14*tmp21*tmp3*tmp6 - (32*tmp10*t&
                      &mp19*tmp20*tmp21*tmp3*tmp6)/tmp12 - (32*discbtmm*tmp10*tmp19*tmp20*tmp21*tmp3*tm&
                      &p6)/tmp12 - 16*discbtmm*tmp11*tmp12*tmp18*tmp43*tmp51*tmp6 + 16*discbtmm*tmp11*t&
                      &mp18*tmp20*tmp43*tmp51*tmp6 - (16*discbtmm*tmp11*tmp17*tmp18*tmp20*tmp43*tmp51*t&
                      &mp6)/tmp3 + (16*discbtmm*tmp11*tmp12*tmp18*tmp3*tmp43*tmp51*tmp6)/tmp17 - 16*dis&
                      &cbtmm*tmp11*tmp12*tmp18*tmp49*tmp51*tmp6 + 16*discbtmm*tmp11*tmp18*tmp20*tmp49*t&
                      &mp51*tmp6 + (16*discbtmm*tmp11*tmp12*tmp17*tmp18*tmp49*tmp51*tmp6)/tmp3 - (16*di&
                      &scbtmm*tmp11*tmp18*tmp20*tmp3*tmp49*tmp51*tmp6)/tmp17 + 16*discbtmm*tmp11*tmp12*&
                      &tmp43*tmp58*tmp6 - 16*discbtmm*tmp11*tmp20*tmp43*tmp58*tmp6 + (16*discbtmm*tmp11&
                      &*tmp17*tmp20*tmp43*tmp58*tmp6)/tmp3 - (16*discbtmm*tmp11*tmp12*tmp3*tmp43*tmp58*&
                      &tmp6)/tmp17 + 16*discbtmm*tmp11*tmp12*tmp49*tmp58*tmp6 - 16*discbtmm*tmp11*tmp20&
                      &*tmp49*tmp58*tmp6 - (16*discbtmm*tmp11*tmp12*tmp17*tmp49*tmp58*tmp6)/tmp3 + (16*&
                      &discbtmm*tmp11*tmp20*tmp3*tmp49*tmp58*tmp6)/tmp17 + 16*tmp12*tmp18*tmp43*tmp53*t&
                      &mp58*tmp6 + 32*discbtmm*tmp12*tmp18*tmp43*tmp53*tmp58*tmp6 - 16*tmp18*tmp20*tmp4&
                      &3*tmp53*tmp58*tmp6 - 32*discbtmm*tmp18*tmp20*tmp43*tmp53*tmp58*tmp6 + (16*tmp17*&
                      &tmp18*tmp20*tmp43*tmp53*tmp58*tmp6)/tmp3 + (32*discbtmm*tmp17*tmp18*tmp20*tmp43*&
                      &tmp53*tmp58*tmp6)/tmp3 - (16*tmp12*tmp18*tmp3*tmp43*tmp53*tmp58*tmp6)/tmp17 - (3&
                      &2*discbtmm*tmp12*tmp18*tmp3*tmp43*tmp53*tmp58*tmp6)/tmp17 + 16*tmp12*tmp18*tmp49&
                      &*tmp53*tmp58*tmp6 + 32*discbtmm*tmp12*tmp18*tmp49*tmp53*tmp58*tmp6 - 16*tmp18*tm&
                      &p20*tmp49*tmp53*tmp58*tmp6 - 32*discbtmm*tmp18*tmp20*tmp49*tmp53*tmp58*tmp6 - (1&
                      &6*tmp12*tmp17*tmp18*tmp49*tmp53*tmp58*tmp6)/tmp3 - (32*discbtmm*tmp12*tmp17*tmp1&
                      &8*tmp49*tmp53*tmp58*tmp6)/tmp3 + (16*tmp18*tmp20*tmp3*tmp49*tmp53*tmp58*tmp6)/tm&
                      &p17 + (32*discbtmm*tmp18*tmp20*tmp3*tmp49*tmp53*tmp58*tmp6)/tmp17 + (32*discbs*t&
                      &mp11*tmp14*tmp17*tmp18*tmp3*tmp32*tmp36*tmp39)/(tmp12*tmp61) - (64*discbs*mm2*tm&
                      &p11*tmp14*tmp32*tmp36*tmp39*tmp40)/(tmp12*tmp61) + (32*discbs*tmp11*tmp14*tmp3*t&
                      &mp32*tmp36*tmp39*tmp43)/tmp61 - (32*discbs*tmp11*tmp14*tmp17*tmp32*tmp36*tmp39*t&
                      &mp49)/tmp61 + (64*discbs*mm2*tmp11*tmp14*tmp17*tmp32*tmp36*tmp51)/tmp61 + (64*di&
                      &scbs*tmp11*tmp14*tmp17*tmp18*tmp32*tmp36*tmp51)/tmp61 - (64*discbs*mm2*tmp11*tmp&
                      &14*tmp3*tmp32*tmp36*tmp51)/tmp61 - (64*discbs*tmp11*tmp14*tmp18*tmp3*tmp32*tmp36&
                      &*tmp51)/tmp61 + (64*discbs*mm2*tmp11*tmp14*tmp32*tmp36*tmp40*tmp51)/(tmp17*tmp61&
                      &) - (64*discbs*mm2*tmp11*tmp14*tmp32*tmp36*tmp51*tmp52)/(tmp3*tmp61) - (32*discb&
                      &s*tmp11*tmp17*tmp18*tmp3*tmp32*tmp36*tmp58)/(tmp12*tmp61) + (64*discbs*mm2*tmp11&
                      &*tmp32*tmp36*tmp40*tmp58)/(tmp12*tmp61) - (32*discbs*tmp11*tmp3*tmp32*tmp36*tmp4&
                      &3*tmp58)/tmp61 + (32*discbs*tmp11*tmp17*tmp32*tmp36*tmp49*tmp58)/tmp61 - (128*di&
                      &scbs*mm2*tmp14*tmp17*tmp32*tmp36*tmp53*tmp58)/tmp61 - (128*discbs*tmp14*tmp17*tm&
                      &p18*tmp32*tmp36*tmp53*tmp58)/tmp61 + (128*discbs*mm2*tmp14*tmp3*tmp32*tmp36*tmp5&
                      &3*tmp58)/tmp61 + (128*discbs*tmp14*tmp18*tmp3*tmp32*tmp36*tmp53*tmp58)/tmp61 - (&
                      &128*discbs*mm2*tmp14*tmp32*tmp36*tmp40*tmp53*tmp58)/(tmp17*tmp61) + (128*discbs*&
                      &mm2*tmp14*tmp32*tmp36*tmp52*tmp53*tmp58)/(tmp3*tmp61) + (32*discbs*tmp11*tmp14*t&
                      &mp17*tmp18*tmp3*tmp32*tmp58*tmp59)/(tmp12*tmp61) - (64*discbs*mm2*tmp11*tmp14*tm&
                      &p32*tmp40*tmp58*tmp59)/(tmp12*tmp61) + (32*discbs*tmp11*tmp14*tmp3*tmp32*tmp43*t&
                      &mp58*tmp59)/tmp61 - (32*discbs*tmp11*tmp14*tmp17*tmp32*tmp49*tmp58*tmp59)/tmp61 &
                      &+ (32*discbs*tmp11*tmp14*tmp17*tmp18*tmp3*tmp36*tmp58*tmp60)/(tmp12*tmp61) - (64&
                      &*discbs*mm2*tmp11*tmp14*tmp36*tmp40*tmp58*tmp60)/(tmp12*tmp61) + (32*discbs*tmp1&
                      &1*tmp14*tmp3*tmp36*tmp43*tmp58*tmp60)/tmp61 - (32*discbs*tmp11*tmp14*tmp17*tmp36&
                      &*tmp49*tmp58*tmp60)/tmp61 + (32*discbs*tmp11*tmp14*tmp17*tmp18*tmp3*tmp32*tmp36*&
                      &tmp58*tmp71)/(tmp12*tmp61) - (64*discbs*mm2*tmp11*tmp14*tmp32*tmp36*tmp40*tmp58*&
                      &tmp71)/(tmp12*tmp61) + (32*discbs*tmp11*tmp14*tmp3*tmp32*tmp36*tmp43*tmp58*tmp71&
                      &)/tmp61 - (32*discbs*tmp11*tmp14*tmp17*tmp32*tmp36*tmp49*tmp58*tmp71)/tmp61 - 16&
                      &*discbtmm*tmp11*tmp12*tmp18*tmp43*tmp58*tmp72 + 16*discbtmm*tmp11*tmp18*tmp20*tm&
                      &p43*tmp58*tmp72 - (16*discbtmm*tmp11*tmp17*tmp18*tmp20*tmp43*tmp58*tmp72)/tmp3 +&
                      & (16*discbtmm*tmp11*tmp12*tmp18*tmp3*tmp43*tmp58*tmp72)/tmp17 - 16*discbtmm*tmp1&
                      &1*tmp12*tmp18*tmp49*tmp58*tmp72 + 16*discbtmm*tmp11*tmp18*tmp20*tmp49*tmp58*tmp7&
                      &2 + (16*discbtmm*tmp11*tmp12*tmp17*tmp18*tmp49*tmp58*tmp72)/tmp3 - (16*discbtmm*&
                      &tmp11*tmp18*tmp20*tmp3*tmp49*tmp58*tmp72)/tmp17 - 128*mm2*tmp11*tmp24*tmp78*tmp8&
                      &5 - 64*discbtmm*mm2*tmp11*tmp24*tmp78*tmp85 + (128*mm2*tmp11*tmp12*tmp24*tmp78*t&
                      &mp85)/tmp20 + (64*discbtmm*mm2*tmp11*tmp12*tmp24*tmp78*tmp85)/tmp20 + 32*tmp21*t&
                      &mp24*tmp78*tmp85 + 32*discbtmm*tmp21*tmp24*tmp78*tmp85 - (32*tmp12*tmp21*tmp24*t&
                      &mp78*tmp85)/tmp20 - (32*discbtmm*tmp12*tmp21*tmp24*tmp78*tmp85)/tmp20 - 16*discb&
                      &tmm*tmp11*tmp12*tmp18*tmp43*tmp58*tmp6*tmp90 + 16*discbtmm*tmp11*tmp18*tmp20*tmp&
                      &43*tmp58*tmp6*tmp90 - (16*discbtmm*tmp11*tmp17*tmp18*tmp20*tmp43*tmp58*tmp6*tmp9&
                      &0)/tmp3 + (16*discbtmm*tmp11*tmp12*tmp18*tmp3*tmp43*tmp58*tmp6*tmp90)/tmp17 - 16&
                      &*discbtmm*tmp11*tmp12*tmp18*tmp49*tmp58*tmp6*tmp90 + 16*discbtmm*tmp11*tmp18*tmp&
                      &20*tmp49*tmp58*tmp6*tmp90 + (16*discbtmm*tmp11*tmp12*tmp17*tmp18*tmp49*tmp58*tmp&
                      &6*tmp90)/tmp3 - (16*discbtmm*tmp11*tmp18*tmp20*tmp3*tmp49*tmp58*tmp6*tmp90)/tmp1&
                      &7 + (32*discbu*tmp11*tmp17*tmp18*tmp19*tmp3*tmp39*tmp45*tmp47)/(tmp12*tmp91) - (&
                      &64*discbu*mm2*tmp11*tmp19*tmp39*tmp40*tmp45*tmp47)/(tmp12*tmp91) + (32*discbu*tm&
                      &p11*tmp19*tmp3*tmp39*tmp43*tmp45*tmp47)/tmp91 - (32*discbu*tmp11*tmp17*tmp19*tmp&
                      &39*tmp45*tmp47*tmp49)/tmp91 + (64*discbu*mm2*tmp11*tmp17*tmp19*tmp45*tmp47*tmp51&
                      &)/tmp91 + (64*discbu*tmp11*tmp17*tmp18*tmp19*tmp45*tmp47*tmp51)/tmp91 - (64*disc&
                      &bu*mm2*tmp11*tmp19*tmp3*tmp45*tmp47*tmp51)/tmp91 - (64*discbu*tmp11*tmp18*tmp19*&
                      &tmp3*tmp45*tmp47*tmp51)/tmp91 + (64*discbu*mm2*tmp11*tmp19*tmp40*tmp45*tmp47*tmp&
                      &51)/(tmp17*tmp91) - (64*discbu*mm2*tmp11*tmp19*tmp45*tmp47*tmp51*tmp52)/(tmp3*tm&
                      &p91) - (64*discbu*mm2*tmp11*tmp17*tmp45*tmp47*tmp58)/tmp91 - (64*discbu*tmp11*tm&
                      &p17*tmp18*tmp45*tmp47*tmp58)/tmp91 + (64*discbu*mm2*tmp11*tmp3*tmp45*tmp47*tmp58&
                      &)/tmp91 + (64*discbu*tmp11*tmp18*tmp3*tmp45*tmp47*tmp58)/tmp91 - (32*discbu*tmp1&
                      &1*tmp17*tmp18*tmp3*tmp45*tmp47*tmp58)/(tmp12*tmp91) + (64*discbu*mm2*tmp11*tmp40&
                      &*tmp45*tmp47*tmp58)/(tmp12*tmp91) - (64*discbu*mm2*tmp11*tmp40*tmp45*tmp47*tmp58&
                      &)/(tmp17*tmp91) - (32*discbu*tmp11*tmp3*tmp43*tmp45*tmp47*tmp58)/tmp91 + (32*dis&
                      &cbu*tmp11*tmp17*tmp45*tmp47*tmp49*tmp58)/tmp91 + (64*discbu*mm2*tmp11*tmp45*tmp4&
                      &7*tmp52*tmp58)/(tmp3*tmp91) - (128*discbu*mm2*tmp17*tmp19*tmp45*tmp47*tmp53*tmp5&
                      &8)/tmp91 - (128*discbu*tmp17*tmp18*tmp19*tmp45*tmp47*tmp53*tmp58)/tmp91 + (128*d&
                      &iscbu*mm2*tmp19*tmp3*tmp45*tmp47*tmp53*tmp58)/tmp91 + (128*discbu*tmp18*tmp19*tm&
                      &p3*tmp45*tmp47*tmp53*tmp58)/tmp91 - (128*discbu*mm2*tmp19*tmp40*tmp45*tmp47*tmp5&
                      &3*tmp58)/(tmp17*tmp91) + (128*discbu*mm2*tmp19*tmp45*tmp47*tmp52*tmp53*tmp58)/(t&
                      &mp3*tmp91) + (64*discbu*mm2*tmp11*tmp17*tmp19*tmp45*tmp58*tmp73)/tmp91 + (64*dis&
                      &cbu*tmp11*tmp17*tmp18*tmp19*tmp45*tmp58*tmp73)/tmp91 - (64*discbu*mm2*tmp11*tmp1&
                      &9*tmp3*tmp45*tmp58*tmp73)/tmp91 - (64*discbu*tmp11*tmp18*tmp19*tmp3*tmp45*tmp58*&
                      &tmp73)/tmp91 + (32*discbu*tmp11*tmp17*tmp18*tmp19*tmp3*tmp45*tmp58*tmp73)/(tmp12&
                      &*tmp91) - (64*discbu*mm2*tmp11*tmp19*tmp40*tmp45*tmp58*tmp73)/(tmp12*tmp91) + (6&
                      &4*discbu*mm2*tmp11*tmp19*tmp40*tmp45*tmp58*tmp73)/(tmp17*tmp91) + (32*discbu*tmp&
                      &11*tmp19*tmp3*tmp43*tmp45*tmp58*tmp73)/tmp91 - (32*discbu*tmp11*tmp17*tmp19*tmp4&
                      &5*tmp49*tmp58*tmp73)/tmp91 - (64*discbu*mm2*tmp11*tmp19*tmp45*tmp52*tmp58*tmp73)&
                      &/(tmp3*tmp91) + (64*discbu*mm2*tmp11*tmp17*tmp19*tmp47*tmp58*tmp74)/tmp91 + (64*&
                      &discbu*tmp11*tmp17*tmp18*tmp19*tmp47*tmp58*tmp74)/tmp91 - (64*discbu*mm2*tmp11*t&
                      &mp19*tmp3*tmp47*tmp58*tmp74)/tmp91 - (64*discbu*tmp11*tmp18*tmp19*tmp3*tmp47*tmp&
                      &58*tmp74)/tmp91 + (32*discbu*tmp11*tmp17*tmp18*tmp19*tmp3*tmp47*tmp58*tmp74)/(tm&
                      &p12*tmp91) - (64*discbu*mm2*tmp11*tmp19*tmp40*tmp47*tmp58*tmp74)/(tmp12*tmp91) +&
                      & (64*discbu*mm2*tmp11*tmp19*tmp40*tmp47*tmp58*tmp74)/(tmp17*tmp91) + (32*discbu*&
                      &tmp11*tmp19*tmp3*tmp43*tmp47*tmp58*tmp74)/tmp91 - (32*discbu*tmp11*tmp17*tmp19*t&
                      &mp47*tmp49*tmp58*tmp74)/tmp91 - (64*discbu*mm2*tmp11*tmp19*tmp47*tmp52*tmp58*tmp&
                      &74)/(tmp3*tmp91) - (64*discbu*mm2*tmp11*tmp17*tmp19*tmp45*tmp47*tmp58*tmp98)/tmp&
                      &91 - (64*discbu*tmp11*tmp17*tmp18*tmp19*tmp45*tmp47*tmp58*tmp98)/tmp91 + (64*dis&
                      &cbu*mm2*tmp11*tmp19*tmp3*tmp45*tmp47*tmp58*tmp98)/tmp91 + (64*discbu*tmp11*tmp18&
                      &*tmp19*tmp3*tmp45*tmp47*tmp58*tmp98)/tmp91 - (32*discbu*tmp11*tmp17*tmp18*tmp19*&
                      &tmp3*tmp45*tmp47*tmp58*tmp98)/(tmp12*tmp91) + (64*discbu*mm2*tmp11*tmp19*tmp40*t&
                      &mp45*tmp47*tmp58*tmp98)/(tmp12*tmp91) - (64*discbu*mm2*tmp11*tmp19*tmp40*tmp45*t&
                      &mp47*tmp58*tmp98)/(tmp17*tmp91) - (32*discbu*tmp11*tmp19*tmp3*tmp43*tmp45*tmp47*&
                      &tmp58*tmp98)/tmp91 + (32*discbu*tmp11*tmp17*tmp19*tmp45*tmp47*tmp49*tmp58*tmp98)&
                      &/tmp91 + (64*discbu*mm2*tmp11*tmp19*tmp45*tmp47*tmp52*tmp58*tmp98)/(tmp3*tmp91)

  END FUNCTION LBK_NLP_EP_QEQM3

  FUNCTION LBK_NLP_EP_QE3QM(me2, mm2, ss, tt, s15, s25, s35)
  implicit none
  real LBK_NLP_EP_Qe3Qm
  real(kind=prec) ::  me2
  real(kind=prec) ::  mm2
  real(kind=prec) ::  ss
  real(kind=prec) ::  tt
  real(kind=prec) ::  s15
  real(kind=prec) ::  s25
  real(kind=prec) ::  s35
  real tmp1, tmp2, tmp3, tmp4, tmp5
  real tmp6, tmp7, tmp8, tmp9, tmp10
  real tmp11, tmp12, tmp13, tmp14, tmp15
  real tmp16, tmp17, tmp18, tmp19, tmp20
  real tmp21, tmp22, tmp23, tmp24, tmp25
  real tmp26, tmp27, tmp28, tmp29, tmp30
  real tmp31, tmp32, tmp33, tmp34, tmp35
  real tmp36, tmp37, tmp38, tmp39, tmp40
  real tmp41, tmp42, tmp43, tmp44, tmp45
  real tmp46, tmp47, tmp48, tmp49, tmp50
  real tmp51, tmp52, tmp53, tmp54, tmp55
  real tmp56, tmp57, tmp58, tmp59, tmp60
  real tmp61, tmp62, tmp63, tmp64, tmp65
  real tmp66, tmp67, tmp68, tmp69, tmp70
  real tmp71, tmp72, tmp73, tmp74, tmp75
  real tmp76, tmp77, tmp78, tmp79, tmp80
  real tmp81, tmp82, tmp83, tmp84, tmp85
  real tmp86, tmp87, tmp88, tmp89, tmp90
  real tmp91, tmp92, tmp93, tmp94, tmp95
  real tmp96, tmp97, tmp98

  tmp1 = -tt
  tmp2 = me2**2
  tmp3 = 1/s15
  tmp4 = 4*me2
  tmp5 = tmp1 + tmp4
  tmp6 = 1/tmp5
  tmp7 = 2*me2
  tmp8 = 2*mm2
  tmp9 = -2*ss
  tmp10 = tmp1 + tmp7 + tmp8 + tmp9
  tmp11 = tt**(-2)
  tmp12 = -ss
  tmp13 = me2 + mm2 + tmp12
  tmp14 = 1/s25
  tmp15 = 1/s35
  tmp16 = tmp1 + tmp7
  tmp17 = -(1/tmp15)
  tmp18 = 1/tmp14 + tmp17 + 1/tmp3
  tmp19 = 1/tmp18
  tmp20 = tmp1 + tmp13
  tmp21 = 1/tt
  tmp22 = -4*me2
  tmp23 = 1/tmp21 + tmp22
  tmp24 = 1/tmp23
  tmp25 = tmp1 + tmp8
  tmp26 = sqrt(me2)
  tmp27 = sqrt(mm2)
  tmp28 = -tmp27
  tmp29 = tmp26 + tmp28
  tmp30 = tmp29**2
  tmp31 = tmp12 + tmp30
  tmp32 = 1/tmp31
  tmp33 = tmp26 + tmp27
  tmp34 = tmp33**2
  tmp35 = tmp12 + tmp34
  tmp36 = 1/tmp35
  tmp37 = -4*tmp13
  tmp38 = 2/tmp21
  tmp39 = tmp37 + tmp38
  tmp40 = tmp3**2
  tmp41 = -me2
  tmp42 = -mm2
  tmp43 = ss + tmp41 + tmp42
  tmp44 = tmp1 + tmp31
  tmp45 = 1/tmp44
  tmp46 = tmp1 + tmp35
  tmp47 = 1/tmp46
  tmp48 = tmp12 + tmp25 + tmp7
  tmp49 = 1/tmp21 + tmp43
  tmp50 = 2*ss
  tmp51 = tmp38 + tmp50
  tmp52 = tmp21**3
  tmp53 = tmp13**2
  tmp54 = 2*tmp53
  tmp55 = tmp50/tmp21
  tmp56 = tmp21**(-2)
  tmp57 = tmp54 + tmp55 + tmp56
  tmp58 = tmp36**2
  tmp59 = tmp32**2
  tmp60 = 1/ss
  tmp61 = -tmp60
  tmp62 = -2*me2*mm2
  tmp63 = mm2**2
  tmp64 = (-2*me2)/tmp60
  tmp65 = (-2*mm2)/tmp60
  tmp66 = tmp60**(-2)
  tmp67 = tmp2 + tmp62 + tmp63 + tmp64 + tmp65 + tmp66
  tmp68 = 1/tmp67
  tmp69 = tmp43*tmp68
  tmp70 = tmp61 + tmp69
  tmp71 = tmp6**2
  tmp72 = tmp47**2
  tmp73 = tmp45**2
  tmp74 = tmp17 + 1/tmp3
  tmp75 = -(1/tmp3)
  tmp76 = -(1/tmp14)
  tmp77 = 1/tmp15 + tmp75 + tmp76
  tmp78 = 1/tmp77
  tmp79 = 2*tmp2
  tmp80 = mm2*tmp4
  tmp81 = 2*tmp63
  tmp82 = tmp22/tmp60
  tmp83 = (-4*mm2)/tmp60
  tmp84 = 2*tmp66
  tmp85 = tmp55 + tmp56 + tmp79 + tmp80 + tmp81 + tmp82 + tmp83 + tmp84
  tmp86 = -tmp21
  tmp87 = -2*me2
  tmp88 = 1/tmp21 + tmp87
  tmp89 = tmp21*tmp24*tmp88
  tmp90 = tmp86 + tmp89
  tmp91 = 1/tmp48
  tmp92 = -tmp91
  tmp93 = tmp87/tmp21
  tmp94 = (-2*mm2)/tmp21
  tmp95 = tmp55 + tmp56 + tmp67 + tmp93 + tmp94
  tmp96 = 1/tmp95
  tmp97 = tmp20*tmp96
  tmp98 = tmp92 + tmp97
  LBK_NLP_EP_Qe3Qm = 256*me2*mm2*tmp10*tmp11*tmp14*tmp24 + 128*discbtme*me2*mm2*tmp10*tmp11*tmp14*tmp&
                      &24 - 64*mm2*tmp10*tmp14*tmp21*tmp24 - 64*discbtme*mm2*tmp10*tmp14*tmp21*tmp24 - &
                      &128*me2*tmp10*tmp11*tmp19*tmp24*tmp25 - 64*discbtme*me2*tmp10*tmp11*tmp19*tmp24*&
                      &tmp25 + 32*tmp10*tmp19*tmp21*tmp24*tmp25 + 32*discbtme*tmp10*tmp19*tmp21*tmp24*t&
                      &mp25 + 16*tmp11*tmp15*tmp43*tmp51 - (16*tmp11*tmp15*tmp19*tmp43*tmp51)/tmp14 - 1&
                      &6*tmp11*tmp3*tmp43*tmp51 + (16*tmp11*tmp14*tmp3*tmp43*tmp51)/tmp19 + 16*tmp11*tm&
                      &p15*tmp49*tmp51 - (16*tmp11*tmp14*tmp15*tmp49*tmp51)/tmp19 - 16*tmp11*tmp3*tmp49&
                      &*tmp51 + (16*tmp11*tmp19*tmp3*tmp49*tmp51)/tmp14 - (32*tmp11*tmp13*tmp15*tmp16*t&
                      &mp3*tmp32*tmp36*tmp57)/tmp14 + (32*discbs*tmp11*tmp13*tmp15*tmp16*tmp3*tmp32*tmp&
                      &36*tmp57)/tmp14 + (64*me2*tmp11*tmp13*tmp32*tmp36*tmp40*tmp57)/tmp14 - (64*discb&
                      &s*me2*tmp11*tmp13*tmp32*tmp36*tmp40*tmp57)/tmp14 - 32*tmp11*tmp13*tmp3*tmp32*tmp&
                      &36*tmp43*tmp57 + 32*discbs*tmp11*tmp13*tmp3*tmp32*tmp36*tmp43*tmp57 + (32*tmp11*&
                      &tmp15*tmp16*tmp20*tmp3*tmp45*tmp47*tmp57)/tmp14 - (32*discbu*tmp11*tmp15*tmp16*t&
                      &mp20*tmp3*tmp45*tmp47*tmp57)/tmp14 - (64*me2*tmp11*tmp20*tmp40*tmp45*tmp47*tmp57&
                      &)/tmp14 + (64*discbu*me2*tmp11*tmp20*tmp40*tmp45*tmp47*tmp57)/tmp14 + 32*tmp11*t&
                      &mp20*tmp3*tmp43*tmp45*tmp47*tmp57 - 32*discbu*tmp11*tmp20*tmp3*tmp43*tmp45*tmp47&
                      &*tmp57 + 32*tmp11*tmp13*tmp15*tmp32*tmp36*tmp49*tmp57 - 32*discbs*tmp11*tmp13*tm&
                      &p15*tmp32*tmp36*tmp49*tmp57 - 32*tmp11*tmp15*tmp20*tmp45*tmp47*tmp49*tmp57 + 32*&
                      &discbu*tmp11*tmp15*tmp20*tmp45*tmp47*tmp49*tmp57 - 32*tmp15*tmp43*tmp52*tmp57 + &
                      &(32*tmp15*tmp19*tmp43*tmp52*tmp57)/tmp14 + 32*tmp3*tmp43*tmp52*tmp57 - (32*tmp14&
                      &*tmp3*tmp43*tmp52*tmp57)/tmp19 - 32*tmp15*tmp49*tmp52*tmp57 + (32*tmp14*tmp15*tm&
                      &p49*tmp52*tmp57)/tmp19 + 32*tmp3*tmp49*tmp52*tmp57 - (32*tmp19*tmp3*tmp49*tmp52*&
                      &tmp57)/tmp14 - 128*me2*tmp10*tmp11*tmp13*tmp14*tmp6 - 64*discbtme*me2*tmp10*tmp1&
                      &1*tmp13*tmp14*tmp6 + 128*me2*tmp10*tmp11*tmp15*tmp16*tmp6 + 64*discbtme*me2*tmp1&
                      &0*tmp11*tmp15*tmp16*tmp6 + 32*tmp10*tmp13*tmp14*tmp21*tmp6 + 32*discbtme*tmp10*t&
                      &mp13*tmp14*tmp21*tmp6 - 32*tmp10*tmp15*tmp16*tmp21*tmp6 - 32*discbtme*tmp10*tmp1&
                      &5*tmp16*tmp21*tmp6 + (128*me2*tmp10*tmp11*tmp14*tmp15*tmp20*tmp6)/tmp3 + (64*dis&
                      &cbtme*me2*tmp10*tmp11*tmp14*tmp15*tmp20*tmp6)/tmp3 - (32*tmp10*tmp14*tmp15*tmp20&
                      &*tmp21*tmp6)/tmp3 - (32*discbtme*tmp10*tmp14*tmp15*tmp20*tmp21*tmp6)/tmp3 - 128*&
                      &me2*tmp10*tmp11*tmp13*tmp3*tmp6 - 64*discbtme*me2*tmp10*tmp11*tmp13*tmp3*tmp6 - &
                      &256*tmp10*tmp11*tmp2*tmp3*tmp6 - 128*discbtme*tmp10*tmp11*tmp2*tmp3*tmp6 + (128*&
                      &me2*tmp10*tmp11*tmp19*tmp20*tmp3*tmp6)/tmp14 + (64*discbtme*me2*tmp10*tmp11*tmp1&
                      &9*tmp20*tmp3*tmp6)/tmp14 + 64*me2*tmp10*tmp21*tmp3*tmp6 + 64*discbtme*me2*tmp10*&
                      &tmp21*tmp3*tmp6 + 32*tmp10*tmp13*tmp21*tmp3*tmp6 + 32*discbtme*tmp10*tmp13*tmp21&
                      &*tmp3*tmp6 - (32*tmp10*tmp19*tmp20*tmp21*tmp3*tmp6)/tmp14 - (32*discbtme*tmp10*t&
                      &mp19*tmp20*tmp21*tmp3*tmp6)/tmp14 + 16*discbtme*tmp11*tmp15*tmp16*tmp43*tmp51*tm&
                      &p6 - (16*discbtme*tmp11*tmp15*tmp16*tmp19*tmp43*tmp51*tmp6)/tmp14 - 16*discbtme*&
                      &tmp11*tmp16*tmp3*tmp43*tmp51*tmp6 + (16*discbtme*tmp11*tmp14*tmp16*tmp3*tmp43*tm&
                      &p51*tmp6)/tmp19 + 16*discbtme*tmp11*tmp15*tmp16*tmp49*tmp51*tmp6 - (16*discbtme*&
                      &tmp11*tmp14*tmp15*tmp16*tmp49*tmp51*tmp6)/tmp19 - 16*discbtme*tmp11*tmp16*tmp3*t&
                      &mp49*tmp51*tmp6 + (16*discbtme*tmp11*tmp16*tmp19*tmp3*tmp49*tmp51*tmp6)/tmp14 - &
                      &16*discbtme*tmp11*tmp15*tmp43*tmp57*tmp6 + (16*discbtme*tmp11*tmp15*tmp19*tmp43*&
                      &tmp57*tmp6)/tmp14 + 16*discbtme*tmp11*tmp3*tmp43*tmp57*tmp6 - (16*discbtme*tmp11&
                      &*tmp14*tmp3*tmp43*tmp57*tmp6)/tmp19 - 16*discbtme*tmp11*tmp15*tmp49*tmp57*tmp6 +&
                      & (16*discbtme*tmp11*tmp14*tmp15*tmp49*tmp57*tmp6)/tmp19 + 16*discbtme*tmp11*tmp3&
                      &*tmp49*tmp57*tmp6 - (16*discbtme*tmp11*tmp19*tmp3*tmp49*tmp57*tmp6)/tmp14 - 16*t&
                      &mp15*tmp16*tmp43*tmp52*tmp57*tmp6 - 32*discbtme*tmp15*tmp16*tmp43*tmp52*tmp57*tm&
                      &p6 + (16*tmp15*tmp16*tmp19*tmp43*tmp52*tmp57*tmp6)/tmp14 + (32*discbtme*tmp15*tm&
                      &p16*tmp19*tmp43*tmp52*tmp57*tmp6)/tmp14 + 16*tmp16*tmp3*tmp43*tmp52*tmp57*tmp6 +&
                      & 32*discbtme*tmp16*tmp3*tmp43*tmp52*tmp57*tmp6 - (16*tmp14*tmp16*tmp3*tmp43*tmp5&
                      &2*tmp57*tmp6)/tmp19 - (32*discbtme*tmp14*tmp16*tmp3*tmp43*tmp52*tmp57*tmp6)/tmp1&
                      &9 - 16*tmp15*tmp16*tmp49*tmp52*tmp57*tmp6 - 32*discbtme*tmp15*tmp16*tmp49*tmp52*&
                      &tmp57*tmp6 + (16*tmp14*tmp15*tmp16*tmp49*tmp52*tmp57*tmp6)/tmp19 + (32*discbtme*&
                      &tmp14*tmp15*tmp16*tmp49*tmp52*tmp57*tmp6)/tmp19 + 16*tmp16*tmp3*tmp49*tmp52*tmp5&
                      &7*tmp6 + 32*discbtme*tmp16*tmp3*tmp49*tmp52*tmp57*tmp6 - (16*tmp16*tmp19*tmp3*tm&
                      &p49*tmp52*tmp57*tmp6)/tmp14 - (32*discbtme*tmp16*tmp19*tmp3*tmp49*tmp52*tmp57*tm&
                      &p6)/tmp14 + (32*discbs*tmp11*tmp13*tmp15*tmp16*tmp3*tmp32*tmp36*tmp39)/(tmp14*tm&
                      &p60) - (64*discbs*me2*tmp11*tmp13*tmp32*tmp36*tmp39*tmp40)/(tmp14*tmp60) + (32*d&
                      &iscbs*tmp11*tmp13*tmp3*tmp32*tmp36*tmp39*tmp43)/tmp60 - (32*discbs*tmp11*tmp13*t&
                      &mp15*tmp32*tmp36*tmp39*tmp49)/tmp60 - (32*discbs*tmp11*tmp15*tmp16*tmp3*tmp32*tm&
                      &p36*tmp57)/(tmp14*tmp60) + (64*discbs*me2*tmp11*tmp32*tmp36*tmp40*tmp57)/(tmp14*&
                      &tmp60) - (32*discbs*tmp11*tmp3*tmp32*tmp36*tmp43*tmp57)/tmp60 + (32*discbs*tmp11&
                      &*tmp15*tmp32*tmp36*tmp49*tmp57)/tmp60 + (32*discbs*tmp11*tmp13*tmp15*tmp16*tmp3*&
                      &tmp32*tmp57*tmp58)/(tmp14*tmp60) - (64*discbs*me2*tmp11*tmp13*tmp32*tmp40*tmp57*&
                      &tmp58)/(tmp14*tmp60) + (32*discbs*tmp11*tmp13*tmp3*tmp32*tmp43*tmp57*tmp58)/tmp6&
                      &0 - (32*discbs*tmp11*tmp13*tmp15*tmp32*tmp49*tmp57*tmp58)/tmp60 + (32*discbs*tmp&
                      &11*tmp13*tmp15*tmp16*tmp3*tmp36*tmp57*tmp59)/(tmp14*tmp60) - (64*discbs*me2*tmp1&
                      &1*tmp13*tmp36*tmp40*tmp57*tmp59)/(tmp14*tmp60) + (32*discbs*tmp11*tmp13*tmp3*tmp&
                      &36*tmp43*tmp57*tmp59)/tmp60 - (32*discbs*tmp11*tmp13*tmp15*tmp36*tmp49*tmp57*tmp&
                      &59)/tmp60 + (32*discbs*tmp11*tmp13*tmp15*tmp16*tmp3*tmp32*tmp36*tmp57*tmp70)/(tm&
                      &p14*tmp60) - (64*discbs*me2*tmp11*tmp13*tmp32*tmp36*tmp40*tmp57*tmp70)/(tmp14*tm&
                      &p60) + (32*discbs*tmp11*tmp13*tmp3*tmp32*tmp36*tmp43*tmp57*tmp70)/tmp60 - (32*di&
                      &scbs*tmp11*tmp13*tmp15*tmp32*tmp36*tmp49*tmp57*tmp70)/tmp60 + 16*discbtme*tmp11*&
                      &tmp15*tmp16*tmp43*tmp57*tmp71 - (16*discbtme*tmp11*tmp15*tmp16*tmp19*tmp43*tmp57&
                      &*tmp71)/tmp14 - 16*discbtme*tmp11*tmp16*tmp3*tmp43*tmp57*tmp71 + (16*discbtme*tm&
                      &p11*tmp14*tmp16*tmp3*tmp43*tmp57*tmp71)/tmp19 + 16*discbtme*tmp11*tmp15*tmp16*tm&
                      &p49*tmp57*tmp71 - (16*discbtme*tmp11*tmp14*tmp15*tmp16*tmp49*tmp57*tmp71)/tmp19 &
                      &- 16*discbtme*tmp11*tmp16*tmp3*tmp49*tmp57*tmp71 + (16*discbtme*tmp11*tmp16*tmp1&
                      &9*tmp3*tmp49*tmp57*tmp71)/tmp14 + (64*discbs*tmp11*tmp13*tmp15*tmp3*tmp32*tmp36*&
                      &tmp57*tmp74)/tmp60 - 128*me2*tmp11*tmp24*tmp78*tmp85 - 64*discbtme*me2*tmp11*tmp&
                      &24*tmp78*tmp85 + 32*tmp21*tmp24*tmp78*tmp85 + 32*discbtme*tmp21*tmp24*tmp78*tmp8&
                      &5 + (128*me2*tmp11*tmp24*tmp3*tmp78*tmp85)/tmp15 + (64*discbtme*me2*tmp11*tmp24*&
                      &tmp3*tmp78*tmp85)/tmp15 - (32*tmp21*tmp24*tmp3*tmp78*tmp85)/tmp15 - (32*discbtme&
                      &*tmp21*tmp24*tmp3*tmp78*tmp85)/tmp15 + 16*discbtme*tmp11*tmp15*tmp16*tmp43*tmp57&
                      &*tmp6*tmp90 - (16*discbtme*tmp11*tmp15*tmp16*tmp19*tmp43*tmp57*tmp6*tmp90)/tmp14&
                      & - 16*discbtme*tmp11*tmp16*tmp3*tmp43*tmp57*tmp6*tmp90 + (16*discbtme*tmp11*tmp1&
                      &4*tmp16*tmp3*tmp43*tmp57*tmp6*tmp90)/tmp19 + 16*discbtme*tmp11*tmp15*tmp16*tmp49&
                      &*tmp57*tmp6*tmp90 - (16*discbtme*tmp11*tmp14*tmp15*tmp16*tmp49*tmp57*tmp6*tmp90)&
                      &/tmp19 - 16*discbtme*tmp11*tmp16*tmp3*tmp49*tmp57*tmp6*tmp90 + (16*discbtme*tmp1&
                      &1*tmp16*tmp19*tmp3*tmp49*tmp57*tmp6*tmp90)/tmp14 + (32*discbu*tmp11*tmp15*tmp16*&
                      &tmp20*tmp3*tmp39*tmp45*tmp47)/(tmp14*tmp91) - (64*discbu*me2*tmp11*tmp20*tmp39*t&
                      &mp40*tmp45*tmp47)/(tmp14*tmp91) + (32*discbu*tmp11*tmp20*tmp3*tmp39*tmp43*tmp45*&
                      &tmp47)/tmp91 - (32*discbu*tmp11*tmp15*tmp20*tmp39*tmp45*tmp47*tmp49)/tmp91 - (32&
                      &*discbu*tmp11*tmp15*tmp16*tmp3*tmp45*tmp47*tmp57)/(tmp14*tmp91) + (64*discbu*me2&
                      &*tmp11*tmp40*tmp45*tmp47*tmp57)/(tmp14*tmp91) - (32*discbu*tmp11*tmp3*tmp43*tmp4&
                      &5*tmp47*tmp57)/tmp91 + (32*discbu*tmp11*tmp15*tmp45*tmp47*tmp49*tmp57)/tmp91 + (&
                      &32*discbu*tmp11*tmp15*tmp16*tmp20*tmp3*tmp45*tmp57*tmp72)/(tmp14*tmp91) - (64*di&
                      &scbu*me2*tmp11*tmp20*tmp40*tmp45*tmp57*tmp72)/(tmp14*tmp91) + (32*discbu*tmp11*t&
                      &mp20*tmp3*tmp43*tmp45*tmp57*tmp72)/tmp91 - (32*discbu*tmp11*tmp15*tmp20*tmp45*tm&
                      &p49*tmp57*tmp72)/tmp91 + (32*discbu*tmp11*tmp15*tmp16*tmp20*tmp3*tmp47*tmp57*tmp&
                      &73)/(tmp14*tmp91) - (64*discbu*me2*tmp11*tmp20*tmp40*tmp47*tmp57*tmp73)/(tmp14*t&
                      &mp91) + (32*discbu*tmp11*tmp20*tmp3*tmp43*tmp47*tmp57*tmp73)/tmp91 - (32*discbu*&
                      &tmp11*tmp15*tmp20*tmp47*tmp49*tmp57*tmp73)/tmp91 + (64*discbu*tmp11*tmp15*tmp20*&
                      &tmp3*tmp45*tmp47*tmp57*tmp74)/tmp91 - (32*discbu*tmp11*tmp15*tmp16*tmp20*tmp3*tm&
                      &p45*tmp47*tmp57*tmp98)/(tmp14*tmp91) + (64*discbu*me2*tmp11*tmp20*tmp40*tmp45*tm&
                      &p47*tmp57*tmp98)/(tmp14*tmp91) - (32*discbu*tmp11*tmp20*tmp3*tmp43*tmp45*tmp47*t&
                      &mp57*tmp98)/tmp91 + (32*discbu*tmp11*tmp15*tmp20*tmp45*tmp47*tmp49*tmp57*tmp98)/&
                      &tmp91

  END FUNCTION LBK_NLP_EP_QE3QM

  FUNCTION LBK_NLP_EP_QE4(me2, mm2, ss, tt, s15, s25, s35)
  implicit none
  real LBK_NLP_EP_Qe4
  real(kind=prec) ::  me2
  real(kind=prec) ::  mm2
  real(kind=prec) ::  ss
  real(kind=prec) ::  tt
  real(kind=prec) ::  s15
  real(kind=prec) ::  s25
  real(kind=prec) ::  s35
  real tmp1, tmp2, tmp3, tmp4, tmp5
  real tmp6, tmp7, tmp8, tmp9, tmp10
  real tmp11, tmp12, tmp13, tmp14, tmp15
  real tmp16, tmp17, tmp18, tmp19, tmp20
  real tmp21, tmp22, tmp23, tmp24, tmp25
  real tmp26, tmp27, tmp28, tmp29, tmp30
  real tmp31, tmp32, tmp33, tmp34, tmp35
  real tmp36

  tmp1 = -tt
  tmp2 = me2**2
  tmp3 = s15**(-2)
  tmp4 = 4*me2
  tmp5 = tmp1 + tmp4
  tmp6 = 1/tmp5
  tmp7 = 2*me2
  tmp8 = 2*mm2
  tmp9 = -2*ss
  tmp10 = tmp1 + tmp7 + tmp8 + tmp9
  tmp11 = tt**(-2)
  tmp12 = 1/s15
  tmp13 = -ss
  tmp14 = me2 + mm2 + tmp13
  tmp15 = 1/s35
  tmp16 = tmp1 + tmp7
  tmp17 = tmp1 + tmp14
  tmp18 = 1/tt
  tmp19 = -(1/tmp12)
  tmp20 = -s25
  tmp21 = 1/tmp15 + tmp19 + tmp20
  tmp22 = 1/tmp21
  tmp23 = -4*me2
  tmp24 = 1/tmp18 + tmp23
  tmp25 = 1/tmp24
  tmp26 = 2*tmp2
  tmp27 = mm2*tmp4
  tmp28 = mm2**2
  tmp29 = 2*tmp28
  tmp30 = ss*tmp23
  tmp31 = -4*mm2*ss
  tmp32 = ss**2
  tmp33 = 2*tmp32
  tmp34 = (2*ss)/tmp18
  tmp35 = tmp18**(-2)
  tmp36 = tmp26 + tmp27 + tmp29 + tmp30 + tmp31 + tmp33 + tmp34 + tmp35
  LBK_NLP_EP_Qe4 = -256*me2*tmp11*tmp22*tmp25*tmp36 - 128*discbtme*me2*tmp11*tmp22*tmp25*tmp36 - 12&
                    &8*me2*s25*tmp11*tmp12*tmp22*tmp25*tmp36 - 64*discbtme*me2*s25*tmp11*tmp12*tmp22*&
                    &tmp25*tmp36 + (128*me2*tmp11*tmp12*tmp22*tmp25*tmp36)/tmp15 + (64*discbtme*me2*t&
                    &mp11*tmp12*tmp22*tmp25*tmp36)/tmp15 + 128*me2*s25*tmp11*tmp15*tmp22*tmp25*tmp36 &
                    &+ 64*discbtme*me2*s25*tmp11*tmp15*tmp22*tmp25*tmp36 + (128*me2*tmp11*tmp15*tmp22&
                    &*tmp25*tmp36)/tmp12 + (64*discbtme*me2*tmp11*tmp15*tmp22*tmp25*tmp36)/tmp12 + 64&
                    &*tmp18*tmp22*tmp25*tmp36 + 64*discbtme*tmp18*tmp22*tmp25*tmp36 + 32*s25*tmp12*tm&
                    &p18*tmp22*tmp25*tmp36 + 32*discbtme*s25*tmp12*tmp18*tmp22*tmp25*tmp36 - (32*tmp1&
                    &2*tmp18*tmp22*tmp25*tmp36)/tmp15 - (32*discbtme*tmp12*tmp18*tmp22*tmp25*tmp36)/t&
                    &mp15 - 32*s25*tmp15*tmp18*tmp22*tmp25*tmp36 - 32*discbtme*s25*tmp15*tmp18*tmp22*&
                    &tmp25*tmp36 - (32*tmp15*tmp18*tmp22*tmp25*tmp36)/tmp12 - (32*discbtme*tmp15*tmp1&
                    &8*tmp22*tmp25*tmp36)/tmp12 + 128*me2*tmp10*tmp11*tmp12*tmp14*tmp6 + 64*discbtme*&
                    &me2*tmp10*tmp11*tmp12*tmp14*tmp6 - 128*me2*s25*tmp10*tmp11*tmp12*tmp15*tmp16*tmp&
                    &6 - 64*discbtme*me2*s25*tmp10*tmp11*tmp12*tmp15*tmp16*tmp6 - 128*me2*tmp10*tmp11&
                    &*tmp15*tmp17*tmp6 - 64*discbtme*me2*tmp10*tmp11*tmp15*tmp17*tmp6 - 32*tmp10*tmp1&
                    &2*tmp14*tmp18*tmp6 - 32*discbtme*tmp10*tmp12*tmp14*tmp18*tmp6 + 32*s25*tmp10*tmp&
                    &12*tmp15*tmp16*tmp18*tmp6 + 32*discbtme*s25*tmp10*tmp12*tmp15*tmp16*tmp18*tmp6 +&
                    & 32*tmp10*tmp15*tmp17*tmp18*tmp6 + 32*discbtme*tmp10*tmp15*tmp17*tmp18*tmp6 - 64&
                    &*me2*s25*tmp10*tmp18*tmp3*tmp6 - 64*discbtme*me2*s25*tmp10*tmp18*tmp3*tmp6 + 256&
                    &*s25*tmp10*tmp11*tmp2*tmp3*tmp6 + 128*discbtme*s25*tmp10*tmp11*tmp2*tmp3*tmp6

  END FUNCTION LBK_NLP_EP_QE4

  FUNCTION LBK_NLP_EP_QM4(me2, mm2, ss, tt, s15, s25, s35)
  implicit none
  real LBK_NLP_EP_Qm4
  real(kind=prec) ::  me2
  real(kind=prec) ::  mm2
  real(kind=prec) ::  ss
  real(kind=prec) ::  tt
  real(kind=prec) ::  s15
  real(kind=prec) ::  s25
  real(kind=prec) ::  s35
  real tmp1, tmp2, tmp3, tmp4, tmp5
  real tmp6, tmp7, tmp8, tmp9, tmp10
  real tmp11, tmp12, tmp13, tmp14, tmp15
  real tmp16, tmp17, tmp18, tmp19, tmp20
  real tmp21, tmp22, tmp23, tmp24, tmp25
  real tmp26, tmp27, tmp28, tmp29, tmp30
  real tmp31, tmp32, tmp33, tmp34, tmp35
  real tmp36, tmp37, tmp38, tmp39, tmp40

  tmp1 = -tt
  tmp2 = mm2**2
  tmp3 = s25**(-2)
  tmp4 = 4*mm2
  tmp5 = tmp1 + tmp4
  tmp6 = 1/tmp5
  tmp7 = 2*me2
  tmp8 = 2*mm2
  tmp9 = -2*ss
  tmp10 = tmp1 + tmp7 + tmp8 + tmp9
  tmp11 = tt**(-2)
  tmp12 = 1/s25
  tmp13 = -ss
  tmp14 = me2 + mm2 + tmp13
  tmp15 = -s35
  tmp16 = s15 + 1/tmp12 + tmp15
  tmp17 = 1/tmp16
  tmp18 = tmp1 + tmp8
  tmp19 = tmp1 + tmp14
  tmp20 = 1/tt
  tmp21 = 2*ss
  tmp22 = 2/tmp20
  tmp23 = tmp21 + tmp22
  tmp24 = tmp17**2
  tmp25 = tmp18**2
  tmp26 = tmp20**3
  tmp27 = tmp14**2
  tmp28 = 2*tmp27
  tmp29 = ss*tmp22
  tmp30 = tmp20**(-2)
  tmp31 = tmp28 + tmp29 + tmp30
  tmp32 = tmp6**2
  tmp33 = -tmp20
  tmp34 = -4*mm2
  tmp35 = 1/tmp20 + tmp34
  tmp36 = 1/tmp35
  tmp37 = -2*mm2
  tmp38 = 1/tmp20 + tmp37
  tmp39 = tmp20*tmp36*tmp38
  tmp40 = tmp33 + tmp39
  LBK_NLP_EP_Qm4 = -32*mm2*tmp11*tmp12*tmp23 + 32*mm2*tmp11*tmp17*tmp23 - 32*tmp11*tmp12*tmp18*tmp2&
                    &3 + 32*tmp11*tmp17*tmp18*tmp23 - (32*mm2*tmp11*tmp23*tmp24)/tmp12 + (32*mm2*tmp1&
                    &1*tmp23*tmp3)/tmp17 + 64*mm2*tmp12*tmp26*tmp31 - 64*mm2*tmp17*tmp26*tmp31 + 64*t&
                    &mp12*tmp18*tmp26*tmp31 - 64*tmp17*tmp18*tmp26*tmp31 + (64*mm2*tmp24*tmp26*tmp31)&
                    &/tmp12 - (64*mm2*tmp26*tmp3*tmp31)/tmp17 - 32*discbtmm*mm2*tmp11*tmp12*tmp18*tmp&
                    &31*tmp32 + 32*discbtmm*mm2*tmp11*tmp17*tmp18*tmp31*tmp32 - (32*discbtmm*mm2*tmp1&
                    &1*tmp18*tmp24*tmp31*tmp32)/tmp12 - 32*discbtmm*tmp11*tmp12*tmp25*tmp31*tmp32 + 3&
                    &2*discbtmm*tmp11*tmp17*tmp25*tmp31*tmp32 + (32*discbtmm*mm2*tmp11*tmp18*tmp3*tmp&
                    &31*tmp32)/tmp17 + 128*mm2*tmp10*tmp11*tmp12*tmp14*tmp6 + 64*discbtmm*mm2*tmp10*t&
                    &mp11*tmp12*tmp14*tmp6 - 128*mm2*s15*tmp10*tmp11*tmp12*tmp17*tmp18*tmp6 - 64*disc&
                    &btmm*mm2*s15*tmp10*tmp11*tmp12*tmp17*tmp18*tmp6 - 128*mm2*tmp10*tmp11*tmp17*tmp1&
                    &9*tmp6 - 64*discbtmm*mm2*tmp10*tmp11*tmp17*tmp19*tmp6 - 32*tmp10*tmp12*tmp14*tmp&
                    &20*tmp6 - 32*discbtmm*tmp10*tmp12*tmp14*tmp20*tmp6 + 32*s15*tmp10*tmp12*tmp17*tm&
                    &p18*tmp20*tmp6 + 32*discbtmm*s15*tmp10*tmp12*tmp17*tmp18*tmp20*tmp6 + 32*tmp10*t&
                    &mp17*tmp19*tmp20*tmp6 + 32*discbtmm*tmp10*tmp17*tmp19*tmp20*tmp6 - 32*discbtmm*m&
                    &m2*tmp11*tmp12*tmp18*tmp23*tmp6 + 32*discbtmm*mm2*tmp11*tmp17*tmp18*tmp23*tmp6 -&
                    & (32*discbtmm*mm2*tmp11*tmp18*tmp23*tmp24*tmp6)/tmp12 - 32*discbtmm*tmp11*tmp12*&
                    &tmp23*tmp25*tmp6 + 32*discbtmm*tmp11*tmp17*tmp23*tmp25*tmp6 + 256*s15*tmp10*tmp1&
                    &1*tmp2*tmp3*tmp6 + 128*discbtmm*s15*tmp10*tmp11*tmp2*tmp3*tmp6 - 64*mm2*s15*tmp1&
                    &0*tmp20*tmp3*tmp6 - 64*discbtmm*mm2*s15*tmp10*tmp20*tmp3*tmp6 + (32*discbtmm*mm2&
                    &*tmp11*tmp18*tmp23*tmp3*tmp6)/tmp17 + 32*discbtmm*mm2*tmp11*tmp12*tmp31*tmp6 - 3&
                    &2*discbtmm*mm2*tmp11*tmp17*tmp31*tmp6 + 32*discbtmm*tmp11*tmp12*tmp18*tmp31*tmp6&
                    & - 32*discbtmm*tmp11*tmp17*tmp18*tmp31*tmp6 + (32*discbtmm*mm2*tmp11*tmp24*tmp31&
                    &*tmp6)/tmp12 + 32*mm2*tmp12*tmp18*tmp26*tmp31*tmp6 + 64*discbtmm*mm2*tmp12*tmp18&
                    &*tmp26*tmp31*tmp6 - 32*mm2*tmp17*tmp18*tmp26*tmp31*tmp6 - 64*discbtmm*mm2*tmp17*&
                    &tmp18*tmp26*tmp31*tmp6 + (32*mm2*tmp18*tmp24*tmp26*tmp31*tmp6)/tmp12 + (64*discb&
                    &tmm*mm2*tmp18*tmp24*tmp26*tmp31*tmp6)/tmp12 + 32*tmp12*tmp25*tmp26*tmp31*tmp6 + &
                    &64*discbtmm*tmp12*tmp25*tmp26*tmp31*tmp6 - 32*tmp17*tmp25*tmp26*tmp31*tmp6 - 64*&
                    &discbtmm*tmp17*tmp25*tmp26*tmp31*tmp6 - (32*discbtmm*mm2*tmp11*tmp3*tmp31*tmp6)/&
                    &tmp17 - (32*mm2*tmp18*tmp26*tmp3*tmp31*tmp6)/tmp17 - (64*discbtmm*mm2*tmp18*tmp2&
                    &6*tmp3*tmp31*tmp6)/tmp17 - 32*discbtmm*mm2*tmp11*tmp12*tmp18*tmp31*tmp40*tmp6 + &
                    &32*discbtmm*mm2*tmp11*tmp17*tmp18*tmp31*tmp40*tmp6 - (32*discbtmm*mm2*tmp11*tmp1&
                    &8*tmp24*tmp31*tmp40*tmp6)/tmp12 - 32*discbtmm*tmp11*tmp12*tmp25*tmp31*tmp40*tmp6&
                    & + 32*discbtmm*tmp11*tmp17*tmp25*tmp31*tmp40*tmp6 + (32*discbtmm*mm2*tmp11*tmp18&
                    &*tmp3*tmp31*tmp40*tmp6)/tmp17

  END FUNCTION LBK_NLP_EP_QM4

  FUNCTION LBK_NLP_QE2QM2(me2, mm2, ss, tt, s15, s25, s35)
  implicit none
  real LBK_NLP_Qe2Qm2
  real(kind=prec) ::  me2
  real(kind=prec) ::  mm2
  real(kind=prec) ::  ss
  real(kind=prec) ::  tt
  real(kind=prec) ::  s15
  real(kind=prec) ::  s25
  real(kind=prec) ::  s35
  real tmp1, tmp2, tmp3, tmp4, tmp5
  real tmp6, tmp7, tmp8, tmp9, tmp10
  real tmp11, tmp12, tmp13, tmp14, tmp15
  real tmp16, tmp17, tmp18, tmp19, tmp20
  real tmp21, tmp22, tmp23, tmp24, tmp25
  real tmp26, tmp27, tmp28, tmp29, tmp30
  real tmp31, tmp32, tmp33, tmp34, tmp35
  real tmp36, tmp37, tmp38, tmp39, tmp40
  real tmp41, tmp42, tmp43, tmp44, tmp45
  real tmp46, tmp47, tmp48, tmp49, tmp50
  real tmp51, tmp52, tmp53, tmp54, tmp55
  real tmp56, tmp57, tmp58, tmp59, tmp60
  real tmp61, tmp62, tmp63, tmp64, tmp65
  real tmp66, tmp67, tmp68, tmp69, tmp70
  real tmp71, tmp72, tmp73, tmp74, tmp75
  real tmp76, tmp77, tmp78, tmp79, tmp80
  real tmp81, tmp82, tmp83, tmp84, tmp85
  real tmp86, tmp87, tmp88, tmp89, tmp90
  real tmp91, tmp92, tmp93, tmp94, tmp95
  real tmp96, tmp97, tmp98, tmp99, tmp100
  real tmp101, tmp102, tmp103, tmp104, tmp105
  real tmp106, tmp107, tmp108, tmp109, tmp110
  real tmp111, tmp112, tmp113, tmp114, tmp115
  real tmp116, tmp117, tmp118, tmp119, tmp120
  real tmp121, tmp122, tmp123, tmp124, tmp125
  real tmp126, tmp127, tmp128, tmp129, tmp130
  real tmp131, tmp132, tmp133, tmp134, tmp135
  real tmp136, tmp137, tmp138, tmp139, tmp140
  real tmp141, tmp142, tmp143, tmp144, tmp145
  real tmp146, tmp147, tmp148, tmp149, tmp150
  real tmp151, tmp152, tmp153, tmp154, tmp155
  real tmp156, tmp157, tmp158, tmp159, tmp160
  real tmp161, tmp162, tmp163, tmp164, tmp165
  real tmp166, tmp167, tmp168, tmp169, tmp170
  real tmp171, tmp172, tmp173, tmp174, tmp175
  real tmp176, tmp177, tmp178, tmp179, tmp180
  real tmp181, tmp182, tmp183, tmp184, tmp185
  real tmp186, tmp187, tmp188, tmp189, tmp190
  real tmp191, tmp192, tmp193, tmp194, tmp195
  real tmp196, tmp197, tmp198, tmp199, tmp200
  real tmp201, tmp202, tmp203, tmp204, tmp205
  real tmp206, tmp207, tmp208, tmp209, tmp210
  real tmp211, tmp212, tmp213, tmp214, tmp215
  real tmp216, tmp217, tmp218, tmp219, tmp220
  real tmp221, tmp222, tmp223, tmp224, tmp225
  real tmp226, tmp227, tmp228, tmp229, tmp230
  real tmp231, tmp232, tmp233, tmp234, tmp235
  real tmp236, tmp237, tmp238, tmp239, tmp240
  real tmp241, tmp242, tmp243, tmp244, tmp245
  real tmp246, tmp247, tmp248, tmp249, tmp250
  real tmp251, tmp252, tmp253, tmp254, tmp255
  real tmp256, tmp257, tmp258, tmp259, tmp260
  real tmp261

  tmp1 = -tt
  tmp2 = 1/s15
  tmp3 = -me2
  tmp4 = -mm2
  tmp5 = ss + tmp3 + tmp4
  tmp6 = 4*me2
  tmp7 = 4*mm2
  tmp8 = -2*ss
  tmp9 = tmp1 + tmp6 + tmp7 + tmp8
  tmp10 = -ss
  tmp11 = me2 + mm2 + tmp1 + tmp10
  tmp12 = tt**(-2)
  tmp13 = -s35
  tmp14 = s25 + tmp13 + 1/tmp2
  tmp15 = 1/s35
  tmp16 = sqrt(me2)
  tmp17 = sqrt(mm2)
  tmp18 = 1/s25
  tmp19 = -tmp17
  tmp20 = tmp16 + tmp19
  tmp21 = tmp20**2
  tmp22 = tmp1 + tmp10 + tmp21
  tmp23 = 1/tmp22
  tmp24 = tmp16 + tmp17
  tmp25 = tmp24**2
  tmp26 = tmp1 + tmp10 + tmp25
  tmp27 = 1/tmp26
  tmp28 = 2*me2
  tmp29 = 2*mm2
  tmp30 = tmp1 + tmp10 + tmp28 + tmp29
  tmp31 = 1/tmp14
  tmp32 = me2 + mm2 + tmp10
  tmp33 = 1/tt
  tmp34 = tmp10 + tmp21
  tmp35 = 1/tmp34
  tmp36 = tmp10 + tmp25
  tmp37 = 1/tmp36
  tmp38 = me2**2
  tmp39 = -2*me2*mm2
  tmp40 = mm2**2
  tmp41 = ss**2
  tmp42 = -tmp41
  tmp43 = tmp38 + tmp39 + tmp40 + tmp42
  tmp44 = mm2 + tmp10
  tmp45 = tmp44**2
  tmp46 = mm2 + ss
  tmp47 = -2*me2*tmp46
  tmp48 = tmp38 + tmp45 + tmp47
  tmp49 = 1/tmp48
  tmp50 = tmp1 + tmp28
  tmp51 = tmp1 + tmp29
  tmp52 = tmp27**2
  tmp53 = tmp23**2
  tmp54 = 1/tmp18 + 1/tmp2
  tmp55 = 4*tmp16*tmp17
  tmp56 = tmp1 + tmp6
  tmp57 = 1/tmp56
  tmp58 = tmp1 + tmp55
  tmp59 = 2*tmp32
  tmp60 = tmp1 + tmp59
  tmp61 = 1/tmp33 + tmp55
  tmp62 = -4*mm2
  tmp63 = 1/tmp33 + tmp62
  tmp64 = tmp63**(-2)
  tmp65 = 1/tmp63
  tmp66 = tmp57**2
  tmp67 = 1/tmp33 + tmp5
  tmp68 = 2*ss
  tmp69 = 1/tmp33 + tmp68
  tmp70 = me2 + tmp4
  tmp71 = tmp70**2
  tmp72 = tmp42 + tmp71
  tmp73 = me2*tmp8
  tmp74 = mm2*tmp8
  tmp75 = tmp38 + tmp39 + tmp40 + tmp41 + tmp73 + tmp74
  tmp76 = tmp75**(-2)
  tmp77 = tmp32**2
  tmp78 = 1/tmp75
  tmp79 = mm2 + tmp3
  tmp80 = 1/ss
  tmp81 = -2*me2
  tmp82 = -2*tmp44
  tmp83 = tmp81 + tmp82
  tmp84 = tmp49**2
  tmp85 = -tmp80
  tmp86 = tmp5*tmp78
  tmp87 = tmp85 + tmp86
  tmp88 = -8*me2
  tmp89 = 2/tmp33
  tmp90 = tmp88 + tmp89
  tmp91 = tmp1 + tmp7
  tmp92 = 1/tmp91
  tmp93 = -8*mm2
  tmp94 = tmp89 + tmp93
  tmp95 = -8*tmp32
  tmp96 = tmp89 + tmp95
  tmp97 = tmp18**2
  tmp98 = -4*tmp32
  tmp99 = tmp89 + tmp98
  tmp100 = tmp2**2
  tmp101 = 1/tmp33 + tmp81
  tmp102 = -2*mm2
  tmp103 = tmp102 + 1/tmp33
  tmp104 = tmp68 + tmp89
  tmp105 = tmp31**2
  tmp106 = 6/tmp33
  tmp107 = tmp106 + tmp95
  tmp108 = me2 + mm2
  tmp109 = -4*tmp108
  tmp110 = 6/tmp80
  tmp111 = tmp106 + tmp109 + tmp110
  tmp112 = -3*tmp38
  tmp113 = -6/tmp80
  tmp114 = tmp113 + tmp29
  tmp115 = tmp114*tmp3
  tmp116 = -3*tmp45
  tmp117 = -2/(tmp33*tmp80)
  tmp118 = tmp112 + tmp115 + tmp116 + tmp117
  tmp119 = -2*tmp32
  tmp120 = tmp119 + 1/tmp33
  tmp121 = tmp103 + 1/tmp80 + tmp81
  tmp122 = 1/tmp121
  tmp123 = 3*tmp40
  tmp124 = tmp29/tmp80
  tmp125 = tmp29 + 1/tmp80
  tmp126 = tmp125*tmp81
  tmp127 = tmp70 + 1/tmp80
  tmp128 = tmp127*tmp89
  tmp129 = tmp123 + tmp124 + tmp126 + tmp128 + tmp38 + tmp41
  tmp130 = 3*tmp38
  tmp131 = -7/tmp80
  tmp132 = mm2 + tmp131
  tmp133 = tmp132*tmp28
  tmp134 = 3*tmp45
  tmp135 = tmp130 + tmp133 + tmp134
  tmp136 = -2*tmp135
  tmp137 = tmp10 + tmp28
  tmp138 = (12*tmp137)/tmp33
  tmp139 = tmp33**(-2)
  tmp140 = -9*tmp139
  tmp141 = tmp136 + tmp138 + tmp140
  tmp142 = -32*me2*tmp32
  tmp143 = -14*me2
  tmp144 = -6*tmp44
  tmp145 = tmp143 + tmp144
  tmp146 = (-2*tmp145)/tmp33
  tmp147 = -6*tmp139
  tmp148 = tmp142 + tmp146 + tmp147
  tmp149 = -32*mm2*tmp32
  tmp150 = -6*me2
  tmp151 = -14*mm2
  tmp152 = tmp110 + tmp150 + tmp151
  tmp153 = (-2*tmp152)/tmp33
  tmp154 = tmp147 + tmp149 + tmp153
  tmp155 = 8*tmp38
  tmp156 = tmp88/tmp33
  tmp157 = tmp139 + tmp155 + tmp156
  tmp158 = tmp92**2
  tmp159 = 8*tmp40
  tmp160 = tmp93/tmp33
  tmp161 = tmp139 + tmp159 + tmp160
  tmp162 = tmp33**3
  tmp163 = 2*tmp77
  tmp164 = tmp89/tmp80
  tmp165 = tmp139 + tmp163 + tmp164
  tmp166 = tmp13 + 1/tmp2
  tmp167 = tmp37**2
  tmp168 = tmp35**2
  tmp169 = -4*me2
  tmp170 = tmp169 + 1/tmp33
  tmp171 = tmp170**(-2)
  tmp172 = 1/tmp170
  tmp173 = 4*tmp77
  tmp174 = tmp139 + tmp164 + tmp173
  tmp175 = tmp30**2
  tmp176 = -tmp175
  tmp177 = tmp176 + tmp71
  tmp178 = tmp81/tmp33
  tmp179 = tmp102/tmp33
  tmp180 = tmp139 + tmp164 + tmp178 + tmp179 + tmp75
  tmp181 = tmp180**(-2)
  tmp182 = 1/tmp30
  tmp183 = 1/tmp180
  tmp184 = tmp67**2
  tmp185 = tmp7*tmp83
  tmp186 = tmp29 + tmp68 + tmp81
  tmp187 = tmp186/tmp33
  tmp188 = tmp139 + tmp185 + tmp187
  tmp189 = tmp109/tmp33
  tmp190 = tmp110/tmp33
  tmp191 = 3*tmp139
  tmp192 = tmp173 + tmp189 + tmp190 + tmp191
  tmp193 = tmp122**2
  tmp194 = -tmp38
  tmp195 = tmp194 + tmp45 + tmp73
  tmp196 = tmp195*tmp7
  tmp197 = tmp123 + tmp124 + tmp126 + tmp38 + tmp41
  tmp198 = tmp197/tmp33
  tmp199 = tmp127*tmp139
  tmp200 = tmp196 + tmp198 + tmp199
  tmp201 = tmp80**2
  tmp202 = 16*me2*tmp77
  tmp203 = tmp136/tmp33
  tmp204 = 6*tmp137*tmp139
  tmp205 = 1/tmp162
  tmp206 = -3*tmp205
  tmp207 = tmp202 + tmp203 + tmp204 + tmp206
  tmp208 = -tmp33
  tmp209 = tmp101*tmp172*tmp33
  tmp210 = tmp208 + tmp209
  tmp211 = 1/tmp33 + tmp4 + 1/tmp80
  tmp212 = tmp211**2
  tmp213 = mm2 + 1/tmp33 + 1/tmp80
  tmp214 = tmp213*tmp81
  tmp215 = tmp212 + tmp214 + tmp38
  tmp216 = 1/tmp215
  tmp217 = tmp4 + 1/tmp80
  tmp218 = (2*tmp217)/tmp80
  tmp219 = tmp46*tmp89
  tmp220 = tmp8 + tmp94
  tmp221 = me2*tmp220
  tmp222 = tmp218 + tmp219 + tmp221
  tmp223 = -tmp182
  tmp224 = tmp11*tmp183
  tmp225 = tmp223 + tmp224
  tmp226 = -3/tmp80
  tmp227 = tmp217*tmp89
  tmp228 = 1/tmp33 + 1/tmp80
  tmp229 = -3*tmp228
  tmp230 = tmp226 + tmp229 + tmp29 + 1/tmp33
  tmp231 = me2*tmp230
  tmp232 = tmp130 + tmp134 + tmp139 + tmp164 + tmp227 + tmp231
  tmp233 = me2**3
  tmp234 = tmp44**3
  tmp235 = tmp40 + tmp42
  tmp236 = tmp235/tmp33
  tmp237 = tmp226 + 1/tmp33 + tmp4
  tmp238 = tmp237*tmp38
  tmp239 = -3*tmp41
  tmp240 = tmp228*tmp29
  tmp241 = tmp239 + tmp240 + tmp40
  tmp242 = tmp241*tmp3
  tmp243 = tmp233 + tmp234 + tmp236 + tmp238 + tmp242
  tmp244 = 3/tmp80
  tmp245 = 2*tmp211
  tmp246 = tmp245 + tmp81
  tmp247 = tmp216**2
  tmp248 = -tmp233
  tmp249 = -tmp234
  tmp250 = mm2 + tmp244
  tmp251 = tmp250*tmp38
  tmp252 = tmp227/tmp80
  tmp253 = tmp139*tmp46
  tmp254 = -4/tmp33
  tmp255 = tmp254 + 1/tmp80
  tmp256 = tmp255*tmp29
  tmp257 = tmp1 + tmp244
  tmp258 = -(tmp228*tmp257)
  tmp259 = tmp256 + tmp258 + tmp40
  tmp260 = me2*tmp259
  tmp261 = tmp248 + tmp249 + tmp251 + tmp252 + tmp253 + tmp260
  LBK_NLP_Qe2Qm2 = (-64*mm2*tmp104*tmp105*tmp12)/tmp18 - (32*logmume*mm2*tmp104*tmp105*tmp12)/tmp18&
                    & - (32*mm2*scalarc0ir6tme*tmp101*tmp104*tmp105*tmp12)/tmp18 - (32*mm2*scalarc0ir&
                    &6tme*tmp105*tmp12*tmp165)/tmp18 + (128*mm2*tmp105*tmp162*tmp165)/tmp18 + (64*log&
                    &mume*mm2*tmp105*tmp162*tmp165)/tmp18 + (64*mm2*scalarc0ir6tme*tmp101*tmp105*tmp1&
                    &62*tmp165)/tmp18 + (32*discbtme*mm2*tmp101*tmp105*tmp12*tmp165*tmp171)/tmp18 - 6&
                    &4*mm2*tmp104*tmp12*tmp18 - 32*logmume*mm2*tmp104*tmp12*tmp18 - 32*mm2*scalarc0ir&
                    &6tme*tmp101*tmp104*tmp12*tmp18 - 32*mm2*scalarc0ir6tme*tmp12*tmp165*tmp18 + 128*&
                    &mm2*tmp162*tmp165*tmp18 + 64*logmume*mm2*tmp162*tmp165*tmp18 + 64*mm2*scalarc0ir&
                    &6tme*tmp101*tmp162*tmp165*tmp18 + 32*discbtme*mm2*tmp101*tmp12*tmp165*tmp171*tmp&
                    &18 + 64*tmp12*tmp15*tmp165*tmp166*tmp2 + 32*logmumm*tmp12*tmp15*tmp165*tmp166*tm&
                    &p2 + 32*scalarc0ir6tmm*tmp103*tmp12*tmp15*tmp165*tmp166*tmp2 + 64*logmume*mm2*tm&
                    &p11*tmp12*tmp165*tmp18*tmp23*tmp27 - 64*discbu*logmume*mm2*tmp11*tmp12*tmp165*tm&
                    &p18*tmp23*tmp27 + (64*discbu*logmume*tmp11*tmp12*tmp15*tmp165*tmp23*tmp27)/tmp18&
                    &2 + (32*discbulogt*mm2*tmp107*tmp11*tmp12*tmp18*tmp23*tmp27)/tmp182 - (64*discbu&
                    &*logmume*mm2*tmp12*tmp165*tmp18*tmp23*tmp27)/tmp182 - 32*discbulogt*mm2*tmp11*tm&
                    &p12*tmp18*tmp192*tmp23*tmp27 + 32*logt*mm2*tmp11*tmp12*tmp18*tmp192*tmp23*tmp27 &
                    &+ (32*discbulogt*tmp11*tmp12*tmp15*tmp192*tmp23*tmp27)/tmp182 - (32*discbulogt*m&
                    &m2*tmp12*tmp18*tmp192*tmp23*tmp27)/tmp182 + 64*logmume*me2*tmp11*tmp12*tmp165*tm&
                    &p2*tmp23*tmp27 - 64*discbu*logmume*me2*tmp11*tmp12*tmp165*tmp2*tmp23*tmp27 + (32&
                    &*discbulogt*me2*tmp107*tmp11*tmp12*tmp2*tmp23*tmp27)/tmp182 - (64*discbu*logmume&
                    &*me2*tmp12*tmp165*tmp2*tmp23*tmp27)/tmp182 - 32*discbulogt*me2*tmp11*tmp12*tmp19&
                    &2*tmp2*tmp23*tmp27 + 32*logt*me2*tmp11*tmp12*tmp192*tmp2*tmp23*tmp27 - (32*discb&
                    &ulogt*me2*tmp12*tmp192*tmp2*tmp23*tmp27)/tmp182 - (64*discbu*logmume*mm2*tmp11*t&
                    &mp12*tmp165*tmp18*tmp225*tmp23*tmp27)/tmp182 - (32*discbulogt*mm2*tmp11*tmp12*tm&
                    &p18*tmp192*tmp225*tmp23*tmp27)/tmp182 - (64*discbu*logmume*me2*tmp11*tmp12*tmp16&
                    &5*tmp2*tmp225*tmp23*tmp27)/tmp182 - (32*discbulogt*me2*tmp11*tmp12*tmp192*tmp2*t&
                    &mp225*tmp23*tmp27)/tmp182 + 64*mm2*tmp104*tmp12*tmp31 + 32*logmume*mm2*tmp104*tm&
                    &p12*tmp31 + 32*mm2*scalarc0ir6tme*tmp101*tmp104*tmp12*tmp31 + 32*mm2*scalarc0ir6&
                    &tme*tmp12*tmp165*tmp31 - 128*mm2*tmp162*tmp165*tmp31 - 64*logmume*mm2*tmp162*tmp&
                    &165*tmp31 - 64*mm2*scalarc0ir6tme*tmp101*tmp162*tmp165*tmp31 - 32*discbtme*mm2*t&
                    &mp101*tmp12*tmp165*tmp171*tmp31 + (64*discbu*logmume*tmp11*tmp12*tmp165*tmp2*tmp&
                    &23*tmp27*tmp31)/(tmp15*tmp182) + (32*discbulogt*tmp11*tmp12*tmp192*tmp2*tmp23*tm&
                    &p27*tmp31)/(tmp15*tmp182) - 64*mm2*scalarc0ir6u*tmp11*tmp18*tmp33 - 64*me2*scala&
                    &rc0ir6u*tmp11*tmp2*tmp33 - 32*discbu*mm2*tmp18*tmp216*tmp232*tmp33 - 32*discbu*m&
                    &e2*tmp2*tmp216*tmp232*tmp33 - 32*discbu*tmp15*tmp216*tmp261*tmp33 - 32*mm2*tmp18&
                    &*tmp182*tmp216*tmp261*tmp33 - 32*me2*tmp182*tmp2*tmp216*tmp261*tmp33 + 32*discbu&
                    &*mm2*tmp18*tmp216*tmp225*tmp261*tmp33 + 32*discbu*me2*tmp2*tmp216*tmp225*tmp261*&
                    &tmp33 + 32*discbu*mm2*tmp18*tmp246*tmp247*tmp261*tmp33 + 32*discbu*me2*tmp2*tmp2&
                    &46*tmp247*tmp261*tmp33 - (32*discbu*logmm*mm2*tmp11*tmp18*tmp23*tmp27*tmp33)/tmp&
                    &182 - (32*discbu*logmm*me2*tmp11*tmp2*tmp23*tmp27*tmp33)/tmp182 - (32*discbu*tmp&
                    &2*tmp216*tmp261*tmp31*tmp33)/tmp15 + 64*mm2*scalarc0ir6s*tmp18*tmp32*tmp33 + 64*&
                    &me2*scalarc0ir6s*tmp2*tmp32*tmp33 + 32*discbs*mm2*tmp167*tmp18*tmp243*tmp33*tmp3&
                    &5 + 32*discbs*me2*tmp167*tmp2*tmp243*tmp33*tmp35 + 32*discbs*mm2*tmp168*tmp18*tm&
                    &p243*tmp33*tmp37 + 32*discbs*me2*tmp168*tmp2*tmp243*tmp33*tmp37 + logt*tmp12*tmp&
                    &149*tmp174*tmp18*tmp35*tmp37 + logt*tmp12*tmp142*tmp174*tmp2*tmp35*tmp37 - 64*lo&
                    &gmume*mm2*tmp12*tmp165*tmp18*tmp32*tmp35*tmp37 + 64*discbs*logmume*mm2*tmp12*tmp&
                    &165*tmp18*tmp32*tmp35*tmp37 + 32*discbslogt*mm2*tmp12*tmp174*tmp18*tmp32*tmp35*t&
                    &mp37 - 64*logmume*me2*tmp12*tmp165*tmp2*tmp32*tmp35*tmp37 + 64*discbs*logmume*me&
                    &2*tmp12*tmp165*tmp2*tmp32*tmp35*tmp37 + 32*discbslogt*me2*tmp12*tmp174*tmp2*tmp3&
                    &2*tmp35*tmp37 + 32*discbs*mm2*tmp118*tmp18*tmp33*tmp35*tmp37 + 32*discbs*me2*tmp&
                    &118*tmp2*tmp33*tmp35*tmp37 + 32*discbs*tmp15*tmp243*tmp33*tmp35*tmp37 + (32*disc&
                    &bs*tmp2*tmp243*tmp31*tmp33*tmp35*tmp37)/tmp15 + 16*discbu*tmp12*tmp15*tmp216*tmp&
                    &261*tmp5 - 16*discbu*tmp12*tmp2*tmp216*tmp261*tmp5 + 32*logmume*tmp11*tmp12*tmp1&
                    &5*tmp165*tmp23*tmp27*tmp5 - 32*discbu*logmume*tmp11*tmp12*tmp15*tmp165*tmp23*tmp&
                    &27*tmp5 - 32*logmume*tmp11*tmp12*tmp165*tmp18*tmp23*tmp27*tmp5 + 32*discbu*logmu&
                    &me*tmp11*tmp12*tmp165*tmp18*tmp23*tmp27*tmp5 + (32*discbu*logmume*tmp104*tmp11*t&
                    &mp12*tmp15*tmp23*tmp27*tmp5)/tmp182 + (16*discbulogt*tmp11*tmp111*tmp12*tmp15*tm&
                    &p23*tmp27*tmp5)/tmp182 - (32*discbu*logmume*tmp12*tmp15*tmp165*tmp23*tmp27*tmp5)&
                    &/tmp182 - (64*discbu*logmume*tmp11*tmp15*tmp162*tmp165*tmp23*tmp27*tmp5)/tmp182 &
                    &- (16*discbulogt*tmp107*tmp11*tmp12*tmp18*tmp23*tmp27*tmp5)/tmp182 + (32*discbu*&
                    &logmume*tmp12*tmp165*tmp18*tmp23*tmp27*tmp5)/tmp182 - 16*discbulogt*tmp11*tmp12*&
                    &tmp15*tmp192*tmp23*tmp27*tmp5 + 16*logt*tmp11*tmp12*tmp15*tmp192*tmp23*tmp27*tmp&
                    &5 + 16*discbulogt*tmp11*tmp12*tmp18*tmp192*tmp23*tmp27*tmp5 - 16*logt*tmp11*tmp1&
                    &2*tmp18*tmp192*tmp23*tmp27*tmp5 - (16*discbulogt*tmp12*tmp15*tmp192*tmp23*tmp27*&
                    &tmp5)/tmp182 - (16*discbu*tmp11*tmp15*tmp162*tmp192*tmp23*tmp27*tmp5)/tmp182 - (&
                    &32*discbulogt*tmp11*tmp15*tmp162*tmp192*tmp23*tmp27*tmp5)/tmp182 + (16*discbulog&
                    &t*tmp12*tmp18*tmp192*tmp23*tmp27*tmp5)/tmp182 - 64*logmume*tmp11*tmp12*tmp165*tm&
                    &p2*tmp23*tmp27*tmp5 + 64*discbu*logmume*tmp11*tmp12*tmp165*tmp2*tmp23*tmp27*tmp5&
                    & - (32*discbu*logmume*tmp104*tmp11*tmp12*tmp2*tmp23*tmp27*tmp5)/tmp182 - (16*dis&
                    &cbulogt*tmp107*tmp11*tmp12*tmp2*tmp23*tmp27*tmp5)/tmp182 - (16*discbulogt*tmp11*&
                    &tmp111*tmp12*tmp2*tmp23*tmp27*tmp5)/tmp182 + (64*discbu*logmume*tmp12*tmp165*tmp&
                    &2*tmp23*tmp27*tmp5)/tmp182 + (64*discbu*logmume*tmp11*tmp162*tmp165*tmp2*tmp23*t&
                    &mp27*tmp5)/tmp182 + 32*discbulogt*tmp11*tmp12*tmp192*tmp2*tmp23*tmp27*tmp5 - 32*&
                    &logt*tmp11*tmp12*tmp192*tmp2*tmp23*tmp27*tmp5 + (32*discbulogt*tmp12*tmp192*tmp2&
                    &*tmp23*tmp27*tmp5)/tmp182 + (16*discbu*tmp11*tmp162*tmp192*tmp2*tmp23*tmp27*tmp5&
                    &)/tmp182 + (32*discbulogt*tmp11*tmp162*tmp192*tmp2*tmp23*tmp27*tmp5)/tmp182 - (3&
                    &2*discbu*logmume*tmp11*tmp12*tmp15*tmp165*tmp225*tmp23*tmp27*tmp5)/tmp182 + (32*&
                    &discbu*logmume*tmp11*tmp12*tmp165*tmp18*tmp225*tmp23*tmp27*tmp5)/tmp182 - (16*di&
                    &scbulogt*tmp11*tmp12*tmp15*tmp192*tmp225*tmp23*tmp27*tmp5)/tmp182 + (16*discbulo&
                    &gt*tmp11*tmp12*tmp18*tmp192*tmp225*tmp23*tmp27*tmp5)/tmp182 + (64*discbu*logmume&
                    &*tmp11*tmp12*tmp165*tmp2*tmp225*tmp23*tmp27*tmp5)/tmp182 + (32*discbulogt*tmp11*&
                    &tmp12*tmp192*tmp2*tmp225*tmp23*tmp27*tmp5)/tmp182 + (16*discbu*tmp12*tmp18*tmp2*&
                    &tmp216*tmp261*tmp5)/tmp31 + (32*logmume*tmp11*tmp12*tmp165*tmp18*tmp2*tmp23*tmp2&
                    &7*tmp5)/tmp31 - (32*discbu*logmume*tmp11*tmp12*tmp165*tmp18*tmp2*tmp23*tmp27*tmp&
                    &5)/tmp31 + (32*discbu*logmume*tmp104*tmp11*tmp12*tmp18*tmp2*tmp23*tmp27*tmp5)/(t&
                    &mp182*tmp31) + (16*discbulogt*tmp11*tmp111*tmp12*tmp18*tmp2*tmp23*tmp27*tmp5)/(t&
                    &mp182*tmp31) - (32*discbu*logmume*tmp12*tmp165*tmp18*tmp2*tmp23*tmp27*tmp5)/(tmp&
                    &182*tmp31) - (64*discbu*logmume*tmp11*tmp162*tmp165*tmp18*tmp2*tmp23*tmp27*tmp5)&
                    &/(tmp182*tmp31) - (16*discbulogt*tmp11*tmp12*tmp18*tmp192*tmp2*tmp23*tmp27*tmp5)&
                    &/tmp31 + (16*logt*tmp11*tmp12*tmp18*tmp192*tmp2*tmp23*tmp27*tmp5)/tmp31 - (16*di&
                    &scbulogt*tmp12*tmp18*tmp192*tmp2*tmp23*tmp27*tmp5)/(tmp182*tmp31) - (16*discbu*t&
                    &mp11*tmp162*tmp18*tmp192*tmp2*tmp23*tmp27*tmp5)/(tmp182*tmp31) - (32*discbulogt*&
                    &tmp11*tmp162*tmp18*tmp192*tmp2*tmp23*tmp27*tmp5)/(tmp182*tmp31) - (32*discbu*log&
                    &mume*tmp11*tmp12*tmp165*tmp18*tmp2*tmp225*tmp23*tmp27*tmp5)/(tmp182*tmp31) - (16&
                    &*discbulogt*tmp11*tmp12*tmp18*tmp192*tmp2*tmp225*tmp23*tmp27*tmp5)/(tmp182*tmp31&
                    &) - (16*discbu*tmp12*tmp15*tmp216*tmp261*tmp31*tmp5)/tmp18 - (32*logmume*tmp11*t&
                    &mp12*tmp15*tmp165*tmp23*tmp27*tmp31*tmp5)/tmp18 + (32*discbu*logmume*tmp11*tmp12&
                    &*tmp15*tmp165*tmp23*tmp27*tmp31*tmp5)/tmp18 - (32*discbu*logmume*tmp104*tmp11*tm&
                    &p12*tmp15*tmp23*tmp27*tmp31*tmp5)/(tmp18*tmp182) - (16*discbulogt*tmp11*tmp111*t&
                    &mp12*tmp15*tmp23*tmp27*tmp31*tmp5)/(tmp18*tmp182) + (32*discbu*logmume*tmp12*tmp&
                    &15*tmp165*tmp23*tmp27*tmp31*tmp5)/(tmp18*tmp182) + (64*discbu*logmume*tmp11*tmp1&
                    &5*tmp162*tmp165*tmp23*tmp27*tmp31*tmp5)/(tmp18*tmp182) + (16*discbulogt*tmp11*tm&
                    &p12*tmp15*tmp192*tmp23*tmp27*tmp31*tmp5)/tmp18 - (16*logt*tmp11*tmp12*tmp15*tmp1&
                    &92*tmp23*tmp27*tmp31*tmp5)/tmp18 + (16*discbulogt*tmp12*tmp15*tmp192*tmp23*tmp27&
                    &*tmp31*tmp5)/(tmp18*tmp182) + (16*discbu*tmp11*tmp15*tmp162*tmp192*tmp23*tmp27*t&
                    &mp31*tmp5)/(tmp18*tmp182) + (32*discbulogt*tmp11*tmp15*tmp162*tmp192*tmp23*tmp27&
                    &*tmp31*tmp5)/(tmp18*tmp182) + (32*discbu*logmume*tmp11*tmp12*tmp15*tmp165*tmp225&
                    &*tmp23*tmp27*tmp31*tmp5)/(tmp18*tmp182) + (16*discbulogt*tmp11*tmp12*tmp15*tmp19&
                    &2*tmp225*tmp23*tmp27*tmp31*tmp5)/(tmp18*tmp182) - 16*scalarc0ir6u*tmp11*tmp15*tm&
                    &p33*tmp5 + 32*scalarc0ir6u*tmp11*tmp18*tmp33*tmp5 + 48*scalarc0ir6u*tmp11*tmp2*t&
                    &mp33*tmp5 - 16*discbu*tmp15*tmp216*tmp222*tmp33*tmp5 + 16*discbu*tmp2*tmp216*tmp&
                    &222*tmp33*tmp5 + 16*discbu*tmp18*tmp216*tmp232*tmp33*tmp5 + 16*discbu*tmp2*tmp21&
                    &6*tmp232*tmp33*tmp5 - 16*tmp15*tmp182*tmp216*tmp261*tmp33*tmp5 + 16*tmp18*tmp182&
                    &*tmp216*tmp261*tmp33*tmp5 + 32*tmp182*tmp2*tmp216*tmp261*tmp33*tmp5 + 16*discbu*&
                    &tmp15*tmp216*tmp225*tmp261*tmp33*tmp5 - 16*discbu*tmp18*tmp216*tmp225*tmp261*tmp&
                    &33*tmp5 - 32*discbu*tmp2*tmp216*tmp225*tmp261*tmp33*tmp5 + 16*discbu*tmp15*tmp24&
                    &6*tmp247*tmp261*tmp33*tmp5 - 16*discbu*tmp18*tmp246*tmp247*tmp261*tmp33*tmp5 - 3&
                    &2*discbu*tmp2*tmp246*tmp247*tmp261*tmp33*tmp5 - (8*discbu*logmm*tmp11*tmp15*tmp2&
                    &3*tmp27*tmp33*tmp5)/tmp182 + (16*discbu*logmm*tmp11*tmp18*tmp23*tmp27*tmp33*tmp5&
                    &)/tmp182 + (24*discbu*logmm*tmp11*tmp2*tmp23*tmp27*tmp33*tmp5)/tmp182 - (16*scal&
                    &arc0ir6u*tmp11*tmp18*tmp2*tmp33*tmp5)/tmp31 - (16*discbu*tmp18*tmp2*tmp216*tmp22&
                    &2*tmp33*tmp5)/tmp31 - (16*tmp18*tmp182*tmp2*tmp216*tmp261*tmp33*tmp5)/tmp31 + (1&
                    &6*discbu*tmp18*tmp2*tmp216*tmp225*tmp261*tmp33*tmp5)/tmp31 + (16*discbu*tmp18*tm&
                    &p2*tmp246*tmp247*tmp261*tmp33*tmp5)/tmp31 - (8*discbu*logmm*tmp11*tmp18*tmp2*tmp&
                    &23*tmp27*tmp33*tmp5)/(tmp182*tmp31) + (16*scalarc0ir6u*tmp11*tmp15*tmp31*tmp33*t&
                    &mp5)/tmp18 + (16*discbu*tmp15*tmp216*tmp222*tmp31*tmp33*tmp5)/tmp18 + (16*tmp15*&
                    &tmp182*tmp216*tmp261*tmp31*tmp33*tmp5)/tmp18 - (16*discbu*tmp15*tmp216*tmp225*tm&
                    &p261*tmp31*tmp33*tmp5)/tmp18 - (16*discbu*tmp15*tmp246*tmp247*tmp261*tmp31*tmp33&
                    &*tmp5)/tmp18 + (8*discbu*logmm*tmp11*tmp15*tmp23*tmp27*tmp31*tmp33*tmp5)/(tmp18*&
                    &tmp182) + 16*scalarc0ir6s*tmp15*tmp32*tmp33*tmp5 - 32*scalarc0ir6s*tmp18*tmp32*t&
                    &mp33*tmp5 - 48*scalarc0ir6s*tmp2*tmp32*tmp33*tmp5 + (16*scalarc0ir6s*tmp18*tmp2*&
                    &tmp32*tmp33*tmp5)/tmp31 - (16*scalarc0ir6s*tmp15*tmp31*tmp32*tmp33*tmp5)/tmp18 -&
                    & 16*discbs*tmp167*tmp18*tmp243*tmp33*tmp35*tmp5 - 16*discbs*tmp167*tmp2*tmp243*t&
                    &mp33*tmp35*tmp5 - 16*discbs*tmp168*tmp18*tmp243*tmp33*tmp37*tmp5 - 16*discbs*tmp&
                    &168*tmp2*tmp243*tmp33*tmp37*tmp5 - 16*discbs*tmp12*tmp15*tmp243*tmp35*tmp37*tmp5&
                    & + 16*discbs*tmp12*tmp2*tmp243*tmp35*tmp37*tmp5 - (16*discbs*tmp12*tmp18*tmp2*tm&
                    &p243*tmp35*tmp37*tmp5)/tmp31 + (16*discbs*tmp12*tmp15*tmp243*tmp31*tmp35*tmp37*t&
                    &mp5)/tmp18 + 32*logmume*tmp12*tmp165*tmp18*tmp32*tmp35*tmp37*tmp5 - 32*discbs*lo&
                    &gmume*tmp12*tmp165*tmp18*tmp32*tmp35*tmp37*tmp5 - 16*discbslogt*tmp12*tmp174*tmp&
                    &18*tmp32*tmp35*tmp37*tmp5 + 16*logt*tmp12*tmp174*tmp18*tmp32*tmp35*tmp37*tmp5 + &
                    &32*logmume*tmp12*tmp165*tmp2*tmp32*tmp35*tmp37*tmp5 - 32*discbs*logmume*tmp12*tm&
                    &p165*tmp2*tmp32*tmp35*tmp37*tmp5 - 16*discbslogt*tmp12*tmp174*tmp2*tmp32*tmp35*t&
                    &mp37*tmp5 + 16*logt*tmp12*tmp174*tmp2*tmp32*tmp35*tmp37*tmp5 - 16*discbs*tmp118*&
                    &tmp18*tmp33*tmp35*tmp37*tmp5 - 16*discbs*tmp118*tmp2*tmp33*tmp35*tmp37*tmp5 + 16&
                    &*discbs*tmp15*tmp33*tmp35*tmp37*tmp43*tmp5 - 16*discbs*tmp2*tmp33*tmp35*tmp37*tm&
                    &p43*tmp5 + (16*discbs*tmp18*tmp2*tmp33*tmp35*tmp37*tmp43*tmp5)/tmp31 - (16*discb&
                    &s*tmp15*tmp31*tmp33*tmp35*tmp37*tmp43*tmp5)/tmp18 - (32*mm2*scalarc0ir6tme*tmp10&
                    &1*tmp105*tmp162*tmp165*tmp172*tmp50)/tmp18 - 32*mm2*scalarc0ir6tme*tmp101*tmp162&
                    &*tmp165*tmp172*tmp18*tmp50 - 32*logmume*tmp11*tmp12*tmp15*tmp165*tmp23*tmp27*tmp&
                    &50 + 32*discbu*logmume*tmp11*tmp12*tmp15*tmp165*tmp23*tmp27*tmp50 - (16*discbulo&
                    &gt*tmp107*tmp11*tmp12*tmp15*tmp23*tmp27*tmp50)/tmp182 + (32*discbu*logmume*tmp12&
                    &*tmp15*tmp165*tmp23*tmp27*tmp50)/tmp182 + 16*discbulogt*tmp11*tmp12*tmp15*tmp192&
                    &*tmp23*tmp27*tmp50 - 16*logt*tmp11*tmp12*tmp15*tmp192*tmp23*tmp27*tmp50 + (16*di&
                    &scbulogt*tmp12*tmp15*tmp192*tmp23*tmp27*tmp50)/tmp182 + (32*discbu*logmume*tmp11&
                    &*tmp12*tmp15*tmp165*tmp225*tmp23*tmp27*tmp50)/tmp182 + (16*discbulogt*tmp11*tmp1&
                    &2*tmp15*tmp192*tmp225*tmp23*tmp27*tmp50)/tmp182 + 32*mm2*scalarc0ir6tme*tmp101*t&
                    &mp162*tmp165*tmp172*tmp31*tmp50 + 32*scalarc0ir6u*tmp11*tmp15*tmp33*tmp50 + 16*d&
                    &iscbu*tmp15*tmp216*tmp232*tmp33*tmp50 + 16*tmp15*tmp182*tmp216*tmp261*tmp33*tmp5&
                    &0 - 16*discbu*tmp15*tmp216*tmp225*tmp261*tmp33*tmp50 - 16*discbu*tmp15*tmp246*tm&
                    &p247*tmp261*tmp33*tmp50 + (16*discbu*logmm*tmp11*tmp15*tmp23*tmp27*tmp33*tmp50)/&
                    &tmp182 - 32*scalarc0ir6s*tmp15*tmp32*tmp33*tmp50 - 16*discbs*tmp15*tmp167*tmp243&
                    &*tmp33*tmp35*tmp50 - 16*discbs*tmp15*tmp168*tmp243*tmp33*tmp37*tmp50 + 32*logmum&
                    &e*tmp12*tmp15*tmp165*tmp32*tmp35*tmp37*tmp50 - 32*discbs*logmume*tmp12*tmp15*tmp&
                    &165*tmp32*tmp35*tmp37*tmp50 - 16*discbslogt*tmp12*tmp15*tmp174*tmp32*tmp35*tmp37&
                    &*tmp50 + 16*logt*tmp12*tmp15*tmp174*tmp32*tmp35*tmp37*tmp50 - 16*discbs*tmp118*t&
                    &mp15*tmp33*tmp35*tmp37*tmp50 - 64*tmp104*tmp12*tmp18*tmp51 - 32*logmume*tmp104*t&
                    &mp12*tmp18*tmp51 - 32*scalarc0ir6tme*tmp101*tmp104*tmp12*tmp18*tmp51 - 32*scalar&
                    &c0ir6tme*tmp12*tmp165*tmp18*tmp51 + 128*tmp162*tmp165*tmp18*tmp51 + 64*logmume*t&
                    &mp162*tmp165*tmp18*tmp51 + 64*scalarc0ir6tme*tmp101*tmp162*tmp165*tmp18*tmp51 + &
                    &32*discbtme*tmp101*tmp12*tmp165*tmp171*tmp18*tmp51 + 64*tmp104*tmp12*tmp31*tmp51&
                    & + 32*logmume*tmp104*tmp12*tmp31*tmp51 + 32*scalarc0ir6tme*tmp101*tmp104*tmp12*t&
                    &mp31*tmp51 + 32*scalarc0ir6tme*tmp12*tmp165*tmp31*tmp51 - 128*tmp162*tmp165*tmp3&
                    &1*tmp51 - 64*logmume*tmp162*tmp165*tmp31*tmp51 - 64*scalarc0ir6tme*tmp101*tmp162&
                    &*tmp165*tmp31*tmp51 - 32*discbtme*tmp101*tmp12*tmp165*tmp171*tmp31*tmp51 - 32*lo&
                    &gmume*tmp11*tmp12*tmp165*tmp23*tmp27*tmp31*tmp51 + 32*discbu*logmume*tmp11*tmp12&
                    &*tmp165*tmp23*tmp27*tmp31*tmp51 - (16*discbulogt*tmp107*tmp11*tmp12*tmp23*tmp27*&
                    &tmp31*tmp51)/tmp182 + (32*discbu*logmume*tmp12*tmp165*tmp23*tmp27*tmp31*tmp51)/t&
                    &mp182 + 16*discbulogt*tmp11*tmp12*tmp192*tmp23*tmp27*tmp31*tmp51 - 16*logt*tmp11&
                    &*tmp12*tmp192*tmp23*tmp27*tmp31*tmp51 + (16*discbulogt*tmp12*tmp192*tmp23*tmp27*&
                    &tmp31*tmp51)/tmp182 + (32*discbu*logmume*tmp11*tmp12*tmp165*tmp225*tmp23*tmp27*t&
                    &mp31*tmp51)/tmp182 + (16*discbulogt*tmp11*tmp12*tmp192*tmp225*tmp23*tmp27*tmp31*&
                    &tmp51)/tmp182 + 32*scalarc0ir6u*tmp11*tmp31*tmp33*tmp51 + 16*discbu*tmp216*tmp23&
                    &2*tmp31*tmp33*tmp51 + 16*tmp182*tmp216*tmp261*tmp31*tmp33*tmp51 - 16*discbu*tmp2&
                    &16*tmp225*tmp261*tmp31*tmp33*tmp51 - 16*discbu*tmp246*tmp247*tmp261*tmp31*tmp33*&
                    &tmp51 + (16*discbu*logmm*tmp11*tmp23*tmp27*tmp31*tmp33*tmp51)/tmp182 - 32*scalar&
                    &c0ir6s*tmp31*tmp32*tmp33*tmp51 - 16*discbs*tmp167*tmp243*tmp31*tmp33*tmp35*tmp51&
                    & - 16*discbs*tmp168*tmp243*tmp31*tmp33*tmp37*tmp51 + 32*logmume*tmp12*tmp165*tmp&
                    &31*tmp32*tmp35*tmp37*tmp51 - 32*discbs*logmume*tmp12*tmp165*tmp31*tmp32*tmp35*tm&
                    &p37*tmp51 - 16*discbslogt*tmp12*tmp174*tmp31*tmp32*tmp35*tmp37*tmp51 + 16*logt*t&
                    &mp12*tmp174*tmp31*tmp32*tmp35*tmp37*tmp51 - 16*discbs*tmp118*tmp31*tmp33*tmp35*t&
                    &mp37*tmp51 - 32*scalarc0ir6tme*tmp101*tmp162*tmp165*tmp172*tmp18*tmp50*tmp51 + 3&
                    &2*scalarc0ir6tme*tmp101*tmp162*tmp165*tmp172*tmp31*tmp50*tmp51 + (64*discbu*logm&
                    &ume*mm2*tmp11*tmp12*tmp165*tmp18*tmp23*tmp52)/tmp182 + (32*discbulogt*mm2*tmp11*&
                    &tmp12*tmp18*tmp192*tmp23*tmp52)/tmp182 + (64*discbu*logmume*me2*tmp11*tmp12*tmp1&
                    &65*tmp2*tmp23*tmp52)/tmp182 + (32*discbulogt*me2*tmp11*tmp12*tmp192*tmp2*tmp23*t&
                    &mp52)/tmp182 + (32*discbu*logmume*tmp11*tmp12*tmp15*tmp165*tmp23*tmp5*tmp52)/tmp&
                    &182 - (32*discbu*logmume*tmp11*tmp12*tmp165*tmp18*tmp23*tmp5*tmp52)/tmp182 + (16&
                    &*discbulogt*tmp11*tmp12*tmp15*tmp192*tmp23*tmp5*tmp52)/tmp182 - (16*discbulogt*t&
                    &mp11*tmp12*tmp18*tmp192*tmp23*tmp5*tmp52)/tmp182 - (64*discbu*logmume*tmp11*tmp1&
                    &2*tmp165*tmp2*tmp23*tmp5*tmp52)/tmp182 - (32*discbulogt*tmp11*tmp12*tmp192*tmp2*&
                    &tmp23*tmp5*tmp52)/tmp182 + (32*discbu*logmume*tmp11*tmp12*tmp165*tmp18*tmp2*tmp2&
                    &3*tmp5*tmp52)/(tmp182*tmp31) + (16*discbulogt*tmp11*tmp12*tmp18*tmp192*tmp2*tmp2&
                    &3*tmp5*tmp52)/(tmp182*tmp31) - (32*discbu*logmume*tmp11*tmp12*tmp15*tmp165*tmp23&
                    &*tmp31*tmp5*tmp52)/(tmp18*tmp182) - (16*discbulogt*tmp11*tmp12*tmp15*tmp192*tmp2&
                    &3*tmp31*tmp5*tmp52)/(tmp18*tmp182) - (32*discbu*logmume*tmp11*tmp12*tmp15*tmp165&
                    &*tmp23*tmp50*tmp52)/tmp182 - (16*discbulogt*tmp11*tmp12*tmp15*tmp192*tmp23*tmp50&
                    &*tmp52)/tmp182 - (32*discbu*logmume*tmp11*tmp12*tmp165*tmp23*tmp31*tmp51*tmp52)/&
                    &tmp182 - (16*discbulogt*tmp11*tmp12*tmp192*tmp23*tmp31*tmp51*tmp52)/tmp182 + (64&
                    &*discbu*logmume*mm2*tmp11*tmp12*tmp165*tmp18*tmp27*tmp53)/tmp182 + (32*discbulog&
                    &t*mm2*tmp11*tmp12*tmp18*tmp192*tmp27*tmp53)/tmp182 + (64*discbu*logmume*me2*tmp1&
                    &1*tmp12*tmp165*tmp2*tmp27*tmp53)/tmp182 + (32*discbulogt*me2*tmp11*tmp12*tmp192*&
                    &tmp2*tmp27*tmp53)/tmp182 + (32*discbu*logmume*tmp11*tmp12*tmp15*tmp165*tmp27*tmp&
                    &5*tmp53)/tmp182 - (32*discbu*logmume*tmp11*tmp12*tmp165*tmp18*tmp27*tmp5*tmp53)/&
                    &tmp182 + (16*discbulogt*tmp11*tmp12*tmp15*tmp192*tmp27*tmp5*tmp53)/tmp182 - (16*&
                    &discbulogt*tmp11*tmp12*tmp18*tmp192*tmp27*tmp5*tmp53)/tmp182 - (64*discbu*logmum&
                    &e*tmp11*tmp12*tmp165*tmp2*tmp27*tmp5*tmp53)/tmp182 - (32*discbulogt*tmp11*tmp12*&
                    &tmp192*tmp2*tmp27*tmp5*tmp53)/tmp182 + (32*discbu*logmume*tmp11*tmp12*tmp165*tmp&
                    &18*tmp2*tmp27*tmp5*tmp53)/(tmp182*tmp31) + (16*discbulogt*tmp11*tmp12*tmp18*tmp1&
                    &92*tmp2*tmp27*tmp5*tmp53)/(tmp182*tmp31) - (32*discbu*logmume*tmp11*tmp12*tmp15*&
                    &tmp165*tmp27*tmp31*tmp5*tmp53)/(tmp18*tmp182) - (16*discbulogt*tmp11*tmp12*tmp15&
                    &*tmp192*tmp27*tmp31*tmp5*tmp53)/(tmp18*tmp182) - (32*discbu*logmume*tmp11*tmp12*&
                    &tmp15*tmp165*tmp27*tmp50*tmp53)/tmp182 - (16*discbulogt*tmp11*tmp12*tmp15*tmp192&
                    &*tmp27*tmp50*tmp53)/tmp182 - (32*discbu*logmume*tmp11*tmp12*tmp165*tmp27*tmp31*t&
                    &mp51*tmp53)/tmp182 - (16*discbulogt*tmp11*tmp12*tmp192*tmp27*tmp31*tmp51*tmp53)/&
                    &tmp182 - (64*discbu*logmume*tmp11*tmp12*tmp15*tmp165*tmp23*tmp27*tmp31*tmp54)/tm&
                    &p182 - (32*discbulogt*tmp11*tmp12*tmp15*tmp192*tmp23*tmp27*tmp31*tmp54)/tmp182 +&
                    & 32*discbu*tmp15*tmp216*tmp261*tmp31*tmp33*tmp54 - 32*discbs*tmp15*tmp243*tmp31*&
                    &tmp33*tmp35*tmp37*tmp54 - (16*discbtme*mm2*tmp105*tmp12*tmp141*tmp57)/tmp18 + (3&
                    &2*discbtme*logmume*mm2*tmp105*tmp12*tmp165*tmp57)/tmp18 - 16*discbtme*mm2*tmp12*&
                    &tmp141*tmp18*tmp57 + 32*discbtme*logmume*mm2*tmp12*tmp165*tmp18*tmp57 + (16*mm2*&
                    &tmp105*tmp162*tmp207*tmp57)/tmp18 + (32*discbtme*mm2*tmp105*tmp162*tmp207*tmp57)&
                    &/tmp18 + 16*mm2*tmp162*tmp18*tmp207*tmp57 + 32*discbtme*mm2*tmp162*tmp18*tmp207*&
                    &tmp57 - (16*discbtme*mm2*tmp105*tmp12*tmp207*tmp210*tmp57)/tmp18 - 16*discbtme*m&
                    &m2*tmp12*tmp18*tmp207*tmp210*tmp57 + 16*discbtme*mm2*tmp12*tmp141*tmp31*tmp57 - &
                    &32*discbtme*logmume*mm2*tmp12*tmp165*tmp31*tmp57 - 16*mm2*tmp162*tmp207*tmp31*tm&
                    &p57 - 32*discbtme*mm2*tmp162*tmp207*tmp31*tmp57 + 16*discbtme*mm2*tmp12*tmp207*t&
                    &mp210*tmp31*tmp57 + 64*mm2*scalarc0tme*tmp157*tmp18*tmp33*tmp57 + 64*me2*scalarc&
                    &0tme*tmp157*tmp2*tmp33*tmp57 + 8*discbtme*tmp12*tmp148*tmp18*tmp5*tmp57 + 16*sca&
                    &larc0tme*tmp15*tmp157*tmp33*tmp5*tmp57 - 32*scalarc0tme*tmp157*tmp18*tmp33*tmp5*&
                    &tmp57 - 48*scalarc0tme*tmp157*tmp2*tmp33*tmp5*tmp57 + (16*scalarc0tme*tmp157*tmp&
                    &18*tmp2*tmp33*tmp5*tmp57)/tmp31 - (16*scalarc0tme*tmp15*tmp157*tmp31*tmp33*tmp5*&
                    &tmp57)/tmp18 - (32*discbtme*logmume*mm2*tmp104*tmp105*tmp12*tmp50*tmp57)/tmp18 +&
                    & (32*logmume*mm2*tmp105*tmp162*tmp165*tmp50*tmp57)/tmp18 + (64*discbtme*logmume*&
                    &mm2*tmp105*tmp162*tmp165*tmp50*tmp57)/tmp18 - 32*discbtme*logmume*mm2*tmp104*tmp&
                    &12*tmp18*tmp50*tmp57 + 32*logmume*mm2*tmp162*tmp165*tmp18*tmp50*tmp57 + 64*discb&
                    &tme*logmume*mm2*tmp162*tmp165*tmp18*tmp50*tmp57 - (32*discbtme*logmume*mm2*tmp10&
                    &5*tmp12*tmp165*tmp210*tmp50*tmp57)/tmp18 - 32*discbtme*logmume*mm2*tmp12*tmp165*&
                    &tmp18*tmp210*tmp50*tmp57 + 32*discbtme*logmume*mm2*tmp104*tmp12*tmp31*tmp50*tmp5&
                    &7 - 32*logmume*mm2*tmp162*tmp165*tmp31*tmp50*tmp57 - 64*discbtme*logmume*mm2*tmp&
                    &162*tmp165*tmp31*tmp50*tmp57 + 32*discbtme*logmume*mm2*tmp12*tmp165*tmp210*tmp31&
                    &*tmp50*tmp57 - 32*scalarc0tme*tmp15*tmp157*tmp33*tmp50*tmp57 - 16*discbtme*tmp12&
                    &*tmp141*tmp18*tmp51*tmp57 + 32*discbtme*logmume*tmp12*tmp165*tmp18*tmp51*tmp57 +&
                    & 16*tmp162*tmp18*tmp207*tmp51*tmp57 + 32*discbtme*tmp162*tmp18*tmp207*tmp51*tmp5&
                    &7 - 16*discbtme*tmp12*tmp18*tmp207*tmp210*tmp51*tmp57 + 16*discbtme*tmp12*tmp141&
                    &*tmp31*tmp51*tmp57 - 32*discbtme*logmume*tmp12*tmp165*tmp31*tmp51*tmp57 + (8*dis&
                    &cbtme*tmp12*tmp148*tmp18*tmp31*tmp51*tmp57)/tmp2 - 16*tmp162*tmp207*tmp31*tmp51*&
                    &tmp57 - 32*discbtme*tmp162*tmp207*tmp31*tmp51*tmp57 + 16*discbtme*tmp12*tmp207*t&
                    &mp210*tmp31*tmp51*tmp57 - 32*scalarc0tme*tmp157*tmp31*tmp33*tmp51*tmp57 - 32*dis&
                    &cbtme*logmume*tmp104*tmp12*tmp18*tmp50*tmp51*tmp57 + 32*logmume*tmp162*tmp165*tm&
                    &p18*tmp50*tmp51*tmp57 + 64*discbtme*logmume*tmp162*tmp165*tmp18*tmp50*tmp51*tmp5&
                    &7 - 32*discbtme*logmume*tmp12*tmp165*tmp18*tmp210*tmp50*tmp51*tmp57 + 32*discbtm&
                    &e*logmume*tmp104*tmp12*tmp31*tmp50*tmp51*tmp57 - 32*logmume*tmp162*tmp165*tmp31*&
                    &tmp50*tmp51*tmp57 - 64*discbtme*logmume*tmp162*tmp165*tmp31*tmp50*tmp51*tmp57 + &
                    &32*discbtme*logmume*tmp12*tmp165*tmp210*tmp31*tmp50*tmp51*tmp57 - 16*logbbemm*tm&
                    &p12*tmp15*tmp158*tmp161*tmp5*tmp60 + 32*logbemm*tmp12*tmp15*tmp158*tmp161*tmp5*t&
                    &mp60 - 16*scalarc0tmm*tmp103*tmp12*tmp15*tmp158*tmp161*tmp5*tmp60 + 16*logbbemm*&
                    &tmp12*tmp158*tmp161*tmp2*tmp5*tmp60 - 32*logbemm*tmp12*tmp158*tmp161*tmp2*tmp5*t&
                    &mp60 + 16*scalarc0tmm*tmp103*tmp12*tmp158*tmp161*tmp2*tmp5*tmp60 - (16*logbbemm*&
                    &tmp12*tmp158*tmp161*tmp18*tmp2*tmp5*tmp60)/tmp31 + (32*logbemm*tmp12*tmp158*tmp1&
                    &61*tmp18*tmp2*tmp5*tmp60)/tmp31 - (16*scalarc0tmm*tmp103*tmp12*tmp158*tmp161*tmp&
                    &18*tmp2*tmp5*tmp60)/tmp31 + (16*logbbemm*tmp12*tmp15*tmp158*tmp161*tmp31*tmp5*tm&
                    &p60)/tmp18 - (32*logbemm*tmp12*tmp15*tmp158*tmp161*tmp31*tmp5*tmp60)/tmp18 + (16&
                    &*scalarc0tmm*tmp103*tmp12*tmp15*tmp158*tmp161*tmp31*tmp5*tmp60)/tmp18 - 16*scala&
                    &rc0tmm*tmp15*tmp158*tmp161*tmp33*tmp5*tmp60 + 16*scalarc0tmm*tmp158*tmp161*tmp2*&
                    &tmp33*tmp5*tmp60 - (16*scalarc0tmm*tmp158*tmp161*tmp18*tmp2*tmp33*tmp5*tmp60)/tm&
                    &p31 + (16*scalarc0tmm*tmp15*tmp158*tmp161*tmp31*tmp33*tmp5*tmp60)/tmp18 - 32*sca&
                    &larc0tme*tmp15*tmp157*tmp33*tmp57*tmp60 - (32*scalarc0tme*tmp157*tmp2*tmp31*tmp3&
                    &3*tmp57*tmp60)/tmp15 + 16*scalarc0tme*tmp12*tmp15*tmp157*tmp5*tmp57*tmp60 - 16*s&
                    &calarc0tme*tmp12*tmp157*tmp2*tmp5*tmp57*tmp60 + (16*scalarc0tme*tmp12*tmp157*tmp&
                    &18*tmp2*tmp5*tmp57*tmp60)/tmp31 - (16*scalarc0tme*tmp12*tmp15*tmp157*tmp31*tmp5*&
                    &tmp57*tmp60)/tmp18 + 32*scalarc0tme*tmp15*tmp157*tmp31*tmp33*tmp54*tmp57*tmp60 +&
                    & 16*logt*tmp15*tmp33*tmp5*tmp57*tmp58*tmp60*tmp61*tmp64 - 16*logt*tmp2*tmp33*tmp&
                    &5*tmp57*tmp58*tmp60*tmp61*tmp64 + (16*logt*tmp18*tmp2*tmp33*tmp5*tmp57*tmp58*tmp&
                    &60*tmp61*tmp64)/tmp31 - (16*logt*tmp15*tmp31*tmp33*tmp5*tmp57*tmp58*tmp60*tmp61*&
                    &tmp64)/tmp18 - 16*logt*tmp15*tmp33*tmp5*tmp57*tmp58*tmp60*tmp65 + 16*logt*tmp2*t&
                    &mp33*tmp5*tmp57*tmp58*tmp60*tmp65 - (16*logt*tmp18*tmp2*tmp33*tmp5*tmp57*tmp58*t&
                    &mp60*tmp65)/tmp31 + (16*logt*tmp15*tmp31*tmp33*tmp5*tmp57*tmp58*tmp60*tmp65)/tmp&
                    &18 + 64*logt*mm2*tmp18*tmp33*tmp57*tmp58*tmp61*tmp65 + 64*logt*me2*tmp2*tmp33*tm&
                    &p57*tmp58*tmp61*tmp65 + 16*logt*tmp15*tmp33*tmp5*tmp57*tmp58*tmp61*tmp65 - 32*lo&
                    &gt*tmp18*tmp33*tmp5*tmp57*tmp58*tmp61*tmp65 - 48*logt*tmp2*tmp33*tmp5*tmp57*tmp5&
                    &8*tmp61*tmp65 + (16*logt*tmp18*tmp2*tmp33*tmp5*tmp57*tmp58*tmp61*tmp65)/tmp31 - &
                    &(16*logt*tmp15*tmp31*tmp33*tmp5*tmp57*tmp58*tmp61*tmp65)/tmp18 - 32*logt*tmp15*t&
                    &mp33*tmp50*tmp57*tmp58*tmp61*tmp65 - 32*logt*tmp31*tmp33*tmp51*tmp57*tmp58*tmp61&
                    &*tmp65 + 16*logt*tmp15*tmp33*tmp5*tmp57*tmp60*tmp61*tmp65 - 16*logt*tmp2*tmp33*t&
                    &mp5*tmp57*tmp60*tmp61*tmp65 + (16*logt*tmp18*tmp2*tmp33*tmp5*tmp57*tmp60*tmp61*t&
                    &mp65)/tmp31 - (16*logt*tmp15*tmp31*tmp33*tmp5*tmp57*tmp60*tmp61*tmp65)/tmp18 - 3&
                    &2*logt*tmp15*tmp33*tmp57*tmp58*tmp60*tmp61*tmp65 - (32*logt*tmp2*tmp31*tmp33*tmp&
                    &57*tmp58*tmp60*tmp61*tmp65)/tmp15 + 16*tmp12*tmp15*tmp5*tmp57*tmp58*tmp60*tmp61*&
                    &tmp65 + 16*logt*tmp12*tmp15*tmp5*tmp57*tmp58*tmp60*tmp61*tmp65 - 16*tmp12*tmp2*t&
                    &mp5*tmp57*tmp58*tmp60*tmp61*tmp65 - 16*logt*tmp12*tmp2*tmp5*tmp57*tmp58*tmp60*tm&
                    &p61*tmp65 + (16*tmp12*tmp18*tmp2*tmp5*tmp57*tmp58*tmp60*tmp61*tmp65)/tmp31 + (16&
                    &*logt*tmp12*tmp18*tmp2*tmp5*tmp57*tmp58*tmp60*tmp61*tmp65)/tmp31 - (16*tmp12*tmp&
                    &15*tmp31*tmp5*tmp57*tmp58*tmp60*tmp61*tmp65)/tmp18 - (16*logt*tmp12*tmp15*tmp31*&
                    &tmp5*tmp57*tmp58*tmp60*tmp61*tmp65)/tmp18 + 32*logt*tmp15*tmp31*tmp33*tmp54*tmp5&
                    &7*tmp58*tmp60*tmp61*tmp65 - (16*discbtme*mm2*tmp105*tmp12*tmp207*tmp66)/tmp18 - &
                    &16*discbtme*mm2*tmp12*tmp18*tmp207*tmp66 + 16*discbtme*mm2*tmp12*tmp207*tmp31*tm&
                    &p66 - (32*discbtme*logmume*mm2*tmp105*tmp12*tmp165*tmp50*tmp66)/tmp18 - 32*discb&
                    &tme*logmume*mm2*tmp12*tmp165*tmp18*tmp50*tmp66 + 32*discbtme*logmume*mm2*tmp12*t&
                    &mp165*tmp31*tmp50*tmp66 - 16*discbtme*tmp12*tmp18*tmp207*tmp51*tmp66 + 16*discbt&
                    &me*tmp12*tmp207*tmp31*tmp51*tmp66 - 32*discbtme*logmume*tmp12*tmp165*tmp18*tmp50&
                    &*tmp51*tmp66 + 32*discbtme*logmume*tmp12*tmp165*tmp31*tmp50*tmp51*tmp66 - 16*log&
                    &bbeme*tmp12*tmp15*tmp157*tmp5*tmp60*tmp66 + 32*logbeme*tmp12*tmp15*tmp157*tmp5*t&
                    &mp60*tmp66 - 16*scalarc0tme*tmp101*tmp12*tmp15*tmp157*tmp5*tmp60*tmp66 + 16*logb&
                    &beme*tmp12*tmp157*tmp2*tmp5*tmp60*tmp66 - 32*logbeme*tmp12*tmp157*tmp2*tmp5*tmp6&
                    &0*tmp66 + 16*scalarc0tme*tmp101*tmp12*tmp157*tmp2*tmp5*tmp60*tmp66 - (16*logbbem&
                    &e*tmp12*tmp157*tmp18*tmp2*tmp5*tmp60*tmp66)/tmp31 + (32*logbeme*tmp12*tmp157*tmp&
                    &18*tmp2*tmp5*tmp60*tmp66)/tmp31 - (16*scalarc0tme*tmp101*tmp12*tmp157*tmp18*tmp2&
                    &*tmp5*tmp60*tmp66)/tmp31 + (16*logbbeme*tmp12*tmp15*tmp157*tmp31*tmp5*tmp60*tmp6&
                    &6)/tmp18 - (32*logbeme*tmp12*tmp15*tmp157*tmp31*tmp5*tmp60*tmp66)/tmp18 + (16*sc&
                    &alarc0tme*tmp101*tmp12*tmp15*tmp157*tmp31*tmp5*tmp60*tmp66)/tmp18 - 16*scalarc0t&
                    &me*tmp15*tmp157*tmp33*tmp5*tmp60*tmp66 + 16*scalarc0tme*tmp157*tmp2*tmp33*tmp5*t&
                    &mp60*tmp66 - (16*scalarc0tme*tmp157*tmp18*tmp2*tmp33*tmp5*tmp60*tmp66)/tmp31 + (&
                    &16*scalarc0tme*tmp15*tmp157*tmp31*tmp33*tmp5*tmp60*tmp66)/tmp18 - 16*logt*tmp15*&
                    &tmp33*tmp5*tmp58*tmp60*tmp61*tmp65*tmp66 + 16*logt*tmp2*tmp33*tmp5*tmp58*tmp60*t&
                    &mp61*tmp65*tmp66 - (16*logt*tmp18*tmp2*tmp33*tmp5*tmp58*tmp60*tmp61*tmp65*tmp66)&
                    &/tmp31 + (16*logt*tmp15*tmp31*tmp33*tmp5*tmp58*tmp60*tmp61*tmp65*tmp66)/tmp18 + &
                    &16*discbu*tmp12*tmp15*tmp216*tmp261*tmp67 - 16*discbu*tmp12*tmp2*tmp216*tmp261*t&
                    &mp67 + 32*logmume*tmp11*tmp12*tmp15*tmp165*tmp23*tmp27*tmp67 - 32*discbu*logmume&
                    &*tmp11*tmp12*tmp15*tmp165*tmp23*tmp27*tmp67 + (32*discbu*logmume*tmp104*tmp11*tm&
                    &p12*tmp15*tmp23*tmp27*tmp67)/tmp182 + (16*discbulogt*tmp11*tmp111*tmp12*tmp15*tm&
                    &p23*tmp27*tmp67)/tmp182 - (32*discbu*logmume*tmp12*tmp15*tmp165*tmp23*tmp27*tmp6&
                    &7)/tmp182 - (64*discbu*logmume*tmp11*tmp15*tmp162*tmp165*tmp23*tmp27*tmp67)/tmp1&
                    &82 - 16*discbulogt*tmp11*tmp12*tmp15*tmp192*tmp23*tmp27*tmp67 + 16*logt*tmp11*tm&
                    &p12*tmp15*tmp192*tmp23*tmp27*tmp67 - (16*discbulogt*tmp12*tmp15*tmp192*tmp23*tmp&
                    &27*tmp67)/tmp182 - (16*discbu*tmp11*tmp15*tmp162*tmp192*tmp23*tmp27*tmp67)/tmp18&
                    &2 - (32*discbulogt*tmp11*tmp15*tmp162*tmp192*tmp23*tmp27*tmp67)/tmp182 + (32*log&
                    &mume*tmp11*tmp12*tmp15*tmp165*tmp18*tmp23*tmp27*tmp67)/tmp2 - (32*discbu*logmume&
                    &*tmp11*tmp12*tmp15*tmp165*tmp18*tmp23*tmp27*tmp67)/tmp2 + (16*discbulogt*tmp107*&
                    &tmp11*tmp12*tmp15*tmp18*tmp23*tmp27*tmp67)/(tmp182*tmp2) - (32*discbu*logmume*tm&
                    &p12*tmp15*tmp165*tmp18*tmp23*tmp27*tmp67)/(tmp182*tmp2) - (16*discbulogt*tmp11*t&
                    &mp12*tmp15*tmp18*tmp192*tmp23*tmp27*tmp67)/tmp2 + (16*logt*tmp11*tmp12*tmp15*tmp&
                    &18*tmp192*tmp23*tmp27*tmp67)/tmp2 - (16*discbulogt*tmp12*tmp15*tmp18*tmp192*tmp2&
                    &3*tmp27*tmp67)/(tmp182*tmp2) - 32*logmume*tmp11*tmp12*tmp165*tmp2*tmp23*tmp27*tm&
                    &p67 + 32*discbu*logmume*tmp11*tmp12*tmp165*tmp2*tmp23*tmp27*tmp67 - (32*discbu*l&
                    &ogmume*tmp104*tmp11*tmp12*tmp2*tmp23*tmp27*tmp67)/tmp182 - (16*discbulogt*tmp11*&
                    &tmp111*tmp12*tmp2*tmp23*tmp27*tmp67)/tmp182 + (32*discbu*logmume*tmp12*tmp165*tm&
                    &p2*tmp23*tmp27*tmp67)/tmp182 + (64*discbu*logmume*tmp11*tmp162*tmp165*tmp2*tmp23&
                    &*tmp27*tmp67)/tmp182 + 16*discbulogt*tmp11*tmp12*tmp192*tmp2*tmp23*tmp27*tmp67 -&
                    & 16*logt*tmp11*tmp12*tmp192*tmp2*tmp23*tmp27*tmp67 + (16*discbulogt*tmp12*tmp192&
                    &*tmp2*tmp23*tmp27*tmp67)/tmp182 + (16*discbu*tmp11*tmp162*tmp192*tmp2*tmp23*tmp2&
                    &7*tmp67)/tmp182 + (32*discbulogt*tmp11*tmp162*tmp192*tmp2*tmp23*tmp27*tmp67)/tmp&
                    &182 - (32*discbu*logmume*tmp11*tmp12*tmp15*tmp165*tmp225*tmp23*tmp27*tmp67)/tmp1&
                    &82 - (16*discbulogt*tmp11*tmp12*tmp15*tmp192*tmp225*tmp23*tmp27*tmp67)/tmp182 - &
                    &(32*discbu*logmume*tmp11*tmp12*tmp15*tmp165*tmp18*tmp225*tmp23*tmp27*tmp67)/(tmp&
                    &182*tmp2) - (16*discbulogt*tmp11*tmp12*tmp15*tmp18*tmp192*tmp225*tmp23*tmp27*tmp&
                    &67)/(tmp182*tmp2) + (32*discbu*logmume*tmp11*tmp12*tmp165*tmp2*tmp225*tmp23*tmp2&
                    &7*tmp67)/tmp182 + (16*discbulogt*tmp11*tmp12*tmp192*tmp2*tmp225*tmp23*tmp27*tmp6&
                    &7)/tmp182 - (16*discbu*tmp12*tmp15*tmp18*tmp216*tmp261*tmp67)/tmp31 - (32*logmum&
                    &e*tmp11*tmp12*tmp15*tmp165*tmp18*tmp23*tmp27*tmp67)/tmp31 + (32*discbu*logmume*t&
                    &mp11*tmp12*tmp15*tmp165*tmp18*tmp23*tmp27*tmp67)/tmp31 - (32*discbu*logmume*tmp1&
                    &04*tmp11*tmp12*tmp15*tmp18*tmp23*tmp27*tmp67)/(tmp182*tmp31) - (16*discbulogt*tm&
                    &p11*tmp111*tmp12*tmp15*tmp18*tmp23*tmp27*tmp67)/(tmp182*tmp31) + (32*discbu*logm&
                    &ume*tmp12*tmp15*tmp165*tmp18*tmp23*tmp27*tmp67)/(tmp182*tmp31) + (64*discbu*logm&
                    &ume*tmp11*tmp15*tmp162*tmp165*tmp18*tmp23*tmp27*tmp67)/(tmp182*tmp31) + (16*disc&
                    &bulogt*tmp11*tmp12*tmp15*tmp18*tmp192*tmp23*tmp27*tmp67)/tmp31 - (16*logt*tmp11*&
                    &tmp12*tmp15*tmp18*tmp192*tmp23*tmp27*tmp67)/tmp31 + (16*discbulogt*tmp12*tmp15*t&
                    &mp18*tmp192*tmp23*tmp27*tmp67)/(tmp182*tmp31) + (16*discbu*tmp11*tmp15*tmp162*tm&
                    &p18*tmp192*tmp23*tmp27*tmp67)/(tmp182*tmp31) + (32*discbulogt*tmp11*tmp15*tmp162&
                    &*tmp18*tmp192*tmp23*tmp27*tmp67)/(tmp182*tmp31) + (32*discbu*logmume*tmp11*tmp12&
                    &*tmp15*tmp165*tmp18*tmp225*tmp23*tmp27*tmp67)/(tmp182*tmp31) + (16*discbulogt*tm&
                    &p11*tmp12*tmp15*tmp18*tmp192*tmp225*tmp23*tmp27*tmp67)/(tmp182*tmp31) + (16*disc&
                    &bu*tmp12*tmp2*tmp216*tmp261*tmp31*tmp67)/tmp18 + (64*logmume*tmp11*tmp12*tmp165*&
                    &tmp2*tmp23*tmp27*tmp31*tmp67)/tmp18 - (64*discbu*logmume*tmp11*tmp12*tmp165*tmp2&
                    &*tmp23*tmp27*tmp31*tmp67)/tmp18 + (32*discbu*logmume*tmp104*tmp11*tmp12*tmp2*tmp&
                    &23*tmp27*tmp31*tmp67)/(tmp18*tmp182) + (16*discbulogt*tmp107*tmp11*tmp12*tmp2*tm&
                    &p23*tmp27*tmp31*tmp67)/(tmp18*tmp182) + (16*discbulogt*tmp11*tmp111*tmp12*tmp2*t&
                    &mp23*tmp27*tmp31*tmp67)/(tmp18*tmp182) - (64*discbu*logmume*tmp12*tmp165*tmp2*tm&
                    &p23*tmp27*tmp31*tmp67)/(tmp18*tmp182) - (64*discbu*logmume*tmp11*tmp162*tmp165*t&
                    &mp2*tmp23*tmp27*tmp31*tmp67)/(tmp18*tmp182) - (32*discbulogt*tmp11*tmp12*tmp192*&
                    &tmp2*tmp23*tmp27*tmp31*tmp67)/tmp18 + (32*logt*tmp11*tmp12*tmp192*tmp2*tmp23*tmp&
                    &27*tmp31*tmp67)/tmp18 - (32*discbulogt*tmp12*tmp192*tmp2*tmp23*tmp27*tmp31*tmp67&
                    &)/(tmp18*tmp182) - (16*discbu*tmp11*tmp162*tmp192*tmp2*tmp23*tmp27*tmp31*tmp67)/&
                    &(tmp18*tmp182) - (32*discbulogt*tmp11*tmp162*tmp192*tmp2*tmp23*tmp27*tmp31*tmp67&
                    &)/(tmp18*tmp182) - (64*discbu*logmume*tmp11*tmp12*tmp165*tmp2*tmp225*tmp23*tmp27&
                    &*tmp31*tmp67)/(tmp18*tmp182) - (32*discbulogt*tmp11*tmp12*tmp192*tmp2*tmp225*tmp&
                    &23*tmp27*tmp31*tmp67)/(tmp18*tmp182) - 16*scalarc0ir6u*tmp11*tmp15*tmp33*tmp67 -&
                    & (32*scalarc0ir6u*tmp11*tmp15*tmp18*tmp33*tmp67)/tmp2 + 16*scalarc0ir6u*tmp11*tm&
                    &p2*tmp33*tmp67 - 16*discbu*tmp15*tmp216*tmp222*tmp33*tmp67 + 16*discbu*tmp2*tmp2&
                    &16*tmp222*tmp33*tmp67 - (16*discbu*tmp15*tmp18*tmp216*tmp232*tmp33*tmp67)/tmp2 -&
                    & 16*tmp15*tmp182*tmp216*tmp261*tmp33*tmp67 - (16*tmp15*tmp18*tmp182*tmp216*tmp26&
                    &1*tmp33*tmp67)/tmp2 + 16*tmp182*tmp2*tmp216*tmp261*tmp33*tmp67 + 16*discbu*tmp15&
                    &*tmp216*tmp225*tmp261*tmp33*tmp67 + (16*discbu*tmp15*tmp18*tmp216*tmp225*tmp261*&
                    &tmp33*tmp67)/tmp2 - 16*discbu*tmp2*tmp216*tmp225*tmp261*tmp33*tmp67 + 16*discbu*&
                    &tmp15*tmp246*tmp247*tmp261*tmp33*tmp67 + (16*discbu*tmp15*tmp18*tmp246*tmp247*tm&
                    &p261*tmp33*tmp67)/tmp2 - 16*discbu*tmp2*tmp246*tmp247*tmp261*tmp33*tmp67 - (8*di&
                    &scbu*logmm*tmp11*tmp15*tmp23*tmp27*tmp33*tmp67)/tmp182 - (16*discbu*logmm*tmp11*&
                    &tmp15*tmp18*tmp23*tmp27*tmp33*tmp67)/(tmp182*tmp2) + (8*discbu*logmm*tmp11*tmp2*&
                    &tmp23*tmp27*tmp33*tmp67)/tmp182 + (16*scalarc0ir6u*tmp11*tmp15*tmp18*tmp33*tmp67&
                    &)/tmp31 + (16*discbu*tmp15*tmp18*tmp216*tmp222*tmp33*tmp67)/tmp31 + (16*tmp15*tm&
                    &p18*tmp182*tmp216*tmp261*tmp33*tmp67)/tmp31 - (16*discbu*tmp15*tmp18*tmp216*tmp2&
                    &25*tmp261*tmp33*tmp67)/tmp31 - (16*discbu*tmp15*tmp18*tmp246*tmp247*tmp261*tmp33&
                    &*tmp67)/tmp31 + (8*discbu*logmm*tmp11*tmp15*tmp18*tmp23*tmp27*tmp33*tmp67)/(tmp1&
                    &82*tmp31) - (48*scalarc0ir6u*tmp11*tmp2*tmp31*tmp33*tmp67)/tmp18 - (16*discbu*tm&
                    &p2*tmp216*tmp222*tmp31*tmp33*tmp67)/tmp18 - (16*discbu*tmp2*tmp216*tmp232*tmp31*&
                    &tmp33*tmp67)/tmp18 - (32*tmp182*tmp2*tmp216*tmp261*tmp31*tmp33*tmp67)/tmp18 + (3&
                    &2*discbu*tmp2*tmp216*tmp225*tmp261*tmp31*tmp33*tmp67)/tmp18 + (32*discbu*tmp2*tm&
                    &p246*tmp247*tmp261*tmp31*tmp33*tmp67)/tmp18 - (24*discbu*logmm*tmp11*tmp2*tmp23*&
                    &tmp27*tmp31*tmp33*tmp67)/(tmp18*tmp182) + 16*scalarc0ir6s*tmp15*tmp32*tmp33*tmp6&
                    &7 + (32*scalarc0ir6s*tmp15*tmp18*tmp32*tmp33*tmp67)/tmp2 - 16*scalarc0ir6s*tmp2*&
                    &tmp32*tmp33*tmp67 - (16*scalarc0ir6s*tmp15*tmp18*tmp32*tmp33*tmp67)/tmp31 + (48*&
                    &scalarc0ir6s*tmp2*tmp31*tmp32*tmp33*tmp67)/tmp18 + (16*discbs*tmp15*tmp167*tmp18&
                    &*tmp243*tmp33*tmp35*tmp67)/tmp2 + (16*discbs*tmp167*tmp2*tmp243*tmp31*tmp33*tmp3&
                    &5*tmp67)/tmp18 + (16*discbs*tmp15*tmp168*tmp18*tmp243*tmp33*tmp37*tmp67)/tmp2 + &
                    &(16*discbs*tmp168*tmp2*tmp243*tmp31*tmp33*tmp37*tmp67)/tmp18 - 16*discbs*tmp12*t&
                    &mp15*tmp243*tmp35*tmp37*tmp67 + 16*discbs*tmp12*tmp2*tmp243*tmp35*tmp37*tmp67 + &
                    &(16*discbs*tmp12*tmp15*tmp18*tmp243*tmp35*tmp37*tmp67)/tmp31 - (16*discbs*tmp12*&
                    &tmp2*tmp243*tmp31*tmp35*tmp37*tmp67)/tmp18 - (32*logmume*tmp12*tmp15*tmp165*tmp1&
                    &8*tmp32*tmp35*tmp37*tmp67)/tmp2 + (32*discbs*logmume*tmp12*tmp15*tmp165*tmp18*tm&
                    &p32*tmp35*tmp37*tmp67)/tmp2 + (16*discbslogt*tmp12*tmp15*tmp174*tmp18*tmp32*tmp3&
                    &5*tmp37*tmp67)/tmp2 - (16*logt*tmp12*tmp15*tmp174*tmp18*tmp32*tmp35*tmp37*tmp67)&
                    &/tmp2 - (32*logmume*tmp12*tmp165*tmp2*tmp31*tmp32*tmp35*tmp37*tmp67)/tmp18 + (32&
                    &*discbs*logmume*tmp12*tmp165*tmp2*tmp31*tmp32*tmp35*tmp37*tmp67)/tmp18 + (16*dis&
                    &cbslogt*tmp12*tmp174*tmp2*tmp31*tmp32*tmp35*tmp37*tmp67)/tmp18 - (16*logt*tmp12*&
                    &tmp174*tmp2*tmp31*tmp32*tmp35*tmp37*tmp67)/tmp18 + (16*discbs*tmp118*tmp15*tmp18&
                    &*tmp33*tmp35*tmp37*tmp67)/tmp2 + (16*discbs*tmp118*tmp2*tmp31*tmp33*tmp35*tmp37*&
                    &tmp67)/tmp18 + 16*discbs*tmp15*tmp33*tmp35*tmp37*tmp43*tmp67 - 16*discbs*tmp2*tm&
                    &p33*tmp35*tmp37*tmp43*tmp67 - (16*discbs*tmp15*tmp18*tmp33*tmp35*tmp37*tmp43*tmp&
                    &67)/tmp31 + (16*discbs*tmp2*tmp31*tmp33*tmp35*tmp37*tmp43*tmp67)/tmp18 + (32*dis&
                    &cbu*logmume*tmp11*tmp12*tmp15*tmp165*tmp23*tmp52*tmp67)/tmp182 + (16*discbulogt*&
                    &tmp11*tmp12*tmp15*tmp192*tmp23*tmp52*tmp67)/tmp182 + (32*discbu*logmume*tmp11*tm&
                    &p12*tmp15*tmp165*tmp18*tmp23*tmp52*tmp67)/(tmp182*tmp2) + (16*discbulogt*tmp11*t&
                    &mp12*tmp15*tmp18*tmp192*tmp23*tmp52*tmp67)/(tmp182*tmp2) - (32*discbu*logmume*tm&
                    &p11*tmp12*tmp165*tmp2*tmp23*tmp52*tmp67)/tmp182 - (16*discbulogt*tmp11*tmp12*tmp&
                    &192*tmp2*tmp23*tmp52*tmp67)/tmp182 - (32*discbu*logmume*tmp11*tmp12*tmp15*tmp165&
                    &*tmp18*tmp23*tmp52*tmp67)/(tmp182*tmp31) - (16*discbulogt*tmp11*tmp12*tmp15*tmp1&
                    &8*tmp192*tmp23*tmp52*tmp67)/(tmp182*tmp31) + (64*discbu*logmume*tmp11*tmp12*tmp1&
                    &65*tmp2*tmp23*tmp31*tmp52*tmp67)/(tmp18*tmp182) + (32*discbulogt*tmp11*tmp12*tmp&
                    &192*tmp2*tmp23*tmp31*tmp52*tmp67)/(tmp18*tmp182) + (32*discbu*logmume*tmp11*tmp1&
                    &2*tmp15*tmp165*tmp27*tmp53*tmp67)/tmp182 + (16*discbulogt*tmp11*tmp12*tmp15*tmp1&
                    &92*tmp27*tmp53*tmp67)/tmp182 + (32*discbu*logmume*tmp11*tmp12*tmp15*tmp165*tmp18&
                    &*tmp27*tmp53*tmp67)/(tmp182*tmp2) + (16*discbulogt*tmp11*tmp12*tmp15*tmp18*tmp19&
                    &2*tmp27*tmp53*tmp67)/(tmp182*tmp2) - (32*discbu*logmume*tmp11*tmp12*tmp165*tmp2*&
                    &tmp27*tmp53*tmp67)/tmp182 - (16*discbulogt*tmp11*tmp12*tmp192*tmp2*tmp27*tmp53*t&
                    &mp67)/tmp182 - (32*discbu*logmume*tmp11*tmp12*tmp15*tmp165*tmp18*tmp27*tmp53*tmp&
                    &67)/(tmp182*tmp31) - (16*discbulogt*tmp11*tmp12*tmp15*tmp18*tmp192*tmp27*tmp53*t&
                    &mp67)/(tmp182*tmp31) + (64*discbu*logmume*tmp11*tmp12*tmp165*tmp2*tmp27*tmp31*tm&
                    &p53*tmp67)/(tmp18*tmp182) + (32*discbulogt*tmp11*tmp12*tmp192*tmp2*tmp27*tmp31*t&
                    &mp53*tmp67)/(tmp18*tmp182) - 8*discbtme*tmp12*tmp148*tmp31*tmp57*tmp67 + 16*scal&
                    &arc0tme*tmp15*tmp157*tmp33*tmp57*tmp67 + (32*scalarc0tme*tmp15*tmp157*tmp18*tmp3&
                    &3*tmp57*tmp67)/tmp2 - 16*scalarc0tme*tmp157*tmp2*tmp33*tmp57*tmp67 - (16*scalarc&
                    &0tme*tmp15*tmp157*tmp18*tmp33*tmp57*tmp67)/tmp31 + (48*scalarc0tme*tmp157*tmp2*t&
                    &mp31*tmp33*tmp57*tmp67)/tmp18 - 16*logbbemm*tmp12*tmp15*tmp158*tmp161*tmp60*tmp6&
                    &7 + 32*logbemm*tmp12*tmp15*tmp158*tmp161*tmp60*tmp67 - 16*scalarc0tmm*tmp103*tmp&
                    &12*tmp15*tmp158*tmp161*tmp60*tmp67 + 16*logbbemm*tmp12*tmp158*tmp161*tmp2*tmp60*&
                    &tmp67 - 32*logbemm*tmp12*tmp158*tmp161*tmp2*tmp60*tmp67 + 16*scalarc0tmm*tmp103*&
                    &tmp12*tmp158*tmp161*tmp2*tmp60*tmp67 + (16*logbbemm*tmp12*tmp15*tmp158*tmp161*tm&
                    &p18*tmp60*tmp67)/tmp31 - (32*logbemm*tmp12*tmp15*tmp158*tmp161*tmp18*tmp60*tmp67&
                    &)/tmp31 + (16*scalarc0tmm*tmp103*tmp12*tmp15*tmp158*tmp161*tmp18*tmp60*tmp67)/tm&
                    &p31 - (16*logbbemm*tmp12*tmp158*tmp161*tmp2*tmp31*tmp60*tmp67)/tmp18 + (32*logbe&
                    &mm*tmp12*tmp158*tmp161*tmp2*tmp31*tmp60*tmp67)/tmp18 - (16*scalarc0tmm*tmp103*tm&
                    &p12*tmp158*tmp161*tmp2*tmp31*tmp60*tmp67)/tmp18 - 16*scalarc0tmm*tmp15*tmp158*tm&
                    &p161*tmp33*tmp60*tmp67 + 16*scalarc0tmm*tmp158*tmp161*tmp2*tmp33*tmp60*tmp67 + (&
                    &16*scalarc0tmm*tmp15*tmp158*tmp161*tmp18*tmp33*tmp60*tmp67)/tmp31 - (16*scalarc0&
                    &tmm*tmp158*tmp161*tmp2*tmp31*tmp33*tmp60*tmp67)/tmp18 + 16*scalarc0tme*tmp12*tmp&
                    &15*tmp157*tmp57*tmp60*tmp67 - 16*scalarc0tme*tmp12*tmp157*tmp2*tmp57*tmp60*tmp67&
                    & - (16*scalarc0tme*tmp12*tmp15*tmp157*tmp18*tmp57*tmp60*tmp67)/tmp31 + (16*scala&
                    &rc0tme*tmp12*tmp157*tmp2*tmp31*tmp57*tmp60*tmp67)/tmp18 + 16*logt*tmp15*tmp33*tm&
                    &p57*tmp58*tmp60*tmp61*tmp64*tmp67 - 16*logt*tmp2*tmp33*tmp57*tmp58*tmp60*tmp61*t&
                    &mp64*tmp67 - (16*logt*tmp15*tmp18*tmp33*tmp57*tmp58*tmp60*tmp61*tmp64*tmp67)/tmp&
                    &31 + (16*logt*tmp2*tmp31*tmp33*tmp57*tmp58*tmp60*tmp61*tmp64*tmp67)/tmp18 - 16*l&
                    &ogt*tmp15*tmp33*tmp57*tmp58*tmp60*tmp65*tmp67 + 16*logt*tmp2*tmp33*tmp57*tmp58*t&
                    &mp60*tmp65*tmp67 + (16*logt*tmp15*tmp18*tmp33*tmp57*tmp58*tmp60*tmp65*tmp67)/tmp&
                    &31 - (16*logt*tmp2*tmp31*tmp33*tmp57*tmp58*tmp60*tmp65*tmp67)/tmp18 + 16*logt*tm&
                    &p15*tmp33*tmp57*tmp58*tmp61*tmp65*tmp67 + (32*logt*tmp15*tmp18*tmp33*tmp57*tmp58&
                    &*tmp61*tmp65*tmp67)/tmp2 - 16*logt*tmp2*tmp33*tmp57*tmp58*tmp61*tmp65*tmp67 - (1&
                    &6*logt*tmp15*tmp18*tmp33*tmp57*tmp58*tmp61*tmp65*tmp67)/tmp31 + (48*logt*tmp2*tm&
                    &p31*tmp33*tmp57*tmp58*tmp61*tmp65*tmp67)/tmp18 + 16*logt*tmp15*tmp33*tmp57*tmp60&
                    &*tmp61*tmp65*tmp67 - 16*logt*tmp2*tmp33*tmp57*tmp60*tmp61*tmp65*tmp67 - (16*logt&
                    &*tmp15*tmp18*tmp33*tmp57*tmp60*tmp61*tmp65*tmp67)/tmp31 + (16*logt*tmp2*tmp31*tm&
                    &p33*tmp57*tmp60*tmp61*tmp65*tmp67)/tmp18 + 16*tmp12*tmp15*tmp57*tmp58*tmp60*tmp6&
                    &1*tmp65*tmp67 + 16*logt*tmp12*tmp15*tmp57*tmp58*tmp60*tmp61*tmp65*tmp67 - 16*tmp&
                    &12*tmp2*tmp57*tmp58*tmp60*tmp61*tmp65*tmp67 - 16*logt*tmp12*tmp2*tmp57*tmp58*tmp&
                    &60*tmp61*tmp65*tmp67 - (16*tmp12*tmp15*tmp18*tmp57*tmp58*tmp60*tmp61*tmp65*tmp67&
                    &)/tmp31 - (16*logt*tmp12*tmp15*tmp18*tmp57*tmp58*tmp60*tmp61*tmp65*tmp67)/tmp31 &
                    &+ (16*tmp12*tmp2*tmp31*tmp57*tmp58*tmp60*tmp61*tmp65*tmp67)/tmp18 + (16*logt*tmp&
                    &12*tmp2*tmp31*tmp57*tmp58*tmp60*tmp61*tmp65*tmp67)/tmp18 - 16*logbbeme*tmp12*tmp&
                    &15*tmp157*tmp60*tmp66*tmp67 + 32*logbeme*tmp12*tmp15*tmp157*tmp60*tmp66*tmp67 - &
                    &16*scalarc0tme*tmp101*tmp12*tmp15*tmp157*tmp60*tmp66*tmp67 + 16*logbbeme*tmp12*t&
                    &mp157*tmp2*tmp60*tmp66*tmp67 - 32*logbeme*tmp12*tmp157*tmp2*tmp60*tmp66*tmp67 + &
                    &16*scalarc0tme*tmp101*tmp12*tmp157*tmp2*tmp60*tmp66*tmp67 + (16*logbbeme*tmp12*t&
                    &mp15*tmp157*tmp18*tmp60*tmp66*tmp67)/tmp31 - (32*logbeme*tmp12*tmp15*tmp157*tmp1&
                    &8*tmp60*tmp66*tmp67)/tmp31 + (16*scalarc0tme*tmp101*tmp12*tmp15*tmp157*tmp18*tmp&
                    &60*tmp66*tmp67)/tmp31 - (16*logbbeme*tmp12*tmp157*tmp2*tmp31*tmp60*tmp66*tmp67)/&
                    &tmp18 + (32*logbeme*tmp12*tmp157*tmp2*tmp31*tmp60*tmp66*tmp67)/tmp18 - (16*scala&
                    &rc0tme*tmp101*tmp12*tmp157*tmp2*tmp31*tmp60*tmp66*tmp67)/tmp18 - 16*scalarc0tme*&
                    &tmp15*tmp157*tmp33*tmp60*tmp66*tmp67 + 16*scalarc0tme*tmp157*tmp2*tmp33*tmp60*tm&
                    &p66*tmp67 + (16*scalarc0tme*tmp15*tmp157*tmp18*tmp33*tmp60*tmp66*tmp67)/tmp31 - &
                    &(16*scalarc0tme*tmp157*tmp2*tmp31*tmp33*tmp60*tmp66*tmp67)/tmp18 - 16*logt*tmp15&
                    &*tmp33*tmp58*tmp60*tmp61*tmp65*tmp66*tmp67 + 16*logt*tmp2*tmp33*tmp58*tmp60*tmp6&
                    &1*tmp65*tmp66*tmp67 + (16*logt*tmp15*tmp18*tmp33*tmp58*tmp60*tmp61*tmp65*tmp66*t&
                    &mp67)/tmp31 - (16*logt*tmp2*tmp31*tmp33*tmp58*tmp60*tmp61*tmp65*tmp66*tmp67)/tmp&
                    &18 - 32*mm2*scalarc0ir6s*tmp18*tmp33*tmp69 - 32*me2*scalarc0ir6s*tmp2*tmp33*tmp6&
                    &9 + 32*scalarc0ir6s*tmp15*tmp32*tmp33*tmp69 + (32*scalarc0ir6s*tmp2*tmp31*tmp32*&
                    &tmp33*tmp69)/tmp15 - 16*logmm*mm2*tmp18*tmp32*tmp33*tmp49*tmp69 + 16*discbs*logm&
                    &m*mm2*tmp18*tmp32*tmp33*tmp49*tmp69 - 16*logmm*me2*tmp2*tmp32*tmp33*tmp49*tmp69 &
                    &+ 16*discbs*logmm*me2*tmp2*tmp32*tmp33*tmp49*tmp69 - 16*scalarc0ir6s*tmp12*tmp15&
                    &*tmp32*tmp5*tmp69 + 16*scalarc0ir6s*tmp12*tmp2*tmp32*tmp5*tmp69 - (16*scalarc0ir&
                    &6s*tmp12*tmp18*tmp2*tmp32*tmp5*tmp69)/tmp31 + (16*scalarc0ir6s*tmp12*tmp15*tmp31&
                    &*tmp32*tmp5*tmp69)/tmp18 + 16*scalarc0ir6s*tmp18*tmp33*tmp5*tmp69 + 16*scalarc0i&
                    &r6s*tmp2*tmp33*tmp5*tmp69 + 8*logmm*tmp18*tmp32*tmp33*tmp49*tmp5*tmp69 + 8*logmm&
                    &*tmp2*tmp32*tmp33*tmp49*tmp5*tmp69 + 16*scalarc0ir6s*tmp15*tmp33*tmp50*tmp69 + 8&
                    &*logmm*tmp15*tmp32*tmp33*tmp49*tmp50*tmp69 + 16*scalarc0ir6s*tmp31*tmp33*tmp51*t&
                    &mp69 + 8*logmm*tmp31*tmp32*tmp33*tmp49*tmp51*tmp69 - 32*scalarc0ir6s*tmp15*tmp31&
                    &*tmp32*tmp33*tmp54*tmp69 - 16*scalarc0ir6s*tmp12*tmp15*tmp32*tmp67*tmp69 + 16*sc&
                    &alarc0ir6s*tmp12*tmp2*tmp32*tmp67*tmp69 + (16*scalarc0ir6s*tmp12*tmp15*tmp18*tmp&
                    &32*tmp67*tmp69)/tmp31 - (16*scalarc0ir6s*tmp12*tmp2*tmp31*tmp32*tmp67*tmp69)/tmp&
                    &18 - (16*scalarc0ir6s*tmp15*tmp18*tmp33*tmp67*tmp69)/tmp2 - (16*scalarc0ir6s*tmp&
                    &2*tmp31*tmp33*tmp67*tmp69)/tmp18 + (8*discbs*logmm*tmp15*tmp18*tmp32*tmp33*tmp49&
                    &*tmp67*tmp69)/tmp2 + (8*discbs*logmm*tmp2*tmp31*tmp32*tmp33*tmp49*tmp67*tmp69)/t&
                    &mp18 + 32*discbs*mm2*tmp18*tmp32*tmp33*tmp69*tmp72*tmp76 + 32*discbs*me2*tmp2*tm&
                    &p32*tmp33*tmp69*tmp72*tmp76 - 16*discbs*tmp18*tmp32*tmp33*tmp5*tmp69*tmp72*tmp76&
                    & - 16*discbs*tmp2*tmp32*tmp33*tmp5*tmp69*tmp72*tmp76 - 16*discbs*tmp15*tmp32*tmp&
                    &33*tmp50*tmp69*tmp72*tmp76 - 16*discbs*tmp31*tmp32*tmp33*tmp51*tmp69*tmp72*tmp76&
                    & + (16*discbs*tmp15*tmp18*tmp32*tmp33*tmp67*tmp69*tmp72*tmp76)/tmp2 + (16*discbs&
                    &*tmp2*tmp31*tmp32*tmp33*tmp67*tmp69*tmp72*tmp76)/tmp18 + 32*mm2*scalarc0ir6s*tmp&
                    &18*tmp33*tmp69*tmp77*tmp78 + 32*me2*scalarc0ir6s*tmp2*tmp33*tmp69*tmp77*tmp78 - &
                    &16*scalarc0ir6s*tmp15*tmp33*tmp50*tmp69*tmp77*tmp78 - 16*scalarc0ir6s*tmp31*tmp3&
                    &3*tmp51*tmp69*tmp77*tmp78 + (16*scalarc0ir6s*tmp15*tmp18*tmp33*tmp67*tmp69*tmp77&
                    &*tmp78)/tmp2 + (16*scalarc0ir6s*tmp2*tmp31*tmp33*tmp67*tmp69*tmp77*tmp78)/tmp18 &
                    &+ (64*discbs*logmume*mm2*tmp12*tmp165*tmp167*tmp18*tmp32*tmp35)/tmp80 + (32*disc&
                    &bslogt*mm2*tmp12*tmp167*tmp174*tmp18*tmp32*tmp35)/tmp80 + (64*discbs*logmume*me2&
                    &*tmp12*tmp165*tmp167*tmp2*tmp32*tmp35)/tmp80 + (32*discbslogt*me2*tmp12*tmp167*t&
                    &mp174*tmp2*tmp32*tmp35)/tmp80 + (64*discbs*logmume*mm2*tmp12*tmp165*tmp168*tmp18&
                    &*tmp32*tmp37)/tmp80 + (32*discbslogt*mm2*tmp12*tmp168*tmp174*tmp18*tmp32*tmp37)/&
                    &tmp80 + (64*discbs*logmume*me2*tmp12*tmp165*tmp168*tmp2*tmp32*tmp37)/tmp80 + (32&
                    &*discbslogt*me2*tmp12*tmp168*tmp174*tmp2*tmp32*tmp37)/tmp80 - (64*discbs*logmume&
                    &*mm2*tmp12*tmp165*tmp18*tmp35*tmp37)/tmp80 - (32*discbslogt*mm2*tmp12*tmp174*tmp&
                    &18*tmp35*tmp37)/tmp80 - (64*discbs*logmume*me2*tmp12*tmp165*tmp2*tmp35*tmp37)/tm&
                    &p80 - (32*discbslogt*me2*tmp12*tmp174*tmp2*tmp35*tmp37)/tmp80 + (64*discbs*logmu&
                    &me*tmp12*tmp15*tmp165*tmp32*tmp35*tmp37)/tmp80 + (32*discbslogt*tmp12*tmp15*tmp1&
                    &74*tmp32*tmp35*tmp37)/tmp80 + (64*discbs*logmume*tmp12*tmp165*tmp2*tmp31*tmp32*t&
                    &mp35*tmp37)/(tmp15*tmp80) + (32*discbslogt*tmp12*tmp174*tmp2*tmp31*tmp32*tmp35*t&
                    &mp37)/(tmp15*tmp80) + (32*discbs*logmm*mm2*tmp18*tmp32*tmp33*tmp49)/tmp80 + (32*&
                    &discbs*logmm*me2*tmp2*tmp32*tmp33*tmp49)/tmp80 - (32*discbs*logmume*tmp12*tmp165&
                    &*tmp167*tmp18*tmp32*tmp35*tmp5)/tmp80 - (16*discbslogt*tmp12*tmp167*tmp174*tmp18&
                    &*tmp32*tmp35*tmp5)/tmp80 - (32*discbs*logmume*tmp12*tmp165*tmp167*tmp2*tmp32*tmp&
                    &35*tmp5)/tmp80 - (16*discbslogt*tmp12*tmp167*tmp174*tmp2*tmp32*tmp35*tmp5)/tmp80&
                    & - (32*discbs*logmume*tmp12*tmp165*tmp168*tmp18*tmp32*tmp37*tmp5)/tmp80 - (16*di&
                    &scbslogt*tmp12*tmp168*tmp174*tmp18*tmp32*tmp37*tmp5)/tmp80 - (32*discbs*logmume*&
                    &tmp12*tmp165*tmp168*tmp2*tmp32*tmp37*tmp5)/tmp80 - (16*discbslogt*tmp12*tmp168*t&
                    &mp174*tmp2*tmp32*tmp37*tmp5)/tmp80 + (32*discbs*logmume*tmp12*tmp165*tmp18*tmp35&
                    &*tmp37*tmp5)/tmp80 + (16*discbslogt*tmp12*tmp174*tmp18*tmp35*tmp37*tmp5)/tmp80 +&
                    & (32*discbs*logmume*tmp12*tmp165*tmp2*tmp35*tmp37*tmp5)/tmp80 + (16*discbslogt*t&
                    &mp12*tmp174*tmp2*tmp35*tmp37*tmp5)/tmp80 + (16*discbslogt*tmp104*tmp12*tmp15*tmp&
                    &32*tmp35*tmp37*tmp5)/tmp80 + (32*discbs*logmume*tmp104*tmp12*tmp15*tmp32*tmp35*t&
                    &mp37*tmp5)/tmp80 - (64*discbs*logmume*tmp15*tmp162*tmp165*tmp32*tmp35*tmp37*tmp5&
                    &)/tmp80 - (16*discbs*tmp15*tmp162*tmp174*tmp32*tmp35*tmp37*tmp5)/tmp80 - (32*dis&
                    &cbslogt*tmp15*tmp162*tmp174*tmp32*tmp35*tmp37*tmp5)/tmp80 - (16*discbslogt*tmp10&
                    &4*tmp12*tmp2*tmp32*tmp35*tmp37*tmp5)/tmp80 - (32*discbs*logmume*tmp104*tmp12*tmp&
                    &2*tmp32*tmp35*tmp37*tmp5)/tmp80 + (64*discbs*logmume*tmp162*tmp165*tmp2*tmp32*tm&
                    &p35*tmp37*tmp5)/tmp80 + (16*discbs*tmp162*tmp174*tmp2*tmp32*tmp35*tmp37*tmp5)/tm&
                    &p80 + (32*discbslogt*tmp162*tmp174*tmp2*tmp32*tmp35*tmp37*tmp5)/tmp80 + (16*disc&
                    &bslogt*tmp104*tmp12*tmp18*tmp2*tmp32*tmp35*tmp37*tmp5)/(tmp31*tmp80) + (32*discb&
                    &s*logmume*tmp104*tmp12*tmp18*tmp2*tmp32*tmp35*tmp37*tmp5)/(tmp31*tmp80) - (64*di&
                    &scbs*logmume*tmp162*tmp165*tmp18*tmp2*tmp32*tmp35*tmp37*tmp5)/(tmp31*tmp80) - (1&
                    &6*discbs*tmp162*tmp174*tmp18*tmp2*tmp32*tmp35*tmp37*tmp5)/(tmp31*tmp80) - (32*di&
                    &scbslogt*tmp162*tmp174*tmp18*tmp2*tmp32*tmp35*tmp37*tmp5)/(tmp31*tmp80) - (16*di&
                    &scbslogt*tmp104*tmp12*tmp15*tmp31*tmp32*tmp35*tmp37*tmp5)/(tmp18*tmp80) - (32*di&
                    &scbs*logmume*tmp104*tmp12*tmp15*tmp31*tmp32*tmp35*tmp37*tmp5)/(tmp18*tmp80) + (6&
                    &4*discbs*logmume*tmp15*tmp162*tmp165*tmp31*tmp32*tmp35*tmp37*tmp5)/(tmp18*tmp80)&
                    & + (16*discbs*tmp15*tmp162*tmp174*tmp31*tmp32*tmp35*tmp37*tmp5)/(tmp18*tmp80) + &
                    &(32*discbslogt*tmp15*tmp162*tmp174*tmp31*tmp32*tmp35*tmp37*tmp5)/(tmp18*tmp80) +&
                    & (8*discbs*logmm*tmp15*tmp32*tmp33*tmp49*tmp5)/tmp80 - (16*discbs*logmm*tmp18*tm&
                    &p32*tmp33*tmp49*tmp5)/tmp80 - (24*discbs*logmm*tmp2*tmp32*tmp33*tmp49*tmp5)/tmp8&
                    &0 + (8*discbs*logmm*tmp18*tmp2*tmp32*tmp33*tmp49*tmp5)/(tmp31*tmp80) - (32*discb&
                    &s*logmume*tmp12*tmp15*tmp165*tmp167*tmp32*tmp35*tmp50)/tmp80 - (16*discbslogt*tm&
                    &p12*tmp15*tmp167*tmp174*tmp32*tmp35*tmp50)/tmp80 - (32*discbs*logmume*tmp12*tmp1&
                    &5*tmp165*tmp168*tmp32*tmp37*tmp50)/tmp80 - (16*discbslogt*tmp12*tmp15*tmp168*tmp&
                    &174*tmp32*tmp37*tmp50)/tmp80 + (32*discbs*logmume*tmp12*tmp15*tmp165*tmp35*tmp37&
                    &*tmp50)/tmp80 + (16*discbslogt*tmp12*tmp15*tmp174*tmp35*tmp37*tmp50)/tmp80 - (16&
                    &*discbs*logmm*tmp15*tmp32*tmp33*tmp49*tmp50)/tmp80 - (32*discbs*logmume*tmp12*tm&
                    &p165*tmp167*tmp31*tmp32*tmp35*tmp51)/tmp80 - (16*discbslogt*tmp12*tmp167*tmp174*&
                    &tmp31*tmp32*tmp35*tmp51)/tmp80 - (32*discbs*logmume*tmp12*tmp165*tmp168*tmp31*tm&
                    &p32*tmp37*tmp51)/tmp80 - (16*discbslogt*tmp12*tmp168*tmp174*tmp31*tmp32*tmp37*tm&
                    &p51)/tmp80 + (32*discbs*logmume*tmp12*tmp165*tmp31*tmp35*tmp37*tmp51)/tmp80 + (1&
                    &6*discbslogt*tmp12*tmp174*tmp31*tmp35*tmp37*tmp51)/tmp80 - (16*discbs*logmm*tmp3&
                    &1*tmp32*tmp33*tmp49*tmp51)/tmp80 - (64*discbs*logmume*tmp12*tmp15*tmp165*tmp31*t&
                    &mp32*tmp35*tmp37*tmp54)/tmp80 - (32*discbslogt*tmp12*tmp15*tmp174*tmp31*tmp32*tm&
                    &p35*tmp37*tmp54)/tmp80 + (32*discbs*logmume*tmp12*tmp15*tmp165*tmp167*tmp18*tmp3&
                    &2*tmp35*tmp67)/(tmp2*tmp80) + (16*discbslogt*tmp12*tmp15*tmp167*tmp174*tmp18*tmp&
                    &32*tmp35*tmp67)/(tmp2*tmp80) + (32*discbs*logmume*tmp12*tmp165*tmp167*tmp2*tmp31&
                    &*tmp32*tmp35*tmp67)/(tmp18*tmp80) + (16*discbslogt*tmp12*tmp167*tmp174*tmp2*tmp3&
                    &1*tmp32*tmp35*tmp67)/(tmp18*tmp80) + (32*discbs*logmume*tmp12*tmp15*tmp165*tmp16&
                    &8*tmp18*tmp32*tmp37*tmp67)/(tmp2*tmp80) + (16*discbslogt*tmp12*tmp15*tmp168*tmp1&
                    &74*tmp18*tmp32*tmp37*tmp67)/(tmp2*tmp80) + (32*discbs*logmume*tmp12*tmp165*tmp16&
                    &8*tmp2*tmp31*tmp32*tmp37*tmp67)/(tmp18*tmp80) + (16*discbslogt*tmp12*tmp168*tmp1&
                    &74*tmp2*tmp31*tmp32*tmp37*tmp67)/(tmp18*tmp80) - (32*discbs*logmume*tmp12*tmp15*&
                    &tmp165*tmp18*tmp35*tmp37*tmp67)/(tmp2*tmp80) - (16*discbslogt*tmp12*tmp15*tmp174&
                    &*tmp18*tmp35*tmp37*tmp67)/(tmp2*tmp80) - (32*discbs*logmume*tmp12*tmp165*tmp2*tm&
                    &p31*tmp35*tmp37*tmp67)/(tmp18*tmp80) - (16*discbslogt*tmp12*tmp174*tmp2*tmp31*tm&
                    &p35*tmp37*tmp67)/(tmp18*tmp80) + (16*discbslogt*tmp104*tmp12*tmp15*tmp32*tmp35*t&
                    &mp37*tmp67)/tmp80 + (32*discbs*logmume*tmp104*tmp12*tmp15*tmp32*tmp35*tmp37*tmp6&
                    &7)/tmp80 - (64*discbs*logmume*tmp15*tmp162*tmp165*tmp32*tmp35*tmp37*tmp67)/tmp80&
                    & - (16*discbs*tmp15*tmp162*tmp174*tmp32*tmp35*tmp37*tmp67)/tmp80 - (32*discbslog&
                    &t*tmp15*tmp162*tmp174*tmp32*tmp35*tmp37*tmp67)/tmp80 - (16*discbslogt*tmp104*tmp&
                    &12*tmp2*tmp32*tmp35*tmp37*tmp67)/tmp80 - (32*discbs*logmume*tmp104*tmp12*tmp2*tm&
                    &p32*tmp35*tmp37*tmp67)/tmp80 + (64*discbs*logmume*tmp162*tmp165*tmp2*tmp32*tmp35&
                    &*tmp37*tmp67)/tmp80 + (16*discbs*tmp162*tmp174*tmp2*tmp32*tmp35*tmp37*tmp67)/tmp&
                    &80 + (32*discbslogt*tmp162*tmp174*tmp2*tmp32*tmp35*tmp37*tmp67)/tmp80 - (16*disc&
                    &bslogt*tmp104*tmp12*tmp15*tmp18*tmp32*tmp35*tmp37*tmp67)/(tmp31*tmp80) - (32*dis&
                    &cbs*logmume*tmp104*tmp12*tmp15*tmp18*tmp32*tmp35*tmp37*tmp67)/(tmp31*tmp80) + (6&
                    &4*discbs*logmume*tmp15*tmp162*tmp165*tmp18*tmp32*tmp35*tmp37*tmp67)/(tmp31*tmp80&
                    &) + (16*discbs*tmp15*tmp162*tmp174*tmp18*tmp32*tmp35*tmp37*tmp67)/(tmp31*tmp80) &
                    &+ (32*discbslogt*tmp15*tmp162*tmp174*tmp18*tmp32*tmp35*tmp37*tmp67)/(tmp31*tmp80&
                    &) + (16*discbslogt*tmp104*tmp12*tmp2*tmp31*tmp32*tmp35*tmp37*tmp67)/(tmp18*tmp80&
                    &) + (32*discbs*logmume*tmp104*tmp12*tmp2*tmp31*tmp32*tmp35*tmp37*tmp67)/(tmp18*t&
                    &mp80) - (64*discbs*logmume*tmp162*tmp165*tmp2*tmp31*tmp32*tmp35*tmp37*tmp67)/(tm&
                    &p18*tmp80) - (16*discbs*tmp162*tmp174*tmp2*tmp31*tmp32*tmp35*tmp37*tmp67)/(tmp18&
                    &*tmp80) - (32*discbslogt*tmp162*tmp174*tmp2*tmp31*tmp32*tmp35*tmp37*tmp67)/(tmp1&
                    &8*tmp80) + (8*discbs*logmm*tmp15*tmp32*tmp33*tmp49*tmp67)/tmp80 + (16*discbs*log&
                    &mm*tmp15*tmp18*tmp32*tmp33*tmp49*tmp67)/(tmp2*tmp80) + (24*discbs*logmm*tmp2*tmp&
                    &31*tmp32*tmp33*tmp49*tmp67)/(tmp18*tmp80) - (16*discbs*logmm*mm2*tmp18*tmp33*tmp&
                    &49*tmp69)/tmp80 - (16*discbs*logmm*me2*tmp2*tmp33*tmp49*tmp69)/tmp80 + (16*discb&
                    &s*logmm*tmp15*tmp32*tmp33*tmp49*tmp69)/tmp80 + (16*discbs*logmm*tmp2*tmp31*tmp32&
                    &*tmp33*tmp49*tmp69)/(tmp15*tmp80) + (8*discbs*logmm*tmp12*tmp2*tmp32*tmp49*tmp5*&
                    &tmp69)/tmp80 + (8*discbs*logmm*tmp12*tmp15*tmp31*tmp32*tmp49*tmp5*tmp69)/(tmp18*&
                    &tmp80) + (8*discbs*logmm*tmp18*tmp33*tmp49*tmp5*tmp69)/tmp80 + (8*discbs*logmm*t&
                    &mp2*tmp33*tmp49*tmp5*tmp69)/tmp80 + (8*discbs*logmm*tmp15*tmp33*tmp49*tmp50*tmp6&
                    &9)/tmp80 + (8*discbs*logmm*tmp31*tmp33*tmp49*tmp51*tmp69)/tmp80 - (16*discbs*log&
                    &mm*tmp15*tmp31*tmp32*tmp33*tmp49*tmp54*tmp69)/tmp80 + (8*discbs*logmm*tmp12*tmp2&
                    &*tmp32*tmp49*tmp67*tmp69)/tmp80 + (8*discbs*logmm*tmp12*tmp15*tmp18*tmp32*tmp49*&
                    &tmp67*tmp69)/(tmp31*tmp80) - (8*discbs*logmm*tmp15*tmp18*tmp33*tmp49*tmp67*tmp69&
                    &)/(tmp2*tmp80) - (8*discbs*logmm*tmp2*tmp31*tmp33*tmp49*tmp67*tmp69)/(tmp18*tmp8&
                    &0) - 32*mm2*tmp18*tmp243*tmp33*tmp35*tmp37*tmp80 - 32*me2*tmp2*tmp243*tmp33*tmp3&
                    &5*tmp37*tmp80 + 8*logmm*tmp120*tmp122*tmp15*tmp158*tmp200*tmp33*tmp5*tmp80 - 8*l&
                    &ogmm*tmp120*tmp122*tmp158*tmp2*tmp200*tmp33*tmp5*tmp80 + (8*logmm*tmp120*tmp122*&
                    &tmp158*tmp18*tmp2*tmp200*tmp33*tmp5*tmp80)/tmp31 - (8*logmm*tmp120*tmp122*tmp15*&
                    &tmp158*tmp200*tmp31*tmp33*tmp5*tmp80)/tmp18 + 16*tmp18*tmp243*tmp33*tmp35*tmp37*&
                    &tmp5*tmp80 + 16*tmp2*tmp243*tmp33*tmp35*tmp37*tmp5*tmp80 + 16*tmp15*tmp243*tmp33&
                    &*tmp35*tmp37*tmp50*tmp80 + 16*tmp243*tmp31*tmp33*tmp35*tmp37*tmp51*tmp80 + 8*log&
                    &mm*tmp120*tmp122*tmp15*tmp158*tmp200*tmp33*tmp67*tmp80 - 8*logmm*tmp120*tmp122*t&
                    &mp158*tmp2*tmp200*tmp33*tmp67*tmp80 - (8*logmm*tmp120*tmp122*tmp15*tmp158*tmp18*&
                    &tmp200*tmp33*tmp67*tmp80)/tmp31 + (8*logmm*tmp120*tmp122*tmp158*tmp2*tmp200*tmp3&
                    &1*tmp33*tmp67*tmp80)/tmp18 - (16*tmp15*tmp18*tmp243*tmp33*tmp35*tmp37*tmp67*tmp8&
                    &0)/tmp2 - (16*tmp2*tmp243*tmp31*tmp33*tmp35*tmp37*tmp67*tmp80)/tmp18 + 16*logmm*&
                    &mm2*tmp18*tmp32*tmp33*tmp69*tmp78*tmp79*tmp80 + 16*logmm*me2*tmp2*tmp32*tmp33*tm&
                    &p69*tmp78*tmp79*tmp80 + (8*logmm*tmp15*tmp18*tmp32*tmp33*tmp67*tmp69*tmp78*tmp79&
                    &*tmp80)/tmp2 + (8*logmm*tmp2*tmp31*tmp32*tmp33*tmp67*tmp69*tmp78*tmp79*tmp80)/tm&
                    &p18 - (16*discbs*logmm*mm2*tmp18*tmp32*tmp33*tmp69*tmp83*tmp84)/tmp80 - (16*disc&
                    &bs*logmm*me2*tmp2*tmp32*tmp33*tmp69*tmp83*tmp84)/tmp80 + (8*discbs*logmm*tmp18*t&
                    &mp32*tmp33*tmp5*tmp69*tmp83*tmp84)/tmp80 + (8*discbs*logmm*tmp2*tmp32*tmp33*tmp5&
                    &*tmp69*tmp83*tmp84)/tmp80 + (8*discbs*logmm*tmp15*tmp32*tmp33*tmp50*tmp69*tmp83*&
                    &tmp84)/tmp80 + (8*discbs*logmm*tmp31*tmp32*tmp33*tmp51*tmp69*tmp83*tmp84)/tmp80 &
                    &- 16*scalarc0ir6s*tmp18*tmp33*tmp69*tmp77*tmp86 - 16*scalarc0ir6s*tmp2*tmp33*tmp&
                    &69*tmp77*tmp86 + 32*discbs*mm2*tmp18*tmp243*tmp33*tmp35*tmp37*tmp87 + 32*discbs*&
                    &me2*tmp2*tmp243*tmp33*tmp35*tmp37*tmp87 - 16*discbs*tmp18*tmp243*tmp33*tmp35*tmp&
                    &37*tmp5*tmp87 - 16*discbs*tmp2*tmp243*tmp33*tmp35*tmp37*tmp5*tmp87 - 16*discbs*t&
                    &mp15*tmp243*tmp33*tmp35*tmp37*tmp50*tmp87 - 16*discbs*tmp243*tmp31*tmp33*tmp35*t&
                    &mp37*tmp51*tmp87 + (16*discbs*tmp15*tmp18*tmp243*tmp33*tmp35*tmp37*tmp67*tmp87)/&
                    &tmp2 + (16*discbs*tmp2*tmp243*tmp31*tmp33*tmp35*tmp37*tmp67*tmp87)/tmp18 + (64*d&
                    &iscbs*logmume*mm2*tmp12*tmp165*tmp18*tmp32*tmp35*tmp37*tmp87)/tmp80 + (32*discbs&
                    &logt*mm2*tmp12*tmp174*tmp18*tmp32*tmp35*tmp37*tmp87)/tmp80 + (64*discbs*logmume*&
                    &me2*tmp12*tmp165*tmp2*tmp32*tmp35*tmp37*tmp87)/tmp80 + (32*discbslogt*me2*tmp12*&
                    &tmp174*tmp2*tmp32*tmp35*tmp37*tmp87)/tmp80 - (32*discbs*logmume*tmp12*tmp165*tmp&
                    &18*tmp32*tmp35*tmp37*tmp5*tmp87)/tmp80 - (16*discbslogt*tmp12*tmp174*tmp18*tmp32&
                    &*tmp35*tmp37*tmp5*tmp87)/tmp80 - (32*discbs*logmume*tmp12*tmp165*tmp2*tmp32*tmp3&
                    &5*tmp37*tmp5*tmp87)/tmp80 - (16*discbslogt*tmp12*tmp174*tmp2*tmp32*tmp35*tmp37*t&
                    &mp5*tmp87)/tmp80 - (32*discbs*logmume*tmp12*tmp15*tmp165*tmp32*tmp35*tmp37*tmp50&
                    &*tmp87)/tmp80 - (16*discbslogt*tmp12*tmp15*tmp174*tmp32*tmp35*tmp37*tmp50*tmp87)&
                    &/tmp80 - (32*discbs*logmume*tmp12*tmp165*tmp31*tmp32*tmp35*tmp37*tmp51*tmp87)/tm&
                    &p80 - (16*discbslogt*tmp12*tmp174*tmp31*tmp32*tmp35*tmp37*tmp51*tmp87)/tmp80 + (&
                    &32*discbs*logmume*tmp12*tmp15*tmp165*tmp18*tmp32*tmp35*tmp37*tmp67*tmp87)/(tmp2*&
                    &tmp80) + (16*discbslogt*tmp12*tmp15*tmp174*tmp18*tmp32*tmp35*tmp37*tmp67*tmp87)/&
                    &(tmp2*tmp80) + (32*discbs*logmume*tmp12*tmp165*tmp2*tmp31*tmp32*tmp35*tmp37*tmp6&
                    &7*tmp87)/(tmp18*tmp80) + (16*discbslogt*tmp12*tmp174*tmp2*tmp31*tmp32*tmp35*tmp3&
                    &7*tmp67*tmp87)/(tmp18*tmp80) + (16*discbs*logmm*mm2*tmp18*tmp32*tmp33*tmp49*tmp6&
                    &9*tmp87)/tmp80 + (16*discbs*logmm*me2*tmp2*tmp32*tmp33*tmp49*tmp69*tmp87)/tmp80 &
                    &+ (8*discbs*logmm*tmp15*tmp18*tmp32*tmp33*tmp49*tmp67*tmp69*tmp87)/(tmp2*tmp80) &
                    &+ (8*discbs*logmm*tmp2*tmp31*tmp32*tmp33*tmp49*tmp67*tmp69*tmp87)/(tmp18*tmp80) &
                    &+ 32*scalarc0ir6u*tmp11*tmp15*tmp33*tmp9 - 32*mm2*scalarc0ir6u*tmp18*tmp33*tmp9 &
                    &- 32*discbu*mm2*tmp11*tmp177*tmp18*tmp181*tmp33*tmp9 - 32*me2*scalarc0ir6u*tmp2*&
                    &tmp33*tmp9 - 32*discbu*me2*tmp11*tmp177*tmp181*tmp2*tmp33*tmp9 - 16*scalarc0ir6u&
                    &*tmp15*tmp184*tmp224*tmp33*tmp9 - (16*scalarc0ir6u*tmp15*tmp18*tmp184*tmp224*tmp&
                    &33*tmp9)/tmp2 + 16*scalarc0ir6u*tmp184*tmp2*tmp224*tmp33*tmp9 + 16*logmm*mm2*tmp&
                    &11*tmp18*tmp23*tmp27*tmp33*tmp9 - 16*discbu*logmm*mm2*tmp11*tmp18*tmp23*tmp27*tm&
                    &p33*tmp9 + (16*discbu*logmm*tmp11*tmp15*tmp23*tmp27*tmp33*tmp9)/tmp182 - (16*dis&
                    &cbu*logmm*mm2*tmp18*tmp23*tmp27*tmp33*tmp9)/tmp182 + 16*logmm*me2*tmp11*tmp2*tmp&
                    &23*tmp27*tmp33*tmp9 - 16*discbu*logmm*me2*tmp11*tmp2*tmp23*tmp27*tmp33*tmp9 - (1&
                    &6*discbu*logmm*me2*tmp2*tmp23*tmp27*tmp33*tmp9)/tmp182 - (16*discbu*logmm*mm2*tm&
                    &p11*tmp18*tmp225*tmp23*tmp27*tmp33*tmp9)/tmp182 - (16*discbu*logmm*me2*tmp11*tmp&
                    &2*tmp225*tmp23*tmp27*tmp33*tmp9)/tmp182 + (16*scalarc0ir6u*tmp15*tmp18*tmp184*tm&
                    &p224*tmp33*tmp9)/tmp31 + (32*scalarc0ir6u*tmp11*tmp2*tmp31*tmp33*tmp9)/tmp15 - (&
                    &32*scalarc0ir6u*tmp184*tmp2*tmp224*tmp31*tmp33*tmp9)/tmp18 + (16*discbu*logmm*tm&
                    &p11*tmp2*tmp23*tmp27*tmp31*tmp33*tmp9)/(tmp15*tmp182) - 16*scalarc0ir6u*tmp11*tm&
                    &p12*tmp15*tmp5*tmp9 + 16*scalarc0ir6u*tmp11*tmp12*tmp2*tmp5*tmp9 - (8*discbu*log&
                    &mm*tmp11*tmp12*tmp15*tmp23*tmp27*tmp5*tmp9)/tmp182 + (8*discbu*logmm*tmp11*tmp12&
                    &*tmp2*tmp23*tmp27*tmp5*tmp9)/tmp182 - (16*scalarc0ir6u*tmp11*tmp12*tmp18*tmp2*tm&
                    &p5*tmp9)/tmp31 - (8*discbu*logmm*tmp11*tmp12*tmp18*tmp2*tmp23*tmp27*tmp5*tmp9)/(&
                    &tmp182*tmp31) + (16*scalarc0ir6u*tmp11*tmp12*tmp15*tmp31*tmp5*tmp9)/tmp18 + (8*d&
                    &iscbu*logmm*tmp11*tmp12*tmp15*tmp23*tmp27*tmp31*tmp5*tmp9)/(tmp18*tmp182) - 16*s&
                    &calarc0ir6u*tmp15*tmp33*tmp5*tmp9 + 16*scalarc0ir6u*tmp18*tmp33*tmp5*tmp9 - 16*d&
                    &iscbu*tmp11*tmp15*tmp177*tmp181*tmp33*tmp5*tmp9 + 16*discbu*tmp11*tmp177*tmp18*t&
                    &mp181*tmp33*tmp5*tmp9 + 32*scalarc0ir6u*tmp2*tmp33*tmp5*tmp9 + 32*discbu*tmp11*t&
                    &mp177*tmp181*tmp2*tmp33*tmp5*tmp9 + 8*logmm*tmp11*tmp15*tmp23*tmp27*tmp33*tmp5*t&
                    &mp9 - 8*discbu*logmm*tmp11*tmp15*tmp23*tmp27*tmp33*tmp5*tmp9 - 8*logmm*tmp11*tmp&
                    &18*tmp23*tmp27*tmp33*tmp5*tmp9 + 8*discbu*logmm*tmp11*tmp18*tmp23*tmp27*tmp33*tm&
                    &p5*tmp9 - (8*discbu*logmm*tmp15*tmp23*tmp27*tmp33*tmp5*tmp9)/tmp182 + (8*discbu*&
                    &logmm*tmp18*tmp23*tmp27*tmp33*tmp5*tmp9)/tmp182 - 16*logmm*tmp11*tmp2*tmp23*tmp2&
                    &7*tmp33*tmp5*tmp9 + 16*discbu*logmm*tmp11*tmp2*tmp23*tmp27*tmp33*tmp5*tmp9 + (16&
                    &*discbu*logmm*tmp2*tmp23*tmp27*tmp33*tmp5*tmp9)/tmp182 - (8*discbu*logmm*tmp11*t&
                    &mp15*tmp225*tmp23*tmp27*tmp33*tmp5*tmp9)/tmp182 + (8*discbu*logmm*tmp11*tmp18*tm&
                    &p225*tmp23*tmp27*tmp33*tmp5*tmp9)/tmp182 + (16*discbu*logmm*tmp11*tmp2*tmp225*tm&
                    &p23*tmp27*tmp33*tmp5*tmp9)/tmp182 - (16*scalarc0ir6u*tmp18*tmp2*tmp33*tmp5*tmp9)&
                    &/tmp31 - (16*discbu*tmp11*tmp177*tmp18*tmp181*tmp2*tmp33*tmp5*tmp9)/tmp31 + (8*l&
                    &ogmm*tmp11*tmp18*tmp2*tmp23*tmp27*tmp33*tmp5*tmp9)/tmp31 - (8*discbu*logmm*tmp11&
                    &*tmp18*tmp2*tmp23*tmp27*tmp33*tmp5*tmp9)/tmp31 - (8*discbu*logmm*tmp18*tmp2*tmp2&
                    &3*tmp27*tmp33*tmp5*tmp9)/(tmp182*tmp31) - (8*discbu*logmm*tmp11*tmp18*tmp2*tmp22&
                    &5*tmp23*tmp27*tmp33*tmp5*tmp9)/(tmp182*tmp31) + (16*scalarc0ir6u*tmp15*tmp31*tmp&
                    &33*tmp5*tmp9)/tmp18 + (16*discbu*tmp11*tmp15*tmp177*tmp181*tmp31*tmp33*tmp5*tmp9&
                    &)/tmp18 - (8*logmm*tmp11*tmp15*tmp23*tmp27*tmp31*tmp33*tmp5*tmp9)/tmp18 + (8*dis&
                    &cbu*logmm*tmp11*tmp15*tmp23*tmp27*tmp31*tmp33*tmp5*tmp9)/tmp18 + (8*discbu*logmm&
                    &*tmp15*tmp23*tmp27*tmp31*tmp33*tmp5*tmp9)/(tmp18*tmp182) + (8*discbu*logmm*tmp11&
                    &*tmp15*tmp225*tmp23*tmp27*tmp31*tmp33*tmp5*tmp9)/(tmp18*tmp182) + 16*scalarc0ir6&
                    &u*tmp15*tmp33*tmp50*tmp9 + 16*discbu*tmp11*tmp15*tmp177*tmp181*tmp33*tmp50*tmp9 &
                    &- 8*logmm*tmp11*tmp15*tmp23*tmp27*tmp33*tmp50*tmp9 + 8*discbu*logmm*tmp11*tmp15*&
                    &tmp23*tmp27*tmp33*tmp50*tmp9 + (8*discbu*logmm*tmp15*tmp23*tmp27*tmp33*tmp50*tmp&
                    &9)/tmp182 + (8*discbu*logmm*tmp11*tmp15*tmp225*tmp23*tmp27*tmp33*tmp50*tmp9)/tmp&
                    &182 + 16*scalarc0ir6u*tmp31*tmp33*tmp51*tmp9 + 16*discbu*tmp11*tmp177*tmp181*tmp&
                    &31*tmp33*tmp51*tmp9 - 8*logmm*tmp11*tmp23*tmp27*tmp31*tmp33*tmp51*tmp9 + 8*discb&
                    &u*logmm*tmp11*tmp23*tmp27*tmp31*tmp33*tmp51*tmp9 + (8*discbu*logmm*tmp23*tmp27*t&
                    &mp31*tmp33*tmp51*tmp9)/tmp182 + (8*discbu*logmm*tmp11*tmp225*tmp23*tmp27*tmp31*t&
                    &mp33*tmp51*tmp9)/tmp182 + (16*discbu*logmm*mm2*tmp11*tmp18*tmp23*tmp33*tmp52*tmp&
                    &9)/tmp182 + (16*discbu*logmm*me2*tmp11*tmp2*tmp23*tmp33*tmp52*tmp9)/tmp182 + (8*&
                    &discbu*logmm*tmp11*tmp15*tmp23*tmp33*tmp5*tmp52*tmp9)/tmp182 - (8*discbu*logmm*t&
                    &mp11*tmp18*tmp23*tmp33*tmp5*tmp52*tmp9)/tmp182 - (16*discbu*logmm*tmp11*tmp2*tmp&
                    &23*tmp33*tmp5*tmp52*tmp9)/tmp182 + (8*discbu*logmm*tmp11*tmp18*tmp2*tmp23*tmp33*&
                    &tmp5*tmp52*tmp9)/(tmp182*tmp31) - (8*discbu*logmm*tmp11*tmp15*tmp23*tmp31*tmp33*&
                    &tmp5*tmp52*tmp9)/(tmp18*tmp182) - (8*discbu*logmm*tmp11*tmp15*tmp23*tmp33*tmp50*&
                    &tmp52*tmp9)/tmp182 - (8*discbu*logmm*tmp11*tmp23*tmp31*tmp33*tmp51*tmp52*tmp9)/t&
                    &mp182 + (16*discbu*logmm*mm2*tmp11*tmp18*tmp27*tmp33*tmp53*tmp9)/tmp182 + (16*di&
                    &scbu*logmm*me2*tmp11*tmp2*tmp27*tmp33*tmp53*tmp9)/tmp182 + (8*discbu*logmm*tmp11&
                    &*tmp15*tmp27*tmp33*tmp5*tmp53*tmp9)/tmp182 - (8*discbu*logmm*tmp11*tmp18*tmp27*t&
                    &mp33*tmp5*tmp53*tmp9)/tmp182 - (16*discbu*logmm*tmp11*tmp2*tmp27*tmp33*tmp5*tmp5&
                    &3*tmp9)/tmp182 + (8*discbu*logmm*tmp11*tmp18*tmp2*tmp27*tmp33*tmp5*tmp53*tmp9)/(&
                    &tmp182*tmp31) - (8*discbu*logmm*tmp11*tmp15*tmp27*tmp31*tmp33*tmp5*tmp53*tmp9)/(&
                    &tmp18*tmp182) - (8*discbu*logmm*tmp11*tmp15*tmp27*tmp33*tmp50*tmp53*tmp9)/tmp182&
                    & - (8*discbu*logmm*tmp11*tmp27*tmp31*tmp33*tmp51*tmp53*tmp9)/tmp182 - 32*scalarc&
                    &0ir6u*tmp11*tmp15*tmp31*tmp33*tmp54*tmp9 - (16*discbu*logmm*tmp11*tmp15*tmp23*tm&
                    &p27*tmp31*tmp33*tmp54*tmp9)/tmp182 - 16*scalarc0ir6u*tmp11*tmp12*tmp15*tmp67*tmp&
                    &9 + 16*scalarc0ir6u*tmp11*tmp12*tmp2*tmp67*tmp9 - (8*discbu*logmm*tmp11*tmp12*tm&
                    &p15*tmp23*tmp27*tmp67*tmp9)/tmp182 + (8*discbu*logmm*tmp11*tmp12*tmp2*tmp23*tmp2&
                    &7*tmp67*tmp9)/tmp182 + (16*scalarc0ir6u*tmp11*tmp12*tmp15*tmp18*tmp67*tmp9)/tmp3&
                    &1 + (8*discbu*logmm*tmp11*tmp12*tmp15*tmp18*tmp23*tmp27*tmp67*tmp9)/(tmp182*tmp3&
                    &1) - (16*scalarc0ir6u*tmp11*tmp12*tmp2*tmp31*tmp67*tmp9)/tmp18 - (8*discbu*logmm&
                    &*tmp11*tmp12*tmp2*tmp23*tmp27*tmp31*tmp67*tmp9)/(tmp18*tmp182) - 16*scalarc0ir6u&
                    &*tmp15*tmp33*tmp67*tmp9 - 16*discbu*tmp11*tmp15*tmp177*tmp181*tmp33*tmp67*tmp9 -&
                    & (16*scalarc0ir6u*tmp15*tmp18*tmp33*tmp67*tmp9)/tmp2 - (16*discbu*tmp11*tmp15*tm&
                    &p177*tmp18*tmp181*tmp33*tmp67*tmp9)/tmp2 + 16*scalarc0ir6u*tmp2*tmp33*tmp67*tmp9&
                    & + 16*discbu*tmp11*tmp177*tmp181*tmp2*tmp33*tmp67*tmp9 - 32*mm2*scalarc0ir6u*tmp&
                    &18*tmp224*tmp33*tmp67*tmp9 - 32*me2*scalarc0ir6u*tmp2*tmp224*tmp33*tmp67*tmp9 + &
                    &8*logmm*tmp11*tmp15*tmp23*tmp27*tmp33*tmp67*tmp9 - 8*discbu*logmm*tmp11*tmp15*tm&
                    &p23*tmp27*tmp33*tmp67*tmp9 - (8*discbu*logmm*tmp15*tmp23*tmp27*tmp33*tmp67*tmp9)&
                    &/tmp182 + (8*logmm*tmp11*tmp15*tmp18*tmp23*tmp27*tmp33*tmp67*tmp9)/tmp2 - (8*dis&
                    &cbu*logmm*tmp11*tmp15*tmp18*tmp23*tmp27*tmp33*tmp67*tmp9)/tmp2 - (8*discbu*logmm&
                    &*tmp15*tmp18*tmp23*tmp27*tmp33*tmp67*tmp9)/(tmp182*tmp2) - 8*logmm*tmp11*tmp2*tm&
                    &p23*tmp27*tmp33*tmp67*tmp9 + 8*discbu*logmm*tmp11*tmp2*tmp23*tmp27*tmp33*tmp67*t&
                    &mp9 + (8*discbu*logmm*tmp2*tmp23*tmp27*tmp33*tmp67*tmp9)/tmp182 - (8*discbu*logm&
                    &m*tmp11*tmp15*tmp225*tmp23*tmp27*tmp33*tmp67*tmp9)/tmp182 - (8*discbu*logmm*tmp1&
                    &1*tmp15*tmp18*tmp225*tmp23*tmp27*tmp33*tmp67*tmp9)/(tmp182*tmp2) + (8*discbu*log&
                    &mm*tmp11*tmp2*tmp225*tmp23*tmp27*tmp33*tmp67*tmp9)/tmp182 + (16*scalarc0ir6u*tmp&
                    &15*tmp18*tmp33*tmp67*tmp9)/tmp31 + (16*discbu*tmp11*tmp15*tmp177*tmp18*tmp181*tm&
                    &p33*tmp67*tmp9)/tmp31 - (8*logmm*tmp11*tmp15*tmp18*tmp23*tmp27*tmp33*tmp67*tmp9)&
                    &/tmp31 + (8*discbu*logmm*tmp11*tmp15*tmp18*tmp23*tmp27*tmp33*tmp67*tmp9)/tmp31 +&
                    & (8*discbu*logmm*tmp15*tmp18*tmp23*tmp27*tmp33*tmp67*tmp9)/(tmp182*tmp31) + (8*d&
                    &iscbu*logmm*tmp11*tmp15*tmp18*tmp225*tmp23*tmp27*tmp33*tmp67*tmp9)/(tmp182*tmp31&
                    &) - (32*scalarc0ir6u*tmp2*tmp31*tmp33*tmp67*tmp9)/tmp18 - (32*discbu*tmp11*tmp17&
                    &7*tmp181*tmp2*tmp31*tmp33*tmp67*tmp9)/tmp18 + (16*logmm*tmp11*tmp2*tmp23*tmp27*t&
                    &mp31*tmp33*tmp67*tmp9)/tmp18 - (16*discbu*logmm*tmp11*tmp2*tmp23*tmp27*tmp31*tmp&
                    &33*tmp67*tmp9)/tmp18 - (16*discbu*logmm*tmp2*tmp23*tmp27*tmp31*tmp33*tmp67*tmp9)&
                    &/(tmp18*tmp182) - (16*discbu*logmm*tmp11*tmp2*tmp225*tmp23*tmp27*tmp31*tmp33*tmp&
                    &67*tmp9)/(tmp18*tmp182) - 16*scalarc0ir6u*tmp15*tmp224*tmp33*tmp5*tmp67*tmp9 + 1&
                    &6*scalarc0ir6u*tmp18*tmp224*tmp33*tmp5*tmp67*tmp9 + 32*scalarc0ir6u*tmp2*tmp224*&
                    &tmp33*tmp5*tmp67*tmp9 - (16*scalarc0ir6u*tmp18*tmp2*tmp224*tmp33*tmp5*tmp67*tmp9&
                    &)/tmp31 + (16*scalarc0ir6u*tmp15*tmp224*tmp31*tmp33*tmp5*tmp67*tmp9)/tmp18 + 16*&
                    &scalarc0ir6u*tmp15*tmp224*tmp33*tmp50*tmp67*tmp9 + 16*scalarc0ir6u*tmp224*tmp31*&
                    &tmp33*tmp51*tmp67*tmp9 + (8*discbu*logmm*tmp11*tmp15*tmp23*tmp33*tmp52*tmp67*tmp&
                    &9)/tmp182 + (8*discbu*logmm*tmp11*tmp15*tmp18*tmp23*tmp33*tmp52*tmp67*tmp9)/(tmp&
                    &182*tmp2) - (8*discbu*logmm*tmp11*tmp2*tmp23*tmp33*tmp52*tmp67*tmp9)/tmp182 - (8&
                    &*discbu*logmm*tmp11*tmp15*tmp18*tmp23*tmp33*tmp52*tmp67*tmp9)/(tmp182*tmp31) + (&
                    &16*discbu*logmm*tmp11*tmp2*tmp23*tmp31*tmp33*tmp52*tmp67*tmp9)/(tmp18*tmp182) + &
                    &(8*discbu*logmm*tmp11*tmp15*tmp27*tmp33*tmp53*tmp67*tmp9)/tmp182 + (8*discbu*log&
                    &mm*tmp11*tmp15*tmp18*tmp27*tmp33*tmp53*tmp67*tmp9)/(tmp182*tmp2) - (8*discbu*log&
                    &mm*tmp11*tmp2*tmp27*tmp33*tmp53*tmp67*tmp9)/tmp182 - (8*discbu*logmm*tmp11*tmp15&
                    &*tmp18*tmp27*tmp33*tmp53*tmp67*tmp9)/(tmp182*tmp31) + (16*discbu*logmm*tmp11*tmp&
                    &2*tmp27*tmp31*tmp33*tmp53*tmp67*tmp9)/(tmp18*tmp182) - 16*logmm*mm2*tmp18*tmp182&
                    &*tmp224*tmp33*tmp79*tmp9 - 16*logmm*me2*tmp182*tmp2*tmp224*tmp33*tmp79*tmp9 - 8*&
                    &logmm*tmp15*tmp182*tmp224*tmp33*tmp5*tmp79*tmp9 + 8*logmm*tmp18*tmp182*tmp224*tm&
                    &p33*tmp5*tmp79*tmp9 + 16*logmm*tmp182*tmp2*tmp224*tmp33*tmp5*tmp79*tmp9 - (8*log&
                    &mm*tmp18*tmp182*tmp2*tmp224*tmp33*tmp5*tmp79*tmp9)/tmp31 + (8*logmm*tmp15*tmp182&
                    &*tmp224*tmp31*tmp33*tmp5*tmp79*tmp9)/tmp18 + 8*logmm*tmp15*tmp182*tmp224*tmp33*t&
                    &mp50*tmp79*tmp9 + 8*logmm*tmp182*tmp224*tmp31*tmp33*tmp51*tmp79*tmp9 - 8*logmm*t&
                    &mp15*tmp182*tmp224*tmp33*tmp67*tmp79*tmp9 - (8*logmm*tmp15*tmp18*tmp182*tmp224*t&
                    &mp33*tmp67*tmp79*tmp9)/tmp2 + 8*logmm*tmp182*tmp2*tmp224*tmp33*tmp67*tmp79*tmp9 &
                    &+ (8*logmm*tmp15*tmp18*tmp182*tmp224*tmp33*tmp67*tmp79*tmp9)/tmp31 - (16*logmm*t&
                    &mp182*tmp2*tmp224*tmp31*tmp33*tmp67*tmp79*tmp9)/tmp18 - 16*scalarc0tme*tmp15*tmp&
                    &33*tmp5*tmp57*tmp60*tmp90 + 16*scalarc0tme*tmp2*tmp33*tmp5*tmp57*tmp60*tmp90 - (&
                    &16*scalarc0tme*tmp18*tmp2*tmp33*tmp5*tmp57*tmp60*tmp90)/tmp31 + (16*scalarc0tme*&
                    &tmp15*tmp31*tmp33*tmp5*tmp57*tmp60*tmp90)/tmp18 - 16*scalarc0tme*tmp15*tmp33*tmp&
                    &57*tmp60*tmp67*tmp90 + 16*scalarc0tme*tmp2*tmp33*tmp57*tmp60*tmp67*tmp90 + (16*s&
                    &calarc0tme*tmp15*tmp18*tmp33*tmp57*tmp60*tmp67*tmp90)/tmp31 - (16*scalarc0tme*tm&
                    &p2*tmp31*tmp33*tmp57*tmp60*tmp67*tmp90)/tmp18 - (16*discbtmm*me2*tmp100*tmp12*tm&
                    &p154*tmp92)/tmp18 + 64*mm2*scalarc0tmm*tmp161*tmp18*tmp33*tmp92 + 64*me2*scalarc&
                    &0tmm*tmp161*tmp2*tmp33*tmp92 - 16*logmm*mm2*tmp120*tmp122*tmp18*tmp200*tmp201*tm&
                    &p33*tmp92 - 16*logmm*me2*tmp120*tmp122*tmp2*tmp200*tmp201*tmp33*tmp92 + 8*discbt&
                    &mm*tmp12*tmp154*tmp2*tmp5*tmp92 + 16*scalarc0tmm*tmp15*tmp161*tmp33*tmp5*tmp92 -&
                    & 32*scalarc0tmm*tmp161*tmp18*tmp33*tmp5*tmp92 - 48*scalarc0tmm*tmp161*tmp2*tmp33&
                    &*tmp5*tmp92 + 8*logmm*tmp120*tmp122*tmp18*tmp200*tmp201*tmp33*tmp5*tmp92 + 8*log&
                    &mm*tmp120*tmp122*tmp2*tmp200*tmp201*tmp33*tmp5*tmp92 + (16*scalarc0tmm*tmp161*tm&
                    &p18*tmp2*tmp33*tmp5*tmp92)/tmp31 - (16*scalarc0tmm*tmp15*tmp161*tmp31*tmp33*tmp5&
                    &*tmp92)/tmp18 + (8*discbtmm*tmp12*tmp15*tmp154*tmp2*tmp50*tmp92)/tmp18 - 32*scal&
                    &arc0tmm*tmp15*tmp161*tmp33*tmp50*tmp92 + 8*logmm*tmp120*tmp122*tmp15*tmp200*tmp2&
                    &01*tmp33*tmp50*tmp92 + 32*discbtmm*logmumm*tmp12*tmp15*tmp165*tmp166*tmp2*tmp51*&
                    &tmp92 - 32*scalarc0tmm*tmp161*tmp31*tmp33*tmp51*tmp92 + 8*logmm*tmp120*tmp122*tm&
                    &p200*tmp201*tmp31*tmp33*tmp51*tmp92 - 32*scalarc0tmm*tmp15*tmp161*tmp33*tmp60*tm&
                    &p92 - (32*scalarc0tmm*tmp161*tmp2*tmp31*tmp33*tmp60*tmp92)/tmp15 + 16*scalarc0tm&
                    &m*tmp12*tmp15*tmp161*tmp5*tmp60*tmp92 - 16*scalarc0tmm*tmp12*tmp161*tmp2*tmp5*tm&
                    &p60*tmp92 + (16*scalarc0tmm*tmp12*tmp161*tmp18*tmp2*tmp5*tmp60*tmp92)/tmp31 - (1&
                    &6*scalarc0tmm*tmp12*tmp15*tmp161*tmp31*tmp5*tmp60*tmp92)/tmp18 + 32*scalarc0tmm*&
                    &tmp15*tmp161*tmp31*tmp33*tmp54*tmp60*tmp92 - 8*discbtmm*tmp12*tmp15*tmp154*tmp67&
                    &*tmp92 + 16*scalarc0tmm*tmp15*tmp161*tmp33*tmp67*tmp92 + (32*scalarc0tmm*tmp15*t&
                    &mp161*tmp18*tmp33*tmp67*tmp92)/tmp2 - 16*scalarc0tmm*tmp161*tmp2*tmp33*tmp67*tmp&
                    &92 - (8*logmm*tmp120*tmp122*tmp15*tmp18*tmp200*tmp201*tmp33*tmp67*tmp92)/tmp2 - &
                    &(16*scalarc0tmm*tmp15*tmp161*tmp18*tmp33*tmp67*tmp92)/tmp31 + (48*scalarc0tmm*tm&
                    &p161*tmp2*tmp31*tmp33*tmp67*tmp92)/tmp18 - (8*logmm*tmp120*tmp122*tmp2*tmp200*tm&
                    &p201*tmp31*tmp33*tmp67*tmp92)/tmp18 + 16*scalarc0tmm*tmp12*tmp15*tmp161*tmp60*tm&
                    &p67*tmp92 - 16*scalarc0tmm*tmp12*tmp161*tmp2*tmp60*tmp67*tmp92 - (16*scalarc0tmm&
                    &*tmp12*tmp15*tmp161*tmp18*tmp60*tmp67*tmp92)/tmp31 + (16*scalarc0tmm*tmp12*tmp16&
                    &1*tmp2*tmp31*tmp60*tmp67*tmp92)/tmp18 + 16*discbtmm*tmp12*tmp15*tmp166*tmp2*(tmp&
                    &206 + 6*tmp139*(tmp10 + tmp29) + 16*mm2*tmp77 - (2*(tmp123 + tmp130 + (mm2 + tmp&
                    &226)*tmp28 + 3*tmp41 + tmp151/tmp80))/tmp33)*tmp92 + 16*logmm*mm2*tmp120*tmp122*&
                    &tmp18*tmp188*tmp33*tmp80*tmp92 + 16*logmm*me2*tmp120*tmp122*tmp188*tmp2*tmp33*tm&
                    &p80*tmp92 + 16*logmm*tmp120*tmp122*tmp15*tmp200*tmp33*tmp80*tmp92 + 32*logmm*mm2&
                    &*tmp122*tmp18*tmp200*tmp33*tmp80*tmp92 - 16*logmm*mm2*tmp120*tmp18*tmp193*tmp200&
                    &*tmp33*tmp80*tmp92 + 32*logmm*me2*tmp122*tmp2*tmp200*tmp33*tmp80*tmp92 - 16*logm&
                    &m*me2*tmp120*tmp193*tmp2*tmp200*tmp33*tmp80*tmp92 + (16*logmm*tmp120*tmp122*tmp2&
                    &*tmp200*tmp31*tmp33*tmp80*tmp92)/tmp15 - 8*logmm*tmp12*tmp120*tmp122*tmp15*tmp20&
                    &0*tmp5*tmp80*tmp92 + 8*logmm*tmp12*tmp120*tmp122*tmp2*tmp200*tmp5*tmp80*tmp92 - &
                    &(8*logmm*tmp12*tmp120*tmp122*tmp18*tmp2*tmp200*tmp5*tmp80*tmp92)/tmp31 + (8*logm&
                    &m*tmp12*tmp120*tmp122*tmp15*tmp200*tmp31*tmp5*tmp80*tmp92)/tmp18 + 8*logmm*tmp12&
                    &0*tmp122*tmp129*tmp15*tmp33*tmp5*tmp80*tmp92 - 8*logmm*tmp120*tmp122*tmp18*tmp18&
                    &8*tmp33*tmp5*tmp80*tmp92 - 8*logmm*tmp120*tmp122*tmp129*tmp2*tmp33*tmp5*tmp80*tm&
                    &p92 - 8*logmm*tmp120*tmp122*tmp188*tmp2*tmp33*tmp5*tmp80*tmp92 + 8*logmm*tmp122*&
                    &tmp15*tmp200*tmp33*tmp5*tmp80*tmp92 - 16*logmm*tmp122*tmp18*tmp200*tmp33*tmp5*tm&
                    &p80*tmp92 - 8*logmm*tmp120*tmp15*tmp193*tmp200*tmp33*tmp5*tmp80*tmp92 + 8*logmm*&
                    &tmp120*tmp18*tmp193*tmp200*tmp33*tmp5*tmp80*tmp92 - 24*logmm*tmp122*tmp2*tmp200*&
                    &tmp33*tmp5*tmp80*tmp92 + 16*logmm*tmp120*tmp193*tmp2*tmp200*tmp33*tmp5*tmp80*tmp&
                    &92 + (8*logmm*tmp120*tmp122*tmp129*tmp18*tmp2*tmp33*tmp5*tmp80*tmp92)/tmp31 + (8&
                    &*logmm*tmp122*tmp18*tmp2*tmp200*tmp33*tmp5*tmp80*tmp92)/tmp31 - (8*logmm*tmp120*&
                    &tmp18*tmp193*tmp2*tmp200*tmp33*tmp5*tmp80*tmp92)/tmp31 - (8*logmm*tmp120*tmp122*&
                    &tmp129*tmp15*tmp31*tmp33*tmp5*tmp80*tmp92)/tmp18 - (8*logmm*tmp122*tmp15*tmp200*&
                    &tmp31*tmp33*tmp5*tmp80*tmp92)/tmp18 + (8*logmm*tmp120*tmp15*tmp193*tmp200*tmp31*&
                    &tmp33*tmp5*tmp80*tmp92)/tmp18 - 8*logmm*tmp120*tmp122*tmp15*tmp188*tmp33*tmp50*t&
                    &mp80*tmp92 - 16*logmm*tmp122*tmp15*tmp200*tmp33*tmp50*tmp80*tmp92 + 8*logmm*tmp1&
                    &20*tmp15*tmp193*tmp200*tmp33*tmp50*tmp80*tmp92 - 8*logmm*tmp120*tmp122*tmp188*tm&
                    &p31*tmp33*tmp51*tmp80*tmp92 - 16*logmm*tmp122*tmp200*tmp31*tmp33*tmp51*tmp80*tmp&
                    &92 + 8*logmm*tmp120*tmp193*tmp200*tmp31*tmp33*tmp51*tmp80*tmp92 - 16*logmm*tmp12&
                    &0*tmp122*tmp15*tmp200*tmp31*tmp33*tmp54*tmp80*tmp92 - 8*logmm*tmp12*tmp120*tmp12&
                    &2*tmp15*tmp200*tmp67*tmp80*tmp92 + 8*logmm*tmp12*tmp120*tmp122*tmp2*tmp200*tmp67&
                    &*tmp80*tmp92 + (8*logmm*tmp12*tmp120*tmp122*tmp15*tmp18*tmp200*tmp67*tmp80*tmp92&
                    &)/tmp31 - (8*logmm*tmp12*tmp120*tmp122*tmp2*tmp200*tmp31*tmp67*tmp80*tmp92)/tmp1&
                    &8 + 8*logmm*tmp120*tmp122*tmp129*tmp15*tmp33*tmp67*tmp80*tmp92 + (8*logmm*tmp120&
                    &*tmp122*tmp15*tmp18*tmp188*tmp33*tmp67*tmp80*tmp92)/tmp2 - 8*logmm*tmp120*tmp122&
                    &*tmp129*tmp2*tmp33*tmp67*tmp80*tmp92 + 8*logmm*tmp122*tmp15*tmp200*tmp33*tmp67*t&
                    &mp80*tmp92 - 8*logmm*tmp120*tmp15*tmp193*tmp200*tmp33*tmp67*tmp80*tmp92 + (16*lo&
                    &gmm*tmp122*tmp15*tmp18*tmp200*tmp33*tmp67*tmp80*tmp92)/tmp2 - (8*logmm*tmp120*tm&
                    &p15*tmp18*tmp193*tmp200*tmp33*tmp67*tmp80*tmp92)/tmp2 - 8*logmm*tmp122*tmp2*tmp2&
                    &00*tmp33*tmp67*tmp80*tmp92 + 8*logmm*tmp120*tmp193*tmp2*tmp200*tmp33*tmp67*tmp80&
                    &*tmp92 - (8*logmm*tmp120*tmp122*tmp129*tmp15*tmp18*tmp33*tmp67*tmp80*tmp92)/tmp3&
                    &1 - (8*logmm*tmp122*tmp15*tmp18*tmp200*tmp33*tmp67*tmp80*tmp92)/tmp31 + (8*logmm&
                    &*tmp120*tmp15*tmp18*tmp193*tmp200*tmp33*tmp67*tmp80*tmp92)/tmp31 + (8*logmm*tmp1&
                    &20*tmp122*tmp129*tmp2*tmp31*tmp33*tmp67*tmp80*tmp92)/tmp18 + (8*logmm*tmp120*tmp&
                    &122*tmp188*tmp2*tmp31*tmp33*tmp67*tmp80*tmp92)/tmp18 + (24*logmm*tmp122*tmp2*tmp&
                    &200*tmp31*tmp33*tmp67*tmp80*tmp92)/tmp18 - (16*logmm*tmp120*tmp193*tmp2*tmp200*t&
                    &mp31*tmp33*tmp67*tmp80*tmp92)/tmp18 - 16*scalarc0tmm*tmp15*tmp33*tmp5*tmp60*tmp9&
                    &2*tmp94 + 16*scalarc0tmm*tmp2*tmp33*tmp5*tmp60*tmp92*tmp94 - (16*scalarc0tmm*tmp&
                    &18*tmp2*tmp33*tmp5*tmp60*tmp92*tmp94)/tmp31 + (16*scalarc0tmm*tmp15*tmp31*tmp33*&
                    &tmp5*tmp60*tmp92*tmp94)/tmp18 - 16*scalarc0tmm*tmp15*tmp33*tmp60*tmp67*tmp92*tmp&
                    &94 + 16*scalarc0tmm*tmp2*tmp33*tmp60*tmp67*tmp92*tmp94 + (16*scalarc0tmm*tmp15*t&
                    &mp18*tmp33*tmp60*tmp67*tmp92*tmp94)/tmp31 - (16*scalarc0tmm*tmp2*tmp31*tmp33*tmp&
                    &60*tmp67*tmp92*tmp94)/tmp18 + discbs*logmm*tmp18*tmp33*tmp49*tmp5*tmp69*tmp95 + &
                    &discbs*logmm*tmp2*tmp33*tmp49*tmp5*tmp69*tmp95 + discbs*logmm*tmp15*tmp33*tmp49*&
                    &tmp50*tmp69*tmp95 + discbs*logmm*tmp31*tmp33*tmp49*tmp51*tmp69*tmp95 + (logmm*tm&
                    &p15*tmp18*tmp33*tmp49*tmp67*tmp69*tmp95)/tmp2 + (logmm*tmp2*tmp31*tmp33*tmp49*tm&
                    &p67*tmp69*tmp95)/tmp18 + (discbs*logmm*tmp15*tmp31*tmp33*tmp49*tmp5*tmp95)/(tmp1&
                    &8*tmp80) + (discbs*logmm*tmp2*tmp33*tmp49*tmp67*tmp95)/tmp80 + (discbs*logmm*tmp&
                    &15*tmp18*tmp33*tmp49*tmp67*tmp95)/(tmp31*tmp80) + (discbs*logmm*tmp12*tmp15*tmp4&
                    &9*tmp5*tmp69*tmp95)/tmp80 + (discbs*logmm*tmp12*tmp18*tmp2*tmp49*tmp5*tmp69*tmp9&
                    &5)/(tmp31*tmp80) + (discbs*logmm*tmp12*tmp15*tmp49*tmp67*tmp69*tmp95)/tmp80 + (d&
                    &iscbs*logmm*tmp12*tmp2*tmp31*tmp49*tmp67*tmp69*tmp95)/(tmp18*tmp80) + logmm*tmp1&
                    &5*tmp33*tmp50*tmp69*tmp78*tmp79*tmp80*tmp95 + logmm*tmp31*tmp33*tmp51*tmp69*tmp7&
                    &8*tmp79*tmp80*tmp95 + (discbs*logmm*tmp15*tmp18*tmp33*tmp67*tmp69*tmp83*tmp84*tm&
                    &p95)/(tmp2*tmp80) + (discbs*logmm*tmp2*tmp31*tmp33*tmp67*tmp69*tmp83*tmp84*tmp95&
                    &)/(tmp18*tmp80) + logmm*tmp18*tmp33*tmp69*tmp79*tmp80*tmp86*tmp95 + logmm*tmp2*t&
                    &mp33*tmp69*tmp79*tmp80*tmp86*tmp95 + (discbs*logmm*tmp18*tmp33*tmp49*tmp5*tmp69*&
                    &tmp87*tmp95)/tmp80 + (discbs*logmm*tmp2*tmp33*tmp49*tmp5*tmp69*tmp87*tmp95)/tmp8&
                    &0 + (discbs*logmm*tmp15*tmp33*tmp49*tmp50*tmp69*tmp87*tmp95)/tmp80 + (discbs*log&
                    &mm*tmp31*tmp33*tmp49*tmp51*tmp69*tmp87*tmp95)/tmp80 + (32*discbslogt*mm2*tmp12*t&
                    &mp18*tmp32*tmp35*tmp37*tmp96)/tmp80 + (32*discbslogt*me2*tmp12*tmp2*tmp32*tmp35*&
                    &tmp37*tmp96)/tmp80 - (16*discbslogt*tmp12*tmp18*tmp32*tmp35*tmp37*tmp5*tmp96)/tm&
                    &p80 - (16*discbslogt*tmp12*tmp2*tmp32*tmp35*tmp37*tmp5*tmp96)/tmp80 - (16*discbs&
                    &logt*tmp12*tmp15*tmp32*tmp35*tmp37*tmp50*tmp96)/tmp80 - (16*discbslogt*tmp12*tmp&
                    &31*tmp32*tmp35*tmp37*tmp51*tmp96)/tmp80 + (16*discbslogt*tmp12*tmp15*tmp18*tmp32&
                    &*tmp35*tmp37*tmp67*tmp96)/(tmp2*tmp80) + (16*discbslogt*tmp12*tmp2*tmp31*tmp32*t&
                    &mp35*tmp37*tmp67*tmp96)/(tmp18*tmp80) + (64*mm2*tmp104*tmp12*tmp97)/tmp31 + (32*&
                    &logmume*mm2*tmp104*tmp12*tmp97)/tmp31 + (32*mm2*scalarc0ir6tme*tmp101*tmp104*tmp&
                    &12*tmp97)/tmp31 + (32*mm2*scalarc0ir6tme*tmp12*tmp165*tmp97)/tmp31 - (128*mm2*tm&
                    &p162*tmp165*tmp97)/tmp31 - (64*logmume*mm2*tmp162*tmp165*tmp97)/tmp31 - (64*mm2*&
                    &scalarc0ir6tme*tmp101*tmp162*tmp165*tmp97)/tmp31 - (32*discbtme*mm2*tmp101*tmp12&
                    &*tmp165*tmp171*tmp97)/tmp31 + (32*mm2*scalarc0ir6tme*tmp101*tmp162*tmp165*tmp172&
                    &*tmp50*tmp97)/tmp31 - (16*discbtme*mm2*tmp12*tmp148*tmp57*tmp97)/tmp2 + (16*disc&
                    &btme*mm2*tmp12*tmp141*tmp57*tmp97)/tmp31 - (32*discbtme*logmume*mm2*tmp12*tmp165&
                    &*tmp57*tmp97)/tmp31 - (16*mm2*tmp162*tmp207*tmp57*tmp97)/tmp31 - (32*discbtme*mm&
                    &2*tmp162*tmp207*tmp57*tmp97)/tmp31 + (16*discbtme*mm2*tmp12*tmp207*tmp210*tmp57*&
                    &tmp97)/tmp31 + (32*discbtme*logmume*mm2*tmp104*tmp12*tmp50*tmp57*tmp97)/tmp31 - &
                    &(32*logmume*mm2*tmp162*tmp165*tmp50*tmp57*tmp97)/tmp31 - (64*discbtme*logmume*mm&
                    &2*tmp162*tmp165*tmp50*tmp57*tmp97)/tmp31 + (32*discbtme*logmume*mm2*tmp12*tmp165&
                    &*tmp210*tmp50*tmp57*tmp97)/tmp31 + (16*discbtme*mm2*tmp12*tmp207*tmp66*tmp97)/tm&
                    &p31 + (32*discbtme*logmume*mm2*tmp12*tmp165*tmp50*tmp66*tmp97)/tmp31 - (64*me2*t&
                    &mp100*tmp12*tmp99)/tmp18 - (32*logmumm*me2*tmp100*tmp12*tmp99)/tmp18 - (32*me2*s&
                    &calarc0ir6tmm*tmp100*tmp103*tmp12*tmp99)/tmp18 + (64*discbu*logmume*mm2*tmp11*tm&
                    &p12*tmp18*tmp23*tmp27*tmp99)/tmp182 + (64*discbu*logmume*me2*tmp11*tmp12*tmp2*tm&
                    &p23*tmp27*tmp99)/tmp182 + 32*tmp12*tmp18*tmp5*tmp99 + 16*logmume*tmp12*tmp18*tmp&
                    &5*tmp99 + 16*scalarc0ir6tme*tmp101*tmp12*tmp18*tmp5*tmp99 + 32*tmp12*tmp2*tmp5*t&
                    &mp99 + 16*logmumm*tmp12*tmp2*tmp5*tmp99 + 16*scalarc0ir6tmm*tmp103*tmp12*tmp2*tm&
                    &p5*tmp99 - (32*discbu*logmume*tmp11*tmp12*tmp18*tmp23*tmp27*tmp5*tmp99)/tmp182 -&
                    & (32*discbu*logmume*tmp11*tmp12*tmp2*tmp23*tmp27*tmp5*tmp99)/tmp182 + (32*tmp12*&
                    &tmp15*tmp2*tmp50*tmp99)/tmp18 + (16*logmumm*tmp12*tmp15*tmp2*tmp50*tmp99)/tmp18 &
                    &+ (16*scalarc0ir6tmm*tmp103*tmp12*tmp15*tmp2*tmp50*tmp99)/tmp18 - (32*discbu*log&
                    &mume*tmp11*tmp12*tmp15*tmp23*tmp27*tmp50*tmp99)/tmp182 + (32*tmp12*tmp18*tmp31*t&
                    &mp51*tmp99)/tmp2 + (16*logmume*tmp12*tmp18*tmp31*tmp51*tmp99)/tmp2 + (16*scalarc&
                    &0ir6tme*tmp101*tmp12*tmp18*tmp31*tmp51*tmp99)/tmp2 - (32*discbu*logmume*tmp11*tm&
                    &p12*tmp23*tmp27*tmp31*tmp51*tmp99)/tmp182 + 16*discbtme*logmume*tmp12*tmp18*tmp5&
                    &*tmp50*tmp57*tmp99 + (16*discbtme*logmume*tmp12*tmp18*tmp31*tmp50*tmp51*tmp57*tm&
                    &p99)/tmp2 - 32*tmp12*tmp15*tmp67*tmp99 - 16*logmumm*tmp12*tmp15*tmp67*tmp99 - 16&
                    &*scalarc0ir6tmm*tmp103*tmp12*tmp15*tmp67*tmp99 + (32*discbu*logmume*tmp11*tmp12*&
                    &tmp15*tmp18*tmp23*tmp27*tmp67*tmp99)/(tmp182*tmp2) - 32*tmp12*tmp31*tmp67*tmp99 &
                    &- 16*logmume*tmp12*tmp31*tmp67*tmp99 - 16*scalarc0ir6tme*tmp101*tmp12*tmp31*tmp6&
                    &7*tmp99 + (32*discbu*logmume*tmp11*tmp12*tmp2*tmp23*tmp27*tmp31*tmp67*tmp99)/(tm&
                    &p18*tmp182) - 16*discbtme*logmume*tmp12*tmp31*tmp50*tmp57*tmp67*tmp99 + (64*disc&
                    &bs*logmume*mm2*tmp12*tmp18*tmp32*tmp35*tmp37*tmp99)/tmp80 + (64*discbs*logmume*m&
                    &e2*tmp12*tmp2*tmp32*tmp35*tmp37*tmp99)/tmp80 - (32*discbs*logmume*tmp12*tmp18*tm&
                    &p32*tmp35*tmp37*tmp5*tmp99)/tmp80 - (32*discbs*logmume*tmp12*tmp2*tmp32*tmp35*tm&
                    &p37*tmp5*tmp99)/tmp80 - (32*discbs*logmume*tmp12*tmp15*tmp32*tmp35*tmp37*tmp50*t&
                    &mp99)/tmp80 - (32*discbs*logmume*tmp12*tmp31*tmp32*tmp35*tmp37*tmp51*tmp99)/tmp8&
                    &0 + (32*discbs*logmume*tmp12*tmp15*tmp18*tmp32*tmp35*tmp37*tmp67*tmp99)/(tmp2*tm&
                    &p80) + (32*discbs*logmume*tmp12*tmp2*tmp31*tmp32*tmp35*tmp37*tmp67*tmp99)/(tmp18&
                    &*tmp80) - (32*discbtmm*logmumm*me2*tmp100*tmp12*tmp51*tmp92*tmp99)/tmp18 + 16*di&
                    &scbtmm*logmumm*tmp12*tmp2*tmp5*tmp51*tmp92*tmp99 + (16*discbtmm*logmumm*tmp12*tm&
                    &p15*tmp2*tmp50*tmp51*tmp92*tmp99)/tmp18 - 16*discbtmm*logmumm*tmp12*tmp15*tmp51*&
                    &tmp67*tmp92*tmp99 - (64*mm2*tmp12*tmp97*tmp99)/tmp2 - (32*logmume*mm2*tmp12*tmp9&
                    &7*tmp99)/tmp2 - (32*mm2*scalarc0ir6tme*tmp101*tmp12*tmp97*tmp99)/tmp2 - (32*disc&
                    &btme*logmume*mm2*tmp12*tmp50*tmp57*tmp97*tmp99)/tmp2

  END FUNCTION LBK_NLP_QE2QM2

  FUNCTION LBK_NLP_QEQM3(me2, mm2, ss, tt, s15, s25, s35)
  implicit none
  real LBK_NLP_QeQm3
  real(kind=prec) ::  me2
  real(kind=prec) ::  mm2
  real(kind=prec) ::  ss
  real(kind=prec) ::  tt
  real(kind=prec) ::  s15
  real(kind=prec) ::  s25
  real(kind=prec) ::  s35
  real tmp1, tmp2, tmp3, tmp4, tmp5
  real tmp6, tmp7, tmp8, tmp9, tmp10
  real tmp11, tmp12, tmp13, tmp14, tmp15
  real tmp16, tmp17, tmp18, tmp19, tmp20
  real tmp21, tmp22, tmp23, tmp24, tmp25
  real tmp26, tmp27, tmp28, tmp29, tmp30
  real tmp31, tmp32, tmp33, tmp34, tmp35
  real tmp36, tmp37, tmp38, tmp39, tmp40
  real tmp41, tmp42, tmp43, tmp44, tmp45
  real tmp46, tmp47, tmp48, tmp49, tmp50
  real tmp51, tmp52, tmp53, tmp54, tmp55
  real tmp56, tmp57, tmp58, tmp59, tmp60
  real tmp61, tmp62, tmp63, tmp64, tmp65
  real tmp66, tmp67, tmp68, tmp69, tmp70
  real tmp71, tmp72, tmp73, tmp74, tmp75
  real tmp76, tmp77, tmp78, tmp79, tmp80
  real tmp81, tmp82, tmp83, tmp84, tmp85
  real tmp86, tmp87, tmp88, tmp89, tmp90
  real tmp91, tmp92, tmp93, tmp94, tmp95
  real tmp96, tmp97, tmp98, tmp99, tmp100
  real tmp101, tmp102, tmp103, tmp104, tmp105
  real tmp106, tmp107, tmp108, tmp109, tmp110
  real tmp111, tmp112, tmp113, tmp114, tmp115
  real tmp116, tmp117, tmp118, tmp119, tmp120
  real tmp121, tmp122, tmp123, tmp124, tmp125
  real tmp126, tmp127, tmp128, tmp129, tmp130
  real tmp131, tmp132, tmp133, tmp134, tmp135
  real tmp136, tmp137, tmp138, tmp139, tmp140
  real tmp141, tmp142, tmp143, tmp144, tmp145
  real tmp146, tmp147, tmp148, tmp149, tmp150
  real tmp151, tmp152, tmp153, tmp154, tmp155
  real tmp156, tmp157, tmp158, tmp159, tmp160
  real tmp161, tmp162, tmp163, tmp164, tmp165
  real tmp166, tmp167, tmp168, tmp169, tmp170
  real tmp171, tmp172, tmp173, tmp174, tmp175
  real tmp176, tmp177, tmp178, tmp179, tmp180
  real tmp181, tmp182, tmp183, tmp184, tmp185
  real tmp186, tmp187, tmp188, tmp189, tmp190
  real tmp191, tmp192, tmp193, tmp194, tmp195
  real tmp196, tmp197, tmp198, tmp199, tmp200
  real tmp201, tmp202, tmp203, tmp204, tmp205
  real tmp206, tmp207, tmp208, tmp209, tmp210
  real tmp211, tmp212, tmp213, tmp214, tmp215
  real tmp216, tmp217, tmp218, tmp219, tmp220
  real tmp221, tmp222, tmp223, tmp224, tmp225
  real tmp226, tmp227, tmp228, tmp229, tmp230
  real tmp231, tmp232, tmp233, tmp234, tmp235
  real tmp236, tmp237, tmp238, tmp239, tmp240
  real tmp241, tmp242, tmp243, tmp244, tmp245
  real tmp246, tmp247, tmp248, tmp249

  tmp1 = -tt
  tmp2 = 4*me2
  tmp3 = 4*mm2
  tmp4 = -2*ss
  tmp5 = tmp1 + tmp2 + tmp3 + tmp4
  tmp6 = -ss
  tmp7 = me2 + mm2 + tmp1 + tmp6
  tmp8 = tt**(-2)
  tmp9 = -s35
  tmp10 = s15 + s25 + tmp9
  tmp11 = 1/s25
  tmp12 = 1/tmp10
  tmp13 = 2*mm2
  tmp14 = tmp1 + tmp13
  tmp15 = sqrt(me2)
  tmp16 = sqrt(mm2)
  tmp17 = tmp12**2
  tmp18 = -tmp16
  tmp19 = tmp15 + tmp18
  tmp20 = tmp19**2
  tmp21 = tmp1 + tmp20 + tmp6
  tmp22 = 1/tmp21
  tmp23 = tmp15 + tmp16
  tmp24 = tmp23**2
  tmp25 = tmp1 + tmp24 + tmp6
  tmp26 = 1/tmp25
  tmp27 = 2*me2
  tmp28 = tmp14 + tmp27 + tmp6
  tmp29 = tmp11**2
  tmp30 = me2 + mm2 + tmp6
  tmp31 = 1/tt
  tmp32 = tmp20 + tmp6
  tmp33 = 1/tmp32
  tmp34 = tmp24 + tmp6
  tmp35 = 1/tmp34
  tmp36 = me2**2
  tmp37 = -2*me2*mm2
  tmp38 = mm2**2
  tmp39 = ss**2
  tmp40 = -tmp39
  tmp41 = tmp36 + tmp37 + tmp38 + tmp40
  tmp42 = mm2 + tmp6
  tmp43 = tmp42**2
  tmp44 = mm2 + ss
  tmp45 = -2*me2*tmp44
  tmp46 = tmp36 + tmp43 + tmp45
  tmp47 = 1/tmp46
  tmp48 = -me2
  tmp49 = -mm2
  tmp50 = ss + tmp48 + tmp49
  tmp51 = tmp26**2
  tmp52 = tmp22**2
  tmp53 = 4*tmp15*tmp16
  tmp54 = tmp1 + tmp2
  tmp55 = 1/tmp54
  tmp56 = tmp1 + tmp53
  tmp57 = 2*tmp30
  tmp58 = tmp1 + tmp57
  tmp59 = 1/tmp31 + tmp53
  tmp60 = -4*mm2
  tmp61 = 1/tmp31 + tmp60
  tmp62 = tmp61**(-2)
  tmp63 = 1/tmp61
  tmp64 = tmp55**2
  tmp65 = 1/tmp31 + tmp50
  tmp66 = 2*ss
  tmp67 = 1/tmp31 + tmp66
  tmp68 = me2 + tmp49
  tmp69 = tmp68**2
  tmp70 = tmp40 + tmp69
  tmp71 = me2*tmp4
  tmp72 = mm2*tmp4
  tmp73 = tmp36 + tmp37 + tmp38 + tmp39 + tmp71 + tmp72
  tmp74 = tmp73**(-2)
  tmp75 = 1/tmp73
  tmp76 = tmp30**2
  tmp77 = mm2 + tmp48
  tmp78 = 1/ss
  tmp79 = -2*me2
  tmp80 = -2*tmp42
  tmp81 = tmp79 + tmp80
  tmp82 = tmp47**2
  tmp83 = -tmp78
  tmp84 = tmp50*tmp75
  tmp85 = tmp83 + tmp84
  tmp86 = -8*me2
  tmp87 = 2/tmp31
  tmp88 = tmp86 + tmp87
  tmp89 = tmp1 + tmp3
  tmp90 = 1/tmp89
  tmp91 = -8*mm2
  tmp92 = tmp87 + tmp91
  tmp93 = -8*tmp30
  tmp94 = tmp87 + tmp93
  tmp95 = 1/s15
  tmp96 = -4*tmp30
  tmp97 = tmp87 + tmp96
  tmp98 = 1/s35
  tmp99 = tmp1 + tmp27
  tmp100 = -2*mm2
  tmp101 = tmp100 + 1/tmp31
  tmp102 = tmp66 + tmp87
  tmp103 = 6/tmp31
  tmp104 = tmp103 + tmp93
  tmp105 = me2 + mm2
  tmp106 = -4*tmp105
  tmp107 = 6/tmp78
  tmp108 = tmp103 + tmp106 + tmp107
  tmp109 = -3*tmp36
  tmp110 = -6/tmp78
  tmp111 = tmp110 + tmp13
  tmp112 = tmp111*tmp48
  tmp113 = -3*tmp43
  tmp114 = -2/(tmp31*tmp78)
  tmp115 = tmp109 + tmp112 + tmp113 + tmp114
  tmp116 = -2*tmp30
  tmp117 = tmp116 + 1/tmp31
  tmp118 = tmp101 + 1/tmp78 + tmp79
  tmp119 = 1/tmp118
  tmp120 = 3*tmp38
  tmp121 = tmp13/tmp78
  tmp122 = tmp13 + 1/tmp78
  tmp123 = tmp122*tmp79
  tmp124 = tmp68 + 1/tmp78
  tmp125 = tmp124*tmp87
  tmp126 = tmp120 + tmp121 + tmp123 + tmp125 + tmp36 + tmp39
  tmp127 = 3*tmp36
  tmp128 = -3/tmp78
  tmp129 = mm2 + tmp128
  tmp130 = tmp129*tmp27
  tmp131 = (-14*mm2)/tmp78
  tmp132 = 3*tmp39
  tmp133 = tmp120 + tmp127 + tmp130 + tmp131 + tmp132
  tmp134 = -2*tmp133
  tmp135 = tmp13 + tmp6
  tmp136 = (12*tmp135)/tmp31
  tmp137 = tmp31**(-2)
  tmp138 = -9*tmp137
  tmp139 = tmp134 + tmp136 + tmp138
  tmp140 = -32*mm2*tmp30
  tmp141 = -6*me2
  tmp142 = -14*mm2
  tmp143 = tmp107 + tmp141 + tmp142
  tmp144 = (-2*tmp143)/tmp31
  tmp145 = -6*tmp137
  tmp146 = tmp140 + tmp144 + tmp145
  tmp147 = 8*tmp36
  tmp148 = tmp86/tmp31
  tmp149 = tmp137 + tmp147 + tmp148
  tmp150 = 1/tmp31 + tmp79
  tmp151 = tmp90**2
  tmp152 = 8*tmp38
  tmp153 = tmp91/tmp31
  tmp154 = tmp137 + tmp152 + tmp153
  tmp155 = tmp31**3
  tmp156 = 2*tmp76
  tmp157 = tmp87/tmp78
  tmp158 = tmp137 + tmp156 + tmp157
  tmp159 = 1/tmp11 + 1/tmp95
  tmp160 = tmp35**2
  tmp161 = tmp33**2
  tmp162 = 4*tmp76
  tmp163 = tmp137 + tmp157 + tmp162
  tmp164 = tmp28**2
  tmp165 = -tmp164
  tmp166 = tmp165 + tmp69
  tmp167 = tmp79/tmp31
  tmp168 = tmp100/tmp31
  tmp169 = tmp137 + tmp157 + tmp167 + tmp168 + tmp73
  tmp170 = tmp169**(-2)
  tmp171 = 1/tmp28
  tmp172 = 1/tmp169
  tmp173 = tmp3*tmp81
  tmp174 = tmp13 + tmp66 + tmp79
  tmp175 = tmp174/tmp31
  tmp176 = tmp137 + tmp173 + tmp175
  tmp177 = tmp106/tmp31
  tmp178 = tmp107/tmp31
  tmp179 = 3*tmp137
  tmp180 = tmp162 + tmp177 + tmp178 + tmp179
  tmp181 = tmp119**2
  tmp182 = -tmp36
  tmp183 = tmp182 + tmp43 + tmp71
  tmp184 = tmp183*tmp3
  tmp185 = tmp120 + tmp121 + tmp123 + tmp36 + tmp39
  tmp186 = tmp185/tmp31
  tmp187 = tmp124*tmp137
  tmp188 = tmp184 + tmp186 + tmp187
  tmp189 = tmp78**2
  tmp190 = 16*mm2*tmp76
  tmp191 = tmp134/tmp31
  tmp192 = 6*tmp135*tmp137
  tmp193 = 1/tmp155
  tmp194 = -3*tmp193
  tmp195 = tmp190 + tmp191 + tmp192 + tmp194
  tmp196 = -tmp31
  tmp197 = tmp101*tmp31*tmp63
  tmp198 = tmp196 + tmp197
  tmp199 = 1/tmp31 + tmp49 + 1/tmp78
  tmp200 = tmp199**2
  tmp201 = mm2 + 1/tmp31 + 1/tmp78
  tmp202 = tmp201*tmp79
  tmp203 = tmp200 + tmp202 + tmp36
  tmp204 = 1/tmp203
  tmp205 = tmp49 + 1/tmp78
  tmp206 = (2*tmp205)/tmp78
  tmp207 = tmp44*tmp87
  tmp208 = tmp4 + tmp92
  tmp209 = me2*tmp208
  tmp210 = tmp206 + tmp207 + tmp209
  tmp211 = -tmp171
  tmp212 = tmp172*tmp7
  tmp213 = tmp211 + tmp212
  tmp214 = 3*tmp43
  tmp215 = tmp205*tmp87
  tmp216 = 1/tmp31 + 1/tmp78
  tmp217 = -3*tmp216
  tmp218 = tmp128 + tmp13 + tmp217 + 1/tmp31
  tmp219 = me2*tmp218
  tmp220 = tmp127 + tmp137 + tmp157 + tmp214 + tmp215 + tmp219
  tmp221 = me2**3
  tmp222 = tmp42**3
  tmp223 = tmp38 + tmp40
  tmp224 = tmp223/tmp31
  tmp225 = tmp128 + 1/tmp31 + tmp49
  tmp226 = tmp225*tmp36
  tmp227 = -3*tmp39
  tmp228 = tmp13*tmp216
  tmp229 = tmp227 + tmp228 + tmp38
  tmp230 = tmp229*tmp48
  tmp231 = tmp221 + tmp222 + tmp224 + tmp226 + tmp230
  tmp232 = 3/tmp78
  tmp233 = 2*tmp199
  tmp234 = tmp233 + tmp79
  tmp235 = tmp204**2
  tmp236 = -tmp221
  tmp237 = -tmp222
  tmp238 = mm2 + tmp232
  tmp239 = tmp238*tmp36
  tmp240 = tmp215/tmp78
  tmp241 = tmp137*tmp44
  tmp242 = -4/tmp31
  tmp243 = tmp242 + 1/tmp78
  tmp244 = tmp13*tmp243
  tmp245 = tmp1 + tmp232
  tmp246 = -(tmp216*tmp245)
  tmp247 = tmp244 + tmp246 + tmp38
  tmp248 = me2*tmp247
  tmp249 = tmp236 + tmp237 + tmp239 + tmp240 + tmp241 + tmp248
  LBK_NLP_QeQm3 = scalarc0ir6s*tmp11*tmp140*tmp31 + (scalarc0ir6s*tmp140*tmp17*tmp31)/tmp11 + 32*d&
                   &iscbu*mm2*tmp11*tmp204*tmp210*tmp31 - 32*discbu*mm2*tmp12*tmp204*tmp210*tmp31 + &
                   &32*discbu*tmp11*tmp14*tmp204*tmp210*tmp31 - 32*discbu*tmp12*tmp14*tmp204*tmp210*&
                   &tmp31 + (32*discbu*mm2*tmp17*tmp204*tmp210*tmp31)/tmp11 + 32*mm2*tmp11*tmp171*tm&
                   &p204*tmp249*tmp31 - 32*mm2*tmp12*tmp171*tmp204*tmp249*tmp31 + 32*tmp11*tmp14*tmp&
                   &171*tmp204*tmp249*tmp31 - 32*tmp12*tmp14*tmp171*tmp204*tmp249*tmp31 + (32*mm2*tm&
                   &p17*tmp171*tmp204*tmp249*tmp31)/tmp11 - 32*discbu*mm2*tmp11*tmp204*tmp213*tmp249&
                   &*tmp31 + 32*discbu*mm2*tmp12*tmp204*tmp213*tmp249*tmp31 - 32*discbu*tmp11*tmp14*&
                   &tmp204*tmp213*tmp249*tmp31 + 32*discbu*tmp12*tmp14*tmp204*tmp213*tmp249*tmp31 - &
                   &(32*discbu*mm2*tmp17*tmp204*tmp213*tmp249*tmp31)/tmp11 - 32*discbu*mm2*tmp11*tmp&
                   &234*tmp235*tmp249*tmp31 + 32*discbu*mm2*tmp12*tmp234*tmp235*tmp249*tmp31 - 32*di&
                   &scbu*tmp11*tmp14*tmp234*tmp235*tmp249*tmp31 + 32*discbu*tmp12*tmp14*tmp234*tmp23&
                   &5*tmp249*tmp31 - (32*discbu*mm2*tmp17*tmp234*tmp235*tmp249*tmp31)/tmp11 - (32*di&
                   &scbu*mm2*tmp204*tmp210*tmp29*tmp31)/tmp12 - (32*mm2*tmp171*tmp204*tmp249*tmp29*t&
                   &mp31)/tmp12 + (32*discbu*mm2*tmp204*tmp213*tmp249*tmp29*tmp31)/tmp12 + (32*discb&
                   &u*mm2*tmp234*tmp235*tmp249*tmp29*tmp31)/tmp12 + 32*mm2*scalarc0ir6s*tmp12*tmp30*&
                   &tmp31 - 32*scalarc0ir6s*tmp11*tmp14*tmp30*tmp31 + 32*scalarc0ir6s*tmp12*tmp14*tm&
                   &p30*tmp31 + (32*mm2*scalarc0ir6s*tmp29*tmp30*tmp31)/tmp12 - 32*discbs*mm2*tmp11*&
                   &tmp31*tmp33*tmp35*tmp41 + 32*discbs*mm2*tmp12*tmp31*tmp33*tmp35*tmp41 - 32*discb&
                   &s*tmp11*tmp14*tmp31*tmp33*tmp35*tmp41 + 32*discbs*tmp12*tmp14*tmp31*tmp33*tmp35*&
                   &tmp41 - (32*discbs*mm2*tmp17*tmp31*tmp33*tmp35*tmp41)/tmp11 + (32*discbs*mm2*tmp&
                   &29*tmp31*tmp33*tmp35*tmp41)/tmp12 + 32*mm2*scalarc0ir6u*tmp11*tmp31*tmp5 - 32*mm&
                   &2*scalarc0ir6u*tmp12*tmp31*tmp5 + 32*scalarc0ir6u*tmp11*tmp14*tmp31*tmp5 - 32*sc&
                   &alarc0ir6u*tmp12*tmp14*tmp31*tmp5 + (32*mm2*scalarc0ir6u*tmp17*tmp31*tmp5)/tmp11&
                   & + (16*discbu*logmm*mm2*tmp11*tmp22*tmp26*tmp31*tmp5)/tmp171 - (16*discbu*logmm*&
                   &mm2*tmp12*tmp22*tmp26*tmp31*tmp5)/tmp171 + (16*discbu*logmm*tmp11*tmp14*tmp22*tm&
                   &p26*tmp31*tmp5)/tmp171 - (16*discbu*logmm*tmp12*tmp14*tmp22*tmp26*tmp31*tmp5)/tm&
                   &p171 + (16*discbu*logmm*mm2*tmp17*tmp22*tmp26*tmp31*tmp5)/(tmp11*tmp171) - (32*m&
                   &m2*scalarc0ir6u*tmp29*tmp31*tmp5)/tmp12 - (16*discbu*logmm*mm2*tmp22*tmp26*tmp29&
                   &*tmp31*tmp5)/(tmp12*tmp171) - 16*discbu*tmp11*tmp204*tmp220*tmp31*tmp50 - 16*tmp&
                   &11*tmp171*tmp204*tmp249*tmp31*tmp50 + 16*discbu*tmp11*tmp204*tmp213*tmp249*tmp31&
                   &*tmp50 + 16*discbu*tmp11*tmp234*tmp235*tmp249*tmp31*tmp50 + 32*scalarc0ir6s*tmp1&
                   &1*tmp30*tmp31*tmp50 + 16*discbs*tmp11*tmp160*tmp231*tmp31*tmp33*tmp50 + 16*discb&
                   &s*tmp11*tmp161*tmp231*tmp31*tmp35*tmp50 + 16*discbs*tmp11*tmp115*tmp31*tmp33*tmp&
                   &35*tmp50 - 16*scalarc0ir6u*tmp11*tmp31*tmp5*tmp50 - (8*discbu*logmm*tmp11*tmp22*&
                   &tmp26*tmp31*tmp5*tmp50)/tmp171 - 32*mm2*scalarc0tme*tmp11*tmp149*tmp31*tmp55 + 3&
                   &2*mm2*scalarc0tme*tmp12*tmp149*tmp31*tmp55 - 32*scalarc0tme*tmp11*tmp14*tmp149*t&
                   &mp31*tmp55 + 32*scalarc0tme*tmp12*tmp14*tmp149*tmp31*tmp55 - (32*mm2*scalarc0tme&
                   &*tmp149*tmp17*tmp31*tmp55)/tmp11 + (32*mm2*scalarc0tme*tmp149*tmp29*tmp31*tmp55)&
                   &/tmp12 + 32*scalarc0tme*tmp11*tmp149*tmp31*tmp50*tmp55 + 32*mm2*scalarc0tmm*tmp1&
                   &1*tmp151*tmp154*tmp31*tmp58 - 32*mm2*scalarc0tmm*tmp12*tmp151*tmp154*tmp31*tmp58&
                   & + 32*scalarc0tmm*tmp11*tmp14*tmp151*tmp154*tmp31*tmp58 - 32*scalarc0tmm*tmp12*t&
                   &mp14*tmp151*tmp154*tmp31*tmp58 + (32*mm2*scalarc0tmm*tmp151*tmp154*tmp17*tmp31*t&
                   &mp58)/tmp11 - (32*mm2*scalarc0tmm*tmp151*tmp154*tmp29*tmp31*tmp58)/tmp12 - 32*lo&
                   &gt*mm2*tmp11*tmp31*tmp55*tmp56*tmp58*tmp59*tmp62 + 32*logt*mm2*tmp12*tmp31*tmp55&
                   &*tmp56*tmp58*tmp59*tmp62 - 32*logt*tmp11*tmp14*tmp31*tmp55*tmp56*tmp58*tmp59*tmp&
                   &62 + 32*logt*tmp12*tmp14*tmp31*tmp55*tmp56*tmp58*tmp59*tmp62 - (32*logt*mm2*tmp1&
                   &7*tmp31*tmp55*tmp56*tmp58*tmp59*tmp62)/tmp11 + (32*logt*mm2*tmp29*tmp31*tmp55*tm&
                   &p56*tmp58*tmp59*tmp62)/tmp12 + 32*logt*mm2*tmp11*tmp31*tmp55*tmp56*tmp58*tmp63 -&
                   & 32*logt*mm2*tmp12*tmp31*tmp55*tmp56*tmp58*tmp63 + 32*logt*tmp11*tmp14*tmp31*tmp&
                   &55*tmp56*tmp58*tmp63 - 32*logt*tmp12*tmp14*tmp31*tmp55*tmp56*tmp58*tmp63 + (32*l&
                   &ogt*mm2*tmp17*tmp31*tmp55*tmp56*tmp58*tmp63)/tmp11 - (32*logt*mm2*tmp29*tmp31*tm&
                   &p55*tmp56*tmp58*tmp63)/tmp12 - 32*logt*mm2*tmp11*tmp31*tmp55*tmp56*tmp59*tmp63 +&
                   & 32*logt*mm2*tmp12*tmp31*tmp55*tmp56*tmp59*tmp63 - 32*logt*tmp11*tmp14*tmp31*tmp&
                   &55*tmp56*tmp59*tmp63 + 32*logt*tmp12*tmp14*tmp31*tmp55*tmp56*tmp59*tmp63 - (32*l&
                   &ogt*mm2*tmp17*tmp31*tmp55*tmp56*tmp59*tmp63)/tmp11 + (32*logt*mm2*tmp29*tmp31*tm&
                   &p55*tmp56*tmp59*tmp63)/tmp12 + 32*logt*tmp11*tmp31*tmp50*tmp55*tmp56*tmp59*tmp63&
                   & - 32*logt*mm2*tmp11*tmp31*tmp55*tmp58*tmp59*tmp63 + 32*logt*mm2*tmp12*tmp31*tmp&
                   &55*tmp58*tmp59*tmp63 - 32*logt*tmp11*tmp14*tmp31*tmp55*tmp58*tmp59*tmp63 + 32*lo&
                   &gt*tmp12*tmp14*tmp31*tmp55*tmp58*tmp59*tmp63 - (32*logt*mm2*tmp17*tmp31*tmp55*tm&
                   &p58*tmp59*tmp63)/tmp11 + (32*logt*mm2*tmp29*tmp31*tmp55*tmp58*tmp59*tmp63)/tmp12&
                   & + 32*mm2*scalarc0tme*tmp11*tmp149*tmp31*tmp58*tmp64 - 32*mm2*scalarc0tme*tmp12*&
                   &tmp149*tmp31*tmp58*tmp64 + 32*scalarc0tme*tmp11*tmp14*tmp149*tmp31*tmp58*tmp64 -&
                   & 32*scalarc0tme*tmp12*tmp14*tmp149*tmp31*tmp58*tmp64 + (32*mm2*scalarc0tme*tmp14&
                   &9*tmp17*tmp31*tmp58*tmp64)/tmp11 - (32*mm2*scalarc0tme*tmp149*tmp29*tmp31*tmp58*&
                   &tmp64)/tmp12 + 32*logt*mm2*tmp11*tmp31*tmp56*tmp58*tmp59*tmp63*tmp64 - 32*logt*m&
                   &m2*tmp12*tmp31*tmp56*tmp58*tmp59*tmp63*tmp64 + 32*logt*tmp11*tmp14*tmp31*tmp56*t&
                   &mp58*tmp59*tmp63*tmp64 - 32*logt*tmp12*tmp14*tmp31*tmp56*tmp58*tmp59*tmp63*tmp64&
                   & + (32*logt*mm2*tmp17*tmp31*tmp56*tmp58*tmp59*tmp63*tmp64)/tmp11 - (32*logt*mm2*&
                   &tmp29*tmp31*tmp56*tmp58*tmp59*tmp63*tmp64)/tmp12 + 16*discbu*tmp12*tmp204*tmp220&
                   &*tmp31*tmp65 + 16*tmp12*tmp171*tmp204*tmp249*tmp31*tmp65 - 16*discbu*tmp12*tmp20&
                   &4*tmp213*tmp249*tmp31*tmp65 - 16*discbu*tmp12*tmp234*tmp235*tmp249*tmp31*tmp65 -&
                   & 32*scalarc0ir6s*tmp12*tmp30*tmp31*tmp65 - 16*discbs*tmp12*tmp160*tmp231*tmp31*t&
                   &mp33*tmp65 - 16*discbs*tmp12*tmp161*tmp231*tmp31*tmp35*tmp65 - 16*discbs*tmp115*&
                   &tmp12*tmp31*tmp33*tmp35*tmp65 + 16*scalarc0ir6u*tmp12*tmp31*tmp5*tmp65 + 32*mm2*&
                   &scalarc0ir6u*tmp11*tmp212*tmp31*tmp5*tmp65 - 32*mm2*scalarc0ir6u*tmp12*tmp212*tm&
                   &p31*tmp5*tmp65 + 32*scalarc0ir6u*tmp11*tmp14*tmp212*tmp31*tmp5*tmp65 - 32*scalar&
                   &c0ir6u*tmp12*tmp14*tmp212*tmp31*tmp5*tmp65 + (32*mm2*scalarc0ir6u*tmp17*tmp212*t&
                   &mp31*tmp5*tmp65)/tmp11 + (8*discbu*logmm*tmp12*tmp22*tmp26*tmp31*tmp5*tmp65)/tmp&
                   &171 - (32*mm2*scalarc0ir6u*tmp212*tmp29*tmp31*tmp5*tmp65)/tmp12 - 16*scalarc0ir6&
                   &u*tmp11*tmp212*tmp31*tmp5*tmp50*tmp65 - 32*scalarc0tme*tmp12*tmp149*tmp31*tmp55*&
                   &tmp65 - 32*logt*tmp12*tmp31*tmp55*tmp56*tmp59*tmp63*tmp65 + 16*scalarc0ir6u*tmp1&
                   &2*tmp212*tmp31*tmp5*tmp65**2 - 16*scalarc0ir6s*tmp11*tmp31*tmp50*tmp67 + 8*discb&
                   &s*logmm*tmp11*tmp30*tmp31*tmp47*tmp50*tmp67 + 16*scalarc0ir6s*tmp12*tmp31*tmp65*&
                   &tmp67 + 8*logmm*tmp12*tmp30*tmp31*tmp47*tmp65*tmp67 + (128*discbu*logmume*mm2*tm&
                   &p11*tmp155*tmp158*tmp22*tmp26*tmp7)/tmp171 - (128*discbu*logmume*mm2*tmp12*tmp15&
                   &5*tmp158*tmp22*tmp26*tmp7)/tmp171 + (128*discbu*logmume*tmp11*tmp14*tmp155*tmp15&
                   &8*tmp22*tmp26*tmp7)/tmp171 - (128*discbu*logmume*tmp12*tmp14*tmp155*tmp158*tmp22&
                   &*tmp26*tmp7)/tmp171 + (128*discbu*logmume*mm2*tmp155*tmp158*tmp17*tmp22*tmp26*tm&
                   &p7)/(tmp11*tmp171) + (32*discbu*mm2*tmp11*tmp155*tmp180*tmp22*tmp26*tmp7)/tmp171&
                   & + (64*discbulogt*mm2*tmp11*tmp155*tmp180*tmp22*tmp26*tmp7)/tmp171 - (32*discbu*&
                   &mm2*tmp12*tmp155*tmp180*tmp22*tmp26*tmp7)/tmp171 - (64*discbulogt*mm2*tmp12*tmp1&
                   &55*tmp180*tmp22*tmp26*tmp7)/tmp171 + (32*discbu*tmp11*tmp14*tmp155*tmp180*tmp22*&
                   &tmp26*tmp7)/tmp171 + (64*discbulogt*tmp11*tmp14*tmp155*tmp180*tmp22*tmp26*tmp7)/&
                   &tmp171 - (32*discbu*tmp12*tmp14*tmp155*tmp180*tmp22*tmp26*tmp7)/tmp171 - (64*dis&
                   &cbulogt*tmp12*tmp14*tmp155*tmp180*tmp22*tmp26*tmp7)/tmp171 + (32*discbu*mm2*tmp1&
                   &55*tmp17*tmp180*tmp22*tmp26*tmp7)/(tmp11*tmp171) + (64*discbulogt*mm2*tmp155*tmp&
                   &17*tmp180*tmp22*tmp26*tmp7)/(tmp11*tmp171) - (128*discbu*logmume*mm2*tmp155*tmp1&
                   &58*tmp22*tmp26*tmp29*tmp7)/(tmp12*tmp171) - (32*discbu*mm2*tmp155*tmp180*tmp22*t&
                   &mp26*tmp29*tmp7)/(tmp12*tmp171) - (64*discbulogt*mm2*tmp155*tmp180*tmp22*tmp26*t&
                   &mp29*tmp7)/(tmp12*tmp171) + 32*mm2*scalarc0ir6u*tmp11*tmp31*tmp7 - 32*mm2*scalar&
                   &c0ir6u*tmp12*tmp31*tmp7 + 32*scalarc0ir6u*tmp11*tmp14*tmp31*tmp7 - 32*scalarc0ir&
                   &6u*tmp12*tmp14*tmp31*tmp7 + (32*mm2*scalarc0ir6u*tmp17*tmp31*tmp7)/tmp11 + (16*d&
                   &iscbu*logmm*mm2*tmp11*tmp22*tmp26*tmp31*tmp7)/tmp171 - (16*discbu*logmm*mm2*tmp1&
                   &2*tmp22*tmp26*tmp31*tmp7)/tmp171 + (16*discbu*logmm*tmp11*tmp14*tmp22*tmp26*tmp3&
                   &1*tmp7)/tmp171 - (16*discbu*logmm*tmp12*tmp14*tmp22*tmp26*tmp31*tmp7)/tmp171 + (&
                   &16*discbu*logmm*mm2*tmp17*tmp22*tmp26*tmp31*tmp7)/(tmp11*tmp171) - (32*mm2*scala&
                   &rc0ir6u*tmp29*tmp31*tmp7)/tmp12 - (16*discbu*logmm*mm2*tmp22*tmp26*tmp29*tmp31*t&
                   &mp7)/(tmp12*tmp171) + 32*discbu*mm2*tmp11*tmp166*tmp170*tmp31*tmp5*tmp7 - 32*dis&
                   &cbu*mm2*tmp12*tmp166*tmp170*tmp31*tmp5*tmp7 + 32*discbu*tmp11*tmp14*tmp166*tmp17&
                   &0*tmp31*tmp5*tmp7 - 32*discbu*tmp12*tmp14*tmp166*tmp170*tmp31*tmp5*tmp7 + (32*di&
                   &scbu*mm2*tmp166*tmp17*tmp170*tmp31*tmp5*tmp7)/tmp11 - 16*logmm*mm2*tmp11*tmp22*t&
                   &mp26*tmp31*tmp5*tmp7 + 16*discbu*logmm*mm2*tmp11*tmp22*tmp26*tmp31*tmp5*tmp7 + 1&
                   &6*logmm*mm2*tmp12*tmp22*tmp26*tmp31*tmp5*tmp7 - 16*discbu*logmm*mm2*tmp12*tmp22*&
                   &tmp26*tmp31*tmp5*tmp7 - 16*logmm*tmp11*tmp14*tmp22*tmp26*tmp31*tmp5*tmp7 + 16*di&
                   &scbu*logmm*tmp11*tmp14*tmp22*tmp26*tmp31*tmp5*tmp7 + 16*logmm*tmp12*tmp14*tmp22*&
                   &tmp26*tmp31*tmp5*tmp7 - 16*discbu*logmm*tmp12*tmp14*tmp22*tmp26*tmp31*tmp5*tmp7 &
                   &- (16*logmm*mm2*tmp17*tmp22*tmp26*tmp31*tmp5*tmp7)/tmp11 + (16*discbu*logmm*mm2*&
                   &tmp17*tmp22*tmp26*tmp31*tmp5*tmp7)/tmp11 + (16*discbu*logmm*mm2*tmp11*tmp213*tmp&
                   &22*tmp26*tmp31*tmp5*tmp7)/tmp171 - (16*discbu*logmm*mm2*tmp12*tmp213*tmp22*tmp26&
                   &*tmp31*tmp5*tmp7)/tmp171 + (16*discbu*logmm*tmp11*tmp14*tmp213*tmp22*tmp26*tmp31&
                   &*tmp5*tmp7)/tmp171 - (16*discbu*logmm*tmp12*tmp14*tmp213*tmp22*tmp26*tmp31*tmp5*&
                   &tmp7)/tmp171 + (16*discbu*logmm*mm2*tmp17*tmp213*tmp22*tmp26*tmp31*tmp5*tmp7)/(t&
                   &mp11*tmp171) - (32*discbu*mm2*tmp166*tmp170*tmp29*tmp31*tmp5*tmp7)/tmp12 + (16*l&
                   &ogmm*mm2*tmp22*tmp26*tmp29*tmp31*tmp5*tmp7)/tmp12 - (16*discbu*logmm*mm2*tmp22*t&
                   &mp26*tmp29*tmp31*tmp5*tmp7)/tmp12 - (16*discbu*logmm*mm2*tmp213*tmp22*tmp26*tmp2&
                   &9*tmp31*tmp5*tmp7)/(tmp12*tmp171) - 32*scalarc0ir6u*tmp11*tmp31*tmp50*tmp7 - (16&
                   &*discbu*logmm*tmp11*tmp22*tmp26*tmp31*tmp50*tmp7)/tmp171 - 16*discbu*tmp11*tmp16&
                   &6*tmp170*tmp31*tmp5*tmp50*tmp7 + 8*logmm*tmp11*tmp22*tmp26*tmp31*tmp5*tmp50*tmp7&
                   & - 8*discbu*logmm*tmp11*tmp22*tmp26*tmp31*tmp5*tmp50*tmp7 - (8*discbu*logmm*tmp1&
                   &1*tmp213*tmp22*tmp26*tmp31*tmp5*tmp50*tmp7)/tmp171 - (16*discbu*logmm*mm2*tmp11*&
                   &tmp22*tmp31*tmp5*tmp51*tmp7)/tmp171 + (16*discbu*logmm*mm2*tmp12*tmp22*tmp31*tmp&
                   &5*tmp51*tmp7)/tmp171 - (16*discbu*logmm*tmp11*tmp14*tmp22*tmp31*tmp5*tmp51*tmp7)&
                   &/tmp171 + (16*discbu*logmm*tmp12*tmp14*tmp22*tmp31*tmp5*tmp51*tmp7)/tmp171 - (16&
                   &*discbu*logmm*mm2*tmp17*tmp22*tmp31*tmp5*tmp51*tmp7)/(tmp11*tmp171) + (16*discbu&
                   &*logmm*mm2*tmp22*tmp29*tmp31*tmp5*tmp51*tmp7)/(tmp12*tmp171) + (8*discbu*logmm*t&
                   &mp11*tmp22*tmp31*tmp5*tmp50*tmp51*tmp7)/tmp171 - (16*discbu*logmm*mm2*tmp11*tmp2&
                   &6*tmp31*tmp5*tmp52*tmp7)/tmp171 + (16*discbu*logmm*mm2*tmp12*tmp26*tmp31*tmp5*tm&
                   &p52*tmp7)/tmp171 - (16*discbu*logmm*tmp11*tmp14*tmp26*tmp31*tmp5*tmp52*tmp7)/tmp&
                   &171 + (16*discbu*logmm*tmp12*tmp14*tmp26*tmp31*tmp5*tmp52*tmp7)/tmp171 - (16*dis&
                   &cbu*logmm*mm2*tmp17*tmp26*tmp31*tmp5*tmp52*tmp7)/(tmp11*tmp171) + (16*discbu*log&
                   &mm*mm2*tmp26*tmp29*tmp31*tmp5*tmp52*tmp7)/(tmp12*tmp171) + (8*discbu*logmm*tmp11&
                   &*tmp26*tmp31*tmp5*tmp50*tmp52*tmp7)/tmp171 + 32*scalarc0ir6u*tmp12*tmp31*tmp65*t&
                   &mp7 + (16*discbu*logmm*tmp12*tmp22*tmp26*tmp31*tmp65*tmp7)/tmp171 + 16*discbu*tm&
                   &p12*tmp166*tmp170*tmp31*tmp5*tmp65*tmp7 - 8*logmm*tmp12*tmp22*tmp26*tmp31*tmp5*t&
                   &mp65*tmp7 + 8*discbu*logmm*tmp12*tmp22*tmp26*tmp31*tmp5*tmp65*tmp7 + (8*discbu*l&
                   &ogmm*tmp12*tmp213*tmp22*tmp26*tmp31*tmp5*tmp65*tmp7)/tmp171 - (8*discbu*logmm*tm&
                   &p12*tmp22*tmp31*tmp5*tmp51*tmp65*tmp7)/tmp171 - (8*discbu*logmm*tmp12*tmp26*tmp3&
                   &1*tmp5*tmp52*tmp65*tmp7)/tmp171 + 16*discbs*tmp11*tmp30*tmp31*tmp50*tmp67*tmp70*&
                   &tmp74 - 16*discbs*tmp12*tmp30*tmp31*tmp65*tmp67*tmp70*tmp74 - 16*scalarc0ir6s*tm&
                   &p12*tmp31*tmp65*tmp67*tmp75*tmp76 + 16*logmm*mm2*tmp11*tmp171*tmp212*tmp31*tmp5*&
                   &tmp77 - 16*logmm*mm2*tmp12*tmp171*tmp212*tmp31*tmp5*tmp77 + 16*logmm*tmp11*tmp14&
                   &*tmp171*tmp212*tmp31*tmp5*tmp77 - 16*logmm*tmp12*tmp14*tmp171*tmp212*tmp31*tmp5*&
                   &tmp77 + (16*logmm*mm2*tmp17*tmp171*tmp212*tmp31*tmp5*tmp77)/tmp11 - (16*logmm*mm&
                   &2*tmp171*tmp212*tmp29*tmp31*tmp5*tmp77)/tmp12 - 8*logmm*tmp11*tmp171*tmp212*tmp3&
                   &1*tmp5*tmp50*tmp77 + 8*logmm*tmp12*tmp171*tmp212*tmp31*tmp5*tmp65*tmp77 + (discb&
                   &s*tmp12*tmp140*tmp155*tmp163*tmp33*tmp35)/tmp78 + (discbs*tmp140*tmp155*tmp163*t&
                   &mp29*tmp33*tmp35)/(tmp12*tmp78) + (128*discbs*logmume*mm2*tmp11*tmp155*tmp158*tm&
                   &p30*tmp33*tmp35)/tmp78 - (128*discbs*logmume*mm2*tmp12*tmp155*tmp158*tmp30*tmp33&
                   &*tmp35)/tmp78 + (128*discbs*logmume*tmp11*tmp14*tmp155*tmp158*tmp30*tmp33*tmp35)&
                   &/tmp78 - (128*discbs*logmume*tmp12*tmp14*tmp155*tmp158*tmp30*tmp33*tmp35)/tmp78 &
                   &+ (32*discbs*mm2*tmp11*tmp155*tmp163*tmp30*tmp33*tmp35)/tmp78 + (64*discbslogt*m&
                   &m2*tmp11*tmp155*tmp163*tmp30*tmp33*tmp35)/tmp78 - (64*discbslogt*mm2*tmp12*tmp15&
                   &5*tmp163*tmp30*tmp33*tmp35)/tmp78 + (32*discbs*tmp11*tmp14*tmp155*tmp163*tmp30*t&
                   &mp33*tmp35)/tmp78 + (64*discbslogt*tmp11*tmp14*tmp155*tmp163*tmp30*tmp33*tmp35)/&
                   &tmp78 - (32*discbs*tmp12*tmp14*tmp155*tmp163*tmp30*tmp33*tmp35)/tmp78 - (64*disc&
                   &bslogt*tmp12*tmp14*tmp155*tmp163*tmp30*tmp33*tmp35)/tmp78 + (128*discbs*logmume*&
                   &mm2*tmp155*tmp158*tmp17*tmp30*tmp33*tmp35)/(tmp11*tmp78) + (32*discbs*mm2*tmp155&
                   &*tmp163*tmp17*tmp30*tmp33*tmp35)/(tmp11*tmp78) + (64*discbslogt*mm2*tmp155*tmp16&
                   &3*tmp17*tmp30*tmp33*tmp35)/(tmp11*tmp78) - (128*discbs*logmume*mm2*tmp155*tmp158&
                   &*tmp29*tmp30*tmp33*tmp35)/(tmp12*tmp78) - (64*discbslogt*mm2*tmp155*tmp163*tmp29&
                   &*tmp30*tmp33*tmp35)/(tmp12*tmp78) - (16*discbs*logmm*mm2*tmp11*tmp30*tmp31*tmp47&
                   &)/tmp78 + (16*discbs*logmm*mm2*tmp12*tmp30*tmp31*tmp47)/tmp78 - (16*discbs*logmm&
                   &*tmp11*tmp14*tmp30*tmp31*tmp47)/tmp78 + (16*discbs*logmm*tmp12*tmp14*tmp30*tmp31&
                   &*tmp47)/tmp78 - (16*discbs*logmm*mm2*tmp17*tmp30*tmp31*tmp47)/(tmp11*tmp78) + (1&
                   &6*discbs*logmm*mm2*tmp29*tmp30*tmp31*tmp47)/(tmp12*tmp78) + (16*discbs*logmm*tmp&
                   &11*tmp30*tmp31*tmp47*tmp50)/tmp78 - (16*discbs*logmm*tmp12*tmp30*tmp31*tmp47*tmp&
                   &65)/tmp78 - (8*discbs*logmm*tmp11*tmp31*tmp47*tmp50*tmp67)/tmp78 + (8*discbs*log&
                   &mm*tmp12*tmp31*tmp47*tmp65*tmp67)/tmp78 - 16*logmm*mm2*tmp11*tmp117*tmp119*tmp15&
                   &1*tmp188*tmp31*tmp78 + 16*logmm*mm2*tmp117*tmp119*tmp12*tmp151*tmp188*tmp31*tmp7&
                   &8 - 16*logmm*tmp11*tmp117*tmp119*tmp14*tmp151*tmp188*tmp31*tmp78 + 16*logmm*tmp1&
                   &17*tmp119*tmp12*tmp14*tmp151*tmp188*tmp31*tmp78 - (16*logmm*mm2*tmp117*tmp119*tm&
                   &p151*tmp17*tmp188*tmp31*tmp78)/tmp11 + (16*logmm*mm2*tmp117*tmp119*tmp151*tmp188&
                   &*tmp29*tmp31*tmp78)/tmp12 - 16*tmp11*tmp231*tmp31*tmp33*tmp35*tmp50*tmp78 + 16*t&
                   &mp12*tmp231*tmp31*tmp33*tmp35*tmp65*tmp78 - 32*discbu*mm2*tmp11*tmp204*tmp249*tm&
                   &p8 + 32*discbu*mm2*tmp12*tmp204*tmp249*tmp8 - 32*discbu*tmp11*tmp14*tmp204*tmp24&
                   &9*tmp8 + 32*discbu*tmp12*tmp14*tmp204*tmp249*tmp8 - (32*discbu*mm2*tmp17*tmp204*&
                   &tmp249*tmp8)/tmp11 + (64*discbu*logmume*mm2*tmp11*tmp158*tmp22*tmp26*tmp8)/tmp17&
                   &1 - (64*discbu*logmume*mm2*tmp12*tmp158*tmp22*tmp26*tmp8)/tmp171 + (64*discbu*lo&
                   &gmume*tmp11*tmp14*tmp158*tmp22*tmp26*tmp8)/tmp171 - (64*discbu*logmume*tmp12*tmp&
                   &14*tmp158*tmp22*tmp26*tmp8)/tmp171 + (64*discbu*logmume*mm2*tmp158*tmp17*tmp22*t&
                   &mp26*tmp8)/(tmp11*tmp171) + (32*discbulogt*mm2*tmp11*tmp180*tmp22*tmp26*tmp8)/tm&
                   &p171 - (32*discbulogt*mm2*tmp12*tmp180*tmp22*tmp26*tmp8)/tmp171 + (32*discbulogt&
                   &*tmp11*tmp14*tmp180*tmp22*tmp26*tmp8)/tmp171 - (32*discbulogt*tmp12*tmp14*tmp180&
                   &*tmp22*tmp26*tmp8)/tmp171 + (32*discbulogt*mm2*tmp17*tmp180*tmp22*tmp26*tmp8)/(t&
                   &mp11*tmp171) + (32*discbu*mm2*tmp204*tmp249*tmp29*tmp8)/tmp12 - (64*discbu*logmu&
                   &me*mm2*tmp158*tmp22*tmp26*tmp29*tmp8)/(tmp12*tmp171) - (32*discbulogt*mm2*tmp180&
                   &*tmp22*tmp26*tmp29*tmp8)/(tmp12*tmp171) + 32*discbs*mm2*tmp11*tmp231*tmp33*tmp35&
                   &*tmp8 - 32*discbs*mm2*tmp12*tmp231*tmp33*tmp35*tmp8 + 32*discbs*tmp11*tmp14*tmp2&
                   &31*tmp33*tmp35*tmp8 - 32*discbs*tmp12*tmp14*tmp231*tmp33*tmp35*tmp8 + (32*discbs&
                   &*mm2*tmp17*tmp231*tmp33*tmp35*tmp8)/tmp11 - (32*discbs*mm2*tmp231*tmp29*tmp33*tm&
                   &p35*tmp8)/tmp12 - (32*discbu*logmume*tmp11*tmp158*tmp22*tmp26*tmp50*tmp8)/tmp171&
                   & - (16*discbulogt*tmp11*tmp180*tmp22*tmp26*tmp50*tmp8)/tmp171 - 32*logmume*tmp11&
                   &*tmp158*tmp30*tmp33*tmp35*tmp50*tmp8 + 32*discbs*logmume*tmp11*tmp158*tmp30*tmp3&
                   &3*tmp35*tmp50*tmp8 + 16*discbslogt*tmp11*tmp163*tmp30*tmp33*tmp35*tmp50*tmp8 - 1&
                   &6*logt*tmp11*tmp163*tmp30*tmp33*tmp35*tmp50*tmp8 + 32*logbbemm*mm2*tmp11*tmp151*&
                   &tmp154*tmp58*tmp8 - 64*logbemm*mm2*tmp11*tmp151*tmp154*tmp58*tmp8 + 32*mm2*scala&
                   &rc0tmm*tmp101*tmp11*tmp151*tmp154*tmp58*tmp8 - 32*logbbemm*mm2*tmp12*tmp151*tmp1&
                   &54*tmp58*tmp8 + 64*logbemm*mm2*tmp12*tmp151*tmp154*tmp58*tmp8 - 32*mm2*scalarc0t&
                   &mm*tmp101*tmp12*tmp151*tmp154*tmp58*tmp8 + 32*logbbemm*tmp11*tmp14*tmp151*tmp154&
                   &*tmp58*tmp8 - 64*logbemm*tmp11*tmp14*tmp151*tmp154*tmp58*tmp8 + 32*scalarc0tmm*t&
                   &mp101*tmp11*tmp14*tmp151*tmp154*tmp58*tmp8 - 32*logbbemm*tmp12*tmp14*tmp151*tmp1&
                   &54*tmp58*tmp8 + 64*logbemm*tmp12*tmp14*tmp151*tmp154*tmp58*tmp8 - 32*scalarc0tmm&
                   &*tmp101*tmp12*tmp14*tmp151*tmp154*tmp58*tmp8 + (32*logbbemm*mm2*tmp151*tmp154*tm&
                   &p17*tmp58*tmp8)/tmp11 - (64*logbemm*mm2*tmp151*tmp154*tmp17*tmp58*tmp8)/tmp11 + &
                   &(32*mm2*scalarc0tmm*tmp101*tmp151*tmp154*tmp17*tmp58*tmp8)/tmp11 - (32*logbbemm*&
                   &mm2*tmp151*tmp154*tmp29*tmp58*tmp8)/tmp12 + (64*logbemm*mm2*tmp151*tmp154*tmp29*&
                   &tmp58*tmp8)/tmp12 - (32*mm2*scalarc0tmm*tmp101*tmp151*tmp154*tmp29*tmp58*tmp8)/t&
                   &mp12 - 32*mm2*scalarc0tme*tmp11*tmp149*tmp55*tmp58*tmp8 + 32*mm2*scalarc0tme*tmp&
                   &12*tmp149*tmp55*tmp58*tmp8 - 32*scalarc0tme*tmp11*tmp14*tmp149*tmp55*tmp58*tmp8 &
                   &+ 32*scalarc0tme*tmp12*tmp14*tmp149*tmp55*tmp58*tmp8 - (32*mm2*scalarc0tme*tmp14&
                   &9*tmp17*tmp55*tmp58*tmp8)/tmp11 + (32*mm2*scalarc0tme*tmp149*tmp29*tmp55*tmp58*t&
                   &mp8)/tmp12 - 32*mm2*tmp11*tmp55*tmp56*tmp58*tmp59*tmp63*tmp8 - 32*logt*mm2*tmp11&
                   &*tmp55*tmp56*tmp58*tmp59*tmp63*tmp8 + 32*mm2*tmp12*tmp55*tmp56*tmp58*tmp59*tmp63&
                   &*tmp8 + 32*logt*mm2*tmp12*tmp55*tmp56*tmp58*tmp59*tmp63*tmp8 - 32*tmp11*tmp14*tm&
                   &p55*tmp56*tmp58*tmp59*tmp63*tmp8 - 32*logt*tmp11*tmp14*tmp55*tmp56*tmp58*tmp59*t&
                   &mp63*tmp8 + 32*tmp12*tmp14*tmp55*tmp56*tmp58*tmp59*tmp63*tmp8 + 32*logt*tmp12*tm&
                   &p14*tmp55*tmp56*tmp58*tmp59*tmp63*tmp8 - (32*mm2*tmp17*tmp55*tmp56*tmp58*tmp59*t&
                   &mp63*tmp8)/tmp11 - (32*logt*mm2*tmp17*tmp55*tmp56*tmp58*tmp59*tmp63*tmp8)/tmp11 &
                   &+ (32*mm2*tmp29*tmp55*tmp56*tmp58*tmp59*tmp63*tmp8)/tmp12 + (32*logt*mm2*tmp29*t&
                   &mp55*tmp56*tmp58*tmp59*tmp63*tmp8)/tmp12 + 32*logbbeme*mm2*tmp11*tmp149*tmp58*tm&
                   &p64*tmp8 - 64*logbeme*mm2*tmp11*tmp149*tmp58*tmp64*tmp8 - 32*logbbeme*mm2*tmp12*&
                   &tmp149*tmp58*tmp64*tmp8 + 64*logbeme*mm2*tmp12*tmp149*tmp58*tmp64*tmp8 + 32*logb&
                   &beme*tmp11*tmp14*tmp149*tmp58*tmp64*tmp8 - 64*logbeme*tmp11*tmp14*tmp149*tmp58*t&
                   &mp64*tmp8 - 32*logbbeme*tmp12*tmp14*tmp149*tmp58*tmp64*tmp8 + 64*logbeme*tmp12*t&
                   &mp14*tmp149*tmp58*tmp64*tmp8 + 32*mm2*scalarc0tme*tmp11*tmp149*tmp150*tmp58*tmp6&
                   &4*tmp8 - 32*mm2*scalarc0tme*tmp12*tmp149*tmp150*tmp58*tmp64*tmp8 + 32*scalarc0tm&
                   &e*tmp11*tmp14*tmp149*tmp150*tmp58*tmp64*tmp8 - 32*scalarc0tme*tmp12*tmp14*tmp149&
                   &*tmp150*tmp58*tmp64*tmp8 + (32*logbbeme*mm2*tmp149*tmp17*tmp58*tmp64*tmp8)/tmp11&
                   & - (64*logbeme*mm2*tmp149*tmp17*tmp58*tmp64*tmp8)/tmp11 + (32*mm2*scalarc0tme*tm&
                   &p149*tmp150*tmp17*tmp58*tmp64*tmp8)/tmp11 - (32*logbbeme*mm2*tmp149*tmp29*tmp58*&
                   &tmp64*tmp8)/tmp12 + (64*logbeme*mm2*tmp149*tmp29*tmp58*tmp64*tmp8)/tmp12 - (32*m&
                   &m2*scalarc0tme*tmp149*tmp150*tmp29*tmp58*tmp64*tmp8)/tmp12 + (32*discbu*logmume*&
                   &tmp12*tmp158*tmp22*tmp26*tmp65*tmp8)/tmp171 + (16*discbulogt*tmp12*tmp180*tmp22*&
                   &tmp26*tmp65*tmp8)/tmp171 + 32*logmume*tmp12*tmp158*tmp30*tmp33*tmp35*tmp65*tmp8 &
                   &- 32*discbs*logmume*tmp12*tmp158*tmp30*tmp33*tmp35*tmp65*tmp8 - 16*discbslogt*tm&
                   &p12*tmp163*tmp30*tmp33*tmp35*tmp65*tmp8 + 16*logt*tmp12*tmp163*tmp30*tmp33*tmp35&
                   &*tmp65*tmp8 + scalarc0ir6s*tmp12*tmp140*tmp67*tmp8 + (scalarc0ir6s*tmp140*tmp29*&
                   &tmp67*tmp8)/tmp12 + 32*mm2*scalarc0ir6s*tmp11*tmp30*tmp67*tmp8 + 32*scalarc0ir6s&
                   &*tmp11*tmp14*tmp30*tmp67*tmp8 - 32*scalarc0ir6s*tmp12*tmp14*tmp30*tmp67*tmp8 + (&
                   &32*mm2*scalarc0ir6s*tmp17*tmp30*tmp67*tmp8)/tmp11 - 64*logmume*mm2*tmp11*tmp158*&
                   &tmp22*tmp26*tmp7*tmp8 + 64*discbu*logmume*mm2*tmp11*tmp158*tmp22*tmp26*tmp7*tmp8&
                   & + 64*logmume*mm2*tmp12*tmp158*tmp22*tmp26*tmp7*tmp8 - 64*discbu*logmume*mm2*tmp&
                   &12*tmp158*tmp22*tmp26*tmp7*tmp8 - 64*logmume*tmp11*tmp14*tmp158*tmp22*tmp26*tmp7&
                   &*tmp8 + 64*discbu*logmume*tmp11*tmp14*tmp158*tmp22*tmp26*tmp7*tmp8 + 64*logmume*&
                   &tmp12*tmp14*tmp158*tmp22*tmp26*tmp7*tmp8 - 64*discbu*logmume*tmp12*tmp14*tmp158*&
                   &tmp22*tmp26*tmp7*tmp8 - (64*logmume*mm2*tmp158*tmp17*tmp22*tmp26*tmp7*tmp8)/tmp1&
                   &1 + (64*discbu*logmume*mm2*tmp158*tmp17*tmp22*tmp26*tmp7*tmp8)/tmp11 - (64*discb&
                   &u*logmume*mm2*tmp102*tmp11*tmp22*tmp26*tmp7*tmp8)/tmp171 - (32*discbulogt*mm2*tm&
                   &p108*tmp11*tmp22*tmp26*tmp7*tmp8)/tmp171 + (64*discbu*logmume*mm2*tmp102*tmp12*t&
                   &mp22*tmp26*tmp7*tmp8)/tmp171 + (32*discbulogt*mm2*tmp108*tmp12*tmp22*tmp26*tmp7*&
                   &tmp8)/tmp171 - (64*discbu*logmume*tmp102*tmp11*tmp14*tmp22*tmp26*tmp7*tmp8)/tmp1&
                   &71 - (32*discbulogt*tmp108*tmp11*tmp14*tmp22*tmp26*tmp7*tmp8)/tmp171 + (64*discb&
                   &u*logmume*tmp102*tmp12*tmp14*tmp22*tmp26*tmp7*tmp8)/tmp171 + (32*discbulogt*tmp1&
                   &08*tmp12*tmp14*tmp22*tmp26*tmp7*tmp8)/tmp171 - (64*discbu*logmume*mm2*tmp102*tmp&
                   &17*tmp22*tmp26*tmp7*tmp8)/(tmp11*tmp171) - (32*discbulogt*mm2*tmp108*tmp17*tmp22&
                   &*tmp26*tmp7*tmp8)/(tmp11*tmp171) + 32*discbulogt*mm2*tmp11*tmp180*tmp22*tmp26*tm&
                   &p7*tmp8 - 32*logt*mm2*tmp11*tmp180*tmp22*tmp26*tmp7*tmp8 - 32*discbulogt*mm2*tmp&
                   &12*tmp180*tmp22*tmp26*tmp7*tmp8 + 32*logt*mm2*tmp12*tmp180*tmp22*tmp26*tmp7*tmp8&
                   & + 32*discbulogt*tmp11*tmp14*tmp180*tmp22*tmp26*tmp7*tmp8 - 32*logt*tmp11*tmp14*&
                   &tmp180*tmp22*tmp26*tmp7*tmp8 - 32*discbulogt*tmp12*tmp14*tmp180*tmp22*tmp26*tmp7&
                   &*tmp8 + 32*logt*tmp12*tmp14*tmp180*tmp22*tmp26*tmp7*tmp8 + (32*discbulogt*mm2*tm&
                   &p17*tmp180*tmp22*tmp26*tmp7*tmp8)/tmp11 - (32*logt*mm2*tmp17*tmp180*tmp22*tmp26*&
                   &tmp7*tmp8)/tmp11 + (64*discbu*logmume*mm2*tmp11*tmp158*tmp213*tmp22*tmp26*tmp7*t&
                   &mp8)/tmp171 - (64*discbu*logmume*mm2*tmp12*tmp158*tmp213*tmp22*tmp26*tmp7*tmp8)/&
                   &tmp171 + (64*discbu*logmume*tmp11*tmp14*tmp158*tmp213*tmp22*tmp26*tmp7*tmp8)/tmp&
                   &171 - (64*discbu*logmume*tmp12*tmp14*tmp158*tmp213*tmp22*tmp26*tmp7*tmp8)/tmp171&
                   & + (64*discbu*logmume*mm2*tmp158*tmp17*tmp213*tmp22*tmp26*tmp7*tmp8)/(tmp11*tmp1&
                   &71) + (32*discbulogt*mm2*tmp11*tmp180*tmp213*tmp22*tmp26*tmp7*tmp8)/tmp171 - (32&
                   &*discbulogt*mm2*tmp12*tmp180*tmp213*tmp22*tmp26*tmp7*tmp8)/tmp171 + (32*discbulo&
                   &gt*tmp11*tmp14*tmp180*tmp213*tmp22*tmp26*tmp7*tmp8)/tmp171 - (32*discbulogt*tmp1&
                   &2*tmp14*tmp180*tmp213*tmp22*tmp26*tmp7*tmp8)/tmp171 + (32*discbulogt*mm2*tmp17*t&
                   &mp180*tmp213*tmp22*tmp26*tmp7*tmp8)/(tmp11*tmp171) + (64*logmume*mm2*tmp158*tmp2&
                   &2*tmp26*tmp29*tmp7*tmp8)/tmp12 - (64*discbu*logmume*mm2*tmp158*tmp22*tmp26*tmp29&
                   &*tmp7*tmp8)/tmp12 + (64*discbu*logmume*mm2*tmp102*tmp22*tmp26*tmp29*tmp7*tmp8)/(&
                   &tmp12*tmp171) + (32*discbulogt*mm2*tmp108*tmp22*tmp26*tmp29*tmp7*tmp8)/(tmp12*tm&
                   &p171) - (32*discbulogt*mm2*tmp180*tmp22*tmp26*tmp29*tmp7*tmp8)/tmp12 + (32*logt*&
                   &mm2*tmp180*tmp22*tmp26*tmp29*tmp7*tmp8)/tmp12 - (64*discbu*logmume*mm2*tmp158*tm&
                   &p213*tmp22*tmp26*tmp29*tmp7*tmp8)/(tmp12*tmp171) - (32*discbulogt*mm2*tmp180*tmp&
                   &213*tmp22*tmp26*tmp29*tmp7*tmp8)/(tmp12*tmp171) + 32*mm2*scalarc0ir6u*tmp11*tmp5&
                   &*tmp7*tmp8 - 32*mm2*scalarc0ir6u*tmp12*tmp5*tmp7*tmp8 + 32*scalarc0ir6u*tmp11*tm&
                   &p14*tmp5*tmp7*tmp8 - 32*scalarc0ir6u*tmp12*tmp14*tmp5*tmp7*tmp8 + (32*mm2*scalar&
                   &c0ir6u*tmp17*tmp5*tmp7*tmp8)/tmp11 + (16*discbu*logmm*mm2*tmp11*tmp22*tmp26*tmp5&
                   &*tmp7*tmp8)/tmp171 - (16*discbu*logmm*mm2*tmp12*tmp22*tmp26*tmp5*tmp7*tmp8)/tmp1&
                   &71 + (16*discbu*logmm*tmp11*tmp14*tmp22*tmp26*tmp5*tmp7*tmp8)/tmp171 - (16*discb&
                   &u*logmm*tmp12*tmp14*tmp22*tmp26*tmp5*tmp7*tmp8)/tmp171 + (16*discbu*logmm*mm2*tm&
                   &p17*tmp22*tmp26*tmp5*tmp7*tmp8)/(tmp11*tmp171) - (32*mm2*scalarc0ir6u*tmp29*tmp5&
                   &*tmp7*tmp8)/tmp12 - (16*discbu*logmm*mm2*tmp22*tmp26*tmp29*tmp5*tmp7*tmp8)/(tmp1&
                   &2*tmp171) + 32*logmume*tmp11*tmp158*tmp22*tmp26*tmp50*tmp7*tmp8 - 32*discbu*logm&
                   &ume*tmp11*tmp158*tmp22*tmp26*tmp50*tmp7*tmp8 + (16*discbulogt*tmp104*tmp11*tmp22&
                   &*tmp26*tmp50*tmp7*tmp8)/tmp171 - 16*discbulogt*tmp11*tmp180*tmp22*tmp26*tmp50*tm&
                   &p7*tmp8 + 16*logt*tmp11*tmp180*tmp22*tmp26*tmp50*tmp7*tmp8 - (32*discbu*logmume*&
                   &tmp11*tmp158*tmp213*tmp22*tmp26*tmp50*tmp7*tmp8)/tmp171 - (16*discbulogt*tmp11*t&
                   &mp180*tmp213*tmp22*tmp26*tmp50*tmp7*tmp8)/tmp171 - (64*discbu*logmume*mm2*tmp11*&
                   &tmp158*tmp22*tmp51*tmp7*tmp8)/tmp171 + (64*discbu*logmume*mm2*tmp12*tmp158*tmp22&
                   &*tmp51*tmp7*tmp8)/tmp171 - (64*discbu*logmume*tmp11*tmp14*tmp158*tmp22*tmp51*tmp&
                   &7*tmp8)/tmp171 + (64*discbu*logmume*tmp12*tmp14*tmp158*tmp22*tmp51*tmp7*tmp8)/tm&
                   &p171 - (64*discbu*logmume*mm2*tmp158*tmp17*tmp22*tmp51*tmp7*tmp8)/(tmp11*tmp171)&
                   & - (32*discbulogt*mm2*tmp11*tmp180*tmp22*tmp51*tmp7*tmp8)/tmp171 + (32*discbulog&
                   &t*mm2*tmp12*tmp180*tmp22*tmp51*tmp7*tmp8)/tmp171 - (32*discbulogt*tmp11*tmp14*tm&
                   &p180*tmp22*tmp51*tmp7*tmp8)/tmp171 + (32*discbulogt*tmp12*tmp14*tmp180*tmp22*tmp&
                   &51*tmp7*tmp8)/tmp171 - (32*discbulogt*mm2*tmp17*tmp180*tmp22*tmp51*tmp7*tmp8)/(t&
                   &mp11*tmp171) + (64*discbu*logmume*mm2*tmp158*tmp22*tmp29*tmp51*tmp7*tmp8)/(tmp12&
                   &*tmp171) + (32*discbulogt*mm2*tmp180*tmp22*tmp29*tmp51*tmp7*tmp8)/(tmp12*tmp171)&
                   & + (32*discbu*logmume*tmp11*tmp158*tmp22*tmp50*tmp51*tmp7*tmp8)/tmp171 + (16*dis&
                   &cbulogt*tmp11*tmp180*tmp22*tmp50*tmp51*tmp7*tmp8)/tmp171 - (64*discbu*logmume*mm&
                   &2*tmp11*tmp158*tmp26*tmp52*tmp7*tmp8)/tmp171 + (64*discbu*logmume*mm2*tmp12*tmp1&
                   &58*tmp26*tmp52*tmp7*tmp8)/tmp171 - (64*discbu*logmume*tmp11*tmp14*tmp158*tmp26*t&
                   &mp52*tmp7*tmp8)/tmp171 + (64*discbu*logmume*tmp12*tmp14*tmp158*tmp26*tmp52*tmp7*&
                   &tmp8)/tmp171 - (64*discbu*logmume*mm2*tmp158*tmp17*tmp26*tmp52*tmp7*tmp8)/(tmp11&
                   &*tmp171) - (32*discbulogt*mm2*tmp11*tmp180*tmp26*tmp52*tmp7*tmp8)/tmp171 + (32*d&
                   &iscbulogt*mm2*tmp12*tmp180*tmp26*tmp52*tmp7*tmp8)/tmp171 - (32*discbulogt*tmp11*&
                   &tmp14*tmp180*tmp26*tmp52*tmp7*tmp8)/tmp171 + (32*discbulogt*tmp12*tmp14*tmp180*t&
                   &mp26*tmp52*tmp7*tmp8)/tmp171 - (32*discbulogt*mm2*tmp17*tmp180*tmp26*tmp52*tmp7*&
                   &tmp8)/(tmp11*tmp171) + (64*discbu*logmume*mm2*tmp158*tmp26*tmp29*tmp52*tmp7*tmp8&
                   &)/(tmp12*tmp171) + (32*discbulogt*mm2*tmp180*tmp26*tmp29*tmp52*tmp7*tmp8)/(tmp12&
                   &*tmp171) + (32*discbu*logmume*tmp11*tmp158*tmp26*tmp50*tmp52*tmp7*tmp8)/tmp171 +&
                   & (16*discbulogt*tmp11*tmp180*tmp26*tmp50*tmp52*tmp7*tmp8)/tmp171 - 32*logmume*tm&
                   &p12*tmp158*tmp22*tmp26*tmp65*tmp7*tmp8 + 32*discbu*logmume*tmp12*tmp158*tmp22*tm&
                   &p26*tmp65*tmp7*tmp8 - (16*discbulogt*tmp104*tmp12*tmp22*tmp26*tmp65*tmp7*tmp8)/t&
                   &mp171 + 16*discbulogt*tmp12*tmp180*tmp22*tmp26*tmp65*tmp7*tmp8 - 16*logt*tmp12*t&
                   &mp180*tmp22*tmp26*tmp65*tmp7*tmp8 + (32*discbu*logmume*tmp12*tmp158*tmp213*tmp22&
                   &*tmp26*tmp65*tmp7*tmp8)/tmp171 + (16*discbulogt*tmp12*tmp180*tmp213*tmp22*tmp26*&
                   &tmp65*tmp7*tmp8)/tmp171 - (32*discbu*logmume*tmp12*tmp158*tmp22*tmp51*tmp65*tmp7&
                   &*tmp8)/tmp171 - (16*discbulogt*tmp12*tmp180*tmp22*tmp51*tmp65*tmp7*tmp8)/tmp171 &
                   &- (32*discbu*logmume*tmp12*tmp158*tmp26*tmp52*tmp65*tmp7*tmp8)/tmp171 - (16*disc&
                   &bulogt*tmp12*tmp180*tmp26*tmp52*tmp65*tmp7*tmp8)/tmp171 + (discbslogt*tmp102*tmp&
                   &11*tmp140*tmp33*tmp35*tmp8)/tmp78 + (discbslogt*tmp102*tmp140*tmp17*tmp33*tmp35*&
                   &tmp8)/(tmp11*tmp78) - (64*discbs*logmume*mm2*tmp102*tmp11*tmp30*tmp33*tmp35*tmp8&
                   &)/tmp78 + (32*discbslogt*mm2*tmp102*tmp12*tmp30*tmp33*tmp35*tmp8)/tmp78 + (64*di&
                   &scbs*logmume*mm2*tmp102*tmp12*tmp30*tmp33*tmp35*tmp8)/tmp78 - (32*discbslogt*tmp&
                   &102*tmp11*tmp14*tmp30*tmp33*tmp35*tmp8)/tmp78 - (64*discbs*logmume*tmp102*tmp11*&
                   &tmp14*tmp30*tmp33*tmp35*tmp8)/tmp78 + (32*discbslogt*tmp102*tmp12*tmp14*tmp30*tm&
                   &p33*tmp35*tmp8)/tmp78 + (64*discbs*logmume*tmp102*tmp12*tmp14*tmp30*tmp33*tmp35*&
                   &tmp8)/tmp78 - (64*discbs*logmume*mm2*tmp102*tmp17*tmp30*tmp33*tmp35*tmp8)/(tmp11&
                   &*tmp78) + (32*discbslogt*mm2*tmp102*tmp29*tmp30*tmp33*tmp35*tmp8)/(tmp12*tmp78) &
                   &+ (64*discbs*logmume*mm2*tmp102*tmp29*tmp30*tmp33*tmp35*tmp8)/(tmp12*tmp78) + (3&
                   &2*discbs*logmume*tmp11*tmp158*tmp160*tmp30*tmp33*tmp50*tmp8)/tmp78 + (16*discbsl&
                   &ogt*tmp11*tmp160*tmp163*tmp30*tmp33*tmp50*tmp8)/tmp78 + (32*discbs*logmume*tmp11&
                   &*tmp158*tmp161*tmp30*tmp35*tmp50*tmp8)/tmp78 + (16*discbslogt*tmp11*tmp161*tmp16&
                   &3*tmp30*tmp35*tmp50*tmp8)/tmp78 - (32*discbs*logmume*tmp11*tmp158*tmp33*tmp35*tm&
                   &p50*tmp8)/tmp78 - (16*discbslogt*tmp11*tmp163*tmp33*tmp35*tmp50*tmp8)/tmp78 - (3&
                   &2*discbs*logmume*tmp12*tmp158*tmp160*tmp30*tmp33*tmp65*tmp8)/tmp78 - (16*discbsl&
                   &ogt*tmp12*tmp160*tmp163*tmp30*tmp33*tmp65*tmp8)/tmp78 - (32*discbs*logmume*tmp12&
                   &*tmp158*tmp161*tmp30*tmp35*tmp65*tmp8)/tmp78 - (16*discbslogt*tmp12*tmp161*tmp16&
                   &3*tmp30*tmp35*tmp65*tmp8)/tmp78 + (32*discbs*logmume*tmp12*tmp158*tmp33*tmp35*tm&
                   &p65*tmp8)/tmp78 + (16*discbslogt*tmp12*tmp163*tmp33*tmp35*tmp65*tmp8)/tmp78 + (1&
                   &6*discbs*logmm*mm2*tmp11*tmp30*tmp47*tmp67*tmp8)/tmp78 - (16*discbs*logmm*mm2*tm&
                   &p12*tmp30*tmp47*tmp67*tmp8)/tmp78 + (16*discbs*logmm*tmp11*tmp14*tmp30*tmp47*tmp&
                   &67*tmp8)/tmp78 - (16*discbs*logmm*tmp12*tmp14*tmp30*tmp47*tmp67*tmp8)/tmp78 + (1&
                   &6*discbs*logmm*mm2*tmp17*tmp30*tmp47*tmp67*tmp8)/(tmp11*tmp78) - (16*discbs*logm&
                   &m*mm2*tmp29*tmp30*tmp47*tmp67*tmp8)/(tmp12*tmp78) + (8*discbs*logmm*tmp12*tmp30*&
                   &tmp31*tmp65*tmp67*tmp81*tmp82)/tmp78 + 16*scalarc0ir6s*tmp11*tmp31*tmp67*tmp76*t&
                   &mp84 + 8*logmm*tmp11*tmp30*tmp31*tmp67*tmp77*tmp78*tmp84 + 16*discbs*tmp11*tmp23&
                   &1*tmp31*tmp33*tmp35*tmp50*tmp85 - 16*discbs*tmp12*tmp231*tmp31*tmp33*tmp35*tmp65&
                   &*tmp85 + (8*discbs*logmm*tmp11*tmp30*tmp31*tmp47*tmp50*tmp67*tmp85)/tmp78 + (32*&
                   &discbs*logmume*tmp11*tmp158*tmp30*tmp33*tmp35*tmp50*tmp8*tmp85)/tmp78 + (16*disc&
                   &bslogt*tmp11*tmp163*tmp30*tmp33*tmp35*tmp50*tmp8*tmp85)/tmp78 - (32*discbs*logmu&
                   &me*tmp12*tmp158*tmp30*tmp33*tmp35*tmp65*tmp8*tmp85)/tmp78 - (16*discbslogt*tmp12&
                   &*tmp163*tmp30*tmp33*tmp35*tmp65*tmp8*tmp85)/tmp78 + 32*mm2*scalarc0tme*tmp11*tmp&
                   &31*tmp55*tmp58*tmp88 - 32*mm2*scalarc0tme*tmp12*tmp31*tmp55*tmp58*tmp88 + 32*sca&
                   &larc0tme*tmp11*tmp14*tmp31*tmp55*tmp58*tmp88 - 32*scalarc0tme*tmp12*tmp14*tmp31*&
                   &tmp55*tmp58*tmp88 + (32*mm2*scalarc0tme*tmp17*tmp31*tmp55*tmp58*tmp88)/tmp11 - (&
                   &32*mm2*scalarc0tme*tmp29*tmp31*tmp55*tmp58*tmp88)/tmp12 - 32*mm2*scalarc0tmm*tmp&
                   &11*tmp154*tmp31*tmp90 + 32*mm2*scalarc0tmm*tmp12*tmp154*tmp31*tmp90 - 32*scalarc&
                   &0tmm*tmp11*tmp14*tmp154*tmp31*tmp90 + 32*scalarc0tmm*tmp12*tmp14*tmp154*tmp31*tm&
                   &p90 - (32*mm2*scalarc0tmm*tmp154*tmp17*tmp31*tmp90)/tmp11 + (32*mm2*scalarc0tmm*&
                   &tmp154*tmp29*tmp31*tmp90)/tmp12 + 32*scalarc0tmm*tmp11*tmp154*tmp31*tmp50*tmp90 &
                   &- 8*logmm*tmp11*tmp117*tmp119*tmp188*tmp189*tmp31*tmp50*tmp90 - 32*scalarc0tmm*t&
                   &mp12*tmp154*tmp31*tmp65*tmp90 + 8*logmm*tmp117*tmp119*tmp12*tmp188*tmp189*tmp31*&
                   &tmp65*tmp90 - 16*logmm*mm2*tmp11*tmp117*tmp119*tmp126*tmp31*tmp78*tmp90 + 16*log&
                   &mm*mm2*tmp117*tmp119*tmp12*tmp126*tmp31*tmp78*tmp90 - 16*logmm*tmp11*tmp117*tmp1&
                   &19*tmp126*tmp14*tmp31*tmp78*tmp90 + 16*logmm*tmp117*tmp119*tmp12*tmp126*tmp14*tm&
                   &p31*tmp78*tmp90 - (16*logmm*mm2*tmp117*tmp119*tmp126*tmp17*tmp31*tmp78*tmp90)/tm&
                   &p11 - 16*logmm*mm2*tmp11*tmp119*tmp188*tmp31*tmp78*tmp90 + 16*logmm*mm2*tmp119*t&
                   &mp12*tmp188*tmp31*tmp78*tmp90 - 16*logmm*tmp11*tmp119*tmp14*tmp188*tmp31*tmp78*t&
                   &mp90 + 16*logmm*tmp119*tmp12*tmp14*tmp188*tmp31*tmp78*tmp90 - (16*logmm*mm2*tmp1&
                   &19*tmp17*tmp188*tmp31*tmp78*tmp90)/tmp11 + 16*logmm*mm2*tmp11*tmp117*tmp181*tmp1&
                   &88*tmp31*tmp78*tmp90 - 16*logmm*mm2*tmp117*tmp12*tmp181*tmp188*tmp31*tmp78*tmp90&
                   & + 16*logmm*tmp11*tmp117*tmp14*tmp181*tmp188*tmp31*tmp78*tmp90 - 16*logmm*tmp117&
                   &*tmp12*tmp14*tmp181*tmp188*tmp31*tmp78*tmp90 + (16*logmm*mm2*tmp117*tmp17*tmp181&
                   &*tmp188*tmp31*tmp78*tmp90)/tmp11 + (16*logmm*mm2*tmp117*tmp119*tmp126*tmp29*tmp3&
                   &1*tmp78*tmp90)/tmp12 + (16*logmm*mm2*tmp119*tmp188*tmp29*tmp31*tmp78*tmp90)/tmp1&
                   &2 - (16*logmm*mm2*tmp117*tmp181*tmp188*tmp29*tmp31*tmp78*tmp90)/tmp12 + 8*logmm*&
                   &tmp11*tmp117*tmp119*tmp176*tmp31*tmp50*tmp78*tmp90 + 16*logmm*tmp11*tmp119*tmp18&
                   &8*tmp31*tmp50*tmp78*tmp90 - 8*logmm*tmp11*tmp117*tmp181*tmp188*tmp31*tmp50*tmp78&
                   &*tmp90 - 8*logmm*tmp117*tmp119*tmp12*tmp176*tmp31*tmp65*tmp78*tmp90 - 16*logmm*t&
                   &mp119*tmp12*tmp188*tmp31*tmp65*tmp78*tmp90 + 8*logmm*tmp117*tmp12*tmp181*tmp188*&
                   &tmp31*tmp65*tmp78*tmp90 + 16*discbtmm*mm2*tmp11*tmp146*tmp8*tmp90 - 8*discbtmm*t&
                   &mp12*tmp14*tmp146*tmp8*tmp90 - 8*discbtmm*tmp11*tmp146*tmp50*tmp8*tmp90 - 32*mm2&
                   &*scalarc0tmm*tmp11*tmp154*tmp58*tmp8*tmp90 + 32*mm2*scalarc0tmm*tmp12*tmp154*tmp&
                   &58*tmp8*tmp90 - 32*scalarc0tmm*tmp11*tmp14*tmp154*tmp58*tmp8*tmp90 + 32*scalarc0&
                   &tmm*tmp12*tmp14*tmp154*tmp58*tmp8*tmp90 - (32*mm2*scalarc0tmm*tmp154*tmp17*tmp58&
                   &*tmp8*tmp90)/tmp11 + (32*mm2*scalarc0tmm*tmp154*tmp29*tmp58*tmp8*tmp90)/tmp12 + &
                   &16*logmm*mm2*tmp11*tmp117*tmp119*tmp188*tmp78*tmp8*tmp90 - 16*logmm*mm2*tmp117*t&
                   &mp119*tmp12*tmp188*tmp78*tmp8*tmp90 + 16*logmm*tmp11*tmp117*tmp119*tmp14*tmp188*&
                   &tmp78*tmp8*tmp90 - 16*logmm*tmp117*tmp119*tmp12*tmp14*tmp188*tmp78*tmp8*tmp90 + &
                   &(16*logmm*mm2*tmp117*tmp119*tmp17*tmp188*tmp78*tmp8*tmp90)/tmp11 - (16*logmm*mm2&
                   &*tmp117*tmp119*tmp188*tmp29*tmp78*tmp8*tmp90)/tmp12 + 32*mm2*scalarc0tmm*tmp11*t&
                   &mp31*tmp58*tmp90*tmp92 - 32*mm2*scalarc0tmm*tmp12*tmp31*tmp58*tmp90*tmp92 + 32*s&
                   &calarc0tmm*tmp11*tmp14*tmp31*tmp58*tmp90*tmp92 - 32*scalarc0tmm*tmp12*tmp14*tmp3&
                   &1*tmp58*tmp90*tmp92 + (32*mm2*scalarc0tmm*tmp17*tmp31*tmp58*tmp90*tmp92)/tmp11 -&
                   & (32*mm2*scalarc0tmm*tmp29*tmp31*tmp58*tmp90*tmp92)/tmp12 + logmm*tmp11*tmp31*tm&
                   &p47*tmp50*tmp67*tmp93 + discbs*logmm*tmp12*tmp31*tmp47*tmp65*tmp67*tmp93 + logmm&
                   &*tmp12*tmp31*tmp65*tmp67*tmp75*tmp77*tmp78*tmp93 + (discbs*logmm*tmp11*tmp31*tmp&
                   &50*tmp67*tmp81*tmp82*tmp93)/tmp78 + (discbs*logmm*tmp12*tmp31*tmp47*tmp65*tmp67*&
                   &tmp85*tmp93)/tmp78 + (16*discbslogt*tmp11*tmp30*tmp33*tmp35*tmp50*tmp8*tmp94)/tm&
                   &p78 - (16*discbslogt*tmp12*tmp30*tmp33*tmp35*tmp65*tmp8*tmp94)/tmp78 - (16*discb&
                   &u*tmp11*tmp12*tmp14*tmp204*tmp220*tmp31)/tmp95 - (16*tmp11*tmp12*tmp14*tmp171*tm&
                   &p204*tmp249*tmp31)/tmp95 + (16*discbu*tmp11*tmp12*tmp14*tmp204*tmp213*tmp249*tmp&
                   &31)/tmp95 + (16*discbu*tmp11*tmp12*tmp14*tmp234*tmp235*tmp249*tmp31)/tmp95 + (32&
                   &*discbu*mm2*tmp204*tmp220*tmp29*tmp31)/tmp95 + (32*mm2*tmp171*tmp204*tmp249*tmp2&
                   &9*tmp31)/tmp95 - (32*discbu*mm2*tmp204*tmp213*tmp249*tmp29*tmp31)/tmp95 - (32*di&
                   &scbu*mm2*tmp234*tmp235*tmp249*tmp29*tmp31)/tmp95 + (32*scalarc0ir6s*tmp11*tmp12*&
                   &tmp14*tmp30*tmp31)/tmp95 - (64*mm2*scalarc0ir6s*tmp29*tmp30*tmp31)/tmp95 + (16*d&
                   &iscbs*tmp11*tmp12*tmp14*tmp160*tmp231*tmp31*tmp33)/tmp95 - (32*discbs*mm2*tmp160&
                   &*tmp231*tmp29*tmp31*tmp33)/tmp95 + (16*discbs*tmp11*tmp12*tmp14*tmp161*tmp231*tm&
                   &p31*tmp35)/tmp95 - (32*discbs*mm2*tmp161*tmp231*tmp29*tmp31*tmp35)/tmp95 + (16*d&
                   &iscbs*tmp11*tmp115*tmp12*tmp14*tmp31*tmp33*tmp35)/tmp95 - (32*discbs*mm2*tmp115*&
                   &tmp29*tmp31*tmp33*tmp35)/tmp95 - (16*scalarc0ir6u*tmp11*tmp12*tmp14*tmp31*tmp5)/&
                   &tmp95 - (8*discbu*logmm*tmp11*tmp12*tmp14*tmp22*tmp26*tmp31*tmp5)/(tmp171*tmp95)&
                   & + (32*mm2*scalarc0ir6u*tmp29*tmp31*tmp5)/tmp95 + (16*discbu*logmm*mm2*tmp22*tmp&
                   &26*tmp29*tmp31*tmp5)/(tmp171*tmp95) + (32*scalarc0tme*tmp11*tmp12*tmp14*tmp149*t&
                   &mp31*tmp55)/tmp95 - (64*mm2*scalarc0tme*tmp149*tmp29*tmp31*tmp55)/tmp95 + (32*lo&
                   &gt*tmp11*tmp12*tmp14*tmp31*tmp55*tmp56*tmp59*tmp63)/tmp95 - (64*logt*mm2*tmp29*t&
                   &mp31*tmp55*tmp56*tmp59*tmp63)/tmp95 - (16*scalarc0ir6u*tmp11*tmp12*tmp14*tmp212*&
                   &tmp31*tmp5*tmp65)/tmp95 + (32*mm2*scalarc0ir6u*tmp212*tmp29*tmp31*tmp5*tmp65)/tm&
                   &p95 - (16*scalarc0ir6s*tmp11*tmp12*tmp14*tmp31*tmp67)/tmp95 + (32*mm2*scalarc0ir&
                   &6s*tmp29*tmp31*tmp67)/tmp95 + (8*discbs*logmm*tmp11*tmp12*tmp14*tmp30*tmp31*tmp4&
                   &7*tmp67)/tmp95 + (16*logmm*mm2*tmp29*tmp30*tmp31*tmp47*tmp67)/tmp95 - (16*discbs&
                   &*logmm*mm2*tmp29*tmp30*tmp31*tmp47*tmp67)/tmp95 - (32*scalarc0ir6u*tmp11*tmp12*t&
                   &mp14*tmp31*tmp7)/tmp95 - (16*discbu*logmm*tmp11*tmp12*tmp14*tmp22*tmp26*tmp31*tm&
                   &p7)/(tmp171*tmp95) + (64*mm2*scalarc0ir6u*tmp29*tmp31*tmp7)/tmp95 + (32*discbu*l&
                   &ogmm*mm2*tmp22*tmp26*tmp29*tmp31*tmp7)/(tmp171*tmp95) - (16*discbu*tmp11*tmp12*t&
                   &mp14*tmp166*tmp170*tmp31*tmp5*tmp7)/tmp95 + (8*logmm*tmp11*tmp12*tmp14*tmp22*tmp&
                   &26*tmp31*tmp5*tmp7)/tmp95 - (8*discbu*logmm*tmp11*tmp12*tmp14*tmp22*tmp26*tmp31*&
                   &tmp5*tmp7)/tmp95 - (8*discbu*logmm*tmp11*tmp12*tmp14*tmp213*tmp22*tmp26*tmp31*tm&
                   &p5*tmp7)/(tmp171*tmp95) + (32*discbu*mm2*tmp166*tmp170*tmp29*tmp31*tmp5*tmp7)/tm&
                   &p95 - (16*logmm*mm2*tmp22*tmp26*tmp29*tmp31*tmp5*tmp7)/tmp95 + (16*discbu*logmm*&
                   &mm2*tmp22*tmp26*tmp29*tmp31*tmp5*tmp7)/tmp95 + (16*discbu*logmm*mm2*tmp213*tmp22&
                   &*tmp26*tmp29*tmp31*tmp5*tmp7)/(tmp171*tmp95) + (8*discbu*logmm*tmp11*tmp12*tmp14&
                   &*tmp22*tmp31*tmp5*tmp51*tmp7)/(tmp171*tmp95) - (16*discbu*logmm*mm2*tmp22*tmp29*&
                   &tmp31*tmp5*tmp51*tmp7)/(tmp171*tmp95) + (8*discbu*logmm*tmp11*tmp12*tmp14*tmp26*&
                   &tmp31*tmp5*tmp52*tmp7)/(tmp171*tmp95) - (16*discbu*logmm*mm2*tmp26*tmp29*tmp31*t&
                   &mp5*tmp52*tmp7)/(tmp171*tmp95) + (discbs*tmp140*tmp29*tmp31*tmp67*tmp70*tmp74)/t&
                   &mp95 + (16*discbs*tmp11*tmp12*tmp14*tmp30*tmp31*tmp67*tmp70*tmp74)/tmp95 + (16*s&
                   &calarc0ir6s*tmp11*tmp12*tmp14*tmp31*tmp67*tmp75*tmp76)/tmp95 - (32*mm2*scalarc0i&
                   &r6s*tmp29*tmp31*tmp67*tmp75*tmp76)/tmp95 - (8*logmm*tmp11*tmp12*tmp14*tmp171*tmp&
                   &212*tmp31*tmp5*tmp77)/tmp95 + (16*logmm*mm2*tmp171*tmp212*tmp29*tmp31*tmp5*tmp77&
                   &)/tmp95 + (discbs*logmm*tmp140*tmp29*tmp31*tmp47)/(tmp78*tmp95) + (16*discbs*log&
                   &mm*tmp11*tmp12*tmp14*tmp30*tmp31*tmp47)/(tmp78*tmp95) - (8*discbs*logmm*tmp11*tm&
                   &p12*tmp14*tmp31*tmp47*tmp67)/(tmp78*tmp95) + (16*discbs*logmm*mm2*tmp29*tmp31*tm&
                   &p47*tmp67)/(tmp78*tmp95) - (16*tmp11*tmp12*tmp14*tmp231*tmp31*tmp33*tmp35*tmp78)&
                   &/tmp95 + (32*mm2*tmp231*tmp29*tmp31*tmp33*tmp35*tmp78)/tmp95 + (8*logmm*tmp11*tm&
                   &p12*tmp14*tmp30*tmp31*tmp67*tmp75*tmp77*tmp78)/tmp95 - (16*logmm*mm2*tmp29*tmp30&
                   &*tmp31*tmp67*tmp75*tmp77*tmp78)/tmp95 - (32*discbu*logmume*tmp11*tmp12*tmp14*tmp&
                   &158*tmp22*tmp26*tmp8)/(tmp171*tmp95) - (16*discbulogt*tmp11*tmp12*tmp14*tmp180*t&
                   &mp22*tmp26*tmp8)/(tmp171*tmp95) + (64*discbu*logmume*mm2*tmp158*tmp22*tmp26*tmp2&
                   &9*tmp8)/(tmp171*tmp95) + (32*discbulogt*mm2*tmp180*tmp22*tmp26*tmp29*tmp8)/(tmp1&
                   &71*tmp95) + (discbslogt*tmp140*tmp163*tmp29*tmp33*tmp35*tmp8)/tmp95 - (32*logmum&
                   &e*tmp11*tmp12*tmp14*tmp158*tmp30*tmp33*tmp35*tmp8)/tmp95 + (32*discbs*logmume*tm&
                   &p11*tmp12*tmp14*tmp158*tmp30*tmp33*tmp35*tmp8)/tmp95 + (16*discbslogt*tmp11*tmp1&
                   &2*tmp14*tmp163*tmp30*tmp33*tmp35*tmp8)/tmp95 - (16*logt*tmp11*tmp12*tmp14*tmp163&
                   &*tmp30*tmp33*tmp35*tmp8)/tmp95 + (64*logmume*mm2*tmp158*tmp29*tmp30*tmp33*tmp35*&
                   &tmp8)/tmp95 - (64*discbs*logmume*mm2*tmp158*tmp29*tmp30*tmp33*tmp35*tmp8)/tmp95 &
                   &+ (32*logt*mm2*tmp163*tmp29*tmp30*tmp33*tmp35*tmp8)/tmp95 + (32*logmume*tmp11*tm&
                   &p12*tmp14*tmp158*tmp22*tmp26*tmp7*tmp8)/tmp95 - (32*discbu*logmume*tmp11*tmp12*t&
                   &mp14*tmp158*tmp22*tmp26*tmp7*tmp8)/tmp95 + (16*discbulogt*tmp104*tmp11*tmp12*tmp&
                   &14*tmp22*tmp26*tmp7*tmp8)/(tmp171*tmp95) - (16*discbulogt*tmp11*tmp12*tmp14*tmp1&
                   &80*tmp22*tmp26*tmp7*tmp8)/tmp95 + (16*logt*tmp11*tmp12*tmp14*tmp180*tmp22*tmp26*&
                   &tmp7*tmp8)/tmp95 - (32*discbu*logmume*tmp11*tmp12*tmp14*tmp158*tmp213*tmp22*tmp2&
                   &6*tmp7*tmp8)/(tmp171*tmp95) - (16*discbulogt*tmp11*tmp12*tmp14*tmp180*tmp213*tmp&
                   &22*tmp26*tmp7*tmp8)/(tmp171*tmp95) - (64*logmume*mm2*tmp158*tmp22*tmp26*tmp29*tm&
                   &p7*tmp8)/tmp95 + (64*discbu*logmume*mm2*tmp158*tmp22*tmp26*tmp29*tmp7*tmp8)/tmp9&
                   &5 - (32*discbulogt*mm2*tmp104*tmp22*tmp26*tmp29*tmp7*tmp8)/(tmp171*tmp95) + (32*&
                   &discbulogt*mm2*tmp180*tmp22*tmp26*tmp29*tmp7*tmp8)/tmp95 - (32*logt*mm2*tmp180*t&
                   &mp22*tmp26*tmp29*tmp7*tmp8)/tmp95 + (64*discbu*logmume*mm2*tmp158*tmp213*tmp22*t&
                   &mp26*tmp29*tmp7*tmp8)/(tmp171*tmp95) + (32*discbulogt*mm2*tmp180*tmp213*tmp22*tm&
                   &p26*tmp29*tmp7*tmp8)/(tmp171*tmp95) + (32*discbu*logmume*tmp11*tmp12*tmp14*tmp15&
                   &8*tmp22*tmp51*tmp7*tmp8)/(tmp171*tmp95) + (16*discbulogt*tmp11*tmp12*tmp14*tmp18&
                   &0*tmp22*tmp51*tmp7*tmp8)/(tmp171*tmp95) - (64*discbu*logmume*mm2*tmp158*tmp22*tm&
                   &p29*tmp51*tmp7*tmp8)/(tmp171*tmp95) - (32*discbulogt*mm2*tmp180*tmp22*tmp29*tmp5&
                   &1*tmp7*tmp8)/(tmp171*tmp95) + (32*discbu*logmume*tmp11*tmp12*tmp14*tmp158*tmp26*&
                   &tmp52*tmp7*tmp8)/(tmp171*tmp95) + (16*discbulogt*tmp11*tmp12*tmp14*tmp180*tmp26*&
                   &tmp52*tmp7*tmp8)/(tmp171*tmp95) - (64*discbu*logmume*mm2*tmp158*tmp26*tmp29*tmp5&
                   &2*tmp7*tmp8)/(tmp171*tmp95) - (32*discbulogt*mm2*tmp180*tmp26*tmp29*tmp52*tmp7*t&
                   &mp8)/(tmp171*tmp95) + (discbslogt*tmp140*tmp160*tmp163*tmp29*tmp33*tmp8)/(tmp78*&
                   &tmp95) + (32*discbs*logmume*tmp11*tmp12*tmp14*tmp158*tmp160*tmp30*tmp33*tmp8)/(t&
                   &mp78*tmp95) + (16*discbslogt*tmp11*tmp12*tmp14*tmp160*tmp163*tmp30*tmp33*tmp8)/(&
                   &tmp78*tmp95) - (64*discbs*logmume*mm2*tmp158*tmp160*tmp29*tmp30*tmp33*tmp8)/(tmp&
                   &78*tmp95) + (discbslogt*tmp140*tmp161*tmp163*tmp29*tmp35*tmp8)/(tmp78*tmp95) + (&
                   &32*discbs*logmume*tmp11*tmp12*tmp14*tmp158*tmp161*tmp30*tmp35*tmp8)/(tmp78*tmp95&
                   &) + (16*discbslogt*tmp11*tmp12*tmp14*tmp161*tmp163*tmp30*tmp35*tmp8)/(tmp78*tmp9&
                   &5) - (64*discbs*logmume*mm2*tmp158*tmp161*tmp29*tmp30*tmp35*tmp8)/(tmp78*tmp95) &
                   &- (32*discbs*logmume*tmp11*tmp12*tmp14*tmp158*tmp33*tmp35*tmp8)/(tmp78*tmp95) - &
                   &(16*discbslogt*tmp11*tmp12*tmp14*tmp163*tmp33*tmp35*tmp8)/(tmp78*tmp95) + (64*di&
                   &scbs*logmume*mm2*tmp158*tmp29*tmp33*tmp35*tmp8)/(tmp78*tmp95) + (32*discbslogt*m&
                   &m2*tmp163*tmp29*tmp33*tmp35*tmp8)/(tmp78*tmp95) + (16*discbs*logmm*mm2*tmp29*tmp&
                   &30*tmp31*tmp67*tmp81*tmp82)/(tmp78*tmp95) + (16*discbs*tmp11*tmp12*tmp14*tmp231*&
                   &tmp31*tmp33*tmp35*tmp85)/tmp95 - (32*discbs*mm2*tmp231*tmp29*tmp31*tmp33*tmp35*t&
                   &mp85)/tmp95 + (8*discbs*logmm*tmp11*tmp12*tmp14*tmp30*tmp31*tmp47*tmp67*tmp85)/(&
                   &tmp78*tmp95) - (16*discbs*logmm*mm2*tmp29*tmp30*tmp31*tmp47*tmp67*tmp85)/(tmp78*&
                   &tmp95) + (discbslogt*tmp140*tmp163*tmp29*tmp33*tmp35*tmp8*tmp85)/(tmp78*tmp95) +&
                   & (32*discbs*logmume*tmp11*tmp12*tmp14*tmp158*tmp30*tmp33*tmp35*tmp8*tmp85)/(tmp7&
                   &8*tmp95) + (16*discbslogt*tmp11*tmp12*tmp14*tmp163*tmp30*tmp33*tmp35*tmp8*tmp85)&
                   &/(tmp78*tmp95) - (64*discbs*logmume*mm2*tmp158*tmp29*tmp30*tmp33*tmp35*tmp8*tmp8&
                   &5)/(tmp78*tmp95) + (32*scalarc0tmm*tmp11*tmp12*tmp14*tmp154*tmp31*tmp90)/tmp95 -&
                   & (8*logmm*tmp11*tmp117*tmp119*tmp12*tmp14*tmp188*tmp189*tmp31*tmp90)/tmp95 - (64&
                   &*mm2*scalarc0tmm*tmp154*tmp29*tmp31*tmp90)/tmp95 + (16*logmm*mm2*tmp117*tmp119*t&
                   &mp188*tmp189*tmp29*tmp31*tmp90)/tmp95 + (8*logmm*tmp11*tmp117*tmp119*tmp12*tmp14&
                   &*tmp176*tmp31*tmp78*tmp90)/tmp95 + (16*logmm*tmp11*tmp119*tmp12*tmp14*tmp188*tmp&
                   &31*tmp78*tmp90)/tmp95 - (8*logmm*tmp11*tmp117*tmp12*tmp14*tmp181*tmp188*tmp31*tm&
                   &p78*tmp90)/tmp95 - (16*logmm*mm2*tmp117*tmp119*tmp176*tmp29*tmp31*tmp78*tmp90)/t&
                   &mp95 - (32*logmm*mm2*tmp119*tmp188*tmp29*tmp31*tmp78*tmp90)/tmp95 + (16*logmm*mm&
                   &2*tmp117*tmp181*tmp188*tmp29*tmp31*tmp78*tmp90)/tmp95 + (logmm*tmp11*tmp12*tmp14&
                   &*tmp31*tmp47*tmp67*tmp93)/tmp95 + (discbs*logmm*tmp11*tmp12*tmp14*tmp31*tmp67*tm&
                   &p81*tmp82*tmp93)/(tmp78*tmp95) + (discbslogt*tmp140*tmp29*tmp33*tmp35*tmp8*tmp94&
                   &)/(tmp78*tmp95) + (16*discbslogt*tmp11*tmp12*tmp14*tmp30*tmp33*tmp35*tmp8*tmp94)&
                   &/(tmp78*tmp95) + 64*tmp155*tmp158*tmp50*tmp95 + 32*logmumm*tmp155*tmp158*tmp50*t&
                   &mp95 + 32*scalarc0ir6tmm*tmp101*tmp155*tmp158*tmp50*tmp95 - (64*tmp11*tmp155*tmp&
                   &158*tmp50*tmp95)/tmp12 - (32*logmumm*tmp11*tmp155*tmp158*tmp50*tmp95)/tmp12 - (3&
                   &2*scalarc0ir6tmm*tmp101*tmp11*tmp155*tmp158*tmp50*tmp95)/tmp12 - 16*scalarc0ir6t&
                   &mm*tmp101*tmp14*tmp155*tmp158*tmp50*tmp63*tmp95 + (16*scalarc0ir6tmm*tmp101*tmp1&
                   &1*tmp14*tmp155*tmp158*tmp50*tmp63*tmp95)/tmp12 + 64*tmp155*tmp158*tmp65*tmp95 + &
                   &32*logmumm*tmp155*tmp158*tmp65*tmp95 + 32*scalarc0ir6tmm*tmp101*tmp155*tmp158*tm&
                   &p65*tmp95 - (64*tmp12*tmp155*tmp158*tmp65*tmp95)/tmp11 - (32*logmumm*tmp12*tmp15&
                   &5*tmp158*tmp65*tmp95)/tmp11 - (32*scalarc0ir6tmm*tmp101*tmp12*tmp155*tmp158*tmp6&
                   &5*tmp95)/tmp11 - 16*scalarc0ir6tmm*tmp101*tmp14*tmp155*tmp158*tmp63*tmp65*tmp95 &
                   &+ (16*scalarc0ir6tmm*tmp101*tmp12*tmp14*tmp155*tmp158*tmp63*tmp65*tmp95)/tmp11 -&
                   & 32*tmp102*tmp50*tmp8*tmp95 - 16*logmumm*tmp102*tmp50*tmp8*tmp95 - 16*scalarc0ir&
                   &6tmm*tmp101*tmp102*tmp50*tmp8*tmp95 + (32*tmp102*tmp11*tmp50*tmp8*tmp95)/tmp12 +&
                   & (16*logmumm*tmp102*tmp11*tmp50*tmp8*tmp95)/tmp12 + (16*scalarc0ir6tmm*tmp101*tm&
                   &p102*tmp11*tmp50*tmp8*tmp95)/tmp12 - 16*scalarc0ir6tmm*tmp158*tmp50*tmp8*tmp95 +&
                   & (16*scalarc0ir6tmm*tmp11*tmp158*tmp50*tmp8*tmp95)/tmp12 - 16*discbtmm*logmumm*t&
                   &mp14*tmp151*tmp158*tmp50*tmp8*tmp95 + (16*discbtmm*logmumm*tmp11*tmp14*tmp151*tm&
                   &p158*tmp50*tmp8*tmp95)/tmp12 - 8*discbtmm*tmp151*tmp195*tmp50*tmp8*tmp95 + (8*di&
                   &scbtmm*tmp11*tmp151*tmp195*tmp50*tmp8*tmp95)/tmp12 + 16*discbtmm*tmp101*tmp158*t&
                   &mp50*tmp62*tmp8*tmp95 - (16*discbtmm*tmp101*tmp11*tmp158*tmp50*tmp62*tmp8*tmp95)&
                   &/tmp12 - 32*tmp102*tmp65*tmp8*tmp95 - 16*logmumm*tmp102*tmp65*tmp8*tmp95 - 16*sc&
                   &alarc0ir6tmm*tmp101*tmp102*tmp65*tmp8*tmp95 + (32*tmp102*tmp12*tmp65*tmp8*tmp95)&
                   &/tmp11 + (16*logmumm*tmp102*tmp12*tmp65*tmp8*tmp95)/tmp11 + (16*scalarc0ir6tmm*t&
                   &mp101*tmp102*tmp12*tmp65*tmp8*tmp95)/tmp11 - 16*scalarc0ir6tmm*tmp158*tmp65*tmp8&
                   &*tmp95 + (16*scalarc0ir6tmm*tmp12*tmp158*tmp65*tmp8*tmp95)/tmp11 - 16*discbtmm*l&
                   &ogmumm*tmp14*tmp151*tmp158*tmp65*tmp8*tmp95 + (16*discbtmm*logmumm*tmp12*tmp14*t&
                   &mp151*tmp158*tmp65*tmp8*tmp95)/tmp11 - 8*discbtmm*tmp151*tmp195*tmp65*tmp8*tmp95&
                   & + (8*discbtmm*tmp12*tmp151*tmp195*tmp65*tmp8*tmp95)/tmp11 + 16*discbtmm*tmp101*&
                   &tmp158*tmp62*tmp65*tmp8*tmp95 - (16*discbtmm*tmp101*tmp12*tmp158*tmp62*tmp65*tmp&
                   &8*tmp95)/tmp11 + 16*logmumm*tmp14*tmp155*tmp158*tmp50*tmp90*tmp95 + 32*discbtmm*&
                   &logmumm*tmp14*tmp155*tmp158*tmp50*tmp90*tmp95 - (16*logmumm*tmp11*tmp14*tmp155*t&
                   &mp158*tmp50*tmp90*tmp95)/tmp12 - (32*discbtmm*logmumm*tmp11*tmp14*tmp155*tmp158*&
                   &tmp50*tmp90*tmp95)/tmp12 + 8*tmp155*tmp195*tmp50*tmp90*tmp95 + 16*discbtmm*tmp15&
                   &5*tmp195*tmp50*tmp90*tmp95 - (8*tmp11*tmp155*tmp195*tmp50*tmp90*tmp95)/tmp12 - (&
                   &16*discbtmm*tmp11*tmp155*tmp195*tmp50*tmp90*tmp95)/tmp12 + 16*logmumm*tmp14*tmp1&
                   &55*tmp158*tmp65*tmp90*tmp95 + 32*discbtmm*logmumm*tmp14*tmp155*tmp158*tmp65*tmp9&
                   &0*tmp95 - (16*logmumm*tmp12*tmp14*tmp155*tmp158*tmp65*tmp90*tmp95)/tmp11 - (32*d&
                   &iscbtmm*logmumm*tmp12*tmp14*tmp155*tmp158*tmp65*tmp90*tmp95)/tmp11 + 8*tmp155*tm&
                   &p195*tmp65*tmp90*tmp95 + 16*discbtmm*tmp155*tmp195*tmp65*tmp90*tmp95 - (8*tmp12*&
                   &tmp155*tmp195*tmp65*tmp90*tmp95)/tmp11 - (16*discbtmm*tmp12*tmp155*tmp195*tmp65*&
                   &tmp90*tmp95)/tmp11 + 16*discbtmm*me2*tmp146*tmp8*tmp90*tmp95 - 8*discbtmm*tmp139&
                   &*tmp50*tmp8*tmp90*tmp95 + (8*discbtmm*tmp11*tmp139*tmp50*tmp8*tmp90*tmp95)/tmp12&
                   & - 16*discbtmm*logmumm*tmp102*tmp14*tmp50*tmp8*tmp90*tmp95 + (16*discbtmm*logmum&
                   &m*tmp102*tmp11*tmp14*tmp50*tmp8*tmp90*tmp95)/tmp12 - 8*discbtmm*tmp146*tmp50*tmp&
                   &8*tmp90*tmp95 + 16*discbtmm*logmumm*tmp158*tmp50*tmp8*tmp90*tmp95 - (16*discbtmm&
                   &*logmumm*tmp11*tmp158*tmp50*tmp8*tmp90*tmp95)/tmp12 - 16*discbtmm*logmumm*tmp14*&
                   &tmp158*tmp198*tmp50*tmp8*tmp90*tmp95 + (16*discbtmm*logmumm*tmp11*tmp14*tmp158*t&
                   &mp198*tmp50*tmp8*tmp90*tmp95)/tmp12 - 8*discbtmm*tmp195*tmp198*tmp50*tmp8*tmp90*&
                   &tmp95 + (8*discbtmm*tmp11*tmp195*tmp198*tmp50*tmp8*tmp90*tmp95)/tmp12 - 8*discbt&
                   &mm*tmp139*tmp65*tmp8*tmp90*tmp95 + (8*discbtmm*tmp12*tmp139*tmp65*tmp8*tmp90*tmp&
                   &95)/tmp11 - 16*discbtmm*logmumm*tmp102*tmp14*tmp65*tmp8*tmp90*tmp95 + (16*discbt&
                   &mm*logmumm*tmp102*tmp12*tmp14*tmp65*tmp8*tmp90*tmp95)/tmp11 + (8*discbtmm*tmp12*&
                   &tmp146*tmp65*tmp8*tmp90*tmp95)/tmp11 + 16*discbtmm*logmumm*tmp158*tmp65*tmp8*tmp&
                   &90*tmp95 - (16*discbtmm*logmumm*tmp12*tmp158*tmp65*tmp8*tmp90*tmp95)/tmp11 - 16*&
                   &discbtmm*logmumm*tmp14*tmp158*tmp198*tmp65*tmp8*tmp90*tmp95 + (16*discbtmm*logmu&
                   &mm*tmp12*tmp14*tmp158*tmp198*tmp65*tmp8*tmp90*tmp95)/tmp11 - 8*discbtmm*tmp195*t&
                   &mp198*tmp65*tmp8*tmp90*tmp95 + (8*discbtmm*tmp12*tmp195*tmp198*tmp65*tmp8*tmp90*&
                   &tmp95)/tmp11 + 64*mm2*tmp11*tmp8*tmp97 + 32*logmumm*mm2*tmp11*tmp8*tmp97 + 32*mm&
                   &2*scalarc0ir6tmm*tmp101*tmp11*tmp8*tmp97 - 32*tmp12*tmp14*tmp8*tmp97 - 16*logmum&
                   &m*tmp12*tmp14*tmp8*tmp97 - 16*scalarc0ir6tmm*tmp101*tmp12*tmp14*tmp8*tmp97 - 32*&
                   &tmp11*tmp50*tmp8*tmp97 - 16*logmumm*tmp11*tmp50*tmp8*tmp97 - 16*scalarc0ir6tmm*t&
                   &mp101*tmp11*tmp50*tmp8*tmp97 + (32*discbu*logmume*tmp11*tmp22*tmp26*tmp50*tmp7*t&
                   &mp8*tmp97)/tmp171 - (32*discbu*logmume*tmp12*tmp22*tmp26*tmp65*tmp7*tmp8*tmp97)/&
                   &tmp171 + (32*discbs*logmume*tmp11*tmp30*tmp33*tmp35*tmp50*tmp8*tmp97)/tmp78 - (3&
                   &2*discbs*logmume*tmp12*tmp30*tmp33*tmp35*tmp65*tmp8*tmp97)/tmp78 + 32*discbtmm*l&
                   &ogmumm*mm2*tmp11*tmp14*tmp8*tmp90*tmp97 - 16*discbtmm*logmumm*tmp12*tmp14**2*tmp&
                   &8*tmp90*tmp97 - 16*discbtmm*logmumm*tmp11*tmp14*tmp50*tmp8*tmp90*tmp97 + (32*dis&
                   &cbu*logmume*tmp11*tmp12*tmp14*tmp22*tmp26*tmp7*tmp8*tmp97)/(tmp171*tmp95) - (64*&
                   &discbu*logmume*mm2*tmp22*tmp26*tmp29*tmp7*tmp8*tmp97)/(tmp171*tmp95) + (32*discb&
                   &s*logmume*tmp11*tmp12*tmp14*tmp30*tmp33*tmp35*tmp8*tmp97)/(tmp78*tmp95) - (64*di&
                   &scbs*logmume*mm2*tmp29*tmp30*tmp33*tmp35*tmp8*tmp97)/(tmp78*tmp95) + 64*me2*tmp8&
                   &*tmp95*tmp97 + 32*logmumm*me2*tmp8*tmp95*tmp97 + 32*me2*scalarc0ir6tmm*tmp101*tm&
                   &p8*tmp95*tmp97 - 32*tmp50*tmp8*tmp95*tmp97 - 16*logmumm*tmp50*tmp8*tmp95*tmp97 -&
                   & 16*scalarc0ir6tmm*tmp101*tmp50*tmp8*tmp95*tmp97 + (32*tmp12*tmp65*tmp8*tmp95*tm&
                   &p97)/tmp11 + (16*logmumm*tmp12*tmp65*tmp8*tmp95*tmp97)/tmp11 + (16*scalarc0ir6tm&
                   &m*tmp101*tmp12*tmp65*tmp8*tmp95*tmp97)/tmp11 + 32*discbtmm*logmumm*me2*tmp14*tmp&
                   &8*tmp90*tmp95*tmp97 - 16*discbtmm*logmumm*tmp14*tmp50*tmp8*tmp90*tmp95*tmp97 + (&
                   &16*discbtmm*logmumm*tmp12*tmp14*tmp65*tmp8*tmp90*tmp95*tmp97)/tmp11 + (64*tmp12*&
                   &tmp158*tmp8*tmp95)/tmp98 + (32*logmumm*tmp12*tmp158*tmp8*tmp95)/tmp98 + (32*scal&
                   &arc0ir6tmm*tmp101*tmp12*tmp158*tmp8*tmp95)/tmp98 + (32*discbtmm*logmumm*tmp12*tm&
                   &p14*tmp158*tmp8*tmp90*tmp95)/tmp98 + (16*discbtmm*tmp12*tmp195*tmp8*tmp90*tmp95)&
                   &/tmp98 - 64*tmp155*tmp158*tmp50*tmp98 - 32*logmumm*tmp155*tmp158*tmp50*tmp98 - 3&
                   &2*scalarc0ir6tmm*tmp101*tmp155*tmp158*tmp50*tmp98 + (64*tmp12*tmp155*tmp158*tmp5&
                   &0*tmp98)/tmp11 + (32*logmumm*tmp12*tmp155*tmp158*tmp50*tmp98)/tmp11 + (32*scalar&
                   &c0ir6tmm*tmp101*tmp12*tmp155*tmp158*tmp50*tmp98)/tmp11 + 16*scalarc0ir6tmm*tmp10&
                   &1*tmp14*tmp155*tmp158*tmp50*tmp63*tmp98 - (16*scalarc0ir6tmm*tmp101*tmp12*tmp14*&
                   &tmp155*tmp158*tmp50*tmp63*tmp98)/tmp11 - 64*tmp155*tmp158*tmp65*tmp98 - 32*logmu&
                   &mm*tmp155*tmp158*tmp65*tmp98 - 32*scalarc0ir6tmm*tmp101*tmp155*tmp158*tmp65*tmp9&
                   &8 + (64*tmp11*tmp155*tmp158*tmp65*tmp98)/tmp12 + (32*logmumm*tmp11*tmp155*tmp158&
                   &*tmp65*tmp98)/tmp12 + (32*scalarc0ir6tmm*tmp101*tmp11*tmp155*tmp158*tmp65*tmp98)&
                   &/tmp12 + 16*scalarc0ir6tmm*tmp101*tmp14*tmp155*tmp158*tmp63*tmp65*tmp98 - (16*sc&
                   &alarc0ir6tmm*tmp101*tmp11*tmp14*tmp155*tmp158*tmp63*tmp65*tmp98)/tmp12 + 64*tmp1&
                   &58*tmp8*tmp98 + 32*logmumm*tmp158*tmp8*tmp98 + 32*scalarc0ir6tmm*tmp101*tmp158*t&
                   &mp8*tmp98 - 64*tmp12*tmp158*tmp159*tmp8*tmp98 - 32*logmumm*tmp12*tmp158*tmp159*t&
                   &mp8*tmp98 - 32*scalarc0ir6tmm*tmp101*tmp12*tmp158*tmp159*tmp8*tmp98 + 32*tmp102*&
                   &tmp50*tmp8*tmp98 + 16*logmumm*tmp102*tmp50*tmp8*tmp98 + 16*scalarc0ir6tmm*tmp101&
                   &*tmp102*tmp50*tmp8*tmp98 - (32*tmp102*tmp12*tmp50*tmp8*tmp98)/tmp11 - (16*logmum&
                   &m*tmp102*tmp12*tmp50*tmp8*tmp98)/tmp11 - (16*scalarc0ir6tmm*tmp101*tmp102*tmp12*&
                   &tmp50*tmp8*tmp98)/tmp11 + 16*scalarc0ir6tmm*tmp158*tmp50*tmp8*tmp98 - (16*scalar&
                   &c0ir6tmm*tmp12*tmp158*tmp50*tmp8*tmp98)/tmp11 + 16*discbtmm*logmumm*tmp14*tmp151&
                   &*tmp158*tmp50*tmp8*tmp98 - (16*discbtmm*logmumm*tmp12*tmp14*tmp151*tmp158*tmp50*&
                   &tmp8*tmp98)/tmp11 + 8*discbtmm*tmp151*tmp195*tmp50*tmp8*tmp98 - (8*discbtmm*tmp1&
                   &2*tmp151*tmp195*tmp50*tmp8*tmp98)/tmp11 - 16*discbtmm*tmp101*tmp158*tmp50*tmp62*&
                   &tmp8*tmp98 + (16*discbtmm*tmp101*tmp12*tmp158*tmp50*tmp62*tmp8*tmp98)/tmp11 + 32&
                   &*tmp102*tmp65*tmp8*tmp98 + 16*logmumm*tmp102*tmp65*tmp8*tmp98 + 16*scalarc0ir6tm&
                   &m*tmp101*tmp102*tmp65*tmp8*tmp98 - (32*tmp102*tmp11*tmp65*tmp8*tmp98)/tmp12 - (1&
                   &6*logmumm*tmp102*tmp11*tmp65*tmp8*tmp98)/tmp12 - (16*scalarc0ir6tmm*tmp101*tmp10&
                   &2*tmp11*tmp65*tmp8*tmp98)/tmp12 + 16*scalarc0ir6tmm*tmp158*tmp65*tmp8*tmp98 - (1&
                   &6*scalarc0ir6tmm*tmp11*tmp158*tmp65*tmp8*tmp98)/tmp12 + 16*discbtmm*logmumm*tmp1&
                   &4*tmp151*tmp158*tmp65*tmp8*tmp98 - (16*discbtmm*logmumm*tmp11*tmp14*tmp151*tmp15&
                   &8*tmp65*tmp8*tmp98)/tmp12 + 8*discbtmm*tmp151*tmp195*tmp65*tmp8*tmp98 - (8*discb&
                   &tmm*tmp11*tmp151*tmp195*tmp65*tmp8*tmp98)/tmp12 - 16*discbtmm*tmp101*tmp158*tmp6&
                   &2*tmp65*tmp8*tmp98 + (16*discbtmm*tmp101*tmp11*tmp158*tmp62*tmp65*tmp8*tmp98)/tm&
                   &p12 - 16*logmumm*tmp14*tmp155*tmp158*tmp50*tmp90*tmp98 - 32*discbtmm*logmumm*tmp&
                   &14*tmp155*tmp158*tmp50*tmp90*tmp98 + (16*logmumm*tmp12*tmp14*tmp155*tmp158*tmp50&
                   &*tmp90*tmp98)/tmp11 + (32*discbtmm*logmumm*tmp12*tmp14*tmp155*tmp158*tmp50*tmp90&
                   &*tmp98)/tmp11 - 8*tmp155*tmp195*tmp50*tmp90*tmp98 - 16*discbtmm*tmp155*tmp195*tm&
                   &p50*tmp90*tmp98 + (8*tmp12*tmp155*tmp195*tmp50*tmp90*tmp98)/tmp11 + (16*discbtmm&
                   &*tmp12*tmp155*tmp195*tmp50*tmp90*tmp98)/tmp11 - 16*logmumm*tmp14*tmp155*tmp158*t&
                   &mp65*tmp90*tmp98 - 32*discbtmm*logmumm*tmp14*tmp155*tmp158*tmp65*tmp90*tmp98 + (&
                   &16*logmumm*tmp11*tmp14*tmp155*tmp158*tmp65*tmp90*tmp98)/tmp12 + (32*discbtmm*log&
                   &mumm*tmp11*tmp14*tmp155*tmp158*tmp65*tmp90*tmp98)/tmp12 - 8*tmp155*tmp195*tmp65*&
                   &tmp90*tmp98 - 16*discbtmm*tmp155*tmp195*tmp65*tmp90*tmp98 + (8*tmp11*tmp155*tmp1&
                   &95*tmp65*tmp90*tmp98)/tmp12 + (16*discbtmm*tmp11*tmp155*tmp195*tmp65*tmp90*tmp98&
                   &)/tmp12 + 32*discbtmm*logmumm*tmp14*tmp158*tmp8*tmp90*tmp98 - 32*discbtmm*logmum&
                   &m*tmp12*tmp14*tmp158*tmp159*tmp8*tmp90*tmp98 + 16*discbtmm*tmp195*tmp8*tmp90*tmp&
                   &98 - 16*discbtmm*tmp12*tmp159*tmp195*tmp8*tmp90*tmp98 + 8*discbtmm*tmp139*tmp50*&
                   &tmp8*tmp90*tmp98 - (8*discbtmm*tmp12*tmp139*tmp50*tmp8*tmp90*tmp98)/tmp11 + 16*d&
                   &iscbtmm*logmumm*tmp102*tmp14*tmp50*tmp8*tmp90*tmp98 - (16*discbtmm*logmumm*tmp10&
                   &2*tmp12*tmp14*tmp50*tmp8*tmp90*tmp98)/tmp11 - 16*discbtmm*logmumm*tmp158*tmp50*t&
                   &mp8*tmp90*tmp98 + (16*discbtmm*logmumm*tmp12*tmp158*tmp50*tmp8*tmp90*tmp98)/tmp1&
                   &1 + 16*discbtmm*logmumm*tmp14*tmp158*tmp198*tmp50*tmp8*tmp90*tmp98 - (16*discbtm&
                   &m*logmumm*tmp12*tmp14*tmp158*tmp198*tmp50*tmp8*tmp90*tmp98)/tmp11 + 8*discbtmm*t&
                   &mp195*tmp198*tmp50*tmp8*tmp90*tmp98 - (8*discbtmm*tmp12*tmp195*tmp198*tmp50*tmp8&
                   &*tmp90*tmp98)/tmp11 + 8*discbtmm*tmp139*tmp65*tmp8*tmp90*tmp98 - (8*discbtmm*tmp&
                   &11*tmp139*tmp65*tmp8*tmp90*tmp98)/tmp12 + 16*discbtmm*logmumm*tmp102*tmp14*tmp65&
                   &*tmp8*tmp90*tmp98 - (16*discbtmm*logmumm*tmp102*tmp11*tmp14*tmp65*tmp8*tmp90*tmp&
                   &98)/tmp12 - 16*discbtmm*logmumm*tmp158*tmp65*tmp8*tmp90*tmp98 + (16*discbtmm*log&
                   &mumm*tmp11*tmp158*tmp65*tmp8*tmp90*tmp98)/tmp12 + 16*discbtmm*logmumm*tmp14*tmp1&
                   &58*tmp198*tmp65*tmp8*tmp90*tmp98 - (16*discbtmm*logmumm*tmp11*tmp14*tmp158*tmp19&
                   &8*tmp65*tmp8*tmp90*tmp98)/tmp12 + 8*discbtmm*tmp195*tmp198*tmp65*tmp8*tmp90*tmp9&
                   &8 - (8*discbtmm*tmp11*tmp195*tmp198*tmp65*tmp8*tmp90*tmp98)/tmp12 + (8*discbtmm*&
                   &tmp11*tmp146*tmp65*tmp8*tmp90*tmp98)/tmp95 + (32*tmp11*tmp65*tmp8*tmp97*tmp98)/t&
                   &mp95 + (16*logmumm*tmp11*tmp65*tmp8*tmp97*tmp98)/tmp95 + (16*scalarc0ir6tmm*tmp1&
                   &01*tmp11*tmp65*tmp8*tmp97*tmp98)/tmp95 + (16*discbtmm*logmumm*tmp11*tmp14*tmp65*&
                   &tmp8*tmp90*tmp97*tmp98)/tmp95 - 8*discbtmm*tmp146*tmp8*tmp90*tmp98*tmp99 - 32*tm&
                   &p8*tmp97*tmp98*tmp99 - 16*logmumm*tmp8*tmp97*tmp98*tmp99 - 16*scalarc0ir6tmm*tmp&
                   &101*tmp8*tmp97*tmp98*tmp99 - 16*discbtmm*logmumm*tmp14*tmp8*tmp90*tmp97*tmp98*tm&
                   &p99

  END FUNCTION LBK_NLP_QEQM3

  FUNCTION LBK_NLP_QE3QM(me2, mm2, ss, tt, s15, s25, s35)
  implicit none
  real LBK_NLP_Qe3Qm
  real(kind=prec) ::  me2
  real(kind=prec) ::  mm2
  real(kind=prec) ::  ss
  real(kind=prec) ::  tt
  real(kind=prec) ::  s15
  real(kind=prec) ::  s25
  real(kind=prec) ::  s35
  real tmp1, tmp2, tmp3, tmp4, tmp5
  real tmp6, tmp7, tmp8, tmp9, tmp10
  real tmp11, tmp12, tmp13, tmp14, tmp15
  real tmp16, tmp17, tmp18, tmp19, tmp20
  real tmp21, tmp22, tmp23, tmp24, tmp25
  real tmp26, tmp27, tmp28, tmp29, tmp30
  real tmp31, tmp32, tmp33, tmp34, tmp35
  real tmp36, tmp37, tmp38, tmp39, tmp40
  real tmp41, tmp42, tmp43, tmp44, tmp45
  real tmp46, tmp47, tmp48, tmp49, tmp50
  real tmp51, tmp52, tmp53, tmp54, tmp55
  real tmp56, tmp57, tmp58, tmp59, tmp60
  real tmp61, tmp62, tmp63, tmp64, tmp65
  real tmp66, tmp67, tmp68, tmp69, tmp70
  real tmp71, tmp72, tmp73, tmp74, tmp75
  real tmp76, tmp77, tmp78, tmp79, tmp80
  real tmp81, tmp82, tmp83, tmp84, tmp85
  real tmp86, tmp87, tmp88, tmp89, tmp90
  real tmp91, tmp92, tmp93, tmp94, tmp95
  real tmp96, tmp97, tmp98, tmp99, tmp100
  real tmp101, tmp102, tmp103, tmp104, tmp105
  real tmp106, tmp107, tmp108, tmp109, tmp110
  real tmp111, tmp112, tmp113, tmp114, tmp115
  real tmp116, tmp117, tmp118, tmp119, tmp120
  real tmp121, tmp122, tmp123, tmp124, tmp125
  real tmp126, tmp127, tmp128, tmp129, tmp130
  real tmp131, tmp132, tmp133, tmp134, tmp135
  real tmp136, tmp137, tmp138, tmp139, tmp140
  real tmp141, tmp142, tmp143, tmp144, tmp145
  real tmp146, tmp147, tmp148, tmp149, tmp150
  real tmp151, tmp152, tmp153, tmp154, tmp155
  real tmp156, tmp157, tmp158, tmp159, tmp160
  real tmp161, tmp162, tmp163, tmp164, tmp165
  real tmp166, tmp167, tmp168, tmp169, tmp170
  real tmp171, tmp172, tmp173, tmp174, tmp175
  real tmp176, tmp177, tmp178, tmp179, tmp180
  real tmp181, tmp182, tmp183, tmp184, tmp185
  real tmp186, tmp187, tmp188, tmp189, tmp190
  real tmp191, tmp192, tmp193, tmp194, tmp195
  real tmp196, tmp197, tmp198, tmp199, tmp200
  real tmp201, tmp202, tmp203, tmp204, tmp205
  real tmp206, tmp207, tmp208, tmp209, tmp210
  real tmp211, tmp212, tmp213, tmp214, tmp215
  real tmp216, tmp217, tmp218, tmp219, tmp220
  real tmp221, tmp222, tmp223, tmp224, tmp225
  real tmp226, tmp227, tmp228, tmp229, tmp230
  real tmp231, tmp232, tmp233, tmp234

  tmp1 = -ss
  tmp2 = me2 + mm2 + tmp1
  tmp3 = 1/tt
  tmp4 = s15**(-2)
  tmp5 = 1/s15
  tmp6 = -me2
  tmp7 = -mm2
  tmp8 = ss + tmp6 + tmp7
  tmp9 = me2**2
  tmp10 = mm2 + tmp1
  tmp11 = tmp10**2
  tmp12 = mm2 + ss
  tmp13 = -2*me2*tmp12
  tmp14 = tmp11 + tmp13 + tmp9
  tmp15 = 1/tmp14
  tmp16 = 1/s35
  tmp17 = 2*me2
  tmp18 = -(1/tmp3)
  tmp19 = tmp17 + tmp18
  tmp20 = 4*me2
  tmp21 = 4*mm2
  tmp22 = -2*ss
  tmp23 = tmp18 + tmp20 + tmp21 + tmp22
  tmp24 = me2 + tmp10 + tmp18
  tmp25 = sqrt(me2)
  tmp26 = sqrt(mm2)
  tmp27 = -tmp26
  tmp28 = tmp25 + tmp27
  tmp29 = tmp28**2
  tmp30 = tmp1 + tmp18 + tmp29
  tmp31 = 1/tmp30
  tmp32 = tmp25 + tmp26
  tmp33 = tmp32**2
  tmp34 = tmp1 + tmp18 + tmp33
  tmp35 = 1/tmp34
  tmp36 = 2*mm2
  tmp37 = tmp1 + tmp19 + tmp36
  tmp38 = tmp35**2
  tmp39 = tmp31**2
  tmp40 = -(1/tmp16)
  tmp41 = tmp40 + 1/tmp5
  tmp42 = 4*tmp25*tmp26
  tmp43 = tmp18 + tmp20
  tmp44 = 1/tmp43
  tmp45 = tmp18 + tmp42
  tmp46 = 1/tmp3 + tmp42
  tmp47 = -4*mm2
  tmp48 = 1/tmp3 + tmp47
  tmp49 = 1/tmp48
  tmp50 = 1/tmp3 + tmp8
  tmp51 = 2*ss
  tmp52 = 1/tmp3 + tmp51
  tmp53 = ss**2
  tmp54 = me2 + tmp7
  tmp55 = tmp54**2
  tmp56 = -tmp53
  tmp57 = tmp55 + tmp56
  tmp58 = -2*me2*mm2
  tmp59 = mm2**2
  tmp60 = me2*tmp22
  tmp61 = mm2*tmp22
  tmp62 = tmp53 + tmp58 + tmp59 + tmp60 + tmp61 + tmp9
  tmp63 = tmp62**(-2)
  tmp64 = 1/tmp62
  tmp65 = tmp2**2
  tmp66 = mm2 + tmp6
  tmp67 = 1/ss
  tmp68 = -2*me2
  tmp69 = -2*tmp10
  tmp70 = tmp68 + tmp69
  tmp71 = tmp15**2
  tmp72 = -tmp67
  tmp73 = tmp64*tmp8
  tmp74 = tmp72 + tmp73
  tmp75 = tmp1 + tmp29
  tmp76 = 1/tmp75
  tmp77 = tmp1 + tmp33
  tmp78 = 1/tmp77
  tmp79 = tmp3**2
  tmp80 = -8*tmp2
  tmp81 = 2/tmp3
  tmp82 = tmp80 + tmp81
  tmp83 = -4*tmp2
  tmp84 = tmp81 + tmp83
  tmp85 = 1/s25
  tmp86 = tmp41 + 1/tmp85
  tmp87 = 1/tmp86
  tmp88 = tmp18 + tmp36
  tmp89 = 1/tmp3 + tmp68
  tmp90 = tmp51 + tmp81
  tmp91 = 6/tmp3
  tmp92 = tmp80 + tmp91
  tmp93 = -3*tmp9
  tmp94 = -6/tmp67
  tmp95 = tmp36 + tmp94
  tmp96 = tmp6*tmp95
  tmp97 = -3*tmp11
  tmp98 = -2/(tmp3*tmp67)
  tmp99 = tmp93 + tmp96 + tmp97 + tmp98
  tmp100 = 3*tmp9
  tmp101 = -7/tmp67
  tmp102 = mm2 + tmp101
  tmp103 = tmp102*tmp17
  tmp104 = 3*tmp11
  tmp105 = tmp100 + tmp103 + tmp104
  tmp106 = -2*tmp105
  tmp107 = tmp1 + tmp17
  tmp108 = (12*tmp107)/tmp3
  tmp109 = 1/tmp79
  tmp110 = -9*tmp109
  tmp111 = tmp106 + tmp108 + tmp110
  tmp112 = -32*me2*tmp2
  tmp113 = -14*me2
  tmp114 = -6*tmp10
  tmp115 = tmp113 + tmp114
  tmp116 = (-2*tmp115)/tmp3
  tmp117 = -6*tmp109
  tmp118 = tmp112 + tmp116 + tmp117
  tmp119 = 8*tmp9
  tmp120 = (-8*me2)/tmp3
  tmp121 = tmp109 + tmp119 + tmp120
  tmp122 = 2*tmp2
  tmp123 = tmp122 + tmp18
  tmp124 = tmp18 + tmp21
  tmp125 = 1/tmp124
  tmp126 = 8*tmp59
  tmp127 = (-8*mm2)/tmp3
  tmp128 = tmp109 + tmp126 + tmp127
  tmp129 = tmp3**3
  tmp130 = 2*tmp65
  tmp131 = tmp81/tmp67
  tmp132 = tmp109 + tmp130 + tmp131
  tmp133 = 1/tmp5 + 1/tmp85
  tmp134 = tmp78**2
  tmp135 = tmp76**2
  tmp136 = tmp44**2
  tmp137 = -4*me2
  tmp138 = tmp137 + 1/tmp3
  tmp139 = tmp138**(-2)
  tmp140 = 1/tmp138
  tmp141 = 4*tmp65
  tmp142 = tmp109 + tmp131 + tmp141
  tmp143 = tmp37**2
  tmp144 = -tmp143
  tmp145 = tmp144 + tmp55
  tmp146 = tmp68/tmp3
  tmp147 = (-2*mm2)/tmp3
  tmp148 = tmp109 + tmp131 + tmp146 + tmp147 + tmp62
  tmp149 = tmp148**(-2)
  tmp150 = 1/tmp37
  tmp151 = 1/tmp148
  tmp152 = -2*tmp2
  tmp153 = tmp152 + 1/tmp3
  tmp154 = -2*mm2
  tmp155 = tmp154 + 1/tmp67 + tmp89
  tmp156 = 1/tmp155
  tmp157 = tmp21*tmp70
  tmp158 = tmp36 + tmp51 + tmp68
  tmp159 = tmp158/tmp3
  tmp160 = tmp109 + tmp157 + tmp159
  tmp161 = me2 + mm2
  tmp162 = (-4*tmp161)/tmp3
  tmp163 = tmp91/tmp67
  tmp164 = 3*tmp109
  tmp165 = tmp141 + tmp162 + tmp163 + tmp164
  tmp166 = tmp156**2
  tmp167 = -tmp9
  tmp168 = tmp11 + tmp167 + tmp60
  tmp169 = tmp168*tmp21
  tmp170 = 3*tmp59
  tmp171 = tmp36/tmp67
  tmp172 = tmp36 + 1/tmp67
  tmp173 = tmp172*tmp68
  tmp174 = tmp170 + tmp171 + tmp173 + tmp53 + tmp9
  tmp175 = tmp174/tmp3
  tmp176 = tmp54 + 1/tmp67
  tmp177 = tmp109*tmp176
  tmp178 = tmp169 + tmp175 + tmp177
  tmp179 = tmp67**2
  tmp180 = 16*me2*tmp65
  tmp181 = tmp106/tmp3
  tmp182 = 6*tmp107*tmp109
  tmp183 = 1/tmp129
  tmp184 = -3*tmp183
  tmp185 = tmp180 + tmp181 + tmp182 + tmp184
  tmp186 = -tmp3
  tmp187 = tmp140*tmp3*tmp89
  tmp188 = tmp186 + tmp187
  tmp189 = -tmp150
  tmp190 = tmp151*tmp24
  tmp191 = tmp189 + tmp190
  tmp192 = 1/tmp3 + 1/tmp67 + tmp7
  tmp193 = tmp192**2
  tmp194 = mm2 + 1/tmp3 + 1/tmp67
  tmp195 = tmp194*tmp68
  tmp196 = tmp193 + tmp195 + tmp9
  tmp197 = 1/tmp196
  tmp198 = 1/tmp67 + tmp7
  tmp199 = tmp198*tmp81
  tmp200 = -3/tmp67
  tmp201 = 1/tmp3 + 1/tmp67
  tmp202 = -3*tmp201
  tmp203 = tmp200 + tmp202 + 1/tmp3 + tmp36
  tmp204 = me2*tmp203
  tmp205 = tmp100 + tmp104 + tmp109 + tmp131 + tmp199 + tmp204
  tmp206 = me2**3
  tmp207 = tmp10**3
  tmp208 = tmp56 + tmp59
  tmp209 = tmp208/tmp3
  tmp210 = tmp200 + 1/tmp3 + tmp7
  tmp211 = tmp210*tmp9
  tmp212 = -3*tmp53
  tmp213 = tmp201*tmp36
  tmp214 = tmp212 + tmp213 + tmp59
  tmp215 = tmp214*tmp6
  tmp216 = tmp206 + tmp207 + tmp209 + tmp211 + tmp215
  tmp217 = 3/tmp67
  tmp218 = 2*tmp192
  tmp219 = tmp218 + tmp68
  tmp220 = tmp197**2
  tmp221 = -tmp206
  tmp222 = -tmp207
  tmp223 = mm2 + tmp217
  tmp224 = tmp223*tmp9
  tmp225 = tmp199/tmp67
  tmp226 = tmp109*tmp12
  tmp227 = -4/tmp3
  tmp228 = tmp227 + 1/tmp67
  tmp229 = tmp228*tmp36
  tmp230 = tmp18 + tmp217
  tmp231 = -(tmp201*tmp230)
  tmp232 = tmp229 + tmp231 + tmp59
  tmp233 = me2*tmp232
  tmp234 = tmp221 + tmp222 + tmp224 + tmp225 + tmp226 + tmp233
  LBK_NLP_Qe3Qm = -32*scalarc0tmm*tmp123*tmp125*tmp128*tmp16*tmp3*tmp41*tmp5 - 32*discbu*tmp16*tmp&
                   &197*tmp234*tmp3*tmp41*tmp5 + 32*scalarc0ir6u*tmp16*tmp23*tmp24*tmp3*tmp41*tmp5 +&
                   & (16*discbu*logmm*tmp16*tmp23*tmp24*tmp3*tmp31*tmp35*tmp41*tmp5)/tmp150 - 32*sca&
                   &larc0tme*tmp121*tmp123*tmp16*tmp3*tmp41*tmp44*tmp5 - 32*logt*tmp123*tmp16*tmp3*t&
                   &mp41*tmp44*tmp45*tmp46*tmp49*tmp5 - 64*tmp129*tmp132*tmp16*tmp50 - 32*logmume*tm&
                   &p129*tmp132*tmp16*tmp50 - 32*scalarc0tmm*tmp125*tmp128*tmp16*tmp3*tmp50 + 8*logm&
                   &m*tmp125*tmp153*tmp156*tmp16*tmp178*tmp179*tmp3*tmp50 - 32*scalarc0ir6s*tmp16*tm&
                   &p2*tmp3*tmp50 + 16*discbu*tmp16*tmp197*tmp205*tmp3*tmp50 + 16*scalarc0ir6u*tmp16&
                   &*tmp23*tmp3*tmp50 + 16*tmp150*tmp16*tmp197*tmp234*tmp3*tmp50 - 16*discbu*tmp16*t&
                   &mp191*tmp197*tmp234*tmp3*tmp50 - 16*discbu*tmp16*tmp219*tmp220*tmp234*tmp3*tmp50&
                   & + 32*scalarc0ir6u*tmp16*tmp24*tmp3*tmp50 + 16*discbu*tmp145*tmp149*tmp16*tmp23*&
                   &tmp24*tmp3*tmp50 + (8*discbu*logmm*tmp16*tmp23*tmp3*tmp31*tmp35*tmp50)/tmp150 + &
                   &(16*discbu*logmm*tmp16*tmp24*tmp3*tmp31*tmp35*tmp50)/tmp150 - 8*logmm*tmp16*tmp2&
                   &3*tmp24*tmp3*tmp31*tmp35*tmp50 + 8*discbu*logmm*tmp16*tmp23*tmp24*tmp3*tmp31*tmp&
                   &35*tmp50 + (8*discbu*logmm*tmp16*tmp191*tmp23*tmp24*tmp3*tmp31*tmp35*tmp50)/tmp1&
                   &50 - (8*discbu*logmm*tmp16*tmp23*tmp24*tmp3*tmp31*tmp38*tmp50)/tmp150 - (8*discb&
                   &u*logmm*tmp16*tmp23*tmp24*tmp3*tmp35*tmp39*tmp50)/tmp150 - 8*tmp129*tmp16*tmp185&
                   &*tmp44*tmp50 - 16*discbtme*tmp129*tmp16*tmp185*tmp44*tmp50 - 16*logmume*tmp129*t&
                   &mp132*tmp16*tmp19*tmp44*tmp50 - 32*discbtme*logmume*tmp129*tmp132*tmp16*tmp19*tm&
                   &p44*tmp50 - 32*scalarc0tme*tmp121*tmp16*tmp3*tmp44*tmp50 - 32*logt*tmp16*tmp3*tm&
                   &p44*tmp45*tmp46*tmp49*tmp50 + 64*tmp129*tmp132*tmp5*tmp50 + 32*logmume*tmp129*tm&
                   &p132*tmp5*tmp50 + 8*tmp129*tmp185*tmp44*tmp5*tmp50 + 16*discbtme*tmp129*tmp185*t&
                   &mp44*tmp5*tmp50 + 16*logmume*tmp129*tmp132*tmp19*tmp44*tmp5*tmp50 + 32*discbtme*&
                   &logmume*tmp129*tmp132*tmp19*tmp44*tmp5*tmp50 + 16*scalarc0ir6u*tmp16*tmp190*tmp2&
                   &3*tmp3*tmp50**2 + 32*scalarc0ir6s*tmp16*tmp2*tmp3*tmp41*tmp5*tmp52 + 16*scalarc0&
                   &ir6s*tmp16*tmp3*tmp50*tmp52 + 8*logmm*tmp15*tmp16*tmp2*tmp3*tmp50*tmp52 - 16*dis&
                   &cbs*tmp16*tmp2*tmp3*tmp50*tmp52*tmp57*tmp63 - 16*scalarc0ir6s*tmp16*tmp3*tmp50*t&
                   &mp52*tmp64*tmp65 + 8*logmm*tmp150*tmp16*tmp190*tmp23*tmp3*tmp50*tmp66 - (16*disc&
                   &bs*logmm*tmp15*tmp16*tmp2*tmp3*tmp50)/tmp67 + (16*discbs*logmm*tmp15*tmp16*tmp2*&
                   &tmp3*tmp41*tmp5*tmp52)/tmp67 + (8*discbs*logmm*tmp15*tmp16*tmp3*tmp50*tmp52)/tmp&
                   &67 + 16*logmm*tmp125*tmp153*tmp156*tmp16*tmp178*tmp3*tmp41*tmp5*tmp67 - 8*logmm*&
                   &tmp125*tmp153*tmp156*tmp16*tmp160*tmp3*tmp50*tmp67 - 16*logmm*tmp125*tmp156*tmp1&
                   &6*tmp178*tmp3*tmp50*tmp67 + 8*logmm*tmp125*tmp153*tmp16*tmp166*tmp178*tmp3*tmp50&
                   &*tmp67 + (8*discbs*logmm*tmp16*tmp2*tmp3*tmp50*tmp52*tmp70*tmp71)/tmp67 + 16*sca&
                   &larc0ir6s*tmp3*tmp5*tmp52*tmp65*tmp73 + 8*logmm*tmp2*tmp3*tmp5*tmp52*tmp66*tmp67&
                   &*tmp73 - 16*discbs*tmp134*tmp16*tmp216*tmp3*tmp50*tmp76 - 16*discbs*tmp135*tmp16&
                   &*tmp216*tmp3*tmp50*tmp78 + 32*discbs*tmp16*tmp216*tmp3*tmp41*tmp5*tmp76*tmp78 + &
                   &16*tmp16*tmp216*tmp3*tmp50*tmp67*tmp76*tmp78 - 16*discbs*tmp16*tmp216*tmp3*tmp50&
                   &*tmp74*tmp76*tmp78 + 64*tmp132*tmp16*tmp79 + 32*logmume*tmp132*tmp16*tmp79 + 16*&
                   &discbtme*tmp16*tmp185*tmp44*tmp79 - 8*discbtme*tmp118*tmp16*tmp19*tmp44*tmp79 + &
                   &32*discbtme*logmume*tmp132*tmp16*tmp19*tmp44*tmp79 + (64*discbu*logmume*tmp132*t&
                   &mp16*tmp24*tmp31*tmp35*tmp41*tmp5*tmp79)/tmp150 + (32*discbulogt*tmp16*tmp165*tm&
                   &p24*tmp31*tmp35*tmp41*tmp5*tmp79)/tmp150 + 16*discbtme*me2*tmp118*tmp44*tmp5*tmp&
                   &79 + 16*scalarc0ir6tme*tmp132*tmp16*tmp50*tmp79 + 8*discbtme*tmp136*tmp16*tmp185&
                   &*tmp50*tmp79 + 16*discbtme*logmume*tmp132*tmp136*tmp16*tmp19*tmp50*tmp79 + (32*d&
                   &iscbu*logmume*tmp132*tmp16*tmp31*tmp35*tmp50*tmp79)/tmp150 + (16*discbulogt*tmp1&
                   &6*tmp165*tmp31*tmp35*tmp50*tmp79)/tmp150 - 32*logmume*tmp132*tmp16*tmp24*tmp31*t&
                   &mp35*tmp50*tmp79 + 32*discbu*logmume*tmp132*tmp16*tmp24*tmp31*tmp35*tmp50*tmp79 &
                   &+ 16*discbulogt*tmp16*tmp165*tmp24*tmp31*tmp35*tmp50*tmp79 - 16*logt*tmp16*tmp16&
                   &5*tmp24*tmp31*tmp35*tmp50*tmp79 + (32*discbu*logmume*tmp132*tmp16*tmp191*tmp24*t&
                   &mp31*tmp35*tmp50*tmp79)/tmp150 + (16*discbulogt*tmp16*tmp165*tmp191*tmp24*tmp31*&
                   &tmp35*tmp50*tmp79)/tmp150 - (32*discbu*logmume*tmp132*tmp16*tmp24*tmp31*tmp38*tm&
                   &p50*tmp79)/tmp150 - (16*discbulogt*tmp16*tmp165*tmp24*tmp31*tmp38*tmp50*tmp79)/t&
                   &mp150 - (32*discbu*logmume*tmp132*tmp16*tmp24*tmp35*tmp39*tmp50*tmp79)/tmp150 - &
                   &(16*discbulogt*tmp16*tmp165*tmp24*tmp35*tmp39*tmp50*tmp79)/tmp150 + 8*discbtme*t&
                   &mp111*tmp16*tmp44*tmp50*tmp79 - 16*discbtme*logmume*tmp132*tmp16*tmp44*tmp50*tmp&
                   &79 + 8*discbtme*tmp16*tmp185*tmp188*tmp44*tmp50*tmp79 + 16*discbtme*logmume*tmp1&
                   &32*tmp16*tmp188*tmp19*tmp44*tmp50*tmp79 - 16*scalarc0ir6tme*tmp132*tmp5*tmp50*tm&
                   &p79 - 8*discbtme*tmp136*tmp185*tmp5*tmp50*tmp79 - 16*discbtme*logmume*tmp132*tmp&
                   &136*tmp19*tmp5*tmp50*tmp79 - 8*discbtme*tmp111*tmp44*tmp5*tmp50*tmp79 + 16*discb&
                   &tme*logmume*tmp132*tmp44*tmp5*tmp50*tmp79 - 8*discbtme*tmp185*tmp188*tmp44*tmp5*&
                   &tmp50*tmp79 - 16*discbtme*logmume*tmp132*tmp188*tmp19*tmp44*tmp5*tmp50*tmp79 - (&
                   &32*discbs*logmume*tmp132*tmp134*tmp16*tmp2*tmp50*tmp76*tmp79)/tmp67 - (16*discbs&
                   &logt*tmp134*tmp142*tmp16*tmp2*tmp50*tmp76*tmp79)/tmp67 - (32*discbs*logmume*tmp1&
                   &32*tmp135*tmp16*tmp2*tmp50*tmp78*tmp79)/tmp67 - (16*discbslogt*tmp135*tmp142*tmp&
                   &16*tmp2*tmp50*tmp78*tmp79)/tmp67 + 32*logmume*tmp132*tmp16*tmp2*tmp50*tmp76*tmp7&
                   &8*tmp79 - 32*discbs*logmume*tmp132*tmp16*tmp2*tmp50*tmp76*tmp78*tmp79 - 16*discb&
                   &slogt*tmp142*tmp16*tmp2*tmp50*tmp76*tmp78*tmp79 + 16*logt*tmp142*tmp16*tmp2*tmp5&
                   &0*tmp76*tmp78*tmp79 + (64*discbs*logmume*tmp132*tmp16*tmp2*tmp41*tmp5*tmp76*tmp7&
                   &8*tmp79)/tmp67 + (32*discbslogt*tmp142*tmp16*tmp2*tmp41*tmp5*tmp76*tmp78*tmp79)/&
                   &tmp67 + (32*discbs*logmume*tmp132*tmp16*tmp50*tmp76*tmp78*tmp79)/tmp67 + (16*dis&
                   &cbslogt*tmp142*tmp16*tmp50*tmp76*tmp78*tmp79)/tmp67 - (32*discbs*logmume*tmp132*&
                   &tmp16*tmp2*tmp50*tmp74*tmp76*tmp78*tmp79)/tmp67 - (16*discbslogt*tmp142*tmp16*tm&
                   &p2*tmp50*tmp74*tmp76*tmp78*tmp79)/tmp67 - 64*tmp129*tmp132*tmp16*tmp8 - 32*logmu&
                   &me*tmp129*tmp132*tmp16*tmp8 - 8*tmp129*tmp16*tmp185*tmp44*tmp8 - 16*discbtme*tmp&
                   &129*tmp16*tmp185*tmp44*tmp8 - 16*logmume*tmp129*tmp132*tmp16*tmp19*tmp44*tmp8 - &
                   &32*discbtme*logmume*tmp129*tmp132*tmp16*tmp19*tmp44*tmp8 + 64*tmp129*tmp132*tmp5&
                   &*tmp8 + 32*logmume*tmp129*tmp132*tmp5*tmp8 + 32*scalarc0tmm*tmp125*tmp128*tmp3*t&
                   &mp5*tmp8 - 8*logmm*tmp125*tmp153*tmp156*tmp178*tmp179*tmp3*tmp5*tmp8 + 32*scalar&
                   &c0ir6s*tmp2*tmp3*tmp5*tmp8 - 16*discbu*tmp197*tmp205*tmp3*tmp5*tmp8 - 16*scalarc&
                   &0ir6u*tmp23*tmp3*tmp5*tmp8 - 16*tmp150*tmp197*tmp234*tmp3*tmp5*tmp8 + 16*discbu*&
                   &tmp191*tmp197*tmp234*tmp3*tmp5*tmp8 + 16*discbu*tmp219*tmp220*tmp234*tmp3*tmp5*t&
                   &mp8 - 32*scalarc0ir6u*tmp24*tmp3*tmp5*tmp8 - 16*discbu*tmp145*tmp149*tmp23*tmp24&
                   &*tmp3*tmp5*tmp8 - (8*discbu*logmm*tmp23*tmp3*tmp31*tmp35*tmp5*tmp8)/tmp150 - (16&
                   &*discbu*logmm*tmp24*tmp3*tmp31*tmp35*tmp5*tmp8)/tmp150 + 8*logmm*tmp23*tmp24*tmp&
                   &3*tmp31*tmp35*tmp5*tmp8 - 8*discbu*logmm*tmp23*tmp24*tmp3*tmp31*tmp35*tmp5*tmp8 &
                   &- (8*discbu*logmm*tmp191*tmp23*tmp24*tmp3*tmp31*tmp35*tmp5*tmp8)/tmp150 + (8*dis&
                   &cbu*logmm*tmp23*tmp24*tmp3*tmp31*tmp38*tmp5*tmp8)/tmp150 + (8*discbu*logmm*tmp23&
                   &*tmp24*tmp3*tmp35*tmp39*tmp5*tmp8)/tmp150 + 8*tmp129*tmp185*tmp44*tmp5*tmp8 + 16&
                   &*discbtme*tmp129*tmp185*tmp44*tmp5*tmp8 + 16*logmume*tmp129*tmp132*tmp19*tmp44*t&
                   &mp5*tmp8 + 32*discbtme*logmume*tmp129*tmp132*tmp19*tmp44*tmp5*tmp8 + 32*scalarc0&
                   &tme*tmp121*tmp3*tmp44*tmp5*tmp8 + 32*logt*tmp3*tmp44*tmp45*tmp46*tmp49*tmp5*tmp8&
                   & - 16*scalarc0ir6u*tmp190*tmp23*tmp3*tmp5*tmp50*tmp8 - 16*scalarc0ir6s*tmp3*tmp5&
                   &*tmp52*tmp8 + 8*discbs*logmm*tmp15*tmp2*tmp3*tmp5*tmp52*tmp8 + 16*discbs*tmp2*tm&
                   &p3*tmp5*tmp52*tmp57*tmp63*tmp8 - 8*logmm*tmp150*tmp190*tmp23*tmp3*tmp5*tmp66*tmp&
                   &8 + (16*discbs*logmm*tmp15*tmp2*tmp3*tmp5*tmp8)/tmp67 - (8*discbs*logmm*tmp15*tm&
                   &p3*tmp5*tmp52*tmp8)/tmp67 + 8*logmm*tmp125*tmp153*tmp156*tmp160*tmp3*tmp5*tmp67*&
                   &tmp8 + 16*logmm*tmp125*tmp156*tmp178*tmp3*tmp5*tmp67*tmp8 - 8*logmm*tmp125*tmp15&
                   &3*tmp166*tmp178*tmp3*tmp5*tmp67*tmp8 + (8*discbs*logmm*tmp15*tmp2*tmp3*tmp5*tmp5&
                   &2*tmp74*tmp8)/tmp67 + 16*discbs*tmp134*tmp216*tmp3*tmp5*tmp76*tmp8 + 16*discbs*t&
                   &mp135*tmp216*tmp3*tmp5*tmp78*tmp8 - 16*tmp216*tmp3*tmp5*tmp67*tmp76*tmp78*tmp8 +&
                   & 16*discbs*tmp216*tmp3*tmp5*tmp74*tmp76*tmp78*tmp8 + 16*scalarc0ir6tme*tmp132*tm&
                   &p16*tmp79*tmp8 + 8*discbtme*tmp136*tmp16*tmp185*tmp79*tmp8 + 16*discbtme*logmume&
                   &*tmp132*tmp136*tmp16*tmp19*tmp79*tmp8 + 8*discbtme*tmp111*tmp16*tmp44*tmp79*tmp8&
                   & - 16*discbtme*logmume*tmp132*tmp16*tmp44*tmp79*tmp8 + 8*discbtme*tmp16*tmp185*t&
                   &mp188*tmp44*tmp79*tmp8 + 16*discbtme*logmume*tmp132*tmp16*tmp188*tmp19*tmp44*tmp&
                   &79*tmp8 - 16*scalarc0ir6tme*tmp132*tmp5*tmp79*tmp8 - 8*discbtme*tmp136*tmp185*tm&
                   &p5*tmp79*tmp8 - 16*discbtme*logmume*tmp132*tmp136*tmp19*tmp5*tmp79*tmp8 - (32*di&
                   &scbu*logmume*tmp132*tmp31*tmp35*tmp5*tmp79*tmp8)/tmp150 - (16*discbulogt*tmp165*&
                   &tmp31*tmp35*tmp5*tmp79*tmp8)/tmp150 + 32*logmume*tmp132*tmp24*tmp31*tmp35*tmp5*t&
                   &mp79*tmp8 - 32*discbu*logmume*tmp132*tmp24*tmp31*tmp35*tmp5*tmp79*tmp8 - 16*disc&
                   &bulogt*tmp165*tmp24*tmp31*tmp35*tmp5*tmp79*tmp8 + 16*logt*tmp165*tmp24*tmp31*tmp&
                   &35*tmp5*tmp79*tmp8 - (32*discbu*logmume*tmp132*tmp191*tmp24*tmp31*tmp35*tmp5*tmp&
                   &79*tmp8)/tmp150 - (16*discbulogt*tmp165*tmp191*tmp24*tmp31*tmp35*tmp5*tmp79*tmp8&
                   &)/tmp150 + (32*discbu*logmume*tmp132*tmp24*tmp31*tmp38*tmp5*tmp79*tmp8)/tmp150 +&
                   & (16*discbulogt*tmp165*tmp24*tmp31*tmp38*tmp5*tmp79*tmp8)/tmp150 + (32*discbu*lo&
                   &gmume*tmp132*tmp24*tmp35*tmp39*tmp5*tmp79*tmp8)/tmp150 + (16*discbulogt*tmp165*t&
                   &mp24*tmp35*tmp39*tmp5*tmp79*tmp8)/tmp150 - 8*discbtme*tmp111*tmp44*tmp5*tmp79*tm&
                   &p8 - 8*discbtme*tmp118*tmp44*tmp5*tmp79*tmp8 + 16*discbtme*logmume*tmp132*tmp44*&
                   &tmp5*tmp79*tmp8 - 8*discbtme*tmp185*tmp188*tmp44*tmp5*tmp79*tmp8 - 16*discbtme*l&
                   &ogmume*tmp132*tmp188*tmp19*tmp44*tmp5*tmp79*tmp8 + (32*discbs*logmume*tmp132*tmp&
                   &134*tmp2*tmp5*tmp76*tmp79*tmp8)/tmp67 + (16*discbslogt*tmp134*tmp142*tmp2*tmp5*t&
                   &mp76*tmp79*tmp8)/tmp67 + (32*discbs*logmume*tmp132*tmp135*tmp2*tmp5*tmp78*tmp79*&
                   &tmp8)/tmp67 + (16*discbslogt*tmp135*tmp142*tmp2*tmp5*tmp78*tmp79*tmp8)/tmp67 - 3&
                   &2*logmume*tmp132*tmp2*tmp5*tmp76*tmp78*tmp79*tmp8 + 32*discbs*logmume*tmp132*tmp&
                   &2*tmp5*tmp76*tmp78*tmp79*tmp8 + 16*discbslogt*tmp142*tmp2*tmp5*tmp76*tmp78*tmp79&
                   &*tmp8 - 16*logt*tmp142*tmp2*tmp5*tmp76*tmp78*tmp79*tmp8 - (32*discbs*logmume*tmp&
                   &132*tmp5*tmp76*tmp78*tmp79*tmp8)/tmp67 - (16*discbslogt*tmp142*tmp5*tmp76*tmp78*&
                   &tmp79*tmp8)/tmp67 + (32*discbs*logmume*tmp132*tmp2*tmp5*tmp74*tmp76*tmp78*tmp79*&
                   &tmp8)/tmp67 + (16*discbslogt*tmp142*tmp2*tmp5*tmp74*tmp76*tmp78*tmp79*tmp8)/tmp6&
                   &7 + discbs*logmm*tmp15*tmp16*tmp3*tmp50*tmp52*tmp80 + logmm*tmp16*tmp3*tmp50*tmp&
                   &52*tmp64*tmp66*tmp67*tmp80 + (discbs*logmm*tmp15*tmp16*tmp3*tmp50*tmp52*tmp74*tm&
                   &p80)/tmp67 + logmm*tmp15*tmp3*tmp5*tmp52*tmp8*tmp80 + (discbs*logmm*tmp3*tmp5*tm&
                   &p52*tmp70*tmp71*tmp8*tmp80)/tmp67 - (16*discbslogt*tmp16*tmp2*tmp50*tmp76*tmp78*&
                   &tmp79*tmp82)/tmp67 + (16*discbslogt*tmp2*tmp5*tmp76*tmp78*tmp79*tmp8*tmp82)/tmp6&
                   &7 - 32*tmp16*tmp19*tmp79*tmp84 - 16*logmume*tmp16*tmp19*tmp79*tmp84 - 16*discbtm&
                   &e*logmume*tmp16*tmp19**2*tmp44*tmp79*tmp84 + 64*me2*tmp5*tmp79*tmp84 + 32*logmum&
                   &e*me2*tmp5*tmp79*tmp84 + 32*discbtme*logmume*me2*tmp19*tmp44*tmp5*tmp79*tmp84 - &
                   &(32*discbu*logmume*tmp16*tmp24*tmp31*tmp35*tmp50*tmp79*tmp84)/tmp150 - (32*discb&
                   &s*logmume*tmp16*tmp2*tmp50*tmp76*tmp78*tmp79*tmp84)/tmp67 - 32*tmp5*tmp79*tmp8*t&
                   &mp84 - 16*logmume*tmp5*tmp79*tmp8*tmp84 + (32*discbu*logmume*tmp24*tmp31*tmp35*t&
                   &mp5*tmp79*tmp8*tmp84)/tmp150 - 16*discbtme*logmume*tmp19*tmp44*tmp5*tmp79*tmp8*t&
                   &mp84 + (32*discbs*logmume*tmp2*tmp5*tmp76*tmp78*tmp79*tmp8*tmp84)/tmp67 - (64*me&
                   &2*scalarc0tmm*tmp125*tmp128*tmp3*tmp4)/tmp85 + (16*logmm*me2*tmp125*tmp153*tmp15&
                   &6*tmp178*tmp179*tmp3*tmp4)/tmp85 - (64*me2*scalarc0ir6s*tmp2*tmp3*tmp4)/tmp85 + &
                   &(32*discbu*me2*tmp197*tmp205*tmp3*tmp4)/tmp85 + (32*me2*scalarc0ir6u*tmp23*tmp3*&
                   &tmp4)/tmp85 + (32*me2*tmp150*tmp197*tmp234*tmp3*tmp4)/tmp85 - (32*discbu*me2*tmp&
                   &191*tmp197*tmp234*tmp3*tmp4)/tmp85 - (32*discbu*me2*tmp219*tmp220*tmp234*tmp3*tm&
                   &p4)/tmp85 + (64*me2*scalarc0ir6u*tmp24*tmp3*tmp4)/tmp85 + (32*discbu*me2*tmp145*&
                   &tmp149*tmp23*tmp24*tmp3*tmp4)/tmp85 + (16*discbu*logmm*me2*tmp23*tmp3*tmp31*tmp3&
                   &5*tmp4)/(tmp150*tmp85) + (32*discbu*logmm*me2*tmp24*tmp3*tmp31*tmp35*tmp4)/(tmp1&
                   &50*tmp85) - (16*logmm*me2*tmp23*tmp24*tmp3*tmp31*tmp35*tmp4)/tmp85 + (16*discbu*&
                   &logmm*me2*tmp23*tmp24*tmp3*tmp31*tmp35*tmp4)/tmp85 + (16*discbu*logmm*me2*tmp191&
                   &*tmp23*tmp24*tmp3*tmp31*tmp35*tmp4)/(tmp150*tmp85) - (16*discbu*logmm*me2*tmp23*&
                   &tmp24*tmp3*tmp31*tmp38*tmp4)/(tmp150*tmp85) - (16*discbu*logmm*me2*tmp23*tmp24*t&
                   &mp3*tmp35*tmp39*tmp4)/(tmp150*tmp85) - (64*me2*scalarc0tme*tmp121*tmp3*tmp4*tmp4&
                   &4)/tmp85 - (64*logt*me2*tmp3*tmp4*tmp44*tmp45*tmp46*tmp49)/tmp85 + (32*scalarc0t&
                   &mm*tmp125*tmp128*tmp16*tmp19*tmp3*tmp5)/tmp85 - (8*logmm*tmp125*tmp153*tmp156*tm&
                   &p16*tmp178*tmp179*tmp19*tmp3*tmp5)/tmp85 + (32*scalarc0ir6s*tmp16*tmp19*tmp2*tmp&
                   &3*tmp5)/tmp85 - (16*discbu*tmp16*tmp19*tmp197*tmp205*tmp3*tmp5)/tmp85 - (16*scal&
                   &arc0ir6u*tmp16*tmp19*tmp23*tmp3*tmp5)/tmp85 - (16*tmp150*tmp16*tmp19*tmp197*tmp2&
                   &34*tmp3*tmp5)/tmp85 + (16*discbu*tmp16*tmp19*tmp191*tmp197*tmp234*tmp3*tmp5)/tmp&
                   &85 + (16*discbu*tmp16*tmp19*tmp219*tmp220*tmp234*tmp3*tmp5)/tmp85 - (32*scalarc0&
                   &ir6u*tmp16*tmp19*tmp24*tmp3*tmp5)/tmp85 - (16*discbu*tmp145*tmp149*tmp16*tmp19*t&
                   &mp23*tmp24*tmp3*tmp5)/tmp85 - (8*discbu*logmm*tmp16*tmp19*tmp23*tmp3*tmp31*tmp35&
                   &*tmp5)/(tmp150*tmp85) - (16*discbu*logmm*tmp16*tmp19*tmp24*tmp3*tmp31*tmp35*tmp5&
                   &)/(tmp150*tmp85) + (8*logmm*tmp16*tmp19*tmp23*tmp24*tmp3*tmp31*tmp35*tmp5)/tmp85&
                   & - (8*discbu*logmm*tmp16*tmp19*tmp23*tmp24*tmp3*tmp31*tmp35*tmp5)/tmp85 - (8*dis&
                   &cbu*logmm*tmp16*tmp19*tmp191*tmp23*tmp24*tmp3*tmp31*tmp35*tmp5)/(tmp150*tmp85) +&
                   & (8*discbu*logmm*tmp16*tmp19*tmp23*tmp24*tmp3*tmp31*tmp38*tmp5)/(tmp150*tmp85) +&
                   & (8*discbu*logmm*tmp16*tmp19*tmp23*tmp24*tmp3*tmp35*tmp39*tmp5)/(tmp150*tmp85) +&
                   & (32*scalarc0tme*tmp121*tmp16*tmp19*tmp3*tmp44*tmp5)/tmp85 + (32*logt*tmp16*tmp1&
                   &9*tmp3*tmp44*tmp45*tmp46*tmp49*tmp5)/tmp85 + (32*me2*scalarc0ir6u*tmp190*tmp23*t&
                   &mp3*tmp4*tmp50)/tmp85 - (16*scalarc0ir6u*tmp16*tmp19*tmp190*tmp23*tmp3*tmp5*tmp5&
                   &0)/tmp85 + (32*me2*scalarc0ir6s*tmp3*tmp4*tmp52)/tmp85 + (16*logmm*me2*tmp15*tmp&
                   &2*tmp3*tmp4*tmp52)/tmp85 - (16*discbs*logmm*me2*tmp15*tmp2*tmp3*tmp4*tmp52)/tmp8&
                   &5 - (16*scalarc0ir6s*tmp16*tmp19*tmp3*tmp5*tmp52)/tmp85 + (8*discbs*logmm*tmp15*&
                   &tmp16*tmp19*tmp2*tmp3*tmp5*tmp52)/tmp85 + (discbs*tmp112*tmp3*tmp4*tmp52*tmp57*t&
                   &mp63)/tmp85 + (16*discbs*tmp16*tmp19*tmp2*tmp3*tmp5*tmp52*tmp57*tmp63)/tmp85 - (&
                   &32*me2*scalarc0ir6s*tmp3*tmp4*tmp52*tmp64*tmp65)/tmp85 + (16*scalarc0ir6s*tmp16*&
                   &tmp19*tmp3*tmp5*tmp52*tmp64*tmp65)/tmp85 + (16*logmm*me2*tmp150*tmp190*tmp23*tmp&
                   &3*tmp4*tmp66)/tmp85 - (8*logmm*tmp150*tmp16*tmp19*tmp190*tmp23*tmp3*tmp5*tmp66)/&
                   &tmp85 + (discbs*logmm*tmp112*tmp15*tmp3*tmp4)/(tmp67*tmp85) + (16*discbs*logmm*t&
                   &mp15*tmp16*tmp19*tmp2*tmp3*tmp5)/(tmp67*tmp85) + (16*discbs*logmm*me2*tmp15*tmp3&
                   &*tmp4*tmp52)/(tmp67*tmp85) - (8*discbs*logmm*tmp15*tmp16*tmp19*tmp3*tmp5*tmp52)/&
                   &(tmp67*tmp85) - (16*logmm*me2*tmp125*tmp153*tmp156*tmp160*tmp3*tmp4*tmp67)/tmp85&
                   & - (32*logmm*me2*tmp125*tmp156*tmp178*tmp3*tmp4*tmp67)/tmp85 + (16*logmm*me2*tmp&
                   &125*tmp153*tmp166*tmp178*tmp3*tmp4*tmp67)/tmp85 + (8*logmm*tmp125*tmp153*tmp156*&
                   &tmp16*tmp160*tmp19*tmp3*tmp5*tmp67)/tmp85 + (16*logmm*tmp125*tmp156*tmp16*tmp178&
                   &*tmp19*tmp3*tmp5*tmp67)/tmp85 - (8*logmm*tmp125*tmp153*tmp16*tmp166*tmp178*tmp19&
                   &*tmp3*tmp5*tmp67)/tmp85 - (16*logmm*me2*tmp2*tmp3*tmp4*tmp52*tmp64*tmp66*tmp67)/&
                   &tmp85 + (8*logmm*tmp16*tmp19*tmp2*tmp3*tmp5*tmp52*tmp64*tmp66*tmp67)/tmp85 + (16&
                   &*discbs*logmm*me2*tmp2*tmp3*tmp4*tmp52*tmp70*tmp71)/(tmp67*tmp85) - (16*discbs*l&
                   &ogmm*me2*tmp15*tmp2*tmp3*tmp4*tmp52*tmp74)/(tmp67*tmp85) + (8*discbs*logmm*tmp15&
                   &*tmp16*tmp19*tmp2*tmp3*tmp5*tmp52*tmp74)/(tmp67*tmp85) - (32*discbs*me2*tmp134*t&
                   &mp216*tmp3*tmp4*tmp76)/tmp85 + (16*discbs*tmp134*tmp16*tmp19*tmp216*tmp3*tmp5*tm&
                   &p76)/tmp85 - (32*discbs*me2*tmp135*tmp216*tmp3*tmp4*tmp78)/tmp85 + (16*discbs*tm&
                   &p135*tmp16*tmp19*tmp216*tmp3*tmp5*tmp78)/tmp85 + (32*me2*tmp216*tmp3*tmp4*tmp67*&
                   &tmp76*tmp78)/tmp85 - (16*tmp16*tmp19*tmp216*tmp3*tmp5*tmp67*tmp76*tmp78)/tmp85 -&
                   & (32*discbs*me2*tmp216*tmp3*tmp4*tmp74*tmp76*tmp78)/tmp85 + (16*discbs*tmp16*tmp&
                   &19*tmp216*tmp3*tmp5*tmp74*tmp76*tmp78)/tmp85 + (64*discbu*logmume*me2*tmp132*tmp&
                   &31*tmp35*tmp4*tmp79)/(tmp150*tmp85) + (32*discbulogt*me2*tmp165*tmp31*tmp35*tmp4&
                   &*tmp79)/(tmp150*tmp85) - (64*logmume*me2*tmp132*tmp24*tmp31*tmp35*tmp4*tmp79)/tm&
                   &p85 + (64*discbu*logmume*me2*tmp132*tmp24*tmp31*tmp35*tmp4*tmp79)/tmp85 + (32*di&
                   &scbulogt*me2*tmp165*tmp24*tmp31*tmp35*tmp4*tmp79)/tmp85 - (32*logt*me2*tmp165*tm&
                   &p24*tmp31*tmp35*tmp4*tmp79)/tmp85 + (64*discbu*logmume*me2*tmp132*tmp191*tmp24*t&
                   &mp31*tmp35*tmp4*tmp79)/(tmp150*tmp85) + (32*discbulogt*me2*tmp165*tmp191*tmp24*t&
                   &mp31*tmp35*tmp4*tmp79)/(tmp150*tmp85) - (64*discbu*logmume*me2*tmp132*tmp24*tmp3&
                   &1*tmp38*tmp4*tmp79)/(tmp150*tmp85) - (32*discbulogt*me2*tmp165*tmp24*tmp31*tmp38&
                   &*tmp4*tmp79)/(tmp150*tmp85) - (64*discbu*logmume*me2*tmp132*tmp24*tmp35*tmp39*tm&
                   &p4*tmp79)/(tmp150*tmp85) - (32*discbulogt*me2*tmp165*tmp24*tmp35*tmp39*tmp4*tmp7&
                   &9)/(tmp150*tmp85) - (32*discbu*logmume*tmp132*tmp16*tmp19*tmp31*tmp35*tmp5*tmp79&
                   &)/(tmp150*tmp85) - (16*discbulogt*tmp16*tmp165*tmp19*tmp31*tmp35*tmp5*tmp79)/(tm&
                   &p150*tmp85) + (32*logmume*tmp132*tmp16*tmp19*tmp24*tmp31*tmp35*tmp5*tmp79)/tmp85&
                   & - (32*discbu*logmume*tmp132*tmp16*tmp19*tmp24*tmp31*tmp35*tmp5*tmp79)/tmp85 - (&
                   &16*discbulogt*tmp16*tmp165*tmp19*tmp24*tmp31*tmp35*tmp5*tmp79)/tmp85 + (16*logt*&
                   &tmp16*tmp165*tmp19*tmp24*tmp31*tmp35*tmp5*tmp79)/tmp85 - (32*discbu*logmume*tmp1&
                   &32*tmp16*tmp19*tmp191*tmp24*tmp31*tmp35*tmp5*tmp79)/(tmp150*tmp85) - (16*discbul&
                   &ogt*tmp16*tmp165*tmp19*tmp191*tmp24*tmp31*tmp35*tmp5*tmp79)/(tmp150*tmp85) + (32&
                   &*discbu*logmume*tmp132*tmp16*tmp19*tmp24*tmp31*tmp38*tmp5*tmp79)/(tmp150*tmp85) &
                   &+ (16*discbulogt*tmp16*tmp165*tmp19*tmp24*tmp31*tmp38*tmp5*tmp79)/(tmp150*tmp85)&
                   & + (32*discbu*logmume*tmp132*tmp16*tmp19*tmp24*tmp35*tmp39*tmp5*tmp79)/(tmp150*t&
                   &mp85) + (16*discbulogt*tmp16*tmp165*tmp19*tmp24*tmp35*tmp39*tmp5*tmp79)/(tmp150*&
                   &tmp85) + (discbslogt*tmp112*tmp134*tmp142*tmp4*tmp76*tmp79)/(tmp67*tmp85) - (64*&
                   &discbs*logmume*me2*tmp132*tmp134*tmp2*tmp4*tmp76*tmp79)/(tmp67*tmp85) + (32*disc&
                   &bs*logmume*tmp132*tmp134*tmp16*tmp19*tmp2*tmp5*tmp76*tmp79)/(tmp67*tmp85) + (16*&
                   &discbslogt*tmp134*tmp142*tmp16*tmp19*tmp2*tmp5*tmp76*tmp79)/(tmp67*tmp85) + (dis&
                   &cbslogt*tmp112*tmp135*tmp142*tmp4*tmp78*tmp79)/(tmp67*tmp85) - (64*discbs*logmum&
                   &e*me2*tmp132*tmp135*tmp2*tmp4*tmp78*tmp79)/(tmp67*tmp85) + (32*discbs*logmume*tm&
                   &p132*tmp135*tmp16*tmp19*tmp2*tmp5*tmp78*tmp79)/(tmp67*tmp85) + (16*discbslogt*tm&
                   &p135*tmp142*tmp16*tmp19*tmp2*tmp5*tmp78*tmp79)/(tmp67*tmp85) + (discbslogt*tmp11&
                   &2*tmp142*tmp4*tmp76*tmp78*tmp79)/tmp85 + (64*logmume*me2*tmp132*tmp2*tmp4*tmp76*&
                   &tmp78*tmp79)/tmp85 - (64*discbs*logmume*me2*tmp132*tmp2*tmp4*tmp76*tmp78*tmp79)/&
                   &tmp85 + (32*logt*me2*tmp142*tmp2*tmp4*tmp76*tmp78*tmp79)/tmp85 - (32*logmume*tmp&
                   &132*tmp16*tmp19*tmp2*tmp5*tmp76*tmp78*tmp79)/tmp85 + (32*discbs*logmume*tmp132*t&
                   &mp16*tmp19*tmp2*tmp5*tmp76*tmp78*tmp79)/tmp85 + (16*discbslogt*tmp142*tmp16*tmp1&
                   &9*tmp2*tmp5*tmp76*tmp78*tmp79)/tmp85 - (16*logt*tmp142*tmp16*tmp19*tmp2*tmp5*tmp&
                   &76*tmp78*tmp79)/tmp85 + (64*discbs*logmume*me2*tmp132*tmp4*tmp76*tmp78*tmp79)/(t&
                   &mp67*tmp85) + (32*discbslogt*me2*tmp142*tmp4*tmp76*tmp78*tmp79)/(tmp67*tmp85) - &
                   &(32*discbs*logmume*tmp132*tmp16*tmp19*tmp5*tmp76*tmp78*tmp79)/(tmp67*tmp85) - (1&
                   &6*discbslogt*tmp142*tmp16*tmp19*tmp5*tmp76*tmp78*tmp79)/(tmp67*tmp85) + (discbsl&
                   &ogt*tmp112*tmp142*tmp4*tmp74*tmp76*tmp78*tmp79)/(tmp67*tmp85) - (64*discbs*logmu&
                   &me*me2*tmp132*tmp2*tmp4*tmp74*tmp76*tmp78*tmp79)/(tmp67*tmp85) + (32*discbs*logm&
                   &ume*tmp132*tmp16*tmp19*tmp2*tmp5*tmp74*tmp76*tmp78*tmp79)/(tmp67*tmp85) + (16*di&
                   &scbslogt*tmp142*tmp16*tmp19*tmp2*tmp5*tmp74*tmp76*tmp78*tmp79)/(tmp67*tmp85) + (&
                   &logmm*tmp15*tmp16*tmp19*tmp3*tmp5*tmp52*tmp80)/tmp85 + (discbs*logmm*tmp16*tmp19&
                   &*tmp3*tmp5*tmp52*tmp70*tmp71*tmp80)/(tmp67*tmp85) + (discbslogt*tmp112*tmp4*tmp7&
                   &6*tmp78*tmp79*tmp82)/(tmp67*tmp85) + (16*discbslogt*tmp16*tmp19*tmp2*tmp5*tmp76*&
                   &tmp78*tmp79*tmp82)/(tmp67*tmp85) - (64*discbu*logmume*me2*tmp24*tmp31*tmp35*tmp4&
                   &*tmp79*tmp84)/(tmp150*tmp85) + (32*discbu*logmume*tmp16*tmp19*tmp24*tmp31*tmp35*&
                   &tmp5*tmp79*tmp84)/(tmp150*tmp85) - (64*discbs*logmume*me2*tmp2*tmp4*tmp76*tmp78*&
                   &tmp79*tmp84)/(tmp67*tmp85) + (32*discbs*logmume*tmp16*tmp19*tmp2*tmp5*tmp76*tmp7&
                   &8*tmp79*tmp84)/(tmp67*tmp85) + 16*discbtme*mm2*tmp118*tmp44*tmp79*tmp85 + (8*dis&
                   &cbtme*tmp118*tmp16*tmp44*tmp50*tmp79*tmp85)/tmp5 - 8*discbtme*tmp118*tmp44*tmp79&
                   &*tmp8*tmp85 + 64*mm2*tmp79*tmp84*tmp85 + 32*logmume*mm2*tmp79*tmp84*tmp85 + 32*d&
                   &iscbtme*logmume*mm2*tmp19*tmp44*tmp79*tmp84*tmp85 + (32*tmp16*tmp50*tmp79*tmp84*&
                   &tmp85)/tmp5 + (16*logmume*tmp16*tmp50*tmp79*tmp84*tmp85)/tmp5 + (16*discbtme*log&
                   &mume*tmp16*tmp19*tmp44*tmp50*tmp79*tmp84*tmp85)/tmp5 - 32*tmp79*tmp8*tmp84*tmp85&
                   & - 16*logmume*tmp79*tmp8*tmp84*tmp85 - 16*discbtme*logmume*tmp19*tmp44*tmp79*tmp&
                   &8*tmp84*tmp85 + (64*tmp129*tmp132*tmp16*tmp50*tmp85)/tmp87 + (32*logmume*tmp129*&
                   &tmp132*tmp16*tmp50*tmp85)/tmp87 + (8*tmp129*tmp16*tmp185*tmp44*tmp50*tmp85)/tmp8&
                   &7 + (16*discbtme*tmp129*tmp16*tmp185*tmp44*tmp50*tmp85)/tmp87 + (16*logmume*tmp1&
                   &29*tmp132*tmp16*tmp19*tmp44*tmp50*tmp85)/tmp87 + (32*discbtme*logmume*tmp129*tmp&
                   &132*tmp16*tmp19*tmp44*tmp50*tmp85)/tmp87 - (16*scalarc0ir6tme*tmp132*tmp16*tmp50&
                   &*tmp79*tmp85)/tmp87 - (8*discbtme*tmp136*tmp16*tmp185*tmp50*tmp79*tmp85)/tmp87 -&
                   & (16*discbtme*logmume*tmp132*tmp136*tmp16*tmp19*tmp50*tmp79*tmp85)/tmp87 - (8*di&
                   &scbtme*tmp111*tmp16*tmp44*tmp50*tmp79*tmp85)/tmp87 + (16*discbtme*logmume*tmp132&
                   &*tmp16*tmp44*tmp50*tmp79*tmp85)/tmp87 - (8*discbtme*tmp16*tmp185*tmp188*tmp44*tm&
                   &p50*tmp79*tmp85)/tmp87 - (16*discbtme*logmume*tmp132*tmp16*tmp188*tmp19*tmp44*tm&
                   &p50*tmp79*tmp85)/tmp87 - (64*tmp129*tmp132*tmp5*tmp8*tmp85)/tmp87 - (32*logmume*&
                   &tmp129*tmp132*tmp5*tmp8*tmp85)/tmp87 - (8*tmp129*tmp185*tmp44*tmp5*tmp8*tmp85)/t&
                   &mp87 - (16*discbtme*tmp129*tmp185*tmp44*tmp5*tmp8*tmp85)/tmp87 - (16*logmume*tmp&
                   &129*tmp132*tmp19*tmp44*tmp5*tmp8*tmp85)/tmp87 - (32*discbtme*logmume*tmp129*tmp1&
                   &32*tmp19*tmp44*tmp5*tmp8*tmp85)/tmp87 + (16*scalarc0ir6tme*tmp132*tmp5*tmp79*tmp&
                   &8*tmp85)/tmp87 + (8*discbtme*tmp136*tmp185*tmp5*tmp79*tmp8*tmp85)/tmp87 + (16*di&
                   &scbtme*logmume*tmp132*tmp136*tmp19*tmp5*tmp79*tmp8*tmp85)/tmp87 + (8*discbtme*tm&
                   &p111*tmp44*tmp5*tmp79*tmp8*tmp85)/tmp87 - (16*discbtme*logmume*tmp132*tmp44*tmp5&
                   &*tmp79*tmp8*tmp85)/tmp87 + (8*discbtme*tmp185*tmp188*tmp44*tmp5*tmp79*tmp8*tmp85&
                   &)/tmp87 + (16*discbtme*logmume*tmp132*tmp188*tmp19*tmp44*tmp5*tmp79*tmp8*tmp85)/&
                   &tmp87 - 64*tmp132*tmp133*tmp16*tmp79*tmp87 - 32*logmume*tmp132*tmp133*tmp16*tmp7&
                   &9*tmp87 - 16*discbtme*tmp133*tmp16*tmp185*tmp44*tmp79*tmp87 - 32*discbtme*logmum&
                   &e*tmp132*tmp133*tmp16*tmp19*tmp44*tmp79*tmp87 + (64*tmp132*tmp5*tmp79*tmp87)/tmp&
                   &16 + (32*logmume*tmp132*tmp5*tmp79*tmp87)/tmp16 + (16*discbtme*tmp185*tmp44*tmp5&
                   &*tmp79*tmp87)/tmp16 + (32*discbtme*logmume*tmp132*tmp19*tmp44*tmp5*tmp79*tmp87)/&
                   &tmp16 - (64*tmp129*tmp132*tmp5*tmp50*tmp87)/tmp85 - (32*logmume*tmp129*tmp132*tm&
                   &p5*tmp50*tmp87)/tmp85 - (8*tmp129*tmp185*tmp44*tmp5*tmp50*tmp87)/tmp85 - (16*dis&
                   &cbtme*tmp129*tmp185*tmp44*tmp5*tmp50*tmp87)/tmp85 - (16*logmume*tmp129*tmp132*tm&
                   &p19*tmp44*tmp5*tmp50*tmp87)/tmp85 - (32*discbtme*logmume*tmp129*tmp132*tmp19*tmp&
                   &44*tmp5*tmp50*tmp87)/tmp85 + (16*scalarc0ir6tme*tmp132*tmp5*tmp50*tmp79*tmp87)/t&
                   &mp85 + (8*discbtme*tmp136*tmp185*tmp5*tmp50*tmp79*tmp87)/tmp85 + (16*discbtme*lo&
                   &gmume*tmp132*tmp136*tmp19*tmp5*tmp50*tmp79*tmp87)/tmp85 + (8*discbtme*tmp111*tmp&
                   &44*tmp5*tmp50*tmp79*tmp87)/tmp85 + (8*discbtme*tmp118*tmp44*tmp5*tmp50*tmp79*tmp&
                   &87)/tmp85 - (16*discbtme*logmume*tmp132*tmp44*tmp5*tmp50*tmp79*tmp87)/tmp85 + (8&
                   &*discbtme*tmp185*tmp188*tmp44*tmp5*tmp50*tmp79*tmp87)/tmp85 + (16*discbtme*logmu&
                   &me*tmp132*tmp188*tmp19*tmp44*tmp5*tmp50*tmp79*tmp87)/tmp85 + (64*tmp129*tmp132*t&
                   &mp16*tmp8*tmp87)/tmp85 + (32*logmume*tmp129*tmp132*tmp16*tmp8*tmp87)/tmp85 + (8*&
                   &tmp129*tmp16*tmp185*tmp44*tmp8*tmp87)/tmp85 + (16*discbtme*tmp129*tmp16*tmp185*t&
                   &mp44*tmp8*tmp87)/tmp85 + (16*logmume*tmp129*tmp132*tmp16*tmp19*tmp44*tmp8*tmp87)&
                   &/tmp85 + (32*discbtme*logmume*tmp129*tmp132*tmp16*tmp19*tmp44*tmp8*tmp87)/tmp85 &
                   &- (16*scalarc0ir6tme*tmp132*tmp16*tmp79*tmp8*tmp87)/tmp85 - (8*discbtme*tmp136*t&
                   &mp16*tmp185*tmp79*tmp8*tmp87)/tmp85 - (16*discbtme*logmume*tmp132*tmp136*tmp16*t&
                   &mp19*tmp79*tmp8*tmp87)/tmp85 - (8*discbtme*tmp111*tmp16*tmp44*tmp79*tmp8*tmp87)/&
                   &tmp85 + (16*discbtme*logmume*tmp132*tmp16*tmp44*tmp79*tmp8*tmp87)/tmp85 - (8*dis&
                   &cbtme*tmp16*tmp185*tmp188*tmp44*tmp79*tmp8*tmp87)/tmp85 - (16*discbtme*logmume*t&
                   &mp132*tmp16*tmp188*tmp19*tmp44*tmp79*tmp8*tmp87)/tmp85 + (32*tmp5*tmp50*tmp79*tm&
                   &p84*tmp87)/tmp85 + (16*logmume*tmp5*tmp50*tmp79*tmp84*tmp87)/tmp85 + (16*discbtm&
                   &e*logmume*tmp19*tmp44*tmp5*tmp50*tmp79*tmp84*tmp87)/tmp85 - 8*discbtme*tmp118*tm&
                   &p44*tmp79*tmp87*tmp88 - 32*tmp79*tmp84*tmp87*tmp88 - 16*logmume*tmp79*tmp84*tmp8&
                   &7*tmp88 - 16*discbtme*logmume*tmp19*tmp44*tmp79*tmp84*tmp87*tmp88 - 32*scalarc0i&
                   &r6tme*tmp129*tmp132*tmp16*tmp50*tmp89 + 16*scalarc0ir6tme*tmp129*tmp132*tmp140*t&
                   &mp16*tmp19*tmp50*tmp89 + 32*scalarc0ir6tme*tmp129*tmp132*tmp5*tmp50*tmp89 - 16*s&
                   &calarc0ir6tme*tmp129*tmp132*tmp140*tmp19*tmp5*tmp50*tmp89 + 32*scalarc0ir6tme*tm&
                   &p132*tmp16*tmp79*tmp89 - 16*discbtme*tmp132*tmp139*tmp16*tmp50*tmp79*tmp89 + 16*&
                   &discbtme*tmp132*tmp139*tmp5*tmp50*tmp79*tmp89 - 32*scalarc0ir6tme*tmp129*tmp132*&
                   &tmp16*tmp8*tmp89 + 16*scalarc0ir6tme*tmp129*tmp132*tmp140*tmp16*tmp19*tmp8*tmp89&
                   & + 32*scalarc0ir6tme*tmp129*tmp132*tmp5*tmp8*tmp89 - 16*scalarc0ir6tme*tmp129*tm&
                   &p132*tmp140*tmp19*tmp5*tmp8*tmp89 - 16*discbtme*tmp132*tmp139*tmp16*tmp79*tmp8*t&
                   &mp89 + 16*discbtme*tmp132*tmp139*tmp5*tmp79*tmp8*tmp89 - 16*scalarc0ir6tme*tmp16&
                   &*tmp19*tmp79*tmp84*tmp89 + 32*me2*scalarc0ir6tme*tmp5*tmp79*tmp84*tmp89 - 16*sca&
                   &larc0ir6tme*tmp5*tmp79*tmp8*tmp84*tmp89 + 32*mm2*scalarc0ir6tme*tmp79*tmp84*tmp8&
                   &5*tmp89 + (16*scalarc0ir6tme*tmp16*tmp50*tmp79*tmp84*tmp85*tmp89)/tmp5 - 16*scal&
                   &arc0ir6tme*tmp79*tmp8*tmp84*tmp85*tmp89 + (32*scalarc0ir6tme*tmp129*tmp132*tmp16&
                   &*tmp50*tmp85*tmp89)/tmp87 - (16*scalarc0ir6tme*tmp129*tmp132*tmp140*tmp16*tmp19*&
                   &tmp50*tmp85*tmp89)/tmp87 + (16*discbtme*tmp132*tmp139*tmp16*tmp50*tmp79*tmp85*tm&
                   &p89)/tmp87 - (32*scalarc0ir6tme*tmp129*tmp132*tmp5*tmp8*tmp85*tmp89)/tmp87 + (16&
                   &*scalarc0ir6tme*tmp129*tmp132*tmp140*tmp19*tmp5*tmp8*tmp85*tmp89)/tmp87 - (16*di&
                   &scbtme*tmp132*tmp139*tmp5*tmp79*tmp8*tmp85*tmp89)/tmp87 - 32*scalarc0ir6tme*tmp1&
                   &32*tmp133*tmp16*tmp79*tmp87*tmp89 + (32*scalarc0ir6tme*tmp132*tmp5*tmp79*tmp87*t&
                   &mp89)/tmp16 - (32*scalarc0ir6tme*tmp129*tmp132*tmp5*tmp50*tmp87*tmp89)/tmp85 + (&
                   &16*scalarc0ir6tme*tmp129*tmp132*tmp140*tmp19*tmp5*tmp50*tmp87*tmp89)/tmp85 - (16&
                   &*discbtme*tmp132*tmp139*tmp5*tmp50*tmp79*tmp87*tmp89)/tmp85 + (32*scalarc0ir6tme&
                   &*tmp129*tmp132*tmp16*tmp8*tmp87*tmp89)/tmp85 - (16*scalarc0ir6tme*tmp129*tmp132*&
                   &tmp140*tmp16*tmp19*tmp8*tmp87*tmp89)/tmp85 + (16*discbtme*tmp132*tmp139*tmp16*tm&
                   &p79*tmp8*tmp87*tmp89)/tmp85 + (16*scalarc0ir6tme*tmp5*tmp50*tmp79*tmp84*tmp87*tm&
                   &p89)/tmp85 - 16*scalarc0ir6tme*tmp79*tmp84*tmp87*tmp88*tmp89 + 32*tmp16*tmp50*tm&
                   &p79*tmp90 + 16*logmume*tmp16*tmp50*tmp79*tmp90 + 16*discbtme*logmume*tmp16*tmp19&
                   &*tmp44*tmp50*tmp79*tmp90 - 32*tmp5*tmp50*tmp79*tmp90 - 16*logmume*tmp5*tmp50*tmp&
                   &79*tmp90 - 16*discbtme*logmume*tmp19*tmp44*tmp5*tmp50*tmp79*tmp90 + 32*tmp16*tmp&
                   &79*tmp8*tmp90 + 16*logmume*tmp16*tmp79*tmp8*tmp90 + 16*discbtme*logmume*tmp16*tm&
                   &p19*tmp44*tmp79*tmp8*tmp90 - 32*tmp5*tmp79*tmp8*tmp90 - 16*logmume*tmp5*tmp79*tm&
                   &p8*tmp90 - 16*discbtme*logmume*tmp19*tmp44*tmp5*tmp79*tmp8*tmp90 - (32*tmp16*tmp&
                   &50*tmp79*tmp85*tmp90)/tmp87 - (16*logmume*tmp16*tmp50*tmp79*tmp85*tmp90)/tmp87 -&
                   & (16*discbtme*logmume*tmp16*tmp19*tmp44*tmp50*tmp79*tmp85*tmp90)/tmp87 + (32*tmp&
                   &5*tmp79*tmp8*tmp85*tmp90)/tmp87 + (16*logmume*tmp5*tmp79*tmp8*tmp85*tmp90)/tmp87&
                   & + (16*discbtme*logmume*tmp19*tmp44*tmp5*tmp79*tmp8*tmp85*tmp90)/tmp87 + (32*tmp&
                   &5*tmp50*tmp79*tmp87*tmp90)/tmp85 + (16*logmume*tmp5*tmp50*tmp79*tmp87*tmp90)/tmp&
                   &85 + (16*discbtme*logmume*tmp19*tmp44*tmp5*tmp50*tmp79*tmp87*tmp90)/tmp85 - (32*&
                   &tmp16*tmp79*tmp8*tmp87*tmp90)/tmp85 - (16*logmume*tmp16*tmp79*tmp8*tmp87*tmp90)/&
                   &tmp85 - (16*discbtme*logmume*tmp16*tmp19*tmp44*tmp79*tmp8*tmp87*tmp90)/tmp85 + 1&
                   &6*scalarc0ir6tme*tmp16*tmp50*tmp79*tmp89*tmp90 - 16*scalarc0ir6tme*tmp5*tmp50*tm&
                   &p79*tmp89*tmp90 + 16*scalarc0ir6tme*tmp16*tmp79*tmp8*tmp89*tmp90 - 16*scalarc0ir&
                   &6tme*tmp5*tmp79*tmp8*tmp89*tmp90 - (16*scalarc0ir6tme*tmp16*tmp50*tmp79*tmp85*tm&
                   &p89*tmp90)/tmp87 + (16*scalarc0ir6tme*tmp5*tmp79*tmp8*tmp85*tmp89*tmp90)/tmp87 +&
                   & (16*scalarc0ir6tme*tmp5*tmp50*tmp79*tmp87*tmp89*tmp90)/tmp85 - (16*scalarc0ir6t&
                   &me*tmp16*tmp79*tmp8*tmp87*tmp89*tmp90)/tmp85 - (16*discbulogt*tmp16*tmp24*tmp31*&
                   &tmp35*tmp50*tmp79*tmp92)/tmp150 + (16*discbulogt*tmp24*tmp31*tmp35*tmp5*tmp79*tm&
                   &p8*tmp92)/tmp150 - (32*discbulogt*me2*tmp24*tmp31*tmp35*tmp4*tmp79*tmp92)/(tmp15&
                   &0*tmp85) + (16*discbulogt*tmp16*tmp19*tmp24*tmp31*tmp35*tmp5*tmp79*tmp92)/(tmp15&
                   &0*tmp85) - 16*discbs*tmp16*tmp3*tmp50*tmp76*tmp78*tmp99 + 16*discbs*tmp3*tmp5*tm&
                   &p76*tmp78*tmp8*tmp99 - (32*discbs*me2*tmp3*tmp4*tmp76*tmp78*tmp99)/tmp85 + (16*d&
                   &iscbs*tmp16*tmp19*tmp3*tmp5*tmp76*tmp78*tmp99)/tmp85

  END FUNCTION LBK_NLP_QE3QM

  FUNCTION LBK_NLP_QM4(me2, mm2, ss, tt, s15, s25, s35)
  implicit none
  real LBK_NLP_Qm4
  real(kind=prec) ::  me2
  real(kind=prec) ::  mm2
  real(kind=prec) ::  ss
  real(kind=prec) ::  tt
  real(kind=prec) ::  s15
  real(kind=prec) ::  s25
  real(kind=prec) ::  s35
  real tmp1, tmp2, tmp3, tmp4, tmp5
  real tmp6, tmp7, tmp8, tmp9, tmp10
  real tmp11, tmp12, tmp13, tmp14, tmp15
  real tmp16, tmp17, tmp18, tmp19, tmp20
  real tmp21, tmp22, tmp23, tmp24, tmp25
  real tmp26, tmp27, tmp28, tmp29, tmp30
  real tmp31, tmp32, tmp33, tmp34, tmp35
  real tmp36, tmp37, tmp38, tmp39, tmp40
  real tmp41, tmp42, tmp43, tmp44, tmp45
  real tmp46, tmp47, tmp48, tmp49, tmp50
  real tmp51, tmp52, tmp53, tmp54, tmp55
  real tmp56, tmp57, tmp58, tmp59, tmp60
  real tmp61, tmp62, tmp63, tmp64, tmp65
  real tmp66, tmp67, tmp68, tmp69, tmp70
  real tmp71, tmp72

  tmp1 = s25**(-2)
  tmp2 = tt**(-2)
  tmp3 = -ss
  tmp4 = me2 + mm2 + tmp3
  tmp5 = -4*tmp4
  tmp6 = 2*tt
  tmp7 = tmp5 + tmp6
  tmp8 = 1/s25
  tmp9 = -me2
  tmp10 = -mm2
  tmp11 = ss + tmp10 + tmp9
  tmp12 = -s35
  tmp13 = s15 + tmp12 + 1/tmp8
  tmp14 = 1/tmp13
  tmp15 = 2*mm2
  tmp16 = -tt
  tmp17 = tmp15 + tmp16
  tmp18 = 4*mm2
  tmp19 = tmp16 + tmp18
  tmp20 = 1/tmp19
  tmp21 = -2*mm2
  tmp22 = tmp21 + tt
  tmp23 = tmp11 + tt
  tmp24 = 2*ss
  tmp25 = tmp24 + tmp6
  tmp26 = tmp14**2
  tmp27 = tmp17**2
  tmp28 = me2**2
  tmp29 = 3*tmp28
  tmp30 = mm2**2
  tmp31 = 3*tmp30
  tmp32 = -3*ss
  tmp33 = mm2 + tmp32
  tmp34 = 2*me2*tmp33
  tmp35 = -14*mm2*ss
  tmp36 = ss**2
  tmp37 = 3*tmp36
  tmp38 = tmp29 + tmp31 + tmp34 + tmp35 + tmp37
  tmp39 = -2*tmp38
  tmp40 = tmp15 + tmp3
  tmp41 = 12*tmp40*tt
  tmp42 = 1/tmp2
  tmp43 = -9*tmp42
  tmp44 = tmp39 + tmp41 + tmp43
  tmp45 = -32*mm2*tmp4
  tmp46 = -6*me2
  tmp47 = -14*mm2
  tmp48 = 6*ss
  tmp49 = tmp46 + tmp47 + tmp48
  tmp50 = -2*tmp49*tt
  tmp51 = -6*tmp42
  tmp52 = tmp45 + tmp50 + tmp51
  tmp53 = tt**(-3)
  tmp54 = tmp4**2
  tmp55 = 2*tmp54
  tmp56 = tmp24*tt
  tmp57 = tmp42 + tmp55 + tmp56
  tmp58 = tmp20**2
  tmp59 = -4*mm2
  tmp60 = tmp59 + tt
  tmp61 = tmp60**(-2)
  tmp62 = 1/tmp60
  tmp63 = 16*mm2*tmp54
  tmp64 = tmp39*tt
  tmp65 = 6*tmp40*tmp42
  tmp66 = 1/tmp53
  tmp67 = -3*tmp66
  tmp68 = tmp63 + tmp64 + tmp65 + tmp67
  tmp69 = 1/tt
  tmp70 = -tmp69
  tmp71 = tmp22*tmp62*tmp69
  tmp72 = tmp70 + tmp71
  LBK_NLP_Qm4 = (64*mm2*tmp1*tmp2*tmp25)/tmp14 + (32*logmumm*mm2*tmp1*tmp2*tmp25)/tmp14 + 64*mm2&
                 &*tmp14*tmp2*tmp25 + 32*logmumm*mm2*tmp14*tmp2*tmp25 + 64*tmp14*tmp17*tmp2*tmp25 &
                 &+ 32*logmumm*tmp14*tmp17*tmp2*tmp25 + (32*discbtmm*logmumm*mm2*tmp1*tmp17*tmp2*t&
                 &mp20*tmp25)/tmp14 + 32*discbtmm*logmumm*mm2*tmp14*tmp17*tmp2*tmp20*tmp25 + (32*m&
                 &m2*scalarc0ir6tmm*tmp1*tmp2*tmp22*tmp25)/tmp14 + 32*mm2*scalarc0ir6tmm*tmp14*tmp&
                 &2*tmp22*tmp25 + 32*scalarc0ir6tmm*tmp14*tmp17*tmp2*tmp22*tmp25 + 32*discbtmm*log&
                 &mumm*tmp14*tmp2*tmp20*tmp25*tmp27 + (16*discbtmm*mm2*tmp1*tmp2*tmp20*tmp44)/tmp1&
                 &4 + 16*discbtmm*mm2*tmp14*tmp2*tmp20*tmp44 + 16*discbtmm*tmp14*tmp17*tmp2*tmp20*&
                 &tmp44 - 16*discbtmm*mm2*s15*tmp1*tmp2*tmp20*tmp52 - 8*discbtmm*tmp14*tmp2*tmp20*&
                 &tmp23*tmp52 + (32*mm2*scalarc0ir6tmm*tmp1*tmp2*tmp57)/tmp14 + 32*mm2*scalarc0ir6&
                 &tmm*tmp14*tmp2*tmp57 + 32*scalarc0ir6tmm*tmp14*tmp17*tmp2*tmp57 - (32*discbtmm*l&
                 &ogmumm*mm2*tmp1*tmp2*tmp20*tmp57)/tmp14 - 32*discbtmm*logmumm*mm2*tmp14*tmp2*tmp&
                 &20*tmp57 - 32*discbtmm*logmumm*tmp14*tmp17*tmp2*tmp20*tmp57 - (128*mm2*tmp1*tmp5&
                 &3*tmp57)/tmp14 - (64*logmumm*mm2*tmp1*tmp53*tmp57)/tmp14 - 128*mm2*tmp14*tmp53*t&
                 &mp57 - 64*logmumm*mm2*tmp14*tmp53*tmp57 - 128*tmp14*tmp17*tmp53*tmp57 - 64*logmu&
                 &mm*tmp14*tmp17*tmp53*tmp57 - (32*logmumm*mm2*tmp1*tmp17*tmp20*tmp53*tmp57)/tmp14&
                 & - (64*discbtmm*logmumm*mm2*tmp1*tmp17*tmp20*tmp53*tmp57)/tmp14 - 32*logmumm*mm2&
                 &*tmp14*tmp17*tmp20*tmp53*tmp57 - 64*discbtmm*logmumm*mm2*tmp14*tmp17*tmp20*tmp53&
                 &*tmp57 - (64*mm2*scalarc0ir6tmm*tmp1*tmp22*tmp53*tmp57)/tmp14 - 64*mm2*scalarc0i&
                 &r6tmm*tmp14*tmp22*tmp53*tmp57 - 64*scalarc0ir6tmm*tmp14*tmp17*tmp22*tmp53*tmp57 &
                 &- 32*logmumm*tmp14*tmp20*tmp27*tmp53*tmp57 - 64*discbtmm*logmumm*tmp14*tmp20*tmp&
                 &27*tmp53*tmp57 + (32*discbtmm*logmumm*mm2*tmp1*tmp17*tmp2*tmp57*tmp58)/tmp14 + 3&
                 &2*discbtmm*logmumm*mm2*tmp14*tmp17*tmp2*tmp57*tmp58 + 32*discbtmm*logmumm*tmp14*&
                 &tmp2*tmp27*tmp57*tmp58 - (32*discbtmm*mm2*tmp1*tmp2*tmp22*tmp57*tmp61)/tmp14 - 3&
                 &2*discbtmm*mm2*tmp14*tmp2*tmp22*tmp57*tmp61 - 32*discbtmm*tmp14*tmp17*tmp2*tmp22&
                 &*tmp57*tmp61 + (32*mm2*scalarc0ir6tmm*tmp1*tmp17*tmp22*tmp53*tmp57*tmp62)/tmp14 &
                 &+ 32*mm2*scalarc0ir6tmm*tmp14*tmp17*tmp22*tmp53*tmp57*tmp62 + 32*scalarc0ir6tmm*&
                 &tmp14*tmp22*tmp27*tmp53*tmp57*tmp62 - (16*mm2*tmp1*tmp20*tmp53*tmp68)/tmp14 - (3&
                 &2*discbtmm*mm2*tmp1*tmp20*tmp53*tmp68)/tmp14 - 16*mm2*tmp14*tmp20*tmp53*tmp68 - &
                 &32*discbtmm*mm2*tmp14*tmp20*tmp53*tmp68 - 16*tmp14*tmp17*tmp20*tmp53*tmp68 - 32*&
                 &discbtmm*tmp14*tmp17*tmp20*tmp53*tmp68 + (16*discbtmm*mm2*tmp1*tmp2*tmp58*tmp68)&
                 &/tmp14 + 16*discbtmm*mm2*tmp14*tmp2*tmp58*tmp68 + 16*discbtmm*tmp14*tmp17*tmp2*t&
                 &mp58*tmp68 - 64*mm2*s15*tmp1*tmp2*tmp7 - 32*logmumm*mm2*s15*tmp1*tmp2*tmp7 - 32*&
                 &discbtmm*logmumm*mm2*s15*tmp1*tmp17*tmp2*tmp20*tmp7 - 32*mm2*s15*scalarc0ir6tmm*&
                 &tmp1*tmp2*tmp22*tmp7 - 32*tmp14*tmp2*tmp23*tmp7 - 16*logmumm*tmp14*tmp2*tmp23*tm&
                 &p7 - 16*discbtmm*logmumm*tmp14*tmp17*tmp2*tmp20*tmp23*tmp7 - 16*scalarc0ir6tmm*t&
                 &mp14*tmp2*tmp22*tmp23*tmp7 + (32*discbtmm*logmumm*mm2*tmp1*tmp17*tmp2*tmp20*tmp5&
                 &7*tmp72)/tmp14 + 32*discbtmm*logmumm*mm2*tmp14*tmp17*tmp2*tmp20*tmp57*tmp72 + 32&
                 &*discbtmm*logmumm*tmp14*tmp2*tmp20*tmp27*tmp57*tmp72 + (16*discbtmm*mm2*tmp1*tmp&
                 &2*tmp20*tmp68*tmp72)/tmp14 + 16*discbtmm*mm2*tmp14*tmp2*tmp20*tmp68*tmp72 + 16*d&
                 &iscbtmm*tmp14*tmp17*tmp2*tmp20*tmp68*tmp72 - (64*mm2*tmp2*tmp25*tmp26)/tmp8 - (3&
                 &2*logmumm*mm2*tmp2*tmp25*tmp26)/tmp8 - (32*discbtmm*logmumm*mm2*tmp17*tmp2*tmp20&
                 &*tmp25*tmp26)/tmp8 - (32*mm2*scalarc0ir6tmm*tmp2*tmp22*tmp25*tmp26)/tmp8 - (16*d&
                 &iscbtmm*mm2*tmp2*tmp20*tmp26*tmp44)/tmp8 - (32*mm2*scalarc0ir6tmm*tmp2*tmp26*tmp&
                 &57)/tmp8 + (32*discbtmm*logmumm*mm2*tmp2*tmp20*tmp26*tmp57)/tmp8 + (128*mm2*tmp2&
                 &6*tmp53*tmp57)/tmp8 + (64*logmumm*mm2*tmp26*tmp53*tmp57)/tmp8 + (32*logmumm*mm2*&
                 &tmp17*tmp20*tmp26*tmp53*tmp57)/tmp8 + (64*discbtmm*logmumm*mm2*tmp17*tmp20*tmp26&
                 &*tmp53*tmp57)/tmp8 + (64*mm2*scalarc0ir6tmm*tmp22*tmp26*tmp53*tmp57)/tmp8 - (32*&
                 &discbtmm*logmumm*mm2*tmp17*tmp2*tmp26*tmp57*tmp58)/tmp8 + (32*discbtmm*mm2*tmp2*&
                 &tmp22*tmp26*tmp57*tmp61)/tmp8 - (32*mm2*scalarc0ir6tmm*tmp17*tmp22*tmp26*tmp53*t&
                 &mp57*tmp62)/tmp8 + (16*mm2*tmp20*tmp26*tmp53*tmp68)/tmp8 + (32*discbtmm*mm2*tmp2&
                 &0*tmp26*tmp53*tmp68)/tmp8 - (16*discbtmm*mm2*tmp2*tmp26*tmp58*tmp68)/tmp8 - (32*&
                 &discbtmm*logmumm*mm2*tmp17*tmp2*tmp20*tmp26*tmp57*tmp72)/tmp8 - (16*discbtmm*mm2&
                 &*tmp2*tmp20*tmp26*tmp68*tmp72)/tmp8 - 64*mm2*tmp2*tmp25*tmp8 - 32*logmumm*mm2*tm&
                 &p2*tmp25*tmp8 - 64*tmp17*tmp2*tmp25*tmp8 - 32*logmumm*tmp17*tmp2*tmp25*tmp8 - 32&
                 &*discbtmm*logmumm*mm2*tmp17*tmp2*tmp20*tmp25*tmp8 - 32*mm2*scalarc0ir6tmm*tmp2*t&
                 &mp22*tmp25*tmp8 - 32*scalarc0ir6tmm*tmp17*tmp2*tmp22*tmp25*tmp8 - 32*discbtmm*lo&
                 &gmumm*tmp2*tmp20*tmp25*tmp27*tmp8 - 16*discbtmm*mm2*tmp2*tmp20*tmp44*tmp8 - 16*d&
                 &iscbtmm*tmp17*tmp2*tmp20*tmp44*tmp8 + 8*discbtmm*tmp11*tmp2*tmp20*tmp52*tmp8 + 8&
                 &*discbtmm*s15*tmp14*tmp17*tmp2*tmp20*tmp52*tmp8 - 32*mm2*scalarc0ir6tmm*tmp2*tmp&
                 &57*tmp8 - 32*scalarc0ir6tmm*tmp17*tmp2*tmp57*tmp8 + 32*discbtmm*logmumm*mm2*tmp2&
                 &*tmp20*tmp57*tmp8 + 32*discbtmm*logmumm*tmp17*tmp2*tmp20*tmp57*tmp8 + 128*mm2*tm&
                 &p53*tmp57*tmp8 + 64*logmumm*mm2*tmp53*tmp57*tmp8 + 128*tmp17*tmp53*tmp57*tmp8 + &
                 &64*logmumm*tmp17*tmp53*tmp57*tmp8 + 32*logmumm*mm2*tmp17*tmp20*tmp53*tmp57*tmp8 &
                 &+ 64*discbtmm*logmumm*mm2*tmp17*tmp20*tmp53*tmp57*tmp8 + 64*mm2*scalarc0ir6tmm*t&
                 &mp22*tmp53*tmp57*tmp8 + 64*scalarc0ir6tmm*tmp17*tmp22*tmp53*tmp57*tmp8 + 32*logm&
                 &umm*tmp20*tmp27*tmp53*tmp57*tmp8 + 64*discbtmm*logmumm*tmp20*tmp27*tmp53*tmp57*t&
                 &mp8 - 32*discbtmm*logmumm*mm2*tmp17*tmp2*tmp57*tmp58*tmp8 - 32*discbtmm*logmumm*&
                 &tmp2*tmp27*tmp57*tmp58*tmp8 + 32*discbtmm*mm2*tmp2*tmp22*tmp57*tmp61*tmp8 + 32*d&
                 &iscbtmm*tmp17*tmp2*tmp22*tmp57*tmp61*tmp8 - 32*mm2*scalarc0ir6tmm*tmp17*tmp22*tm&
                 &p53*tmp57*tmp62*tmp8 - 32*scalarc0ir6tmm*tmp22*tmp27*tmp53*tmp57*tmp62*tmp8 + 16&
                 &*mm2*tmp20*tmp53*tmp68*tmp8 + 32*discbtmm*mm2*tmp20*tmp53*tmp68*tmp8 + 16*tmp17*&
                 &tmp20*tmp53*tmp68*tmp8 + 32*discbtmm*tmp17*tmp20*tmp53*tmp68*tmp8 - 16*discbtmm*&
                 &mm2*tmp2*tmp58*tmp68*tmp8 - 16*discbtmm*tmp17*tmp2*tmp58*tmp68*tmp8 + 32*tmp11*t&
                 &mp2*tmp7*tmp8 + 16*logmumm*tmp11*tmp2*tmp7*tmp8 + 32*s15*tmp14*tmp17*tmp2*tmp7*t&
                 &mp8 + 16*logmumm*s15*tmp14*tmp17*tmp2*tmp7*tmp8 + 16*discbtmm*logmumm*tmp11*tmp1&
                 &7*tmp2*tmp20*tmp7*tmp8 + 16*scalarc0ir6tmm*tmp11*tmp2*tmp22*tmp7*tmp8 + 16*s15*s&
                 &calarc0ir6tmm*tmp14*tmp17*tmp2*tmp22*tmp7*tmp8 + 16*discbtmm*logmumm*s15*tmp14*t&
                 &mp2*tmp20*tmp27*tmp7*tmp8 - 32*discbtmm*logmumm*mm2*tmp17*tmp2*tmp20*tmp57*tmp72&
                 &*tmp8 - 32*discbtmm*logmumm*tmp2*tmp20*tmp27*tmp57*tmp72*tmp8 - 16*discbtmm*mm2*&
                 &tmp2*tmp20*tmp68*tmp72*tmp8 - 16*discbtmm*tmp17*tmp2*tmp20*tmp68*tmp72*tmp8

  END FUNCTION LBK_NLP_QM4

  FUNCTION LBK_NLP_QE4(me2, mm2, ss, tt, s15, s25, s35)
  implicit none
  real LBK_NLP_Qe4
  real(kind=prec) ::  me2
  real(kind=prec) ::  mm2
  real(kind=prec) ::  ss
  real(kind=prec) ::  tt
  real(kind=prec) ::  s15
  real(kind=prec) ::  s25
  real(kind=prec) ::  s35
  real tmp1, tmp2, tmp3, tmp4, tmp5
  real tmp6, tmp7, tmp8, tmp9, tmp10
  real tmp11, tmp12, tmp13, tmp14, tmp15
  real tmp16, tmp17, tmp18, tmp19, tmp20
  real tmp21, tmp22, tmp23, tmp24, tmp25
  real tmp26, tmp27, tmp28, tmp29, tmp30
  real tmp31, tmp32, tmp33, tmp34, tmp35
  real tmp36

  tmp1 = s15**(-2)
  tmp2 = tt**(-2)
  tmp3 = -ss
  tmp4 = me2 + mm2 + tmp3
  tmp5 = -4*tmp4
  tmp6 = 2*tt
  tmp7 = tmp5 + tmp6
  tmp8 = 1/s15
  tmp9 = -me2
  tmp10 = -mm2
  tmp11 = ss + tmp10 + tmp9
  tmp12 = 1/s35
  tmp13 = 2*me2
  tmp14 = -tt
  tmp15 = tmp13 + tmp14
  tmp16 = 4*me2
  tmp17 = tmp14 + tmp16
  tmp18 = 1/tmp17
  tmp19 = -2*me2
  tmp20 = tmp19 + tt
  tmp21 = tmp11 + tt
  tmp22 = -32*me2*tmp4
  tmp23 = -14*me2
  tmp24 = mm2 + tmp3
  tmp25 = -6*tmp24
  tmp26 = tmp23 + tmp25
  tmp27 = -2*tmp26*tt
  tmp28 = 1/tmp2
  tmp29 = -6*tmp28
  tmp30 = tmp22 + tmp27 + tmp29
  tmp31 = -(1/tmp12)
  tmp32 = tmp31 + 1/tmp8
  tmp33 = tmp4**2
  tmp34 = 2*tmp33
  tmp35 = ss*tmp6
  tmp36 = tmp28 + tmp34 + tmp35
  LBK_NLP_Qe4 = -16*discbtme*me2*s25*tmp1*tmp18*tmp2*tmp30 - 8*discbtme*tmp12*tmp18*tmp2*tmp21*t&
                 &mp30 - 64*me2*s25*tmp1*tmp2*tmp7 - 32*logmume*me2*s25*tmp1*tmp2*tmp7 - 32*discbt&
                 &me*logmume*me2*s25*tmp1*tmp15*tmp18*tmp2*tmp7 - 32*me2*s25*scalarc0ir6tme*tmp1*t&
                 &mp2*tmp20*tmp7 - 32*tmp12*tmp2*tmp21*tmp7 - 16*logmume*tmp12*tmp2*tmp21*tmp7 - 1&
                 &6*discbtme*logmume*tmp12*tmp15*tmp18*tmp2*tmp21*tmp7 - 16*scalarc0ir6tme*tmp12*t&
                 &mp2*tmp20*tmp21*tmp7 + 8*discbtme*tmp11*tmp18*tmp2*tmp30*tmp8 + 8*discbtme*s25*t&
                 &mp12*tmp15*tmp18*tmp2*tmp30*tmp8 + 64*tmp12*tmp2*tmp32*tmp36*tmp8 + 32*logmume*t&
                 &mp12*tmp2*tmp32*tmp36*tmp8 + 32*discbtme*logmume*tmp12*tmp15*tmp18*tmp2*tmp32*tm&
                 &p36*tmp8 + 32*scalarc0ir6tme*tmp12*tmp2*tmp20*tmp32*tmp36*tmp8 + 32*tmp11*tmp2*t&
                 &mp7*tmp8 + 16*logmume*tmp11*tmp2*tmp7*tmp8 + 32*s25*tmp12*tmp15*tmp2*tmp7*tmp8 +&
                 & 16*logmume*s25*tmp12*tmp15*tmp2*tmp7*tmp8 + 16*discbtme*logmume*tmp11*tmp15*tmp&
                 &18*tmp2*tmp7*tmp8 + 16*discbtme*logmume*s25*tmp12*tmp15**2*tmp18*tmp2*tmp7*tmp8 &
                 &+ 16*scalarc0ir6tme*tmp11*tmp2*tmp20*tmp7*tmp8 + 16*s25*scalarc0ir6tme*tmp12*tmp&
                 &15*tmp2*tmp20*tmp7*tmp8 + 16*discbtme*tmp12*tmp18*tmp2*tmp32*tmp8*(6*tmp28*(tmp1&
                 &3 + tmp3) + 16*me2*tmp33 - 2*(3*me2**2 + (mm2 - 7*ss)*tmp13 + 3*tmp24**2)*tt - 3&
                 &*tt**3)

  END FUNCTION LBK_NLP_QE4

                          !!!!!!!!!!!!!!!!!!!!!!
                         END MODULE mue_em2emgl_lbk
                          !!!!!!!!!!!!!!!!!!!!!!
