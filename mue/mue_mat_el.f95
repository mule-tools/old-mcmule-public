
                 !!!!!!!!!!!!!!!!!!!!!!!!!
                       MODULE MUE_MAT_EL
                 !!!!!!!!!!!!!!!!!!!!!!!!!

  use functions
  use mue_em2emgleeee, only: em2emgl_eeee_old
  use mue_em2emggeeee, only: em2emgg_eeee_old
  use mue_em2emeeee, only: em2emeeee
  use mue_mp2mpgl_pvred, only: mp2mpgl_pvred, em2emgl_eeee_pvred
  use mue_mp2mpgl_coll, only: mp2mpgl_coll, em2emgl_eeee_coll
  use mue_mp2mpgl_fullcoll, only: mp2mpgl_fullcoll, em2emgl_eeee_fullcoll
  use mue_em2emgl_lbk, only: em2emgl_lbk, em2emgl_lbk_eeee, em2emgl_lbk_mixd, em2emgl_lbk_mmmm,  &
                           & em2emgl_lbk_e3m1, em2emgl_lbk_e2m2, em2emgl_lbk_e1m3,               &
                           & em2emgl_ls_e3m1, em2emgl_ls_e2m2, em2emgl_ls_e1m3, em2emgl_ls_mixd, &
                           & em2emgl_lbk11_eeee, mp2mpgl_lbk
  use mue_mp2mp_nf_formfac_el
  use mue_mp2mp_nf_formfac_mu
  use mue_em2em_nfem, only: em2em_nfem, em2em_nfem_sin
  use mue_em2em_mless
  use mue_pepe2mm
  use mue_mp2mp_nuc
  use mue_mp2mp_mono

!!!  use amplitude_lo, only: amplitude_n_LO

  implicit none

  contains


                   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                   !!                             !!
                   !!     MATRIX   ELEMENTS       !!
                   !!                             !!
                   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!




                  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                  !!                             !!
                  !!            MUONE            !!
                  !!                             !!
                  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!




  !!!!!!!!!!    Born    !!!!!!!!!!!!

  FUNCTION EM2EM(p1,p2,q1,q2, lin)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2)
    !! for massive (and massless) electrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: ss,tt, me2,mm2
  real (kind=prec) :: em2em
  real (kind=prec), optional :: lin

  ss = sq(p1+p2); tt = sq(p1-q1)
  me2=sq(p1); mm2=sq(p2)

  em2em = (2*(me2 + mm2 - ss)**2 + 2*ss*tt + tt**2)/(tt**2)

  em2em = 32*pi**2*alpha**2*em2em

  if (present(lin)) then
    lin = 32*pi**2*alpha**2* (-1._prec)
  endif
  END FUNCTION EM2EM


  !!!!!!!!!!    VP    !!!!!!!!!!!!


  FUNCTION EM2EM_A(p1,p2,q1,q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2)
    !! for massive (and massless) electrons
  real (kind=prec), intent(in) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: em2em_a,tt,deltalph_tot

  tt = sq(p1-q1)
  deltalph_tot = deltalph_0_tot(tt)
  em2em_a = 2*deltalph_tot*em2em(p1,p2,q1,q2)

  END FUNCTION EM2EM_A



  !!!!!!!!!!!    Ql**2*Qe**2*Qmu**2 (i.e. VP)      !!!!!!!!!!!!


  !FUNCTION EM2EM_NLO_VP(p1,p2,q1,q2,ml,Ql)
  !real (kind=prec) :: p1(4),p2(4),q1(4),q2(4)
  !real (kind=prec) :: ss,tt,me2,mm2,mu2,logme,ml,Ql
  !real (kind=prec) :: em2em_nlo_vp

  !ss = sq(p1+p2); tt = sq(p1-q1)
  !me2=sq(p1); mm2=sq(p2)
  !mu2 = musq

  !em2em_nlo_vp = -(2*me2**2 + 2*mm2**2 + 4*me2*(mm2 - ss) - 4*mm2*ss + 2*ss**2 + 2*ss*tt + tt**2)&
  !               *(12*ml**2 + 5*tt + 3*(2*ml**2 + tt)*DiscB(tt,ml))

  !em2em_nlo_vp = 64*pi*alpha**3*Ql**2*em2em_nlo_vp/(9*tt**3)

  !END FUNCTION EM2EM_NLO_VP



  !!!!!!!!!!    Qe**4*Qmu**2      !!!!!!!!!!!!

  FUNCTION EM2EML_EE(p1,p2,q1,q2, pole)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: em2eml_ee
  real (kind=prec) :: ss,tt,me2,mm2,mu2,me,logme,discbt,scalarc0ir6t
  real (kind=prec), optional :: pole

  ss = sq(p1+p2); tt = sq(p1-q1)
  me2=sq(p1); mm2=sq(p2)
  me=sqrt(me2)
  mu2 = musq
  logme = log(mu2/me2)
  discbt = DiscB(tt,me)
  scalarc0ir6t = ScalarC0IR6(tt,me)

  em2eml_ee = -16*me2*(2 + discbt - 2*me2*scalarc0ir6t)*(me2 + mm2 - ss)**2 + 2*(-12*me2**3*sc&
            &alarc0ir6t + (4 + 3*discbt)*(mm2 - ss)**2 + me2**2*(4 + 3*discbt - 24*mm2*scalar&
            &c0ir6t + 40*scalarc0ir6t*ss) + 2*me2*(-6*mm2**2*scalarc0ir6t - ss*(12 + 7*discbt&
            & + 6*scalarc0ir6t*ss) + mm2*(4 + discbt + 12*scalarc0ir6t*ss)))*tt + 2*(10*me2**&
            &2*scalarc0ir6t + 2*scalarc0ir6t*(mm2 - ss)**2 + (4 + 3*discbt)*ss - 2*me2*(4 + 3&
            &*discbt - 2*mm2*scalarc0ir6t + 8*scalarc0ir6t*ss))*tt**2 + (4 + 3*discbt + 4*sca&
            &larc0ir6t*(-3*me2 + ss))*tt**3 + 2*scalarc0ir6t*tt**4 - 2*logme*(2*(2 + discbt)*&
            &me2 - (1 + discbt)*tt)*(2*(me2 + mm2 - ss)**2 + 2*ss*tt + tt**2)

  em2eml_ee = 16*pi*alpha**3*em2eml_ee/((4*me2-tt)*tt**2)

  if (present(pole)) then
    pole = -((2*(2 + discbt)*me2 - (1 + discbt)*tt)*(2*(me2 + mm2 - ss)**2 + 2*ss*tt + tt**2))
    pole = 32*pi*alpha**3*pole/((4*me2-tt)*tt**2)
  endif

  END FUNCTION EM2EML_EE


  FUNCTION EM2EMG_EE(p1,p2,q1,q2,q3, lin)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(q3)
    !! for massive (and massless) electrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4),q3(4)
  real (kind=prec) :: me2,mm2,s12,s13,s14,s15,s23,s24,s25,s34,s35,s45
  real (kind=prec) :: if11,if12,if22,den11,den22,den12,denAll
  real (kind=prec) :: em2emg_ee
  real (kind=prec), optional :: lin

  s12 = s(p1,p2); s13 = s(p1,q1); s14 = s(p1,q2); s15 = s(p1,q3)
  s23= s(p2,q1); s24 = s(p2,q2); s25 = s(p2,q3)
  s34 = s(q1,q2); s35 = s(q1,q3); s45 = s(q2,q3)
  me2=sq(p1); mm2=sq(p2)
  den11 = s15**2; den22 = s35**2; den12 = 2*s15*s35; denAll = (s24-2*mm2)**2

  if11 = 4*me2**2*(-4*mm2 + s24) + 2*me2*(-(s14*s23) - s15*s24 - s12*s34 + s25*s34&
         + 2*mm2*(s13 + 2*s15 - s35) + s23*s45) + s15*(s25*s34 - 2*mm2*s35 + s23*s45)

  if22 = -4*me2**2*(4*mm2 - s24) + s35*(-2*mm2*s15 + s14*s25 + s12*s45) - 2*me2*(s14*(s23&
         + s25) + s12*s34 - 2*mm2*(s13 + s15 - 2*s35) - s24*s35 + s12*s45)

  if12 = 2*s13*s14*s23 + s14*s15*s23 + s13*s14*s25 + 2*s12*s13*s34 + s12*s15*s34&
         + 2*s15*s23*s34 - s13*s25*s34 - 4*mm2*s13*(s13 + s15 - s35) - 2*s12*s14*s35&
         - s14*s23*s35 - s12*s34*s35 + s12*s13*s45 - s13*s23*s45 + 2*me2*(-2*s13*s24&
         - s15*s24 + 2*mm2*(4*s13 + s15 - s35) + s24*s35 - 2*s25*s45)

  em2emg_ee = 256*pi**3*alpha**3&
                *(if11/den11 + 2*if12/den12 + if22/den22)/denAll

  if (present(lin)) then
    if11 = me2**2*(8*mm2 - 4*s24) - 2*me2*(2*mm2 - s24)*(s13 + s15 - s35) &
          - s15*(s25*s34 - 4*mm2*s35 + s24*s35 + s23*s45)
    if22 = me2**2*(8*mm2 - 4*s24) - 2*me2*(2*mm2 - s24)*(s13 + s15 - s35) &
          + s35*(4*mm2*s15 - s15*s24 - s14*s25 - s12*s45)
    if12 = -2*s13**2*s24 - 2*s13*s15*s24 + s15*s25*s34 + 4*mm2*(s13 + s15)*(s13 - s35) &
          + 2*s13*s24*s35 + s14*s25*s35 + s15*s23*s45 -   2*s13*s25*s45 + s12*s35*s45 &
          + me2*(-8*mm2*s13 + 4*s13*s24 + 4*s25*s45)
    lin = 256*pi**3*alpha**3&
                  *(if11/den11 + 2*if12/den12 + if22/den22)/denAll
  endif

  END FUNCTION EM2EMG_EE


  FUNCTION EM2EMG_EE_NTS(p1,p2,p3,p4,p5)
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4),p5(4)
  real (kind=prec) :: em2emg_ee_nts
  real (kind=prec) :: ss,tt,s15,s25,s35,m2,mm2

  ss = sq(p1+p2); tt = sq(p2-p4)
  s15 = s(p1,p5); s25 = s(p2,p5); s35 = s(p3,p5)
  m2 = sq(p1); mm2=sq(p2)

  em2emg_ee_nts = (512*alpha**3*Pi**3*(-((m2*(s15 - s35)**2 + s15*s35*tt)*(2*m&
                &2**2 + 2*mm2**2 + 4*m2*(mm2 - ss) - 4*mm2*ss + 2*ss**2 + 2*s&
                &s*tt + tt**2)) + s35*(4*m2**2*s25*(s15 - s35) + s15*tt*(mm2*&
                &(-3*s15 - 2*s25 + s35) + (s15 + 2*s25 + s35)*ss + (s25 + s35&
                &)*tt) + m2*(4*mm2*s25*(s15 - s35) + 4*s25*(-s15 + s35)*ss + &
                &(-3*s15**2 - 4*s15*s25 + s15*s35 + 2*s25*s35)*tt))))/(s15**2&
                &*s35**2*tt**2)
  END FUNCTION


  ! This matrix element is no longer supported and is kept
  ! for legacy / testing purposes. Use at your own risk.
  !! e-(p1) mu-(p2) -> e-(p3) mu-(p4) g(p5)
    !! for massive electrons
  FUNCTION EM2EMG_EE_APPROX(p1,p2,p3,p4,p5)
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4),p5(4)
  real (kind=prec) :: em2emg_ee_approx
  real (kind=prec) :: xi,m2,s15,s35

  xi = 2.*p5(4)/sqrt(scms)
  m2 = sq(p1)
  s15 = s(p1,p5)
  s35 = s(p3,p5)

  if(xi<1e-2) then
    em2emg_ee_approx = em2emg_ee_nts(p1,p2,p3,p4,p5)
  elseif(s15<2*m2) then
    em2emg_ee_approx = em2emg_ee_icoll(p1,p2,p3,p4,p5)
  elseif(s35<2*m2) then
    em2emg_ee_approx = em2emg_ee_fcoll(p1,p2,p3,p4,p5)
  else
    em2emg_ee_approx = em2emg_ee_mless(p1,p2,p3,p4,p5)
  endif
  END FUNCTION


  !!!!!!!!!!    Qe**2*Qmu**4      !!!!!!!!!!!!


  FUNCTION EM2EML_MM(p1,p2,q1,q2,pole)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: em2eml_mm
  real (kind=prec), optional :: pole
  !Corresponds to ee contribution after p2<->p1,q1<->q2
  if(present(pole)) then
    em2eml_mm = em2eml_ee(p2,p1,q2,q1,pole)
  else
   em2eml_mm = em2eml_ee(p2,p1,q2,q1)
  endif
  END FUNCTION EM2EML_MM


  FUNCTION EM2EMG_MM(p1,p2,q1,q2,q3,lin)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(q3)
    !! for massive (and massless) electrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4),q3(4)
  real (kind=prec) :: em2emg_mm
  real (kind=prec), optional :: lin
  !Corresponds to ee contribution after p2<->p1,q1<->q2
  if(present(lin)) then
    em2emg_mm = em2emg_ee(p2,p1,q2,q1,q3,lin)
  else
    em2emg_mm = em2emg_ee(p2,p1,q2,q1,q3)
  endif
  END FUNCTION EM2EMG_MM


  !!!!!!!!!!    Qe**3*Qmu**3     !!!!!!!!!!!!


  FUNCTION EM2EML_EM(p1,p2,q1,q2,pole)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2)
    !! for massive electrons
  real (kind=prec) :: em2eml_em
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: ss,tt,me2,mm2,mu2,me,mm
  real (kind=prec) :: logm,logmu,logt,discbs,discbu
  real (kind=prec) :: scalarc0tme,scalarc0tmm,scalarc0ir6s,scalarc0ir6u
  real (kind=prec), optional :: pole
#include "charge_config.h"


  ss = sq(p1+p2); tt = sq(p1-q1)
  me2=sq(p1); mm2=sq(p2)
  me=sqrt(me2);mm=sqrt(mm2)
  mu2 = musq
  logm = log(me2/mm2)
  logt = log(abs(-me2/tt))
  logmu = log(mu2/me2)
  discbs = DiscB(ss,me,mm)
  discbu = DiscB(2*me2+2*mm2-ss-tt,me,mm)
  scalarc0tme = ScalarC0(tt,me)
  scalarc0tmm = ScalarC0(tt,mm)
  scalarc0ir6s = ScalarC0IR6(ss,me,mm)
  scalarc0ir6u = ScalarC0IR6(2*me2+2*mm2-ss-tt,me,mm)

  em2eml_em = -2*scalarc0ir6u*(4*me2 + 4*mm2 - 2*ss - tt)*(me2 + mm2 - ss - tt)*tt - 2*scalarc&
            &0ir6s*(me2 + mm2 - ss)*tt*(2*ss + tt) + (2*logt*(2*(me2 + mm2 - ss) - tt)*tt*(16&
            &*me2*mm2 - tt**2))/((4*me2 - tt)*(-4*mm2 + tt)) + (2*scalarc0tme*(2*(me2 + mm2 -&
            & ss) - tt)*tt*(8*me2**2 - 8*me2*tt + tt**2))/(4*me2 - tt) + (2*scalarc0tmm*(2*(m&
            &e2 + mm2 - ss) - tt)*tt*(8*mm2**2 - 8*mm2*tt + tt**2))/(4*mm2 - tt) + (logm*(2*(&
            &me2 + mm2 - ss) - tt)*tt*(4*mm2*(-me2**2 + (mm2 - ss)**2 - 2*me2*ss) + (me2**2 +&
            & 3*mm2**2 + 2*mm2*ss + ss**2 - 2*me2*(2*mm2 + ss))*tt + (me2 - mm2 + ss)*tt**2))&
            &/(ss*(4*mm2 - tt)*(-2*me2 - 2*mm2 + ss + tt)) + (discbs*(-8*logt*(me2 + mm2 - ss&
            &)**3*ss - 2*(me2 + mm2 - ss)*((me2 - mm2)**2 - 2*(me2 + mm2)*ss + (1 + logm + 2*&
            &logt)*ss**2)*tt - (2*(me2 - mm2)**2 + (logm + 2*logt)*(me2 + mm2)*ss - (2 + logm&
            & + 2*logt)*ss**2)*tt**2))/(me2**2 + (mm2 - ss)**2 - 2*me2*(mm2 + ss)) + (discbu*&
            &(-2*logt*(me2 + mm2 - ss - tt)*(2*me2 + 2*mm2 - ss - tt)*(4*(me2 + mm2 - ss)**2 &
            &- 2*(2*(me2 + mm2) - 3*ss)*tt + 3*tt**2) + tt*(-2*(me2 + mm2 - ss)*(me2**2 + 4*l&
            &ogm*me2**2 - 2*me2*mm2 + 8*logm*me2*mm2 + mm2**2 + 4*logm*mm2**2 - 2*(1 + 2*logm&
            &)*(me2 + mm2)*ss + (1 + logm)*ss**2) + (-16*me2*mm2 + 14*logm*(me2 + mm2)**2 - (&
            &4 + 17*logm)*(me2 + mm2)*ss + (4 + 5*logm)*ss**2)*tt + (-((-2 + 7*logm)*(me2 + m&
            &m2)) + 2*(1 + 2*logm)*ss)*tt**2 + logm*tt**3)))/(me2**2 + (-mm2 + ss + tt)**2 - &
            &2*me2*(mm2 + ss + tt)) + 4*logmu*((discbs*(me2 + mm2 - ss)*ss*(-2*(me2 + mm2 - s&
            &s)**2 - 2*ss*tt - tt**2))/(me2**2 + (mm2 - ss)**2 - 2*me2*(mm2 + ss)) - (discbu*&
            &(me2 + mm2 - ss - tt)*(2*me2 + 2*mm2 - ss - tt)*(2*(me2 + mm2 - ss)**2 + 2*ss*tt&
            & + tt**2))/(me2**2 + (-mm2 + ss + tt)**2 - 2*me2*(mm2 + ss + tt)))

  em2eml_em = 16*pi*alpha**3*em2eml_em/tt**2 * Qe * Qmu

  if (present(pole)) then
    pole = (discbs*(me2 + mm2 - ss)*ss*(-2*(me2 + mm2 - ss)**2 - 2*ss*tt - tt**2))/(me2**2 &
       &+ (mm2 - ss)**2 - 2*me2*(mm2 + ss)) - (discbu*(me2 + mm2 - ss - tt)*(2*me2 + 2*m&
       &m2 - ss - tt)*(2*(me2 + mm2 - ss)**2 + 2*ss*tt + tt**2))/(me2**2 + (-mm2 + ss + &
       &tt)**2 - 2*me2*(mm2 + ss + tt))
    pole = 64*pi*alpha**3*pole/tt**2 * Qe * Qmu
  endif

  END FUNCTION EM2EML_EM


  FUNCTION EM2EMG_EM(p1,p2,q1,q2,q3)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(q3)
    !! for massive (and massless) electrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4),q3(4)
  real (kind=prec) :: me2,mm2,s12,s13,s14,s15,s23,s24,s25,s34,s35,s45
  real (kind=prec) :: if13,if14,if23,if24,den13,den14,den23,den24,denAll
  real (kind=prec) :: em2emg_em
#include "charge_config.h"

  s12 = s(p1,p2); s13 = s(p1,q1); s14 = s(p1,q2); s15 = s(p1,q3)
  s23= s(p2,q1); s24 = s(p2,q2); s25 = s(p2,q3)
  s34 = s(q1,q2); s35 = s(q1,q3); s45 = s(q2,q3)
  me2=sq(p1); mm2=sq(p2)
  den13 = s15*s25; den14 = s25*s35; den23 = s15*s45
  den24 = s35*s45; denAll = (2*me2-s13)*(2*mm2-s24)

  if13 = -2*s12*s14*s23 + s14*s15*s23 - s15*s23*s24 - s13*s14*s25 + s14*s23*s25 - 2*s12**2*s34&
         + 2*s12*s15*s34 + 2*s12*s25*s34 - 2*s15*s25*s34 + s12*s14*s35 + 2*mm2*(-(s13*(s15 + s25))&
         + s12*(2*s13 - s35) + s15*(s23 - s34 + s35)) + s12*s23*s45 - 2*me2*(s15*s24 - s14*s25&
         + s24*s25 + mm2*(8*s12 - 4*(s15 + s25)) + s25*s34 - s25*s45 + s12*(-2*s24 + s45))

  if14 = 2*s14*s23**2 + 2*s14*s23*s25 + 2*s12*s23*s34 - s15*s23*s34 + s12*s25*s34 + s13*s25*s34&
         - 2*s14*s23*s35 - s12*s24*s35 - 2*s14*s25*s35 - s12*s34*s35 - 2*mm2*(s15*s23 + s13*(2*s23&
         + s25 - s35) - s12*s35 + s14*s35 - s15*s35) + s12*s23*s45 + 2*me2*(-2*s23*s24 + s14*s25&
         - s24*s25 - s25*s34 + 4*mm2*(2*s23 + s25 - s35) + s24*s35 + s23*s45 + s25*s45)

  if23 = 2*s14**2*s23 + 2*s14*s15*s23 + 2*s12*s14*s34 + s12*s15*s34 + s15*s24*s34 - s14*s25*s34&
         + s12*s14*s35 - s12*s13*s45 - 2*s14*s23*s45 - 2*s15*s23*s45 - s12*s34*s45 + 2*me2*(-(s15*s24)&
         - s14*(2*s24 + s25) + 4*mm2*(2*s14 + s15 - s45) + s12*s45 - s23*s45 + s24*s45 + s25*s45)&
         + 2*mm2*(s14*s35 + s15*(s23 - s34 + s35) + s13*(-2*s14 - s15 + s45))

  if24 = -2*s14*s23*s34 - s15*s23*s34 - s14*s25*s34 - 2*s12*s34**2 - s14*s23*s35 + s14*s24*s35&
         - 2*s12*s34*s35 + s13*s23*s45 - s14*s23*s45 - 2*s12*s34*s45 - 2*s12*s35*s45 + 2*mm2*((s12&
         - s14)*s35 + s15*(s34 + s35) + s13*(2*s34 + s35 + s45)) + 2*me2*(s25*s34 + s12*s45 - s23*s45&
         + s25*s45 - 4*mm2*(2*s34 + s35 + s45) + s24*(2*s34 + s35 + s45))

  em2emg_em = 128*pi**3*alpha**3 * Qe * Qmu * &
               2*(if13/den13 + if14/den14 + if23/den23 + if24/den24)/denAll
  END FUNCTION EM2EMG_EM

  ! !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! The following matrix elements are no longer supported and are kept
  ! for legacy / testing purposes. Use at your own risk.

  FUNCTION EM2EML_EE_mfied(p1,p2,q1,q2)
  use massification
  real (kind=prec)::p1(4),p2(4),q1(4),q2(4)
  real (kind=prec)::born,ol(-2:2),olZ(-1:1)
  real (kind=prec)::em2eml_ee_mfied

  born  = em2em(p1,p2,q1,q2)
  ol    = (/ 0.,0.,0.,0.,0. /)
  ol(0) = em2eml_ee_mless(p1,p2,q1,q2)

  call massify(log(mel**2/musq), 1., real(born), 2*pi*real(ol), oneloopZ=olZ)
  ! mel**2 since p1 is modified

  olZ = olZ / 2 / pi
  em2eml_ee_mfied = olZ(0)
  END FUNCTION EM2EML_EE_mfied

  FUNCTION EM2EMG_EE_icoll(p1,p2,q1,q2,q3)
  real (kind=prec)::p1(4),p2(4),q1(4),q2(4),q3(4)
  real (kind=prec)::born,split
  real (kind=prec)::em2emg_ee_icoll

  born = em2em(p1-q3,p2,q1,q2)
  split = split0_isr(p1,q3)
  em2emg_ee_icoll = born*split
  END FUNCTION EM2EMG_EE_icoll

  FUNCTION EM2EMG_EE_fcoll(p1,p2,q1,q2,q3)
  real (kind=prec)::p1(4),p2(4),q1(4),q2(4),q3(4)
  real (kind=prec)::born,split
  real (kind=prec)::em2emg_ee_fcoll

  born = em2em(p1,p2,q1+q3,q2)
  split = split0_fsr(q1,q3)
  em2emg_ee_fcoll = born*split
  END FUNCTION EM2EMG_EE_fcoll

  FUNCTION EM2EMGL_EE_icoll(p1,p2,q1,q2,q3)
  real (kind=prec)::p1(4),p2(4),q1(4),q2(4),q3(4)
  real (kind=prec)::born,ol,olZ,split0,split1
  real (kind=prec)::em2emgl_ee_icoll

  born = em2em(p1-q3,p2,q1,q2)
  ol   = em2eml_ee_mless(p1-q3,p2,q1,q2)
  olZ  = em2eml_ee_mfied(p1-q3,p2,q1,q2)
  split0 = split0_isr(p1,q3)
  split1 = split1_isr(p1,q3)

  em2emgl_ee_icoll = split0*olZ + split1*born
  END FUNCTION EM2EMGL_EE_icoll

  FUNCTION EM2EMGL_EM_icoll(p1,p2,q1,q2,q3)
  real (kind=prec)::p1(4),p2(4),q1(4),q2(4),q3(4)
  real (kind=prec)::born,ol,split0
  real (kind=prec)::em2emgl_em_icoll

  born = em2em(p1-q3,p2,q1,q2)
  ol   = em2eml_em_mless(p1-q3,p2,q1,q2)+em2eml_mm_mless(p1-q3,p2,q1,q2)
  split0 = split0_isr(p1,q3)

  em2emgl_em_icoll = split0*ol
  END FUNCTION EM2EMGL_EM_icoll

  FUNCTION EM2EMGL_EE_fcoll(p1,p2,q1,q2,q3)
  real (kind=prec)::p1(4),p2(4),q1(4),q2(4),q3(4)
  real (kind=prec)::born,ol,olZ,split0,split1
  real (kind=prec)::em2emgl_ee_fcoll

  born = em2em(p1,p2,q1+q3,q2)
  ol   = em2eml_ee_mless(p1,p2,q1+q3,q2)
  olZ  = em2eml_ee_mfied(p1,p2,q1+q3,q2)
  split0 = split0_fsr(q1,q3)
  split1 = split1_fsr(q1,q3)

  em2emgl_ee_fcoll = split1*born + split0*olZ
  END FUNCTION EM2EMGL_EE_fcoll

  FUNCTION EM2EMGL_EM_fcoll(p1,p2,q1,q2,q3)
  real (kind=prec)::p1(4),p2(4),q1(4),q2(4),q3(4)
  real (kind=prec)::born,ol,split0
  real (kind=prec)::em2emgl_em_fcoll

  born = em2em(p1,p2,q1+q3,q2)
  ol   = em2eml_em_mless(p1,p2,q1+q3,q2)+em2eml_mm_mless(p1,p2,q1+q3,q2)
  split0 = split0_fsr(q1,q3)

  em2emgl_em_fcoll = split0*ol
  END FUNCTION EM2EMGL_EM_fcoll

  ! !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!




          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          !!!!                                    !!!!
          !!!!              NNLO                  !!!!
          !!!!                                    !!!!
          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  FUNCTION EM2EMeik_EE(p1,p2,p3,p4, xicut, pole, lin)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) whatever
    !! for massive electrons
  real (kind=prec) :: p1(4), p2(4), p3(4), p4(4)
  real (kind=prec) :: em2emeik_ee, xicut, Epart, scms
  real (kind=prec), optional :: pole, lin

  scms = sq(p1+p2)
  Epart = sqrt(scms)

  em2emeik_ee = alpha/(2.*pi)*Ieik(xicut, Epart, parts((/part(p1, 1, 1), part(p3, 1, -1)/)))

  if (present(pole)) then
    pole = alpha/(2.*pi)*(2*Isin(xicut,Epart,p1,p3) &
                  - Isin(xicut,Epart,p1)- Isin(xicut,Epart,p3))
  endif
  if (present(lin)) then
    call crash("Not fully implemeted yet")
  endif

  END FUNCTION EM2EMeik_EE

  FUNCTION EM2EMeik_EM(p1,p2,p3,p4, xicut, pole, lin)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) whatever
    !! for massive electrons
  real (kind=prec) :: p1(4), p2(4), p3(4), p4(4)
  real (kind=prec) :: em2emeik_em, xicut, Epart, scms
  real (kind=prec), optional :: pole, lin
#include "charge_config.h"

  scms = sq(p1+p2)
  Epart = sqrt(scms)

  em2emeik_em = alpha/(2.*pi)*Ieik(xicut, Epart,                                         &
                                   parts((/part(p1,Qe,+1,1), part(p2,Qmu,+1,2) ,         &
                                           part(p3,Qe,-1,1), part(p4,Qmu,-1,2) /), "x"), &
                                   pole)
  if (present(pole)) then
    pole = alpha/(2.*pi)*pole
  endif
  if (present(lin)) then
    call crash("Not fully implemeted yet")
  endif

  END FUNCTION EM2EMeik_EM

  FUNCTION EM2EMeik(p1,p2,p3,p4, xicut, pole, lin)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) whatever
    !! for massive electrons
  real (kind=prec) :: p1(4), p2(4), p3(4), p4(4)
  real (kind=prec) :: em2emeik, xicut, Epart, scms
  real (kind=prec), optional :: pole, lin
#include "charge_config.h"

  scms = sq(p1+p2)
  Epart = sqrt(scms)

  em2emeik = alpha/(2.*pi)*Ieik(xicut, Epart, parts((/part(p1,Qe, +1), part(p2,Qmu, +1) ,    &
                                                      part(p3,Qe, -1), part(p4,Qmu, -1) /)), &
                                pole)
  if (present(pole)) then
    pole = alpha/(2.*pi)*pole
  endif
  if (present(lin)) then
    call crash("Not fully implemeted yet")
  endif

  END FUNCTION EM2EMeik

  FUNCTION EM2EMll_EEEE(p1, p2, p3, p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! for massive electrons
  use ff_heavyquark
  implicit none
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4), em2emll_eeee
  real(kind=prec) :: x, ss, tt, me2, mm2, lm
  real(kind=prec) :: f11, f12, f22, f11coeff, f12coeff, f22coeff
  complex(kind=prec) :: F1a1(-1:1), F1a2(-2:0), F2a1(-1:1), F2a2(-2:0)

  ss = sq(p1+p2); tt = sq(p1-p3)
  me2=sq(p1); mm2=sq(p2)
  x = (sqrt(-tt+4*me2)-sqrt(-tt))/(sqrt(-tt+4*me2)+sqrt(-tt))

  lm = log(musq/me2)
  call hqff(x, Lm, oneloopF1=F1a1, oneloopF2=F2a1, twoloopF1=F1a2, twoloopF2=F2a2)

  f11 = real(F1a1(0)**2 + 2*F1a1(+1)*F1a1(-1) + 2*F1a2(0), prec)
  f22 = real(F2a1(0)**2, prec)
  f12 = real(F1a1(0)*F2a1(0) + F1a1(-1)*F2a1(+1) + F2a2(0), prec)

  f11coeff = 4.*(mm2 - ss)**2*x**2 - 4.*me2*x*(ss - 2.*mm2*x + ss*x**2) &
              + 2.*me2**2*(1. - 4.*x + 8.*x**2 - 4.*x**3 + x**4)
  f22coeff = ((mm2 - ss)**2*x + me2**2*(2. - 3.*x + 2*x**2) &
                + me2*(mm2*(1. - 4.*x + x**2) - ss*(1. + x**2))) * (1.-x)**2
  f12coeff =  4. * me2*(x-1.)**2*(me2 * (x-1.)**2 - 2*mm2*x)

  em2emll_eeee = f11coeff * f11 + f22coeff * f22 + f12coeff * f12
  em2emll_eeee = em2emll_eeee * 4.*alpha**4 / (me2**2 * (1-x)**4)
  END FUNCTION EM2EMLL_EEEE

  FUNCTION EM2EMff_EEEE(p1, p2, p3, p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! for massive electrons, requires xieik1 = xieik2
  use ff_heavyquark
  implicit none
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4), em2emff_eeee
  real(kind=prec) :: x, ss, tt, me2, mm2
  real(kind=prec) :: f1(0:2), f2(0:2), f11, f22, f12
  real(kind=prec) :: f11coeff, f12coeff, f22coeff
  real(kind=prec) :: ctfin, ctpole, oldmu
  complex(kind=prec) :: F1a1(-1:1), F1a2(-2:0), F2a1(-1:1), F2a2(-2:0)



  ss = sq(p1+p2); tt = sq(p1-p3)
  me2=sq(p1); mm2=sq(p2)
  x = (sqrt(-tt+4*me2)-sqrt(-tt))/(sqrt(-tt+4*me2)+sqrt(-tt))

  oldmu = musq
  musq = me2
  ctfin = em2emeik_ee(p1, p2, p3, p4, xieik2, ctpole)
  musq = oldmu

  call hqff(x, 0., oneloopF1=F1a1, oneloopF2=F2a1, twoloopF1=F1a2, twoloopF2=F2a2)

  ! TODO this is rather ugly..

  f1(0) = 1.

  f1(1) = ctfin
  f1(1) = real(f1(1) + F1a1(0) / pi, prec)

  f1(2) = 0.5*ctfin**2
  f1(2) = real(f1(2) + (F1a1(0) * ctfin + F1a1(+1) * ctpole) / pi, prec)
  f1(2) = real(f1(2) + F1a2(0) / pi**2, prec)

  f2(0) = 0.

  f2(1) = real(F2a1(0) / pi, prec)

  f2(2) = 0.
  f2(2) = real(f2(2) + (F2a1(0) * ctfin + F2a1(+1) * ctpole) / pi, prec)
  f2(2) = real(f2(2) + F2a2(0) / pi**2, prec)

  f11 = f1(1)**2 + 2*f1(0)*f1(2)
  f22 = f2(1)**2 + 2*f2(0)*f2(2)
  f12 = f1(2)*f2(0) + f1(1)*f2(1) + f1(0)*f2(2)

  f11coeff = 4.*(mm2 - ss)**2*x**2 - 4.*me2*x*(ss - 2.*mm2*x + ss*x**2) &
              + 2.*me2**2*(1. - 4.*x + 8.*x**2 - 4.*x**3 + x**4)
  f22coeff = ((mm2 - ss)**2*x + me2**2*(2. - 3.*x + 2*x**2) &
                + me2*(mm2*(1. - 4.*x + x**2) - ss*(1. + x**2))) * (1.-x)**2
  f12coeff =  4. * me2*(x-1.)**2*(me2 * (x-1.)**2 - 2*mm2*x)

  em2emff_eeee = f11coeff * f11 + f22coeff * f22 + f12coeff * f12
  em2emff_eeee = em2emff_eeee * 4.*pi**2*alpha**4 / (me2**2 * (1-x)**4)
  END FUNCTION EM2EMff_EEEE


  ! This matrix element is no longer supported and is kept
  ! for legacy / testing purposes. Use at your own risk.
  FUNCTION EM2EMGL_EEEE_PV(p1, p2, p3, p4, p5, pole)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(q3)
    !! for massive electrons
  real(kind=prec), optional :: pole
  real(kind=prec) :: em2emgl_eeee_pv, p1(4), p2(4), p3(4), p4(4), p5(4)
  real(kind=prec) :: s13, s15, s35, mm2, gramdet

  s13 = s(p1,p3); s15 = s(p1,p5);
  s35 = s(p3,p5); mm2=sq(p1)

  gramdet = (-(s13*s15*s35) + mm2*(s15**2 + s35**2))

  if(present(pole)) then
    if(abs(gramdet)<1._prec) then
      em2emgl_eeee_pv = em2emgl_eeee_coll(p1,p2,p3,p4,p5,pole)
    else
      em2emgl_eeee_pv = em2emgl_eeee_pvred(p1,p2,p3,p4,p5,pole)
    endif
  else
    if(abs(gramdet)<1._prec) then
      em2emgl_eeee_pv = em2emgl_eeee_coll(p1,p2,p3,p4,p5)
    else
      em2emgl_eeee_pv = em2emgl_eeee_pvred(p1,p2,p3,p4,p5)
    endif
  endif
  END FUNCTION EM2EMGL_EEEE_PV

  ! This matrix element is no longer supported and is kept
  ! for legacy / testing purposes. Use at your own risk.
  FUNCTION EM2EMGL_EEEE_CO(p1, p2, p3, p4, p5, pole)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(q3)
    !! for massive electrons
  real(kind=prec), optional :: pole
  real(kind=prec) :: em2emgl_eeee_co, p1(4), p2(4), p3(4), p4(4), p5(4)

  if(present(pole)) then
     em2emgl_eeee_co = em2emgl_eeee_coll(p1,p2,p3,p4,p5,pole)
  else
     em2emgl_eeee_co = em2emgl_eeee_coll(p1,p2,p3,p4,p5)
  endif
  END FUNCTION EM2EMGL_EEEE_CO

  ! This matrix element is no longer supported and is kept
  ! for legacy / testing purposes. Use at your own risk.
  !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(q3)
  !! for massive electrons
  FUNCTION EM2EMGL_MMMM_PV(p1, p2, p3, p4, p5, pole)
  real(kind=prec), optional :: pole
  real(kind=prec) :: em2emgl_mmmm_pv, p1(4), p2(4), p3(4), p4(4), p5(4)
  real(kind=prec) :: s13, s15, s35, mm2, gramdet

  s13 = s(p2,p4); s15 = s(p2,p5);
  s35 = s(p4,p5); mm2=sq(p2)

  gramdet = (-(s13*s15*s35) + mm2*(s15**2 + s35**2))

  if(present(pole)) then
    if(abs(gramdet)<1._prec) then
      em2emgl_mmmm_pv = em2emgl_eeee_coll(p2,p1,p4,p3,p5,pole)
    else
      em2emgl_mmmm_pv = em2emgl_eeee_pvred(p2,p1,p4,p3,p5,pole)
    endif
  else
    if(abs(gramdet)<1._prec) then
      em2emgl_mmmm_pv = em2emgl_eeee_coll(p2,p1,p4,p3,p5)
    else
      em2emgl_mmmm_pv = em2emgl_eeee_pvred(p2,p1,p4,p3,p5)
    endif
  endif
  END FUNCTION EM2EMGL_MMMM_PV

  ! This matrix element is no longer supported and is kept
  ! for legacy / testing purposes. Use at your own risk.
  !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(q3)
  !! for massive electrons
  FUNCTION EM2EMGL_MMMM_CO(p1, p2, p3, p4, p5, pole)
  real(kind=prec), optional :: pole
  real(kind=prec) :: em2emgl_mmmm_co, p1(4), p2(4), p3(4), p4(4), p5(4)

  if(present(pole)) then
     em2emgl_mmmm_co = em2emgl_eeee_coll(p2,p1,p4,p3,p5,pole)
  else
     em2emgl_mmmm_co = em2emgl_eeee_coll(p2,p1,p4,p3,p5)
  endif
  END FUNCTION EM2EMGL_MMMM_CO


  !!! vacuum polarisation corrections !!!


  !! classification according to [1901.03106]


  !! class I (+ genuine 2-loop)
  FUNCTION EM2EM_AA(p1,p2,q1,q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2)
    !! for massive electrons, numerically interpolated
  real (kind=prec), intent(in) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: em2em_aa,deltalph0_tot,deltalph1_tot,tt

  tt = sq(p1-q1)
  ! bubble chain
  deltalph0_tot = deltalph_0_tot(tt)

  ! genuine 2-loop
  deltalph1_tot = deltalph_l_1_tot(tt)

  em2em_aa = (3*deltalph0_tot**2+2*deltalph1_tot)*em2em(p1,p2,q1,q2)
  END FUNCTION EM2EM_AA


  !! class II
  FUNCTION EM2EM_ALEE(p1,p2,q1,q2,pole)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2)
    !! for massive electrons
  real (kind=prec), intent(in) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec), optional :: pole
  real (kind=prec) :: em2em_alee,deltalph_tot,tt

  tt = sq(p1-q1)
  deltalph_tot = deltalph_0_tot(tt)
  ! Factor of 2 due to (1-loop)^2 and factorisable 2-loop
  if(present(pole)) then
    em2em_alee = 2*deltalph_tot*em2eml_ee(p1,p2,q1,q2,pole)
    pole = 2*deltalph_tot*pole
  else
    em2em_alee = 2*deltalph_tot*em2eml_ee(p1,p2,q1,q2)
  endif
  END FUNCTION EM2EM_ALEE

  FUNCTION EM2EM_ALMM(p1,p2,q1,q2,pole)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2)
    !! for massive electrons
  real (kind=prec), intent(in) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec), optional :: pole
  real (kind=prec) :: em2em_almm

  if(present(pole)) then
    em2em_almm = em2em_alee(p2,p1,q2,q1,pole)
  else
    em2em_almm = em2em_alee(p2,p1,q2,q1)
  endif
  END FUNCTION EM2EM_ALMM

  FUNCTION EM2EM_ALEM(p1,p2,q1,q2,pole)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2)
    !! for massive electrons
  real (kind=prec), intent(in) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec), optional :: pole
  real (kind=prec) :: em2em_alem,deltalph_tot,tt,pole_nf,pole_l

  tt = sq(p1-q1)
  deltalph_tot = deltalph_0_tot(tt)
  ! Contribution from (1-loop)^2 and non-factorisable box
  if(present(pole)) then
    em2em_alem = deltalph_tot*(em2eml_em(p1,p2,q1,q2,pole_l)&
                               +em2em_nfem_sin(p1,p2,q1,q2,pole_nf))
    pole = deltalph_tot*(pole_nf+pole_l)
  else
    em2em_alem = deltalph_tot*(em2eml_em(p1,p2,q1,q2)&
                               +em2em_nfem_sin(p1,p2,q1,q2))
  endif
  END FUNCTION EM2EM_ALEM


  !! class III
  FUNCTION EM2EMG_AEE(p1,p2,q1,q2,q3)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(q3)
    !! for massive electrons
  real (kind=prec), intent(in) :: p1(4),p2(4),q1(4),q2(4),q3(4)
  real (kind=prec) :: em2emg_aee,deltalph_tot,tt

  tt = sq(p1-q1-q3)
  deltalph_tot = deltalph_0_tot(tt)
  em2emg_aee = 2*deltalph_tot*em2emg_ee(p1,p2,q1,q2,q3)
  END FUNCTION EM2EMG_AEE

  FUNCTION EM2EMG_AMM(p1,p2,q1,q2,q3)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(q3)
    !! for massive electrons
  real (kind=prec), intent(in) :: p1(4),p2(4),q1(4),q2(4),q3(4)
  real (kind=prec) :: em2emg_amm

  em2emg_amm = em2emg_aee(p2,p1,q2,q1,q3)
  END FUNCTION EM2EMG_AMM

  FUNCTION EM2EMG_AEM(p1,p2,q1,q2,q3)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(q3)
    !! for massive electrons
  real (kind=prec), intent(in) :: p1(4),p2(4),q1(4),q2(4),q3(4)
  real (kind=prec) :: em2emg_aem,tee,tmm

  tee = sq(p1-q1)
  tmm = sq(p2-q2)
  em2emg_aem = (deltalph_0_tot(tee)+deltalph_0_tot(tmm))*em2emg_em(p1,p2,q1,q2,q3)
  END FUNCTION EM2EMG_AEM

  FUNCTION EM2EMG_A(p1,p2,q1,q2,q3)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(q3)
    !! for massive electrons
  real (kind=prec), intent(in) :: p1(4),p2(4),q1(4),q2(4),q3(4)
  real (kind=prec) :: em2emg_a

  em2emg_a = em2emg_aee(p1,p2,q1,q2,q3)&
             +em2emg_aem(p1,p2,q1,q2,q3)&
             +em2emg_amm(p1,p2,q1,q2,q3)
  END FUNCTION EM2EMG_A

  !! class IV
  FUNCTION EM2EM_NFEE(y, p1,p2,q1,q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2)
    !! for massive electrons
  real (kind=prec), intent(in) :: p1(4),p2(4),q1(4),q2(4), y
  real (kind=prec) :: em2em_nfee
  real (kind=prec) :: ss, x, mm2, tt, mp2
  real (kind=prec) :: F1, F2


  ss = sq(p1+p2); tt = sq(p1-q1)
  mm2=sq(p1); mp2=sq(p2)
  x = (sqrt(-tt+4*mm2)-sqrt(-tt))/(sqrt(-tt+4*mm2)+sqrt(-tt))

  F1 = f1nf(y,tt,mm2)
  F2 = f2nf(y,tt,mm2)

  em2em_nfee  = 64*mp2*x**2*(F2*mm2*(-1 + x)**2*(mm2*(-1 + x)**2 - 2*mp2*x)&
              + F1*(2*(mp2 - ss)**2*x**2 - 2*mm2*x*(ss - 2*mp2*x + ss*x**2)&
              + mm2**2*(1 - 4*x + 8*x**2 - 4*x**3 + x**4)))

  em2em_nfee = alpha**2*pi**2/(mm2**2*mp2*(1-x)**4*x**2)*em2em_nfee

  END FUNCTION EM2EM_NFEE

  FUNCTION EM2EM_NFMM(y, p1,p2,q1,q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2)
    !! for massive electrons
  real (kind=prec), intent(in) :: p1(4),p2(4),q1(4),q2(4), y
  real (kind=prec) :: em2em_nfmm

  em2em_nfmm = em2em_nfee(y,p2,p1,q2,q1)
  END FUNCTION EM2EM_NFMM

  FUNCTION EM2EM_NF(y, p1,p2,q1,q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2)
    !! for massive electrons
  real (kind=prec), intent(in) :: p1(4),p2(4),q1(4),q2(4), y
  real (kind=prec) :: em2em_nf

  em2em_nf = em2em_nfee(y,p1,p2,q1,q2)&
             + em2em_nfem(y,p1,p2,q1,q2)&
             + em2em_nfmm(y,p1,p2,q1,q2)
  END FUNCTION EM2EM_NF


  !! class IV
  FUNCTION EM2EM_NFEE_INTERPOL(p1,p2,q1,q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2)
    !! for massive electrons, numerically interpolated
  real (kind=prec), intent(in) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: em2em_nfee_interpol, ss, x, mm2, tt, mp2
  real (kind=prec) :: F1, F2

  ss = sq(p1+p2); tt = sq(p1-q1)
  mm2=sq(p1); mp2=sq(p2)
  x = (sqrt(-tt+4*mm2)-sqrt(-tt))/(sqrt(-tt+4*mm2)+sqrt(-tt))

  F1 = 0._prec; F2 = 0._prec
  if(nel==1) then
    F1 = F1 + f1el_electron(tt)
    F2 = F2 + f2el_electron(tt)
  end if
  if(nmu==1) then
    F1 = F1 + f1el_muon(tt)
    F2 = F2 + f2el_muon(tt)
  end if
  if(ntau==1) then
    F1 = F1 + f1el_tau(tt)
    F2 = F2 + f2el_tau(tt)
  end if
  if(nhad==1) then
    F1 = F1 + f1el_had(tt)
    F2 = F2 + f2el_had(tt)
  end if


  em2em_nfee_interpol  = 64*mp2*x**2*(F2*mm2*(-1 + x)**2*(mm2*(-1 + x)**2 - 2*mp2*x)&
                       + F1*(2*(mp2 - ss)**2*x**2 - 2*mm2*x*(ss - 2*mp2*x + ss*x**2)&
                       + mm2**2*(1 - 4*x + 8*x**2 - 4*x**3 + x**4)))

  em2em_nfee_interpol = alpha**2*pi**2/(mm2**2*mp2*(1-x)**4*x**2)*em2em_nfee_interpol

  END FUNCTION EM2EM_NFEE_INTERPOL


  !!!  end vacuum polarisation corrections !!!




                  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                  !!                             !!
                  !!            MUPAIR           !!
                  !!                             !!
                  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!




  !!!!!!!!!!    Born    !!!!!!!!!!!!

  FUNCTION EE2MM(p1,p2,p3,p4)
    !! e-(p1) e+(p2) -> mu+(p3) mu-(p4)
    !! for massive electrons & positrons
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4)
  real (kind=prec) :: ee2mm

  ee2mm = em2em(p1,-p3,-p2,p4)

  END FUNCTION


  FUNCTION EE2MML(p1, p2, p3, p4,sing)
    !! e-(p1) e+(p2) -> mu+(p3) mu-(p4)
    !! for massive electrons & positrons
  implicit none
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4)
  real(kind=prec) :: ee2mml
  real(kind=prec),optional :: sing
  real(kind=prec) :: sing_ee, sing_em, sing_mm

  if(present(sing)) then
    ee2mml = em2eml_ee(p1,-p3,-p2,p4,sing_ee)&
             +em2eml_em(p1,-p3,-p2,p4,sing_em)&
             +em2eml_mm(p1,-p3,-p2,p4,sing_mm)
    sing = sing_ee + sing_em + sing_mm
  else
    ee2mml = em2eml_ee(p1,-p3,-p2,p4)&
             +em2eml_em(p1,-p3,-p2,p4)&
             +em2eml_mm(p1,-p3,-p2,p4)
  end if
  END FUNCTION


  FUNCTION EE2MMG(p1,p2,p3,p4,p5)
    !! e-(p1) e+(p2) -> mu-(p3) mu+(p4) g(p5)
    !! for massive electrons & positrons
  implicit none
  real(kind=prec) :: p1(4),p2(4),p3(4),p4(4),p5(4)
  real(kind=prec) :: ee2mmg

  ee2mmg = em2emg_ee(p1,-p3,-p2,p4,p5)&
           -em2emg_em(p1,-p3,-p2,p4,p5)&
           +em2emg_mm(p1,-p3,-p2,p4,p5)

  END FUNCTION


  FUNCTION PEPE2MM(p1,pol1,p2,pol2,p3,p4)
    !! e-(p1) e+(p2) -> g* -> mu-(p3) mu+(p4)
    !! for massive electrons
  implicit none
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4),pol1(4),pol2(4)
  real (kind=prec) :: ss,tt, me2, mm2, s2n, s3n, s4n, s1m, s3m, s4m, snm, e
  real (kind=prec) :: pepe2mm

  ss = sq(p1+p2); tt = sq(p1-p3)
  me2=sq(p1); mm2=sq(p3)
  s2n = s(pol1,p2); s3n = s(pol1,p3); s4n = s(pol1,p4)
  s1m = s(pol2,p1); s3m = s(pol2,p3); s4m = s(pol2,p4)
  snm = s(pol1,pol2)
  e = sqrt(4*pi*alpha)

  pepe2mm = 0.25*(-4*e**4*(2*me2**2*(-2 + snm) + 2*mm2**2*(-2 + snm) + s1m*s2n*ss - s2n*s3m*ss + &
            s3n*s4m*ss - s1m*s4n*ss + s3m*s4n*ss - 2*ss**2 - s2n*s3m*tt + s1m*s3n*tt + &
            s2n*s4m*tt - s1m*s4n*tt - 4*ss*tt + 2*snm*ss*tt - 4*tt**2 + 2*snm*tt**2 + &
            mm2*(s2n*s3m - s2n*s4m + s1m*(-2*s2n - s3n + s4n) + 8*tt - 4*snm*tt) + me2*(s2n*s3m &
            - s1m*s3n - s2n*s4m + s1m*s4n + 4*mm2*(-2 + snm) + 8*tt - 4*snm*tt)))/ss**2

  END FUNCTION


  FUNCTION PEPEZMM(p1,pol1,p2,pol2,p3,p4)
    !! e-(p1) e+(p2) -> Z -> mu-(p3) mu+(p4)
    !! for massive electrons
  implicit none
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4),pol1(4),pol2(4)
  real (kind=prec) :: ss,tt, me2, mm2, s2n, s3n, s4n, s1m, s3m, s4m, snm
  real (kind=prec) :: me1, mm1
  real (kind=prec) :: pepezmm

  ss = sq(p1+p2); tt = sq(p1-p3)
  me2=sq(p1); mm2=sq(p3); me1 = sqrt(me2); mm1 = sqrt(mm2)
  s2n = s(pol1,p2); s3n = s(pol1,p3); s4n = s(pol1,p4)
  s1m = s(pol2,p1); s3m = s(pol2,p3); s4m = s(pol2,p4)
  snm = s(pol1,pol2)


  pepezmm = (alpha**2*pi**2*(-2*me2**2*mZ**4*(1 - 4*sW2 + 8*sW2**2)*(-1 - 2*(-2 + &
            snm)*sW2 + 4*(-2 + snm)*sW2**2) - me1**3*mZ**2*(-1 + 4*sW2)*((s1m - &
            s2n - s3m + s3n - s4m + s4n)*ss + 2*mZ**2*(2*(s3n - s4m)*sW2*(-1 + &
            2*sW2) + s4n*(-1 + 2*sW2 - 4*sW2**2) + s3m*(1 - 2*sW2 + 4*sW2**2))) + &
            me1*mZ**2*(-1 + 4*sW2)*((s1m - s2n - s3m + s3n - s4m + s4n)*ss**2*(1 &
            - 2*sW2 + 4*sW2**2) + 2*mZ**2*ss*(2*(s3n - s4m)*sW2*(-1 + 2*sW2) + &
            s4n*(-1 + 4*sW2 - 8*sW2**2) + s3m*(1 - 4*sW2 + 8*sW2**2)) + &
            mm2*((-s1m + s2n + s3m - s3n + s4m - s4n)*ss + 2*mZ**2*(2*(2*s1m - &
            2*s2n - s3n + s4m)*sW2*(-1 + 2*sW2) + s3m*(-1 + 2*sW2 - 4*sW2**2) + &
            s4n*(1 - 2*sW2 + 4*sW2**2))) + (s1m - s2n - s3m + s3n - s4m + &
            s4n)*ss*tt + 2*mZ**2*(2*(s3n - s4m)*sW2*(-1 + 2*sW2) + s4n*(-1 + &
            2*sW2 - 4*sW2**2) + s3m*(1 - 2*sW2 + 4*sW2**2))*tt) + &
            me2*(mZ**2*ss*(-((s3n*s4m - s3m*s4n)*(1 - 4*sW2)**2) + s2n*(s4m + &
            2*s3m*sW2 - 6*s4m*sW2 - 4*s3m*sW2**2 + 12*s4m*sW2**2) + s1m*(s3n - &
            6*s3n*sW2 + 2*s4n*sW2 + 12*s3n*sW2**2 - 4*s4n*sW2**2 + s2n*(-1 + &
            4*sW2 - 8*sW2**2))) + mm2*(-2*mZ**2*(-(s1m*s2n) + (2 + snm)*ss) + &
            ss*(-(s1m*s2n) + (2 + snm)*ss) - 4*mZ**4*(-1 - 16*sW2**2 + 64*sW2**3 &
            - 64*sW2**4 + 2*snm*sW2*(-1 + 6*sW2 - 16*sW2**2 + 16*sW2**3))) - &
            2*mZ**4*(s1m*s3n*sW2 + s2n*s4m*sW2 + 2*s3n*s4m*sW2 - s1m*s4n*sW2 - &
            6*s1m*s3n*sW2**2 - 6*s2n*s4m*sW2**2 - 4*s3n*s4m*sW2**2 + &
            6*s1m*s4n*sW2**2 + 16*s1m*s3n*sW2**3 + 16*s2n*s4m*sW2**3 - &
            16*s1m*s4n*sW2**3 - 16*s1m*s3n*sW2**4 - 16*s2n*s4m*sW2**4 + &
            16*s1m*s4n*sW2**4 + s3m*s4n*(1 - 6*sW2 + 12*sW2**2) + 2*ss*(1 - 6*sW2 &
            + 12*sW2**2) + s2n*s3m*sW2*(-1 + 6*sW2 - 16*sW2**2 + 16*sW2**3) + &
            2*tt - 16*sW2*tt + 4*snm*sW2*tt + 64*sW2**2*tt - 24*snm*sW2**2*tt - &
            128*sW2**3*tt + 64*snm*sW2**3*tt + 128*sW2**4*tt - 64*snm*sW2**4*tt)) &
            - 2*mZ**4*(mm2**2*(1 - 4*sW2 + 8*sW2**2)*(-1 - 2*(-2 + snm)*sW2 + &
            4*(-2 + snm)*sW2**2) + ss**2*(-1 + 8*sW2 - 24*sW2**2 + 32*sW2**3 - &
            32*sW2**4) - (1 - 4*sW2 + 8*sW2**2)*tt*(s2n*(s3m - s4m)*sW2*(-1 + &
            2*sW2) - s1m*(s3n - s4n)*sW2*(-1 + 2*sW2) + tt + 2*(-2 + snm)*sW2*tt &
            - 4*(-2 + snm)*sW2**2*tt) + ss*(-(s3n*s4m*sW2) - s3m*s4n*sW2 + &
            6*s3n*s4m*sW2**2 + 6*s3m*s4n*sW2**2 - 16*s3n*s4m*sW2**3 - &
            16*s3m*s4n*sW2**3 + 16*s3n*s4m*sW2**4 + 16*s3m*s4n*sW2**4 + &
            s2n*s3m*sW2*(1 - 6*sW2 + 16*sW2**2 - 16*sW2**3) + s1m*(s2n - &
            s4n)*sW2*(-1 + 6*sW2 - 16*sW2**2 + 16*sW2**3) - 2*tt + 16*sW2*tt - &
            2*snm*sW2*tt - 48*sW2**2*tt + 12*snm*sW2**2*tt + 64*sW2**3*tt - &
            32*snm*sW2**3*tt - 64*sW2**4*tt + 32*snm*sW2**4*tt) + mm2*(2*ss*(1 - &
            6*sW2 + 12*sW2**2) - (1 - 4*sW2 + 8*sW2**2)*(-(s2n*(s3m - &
            s4m)*sW2*(-1 + 2*sW2)) + s1m*(2*s2n + s3n - s4n)*sW2*(-1 + 2*sW2) + &
            2*(-1 - 2*(-2 + snm)*sW2 + 4*(-2 + snm)*sW2**2)*tt))) + &
            (4*mZ**2*(mZ**2 - ss)*(-1 + sW2)*sW2*(me1*s2n*(4*mm2*mZ**2 + &
            ss**2)*(1 - 4*sW2) - s1m*(me1*(4*mm2*mZ**2 + ss**2)*(1 - 4*sW2) - &
            mZ**2*s2n*(2*mm2 - ss)*(1 - 4*sW2)**2 - s3n*(me2*(ss + mZ**2*(1 - &
            4*sW2)**2) + mZ**2*(1 - 4*sW2)**2*(mm2 - tt)) + s4n*(me2*(ss + &
            mZ**2*(1 - 4*sW2)**2) + mZ**2*(1 - 4*sW2)**2*(mm2 - ss - tt))) + &
            me1*s3n*(1 - 4*sW2)*(2*me2*mZ**2 + 2*mm2*mZ**2 - 2*mZ**2*ss - ss**2 - &
            2*mZ**2*tt) + me1*s4n*(-1 + 4*sW2)*(2*me2*mZ**2 + 2*mm2*mZ**2 - &
            4*mZ**2*ss + ss**2 - 2*mZ**2*tt) - 2*snm*(mZ - 4*mZ*sW2)**2*(me2**2 + &
            mm2**2 + 2*me2*(mm2 - tt) - 2*mm2*tt + tt*(ss + tt)) - &
            s3m*(2*me2*s4n*(mZ**2 - ss) + me2*s2n*(ss + mZ**2*(1 - 4*sW2)**2) + &
            mZ**2*s4n*ss*(1 - 4*sW2)**2 + mZ**2*s2n*(1 - 4*sW2)**2*(mm2 - ss - &
            tt) + me1*(-1 + 4*sW2)*(2*me2*mZ**2 + 2*mm2*mZ**2 - 4*mZ**2*ss + &
            ss**2 - 2*mZ**2*tt)) + 4*mZ**2*(ss**2 + me2**2*(1 - 4*sW2)**2 + &
            mm2**2*(1 - 4*sW2)**2 - 4*ss**2*sW2 + 8*ss**2*sW2**2 + 2*ss*tt - &
            8*ss*sW2*tt + 16*ss*sW2**2*tt + tt**2 - 8*sW2*tt**2 + 16*sW2**2*tt**2 &
            + me2*(-ss + 2*mm2*(1 - 4*sW2)**2 - 2*(1 - 4*sW2)**2*tt) - mm2*(ss + &
            2*(1 - 4*sW2)**2*tt)) + s4m*(me2*((s2n - 2*s3n)*ss + mZ**2*(2*s3n + &
            s2n*(1 - 4*sW2)**2)) + 2*me1**3*mZ**2*(-1 + 4*sW2) + mZ**2*(1 - &
            4*sW2)**2*(mm2*s2n - s3n*ss - s2n*tt) + me1*(-1 + 4*sW2)*(2*mm2*mZ**2 &
            - ss**2 - 2*mZ**2*(ss + tt)))))/ss))/(2.*mZ**4*(mZ**2 - ss)**2*(-1 + &
            sW2)**2*sW2**2)

  END FUNCTION


  FUNCTION PEPEZMMX(p1,pol1,p2,pol2,p3,p4)
    !! e-(p1) e+(p2) -> Z -> mu-(p3) mu+(p4)
    !! for massive electrons, expanded
  implicit none
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4),pol1(4),pol2(4)
  real (kind=prec) :: ss,tt, me2, mm2, s2n, s3n, s4n, s1m, s3m, s4m, snm
  real (kind=prec) :: me1, mm1, c, e
  real (kind=prec) :: pepezmmx

  ss = sq(p1+p2); tt = sq(p1-p3)
  me2=sq(p1); mm2=sq(p4); me1 = sqrt(me2); mm1 = sqrt(mm2)
  s2n = s(pol1,p2); s3n = s(pol1,p3); s4n = s(pol1,p4)
  s1m = s(pol2,p1); s3m = s(pol2,p3); s4m = s(pol2,p4)
  snm = s(pol1,pol2)
  e = sqrt(4*pi*alpha)
  c = 1/(ss*sW2*(1-sW2))

  pepezmmx = (c*e**4*me1*mm2*s2n*(-1 + 4*sW2))/mZ**2 + s3m*((c*e**4*s4n*(2*me2 + ss*(1 - &
             4*sW2)**2))/(4.*mZ**2) + (c*e**4*me1*(-1 + 4*sW2)*(me2 + mm2 - 2*ss - &
             tt))/(2.*mZ**2) + (c*e**4*s2n*(1 - 4*sW2)**2*(me2 + mm2 - ss - tt))/(4.*mZ**2)) + &
             s1m*(-(c*e**4*s2n*(2*mm2 - ss)*(1 - 4*sW2)**2)/(4.*mZ**2) - (c*e**4*me1*mm2*(-1 + &
             4*sW2))/mZ**2 - (c*e**4*s3n*(1 - 4*sW2)**2*(me2 + mm2 - tt))/(4.*mZ**2) + &
             (c*e**4*s4n*(1 - 4*sW2)**2*(me2 + mm2 - ss - tt))/(4.*mZ**2)) + &
             s4m*((c*e**4*s3n*(-2*me2 + ss*(1 - 4*sW2)**2))/(4.*mZ**2) - (c*e**4*s2n*(1 - &
             4*sW2)**2*(me2 + mm2 - tt))/(4.*mZ**2) - (c*e**4*me1*(-1 + 4*sW2)*(me2 + mm2 - ss - &
             tt))/(2.*mZ**2)) - (c*e**4*me1*s4n*(-1 + 4*sW2)*(me2 + mm2 - 2*ss - tt))/(2.*mZ**2) &
             - (c*e**4*me1*s3n*(-1 + 4*sW2)*(-me2 - mm2 + ss + tt))/(2.*mZ**2) - (c*e**4*(me2**2 &
             + 2*me2*mm2 + mm2**2 - me2*ss - mm2*ss + ss**2 - 8*me2**2*sW2 - 16*me2*mm2*sW2 - &
             8*mm2**2*sW2 - 4*ss**2*sW2 + 16*me2**2*sW2**2 + 32*me2*mm2*sW2**2 + 16*mm2**2*sW2**2 &
             + 8*ss**2*sW2**2 - 2*me2*tt - 2*mm2*tt + 2*ss*tt + 16*me2*sW2*tt + 16*mm2*sW2*tt - &
             8*ss*sW2*tt - 32*me2*sW2**2*tt - 32*mm2*sW2**2*tt + 16*ss*sW2**2*tt + tt**2 - &
             8*sW2*tt**2 + 16*sW2**2*tt**2))/mZ**2 + (c*e**4*snm*(1 - 4*sW2)**2*(me2**2 + mm2**2 &
             + 2*me2*(mm2 - tt) - 2*mm2*tt + tt*(ss + tt)))/(2.*mZ**2)

  pepezmmx = pepezmmx*0.5

  END FUNCTION

  FUNCTION EMZEMX(p1,p2,p3,p4)
    !! e-(p1) mu+(p2) -> Z -> e-(p3) mu+(p4)
    !! for massive electrons, expanded
  implicit none
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4)
  real (kind=prec) :: emzemx
  real (kind=prec) :: pol0(4)
#include "charge_config.h"

  pol0 = (/0._prec, 0._prec, 0._prec, 0._prec/)
  if(Qmu == +1) then
    emzemx = pepezmmx(p1,pol0,-p3,pol0,-p2,p4)
  elseif(Qmu == -1) then
    emzemx = pepezmmx(p1,pol0,-p3,pol0,p4,-p2)
  endif

  END FUNCTION


  FUNCTION PEPE2MMa(p1, pol1, p2, pol2, p3, p4)
    !! e-(p1) e+(p2) -> g* -> mu-(p3) mu+(p4)
    !! for massive electrons & positrons
  implicit none
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4), pol1(4), pol2(4)
  real(kind=prec) :: pepe2mma, Pi0

  Pi0 = real(deltalph_0_tot_cplx (sq(p1+p2)), kind=prec)
  pepe2mma = 2*Pi0*pepe2mm(p1,pol1,p2,pol2,p3,p4)
  END FUNCTION

  FUNCTION PEPEZMMAX(p1, pol1, p2, pol2, p3, p4)
    !! e-(p1) e+(p2) -> g* -> mu-(p3) mu+(p4)
    !! for massive electrons & positrons
  implicit none
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4), pol1(4), pol2(4)
  real(kind=prec) :: pepezmmax
  real(kind=prec) :: Me2, Mm2, ss, tt, s1mm, s2nm, s3mm, s3nm, snm
  real(kind=prec) :: Pi1, Pi2, Pi3, Pi1I, Pi2I
  real(kind=prec), parameter :: Pi3I = 0.
  real(kind=prec) :: asym123n1, asym123n2, asym12n1n2, asym23n1n2
  real(kind=prec) :: Me
  real(kind=prec) :: SW, CW
  complex(kind=prec) :: PiGG, Pi3G, Pi2C
  real(kind=prec) :: PiP(2:3)
  SW = sqrt(sw2)
  CW = sqrt(1-SW2)

  Me2 = sq(p1) ; me = sqrt(Me2)
  Mm2 = sq(p3)
  ss = sq(p1+p2)
  tt = sq(p1-p3)
  s2nm = s(p2, pol1) * me
  s1mm = s(p1, pol2) * me
  s3nm = s(p3, pol1) * me
  s3mm = s(p3, pol2) * me
  snm = s(pol1, pol2)

  asym123n1 = asymtensor(p1,p2,p3,pol1) * me
  asym123n2 = asymtensor(p1,p2,p3,pol2) * me
  asym12n1n2 = asymtensor(p1,p2,pol1,pol2)
  asym23n1n2 = asymtensor(p2,p3,pol1,pol2)

  PiGG = deltalph_0_tot_cplx(ss)
  Pi3G = deltalph2_0_tot_cplx(ss)
  PiP = deltalph2p_0_tot(ss)
  PiP(2) = real(-deltalph2_0_tot_cplx(MZ**2)+ SW**2*deltalph_0_tot_cplx(MZ**2), kind=prec) / SW/CW

  Pi1 = real(PiGG, kind=prec)
  Pi1I= aimag(PiGG)


  Pi2C = Pi3G / SW/CW - SW/CW * PiGG + PiP(2)
  Pi3  =                               PiP(3)

  Pi2 = real (Pi2C, kind=prec) * CW*SW
  Pi2I= aimag(Pi2C           ) * CW*SW

  pepezmmax = + 32*(asym123n1 + asym123n2)*me2*Pi1I
  pepezmmax = pepezmmax - 16*Pi1I*(-1 + 4*SW2)*(asym123n2*(s2nm - 2*s3nm) + asym23n1n2*(2*me2 &
              &+ 2*Mm2 - ss - 2*tt) + asym12n1n2*(me2 + 3*Mm2 - ss - tt))
  pepezmmax = pepezmmax + 16*Pi1*(ss**2 + me2**2*(1 - 4*SW2)**2 + Mm2**2*(1 - 4*SW2)**2 - 4*ss&
              &**2*SW2 + 8*ss**2*SW2**2 + 2*ss*tt - 8*ss*SW2*tt + 16*ss*SW2**2*tt + tt**2 - 8*S&
              &W2*tt**2 + 16*SW2**2*tt**2 + me2*(-ss + 2*Mm2*(1 - 4*SW2)**2 - 2*(1 - 4*SW2)**2*&
              &tt) - Mm2*(ss + 2*(1 - 4*SW2)**2*tt))
  pepezmmax = pepezmmax + 8*Pi1*(-1 + 4*SW2)*(Mm2*(3*s1mm - s2nm - 2*(s3mm + s3nm)) + me2*(s1m&
              &m + s2nm - 2*(s3mm + s3nm)) - s1mm*ss - 2*s2nm*ss + 3*s3mm*ss + 3*s3nm*ss - s1mm&
              &*tt - s2nm*tt + 2*s3mm*tt + 2*s3nm*tt)
  pepezmmax = pepezmmax - (8*Pi1*(me2**3*snm*(1 - 4*SW2)**2 + me2*Mm2**2*snm*(1 - 4*SW2)**2 - &
              &2*me2**2*snm*(1 - 4*SW2)**2*tt - Mm2*(1 - 4*SW2)**2*(-(s2nm*s3mm) + s1mm*(s2nm +&
              & s3nm) + 2*me2*snm*(-me2 + tt)) + me2*(2*s2nm*s3mm*(1 - 4*SW2 + 8*SW2**2) - 2*s1&
              &mm*s3nm*(1 - 4*SW2 + 8*SW2**2) + snm*(1 - 4*SW2)**2*tt*(ss + tt)) + (1 - 4*SW2)*&
              &*2*(s1mm*s3nm*(ss + tt) - s3mm*(s3nm*ss + s2nm*tt))))/me2

  pepezmmax = pepezmmax - 64*Pi2I*(asym123n2*(s2nm - 2*s3nm) + asym23n1n2*(2*me2 + 2*Mm2 - ss &
              &- 2*tt) + asym12n1n2*(me2 + 3*Mm2 - ss - tt))
  pepezmmax = pepezmmax - 64*Pi2*(-1 + 4*SW2)*(2*me2**2 + 2*Mm2**2 + ss**2 + 4*me2*(Mm2 - tt) &
              &- 4*Mm2*tt + 2*ss*tt + 2*tt**2)
  pepezmmax = pepezmmax -32*Pi2*(Mm2*(3*s1mm - s2nm - 2*(s3mm + s3nm)) + me2*(s1mm + s2nm - 2*&
              &(s3mm + s3nm)) - s1mm*ss - 2*s2nm*ss + 3*s3mm*ss + 3*s3nm*ss - s1mm*tt - s2nm*tt&
              & + 2*s3mm*tt + 2*s3nm*tt)
  pepezmmax = pepezmmax + (64*Pi2*(-1 + 4*SW2)*(Mm2*s2nm*s3mm - Mm2*s1mm*(s2nm + s3nm) + me2**&
              &3*snm + 2*me2**2*Mm2*snm + me2*Mm2**2*snm + s1mm*s3nm*ss - s3mm*s3nm*ss - s2nm*s&
              &3mm*tt + s1mm*s3nm*tt - 2*me2**2*snm*tt - 2*me2*Mm2*snm*tt + me2*(s2nm*s3mm - s1&
              &mm*s3nm + snm*tt*(ss + tt))))/me2

  pepezmmax = pepezmmax +32*(asym123n1 + asym123n2)*me2*Pi3I
  pepezmmax = pepezmmax -16*Pi3I*(-1 + 4*SW2)*(asym123n2*(s2nm - 2*s3nm) + asym23n1n2*(2*me2 +&
              & 2*Mm2 - ss - 2*tt) + asym12n1n2*(me2 + 3*Mm2 - ss - tt))
  pepezmmax = pepezmmax -16*Pi3*(ss**2 + me2**2*(1 - 4*SW2)**2 + Mm2**2*(1 - 4*SW2)**2 - 4*ss*&
              &*2*SW2 + 8*ss**2*SW2**2 + 2*ss*tt - 8*ss*SW2*tt + 16*ss*SW2**2*tt + tt**2 - 8*SW&
              &2*tt**2 + 16*SW2**2*tt**2 + me2*(-ss + 2*Mm2*(1 - 4*SW2)**2 - 2*(1 - 4*SW2)**2*t&
              &t) - Mm2*(ss + 2*(1 - 4*SW2)**2*tt))
  pepezmmax = pepezmmax - 8*Pi3*(-1 + 4*SW2)*(Mm2*(3*s1mm - s2nm - 2*(s3mm + s3nm)) + me2*(s1m&
              &m + s2nm - 2*(s3mm + s3nm)) - s1mm*ss - 2*s2nm*ss + 3*s3mm*ss + 3*s3nm*ss - s1mm&
              &*tt - s2nm*tt + 2*s3mm*tt + 2*s3nm*tt)
  pepezmmax = pepezmmax + (8*Pi3*(me2**3*snm*(1 - 4*SW2)**2 + me2*Mm2**2*snm*(1 - 4*SW2)**2 - &
              &2*me2**2*snm*(1 - 4*SW2)**2*tt - Mm2*(1 - 4*SW2)**2*(-(s2nm*s3mm) + s1mm*(s2nm +&
              & s3nm) + 2*me2*snm*(-me2 + tt)) + me2*(2*s2nm*s3mm*(1 - 4*SW2 + 8*SW2**2) - 2*s1&
              &mm*s3nm*(1 - 4*SW2 + 8*SW2**2) + snm*(1 - 4*SW2)**2*tt*(ss + tt)) + (1 - 4*SW2)*&
              &*2*(s1mm*s3nm*(ss + tt) - s3mm*(s3nm*ss + s2nm*tt))))/me2

  pepezmmax = -alpha**2*pi**2/2 * pepezmmax / Mz**2 / ss / CW**2 / SW**2

  END FUNCTION


  FUNCTION PEPEZMMLX(p1, pol1, p2, pol2, p3, p4)
    !! e-(p1) e+(p2) -> g* -> mu-(p3) mu+(p4)
    !! for massive electrons & positrons
  use mue_pepeZmml, only: coeffLL, coeffLR, coeffRR, wilsoncoeff, hardcoeff
  implicit none
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4), pol1(4), pol2(4)
  real(kind=prec) :: pepezmmlx
  real(kind=prec) :: Me2, Mm2, ss, tt, s1mm, s2nm, s3mm, s3nm, snm
  complex(kind=prec) :: discu, disct, ls
  real(kind=prec) :: discse, discsm
  real(kind=prec) :: c6se, c6sm, c6u, c6t, ce, cm, lmm, lmu
  real(kind=prec) :: me, mm, betae, betam
  logical pol
  real(kind=prec), dimension(0:1, -1:0) :: CL, CR
  real(kind=prec), dimension(-1:0) :: QQg, QQz
  real(kind=prec) :: CDe, CDm, CLe, CRe
  real(kind=prec), parameter :: zeta2 = pi**2/6.

  real(kind=prec) :: coeff_Cd1(0:0), coeff_Cd2(0:0), coeff_QQ2(0:1)
  real(kind=prec) :: coeff_CL_CL(0:1), coeff_CL_CR(0:1), coeff_CLe_CLe(0:0)
  real(kind=prec) :: coeff_CLe_CRe(0:0), coeff_CR_CR(0:1), coeff_CRe_CRe(0:0)
  real(kind=prec) :: coeff_CL0_CL0_QQ1(0:1), coeff_CL0_CR0_QQ1(0:1), coeff_CR0_CR0_QQ1(0:1)
  real(kind=prec) :: pole, dscheme
  real(kind=prec) :: mw2
  mW2 = mz**2 * (1-sw2)

  Me2 = sq(p1) ; me = sqrt(Me2)
  Mm2 = sq(p4) ; mm = sqrt(Mm2)

  call wilsoncoeff(Me, Mm, Cl, Cr, CDe, CDm, CLe, CRe, QQg, QQz)

  ss = sq(p1+p2)
  tt = sq(p1-p3)
  s2nm = s(p2, pol1) * me
  s1mm = s(p1, pol2) * me
  s3nm = s(p3, pol1) * me
  s3mm = s(p3, pol2) * me
  snm = s(pol1, pol2)

  pol = (sum(abs(pol1)) > 0 .or. sum(abs(pol2)) > 0)

  lmm = log(me2/mm2)
  lmu = -log(mm2/musq)
  ls = log(cmplx(me2/ss))

  disct = DiscB_cplx(tt, me, mm)
  discu = DiscB_cplx(2*me2 + 2*Mm2 - ss - tt, me, mm)
  discse = DiscB(ss, me)
  discsm = DiscB(ss, mm)

  c6t = ScalarC0IR6(tt, me, mm)
  c6u = ScalarC0IR6(2*me2 + 2*Mm2 - ss - tt, me, mm)
  c6se = ScalarC0IR6(ss, me)
  c6sm = ScalarC0IR6(ss, mm)

  betae = sqrt(1 - 4*me2/ss)
  betam = sqrt(1 - 4*mm2/ss)
  ce = (0.5*ss/(ss-4*me2) * discSe**2 + zeta2 + 2*Li2((betae-1)/(betae+1)))/(ss*betae)
  cm = (0.5*ss/(ss-4*mm2) * discSm**2 + zeta2 + 2*Li2((betam-1)/(betam+1)))/(ss*betam)

  pepeZmmlX = Cl(0,0)*Cl(0,0) * coeffLL(Me2, Mm2, ss, tt, s1mm, s2nm, s3mm, s3nm, snm, &
        real(discu,kind=prec), real(disct,kind=prec), discse, discsm, c6se, c6sm, c6u, c6t, ce, cm, lmm, lmu, real(ls, kind=prec), &
        real(disct*Ls,kind=prec), real(discu*Ls,kind=prec), pol) &
            + Cl(0,0)*Cr(0,0) * coeffLR(Me2, Mm2, ss, tt, s1mm, s2nm, s3mm, s3nm, snm, &
        real(discu,kind=prec), real(disct,kind=prec), discse, discsm, c6se, c6sm, c6u, c6t, ce, cm, lmm, lmu, real(ls, kind=prec), &
        real(disct*Ls,kind=prec), real(discu*Ls,kind=prec), pol) &
            + Cr(0,0)*Cr(0,0) * coeffRR(Me2, Mm2, ss, tt, s1mm, s2nm, s3mm, s3nm, snm, &
        real(discu,kind=prec), real(disct,kind=prec), discse, discsm, c6se, c6sm, c6u, c6t, ce, cm, lmm, lmu, real(ls, kind=prec), &
        real(disct*Ls,kind=prec), real(discu*Ls,kind=prec), pol)


  call hardcoeff(me, Me2, Mm2, ss, tt, s1mm, s2nm, s3mm, s3nm, snm, &
        coeff_Cd1, coeff_Cd2, coeff_QQ2, coeff_CL_CL, coeff_CL_CR, coeff_CLe_CLe, &
        coeff_CLe_CRe, coeff_CR_CR, coeff_CRe_CRe, coeff_CL0_CL0_QQ1, coeff_CL0_CR0_QQ1, coeff_CR0_CR0_QQ1)

  pepeZmmlX = 32*pi*pepeZmmlx - ( &
    + coeff_Cd1(0) * Cde &
    + coeff_Cd2(0) * Cdm &
    + coeff_CL_CL(0) * 2*CL(0,0) * CL(1,0) &
      + coeff_CL_CL(1) * 2*CL(0,0) * CL(1,-1) &
    + coeff_CL_CR(0) * (CL(1,0)*CR(0,0)+CL(0,0)*CR(1,0)) &
      + coeff_CL_CR(1) * (CL(1,-1)*CR(0,0)+CL(0,0)*CR(1,-1)) &
    + coeff_CR_CR(0) * 2*CR(0,0) * CR(1,0) &
      + coeff_CR_CR(1) * 2*CR(0,0) * CR(1,-1) &
    + coeff_CLe_CLe(0) * CLe*CLe &
    + coeff_CLe_CRe(0) * CRe*CLe &
    + coeff_CRe_CRe(0) * CRe*CRe &
    + coeff_QQ2(0) * QQz(0) + coeff_QQ2(1) * QQz(-1) &
    + coeff_CL0_CL0_QQ1(0) * CL(0,0)*CL(0,0) * QQg(0) &
      + coeff_CL0_CL0_QQ1(1) * CL(0,0)*CL(0,0) * QQg(-1) &
    + coeff_CL0_CR0_QQ1(0) * CL(0,0)*CR(0,0) * QQg(0) &
      + coeff_CL0_CR0_QQ1(1) * CL(0,0)*CR(0,0) * QQg(-1) &
    + coeff_CR0_CR0_QQ1(0) * CR(0,0)*CR(0,0) * QQg(0) &
      + coeff_CR0_CR0_QQ1(1) * CR(0,0)*CR(0,0) * QQg(-1) )

  pole = real( 1 + (discse*(-2*me2 + ss))/(2.*(-4*me2 + ss)) + (discsm*(-2*Mm2 + ss))/(2.*(-4*M&
       &m2 + ss)) - (disct*(me2 + Mm2 - tt)*tt)/(me2**2 + (Mm2 - tt)**2 - 2*me2*(Mm2 + t&
       &t)) - (discu*(-2*me2 - 2*Mm2 + ss + tt)*(-me2 - Mm2 + ss + tt))/(me2**2 + (-Mm2 &
       &+ ss + tt)**2 - 2*me2*(Mm2 + ss + tt)), kind=prec)

  dscheme = 32*pi/MW2/SW2 * pole * &
    (3*me2 + 3*Mm2 - 2*ss*(1 - 2*SW2 + 4*SW2**2) - 3*tt &
       -(s1mm + 2*s2nm - 3*s3nm - 3*s3mm)*(1 - 4*SW2) &
       + 0.25_prec * (-s1mm*s2nm / Me2 + ss*snm)*(1 - 4*SW2)**2 &
       + 3./2. * (s2nm*s3mm - s1mm*s3nm)/ss)

  pepeZmmlX = pepeZmmlX + dscheme
  pepeZmmlX = -alpha**3*pepeZmmlx / 2
  END FUNCTION

  FUNCTION EMZEMLX(p1,p2,p3,p4)
    !! e-(p1) mu+(p2) -> Z -> e-(p3) mu+(p4)
    !! for massive electrons, expanded
  implicit none
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4)
  real (kind=prec) :: emzemlx
  real (kind=prec) :: pol0(4)
#include "charge_config.h"

  pol0 = (/0._prec, 0._prec, 0._prec, 0._prec/)
  if(Qmu == +1) then
    emzemlx = pepezmmlx(p1,pol0,-p3,pol0,-p2,p4)
  elseif(Qmu == -1) then
    emzemlx = pepezmmlx(p1,pol0,-p3,pol0,p4,-p2)
  endif

  END FUNCTION


  FUNCTION PEPE2MMGL_NTS_EEEE(p1,pol1,p2,pol2,p3,p4,p5)
    !! e-(p1) e+(p2) -> g* -> mu-(p3) mu+(p4) g(p5)
    !! for massive electrons & positrons
  use mue_pepe2mmgl, only: leadingsoft, lbk
  implicit none
  real (kind=prec) :: p1(4), p2(4), p3(4), p4(4), p5(4), pol1(4), pol2(4)
  real (kind=prec) :: pepe2mmgl_nts_eeee
  real (kind=prec) :: Me2, Me1, Mm2, ss, tt
  real (kind=prec) :: s15,s25,s35, s1m,s3m, s5m, s2n,s3n,s5n,snm
  real (kind=prec) :: asym123n,asym123m,asym1235,asym125n,asym125m
  real (kind=prec) :: asym135n,asym135m,asym235n,asym235m
  real (kind=prec) :: Lm, disc, discIm, c0, c0Im, logsm
  real (kind=prec) :: mat0

  ss = sq(p1+p2); tt = sq(p1-p3); s15 = s(p1,p5); s35 = s(p3,p5); s25 = s(p2,p5)
  me2 = sq(p1); mm2 = sq(p3); me1 = sqrt(me2)

  s2n = s(pol1,p2); s3n = s(pol1,p3); s5n = s(pol1,p5)
  s1m = s(pol2,p1); s3m = s(pol2,p3); s5m = s(pol2,p5)
  snm = s(pol1,pol2)

  asym123n = Me1*asymtensor(p1,p2,p3,pol1)
  asym123m = Me1*asymtensor(p1,p2,p3,pol2)
  asym1235 = Me1*asymtensor(p1,p2,p3,p5)
  asym125n = Me1*asymtensor(p1,p2,p5,pol1)
  asym125m = Me1*asymtensor(p1,p2,p5,pol2)
  asym135n = Me1*asymtensor(p1,p3,p5,pol1)
  asym135m = Me1*asymtensor(p1,p3,p5,pol2)
  asym235n = Me1*asymtensor(p2,p3,p5,pol1)
  asym235m = Me1*asymtensor(p2,p3,p5,pol2)

  Lm = log(musq/Me2)

  disc = DiscB(ss,me1)
  discIm = pi*sqrt(1-4*me2/ss)

  logsm = log((ss-4*me2)/me2)
  c0 = ScalarC0IR6(ss,me1)
  c0Im = 1/(2*(-4*me2+ss))*(disc*(2*pi-ss*discIm/sqrt(ss*(-4*me2+ss)))+&
                   discIm*(-2*logsm-ss*disc/sqrt(ss*(-4*me2+ss))))


  pepe2mmgl_nts_eeee = alpha**4*128*pi**2 / (4*Me2-ss) &
          / ss**3 / s15**2 / s25**2 * (&
      leadingsoft(Lm, disc, discIm, c0, c0Im, &
           Me2, Mm2, ss, tt, s15,s25,s35, &
           s1m,s3m, s5m, s2n,s3n,s5n,snm, &
           asym123n,asym123m,asym1235,asym125n,asym125m,&
           asym135n,asym135m,asym235n,asym235m) + &
      lbk(Lm, disc, discIm, c0, c0Im, &
           Me2, Mm2, ss, tt, s15,s25,s35, &
           s1m,s3m, s5m, s2n,s3n,s5n,snm, &
           asym123n,asym123m,asym1235,asym125n,asym125m,&
           asym135n,asym135m,asym235n,asym235m))

   mat0 = 2*Me2**2 + 4*Me2*Mm2 + 2*Mm2**2 + Mm2*s1m*s2n - Me2*s2n*s3m - Mm2*s2n*s3m + Me2*&
       &s1m*s3n + Mm2*s1m*s3n - Me2**2*snm - 2*Me2*Mm2*snm - Mm2**2*snm - s1m*s3n*ss + s&
       &3m*s3n*ss + ss**2 - 4*Me2*tt - 4*Mm2*tt + s2n*s3m*tt - s1m*s3n*tt + 2*Me2*snm*tt&
       & + 2*Mm2*snm*tt + 2*ss*tt - snm*ss*tt + 2*tt**2 - snm*tt**2
mat0 = (32*alpha**2*mat0*Pi**2)/ss**2
  pepe2mmgl_nts_eeee = pepe2mmgl_nts_eeee + mat0 * &
    ntssoft( parts((/ part(p1,1,+1), part(p2,-1,+1)/)), p5)

  END FUNCTION PEPE2MMGL_NTS_EEEE

  FUNCTION PEPE2MMGL_EEEE(p1,pol1,p2,pol2,p3,p4,p5,pole)
    !! e-(p1) e+(p2) -> g* -> mu-(p3) mu+(p4) g(p5)
    !! for massive electrons & positrons
  use olinterface, only: openloops
  implicit none
  real (kind=prec) :: p1(4), p2(4), p3(4), p4(4), p5(4), pol1(4), pol2(4)
  real (kind=prec) :: pepe2mmgl_eeee
  real (kind=prec), optional :: pole
  real(kind=prec) :: gramdet, xi5
  real(kind=prec), dimension(4) :: n1, n2
  real(kind=prec) :: c1, c2
  real(kind=prec) :: same, diff
  real(kind=prec), parameter :: zero = 1e-6

  xi5 = 2.*p5(4)/sqrt(scms)

  if(xi5<ntsSwitch) then
    pepe2mmgl_eeee = pepe2mmgl_nts_eeee(p1,pol1,p2,pol2,p3,p4,p5)
    return
  endif

  n1 = boost_rf(p1, pol1) ; c1 = sum(n1(1:3))
  n2 = boost_rf(p2, pol2) ; c2 = sum(n2(1:3))

  if( (abs(c1) < zero).and.(abs(c2) < zero) ) then
    call openloops('ee2mmgEE', p1, p2, p3, p4, p5, fin=pepe2mmgl_eeee)
  else
    call openloops('ee2mmgEE', p1, p2, p3, p4, p5, fin=same, hel1=+1, hel2=+1)
    call openloops('ee2mmgEE', p1, p2, p3, p4, p5, fin=diff, hel1=+1, hel2=-1)

    pepe2mmgl_eeee = 2 * ((1+c1*c2) * same + (1-c1*c2) * diff)
  endif
  return

  gramdet = -sq(p1+p2) * s(p1,p5) * s(p2,p5) + sq(p1) * ( s(p1,p5) + s(p2,p5) )**2

  if(present(pole)) then
    if(abs(gramdet)<1._prec) then
      pepe2mmgl_eeee = pepe2mmgl_eeee_coll(p1,pol1,p2,pol2,p3,p4,p5,pole)
    else
      pepe2mmgl_eeee = pepe2mmgl_eeee_pvred(p1,pol1,p2,pol2,p3,p4,p5,pole)
    endif
  else
    if(abs(gramdet)<1._prec) then
      pepe2mmgl_eeee = pepe2mmgl_eeee_coll(p1,pol1,p2,pol2,p3,p4,p5)
    else
      pepe2mmgl_eeee = pepe2mmgl_eeee_pvred(p1,pol1,p2,pol2,p3,p4,p5)
    endif
  endif

  END FUNCTION PEPE2MMGL_EEEE

  FUNCTION PEPE2MMGf_EEEE(p1,n1,p2,n2,q1,q2,q3)
    !! e-(p1) e+(p2) -> g* -> mu-(q1) mu+(q2) g(q3)
    !! for massive electrons & positrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4),q3(4)
  real (kind=prec) :: n1(4), n2(4)
  real (kind=prec) :: pepe2mmgf_eeee
  real (kind=prec) :: mat0, ctfin
#if 0
  real(kind=prec) :: pole, ctpole
#endif

  pepe2mmgf_eeee=0.

  mat0 = pepe2mmg_ee(p1,n1,p2,n2,q1,q2, q3)

#if 0
  ctfin = alpha/(2.*pi)*Ieik(xieik1, sqrt(scms), parts(/part(p1, 1, 1), part(p2,-1, 1)/), ctpole)
  ctpole = alpha/(2*pi) * ctpole

  pepe2mmgf_eeee = pepe2mmgl_eeee(p1,n1,p2,n2,q1,q2,q3, pole)

  if (abs((ctpole*mat0+pole)/pole).gt.3e-4) print*,"pepe2mmgf is not finite",abs((ctpole*mat0+pole)/pole)
#else
  ctfin = alpha/(2.*pi)*Ieik(xieik1, sqrt(scms), parts((/part(p1, 1, 1), part(p2,-1, 1)/)))

  pepe2mmgf_eeee = pepe2mmgl_eeee(p1,n1,p2,n2,q1,q2,q3)
#endif

  pepe2mmgf_eeee = pepe2mmgf_eeee+ctfin*mat0

  END FUNCTION PEPE2MMGf_EEEE


  FUNCTION PEPE2MMll_EEEE(p1, n1, p2, n2, p3, p4)
    !! e-(p1) e+(p2) -> g* -> mu-(p3) mu+(p4) g(p5)
    !! for massive electrons & positrons
  use ff_heavyquark
  implicit none
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4), pepe2mmll_eeee
  real(kind=prec) :: n1(4), n2(4)
  real(kind=prec) :: x, ss, tt, me2, mm2, lm
  real (kind=prec) :: s2n, s3n, s4n, s1m, s3m, s4m, snm
  real(kind=prec) :: f11, f12, f22, f11coeff, f12coeff, f22coeff
  complex(kind=prec) :: F1a1(-1:1), F1a2(-2:0), F2a1(-1:1), F2a2(-2:0)

  s2n = s(pol1,p2); s3n = s(pol1,p3); s4n = s(pol1,p4)
  s1m = s(pol2,p1); s3m = s(pol2,p3); s4m = s(pol2,p4)
  snm = s(pol1,pol2)

  ss = sq(p1+p2); tt = sq(p1-p3)
  me2=sq(p1); mm2=sq(p3)

  x = (sqrt(ss) - sqrt(ss-4*me**2)) / (sqrt(ss) + sqrt(ss-4*me**2))

  lm = log(musq/me2)
  call hqff(-x, Lm, oneloopF1=F1a1, oneloopF2=F2a1, twoloopF1=F1a2, twoloopF2=F2a2)

  f11 = abs(F1a1(0))**2 + 2 * real(F1a2(0) + F1a1(1) * conjg(F1a1(-1)), prec)
  f22 = abs(F2a1(0))**2
  f12 = real(F1a1(-1)*conjg(F2a1(1)) + F2a2(0) + F1a1(0)*conjg(F2a1(0)), prec)

  f11coeff = -2*(- 4*ss*tt + 2*snm*ss*tt - 4*tt**2 - 2*ss**2 &
                 + s1m*s2n*ss - s2n*s3m*ss + s3n*s4m*ss - s1m*s4n*ss + s3m*s4n*ss &
                 - s2n*s3m*tt + s1m*s3n*tt + s2n*s4m*tt - s1m*s4n*tt + 2*snm*tt**2 &
                 + mm2*(s2n*s3m - s2n*s4m + s1m*(-2*s2n - s3n + s4n) + 8*tt - 4*snm*tt) &
                 + me2*(s2n*s3m - s1m*s3n - s2n*s4m + s1m*s4n + 4*mm2*(-2 + snm) + 8*tt - 4*snm*tt) &
                 + 2*mm2**2*(-2 + snm) + 2*me2**2*(-2 + snm))

  f22coeff = ss*(s2n*(s3m + s4m) + s1m*(s3n + s4n) - 2*(s3n*s4m + s3m*s4n + (-2 + snm)*tt)) &
             + 2*mm2*(s1m*s2n - (-2 + snm)*ss) + me2*(-(s1m*s2n) + (-2 + snm)*ss) &
             + ((s1m*s2n - (-2 + snm)*ss)*(mm2 - tt)*(-mm2 + ss + tt))/me2 &
             + 4*ss**2 + 2*s1m*s2n*tt

  f12coeff = -(s2n*(3*s3m + s4m) + s1m*(-2*s2n + s3n + 3*s4n) - 4*(s3n*s4m + s3m*s4n))*ss &
             - 8*ss**2 + 2*mm2*(s2n*(s3m - s4m) + s1m*(-4*s2n - s3n + s4n) + 4*(-2 + snm)*ss) &
             + 2*(s2n*(s3m - s4m) + s1m*(-s3n + s4n))*(me2 - tt)

  pepe2mmll_eeee = f11coeff * f11 + f22coeff * f22 + f12coeff * f12
  pepe2mmll_eeee = pepe2mmll_eeee * alpha**4 * 2 / ss**2
  END FUNCTION PEPE2MMLL_EEEE


  FUNCTION PEPE2MMff_EEEE(p1, n1, p2, n2, p3, p4)
    !! e-(p1) e+(p2) -> g* -> mu-(p3) mu+(p4)
    !! for massive electrons & positrons
  use ff_heavyquark
  implicit none
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4), pepe2mmff_eeee
  real(kind=prec) :: n1(4), n2(4)
  real(kind=prec) :: x, ss, tt, me2, mm2, lm
  real (kind=prec) :: s2n, s3n, s4n, s1m, s3m, s4m, snm
  real(kind=prec) :: f11, f12, f22, f11coeff, f12coeff, f22coeff
  complex(kind=prec) :: F1a1(-1:1), F1a2(-2:0), F2a1(-1:1), F2a2(-2:0)
  complex(kind=prec) :: F1a1e(-1:1), F1a2e, F2a1e(0:1), F2a2e
  real(kind=prec) :: ct(-1:0)

  s2n = s(pol1,p2); s3n = s(pol1,p3); s4n = s(pol1,p4)
  s1m = s(pol2,p1); s3m = s(pol2,p3); s4m = s(pol2,p4)
  snm = s(pol1,pol2)

  ct(0) = Ieik(xieik2, sqrt(scms), parts((/part(p1, 1, 1), part(p2,-1, 1)/)), ct(-1))

  ss = sq(p1+p2); tt = sq(p1-p3)
  me2=sq(p1); mm2=sq(p3)
  x = (sqrt(ss) - sqrt(ss-4*me**2)) / (sqrt(ss) + sqrt(ss-4*me**2))

  lm = log(musq/me2)
  call hqff(-x, Lm, oneloopF1=F1a1, oneloopF2=F2a1, twoloopF1=F1a2, twoloopF2=F2a2)

  F1a1e(-1) = ct(-1)/2 + F1a1(-1)
  F1a1e( 0) = ct( 0)/2 + F1a1( 0)
  F1a1e(+1) =          + F1a1(+1)

  F2a1e = F2a1(0:1)

  F1a2e = ct(0)**2 / 8 + ct(0)*F1a1(0) / 2 + ct(-1) * F1a1(1) / 2 + F1a2(0)
  F2a2e = ct(0) * F2a1(0) / 2 + ct(-1) * F2a1(1) / 2 + F2a2(0)


  f11 = abs(F1a1e(0))**2 + 2*real(-F1a1e(-1) * F1a1e(+1) + F1a2e, prec)
  f22 = abs(F2a1e(0))**2
  f12 = real(F1a1e(-1) * conjg(F2a1e(1)) + F1a1e(0) * conjg(F2a1e(0)) + F2a2e, prec)

  f11coeff = -2*(- 4*ss*tt + 2*snm*ss*tt - 4*tt**2 - 2*ss**2 &
                 + s1m*s2n*ss - s2n*s3m*ss + s3n*s4m*ss - s1m*s4n*ss + s3m*s4n*ss &
                 - s2n*s3m*tt + s1m*s3n*tt + s2n*s4m*tt - s1m*s4n*tt + 2*snm*tt**2 &
                 + mm2*(s2n*s3m - s2n*s4m + s1m*(-2*s2n - s3n + s4n) + 8*tt - 4*snm*tt) &
                 + me2*(s2n*s3m - s1m*s3n - s2n*s4m + s1m*s4n + 4*mm2*(-2 + snm) + 8*tt - 4*snm*tt) &
                 + 2*mm2**2*(-2 + snm) + 2*me2**2*(-2 + snm))

  f22coeff = ss*(s2n*(s3m + s4m) + s1m*(s3n + s4n) - 2*(s3n*s4m + s3m*s4n + (-2 + snm)*tt)) &
             + 2*mm2*(s1m*s2n - (-2 + snm)*ss) + me2*(-(s1m*s2n) + (-2 + snm)*ss) &
             + ((s1m*s2n - (-2 + snm)*ss)*(mm2 - tt)*(-mm2 + ss + tt))/me2 &
             + 4*ss**2 + 2*s1m*s2n*tt

  f12coeff = -(s2n*(3*s3m + s4m) + s1m*(-2*s2n + s3n + 3*s4n) - 4*(s3n*s4m + s3m*s4n))*ss &
             - 8*ss**2 + 2*mm2*(s2n*(s3m - s4m) + s1m*(-4*s2n - s3n + s4n) + 4*(-2 + snm)*ss) &
             + 2*(s2n*(s3m - s4m) + s1m*(-s3n + s4n))*(me2 - tt)

  pepe2mmff_eeee = f11coeff * f11 + f22coeff * f22 + f12coeff * f12
  pepe2mmff_eeee = pepe2mmff_eeee * alpha**4 * 2 / ss**2
  END FUNCTION PEPE2MMFF_EEEE




  FUNCTION PEPE2MM_AA(p1,pol1,p2, pol2, q1,q2)
    !! e-(p1) e+(p2) -> mu-(q1) mu+(q2)
    !! for massive electrons, numerically interpolated
  real (kind=prec), intent(in) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec), intent(in) :: pol1(4), pol2(4)
  real (kind=prec) :: pepe2mm_aa,ss
  complex(kind=prec) :: deltalph0_tot,deltalph1_tot

  ss = sq(p1+p2)
  ! bubble chain
  deltalph0_tot = deltalph_0_tot_cplx(ss)

  ! genuine 2-loop
  deltalph1_tot = deltalph_l_1_tot_cplx(ss)

  pepe2mm_aa = real(3*deltalph0_tot**2+2*deltalph1_tot, kind=prec)*pepe2mm(p1,pol1, p2,pol2, q1,q2)
  END FUNCTION PEPE2MM_AA


  !! class II
  FUNCTION PEPE2MM_ALEE(p1,pol1, p2,pol2, q1,q2)
    !! e-(p1) e+(p2) -> mu-(q1) mu+(q2)
    !! for massive electrons
  real (kind=prec), intent(in) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec), intent(in) :: pol1(4), pol2(4)
  real (kind=prec) :: pepe2mm_alee,ss
  complex(kind=prec) :: deltalph_tot

  ss = sq(p1+p2)
  deltalph_tot = deltalph_0_tot_cplx(ss)
  pepe2mm_alee = real(&
    2*deltalph_tot*pepe2mml_ee_cplx(p1,pol1, p2,pol2, q1,q2),&
    kind=prec)

  END FUNCTION PEPE2MM_ALEE


  !! class III
  FUNCTION PEPE2MMG_AEE(p1,pol1, p2,pol2, q1,q2,q3)
    !! e-(p1) e+(p2) -> mu-(q1) mu+(q2) g(q3)
    !! for massive electrons
  real (kind=prec), intent(in) :: p1(4),p2(4),q1(4),q2(4),q3(4)
  real (kind=prec), intent(in) :: pol1(4), pol2(4)
  real (kind=prec) :: pepe2mmg_aee,deltalph_tot,ss

  ss = sq(q1+q2)
  deltalph_tot = deltalph_0_tot(ss)
  pepe2mmg_aee = 2*deltalph_tot*pepe2mmg_ee(p1,pol1, p2,pol2, q1,q2,q3)
  END FUNCTION PEPE2MMG_AEE


  !! class IV
  FUNCTION PEPE2MM_NFEE(y, p1,pol1, p2,pol2, q1,q2)
    !! e-(p1) e+(p2) -> mu-(q1) mu+(q2)
    !! for massive electrons
  real (kind=prec), intent(in) :: p1(4),p2(4),q1(4),q2(4), y
  real (kind=prec), intent(in) :: pol1(4), pol2(4)
  real (kind=prec) :: pepe2mm_nfee
  real (kind=prec) :: ss, me2, tt, mm2
  real (kind=prec) :: s2n, s3n, s4n, s1m, s3m, s4m, snm
  real (kind=prec) :: F1, F2, f11coeff, f12coeff

  s2n = s(pol1,p2); s3n = s(pol1,q1); s4n = s(pol1,q2)
  s1m = s(pol2,p1); s3m = s(pol2,q1); s4m = s(pol2,q2)
  snm = s(pol1,pol2)

  ss = sq(p1+p2); tt = sq(p1-q1)
  me2=sq(p1); mm2=sq(q1)

  F1 = f1nf(y,ss,me2)
  F2 = f2nf(y,ss,me2)

  f11coeff = -2*(- 4*ss*tt + 2*snm*ss*tt - 4*tt**2 - 2*ss**2 &
                 + s1m*s2n*ss - s2n*s3m*ss + s3n*s4m*ss - s1m*s4n*ss + s3m*s4n*ss &
                 - s2n*s3m*tt + s1m*s3n*tt + s2n*s4m*tt - s1m*s4n*tt + 2*snm*tt**2 &
                 + mm2*(s2n*s3m - s2n*s4m + s1m*(-2*s2n - s3n + s4n) + 8*tt - 4*snm*tt) &
                 + me2*(s2n*s3m - s1m*s3n - s2n*s4m + s1m*s4n + 4*mm2*(-2 + snm) + 8*tt - 4*snm*tt) &
                 + 2*mm2**2*(-2 + snm) + 2*me2**2*(-2 + snm))
  f12coeff = -(s2n*(3*s3m + s4m) + s1m*(-2*s2n + s3n + 3*s4n) - 4*(s3n*s4m + s3m*s4n))*ss &
             - 8*ss**2 + 2*mm2*(s2n*(s3m - s4m) + s1m*(-4*s2n - s3n + s4n) + 4*(-2 + snm)*ss) &
             + 2*(s2n*(s3m - s4m) + s1m*(-s3n + s4n))*(me2 - tt)

  pepe2mm_nfee = 16*pi**2*alpha**2/ss**2 * (F1 * f11coeff - 0.5 * F2*f12coeff)

  END FUNCTION PEPE2MM_NFEE

                  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                  !!                             !!
                  !!            MUSE             !!
                  !!                             !!
                  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


  !!!!!!!!!!    Born    !!!!!!!!!!!!

  FUNCTION MP2MP(p1,p2,q1,q2)
    !! mu-(p1) p(p2) -> mu(q1) p(q2)
    !! for massive (and massless) muons
  real(kind=prec), intent(in) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: ss,tt,tau,mp2,mm2,Ge,Gm,F1p,F2p
  real (kind=prec) :: mp2mp, c_F1,c_F1F2,c_F2

  ss = sq(p1+p2); tt = sq(p1-q1)
  mm2=sq(p1); mp2=sq(p2)
  tau = -tt/4/mp2

  Ge = sachs_gel(-tt); Gm = sachs_gmag(-tt)
  F1p = (Ge+Gm*tau)/(1+tau); F2p = -(Ge-Gm)/(1+tau)

  c_F1 = (2*(2*(mm2 + mp2 - ss)**2 + 2*ss*tt + tt**2))/tt
  c_F1F2 = 4*(2*mm2 + tt)
  c_F2 = -((mm2**2 + mp2**2 - 2*mp2*(ss + tt) + ss*(ss + tt) - mm2*(2*mp2 + 2*ss + tt))/mp2)

  mp2mp = 16*pi**2*alpha**2/tt*(c_F1*F1p**2 + c_F1F2*F1p*F2p + c_F2*F2p**2)
  END FUNCTION MP2MP

  FUNCTION MP2MP_NUC(p1,p2,q1,q2)
    !! mu-(p1) p(p2) -> mu(q1) p(q2)
    !! for massive (and massless) muons
  real(kind=prec), intent(in) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: ss,tt,me2,mm2
  real (kind=prec) :: mp2mp_d10d20, mp2mp_d11d20, mp2mp_d12d20
  real (kind=prec) :: mp2mp_d10d21, mp2mp_d10d22, mp2mp_d11d21
  real (kind=prec) :: mp2mp_nuc
#include "charge_config.h"

  ss = sq(p1+p2); tt = sq(p1-q1)
  me2=sq(p1); mm2=sq(p2)

  mp2mp_d10d20 = (32*Pi**2*(2*me2**2 + 2*mm2**2 + 4*me2*(mm2 - ss) - 4*mm2*ss + 2*ss**2 + 2*ss*tt&
               & + tt**2))/tt**2
  mp2mp_d11d20 = (64*Pi**2*(2*me2**2 + 2*mm2**2 + 4*me2*(mm2 - ss) - 4*mm2*ss + 2*ss**2 + 2*ss*tt&
               & + tt**2)*(-1 + (lambda**2*(4*mm2 - kappa*tt))/((4*mm2 - tt)*(-lambda + tt)**2)) &
               &)/tt**2
  mp2mp_d12d20 = (32*Pi**2*(2*me2**2 + 2*mm2**2 + 4*me2*(mm2 - ss) - 4*mm2*ss + 2*ss**2 + 2*ss*tt&
               & + tt**2)*(-1 + (lambda**2*(4*mm2 - kappa*tt))/((4*mm2 - tt)*(-lambda + tt)**2)) &
               &**2)/tt**2
  mp2mp_d10d21 = (256*(-1 + kappa)*lambda**2*mm2*Pi**2*(2*me2 + tt))/((4*mm2 - tt)*tt*(-lambda + &
               &tt)**2)
  mp2mp_d10d22 = (-256*(-1 + kappa)**2*lambda**4*mm2*Pi**2*(me2**2 + mm2**2 - 2*mm2*(ss + tt) + s&
               &s*(ss + tt) - me2*(2*mm2 + 2*ss + tt)))/(tt*(-lambda + tt)**4*(-4*mm2 + tt)**2)
  mp2mp_d11d21 = (-256*(-1 + kappa)*lambda**2*mm2*Pi**2*(2*me2 + tt)*((-1 + kappa)*lambda**2 + 2*&
               &lambda*tt - tt**2 + 4*mm2*(-2*lambda + tt)))/((-lambda + tt)**4*(-4*mm2 + tt)**2 &
               &)

  mp2mp_nuc = alpha**2 * (  mp2mp_d10d20 &
                          + mp2mp_d11d20 &
                          + mp2mp_d12d20 &
                          + mp2mp_d10d21 &
                          + mp2mp_d10d22 &
                          + mp2mp_d11d21  ) * Qmu**2 * Qp**2
  END FUNCTION MP2MP_NUC

  !!!!!!!!!!    VP    !!!!!!!!!!!!


  FUNCTION MP2MP_A(p1,p2,q1,q2)
    !! mu-(p1) p(p2) -> mu(q1) p(q2)
    !! for massive muons
  real (kind=prec), intent(in) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: mp2mp_a,tt,deltalph_tot

  tt = sq(p1-q1)
  deltalph_tot = deltalph_0_tot(tt)
  mp2mp_A = 2*deltalph_tot*mp2mp(p1,p2,q1,q2)
  END FUNCTION MP2MP_A


  !!!!!!!!!!    Vertex    !!!!!!!!!!!!

  FUNCTION MP2MPl(p1,p2,q1,q2,pole)
    !! mu-(p1) p(p2) -> mu(q1) p(q2)
    !! for massive muons
  real (kind=prec), intent(in) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: ss,tt,tau,mm2,mp2,mu2,logmm
  real (kind=prec) :: discbtmm,scalarc0ir6tmm,F1p,F2p,Ge,Gm
  real (kind=prec) :: mp2mpl,c_F1,c_F1F2,c_F2
  real (kind=prec), optional :: pole

  ss = sq(p1+p2); tt = sq(p1-q1)
  mm2=sq(p1); mp2=sq(p2)
  tau = -tt/4/mp2
  mu2 = musq
  logmm = log(mu2/mm2)

  Ge = sachs_gel(-tt); Gm = sachs_gmag(-tt)
  F1p = (Ge+Gm*tau)/(1+tau); F2p = -(Ge-Gm)/(1+tau)
  discbtmm = discb(tt,sqrt(mm2))
  scalarc0ir6tmm = scalarc0ir6(tt,sqrt(mm2))

  c_F1 = (2*(-2*(2*(mm2 + mp2 - ss)**2 + 2*ss*tt + tt**2)*(2 + logmm + scalarc0ir6tmm*(-2&
       &*mm2 + tt)) + (discbtmm*(-16*mm2*(mm2 + mp2 - ss)**2 + 2*(3*mm2**2 + 2*mm2*(mp2 &
       &- 7*ss) + 3*(mp2 - ss)**2)*tt + 6*(-2*mm2 + ss)*tt**2 + 3*tt**3 - 2*logmm*(2*mm2&
       & - tt)*(2*(mm2 + mp2 - ss)**2 + 2*ss*tt + tt**2)))/(4*mm2 - tt)))/tt

  c_F1F2 = (4*(2*(-2 - logmm + scalarc0ir6tmm*(2*mm2 - tt))*(4*mm2 - tt)*(2*mm2 + tt) + dis&
         &cbtmm*(-16*mm2**2 - 8*mm2*tt + 3*tt**2 + 2*logmm*(-4*mm2**2 + tt**2))))/(4*mm2 -&
         & tt)

  c_F2 = -((2*(-2 - logmm + scalarc0ir6tmm*(2*mm2 - tt))*(4*mm2 - tt)*(mm2**2 + mp2**2 - &
       &2*mp2*(ss + tt) + ss*(ss + tt) - mm2*(2*(mp2 + ss) + tt)) + discbtmm*(-8*mm2**3 &
       &+ mm2**2*(16*(mp2 + ss) + 11*tt) - 2*mm2*(4*(mp2 - ss)**2 - 9*mp2*tt + 7*ss*tt +&
       & tt**2) + 3*tt*(mp2**2 - 2*mp2*(ss + tt) + ss*(ss + tt)) - 2*logmm*(2*mm2 - tt)*&
       &(mm2**2 + mp2**2 - 2*mp2*(ss + tt) + ss*(ss + tt) - mm2*(2*(mp2 + ss) + tt))))/(&
       &mp2*(4*mm2 - tt)))

  mp2mpl = 8*pi*alpha**3/tt*(c_F1*F1p**2 + c_F1F2*F1p*F2p + c_F2*F2p**2)

  if (present(pole)) then
    c_F1 = (-4*(2*mm2**2 + 2*mp2**2 + 4*mm2*(mp2 - ss) - 4*mp2*ss + 2*ss**2 + 2*ss*tt + tt**2)&
        *(-4*mm2 + tt + discbtmm*(-2*mm2 + tt)))/(tt*(-4*mm2 + tt))

    c_F1F2 = (-8*(2*mm2 + tt)*(-4*mm2 + tt + discbtmm*(-2*mm2 + tt)))/(-4*mm2 + tt)

    c_F2 =  (2*(4*mm2 + discbtmm*(2*mm2 - tt) - tt)* &
        (mm2**2 + mp2**2 - 2*mp2*(ss + tt) + ss*(ss + tt) - mm2*(2*mp2 + 2*ss + tt)) &
        )/(mp2*(4*mm2 - tt))
    pole = 8*pi*alpha**3/tt*(c_F1*F1p**2 + c_F1F2*F1p*F2p + c_F2*F2p**2)
  endif
  END FUNCTION MP2MPl

  FUNCTION MP2MPl_MP(p1,p2,q1,q2,pole)
    !! mu-(p1) p(p2) -> mu-(q1) p(q2)
    !! for massive muons
  real (kind=prec), intent(in) :: p1(4),p2(4), q1(4),q2(4)
  real (kind=prec) :: mp2mpl_mp, sin
  real (kind=prec), optional :: pole

  mp2mpl_mp = mp2mpl_mp_nuc(p1,p2,q1,q2,sin)

  if (present(pole)) then
    pole = sin
  endif
  END FUNCTION MP2MPl_MP

  FUNCTION MP2MPl_MP_MONO(p1,p2,q1,q2,pole)
    !! mu-(p1) p(p2) -> mu-(q1) p(q2)
    !! for massive muons
  real (kind=prec), intent(in) :: p1(4),p2(4), q1(4),q2(4)
  real (kind=prec) :: mp2mpl_mp_mono, sin
  real (kind=prec), optional :: pole

  mp2mpl_mp_mono = mp2mpl_mp_nucm(p1,p2,q1,q2,sin)

  if (present(pole)) then
    pole = sin
  endif
  END FUNCTION MP2MPl_MP_MONO

  !!!!!!!!!!    Real    !!!!!!!!!!!!

  FUNCTION MP2MPG(p1,p2,q1,q2,q3)
    !! mu-(p1) p(p2) -> mu(q1) p(q2) g(q3)
    !! for massive (and massless) muons
  real (kind=prec), intent(in) :: p1(4),p2(4),q1(4),q2(4),q3(4)
  real (kind=prec) :: mm2,mp2,tau,tt,s12,s13,s15,s23,s25,s35,F1p,F2p,Ge,Gm
  real (kind=prec) :: mp2mpg,c_F1,c_F1F2,c_F2

  s12 = s(p1,p2); s13 = s(p1,q1); s15 = s(p1,q3)
  s23= s(p2,q1); s25 = s(p2,q3); s35 = s(q1,q3)
  mm2=sq(p1); mp2=sq(p2)
  tt = sq(p1-q1-q3); tau = -tt/4/mp2
  Ge = sachs_gel(-tt); Gm = sachs_gmag(-tt)
  F1p = (Ge+Gm*tau)/(1+tau); F2p = -(Ge-Gm)/(1+tau)

  c_F1 = -8*(2*mm2*s15**2*(s12*s13 + s12*s15 - 2*mp2*(s13 + s15) + 2*s12*s23 - s13*s23 - &
       &s15*s23 + 2*s12*s25 - s13*s25 - s15*s25 + 4*mm2*(mp2 - s12 + s23 + s25)) + s15*(&
       &4*mp2*s13**2 - 2*s12*s13**2 + 4*mp2*s13*s15 - 2*s12*s13*s15 + 2*mp2*s15**2 - s12&
       &*s15**2 - 4*s12*s13*s23 + 2*s13**2*s23 - 2*s12*s15*s23 + 2*s13*s15*s23 + s15**2*&
       &s23 - 2*s15*s23**2 + (2*s13**2 + s15**2 - 2*s12*(s13 + s15) + 2*s13*(s15 + s23))&
       &*s25 + 2*mm2*(4*s12*s13 - s12*s15 + 2*mp2*(-2*s13 + s15) - 4*s13*s23 + s15*s23 -&
       & 4*s13*s25 + s15*s25 + 2*s25**2))*s35 + 2*(4*mm2**2*(mp2 - s12 + s23 + s25) + s1&
       &5*(s12**2 - s13*(2*mp2 + s23) + s12*(s13 + s23) - (s13 + s23)*s25) - mm2*(2*mp2*&
       &(s13 + s15) + (s13 + s15)*s23 - s12*(s13 + s15 + 2*s23) + (s13 + s15 + 2*s23)*s2&
       &5))*s35**2 + (2*mm2 + s15)*(2*mp2 - s12 + s23 + s25)*s35**3)

  c_F1F2 = -8*(8*mm2**3*(s15**2 + s35**2) - s15*(s12 + s13 + s15 - s23 - s25 - s35)*s35*(2*&
         &s13**2 + s15**2 + 2*s13*(s15 - s35) + s35**2) - 4*mm2**2*(3*s12*(s15**2 + s35**2&
         &) + (2*s15 - 3*(s23 + s25) - 2*s35)*(s15**2 + s35**2) + 2*s13*(s15**2 + s15*s35 &
         &+ s35**2)) + 2*mm2*(s13**2*(s15**2 + 4*s15*s35 + s35**2) + (s15**2 + s35**2)*(s1&
         &5**2 - s15*(s23 + s25 + s35) + s35*(s23 + s25 + s35)) + s13*(2*s15**3 - s15**2*(&
         &s23 + s25 - 2*s35) - 2*s15*s35*(3*(s23 + s25) + s35) - s35**2*(s23 + s25 + 2*s35&
         &)) + s12*((s15 - s35)*(s15**2 + s35**2) + s13*(s15**2 + 6*s15*s35 + s35**2))))

  c_F2 = -((8*mm2**3*(4*mp2 - s12 + s23 + s25)*(s15**2 + s35**2) - 4*mm2**2*((s12 - s23 -&
       & s25)*(s12*(s15**2 + s35**2) - (2*s15 + s23 + s25 - 2*s35)*(s15**2 + s35**2) - 2&
       &*s13*(s15**2 + s15*s35 + s35**2)) + 8*mp2*(s12*(s15**2 + s35**2) + (s15 - s23 - &
       &s25 - s35)*(s15**2 + s35**2) + s13*(s15**2 + s15*s35 + s35**2))) + s15*s35*(4*s1&
       &2**3*s35 - 4*mp2*(s13 + s15 - s35)*(2*s13**2 + s15**2 + 2*s13*(s15 - s35) + s35*&
       &*2) - s12**2*(2*s13**2 + s15*(s15 + 4*(s23 + s25)) + 2*s13*(s15 + 4*s23 + 2*s25 &
       &- s35) + 4*s25*s35 + s35**2) - (s23 + s25)*(2*s13**3 + s15*(s15**2 - 4*s23**2 + &
       &s15*(s23 + s25)) + 2*s13**2*(2*s15 + s23 + s25 - 2*s35) - (s15**2 + 4*s23*s25)*s&
       &35 + (s15 + s23 + s25)*s35**2 - s35**3 + s13*(3*s15**2 + 4*s23*s25 + 2*s15*(s23 &
       &+ s25 - 2*s35) - 2*(s23 + s25)*s35 + 3*s35**2)) + s12*(2*s13**3 + s15*(s15**2 + &
       &2*s15*(s23 + s25) + 4*s25*(2*s23 + s25)) + 4*s13**2*(s15 + s23 + s25 - s35) - (s&
       &15**2 + 4*s23*(s23 + 2*s25))*s35 + (s15 + 2*(s23 + s25))*s35**2 - s35**3 + s13*(&
       &3*s15**2 + 4*s15*s23 + 8*s23**2 + 4*s15*s25 + 16*s23*s25 + 4*s25**2 - 4*(s15 + s&
       &23 + s25)*s35 + 3*s35**2))) + 2*mm2*(s12**2*(s15**2*(s13 + s15 + 4*(s23 + s25)) &
       &- s15*(-2*s13 + s15)*s35 + (s13 + s15 + 4*s23)*s35**2 - s35**3) + s12*(-(s15**2*&
       &(s13**2 + s15**2 + 2*s15*(s23 + s25) + 4*(s23 + s25)**2 + 2*s13*(s15 + s23 + s25&
       &))) + s15*(16*mp2*s13 - 4*s13**2 + s15**2 + 2*s15*s23 + 2*s15*s25 + 4*s25**2 - 2&
       &*s13*(s15 + 2*(s23 + s25)))*s35 - (s13**2 + 2*s13*(-s15 + s23 + s25) + 2*(s15**2&
       & + s15*(s23 + s25) + 2*s23*(s23 + 2*s25)))*s35**2 + (2*s13 + s15 + 2*(s23 + s25)&
       &)*s35**3 - s35**4) + (s23 + s25)*(s15**3*(s15 + s23 + s25) - s15*(s15**2 + 4*s25&
       &**2 + s15*(s23 + s25))*s35 + (2*s15**2 + 4*s23*s25 + s15*(s23 + s25))*s35**2 - (&
       &s15 + s23 + s25)*s35**3 + s35**4 + s13*(2*s15 + s23 + s25 - 2*s35)*(s15 + s35)**&
       &2 + s13**2*(s15**2 + 4*s15*s35 + s35**2)) + 4*mp2*((s15**2 + s35**2)*(s15**2 - s&
       &15*s35 + s35**2) + s13**2*(s15**2 + 4*s15*s35 + s35**2) + 2*s13*(s15**3 + s15**2&
       &*s35 - s35**3 - s15*s35*(2*(s23 + s25) + s35)))))/mp2)

  mp2mpg = 32*pi**3*alpha**3/(tt*s15*s35)**2*(c_F1*F1p**2 + c_F1F2*F1p*F2p + c_F2*F2p**2)
  END FUNCTION MP2MPG

  FUNCTION MP2MPG_MP(p1,p2,q1,q2,q3)
    !! mu-(p1) p(p2) -> mu-(q1) p(q2) y(q3)
    !! for massive muons
  real (kind=prec), intent(in) :: p1(4),p2(4), q1(4),q2(4),q3(4)
  real (kind=prec) :: mp2mpg_mp

  mp2mpg_mp = mp2mpg_mp_nuc(p1,p2,q1,q2,q3)
  END FUNCTION MP2MPG_MP






          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          !!!!                                    !!!!
          !!!!              NNLO                  !!!!
          !!!!                                    !!!!
          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  FUNCTION MP2MPll(p1, p2, p3, p4)
    !! mu-(p1) p(p2) -> mu(q1) p(q2)
    !! for massive muons
  use ff_heavyquark
  implicit none
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4), mp2mpll
  real(kind=prec) :: x, ss, tt, mm2, mp2
  real(kind=prec) :: f11, f12, f22, f11coeff, f12coeff, f22coeff
  complex(kind=prec) :: F1a1(-1:1), F1a2(-2:0), F2a1(-1:1), F2a2(-2:0)
  real(kind=prec) :: f1p, f2p, tau, Ge, Gm, lm

  ss = sq(p1+p2); tt = sq(p1-p3)
  mm2=sq(p1); mp2=sq(p2)
  x = (sqrt(-tt+4*mm2)-sqrt(-tt))/(sqrt(-tt+4*mm2)+sqrt(-tt))

  tau = -tt/4/mp2
  Ge = sachs_gel(-tt); Gm = sachs_gmag(-tt)
  F1p = (Ge+Gm*tau)/(1+tau); F2p = -(Ge-Gm)/(1+tau)


  lm = log(musq/mm2)
  call hqff(x, Lm, oneloopF1=F1a1, oneloopF2=F2a1, twoloopF1=F1a2, twoloopF2=F2a2)

  f11 = real(F1a1(0)**2 + 2*F1a1(+1)*F1a1(-1) + 2*F1a2(0), prec)
  f22 = real(F2a1(0)**2, prec)
  f12 = real(F1a1(0)*F2a1(0) + F1a1(-1)*F2a1(+1) + F2a2(0), prec)

  ! F1p contribution
  f11coeff = 4.*(mp2 - ss)**2*x**2 - 4.*mm2*x*(ss - 2.*mp2*x + ss*x**2) &
              + 2.*mm2**2*(1. - 4.*x + 8.*x**2 - 4.*x**3 + x**4)
  f22coeff = ((mp2 - ss)**2*x + mm2**2*(2. - 3.*x + 2*x**2) &
                + mm2*(mp2*(1. - 4.*x + x**2) - ss*(1. + x**2))) * (1.-x)**2
  f12coeff =  4. * mm2*(x-1.)**2*(mm2 * (x-1.)**2 - 2*mp2*x)

  mp2mpll = f1p**2 / (mm2**2 * (1-x)**4) * &
      (f11coeff * f11 + f22coeff * f22 + f12coeff * f12)

  ! F1p F2p contribution
  f11coeff = 4. * (1. - 4.*x + x**2)/(x-1.)**2
  f22coeff = -(1.-10.*x+x**2) / 2. / x
  f12coeff = 12
  mp2mpll = mp2mpll + f1p*f2p * (f11coeff * f11 + f22coeff * f22 + f12coeff * f12)

  ! F2p**2 contribution
  f11coeff = x**2 * ((mp2 - ss)**2*x + mm2**2*(1.-x+x**2) &
        + mm2*(2.*mp2*(1. - 3.*x + x**2)- ss*(1. + x**2) )) / (x-1.)**2
  f22coeff = 0.25*(mp2 - ss)**2*x**2 - 0.25*mm2*x*(ss - 6.*mp2*x + ss*x**2) &
        + mm2**2*(1. - 4.*x + 10.*x**2 - 4.*x**3 + x**4) / 16.
  f12coeff = -0.5 * mm2 * x * (mm2 * (x-1)**2 - 8. * mp2 * x)

  mp2mpll = mp2mpll + f2p**2 * (f11coeff * f11 + f22coeff * f22 + f12coeff * f12) / (mp2 * mm2 * x**2)

  mp2mpll = mp2mpll * alpha**4 * 4.

  END FUNCTION MP2MPLL


  FUNCTION MP2MPff(p1, p2, p3, p4)
    !! mu-(p1) p(p2) -> mu(p3) p(p4)
    !! for massive muons
  use ff_heavyquark
  implicit none
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4), mp2mpff
  real(kind=prec) :: x, ss, tt, mm2, mp2
  real(kind=prec) :: f1(0:2), f2(0:2), f11, f22, f12
  complex(kind=prec) :: F1a1(-1:1), F1a2(-2:0), F2a1(-1:1), F2a2(-2:0)
  real(kind=prec) :: f11coeff, f12coeff, f22coeff
  real(kind=prec) :: f1p, f2p, tau, Ge, Gm
  real(kind=prec) :: ctfin, ctpole, oldmu

  ss = sq(p1+p2); tt = sq(p1-p3)
  mm2=sq(p1); mp2=sq(p2)
  x = (sqrt(-tt+4*mm2)-sqrt(-tt))/(sqrt(-tt+4*mm2)+sqrt(-tt))

  tau = -tt/4/mp2
  Ge = sachs_gel(-tt); Gm = sachs_gmag(-tt)
  F1p = (Ge+Gm*tau)/(1+tau); F2p = -(Ge-Gm)/(1+tau)

  oldmu = musq
  musq = mm2
  ctfin = em2emeik_ee(p1, p2, p3, p4, xieik2, ctpole)
  musq = oldmu

  call hqff(x, 0., oneloopF1=F1a1, oneloopF2=F2a1, twoloopF1=F1a2, twoloopF2=F2a2)

  ! TODO this is rather ugly..

  f1(0) = 1.

  f1(1) = ctfin
  f1(1) = real(f1(1) + F1a1(0) / pi, prec)

  f1(2) = 0.5*ctfin**2
  f1(2) = real(f1(2) + (F1a1(0) * ctfin + F1a1(+1) * ctpole) / pi, prec)
  f1(2) = real(f1(2) + F1a2(0) / pi**2, prec)

  f2(0) = 0.

  f2(1) = real(F2a1(0) / pi, prec)

  f2(2) = 0.
  f2(2) = real(f2(2) + (F2a1(0) * ctfin + F2a1(+1) * ctpole) / pi, prec)
  f2(2) = real(f2(2) + F2a2(0) / pi**2, prec)

  f11 = f1(1)**2 + 2*f1(0)*f1(2)
  f22 = f2(1)**2 + 2*f2(0)*f2(2)
  f12 = f1(2)*f2(0) + f1(1)*f2(1) + f1(0)*f2(2)

  ! F1p contribution
  f11coeff = 4.*(mp2 - ss)**2*x**2 - 4.*mm2*x*(ss - 2.*mp2*x + ss*x**2) &
              + 2.*mm2**2*(1. - 4.*x + 8.*x**2 - 4.*x**3 + x**4)
  f22coeff = ((mp2 - ss)**2*x + mm2**2*(2. - 3.*x + 2*x**2) &
                + mm2*(mp2*(1. - 4.*x + x**2) - ss*(1. + x**2))) * (1.-x)**2
  f12coeff =  4. * mm2*(x-1.)**2*(mm2 * (x-1.)**2 - 2*mp2*x)

  mp2mpff = f1p**2 / (mm2**2 * (1-x)**4) * &
      (f11coeff * f11 + f22coeff * f22 + f12coeff * f12)

  ! F1p F2p contribution
  f11coeff = 4. * (1. - 4.*x + x**2)/(x-1.)**2
  f22coeff = -(1.-10.*x+x**2) / 2. / x
  f12coeff = 12
  mp2mpff = mp2mpff + f1p*f2p * (f11coeff * f11 + f22coeff * f22 + f12coeff * f12)

  ! F2p**2 contribution
  f11coeff = x**2 * ((mp2 - ss)**2*x + mm2**2*(1.-x+x**2) &
        + mm2*(2.*mp2*(1. - 3.*x + x**2)- ss*(1. + x**2) )) / (x-1.)**2
  f22coeff = 0.25*(mp2 - ss)**2*x**2 - 0.25*mm2*x*(ss - 6.*mp2*x + ss*x**2) &
        + mm2**2*(1. - 4.*x + 10.*x**2 - 4.*x**3 + x**4) / 16.
  f12coeff = -0.5 * mm2 * x * (mm2 * (x-1)**2 - 8. * mp2 * x)

  mp2mpff = mp2mpff + f2p**2 * (f11coeff * f11 + f22coeff * f22 + f12coeff * f12) / (mp2 * mm2 * x**2)

  mp2mpff = mp2mpff * alpha**4 * 4. * pi**2

  END FUNCTION MP2MPff

  !!! vacuum polarisation corrections !!!


  !! The categorisation in classes I-IV follows [1901.03106]


  !! class I (+genuine 2-loop!)
  FUNCTION MP2MP_AA(p1,p2,q1,q2)
    !! mu-(p1) p(p2) -> mu(q1) p(q2)
    !! for massive muons, numerically interpolated
  real (kind=prec), intent(in) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: mp2mp_aa,deltalph0_tot,deltalph1_tot,tt

  tt = sq(p1-q1)
  ! bubble chain
  deltalph0_tot = deltalph_0_tot(tt)
  ! genuine 2-loop
  deltalph1_tot = deltalph_l_1_tot(tt)
  mp2mp_aa = (3*deltalph0_tot**2+2*deltalph1_tot)*mp2mp(p1,p2,q1,q2)
  END FUNCTION MP2MP_AA


  !! class II
  FUNCTION MP2MP_AL(p1,p2,q1,q2)
    !! mu-(p1) p(p2) -> mu(q1) p(q2)
    !! for massive muons, numerically interpolated
  real (kind=prec), intent(in) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: mp2mp_al,deltalph_tot,tt

  tt = sq(p1-q1)
  deltalph_tot = deltalph_0_tot(tt)
  mp2mp_al = 2*deltalph_tot*mp2mpl(p1,p2,q1,q2)
  END FUNCTION MP2MP_AL


  !! class III
  FUNCTION MP2MPG_A(p1,p2,q1,q2,q3)
    !! mu-(p1) p(p2) -> mu(q1) p(q2) g(q3)
    !! for massive muons, numerically interpolated
  real (kind=prec), intent(in) :: p1(4),p2(4),q1(4),q2(4),q3(4)
  real (kind=prec) :: mp2mpg_a,deltalph_tot,tt

  tt = sq(p1-q1-q3)
  deltalph_tot = deltalph_0_tot(tt)
  mp2mpg_a = 2*deltalph_tot*mp2mpg(p1,p2,q1,q2,q3)
  END FUNCTION MP2MPG_A


  !! class IV
  FUNCTION MP2MP_NF(y,p1,p2,q1,q2)
    !! mu-(p1) p(p2) -> mu(q1) p(q2)
    !! for massive muons
  real (kind=prec), intent(in) :: p1(4),p2(4),q1(4),q2(4), y
  real (kind=prec) :: mp2mp_nf
  real (kind=prec) :: ss, x, mm2, tt, mp2
  real (kind=prec) :: Ge, Gm, F1p, F2p, tau
  real (kind=prec) :: F1, F2, c_F1, c_F2, c_F1F2

  ss = sq(p1+p2); tt = sq(p1-q1)
  mm2=sq(p1); mp2=sq(p2)
  tau = -tt/4/mp2

  Ge = sachs_gel(-tt); Gm = sachs_gmag(-tt)
  F1p = (Ge+Gm*tau)/(1+tau); F2p = -(Ge-Gm)/(1+tau)

  x = (sqrt(-tt+4*mm2)-sqrt(-tt))/(sqrt(-tt+4*mm2)+sqrt(-tt))

  F1 = f1nf(y,tt,mm2)
  F2 = f2nf(y,tt,mm2)

  c_F1 = 64*mp2*x**2*(F2*mm2*(-1 + x)**2*(mm2*(-1 + x)**2 - 2*mp2*x)&
          + F1*(2*(mp2 - ss)**2*x**2 - 2*mm2*x*(ss - 2*mp2*x + ss*x**2)&
          + mm2**2*(1 - 4*x + 8*x**2 - 4*x**3 + x**4)))

  c_F1F2 = -64*mm2**2*mp2*(-1 + x)**2*x**2*(-3*F2*(-1 + x)**2&
           - 2*F1*(1 - 4*x + x**2))

  c_F2 = -8*mm2*(-1 + x)**2*x*(F2*mm2*(-1 + x)**2*(mm2*(-1 + x)**2 - 8*mp2*x)&
         - 4*F1*x*((mp2 - ss)**2*x + mm2**2*(1 - x + x**2) + mm2*(-(ss*(1 + x**2))&
         + 2*mp2*(1 - 3*x + x**2))))

  mp2mp_nf = alpha**2*pi**2/(mm2**2*mp2*(1-x)**4*x**2)&
                *(c_F1*F1p**2 + c_F1F2*F1p*F2p + c_F2*F2p**2)

  END FUNCTION MP2MP_NF



  !! class IV
  FUNCTION MP2MP_NF_INTERPOL(p1,p2,q1,q2)
    !! mu-(p1) p(p2) -> mu(q1) p(q2)
    !! for massive muons, numerically interpolated
  real (kind=prec), intent(in) :: p1(4),p2(4),q1(4),q2(4)
  real (kind=prec) :: mp2mp_nf_interpol, ss, x, mm2, tt, mp2
  real (kind=prec) :: Ge, Gm, tau, F1p, F2p, F1, F2, c_F1, c_F2, c_F1F2
  character(len=1) :: particle

  ss = sq(p1+p2); tt = sq(p1-q1)
  mm2=sq(p1); mp2=sq(p2)
  tau = -tt/4/mp2

  Ge = sachs_gel(-tt); Gm = sachs_gmag(-tt)
  F1p = (Ge+Gm*tau)/(1+tau); F2p = -(Ge-Gm)/(1+tau)

  x = (sqrt(-tt+4*mm2)-sqrt(-tt))/(sqrt(-tt+4*mm2)+sqrt(-tt))

  if(abs(sqrt(mm2)-mel)/mel<1.E-6) then
    particle = 'e'
  elseif(abs(sqrt(sq(p1))-mmu)/mmu<1.E-6) then
    particle = 'm'
  else
    call crash("mp2mp_nf evaluated for masses other than muon and electron")
  endif

  F1 = 0._prec; F2 = 0._prec
  if(nel==1) then
    select case(particle)
      case('e')
        F1 = F1 + f1el_electron(tt)
        F2 = F2 + f2el_electron(tt)
      case('m')
        F1 = F1 + f1mu_electron(tt)
        F2 = F2 + f2mu_electron(tt)
    end select
  end if
  if(nmu==1) then
    select case(particle)
      case('e')
        F1 = F1 + f1el_muon(tt)
        F2 = F2 + f2el_muon(tt)
      case('m')
        F1 = F1 + f1mu_muon(tt)
        F2 = F2 + f2mu_muon(tt)
    end select
  end if
  if(ntau==1) then
    select case(particle)
      case('e')
        F1 = F1 + f1el_tau(tt)
        F2 = F2 + f2el_tau(tt)
      case('m')
        F1 = F1 + f1mu_tau(tt)
        F2 = F2 + f2mu_tau(tt)
    end select
  end if
  if(nhad==1) then
    select case(particle)
      case('e')
        F1 = F1 + f1el_had(tt)
        F2 = F2 + f2el_had(tt)
      case('m')
        F1 = F1 + f1mu_had(tt)
        F2 = F2 + f2mu_had(tt)
    end select
  end if


  c_F1 = 64*mp2*x**2*(F2*mm2*(-1 + x)**2*(mm2*(-1 + x)**2 - 2*mp2*x)&
          + F1*(2*(mp2 - ss)**2*x**2 - 2*mm2*x*(ss - 2*mp2*x + ss*x**2)&
          + mm2**2*(1 - 4*x + 8*x**2 - 4*x**3 + x**4)))

  c_F1F2 = -64*mm2**2*mp2*(-1 + x)**2*x**2*(-3*F2*(-1 + x)**2&
           - 2*F1*(1 - 4*x + x**2))

  c_F2 = -8*mm2*(-1 + x)**2*x*(F2*mm2*(-1 + x)**2*(mm2*(-1 + x)**2 - 8*mp2*x)&
         - 4*F1*x*((mp2 - ss)**2*x + mm2**2*(1 - x + x**2) + mm2*(-(ss*(1 + x**2))&
         + 2*mp2*(1 - 3*x + x**2))))

  mp2mp_nf_interpol = alpha**2*pi**2/(mm2**2*mp2*(1-x)**4*x**2)&
                *(c_F1*F1p**2 + c_F1F2*F1p*F2p + c_F2*F2p**2)

  END FUNCTION MP2MP_NF_INTERPOL





  !!!  end vacuum polarisation corrections !!!


  ! This matrix element is no longer supported and is kept
  ! for legacy / testing purposes. Use at your own risk.
  ! mu-(p1) p(p2) -> mu(p3) p(p4) g(p5)
  ! for massive muons
  FUNCTION MP2MPGL_PV(p1, p2, p3, p4, p5, pole)
  real(kind=prec), optional :: pole
  real(kind=prec) :: mp2mpgl_pv, p1(4), p2(4), p3(4), p4(4), p5(4)
  real(kind=prec) :: s13, s15, s35, mm2, gramdet

  s13 = s(p1,p3); s15 = s(p1,p5);
  s35 = s(p3,p5); mm2=sq(p1)

  gramdet = (-(s13*s15*s35) + mm2*(s15**2 + s35**2))

  if(present(pole)) then
    if(abs(gramdet)<1._prec) then
      mp2mpgl_pv = mp2mpgl_coll(p1,p2,p3,p4,p5,pole)
    else
      mp2mpgl_pv = mp2mpgl_pvred(p1,p2,p3,p4,p5,pole)
    endif
  else
    if(abs(gramdet)<1._prec) then
      mp2mpgl_pv = mp2mpgl_coll(p1,p2,p3,p4,p5)
    else
      mp2mpgl_pv = mp2mpgl_pvred(p1,p2,p3,p4,p5)
    endif
  endif
  END FUNCTION MP2MPGL_PV

  ! This matrix element is no longer supported and is kept
  ! for legacy / testing purposes. Use at your own risk.
  FUNCTION MP2MPGF_PV(p1,p2,q1,q2,q3)
    !! mu-(p1) p(p2) -> mu(q1) p(q2) y(q3)
    !! for massive muons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4),q3(4)
  real (kind=prec) :: mp2mpgf_pv
  real (kind=prec) :: mat0, pole, ctpole, ctfin

  mat0 = mp2mpg(p1,p2,q1,q2,q3)

  ctfin = mp2mpeik(p1, p2, q1, q2, xieik1,ctpole)

  mp2mpgf_pv = mp2mpgl_pv(p1,p2,q1,q2,q3, pole)

  !if (abs((ctpole*mat0+pole)/pole).gt.3e-4) print*,"mp2mpgf is not finite",abs((ctpole*mat0+pole)/pole)
  mp2mpgf_pv = mp2mpgf_pv+ctfin*mat0 !no lin bit since FDH
  END FUNCTION MP2MPGF_PV

  ! This matrix element is no longer supported and is kept
  ! for legacy / testing purposes. Use at your own risk.
  FUNCTION MP2MPGL_CO(p1, p2, p3, p4, p5, pole)
    !! mu-(p1) p(p2) -> mu(p3) p(p4) y(p5)
    !! for massive muons
  real(kind=prec), optional :: pole
  real(kind=prec) :: mp2mpgl_co, p1(4), p2(4), p3(4), p4(4), p5(4)
  real(kind=prec) :: s13, s15, s35, mm2, gramdet

  s13 = s(p1,p3); s15 = s(p1,p5);
  s35 = s(p3,p5); mm2=sq(p1)

  gramdet = (-(s13*s15*s35) + mm2*(s15**2 + s35**2))

  if(present(pole)) then
     mp2mpgl_co = mp2mpgl_coll(p1,p2,p3,p4,p5,pole)
  else
     mp2mpgl_co = mp2mpgl_coll(p1,p2,p3,p4,p5)
  endif
  END FUNCTION MP2MPGL_CO

  ! This matrix element is no longer supported and is kept
  ! for legacy / testing purposes. Use at your own risk.
  FUNCTION MP2MPGF_CO(p1,p2,q1,q2,q3)
    !! mu-(p1) p(p2) -> mu(q1) p(q2) y(q3)
    !! for massive muons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4),q3(4)
  real (kind=prec) :: mp2mpgf_co
  real (kind=prec) :: mat0, pole, ctpole, ctfin

  mat0 = mp2mpg(p1,p2,q1,q2,q3)

  ctfin = mp2mpeik(p1, p2, q1, q2, xieik1,ctpole)

  mp2mpgf_co = mp2mpgl_co(p1,p2,q1,q2,q3, pole)

  !if (abs((ctpole*mat0+pole)/pole).gt.3e-4) print*,"mp2mpgf is not finite",abs((ctpole*mat0+pole)/pole)
  mp2mpgf_co = mp2mpgf_co+ctfin*mat0 !no lin bit since FDH
  END FUNCTION MP2MPGF_CO

  FUNCTION MP2MPeik(p1,p2,p3,p4, xicut, pole, lin)
  real (kind=prec) :: p1(4), p2(4), p3(4), p4(4)
  real (kind=prec) :: mp2mpeik, xicut
  real (kind=prec), optional :: pole, lin
  mp2mpeik = em2emeik_ee(p1, p2, p3, p4, xicut, pole, lin)
  END FUNCTION MP2MPeik

  FUNCTION MP2MPGL_NTS(p1,p2,q1,q2,q3,pole)
    !! mu-(p1) p(p2) -> mu(q1) p(q2) y(q3)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4),q3(4)
  real (kind=prec) :: mat0, nts_hard, nts_soft, hard_sin, soft_sin
  real (kind=prec), optional, intent(out) :: pole
  real (kind=prec) :: mp2mpgl_nts
#include "charge_config.h"

  mat0     = mp2mp(p1,p2,q1,q2)
  nts_hard = mp2mpgl_lbk(p1,p2,q1,q2,q3,hard_sin)
  nts_soft = ntssoft( parts((/ part(p1, -Qmu, +1), part(q1, -Qmu, -1) /)), &
                      q3, soft_sin ) * mat0

  mp2mpgl_nts = nts_hard + nts_soft
  if(present(pole)) pole = hard_sin + soft_sin
  END FUNCTION MP2MPGL_NTS

  FUNCTION MP2MPGL(p1, p2, p3, p4, p5, pole)
    !! mu-(p1) p(p2) -> mu(p3) p(p4) y(p5)
    !! for massive muons
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4),p5(4)
  real (kind=prec) :: xi5
  real (kind=prec) :: mp2mpgl
  real (kind=prec), optional, intent(out) :: pole

  xi5 = 2.*p5(4)/sqrt(scms)

  if(xi5>ntsSwitch) then
        mp2mpgl = mp2mpgl_co(p1,p2,p3,p4,p5,pole)
  elseif(xi5<ntsSwitch) then
        mp2mpgl = mp2mpgl_nts(p1,p2,p3,p4,p5,pole)
  endif
  END FUNCTION MP2MPGL

  FUNCTION MP2MPGf(p1,p2,q1,q2,q3)
    !! mu-(p1) p(p2) -> mu(q1) p(q2) g(q3)
    !! for massive muons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4),q3(4)
  real (kind=prec) :: mp2mpgf
  real (kind=prec) :: mat0, pole, ctpole, ctfin

  mat0 = mp2mpg(p1,p2,q1,q2,q3)

  ctfin = mp2mpeik(p1, p2, q1, q2, xieik1,ctpole)

  mp2mpgf = mp2mpgl(p1,p2,q1,q2,q3, pole)

  !if (abs((ctpole*mat0+pole)/pole).gt.3e-4) print*,"mp2mpgf is not finite",abs((ctpole*mat0+pole)/pole)
  mp2mpgf = mp2mpgf+ctfin*mat0 !no lin bit since FDH
  END FUNCTION MP2MPGF

  FUNCTION MP2MPGG(p1, p2, p3, p4, p5, p6)
    !! mu-(p1) p(p2) -> mu(p3) p(p4) g(p5) g(p6)
    !! for massive (and massless) electrons & muons
  use mue_mp2mpgg, only: hard=>mp2mpgg
  use mue_mp2mpgg_nts, only: mp2mpgg_nts, mp2mpgg_ntss
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4),p5(4),p6(4)
  real (kind=prec) :: mp2mpgg

  mp2mpgg = hard(p1, p2, p3, p4, p5, p6)
  END FUNCTION MP2MPGG


          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          !!!!                                    !!!!
          !!!!      FULL MUONE NNLO               !!!!
          !!!!                                    !!!!
          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


  FUNCTION EM2EMGG_EEEE(p1, p2, p3, p4, p5, p6)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4) g(p5) g(p6)
    !! for massive (and massless) electrons & muons
  use mue_mp2mpgg, only: hard=>em2emgg_eeee
  use mue_mp2mpgg_nts, only: em2emgg_eeee_nts, em2emgg_eeee_ntss
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4),p5(4),p6(4)
  real (kind=prec) :: em2emgg_eeee

  em2emgg_eeee = hard(p1, p2, p3, p4, p5, p6)
  END FUNCTION EM2EMGG_EEEE

  FUNCTION EM2EMGG_MIXD(p1, p2, p3, p4, p5, p6)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4) g(p5) g(p6)
    !! for massive (and massless) electrons & muons
  use mue_mp2mpgg, only: hard=>em2emgg_mixd
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4),p5(4),p6(4)
  real (kind=prec) :: em2emgg_mixd

  em2emgg_mixd = hard(p1, p2, p3, p4, p5, p6)
  END FUNCTION EM2EMGG_MIXD

  FUNCTION EM2EMGG_E3M1(p1, p2, p3, p4, p5, p6)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4) g(p5) g(p6)
    !! for massive (and massless) electrons & muons
  use mue_mp2mpgg, only: hard=>em2emgg_e3m1
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4),p5(4),p6(4)
  real (kind=prec) :: em2emgg_e3m1

  em2emgg_e3m1 = hard(p1, p2, p3, p4, p5, p6)
  END FUNCTION EM2EMGG_E3M1

  FUNCTION EM2EMGG_E2M2(p1, p2, p3, p4, p5, p6)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4) g(p5) g(p6)
    !! for massive (and massless) electrons & muons
  use mue_mp2mpgg, only: hard=>em2emgg_e2m2
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4),p5(4),p6(4)
  real (kind=prec) :: em2emgg_e2m2

  em2emgg_e2m2 = hard(p1, p2, p3, p4, p5, p6)
  END FUNCTION EM2EMGG_E2M2

  FUNCTION EM2EMGG_E1M3(p1, p2, p3, p4, p5, p6)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4) g(p5) g(p6)
    !! for massive (and massless) electrons & muons
  use mue_mp2mpgg, only: hard=>em2emgg_e1m3
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4),p5(4),p6(4)
  real (kind=prec) :: em2emgg_e1m3

  em2emgg_e1m3 = hard(p1, p2, p3, p4, p5, p6)
  END FUNCTION EM2EMGG_E1M3

  FUNCTION EM2EMGG_MIXD_OL(p1, p2, p3, p4, p5, p6)
    !! mu-(p1) p(p2) -> mu(p3) p(p4) g(p5) g(p6)
    !! for massive (and massless) muons and electrons
  use olinterface
  implicit none
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4),p5(4),p6(4)
  real (kind=prec) :: em2emggE, em2emggM, em2emgg, em2emgg_mixd_ol

  call openloops("em2emgg", p1, p2, p3, p4, p5, p6, tree=em2emgg, reload=.true.)
  em2emggE = em2emgg_eeee(p1, p2, p3, p4, p5, p6)
  em2emggM = em2emgg_mmmm(p1, p2, p3, p4, p5, p6)
  em2emgg_mixd_ol = em2emgg - em2emggE - em2emggM
  END FUNCTION EM2EMGG_MIXD_OL

  FUNCTION EM2EMGG_MMMM(p1, p2, p3, p4, p5, p6)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4) g(p5) g(p6)
    !! for massive (and massless) electrons & muons
  use mue_mp2mpgg, only: hard=>em2emgg_mmmm
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4),p5(4),p6(4)
  real (kind=prec) :: em2emgg_mmmm

  em2emgg_mmmm = hard(p1, p2, p3, p4, p5, p6)
  END FUNCTION EM2EMGG_MMMM

  FUNCTION EM2EMGG(p1, p2, p3, p4, p5, p6)
    !! mu-(p1) p(p2) -> mu(p3) p(p4) g(p5) g(p6)
    !! for massive (and massless) muons and electrons
  use olinterface
  implicit none
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4),p5(4),p6(4)
  real (kind=prec) :: em2emgg

  em2emgg = em2emgg_eeee(p1,p2,p3,p4,p5,p6) +  &
            em2emgg_mixd(p1,p2,p3,p4,p5,p6) +  &
            em2emgg_mmmm(p1,p2,p3,p4,p5,p6)
  !call openloops("em2emgg", p1, p2, p3, p4, p5, p6, tree=em2emgg)
  END FUNCTION EM2EMGG


  SUBROUTINE EMEM(ss, tt, m2, born, oneloop, twoloop)
  use mue_em2emll0
  real(kind=prec), intent(in) :: ss, tt, m2
  real(kind=prec), intent(out), optional :: born
  real(kind=prec), intent(out), optional :: oneloop(-2:2,3), twoloop(-4:0,5)
  real(kind=prec) :: ol(-2:2,3), tl(-4:0,5)

  real(kind=prec) :: Lm
  real(kind=prec), parameter :: z1(1:3) = (/ 0., 0., 0. /)
  real(kind=prec), parameter :: z2(1:5) = (/ 0., 0., 0., 0., 0. /)
  integer config
  complex(kind=prec) :: y, x12, x24, z12, z24
  complex(kind=prec) :: uu
  complex(kind=prec) :: Gp(94), Gboth(203), Gnp(2272)
  complex(kind=prec) :: logy, logz
  real(kind=prec) :: finite(5)

  Lm = log(musq/m2)
  uu = 2 * m2 - ss - tt

  if(real(ss) > 0) then
    config = 1
  endif
  if(real(tt) > 0) then
    config = 3
    ! TODO
    call crash("em2emll0")
  endif

  if(config == 1) then
    y = (2*m2 - sqrt(4*m2 - tt)*sqrt(-tt) - tt)/(2*m2)
  elseif (config==3) then
    y = (2*m2 + sqrt(4*m2 - tt)*sqrt(-tt) - tt)/(2*m2)
  endif
  logy = log(y)

  if(present(twoloop)) then
    finite = 0.

    ! Crossed NP
    x12 = - uu / m2
    x24 = - ss / m2
    z12 = sqrt(-1-x24) * sqrt(y) / sqrt(1+x12)
    z24 = sqrt(-1-x12) * sqrt(y) / sqrt(1+x24)
    if(config == 1) then
      z12 = z12 - imag * 1e-15
    elseif (config == 3) then
      z12 = z12 + imag * 1e-15
      z24 = z24 + imag * 1e-15
    endif

    logz = log(z12)

    call get_gpls(config + 1, z12, z24, x12, x24, y, Gboth=Gboth, Gnp=Gnp)

    finite(3) = 2*nonplanar(real(y), real(x12), real(z12), logy, logz, Gboth, Gnp)
  endif

  ! Uncrossed
  x12 = - ss / m2
  x24 = - uu / m2
  z12 = sqrt(-1-x24) * sqrt(y) / sqrt(1+x12)
  z24 =-sqrt(-1-x12) * sqrt(y) / sqrt(1+x24)
  if(config == 1) then
    z12 = z12 + imag * 1e-15
    z24 = z24 - imag * 1e-15
  elseif (config == 3) then
    z12 = z12 + imag * 1e-15
    z24 = z24 + imag * 1e-15
  endif

  if(present(born)) then
    born = real(&
      (1 - 2*(2-x12)*y + 2*(4+x12**2)*y**2 - 2*(2-x12)*y**3 + y**4)/(1-y)**4, kind=prec)
  endif

  logz = log(z12)

  if(present(twoloop)) then
    call get_gpls(config, z12, z24, x12, x24, y, Gp, Gboth, Gnp)
  elseif(present(oneloop)) then
    call get_gpls(config, z12, z24, x12, x24, y, Gp, Gboth)
  endif

  if(present(twoloop)) then
    finite = finite + planar(real(y), real(x12), logy, Gboth, Gp)
    finite(3) = finite(3) + 2*nonplanar(real(y), real(x12), real(z12), logy, logz, Gboth, Gnp)

    tl = reshape(poles(0.5*finite, real(y), real(x12), logy, Gboth, Gp), (/5,5/))
    tl(0,:) = tl(0,:) + 8*oneloopsq(real(y), real(x12), logy, Gboth, Gp)

    twoloop = tl
    twoloop(-3:0,:) = twoloop(-3:0,:) + tl(-4:-1,:) * (2*Lm)
    twoloop(-2:0,:) = twoloop(-2:0,:) + tl(-4:-2,:) * (2*Lm)**2 / 2.
    twoloop(-1:0,:) = twoloop(-1:0,:) + tl(-4:-3,:) * (2*Lm)**3 / 6.
    twoloop(-0:0,:) = twoloop(-0:0,:) + tl(-4:-4,:) * (2*Lm)**4 / 24.
    twoloop(-0:0,:) = twoloop(-0:0,:) + tl(-4:-4,:) * Lm / 2.

  endif

  if(present(oneloop)) then
    ol = 0._prec
    ol(-2:2, 1) = oneloopEE(real(y), real(x12), logy, Gboth, Gp)
    ol(-1:2, 2) = oneloopEM(real(y), real(x12), logy, Gboth, Gp)
    ol(-1:2, 3) = oneloopMM(real(y), real(x12), logy, Gboth, Gp)

    oneloop = ol
    oneloop(-1:2,:) = oneloop(-1:2,:) + ol(-2:+1,:) * Lm
    oneloop(-0:2,:) = oneloop(-0:2,:) + ol(-2:-0,:) * Lm**2 / 2.
    oneloop(+1:2,:) = oneloop(+1:2,:) + ol(-2:-1,:) * Lm**3 / 6.
    oneloop(+2:2,:) = oneloop(+2:2,:) + ol(-2:-2,:) * Lm**4 / 24.
  endif
  END SUBROUTINE EMEM

  SUBROUTINE EMEMZF(p1, p2, p3, p4, olF, tlF, olZ, tlZ)
  use massification
  real(kind=prec), intent(in) :: p1(4), p2(4), p3(4), p4(4)
  real(kind=prec), intent(out), optional :: olF(3), tlF(5)
  real(kind=prec), intent(out), optional :: olZ(-1:1,3), tlZ(-2:0,5)

  real(kind=prec) :: myolZ(-1:1,3), mytlZ(-2:0,5), ct(-1:0,0:2)
  real(kind=prec) :: born, ol(-2:2,3), tl(-4:0,5)
  real(kind=prec) :: Epart
#include "charge_config.h"

  call emem(sq(p1+p2), sq(p1-p3), sq(p2), &
    born=born, oneloop=ol, twoloop=tl)

  born = 32*pi**2*born
  ol = 64*pi**2*ol
  tl = 4*pi**2*tl

  myolZ = 0.
  mytlZ = 0.

  ! massify

  ! gives Qe^3 Qmu a and Qe^5 Qmu a^2
  call massify(log(sq(p1)/musq), 2., &
    born, ol(:,1), tl(:,1), &
    myolZ(:,1), mytlZ(:,1), checkpole=1e-7)

  ! gives Qe^2 Qmu^2 a and Qe^4 Qmu^2 a^2
  call massify(log(sq(p1)/musq), 2., &
    0., ol(:,2), tl(:,2), &
    myolZ(:,2), mytlZ(:,2), checkpole=1e-7)

  ! gives Qe Qmu^3 a and Qe^3 Qmu^3 a^2
  call massify(log(sq(p1)/musq), 2., &
    0., ol(:,3), tl(:,3), &
    myolZ(:,3), mytlZ(:,3), checkpole=1e-7)

  ! Qe^2 Qmu^4 a^2
  mytlZ(:,4) = tl(-2:,4)
  ! Qe^1 Qmu^5 a^2
  mytlZ(:,5) = tl(-2:,5)

  myolZ = myolZ / 2 / pi
  mytlZ = mytlZ / 4 / pi/pi

  if(present(olZ)) olZ = myolZ
  if(present(tlZ)) tlZ = mytlZ

#if 1
  Epart = sqrt(sq(p1+p2))
  born   = em2em(p1, p2, p3, p4)
  myolZ(0,1) = em2eml_ee(p1, p2, p3, p4, myolZ(-1,1))
  myolZ(0,2) = em2eml_em(p1, p2, p3, p4, myolZ(-1,2))
  myolZ(0,2) = myolZ(0,2) / Qe / Qmu ! em2eml_em already contains Q!
  myolZ(0,3) = em2eml_mm(p1, p2, p3, p4, myolZ(-1,3))

  ct( 0, 0) = Ireg(xieik2, Epart, p1) + Ireg(xieik2, Epart, p3) &
          - 2*Ireg(xieik2, Epart, p1, p3)
  ct( 0, 1) = 2*(Ireg(xieik2, Epart, p1, p2) - Ireg(xieik2, Epart, p1, p4) &
                -Ireg(xieik2, Epart, p2, p3) + Ireg(xieik2, Epart, p3, p4))
  ct( 0, 2) = Ireg(xieik2, Epart, p2) + Ireg(xieik2, Epart, p4) &
          - 2*Ireg(xieik2, Epart, p2, p4)
  ct(-1, 0) = Isin(xieik2, Epart, p1) + Isin(xieik2, Epart, p3) &
          - 2*Isin(xieik2, Epart, p1, p3)
  ct(-1, 1) = 2*(Isin(xieik2, Epart, p1, p2) - Isin(xieik2, Epart, p1, p4) &
                -Isin(xieik2, Epart, p2, p3) + Isin(xieik2, Epart, p3, p4))
  ct(-1, 2) = Isin(xieik2, Epart, p2) + Isin(xieik2, Epart, p4) &
          - 2*Isin(xieik2, Epart, p2, p4)

  ct =-alpha * ct / 2 / pi

#else
block
  real(kind=prec) :: y,x
  y = (2*Mmu**2 - sqrt(4*Mmu**2 - sq(p1-p3))*sqrt(-sq(p1-p3)) - sq(p1-p3))/(2*Mmu**2)
  x = -sq(p1+p2) / Mmu**2

  ct(-1,0) = 1 + log(Mel**2/Mmu**2) - 2*log(1-y) + log(y) ! Qe^2
  ct(-1,1) = - 2*log(1-x-y-1/y) + 2*log(-1-x)             ! Qe Qmu
  ct(-1,2) = 1 + (1+y**2)/(1-y**2) * log(y)               ! Qmu^2

  ct(0,:) = ct(-1,:) * log(musq)

  ct = ct/pi
endblock
#endif


#if 0
  ! electronic
  write(*,900) "i1", &
               abs(ct(-1,0)*born + myolZ(-1,1)) / myolZ(-1,1), &
               abs(mytlZ(-2,1) + ct(-1,0) * myolZ(-1,1) + born * ct(-1,0)**2/2) / mytlZ(-2,1), &
               abs(mytlZ(-1,1) + ct( 0,0) * myolZ(-1,1) + ct(-1,0) * myolZ(0,1) + born * ct(-1,0)*ct(0,0)) / mytlZ(-1,1)
  ! muonic
  write(*,900) "1i", &
               abs(ct(-1,2)*born + myolZ(-1,3)) / myolZ(-1,3), &
               abs(mytlZ(-2,5) + ct(-1,2) * myolZ(-1,3) + born * ct(-1,2)**2/2) / mytlZ(-2,5), &
               abs(mytlZ(-1,5) + ct( 0,2) * myolZ(-1,3) + ct(-1,2) * myolZ(0,3) + born * ct(-1,2)*ct(0,2)) / abs(mytlZ(-1,5))

  ! fully mixed
  write(*,900) "ii", &
               abs(ct(-1,1)*born + myolZ(-1,2)) / abs(myolZ(-1,2)), &
               abs(born*(ct(-1, 1)**2 + 2*ct(-1, 0)*ct(-1, 2)) &
                 + 2*ct(-1, 2)*myolZ(-1, 1) + 2*ct(-1, 1)*myolZ(-1, 2) + 2*ct(-1, 0)*myolZ(-1, 3) &
                 + 2*mytlZ(-2, 3))/abs(mytlZ(-2,3)), &
               abs(born*(ct(-1, 2)*ct(0, 0) + ct(-1, 1)*ct(0, 1) + ct(-1, 0)*ct(0, 2)) &
                 + ct(0, 2)*myolZ(-1,1) + ct(0, 1)*myolZ(-1,2) + ct(0, 0)*myolZ(-1,3) &
                 + ct(-1,2)*myolZ(0, 1) + ct(-1,1)*myolZ(0, 2) + ct(-1,0)*myolZ(0, 3) &
                 + mytlZ(-1, 3)) / abs(mytlZ(-1,3))

  write(*,901) "42", &
               abs(born*ct(-1, 0)*ct(-1, 1) + ct(-1, 1)*myolZ(-1, 1) + ct(-1, 0)*myolZ(-1, 2) + mytlZ(-2, 2)) / abs(mytlZ(-2, 2)), &
               abs(born*(ct(-1, 1)*ct(0, 0) + ct(-1, 0)*ct(0, 1)) &
                   + ct(0, 1)*myolZ(-1, 1) + ct(0, 0)*myolZ(-1, 2) &
                   + ct(-1, 1)*myolZ(0, 1) + ct(-1, 0)*myolZ(0, 2) + mytlZ(-1, 2)) / abs(mytlZ(-1, 2))
  write(*,901) "24", &
               abs(born*ct(-1, 1)*ct(-1, 2) + ct(-1, 2)*myolZ(-1, 2) + ct(-1, 1)*myolZ(-1, 3) + mytlZ(-2, 4)) / abs(mytlZ(-2, 4)), &
               abs(born*(ct(-1, 2)*ct(0, 1) + ct(-1, 1)*ct(0, 2)) &
                   + ct(0, 2)*myolZ(-1, 2) + ct(0, 1)*myolZ(-1, 3) &
                   + ct(-1, 2)*myolZ(0, 2) + ct(-1, 1)*myolZ(0, 3) + mytlZ(-1, 4)) / abs(mytlZ(-1, 4))

900 format("Q", A2, ": res. pole: ", "a^3/ep = ", ES7.1E2, ", a^4/ep^2 = ", ES7.1E2, ", a^4/ep = ", ES7.1E2)
901 format("Q", A2, ": res. pole:                   ", "a^4/ep^2 = ", ES7.1E2, ", a^4/ep = ", ES7.1E2)
#endif

  if(present(olF)) then
    olF = born * ct(0, :) + myolZ(0, :)
  endif

  if(present(tlF)) then
    tlF(1) = born*ct(0, 0)**2/2 &
              + ct( 0, 0)*myolZ(0, 1) &
              + ct(-1, 0)*myolZ(1, 1)
    tlF(2) = born*ct(0, 0)*ct(0, 1) &
              + ct( 0, 1)*myolZ(0, 1) + ct( 0, 0)*myolZ(0, 2) &
              + ct(-1, 1)*myolZ(1, 1) + ct(-1, 0)*myolZ(1, 2)
    tlF(3) = born*(ct(0, 1)**2/2 + ct(0, 0)*ct(0, 2)) &
              + ct( 0, 2)*myolZ(0, 1) + ct( 0, 1)*myolZ(0, 2) + ct( 0, 0)*myolZ(0, 3) &
              + ct(-1, 2)*myolZ(1, 1) + ct(-1, 1)*myolZ(1, 2) + ct(-1, 0)*myolZ(1, 3)
    tlF(4) = born*ct(0, 1)*ct(0, 2) &
              + ct( 0, 2)*myolZ(0, 2) + ct( 0, 1)*myolZ(0, 3) &
              + ct(-1, 2)*myolZ(1, 2) + ct(-1, 1)*myolZ(1, 3)
    tlF(5) = born*ct(0, 2)**2/2 &
              + ct( 0, 2)*myolZ(0, 3) &
              + ct(-1, 2)*myolZ(1, 3)
    tlF = tlF + mytlZ(0, :)
  endif

  END SUBROUTINE EMEMZF

  FUNCTION EM2EML0(p1, p2, p3, p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! for massless electrons
  use mue_em2emll0
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4)
  real(kind=prec) :: em2eml0
  real(kind=prec) :: ss, tt
  real(kind=prec) :: oneloop(-2:2,3)
  real(kind=prec) :: m2
#include "charge_config.h"

  m2 = sq(p2)
  ss = sq(p1+p2)
  tt = sq(p1-p3)

  call emem(ss, tt, m2, oneloop=oneloop)

  ! TODO fix factor of 16
  em2eml0 = sum((/Qe**2, Qmu*Qe, Qmu**2/) * oneloop(0, :)) * alpha**3 * 32*pi

  END FUNCTION EM2EML0

  FUNCTION EM2EMLL0(p1, p2, p3, p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! for massless electrons
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4)
  real(kind=prec) :: em2emll0
  real(kind=prec) :: ss, tt
  real(kind=prec) :: twoloop(-4:0,1:5)
  real(kind=prec) :: m2
#include "charge_config.h"

  m2 = sq(p2)
  ss = sq(p1+p2)
  tt = sq(p1-p3)
  call emem(ss, tt, m2, twoloop=twoloop)
  em2emll0 = real(sum((/Qe**4,Qe**3*Qmu**1,Qe**2*Qmu**2,Qe**1*Qmu**3,Qmu**4/) * twoloop(0, :)), kind=prec)

  END FUNCTION EM2EMLL0


  FUNCTION EM2EMGL_NTS(p1,p2,q1,q2,q3,pole)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(q3)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4),q3(4)
  real (kind=prec) :: mat0, nts_hard, nts_soft, hard_sin, soft_sin
  real (kind=prec), optional, intent(out) :: pole
  real (kind=prec) :: em2emgl_nts
#include "charge_config.h"

  mat0     = em2em(p1,p2,q1,q2)
  nts_hard = em2emgl_lbk(p1,p2,q1,q2,q3,hard_sin)
  nts_soft = ntssoft( parts((/ part(p1,Qe, +1), part(p2,Qmu, +1),     &
                               part(q1,Qe, -1), part(q2,Qmu, -1) /)), &
                      q3, soft_sin ) * mat0

  em2emgl_nts = nts_hard + nts_soft
  if(present(pole)) pole = hard_sin + soft_sin
  END FUNCTION EM2EMGL_NTS

  FUNCTION EM2EMGL_NTS_EEEE(p1,p2,q1,q2,q3,pole)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(q3)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4),q3(4)
  real (kind=prec) :: mat0, nts_hard, nts_soft, hard_sin, soft_sin
  real (kind=prec), optional, intent(out) :: pole
  real (kind=prec) :: em2emgl_nts_eeee

  mat0     = em2em(p1,p2,q1,q2)
  nts_hard = em2emgl_lbk_eeee(p1,p2,q1,q2,q3,hard_sin)
  nts_soft = ntssoft( parts((/ part(p1, 1, +1), part(q1, 1, -1) /)), &
                      q3, soft_sin ) * mat0

  em2emgl_nts_eeee = nts_hard + nts_soft
  if(present(pole)) pole = hard_sin + soft_sin
  END FUNCTION EM2EMGL_NTS_EEEE

  FUNCTION EM2EMGL_NTS_MIXD(p1,p2,q1,q2,q3,pole)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(q3)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4),q3(4)
  real (kind=prec) :: mat0, nts_hard, nts_soft, hard_sin, soft_sin
  real (kind=prec), optional, intent(out) :: pole
  real (kind=prec) :: em2emgl_nts_mixd
#include "charge_config.h"

  mat0     = em2em(p1,p2,q1,q2)
  nts_hard = em2emgl_lbk_mixd(p1,p2,q1,q2,q3,hard_sin)
  nts_soft = ntssoft( parts((/ part(p1,Qe, +1, 1), part(p2,Qmu, +1, 2),          &
                               part(q1,Qe, -1, 1), part(q2,Qmu, -1, 2) /), "x"), &
                      q3, soft_sin ) * mat0

  em2emgl_nts_mixd = nts_hard + nts_soft
  if(present(pole)) pole = hard_sin + soft_sin
  END FUNCTION EM2EMGL_NTS_MIXD

  ! !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! The following matrix elements are no longer supported and are kept
  ! for legacy / testing purposes. Use at your own risk.

  FUNCTION EM2EMGL_NTS_E3M1(p1,p2,q1,q2,q3,pole)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(q3)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4),q3(4)
  real (kind=prec) :: mat0, nts_hard, nts_soft, hard_sin, soft_sin
  real (kind=prec), optional, intent(out) :: pole
  real (kind=prec) :: em2emgl_nts_e3m1

  mat0     = em2em(p1,p2,q1,q2)
  nts_hard = em2emgl_lbk_e3m1(p1,p2,q1,q2,q3,hard_sin)
  nts_soft = ntssoftx( parts((/ part(p1, 1, +1, 1), part(p2, 1, +1, 2),          &
                               part(q1, 1, -1, 1), part(q2, 1, -1, 2) /), "1"), &
                      q3, soft_sin ) * mat0

  em2emgl_nts_e3m1 = nts_hard + nts_soft
  if(present(pole)) pole = hard_sin + soft_sin
  END FUNCTION EM2EMGL_NTS_E3M1

  FUNCTION EM2EMGL_NTS_E2M2(p1,p2,q1,q2,q3,pole)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(q3)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4),q3(4)
  real (kind=prec) :: mat0, nts_hard, nts_soft, hard_sin, soft_sin
  real (kind=prec), optional, intent(out) :: pole
  real (kind=prec) :: em2emgl_nts_e2m2

  mat0     = em2em(p1,p2,q1,q2)
  nts_hard = em2emgl_lbk_e2m2(p1,p2,q1,q2,q3,hard_sin)
  nts_soft = ntssoftx( parts((/ part(p1, 1, +1, 1), part(p2, 1, +1, 2),          &
                               part(q1, 1, -1, 1), part(q2, 1, -1, 2) /), "2"), &
                      q3, soft_sin ) * mat0

  em2emgl_nts_e2m2 = nts_hard + nts_soft
  if(present(pole)) pole = hard_sin + soft_sin
  END FUNCTION EM2EMGL_NTS_E2M2

  FUNCTION EM2EMGL_NTS_E1M3(p1,p2,q1,q2,q3,pole)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(q3)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4),q3(4)
  real (kind=prec) :: mat0, nts_hard, nts_soft, hard_sin, soft_sin
  real (kind=prec), optional, intent(out) :: pole
  real (kind=prec) :: em2emgl_nts_e1m3

  mat0     = em2em(p1,p2,q1,q2)
  nts_hard = em2emgl_lbk_e1m3(p1,p2,q1,q2,q3,hard_sin)
  nts_soft = ntssoftx( parts((/ part(p1, 1, +1, 1), part(p2, 1, +1, 2),          &
                               part(q1, 1, -1, 1), part(q2, 1, -1, 2) /), "3"), &
                      q3, soft_sin ) * mat0

  em2emgl_nts_e1m3 = nts_hard + nts_soft
  if(present(pole)) pole = hard_sin + soft_sin
  END FUNCTION EM2EMGL_NTS_E1M3

  FUNCTION EM2EMGL_S_MIXD(p1,p2,q1,q2,q3,pole)
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4),q3(4)
  real (kind=prec) :: s_hard, hard_sin
  real (kind=prec), optional, intent(out) :: pole
  real (kind=prec) :: em2emgl_s_mixd

  s_hard = em2emgl_ls_mixd(p1,p2,q1,q2,q3,hard_sin)
  em2emgl_s_mixd = s_hard
  if(present(pole)) pole = hard_sin
  END FUNCTION EM2EMGL_S_MIXD

  FUNCTION EM2EMGL_S_E3M1(p1,p2,q1,q2,q3,pole)
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4),q3(4)
  real (kind=prec) :: s_hard, hard_sin
  real (kind=prec), optional, intent(out) :: pole
  real (kind=prec) :: em2emgl_s_e3m1

  s_hard = em2emgl_ls_e3m1(p1,p2,q1,q2,q3,hard_sin)
  em2emgl_s_e3m1 = s_hard
  if(present(pole)) pole = hard_sin
  END FUNCTION EM2EMGL_S_E3M1

  FUNCTION EM2EMGL_S_E2M2(p1,p2,q1,q2,q3,pole)
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4),q3(4)
  real (kind=prec) :: s_hard, hard_sin
  real (kind=prec), optional, intent(out) :: pole
  real (kind=prec) :: em2emgl_s_e2m2

  s_hard = em2emgl_ls_e2m2(p1,p2,q1,q2,q3,hard_sin)
  em2emgl_s_e2m2 = s_hard
  if(present(pole)) pole = hard_sin
  END FUNCTION EM2EMGL_S_E2M2

  FUNCTION EM2EMGL_S_E1M3(p1,p2,q1,q2,q3,pole)
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4),q3(4)
  real (kind=prec) :: s_hard, hard_sin
  real (kind=prec), optional, intent(out) :: pole
  real (kind=prec) :: em2emgl_s_e1m3

  s_hard = em2emgl_ls_e1m3(p1,p2,q1,q2,q3,hard_sin)
  em2emgl_s_e1m3 = s_hard
  if(present(pole)) pole = hard_sin
  END FUNCTION EM2EMGL_S_E1M3

  ! !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


  FUNCTION EM2EMGL_NTS_MMMM(p1,p2,q1,q2,q3,pole)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(q3)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4),q3(4)
  real (kind=prec) :: mat0, nts_hard, nts_soft, hard_sin, soft_sin
  real (kind=prec), optional, intent(out) :: pole
  real (kind=prec) :: em2emgl_nts_mmmm

  mat0     = em2em(p1,p2,q1,q2)
  nts_hard = em2emgl_lbk_mmmm(p1,p2,q1,q2,q3,hard_sin)
  nts_soft = ntssoft( parts((/ part(p2, 1, +1), part(q2, 1, -1) /)), &
                      q3, soft_sin ) * mat0

  em2emgl_nts_mmmm = nts_hard + nts_soft
  if(present(pole)) pole = hard_sin + soft_sin
  END FUNCTION EM2EMGL_NTS_MMMM

  ! This matrix element is no longer supported and is kept
  ! for legacy / testing purposes. Use at your own risk.
  FUNCTION EM2EMGL_OL(p1, p2, p3, p4, p5, pole)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4) y(p5)
    !! for massive (and massless) electrons & muons
  use olinterface
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4),p5(4)
  real (kind=prec) :: em2emgl_ol
  real (kind=prec), optional, intent(out) :: pole

  call openloops("em2emg", p1, p2, p3, p4, p5, fin=em2emgl_ol, sin=pole)
  END FUNCTION EM2EMGL_OL

  FUNCTION EM2EMGL(p1, p2, p3, p4, p5, pole)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4) g(p5)
    !! for massive (and massless) electrons & muons
  use olinterface
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4),p5(4)
  real (kind=prec) :: xi5
  real (kind=prec) :: em2emgl
  real (kind=prec), optional, intent(out) :: pole

  xi5 = 2.*p5(4)/sqrt(scms)

  if(xi5>ntsSwitch) then
        call openloops("em2emg", p1, p2, p3, p4, p5, fin=em2emgl, sin=pole)
  elseif(xi5<ntsSwitch) then
        em2emgl = em2emgl_nts(p1,p2,p3,p4,p5,pole)
  endif
  END FUNCTION EM2EMGL

  FUNCTION EM2EMGL_EEEE(p1, p2, p3, p4, p5, pole)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4) g(p5)
    !! for massive (and massless) electrons & muons
  use olinterface
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4),p5(4)
  real (kind=prec) :: xi5
  real (kind=prec) :: em2emgl_eeee
  real (kind=prec), optional, intent(out) :: pole

  xi5 = 2.*p5(4)/sqrt(scms)

  if(xi5>ntsSwitch) then
        call openloops("em2emgEE", p1, p2, p3, p4, p5, fin=em2emgl_eeee, sin=pole)
  elseif(xi5<ntsSwitch) then
        em2emgl_eeee = em2emgl_nts_eeee(p1,p2,p3,p4,p5,pole)
  endif
  END FUNCTION EM2EMGL_EEEE

  ! This matrix element is no longer supported and is kept
  ! for legacy / testing purposes. Use at your own risk.
  FUNCTION EM2EMGL_EEEE_mfied(p1,p2,q1,q2,q3)
  use massification
  real (kind=prec)::p1(4),p2(4),q1(4),q2(4),q3(4)
  real (kind=prec)::born,ol(-2:2),olZ(-1:1)
  real (kind=prec)::em2emgl_eeee_mfied

  born  = em2emg_ee(p1,p2,q1,q2,q3)
  ol    = (/ 0.,0.,0.,0.,0. /)
  ol(0) = em2emgl_eeee_mless(p1,p2,q1,q2,q3)

  call massify(log(sq(p1)/musq), 2., real(born), 2*pi*real(ol), oneloopZ=olZ)

  olZ = olZ / 2 / pi
  em2emgl_eeee_mfied = olZ(0)
  END FUNCTION EM2EMGL_EEEE_mfied

  ! This matrix element is no longer supported and is kept
  ! for legacy / testing purposes. Use at your own risk.
  !! e-(p1) mu-(p2) -> e-(p3) mu-(p4) g(p5)
  !! for massive electrons
  FUNCTION EM2EMGL_EEEE_APPROX(p1,p2,p3,p4,p5)
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4),p5(4)
  real (kind=prec) :: em2emgl_eeee_approx
  real (kind=prec) :: xi,m2,s15,s35

  xi = 2.*p5(4)/sqrt(scms)
  m2 = sq(p1)
  s15 = s(p1,p5)
  s35 = s(p3,p5)

  if(xi<1e-2) then
    em2emgl_eeee_approx = em2emgl_nts_eeee(p1,p2,p3,p4,p5)
  elseif(s15<2*m2) then
    em2emgl_eeee_approx = em2emgl_ee_icoll(p1,p2,p3,p4,p5)
  elseif(s35<2*m2) then
    em2emgl_eeee_approx = em2emgl_ee_fcoll(p1,p2,p3,p4,p5)
  else
    em2emgl_eeee_approx = em2emgl_eeee_mfied(p1,p2,p3,p4,p5)
  endif
  END FUNCTION

  ! This matrix element is no longer supported and is kept
  ! for legacy / testing purposes. Use at your own risk.
  FUNCTION EM2EMGL_MIXD_OL(p1, p2, p3, p4, p5)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4) y(p5)
    !! for massive (and massless) electrons & muons
  use olinterface
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4),p5(4)
  real (kind=prec) :: em2emgl_mixd_ol

  call openloops("em2emgEM", p1, p2, p3, p4, p5, fin=em2emgl_mixd_ol)
  END FUNCTION EM2EMGL_MIXD_OL

  FUNCTION EM2EMGL_MIXD(p1, p2, p3, p4, p5)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4) g(p5)
    !! for massive (and massless) electrons & muons
  use olinterface
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4),p5(4)
  real (kind=prec) :: xi5
  real (kind=prec) :: em2emgl_mixd

  xi5 = 2.*p5(4)/sqrt(scms)

  if(xi5>ntsSwitch) then
        !temporary (bad) hack
        ! call openloops("em2emg",   p1, p2, p3, p4, p5, fin=em2emgl     , sin=pole_tot)
        ! call openloops("em2emgEE", p1, p2, p3, p4, p5, fin=em2emgl_eeee, sin=pole_eeee)
        ! call openloops("em2emgMM", p2, p1, p4, p3, p5, fin=em2emgl_mmmm, sin=pole_mmmm)
        ! em2emgl_mixd_new = em2emgl - em2emgl_eeee - em2emgl_mmmm
        ! if(present(pole)) pole = pole_tot - pole_eeee - pole_mmmm
        call openloops("em2emgEM", p1, p2, p3, p4, p5, fin=em2emgl_mixd)
  elseif(xi5<ntsSwitch) then
        em2emgl_mixd = em2emgl_nts_mixd(p1,p2,p3,p4,p5)
  endif
  END FUNCTION EM2EMGL_MIXD

  FUNCTION EM2EMGL_E3M1(p1, p2, p3, p4, p5)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4) g(p5)
    !! for massive (and massless) electrons & muons
  use olinterface
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4),p5(4)
  real (kind=prec) :: xi5
  real (kind=prec) :: em2emgl_e3m1

  xi5 = 2.*p5(4)/sqrt(scms)

  if(xi5>ntsSwitch) then
        call openloops("em2emgEM31", p1, p2, p3, p4, p5, fin=em2emgl_e3m1)
  elseif(xi5<ntsSwitch) then
        em2emgl_e3m1 = em2emgl_nts_e3m1(p1,p2,p3,p4,p5)
  endif
  END FUNCTION EM2EMGL_E3M1

  FUNCTION EM2EMGL_E2M2(p1, p2, p3, p4, p5)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4) g(p5)
    !! for massive (and massless) electrons & muons
  use olinterface
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4),p5(4)
  real (kind=prec) :: xi5
  real (kind=prec) :: em2emgl_e2m2

  xi5 = 2.*p5(4)/sqrt(scms)

  if(xi5>ntsSwitch) then
        call openloops("em2emgEM22", p1, p2, p3, p4, p5, fin=em2emgl_e2m2)
  elseif(xi5<ntsSwitch) then
        em2emgl_e2m2 = em2emgl_nts_e2m2(p1,p2,p3,p4,p5)
  endif
  END FUNCTION EM2EMGL_E2M2

  FUNCTION EM2EMGL_E1M3(p1, p2, p3, p4, p5)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4) g(p5)
    !! for massive (and massless) electrons & muons
  use olinterface
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4),p5(4)
  real (kind=prec) :: xi5
  real (kind=prec) :: em2emgl_e1m3

  xi5 = 2.*p5(4)/sqrt(scms)

  if(xi5>ntsSwitch) then
        call openloops("em2emgEM13", p1, p2, p3, p4, p5, fin=em2emgl_e1m3)
  elseif(xi5<ntsSwitch) then
        em2emgl_e1m3 = em2emgl_nts_e1m3(p1,p2,p3,p4,p5)
  endif
  END FUNCTION EM2EMGL_E1M3

  FUNCTION EM2EMGL_MMMM(p1, p2, p3, p4, p5)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4) g(p5)
    !! for massive (and massless) electrons & muons
  use olinterface
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4),p5(4)
  real (kind=prec) :: xi5
  real (kind=prec) :: em2emgl_mmmm

  xi5 = 2.*p5(4)/sqrt(scms)

  if(xi5>ntsSwitch) then
        call openloops("em2emgMM", p1, p2, p3, p4, p5, fin=em2emgl_mmmm)
  elseif(xi5<ntsSwitch) then
        em2emgl_mmmm = em2emgl_nts_mmmm(p1,p2,p3,p4,p5)
  endif
  END FUNCTION EM2EMGL_MMMM


          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          !!!!                                    !!!!
          !!!!         integrand function         !!!!
          !!!!                                    !!!!
          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  FUNCTION EM2EMf_EE(p1,p2,p3,p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4)
  real (kind=prec) :: em2emf_ee, mat0, Epart
  Epart = sqrt(scms)
  mat0 = em2em(p1,p2,p3,p4)
  em2emf_ee = em2eml_ee(p1,p2,p3,p4)
  em2emf_ee = em2emf_ee + alpha/(2.*pi)*mat0*Ieik(xieik1, Epart, parts((/part(p1, 1, 1), part(p3, 1, -1)/)))
  END FUNCTION EM2EMf_EE
  FUNCTION EM2EMf_EM(p1,p2,p3,p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4)
  real (kind=prec) :: em2emf_em, mat0, Epart
#include "charge_config.h"
  Epart = sqrt(scms)
  mat0 = em2em(p1,p2,p3,p4)
  em2emf_em = em2eml_em(p1,p2,p3,p4)
  em2emf_em = em2emf_em + alpha/(2.*pi)*mat0                                 &
              *Ieik(xieik1, Epart, parts((/part(p1,Qe,+1,1), part(p2,Qmu,+1,2), &
                                           part(p3,Qe,-1,1), part(p4,Qmu,-1,2)/), "x"))
  !em2emf_em = em2emf_em + alpha/(2.*pi)*mat0*2*(Ireg(xieik1,Epart,p1,p4)&
  !           + Ireg(xieik1,Epart,p2,p3) - Ireg(xieik1,Epart,p1,p2)&
  !            - Ireg(xieik1,Epart,p3,p4))
  END FUNCTION EM2EMf_EM
  FUNCTION EM2EMf_MM(p1,p2,p3,p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4)
  real (kind=prec) :: em2emf_mm, mat0, Epart
  Epart = sqrt(scms)
  mat0 = em2em(p1,p2,p3,p4)
  em2emf_mm = em2eml_mm(p1,p2,p3,p4)
  em2emf_mm = em2emf_mm + alpha/(2.*pi)*mat0*Ieik(xieik1, Epart, parts((/part(p2, 1, 1), part(p4, 1, -1)/)))
  END FUNCTION EM2EMf_MM

  FUNCTION EM2EMF(p1,p2,p3,p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4)
  real (kind=prec) :: em2emf
  em2emf = em2emf_ee(p1,p2,p3,p4) + em2emf_em(p1,p2,p3,p4) + em2emf_mm(p1,p2,p3,p4)
  END FUNCTION EM2EMF

  FUNCTION EM2EM_AFEE(p1,p2,p3,p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! for massive electrons
  real (kind=prec), intent(in) :: p1(4),p2(4),p3(4),p4(4)
  real (kind=prec) :: em2em_afee, mat0, Epart
  Epart = sqrt(scms)
  mat0 = em2em_a(p1,p2,p3,p4)
  em2em_afee = em2em_alee(p1,p2,p3,p4)
  em2em_afee = em2em_afee + alpha/(2.*pi)*mat0*Ieik(xieik1, Epart, parts((/part(p1, 1, 1), part(p3, 1, -1)/)))
  END FUNCTION EM2EM_AFEE


  FUNCTION EM2EM_AFMM(p1,p2,p3,p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! for massive electrons
  real (kind=prec), intent(in) :: p1(4),p2(4),p3(4),p4(4)
  real (kind=prec) :: em2em_afmm

  em2em_afmm = em2em_afee(p2,p1,p4,p3)
  END FUNCTION EM2EM_AFMM

  FUNCTION EM2EM_AFEM(p1,p2,p3,p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4)
  real (kind=prec) :: em2em_afem, mat0, Epart
#include "charge_config.h"
  Epart = sqrt(scms)
  mat0 = em2em_a(p1,p2,p3,p4)
  em2em_afem = em2em_alem(p1,p2,p3,p4)
  em2em_afem = em2em_afem + alpha/(2.*pi)*mat0                                &
              *Ieik(xieik1, Epart, parts((/part(p1,Qe,+1,1), part(p2,Qmu,+1,2), &
                                           part(p3,Qe,-1,1), part(p4,Qmu,-1,2)/), "x"))
  END FUNCTION EM2EM_AFEM


  FUNCTION EM2EM_AF(p1,p2,p3,p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! for massive electrons
  real (kind=prec), intent(in) :: p1(4),p2(4),p3(4),p4(4)
  real (kind=prec) :: em2em_af

  em2em_af = em2em_afee(p1,p2,p3,p4)&
             + em2em_afem(p1,p2,p3,p4)&
             + em2em_afmm(p1,p2,p3,p4)
  END FUNCTION EM2EM_AF


 !! needed to compare individual classes with [1901.03106]
  FUNCTION EM2EM_NFEM_CT(p1,p2,p3,p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4)
  real (kind=prec) :: em2em_nfem_ct, mat0, Epart, tt, deltalph_tot
#include "charge_config.h"
  Epart = sqrt(scms)
  mat0 = em2em_a(p1,p2,p3,p4)

  tt = sq(p1-p3)
  deltalph_tot = deltalph_0_tot(tt)
  em2em_nfem_ct = deltalph_tot*em2em_nfem_sin(p1,p2,p3,p4)

  em2em_nfem_ct = em2em_nfem_ct + alpha/(2.*pi)*mat0/2                                &
              *Ieik(xieik1, Epart, parts((/part(p1,Qe,+1,1), part(p2,Qmu,+1,2), &
                                           part(p3,Qe,-1,1), part(p4,Qmu,-1,2)/), "x"))
  END FUNCTION EM2EM_NFEM_CT


  FUNCTION EE2MMF(p1,p2,p3,p4)
    !! e-(p1) e+(p2) -> mu+(p3) mu-(p4)
    !! for massive electrons and positrons
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4)
  real (kind=prec) :: ee2mmf, mat0, epart
  Epart = sqrt(scms)
  mat0 = ee2mm(p1,p2,p3,p4)
  ee2mmf = ee2mml(p1,p2,p3,p4)
  ee2mmf = ee2mmf + alpha / (2 * pi) * mat0 * &
      Ieik(xieik1, Epart, parts((/part(p1, 1, 1), part(p2,-1, 1), part(p3,-1, -1), part(p4, 1, -1)/)))

  END FUNCTION EE2MMF


  FUNCTION PEPE2MMF_EE(p1,pol1,p2,pol2,p3,p4)
    !! e-(p1) e+(p2) -> g* -> mu-(p3) mu+(p4)
    !! for massive electrons and positrons
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4),pol1(4),pol2(4)
  real (kind=prec) :: pepe2mmf_ee, mat0, epart
  Epart = sqrt(sq(p1+p2))
  mat0 = pepe2mm(p1,pol1,p2,pol2,p3,p4)
  pepe2mmf_ee = pepe2mml_ee(p1,pol1,p2,pol2,p3,p4)
  pepe2mmf_ee = pepe2mmf_ee + alpha / (2 * pi) * mat0 * &
      Ieik(xieik1, Epart, parts((/part(p1, 1, 1), part(p2,-1, 1)/)))

  END FUNCTION


  FUNCTION PEPE2MMF(p1,pol1,p2,pol2,p3,p4)
    !! e-(p1) e+(p2) -> g* -> mu-(p3) mu+(p4)
    !! for massive electrons and positrons
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4),pol1(4),pol2(4)
  real (kind=prec) :: pepe2mmf, mat0, epart
  Epart = sqrt(sq(p1+p2))
  mat0 = pepe2mm(p1,pol1,p2,pol2,p3,p4)
  pepe2mmf = pepe2mml(p1,pol1,p2,pol2,p3,p4)
  pepe2mmf = pepe2mmf + alpha / (2 * pi) * mat0 * &
      Ieik(xieik1, Epart, parts((/part(p1, 1, 1), part(p2,-1, 1), part(p3, 1, -1), part(p4, -1, -1)/)))

  END FUNCTION


  FUNCTION PEPEZMMFX(p1,pol1,p2,pol2,p3,p4)
    !! e-(p1) e+(p2) -> Z -> mu-(p3) mu+(p4)
    !! for massive electrons and positrons
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4),pol1(4),pol2(4)
  real (kind=prec) :: pepezmmfx, mat0, epart
  Epart = sqrt(sq(p1+p2))
  mat0 = pepezmmx(p1,pol1,p2,pol2,p3,p4)
  pepezmmfx = pepezmmlx(p1,pol1,p2,pol2,p3,p4)
  pepezmmfx = pepezmmfx + alpha / (2 * pi) * mat0 * &
      Ieik(xieik1, Epart, parts((/part(p1, 1, 1), part(p2,-1, 1), part(p3, 1, -1), part(p4, -1, -1)/)))

  END FUNCTION

  FUNCTION EMZEMFX(p1,p2,p3,p4)
    !! e-(p1) mu+(p2) -> Z -> e-(p3) mu+(p4)
    !! for massive electrons and positrons
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4)
  real (kind=prec) :: emzemfx, mat0, epart
#include "charge_config.h"
  Epart = sqrt(sq(p1+p2))
  mat0 = emzemx(p1,p2,p3,p4)
  emzemfx = emzemlx(p1,p2,p3,p4)
  emzemfx = emzemfx + alpha / (2 * pi) * mat0 * &
      Ieik(xieik1, Epart, parts((/part(p1, Qe, 1), part(p2,Qmu, 1), part(p3, Qe, -1), part(p4, Qmu, -1)/)))

  END FUNCTION


  FUNCTION PEPE2MM_AFEE(p1,pol1,p2,pol2,p3,p4)
    !! e-(p1) e+(p2) -> mu-(p3) mu+(p4)
    !! for massive electrons and positrons
  real (kind=prec), intent(in) :: p1(4),p2(4),p3(4),p4(4)
  real (kind=prec), intent(in) :: pol1(4),pol2(4)
  real (kind=prec) :: pepe2mm_afee, mat0, Epart
  Epart = sqrt(scms)
  mat0 = pepe2mma(p1,pol1,p2,pol2,p3,p4)
  pepe2mm_afee = pepe2mm_alee(p1,pol1,p2,pol2,p3,p4)
  pepe2mm_afee = pepe2mm_afee + alpha/(2.*pi)*mat0*Ieik(xieik1, Epart, parts((/part(p1, 1, 1), part(p2, 1, -1)/)))
  END FUNCTION PEPE2MM_AFEE


  FUNCTION MP2MPf(p1,p2,p3,p4)
    !! mu-(p1) p(p2) -> mu(p3) p(p4)
    !! for massive muons
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4)
  real (kind=prec) :: mp2mpf, mat0, Epart
  Epart = sqrt(scms)

  mat0 = mp2mp(p1,p2,p3,p4)
  mp2mpf = mp2mpl(p1,p2,p3,p4)
  mp2mpf = mp2mpf + alpha/(2.*pi)*mat0*Ieik(xieik1, Epart, parts((/part(p1, 1, 1), part(p3, 1, -1)/)))
  END FUNCTION MP2MPf

  FUNCTION MP2MPf_MP(p1,p2,p3,p4)
    !! mu-(p1) p(p2) -> mu(p3) p(p4)
    !! for massive muons
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4)
  real (kind=prec) :: mp2mpf_mp, mat0, Epart
#include "charge_config.h"
  Epart = sqrt(scms)

  mat0 = mp2mp_nuc(p1,p2,p3,p4)
  mp2mpf_mp = mp2mpl_mp(p1,p2,p3,p4)
  mp2mpf_mp = mp2mpf_mp + alpha/(2.*pi)*mat0* &
              Ieik(xieik1, Epart, parts((/part(p1,Qmu,+1,1), part(p2,Qp,+1,2), &
                                          part(p3,Qmu,-1,1), part(p4,Qp,-1,2)/), "x"))
  END FUNCTION MP2MPf_MP


  FUNCTION MP2MP_AF(p1,p2,p3,p4)
    !! mu-(p1) p(p2) -> mu(p3) p(p4)
    !! for massive muons
  real (kind=prec), intent(in) :: p1(4),p2(4),p3(4),p4(4)
  real (kind=prec) :: mp2mp_af, mat0, Epart
  Epart = sqrt(scms)
  mat0 = mp2mp_a(p1,p2,p3,p4)
  mp2mp_af = mp2mp_al(p1,p2,p3,p4)
  mp2mp_af = mp2mp_af + alpha/(2.*pi)*mat0*Ieik(xieik1, Epart, parts((/part(p1, 1, 1), part(p3, 1, -1)/)))
  END FUNCTION MP2MP_AF



  FUNCTION EM2EMl(p1,p2,p3,p4,sing)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4)
  real (kind=prec) :: em2eml
  real (kind=prec),optional :: sing
  real (kind=prec) :: sing_ee, sing_em, sing_mm

  if(present(sing)) then
    em2eml =   em2eml_ee(p1,p2,p3,p4,sing_ee) &
             + em2eml_em(p1,p2,p3,p4,sing_em) &
             + em2eml_mm(p1,p2,p3,p4,sing_mm)
    sing   =   sing_ee + sing_em + sing_mm
  else
    em2eml =   em2eml_ee(p1,p2,p3,p4) &
             + em2eml_mm(p1,p2,p3,p4) &
             + em2eml_em(p1,p2,p3,p4)
  endif
  END FUNCTION EM2EMl

  FUNCTION EM2EMC(p1,p2,p3,p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4)
  real (kind=prec) :: em2emc, mat0, Epart
#include "charge_config.h"
  Epart = sqrt(scms)
  mat0 = em2em(p1,p2,p3,p4)
  em2emc = alpha/(2.*pi)*mat0*Ieik(xieik1, Epart, parts((/part(p1, Qe, 1), part(p2,Qmu, 1), &
                                                          part(p3, Qe,-1), part(p4,Qmu,-1)/)))
  END FUNCTION EM2EMC

  FUNCTION EM2EMG(p1,p2,p3,p4,p5)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4) g(p5)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4),p5(4)
  real (kind=prec) :: em2emg
  em2emg = em2emg_ee(p1,p2,p3,p4,p5) + em2emg_mm(p1,p2,p3,p4,p5) + em2emg_em(p1,p2,p3,p4,p5)
  END FUNCTION EM2EMG

  FUNCTION EM2EMGF(p1,p2,q1,q2,q3)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(q3)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4),q3(4)
  real (kind=prec) :: em2emgf
  real (kind=prec) :: mat0, pole, ctpole, ctfin

  mat0 = em2emg(p1,p2,q1,q2,q3)
  ctfin = em2emeik(p1,p2,q1,q2,xieik1,ctpole)
  em2emgf = em2emgl(p1,p2,q1,q2,q3,pole)
  em2emgf = em2emgf + ctfin*mat0

  !if (abs((ctpole*mat0+pole)/pole).gt.3e-4) print*,"em2emgf is not finite",abs((ctpole*mat0+pole)/pole)
  END FUNCTION EM2EMGF

  FUNCTION EM2EMGF_EEEE(p1,p2,q1,q2,q3)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(q3)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4),q3(4)
  real (kind=prec) :: em2emgf_eeee
  real (kind=prec) :: mat0, pole, ctpole, ctfin

  mat0 = em2emg_ee(p1,p2,q1,q2,q3)
  ctfin = em2emeik_ee(p1,p2,q1,q2,xieik1,ctpole)
  em2emgf_eeee = em2emgl_eeee(p1,p2,q1,q2,q3,pole)
  em2emgf_eeee = em2emgf_eeee + ctfin*mat0

  !if (abs((ctpole*mat0+pole)/pole).gt.3e-4) print*,"em2emgf_eeee is not finite",abs((ctpole*mat0+pole)/pole)
  END FUNCTION EM2EMGF_EEEE

  ! This matrix element is no longer supported and is kept
  ! for legacy / testing purposes. Use at your own risk.
  !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(q3)
  !! for massive electrons
  FUNCTION EM2EMGF_EEEE_APPROX(p1,p2,q1,q2,q3)
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4),q3(4)
  real (kind=prec) :: em2emgf_eeee_approx
  real (kind=prec) :: mat0, ctpole, ctfin

  mat0 = em2emg_ee(p1,p2,q1,q2,q3)
  ctfin = em2emeik_ee(p1,p2,q1,q2,xieik1,ctpole)
  em2emgf_eeee_approx = em2emgl_eeee_approx(p1,p2,q1,q2,q3)
  em2emgf_eeee_approx = em2emgf_eeee_approx + ctfin*mat0

  !if (abs((ctpole*mat0+pole)/pole).gt.3e-4) print*,"em2emgf_eeee is not finite",abs((ctpole*mat0+pole)/pole)
  END FUNCTION EM2EMGF_EEEE_APPROX

  ! This matrix element is no longer supported and is kept
  ! for legacy / testing purposes. Use at your own risk.
  FUNCTION EM2EMGF_EEEE_PV(p1,p2,q1,q2,q3)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(q3)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4),q3(4)
  real (kind=prec) :: em2emgf_eeee_pv
  real (kind=prec) :: mat0, pole, ctpole, ctfin

  mat0 = em2emg_ee(p1,p2,q1,q2,q3)
  ctfin = em2emeik_ee(p1,p2,q1,q2,xieik1,ctpole)
  em2emgf_eeee_pv = em2emgl_eeee_pv(p1,p2,q1,q2,q3,pole)
  em2emgf_eeee_pv = em2emgf_eeee_pv + ctfin*mat0
  END FUNCTION EM2EMGF_EEEE_PV

  ! This matrix element is no longer supported and is kept
  ! for legacy / testing purposes. Use at your own risk.
  FUNCTION EM2EMGF_EEEE_CO(p1,p2,q1,q2,q3)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(q3)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4),q3(4)
  real (kind=prec) :: em2emgf_eeee_co
  real (kind=prec) :: mat0, pole, ctpole, ctfin

  mat0 = em2emg_ee(p1,p2,q1,q2,q3)
  ctfin = em2emeik_ee(p1,p2,q1,q2,xieik1,ctpole)
  em2emgf_eeee_co = em2emgl_eeee_co(p1,p2,q1,q2,q3,pole)
  em2emgf_eeee_co = em2emgf_eeee_co + ctfin*mat0
  END FUNCTION EM2EMGF_EEEE_CO

  FUNCTION EM2EMGF_MIXD(p1,p2,p3,p4,p5)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4) g(p5)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4),p5(4)
  real (kind=prec) :: em2emgf_mixd
  real (kind=prec) :: Epart
  real (kind=prec) :: mat0e, mat0m, mat0x
  real (kind=prec) :: ctfine, ctfinm, ctfinx

  Epart  = sqrt(sq(p1+p2))

  mat0e = em2emg_ee(p1,p2,p3,p4,p5)
  mat0m = em2emg_mm(p1,p2,p3,p4,p5)
  mat0x = em2emg_em(p1,p2,p3,p4,p5)

  ctfine = em2emeik_ee(p1,p2,p3,p4,xieik1)
  ctfinm = em2emeik_ee(p2,p1,p4,p3,xieik1)
  ctfinx = em2emeik_em(p1,p2,p3,p4,xieik1)
  !ctfinx = (alpha/2/pi) * 2*(  Ireg(xieik2,Epart,p1,p4) + Ireg(xieik2,Epart,p2,p3)  &
  !                           - Ireg(xieik2,Epart,p1,p2) - Ireg(xieik2,Epart,p3,p4))

  em2emgf_mixd = em2emgl_mixd(p1,p2,p3,p4,p5)
  em2emgf_mixd = em2emgf_mixd + ctfinE*(mat0m+mat0x) + ctfinM*(mat0e+mat0x) + ctfinX*(mat0e+mat0m+mat0x)

  !if (abs((ctpole*mat0+pole)/pole).gt.3e-4) print*,"em2emgf_mixd is not finite",abs((ctpole*mat0+pole)/pole)
  END FUNCTION EM2EMGF_MIXD

  FUNCTION EM2EMGF_E3M1(p1,p2,p3,p4,p5)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4) g(p5)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4),p5(4)
  real (kind=prec) :: em2emgf_e3m1
  real (kind=prec) :: Epart
  real (kind=prec) :: mat0e, mat0m, mat0x
  real (kind=prec) :: ctfine, ctfinm, ctfinx

  Epart  = sqrt(sq(p1+p2))

  mat0e = em2emg_ee(p1,p2,p3,p4,p5)
  mat0m = em2emg_mm(p1,p2,p3,p4,p5)
  mat0x = em2emg_em(p1,p2,p3,p4,p5)

  ctfine = em2emeik_ee(p1,p2,p3,p4,xieik1)
  ctfinm = em2emeik_ee(p2,p1,p4,p3,xieik1)
  ctfinx = em2emeik_em(p1,p2,p3,p4,xieik1)

  em2emgf_e3m1 = em2emgl_e3m1(p1,p2,p3,p4,p5)
  em2emgf_e3m1 = em2emgf_e3m1 + ctfinE*(mat0x) + ctfinM*(0._prec) + ctfinX*(mat0e)
  END FUNCTION EM2EMGF_E3M1

  FUNCTION EM2EMGF_E2M2(p1,p2,p3,p4,p5)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4) g(p5)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4),p5(4)
  real (kind=prec) :: em2emgf_e2m2
  real (kind=prec) :: Epart
  real (kind=prec) :: mat0e, mat0m, mat0x
  real (kind=prec) :: ctfine, ctfinm, ctfinx

  Epart  = sqrt(sq(p1+p2))

  mat0e = em2emg_ee(p1,p2,p3,p4,p5)
  mat0m = em2emg_mm(p1,p2,p3,p4,p5)
  mat0x = em2emg_em(p1,p2,p3,p4,p5)

  ctfine = em2emeik_ee(p1,p2,p3,p4,xieik1)
  ctfinm = em2emeik_ee(p2,p1,p4,p3,xieik1)
  ctfinx = em2emeik_em(p1,p2,p3,p4,xieik1)

  em2emgf_e2m2 = em2emgl_e2m2(p1,p2,p3,p4,p5)
  em2emgf_e2m2 = em2emgf_e2m2 + ctfinE*(mat0m) + ctfinM*(mat0e) + ctfinX*(mat0x)
  END FUNCTION EM2EMGF_E2M2

  FUNCTION EM2EMGF_E1M3(p1,p2,p3,p4,p5)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4) g(p5)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4),p5(4)
  real (kind=prec) :: em2emgf_e1m3
  real (kind=prec) :: Epart
  real (kind=prec) :: mat0e, mat0m, mat0x
  real (kind=prec) :: ctfine, ctfinm, ctfinx

  Epart  = sqrt(sq(p1+p2))

  mat0e = em2emg_ee(p1,p2,p3,p4,p5)
  mat0m = em2emg_mm(p1,p2,p3,p4,p5)
  mat0x = em2emg_em(p1,p2,p3,p4,p5)

  ctfine = em2emeik_ee(p1,p2,p3,p4,xieik1)
  ctfinm = em2emeik_ee(p2,p1,p4,p3,xieik1)
  ctfinx = em2emeik_em(p1,p2,p3,p4,xieik1)

  em2emgf_e1m3 = em2emgl_e1m3(p1,p2,p3,p4,p5)
  em2emgf_e1m3 = em2emgf_e1m3 + ctfinE*(0._prec) + ctfinM*(mat0x) + ctfinX*(mat0m)
  END FUNCTION EM2EMGF_E1M3

  FUNCTION EM2EMGF_MMMM(p1,p2,q1,q2,q3)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(q3)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4),q3(4)
  real (kind=prec) :: em2emgf_mmmm
  real (kind=prec) :: mat0, ctfin

  mat0 = em2emg_mm(p1,p2,q1,q2,q3)
  ctfin = em2emeik_ee(p2,p1,q2,q1,xieik1)
  em2emgf_mmmm = em2emgl_mmmm(p1,p2,q1,q2,q3)
  em2emgf_mmmm = em2emgf_mmmm + ctfin*mat0

  !if (abs((ctpole*mat0+pole)/pole).gt.3e-4) print*,"em2emgf_mmmm is not finite",abs((ctpole*mat0+pole)/pole)
  END FUNCTION EM2EMGF_MMMM

  FUNCTION EM2EMFFz(p1, p2, p3, p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! for massified electrons
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4)
  real(kind=prec) :: em2emffz
  real(kind=prec) :: tlF(5)
#include "charge_config.h"

  call ememzf(p1,p2,p3,p4, tlF=tlF)
  em2emffz = sum((/ Qe**4*Qmu**0, Qe**3*Qmu**1, Qe**2*Qmu**2, Qe**1*Qmu**3, Qe**0*Qmu**4 /) * tlF)

  END FUNCTION EM2EMFFz

  FUNCTION EM2EMFFz_EEEE(p1, p2, p3, p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! for massified electrons
  use massification
  use mue_em2emll0, only: lqffOL, lqffTL
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4)
  real(kind=prec) :: em2emffz_eeee
  real(kind=prec) :: born, ol(-2:2), tl(-4:0)
  real(kind=prec) :: olZ(-1:1), tlZ(-2:0), ct(-1:0)
  real(kind=prec) :: ss, tt, mm2, me2
  real(kind=prec) :: Lm

  ! call ememzf(p1,p2,p3,p4, tlF=tlF)
  ss = sq(p1+p2); tt = sq(p1-p3)
  me2=sq(p1); mm2=sq(p2)
  Lm = log(-tt/musq)

  born = 32*pi*pi*(2*Mm2**2 - 4*Mm2*ss + 2*ss**2 + 2*ss*tt + tt**2)/tt**2

  ol = 2*lqffOL
  tl = 2*lqffTL + (/lqffOL(-2)**2,0.,lqffOL(-1)**2,0.,lqffOL(0)**2/) &
               +2*(/ 0., lqffOL(-2)*lqffOL(-1), lqffOL(-2)*lqffOL(0), &
                         lqffOL(-1)*lqffOL(0) + lqffOL(-2)*lqffOL(1), &
                         lqffOL(-1)*lqffOL(1) + lqffOL(-2)*lqffOL(2) /)

  ol = ol - [0., ol(-2:1)] * Lm &
          + [0., 0., ol(-2:0)] * Lm**2 / 2. &
          - [0., 0., 0., ol(-2:-1)] * Lm**3 / 6. &
          + [0., 0., 0., 0., ol(-2)] * Lm**4 / 24.

  tl = tl - [0., tl(-4:-1)] * (2*Lm) &
          + [0., 0., tl(-4:-2)] * (2*Lm)**2 / 2. &
          - [0., 0., 0., tl(-4:-3)] * (2*Lm)**3 / 6. &
          + [0., 0., 0., 0., tl(-4)] * ((2*Lm)**4 / 24. - 0.5*Lm)

  ol = 2*ol * born
  tl = 4*tl * born

  call massify(log(me2/musq), 2., born, ol, tl, olZ, tlZ, checkpole=1e-6)
  olZ = olZ / 2 / pi
  tlZ = tlZ / 4 / pi/pi

  born   = em2em(p1, p2, p3, p4)
  olZ(0) = em2eml_ee(p1, p2, p3, p4, olZ(-1))

  ct(0) = Ieik(xieik2, sqrt(sq(p1+p2)), parts((/ part(p1, 1, 1), part(p3, 1, -1) /)), ct(-1))
  ct = alpha * ct / 2 / pi

#if 0
  write(*,900) abs(ct(-1)*real(born) + olZ(-1)) / olZ(-1), &
               abs(tlZ(-2) + ct(-1) * olZ(-1) + real(born) * ct(-1)**2/2) / tlZ(-2), &
               abs(tlZ(-1) + ct( 0) * olZ(-1) + ct(-1) * olZ(0) + real(born) * ct(-1)*ct(0)) / tlZ(-1)
900 format("res. pole: ", "a^3/ep = ", ES7.1E2, ", a^4/ep^2 = ", ES7.1E2, ", a^4/ep = ", ES7.1E2)
#endif
  em2emffz_eeee = tlZ(0) + ct(0)*olZ(0) + ct(-1)*olZ(1) + real(born)*ct(0)**2/2

  END FUNCTION EM2EMFFz_EEEE

  FUNCTION EM2EMFF_MMMM(p1,p2,p3,p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! for massive electrons
  real (kind=prec), intent(in) :: p1(4),p2(4),p3(4),p4(4)
  real (kind=prec) :: em2emff_mmmm

  em2emff_mmmm = em2emff_eeee(p2,p1,p4,p3)
  END FUNCTION EM2EMFF_MMMM

  FUNCTION EM2EMFFz_MIXD(p1, p2, p3, p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! for massified electrons
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4)
  real(kind=prec) :: em2emffz_mixd
  real(kind=prec) :: tlF(5)
#include "charge_config.h"

  call ememzf(p1,p2,p3,p4, tlF=tlF)
  em2emffz_mixd = sum((/ Qe**3*Qmu**1, Qe**2*Qmu**2, Qe**1*Qmu**3 /) * tlF(2:4))

  END FUNCTION EM2EMFFz_MIXD

  FUNCTION EM2EMFFz_E3M1(p1, p2, p3, p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! for massified electrons
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4)
  real(kind=prec) :: em2emffz_e3m1
  real(kind=prec) :: tlF(5)
#include "charge_config.h"

  call ememzf(p1,p2,p3,p4, tlF=tlF)
  em2emffz_e3m1 = Qe**3*Qmu**1 * tlF(2)

  END FUNCTION EM2EMFFz_E3M1

  FUNCTION EM2EMFFz_E2M2(p1, p2, p3, p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! for massified electrons
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4)
  real(kind=prec) :: em2emffz_e2m2
  real(kind=prec) :: tlF(5)
#include "charge_config.h"

  call ememzf(p1,p2,p3,p4, tlF=tlF)
  em2emffz_e2m2 = Qe**2*Qmu**2 * tlF(3)

  END FUNCTION EM2EMFFz_E2M2

  FUNCTION EM2EMFFz_E1M3(p1, p2, p3, p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! for massified electrons
  real(kind=prec) :: p1(4), p2(4), p3(4), p4(4)
  real(kind=prec) :: em2emffz_e1m3
  real(kind=prec) :: tlF(5)
#include "charge_config.h"

  call ememzf(p1,p2,p3,p4, tlF=tlF)
  em2emffz_e1m3 = Qe**1*Qmu**3 * tlF(4)

  END FUNCTION EM2EMFFz_E1M3

                 !!!!!!!!!!!!!!!!!!!!!!!!!!!!
                       END MODULE MUE_MAT_EL
                 !!!!!!!!!!!!!!!!!!!!!!!!!!!!
