  integer, parameter :: Qe = -1
  integer, parameter :: Qp = +1
  integer :: Qmu
  select case(flavour)
    case("muone+", "muone160+")
      Qmu = +1
    case("muone-", "muone160-")
      Qmu = -1
    case default
      Qmu = -1
  end select
