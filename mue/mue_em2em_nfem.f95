                   !!!!!!!!!!!!!!!!!!!!!!
                      MODULE MUE_EM2EM_NFEM
                   !!!!!!!!!!!!!!!!!!!!!!

  use functions
  use collier
  implicit none

  INTERFACE D
    module procedure di, dij, dijk
  END INTERFACE


contains


  FUNCTION EM2EM_NFEM(z,p1,p2,q1,q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2)
    !! for massive electrons and muons
  real(kind=prec) :: z,p1(4),p2(4),q1(4),q2(4)
  real(kind=prec) :: em2em_nfem
  real(kind=prec) :: ss,tt,m2,mm2,x,qq2,jac
#include "charge_config.h"

  x = 1 - z**5
  ss = sq(p1+p2)
  m2 = sq(p1)
  mm2 = sq(p2)
  tt = sq(p1-q1)
  qq2 = (m2*x**2)/(1 - x)

  jac = 5*z**4*m2*(2.-x)*x/(1-x)**2
  em2em_nfem = jac*(mbx(qq2,ss,tt,m2,mm2) + mxbx(qq2,ss,tt,m2,mm2))

  ! Factor of two since vp can attach to either photon in box
  em2em_nfem = Qe*Qmu*2*em2em_nfem
  END FUNCTION


  FUNCTION EM2EM_NFEM_SIN(p1,p2,p3,p4,pole)
    real(kind=prec) :: em2em_nfem_sin
    real(kind=prec),intent(in) :: p1(4),p2(4),p3(4),p4(4)
    real(kind=prec), optional :: pole
    real(kind=prec) :: ss, tt, m2, m, mm2, mm, lgmu
    real(kind=prec) :: lg_1,lg_2,discb_1,discb_2,scalarc0ir6_1,scalarc0ir6_2
    real(kind=prec) :: pva_1,pva_2,pvb_1,pvb_2,pvb_3,pvc_1,pvc_2,pvd_1,pvd_2
    complex(kind=prec) :: pvd_1_cplx,pvd_2_cplx
#include "charge_config.h"

    ss = sq(p1+p2); tt = sq(p1-p3)
    m2 = sq(p1); m = sqrt(m2)
    mm2 = sq(p2); mm = sqrt(mm2)
    lgmu = log(musq/m2)

    lg_1 = log(m2/mm2)
    lg_2 = log(-(m2/tt))
    discb_1 = discb(ss,m,mm)
    discb_2 = discb(2*m2 + 2*mm2 - ss - tt,m,mm)
    scalarc0ir6_1 = scalarc0ir6(ss,m,mm)
    scalarc0ir6_2 = scalarc0ir6(2*m2 + 2*mm2 - ss - tt,m,mm)

    pva_1 = (1 + lgmu)*m2
    pva_2 = (1 + lg_1 + lgmu)*mm2
    pvb_1 = 2 + discb_1 + lg_1 + lgmu - (lg_1*(m2 - mm2 + ss))/(2.*ss)
    pvb_2 = 2 + discb_2 + lg_1 + lgmu - (lg_1*(3*m2 + mm2 - ss - tt))/(4*m2 + 4*mm2 - 2*(ss &
        &+ tt))
    pvb_3 = 2 + lg_2 + lgmu
    pvc_1 = scalarc0ir6_1 + (discb_1*(lg_1 + 2*lgmu)*ss)/(2.*(m2**2 + (mm2 - ss)**2 - 2*m2*(&
        &mm2 + ss)))
    pvc_2 = scalarc0ir6_2 + (discb_2*lg_1*(2*m2 + 2*mm2 - ss - tt))/(2.*(m2**2 + (-mm2 + ss &
        &+ tt)**2 - 2*m2*(mm2 + ss + tt))) + (discb_2*lgmu*(2*m2 + 2*mm2 - ss - tt))/(m2*&
        &*2 + (-mm2 + ss + tt)**2 - 2*m2*(mm2 + ss + tt))

    call SetMuIR2_cll(musq)

    call D0_cll(pvd_1_cplx,cmplx(m2),cmplx(mm2),cmplx(mm2)&
        ,cmplx(m2),cmplx(2*m2 + 2*mm2 - ss - tt),cmplx(tt),cmplx(m2),cmplx(0),cmplx(mm2),cmplx(-tt))
    call D0_cll(pvd_2_cplx,cmplx(m2),cmplx(tt),cmplx(mm2),&
        cmplx(ss),cmplx(m2),cmplx(mm2),cmplx(m2),cmplx(-tt),cmplx(0),cmplx(mm2))
    pvd_1=real(pvd_1_cplx)
    pvd_2=real(pvd_2_cplx)

    em2em_nfem_sin = -(2*pvb_3*(2*m2 + 2*mm2 - 2*ss - tt) + pvc_1*(m2 + mm2 - ss)*(2*ss + tt) + 2*pvd&
                 &_2*(m2 + mm2 - ss)*(4*m2**2 + 4*mm2**2 + 8*m2*(mm2 - ss) - 8*mm2*ss + 4*ss**2 + &
                 &2*ss*tt + tt**2) + pvc_2*(4*m2**2 + 4*mm2**2 - 6*mm2*ss + 2*ss**2 + m2*(8*mm2 - &
                 &6*ss - 5*tt) - 5*mm2*tt + 3*ss*tt + tt**2) + 2*pvd_1*(m2 + mm2 - ss - tt)*(4*m2*&
                 &*2 + 8*m2*mm2 + 4*mm2**2 + 4*ss**2 + 6*ss*tt + 3*tt**2 - 4*m2*(2*ss + tt) - 4*mm&
                 &2*(2*ss + tt)) + (2*pvb_2*(m2**3 + mm2**3 - (ss + tt)**3 - mm2**2*(3*ss + 2*tt) &
                 &- m2**2*(mm2 + 3*ss + 2*tt) + mm2*(3*ss**2 + 5*ss*tt + 2*tt**2) + m2*(-mm2**2 - &
                 &2*mm2*ss + 3*ss**2 + 4*mm2*tt + 5*ss*tt + 2*tt**2)))/((m2 - 2*m*mm + mm2 - ss - &
                 &tt)*(m2 + 2*m*mm + mm2 - ss - tt)) + (2*pvb_1*(m2**3 + m2**2*(-mm2 - 3*ss + tt) &
                 &+ (mm2 - ss)*(mm2**2 + ss**2 + mm2*(-2*ss + tt)) - m2*(mm2**2 + ss*(-3*ss + tt) &
                 &+ 2*mm2*(ss + tt))))/((m2 - 2*m*mm + mm2 - ss)*(m2 + 2*m*mm + mm2 - ss)) - (2*pv&
                 &a_2*(2*m2**5 - m2**4*(6*mm2 + 5*(2*ss + tt)) + m2**2*(4*mm2**3 + 4*mm2**2*ss - 2&
                 &0*ss**3 - 30*ss**2*tt - 12*ss*tt**2 - tt**3 + 3*mm2*(4*ss**2 + 2*ss*tt - tt**2))&
                 & + m2**3*(4*mm2**2 + mm2*(8*ss + 6*tt) + 4*(5*ss**2 + 5*ss*tt + tt**2)) + (mm2 -&
                 & ss)*(2*mm2**4 + ss*(ss + tt)**2*(2*ss + tt) - mm2**3*(8*ss + 3*tt) + mm2**2*(12&
                 &*ss**2 + 11*ss*tt + tt**2) - mm2*ss*(8*ss**2 + 13*ss*tt + 5*tt**2)) + m2*(-6*mm2&
                 &**4 + 2*mm2**3*(4*ss + tt) + 2*mm2**2*(6*ss**2 + 4*ss*tt - tt**2) + mm2*(-24*ss*&
                 &*3 - 30*ss**2*tt - 6*ss*tt**2 + tt**3) + 2*ss*(5*ss**3 + 10*ss**2*tt + 6*ss*tt**&
                 &2 + tt**3))))/(mm2*(m2 - 2*m*mm + mm2 - ss)*(m2 + 2*m*mm + mm2 - ss)*(m2 - 2*m*m&
                 &m + mm2 - ss - tt)*(m2 + 2*m*mm + mm2 - ss - tt)) - (2*pva_1*(2*m2**5 + (mm2 - s&
                 &s)**2*(2*mm2 - 2*ss - tt)*(-mm2 + ss + tt)**2 - m2**4*(6*mm2 + 10*ss + 3*tt) + m&
                 &2**3*(4*mm2**2 + 20*ss**2 + 14*ss*tt + tt**2 + 2*mm2*(4*ss + tt)) + 2*m2**2*(2*m&
                 &m2**3 + 2*mm2**2*ss + mm2*(6*ss**2 + 4*ss*tt - tt**2) - ss*(10*ss**2 + 12*ss*tt &
                 &+ 3*tt**2)) + m2*(-6*mm2**4 + mm2**3*(8*ss + 6*tt) + 3*mm2**2*(4*ss**2 + 2*ss*tt&
                 & - tt**2) + mm2*(-24*ss**3 - 30*ss**2*tt - 6*ss*tt**2 + tt**3) + ss*(10*ss**3 + &
                 &18*ss**2*tt + 9*ss*tt**2 + tt**3))))/(m2*(m2 - 2*m*mm + mm2 - ss)*(m2 + 2*m*mm +&
                 & mm2 - ss)*(m2 - 2*m*mm + mm2 - ss - tt)*(m2 + 2*m*mm + mm2 - ss - tt)) - (2*(4*&
                 &m2**5 - 4*m2**4*(3*mm2 + 5*ss + 2*tt) + m2**3*(8*mm2**2 + 40*ss**2 + 34*ss*tt + &
                 &5*tt**2 + 8*mm2*(2*ss + tt)) + m2**2*(8*mm2**3 + 8*mm2**2*ss - 40*ss**3 - 54*ss*&
                 &*2*tt - 18*ss*tt**2 - tt**3 + mm2*(24*ss**2 + 14*ss*tt - 5*tt**2)) + (mm2 - ss)*&
                 &(4*mm2**4 - 8*mm2**3*(2*ss + tt) + 2*ss*(ss + tt)**2*(2*ss + tt) + mm2**2*(24*ss&
                 &**2 + 26*ss*tt + 5*tt**2) - mm2*(16*ss**3 + 28*ss**2*tt + 13*ss*tt**2 + tt**3)) &
                 &+ m2*(-12*mm2**4 + 8*mm2**3*(2*ss + tt) + mm2**2*(24*ss**2 + 14*ss*tt - 5*tt**2)&
                 & - 2*mm2*(24*ss**3 + 30*ss**2*tt + 6*ss*tt**2 - tt**3) + ss*(20*ss**3 + 38*ss**2&
                 &*tt + 21*ss*tt**2 + 3*tt**3))))/((m2 - 2*m*mm + mm2 - ss)*(m2 + 2*m*mm + mm2 - s&
                 &s)*(m2 - 2*m*mm + mm2 - ss - tt)*(m2 + 2*m*mm + mm2 - ss - tt)))/(4.*tt)

    em2em_nfem_sin = 64*alpha**3*pi*em2em_nfem_sin
    ! Factor of two since vp can attach to either photon in box
    em2em_nfem_sin = Qe*Qmu*2*em2em_nfem_sin

    if (present(pole)) then
      pole = -((discb_1*(m2 + mm2 - ss)*ss*(2*m2**2 + 2*mm2**2 + 4*m2*(mm2 - ss) - 4*mm2*ss +&
       & 2*ss**2 + 2*ss*tt + tt**2))/(m2**2 + (mm2 - ss)**2 - 2*m2*(mm2 + ss)) + (discb_&
       &2*(4*m2**4 + 4*mm2**4 + 2*m2**3*(8*mm2 - 7*ss - 3*tt) - 2*mm2**3*(7*ss + 3*tt) +&
       & (ss + tt)**2*(2*ss**2 + 2*ss*tt + tt**2) + 2*mm2**2*(9*ss**2 + 10*ss*tt + 2*tt*&
       &*2) - mm2*(10*ss**3 + 20*ss**2*tt + 13*ss*tt**2 + 3*tt**3) + 2*m2**2*(12*mm2**2 &
       &+ 9*ss**2 + 10*ss*tt + 2*tt**2 - 3*mm2*(7*ss + 3*tt)) + m2*(16*mm2**3 - 10*ss**3&
       & - 20*ss**2*tt - 13*ss*tt**2 - 3*tt**3 - 6*mm2**2*(7*ss + 3*tt) + 4*mm2*(9*ss**2&
       & + 10*ss*tt + 2*tt**2))))/(m2**2 + (-mm2 + ss + tt)**2 - 2*m2*(mm2 + ss + tt)))/&
       &(2.*tt**2)

      pole = 64*alpha**3*pi*pole
      ! Factor of two since vp can attach to either photon in box
      pole = Qe*Qmu*2*pole
    end if
  END FUNCTION



  FUNCTION MBX(qq2,ss,tt,m2,mm2)
  real(kind=prec) :: mbx
  real(kind=prec), intent(in) :: qq2,ss,tt,m2,mm2

  mbx = -2*(-4*m2 + ss + tt)*((m2 + mm2 - ss)*(4*m2**2 + 4*mm2**2 + 8*m2*(mm2 - s&
    &s) - 8*mm2*ss + 4*ss**2 + 2*ss*tt + tt**2)*G012K(3,qq2,ss,tt,m2,mm2) - (&
    &m2 + mm2 - ss + tt)*GIJ(0,1,3,qq2,ss,tt,m2,mm2) + 2*(m2 + mm2 - ss)*GIJ(0&
    &,2,3,qq2,ss,tt,m2,mm2) - (m2 + mm2 - ss + tt)*GIJ(0,3,3,qq2,ss,tt,m2,mm2)&
    & - (m2 + mm2 - ss)*GIJ(1,2,3,qq2,ss,tt,m2,mm2) + (m2 + mm2 - ss + 2*tt)*G&
    &IJ(1,3,3,qq2,ss,tt,m2,mm2) - (m2 + mm2 - ss)*GIJ(2,3,3,qq2,ss,tt,m2,mm2) &
    &+ (4*mm2**2 + 4*ss**2 + ss*tt + tt**2 + mm2*(-8*ss + tt) + m2*(4*mm2 - 4*&
    &ss + tt))*GIJK(0,1,2,qq2,ss,tt,m2,mm2) + (m2 + mm2 - ss)*(2*ss + tt)*GIJ&
    &K(0,1,3,qq2,ss,tt,m2,mm2) + (4*m2**2 + 4*ss**2 + ss*tt + tt**2 + m2*(4*&
    &mm2 - 8*ss + tt) + mm2*(-4*ss + tt))*GIJK(0,2,3,qq2,ss,tt,m2,mm2) + 2*(m2&
    & + mm2 - ss)*ss*GIJK(1,2,3,qq2,ss,tt,m2,mm2) + 2*tt*Gp13(qq2,ss,tt,m2,mm2&
    &) + 2*(m2 + mm2 - ss)*Gp133(qq2,ss,tt,m2,mm2) - 2*tt*Gp2(qq2,ss,tt,m2,mm2&
    &))

  mbx = (8*alpha**3*Pi)*mbx/(tt*(-4*m2 + ss + tt))
  END FUNCTION

  FUNCTION MXBX(qq2,ss,tt,m2,mm2)
  real(kind=prec) :: mxbx
  real(kind=prec), intent(in) :: qq2,ss,tt,m2,mm2

  mxbx = 2*(-4*m2 + ss + tt)*(-((m2 + mm2 - ss - tt)*(4*m2**2 + 8*m2*mm2 + 4*mm2**&
     &2 + 4*ss**2 + 6*ss*tt + 3*tt**2 - 4*m2*(2*ss + tt) - 4*mm2*(2*ss + tt))&
     &*G012K(4,qq2,ss,tt,m2,mm2)) + (m2 + mm2 - ss - 2*tt)*GIJ(0,1,4,qq2,ss,tt&
     &,m2,mm2) - 2*(m2 + mm2 - ss - tt)*GIJ(0,2,4,qq2,ss,tt,m2,mm2) + (m2 + mm2 &
     &- ss - 2*tt)*GIJ(0,4,4,qq2,ss,tt,m2,mm2) + (m2 + mm2 - ss - tt)*GIJ(1,2,&
     &4,qq2,ss,tt,m2,mm2) - (m2 + mm2 - ss - 3*tt)*GIJ(1,4,4,qq2,ss,tt,m2,mm2) &
     &+ (m2 + mm2 - ss - tt)*GIJ(2,4,4,qq2,ss,tt,m2,mm2) + (8*m2**2 + 4*mm2**2 &
     &- 8*mm2*ss + 4*ss**2 + 3*m2*(4*mm2 - 4*ss - 3*tt) - 5*mm2*tt + 7*ss*tt + &
     &4*tt**2)*GIJK(0,1,2,qq2,ss,tt,m2,mm2) - (4*m2 + 4*mm2 - 2*ss - tt)*(m2 +&
     & mm2 - ss - tt)*GIJK(0,1,4,qq2,ss,tt,m2,mm2) + (4*m2**2 + 8*mm2**2 + 4*ss&
     &**2 + m2*(12*mm2 - 8*ss - 5*tt) + 7*ss*tt + 4*tt**2 - 3*mm2*(4*ss + 3*tt&
     &))*GIJK(0,2,4,qq2,ss,tt,m2,mm2) - 2*(m2 + mm2 - ss - tt)*(2*m2 + 2*mm2 - &
     &ss - tt)*GIJK(1,2,4,qq2,ss,tt,m2,mm2) - 2*(m2 + mm2 - ss - tt)*Gp134(qq2&
     &,ss,tt,m2,mm2) + 2*tt*Gp14(qq2,ss,tt,m2,mm2) + 2*tt*Gp4(qq2,ss,tt,m2,mm2)&
     &)

  mxbx = (8*alpha**3*Pi)*mxbx/(tt*(-4*m2 + ss + tt))
  END FUNCTION

  FUNCTION VPCONVOLUTE(kernel,sub,tt,qq2)
  real(kind=prec) :: vpconvolute
  real(kind=prec), intent(in) :: kernel,sub,tt,qq2

  vpconvolute = (deltalph_0_tot(-qq2) - sub*deltalph_0_tot(tt))*kernel
  END FUNCTION


  FUNCTION GIJK(i,j,k,qq2,ss,tt,m2,mm2)
  real(kind=prec) :: gijk
  integer, intent(in) :: i,j,k
  real(kind=prec), intent(in) :: qq2,ss,tt,m2,mm2
  real(kind=prec) :: kernel

  if(i==0) then
    kernel = - d(j,k,qq2,ss,tt,m2,mm2)
    gijk = vpconvolute(kernel,0._prec,tt,qq2)
  else
    kernel = qq2*d(i,j,k,qq2,ss,tt,m2,mm2)
    gijk = vpconvolute(kernel,1._prec,tt,qq2)
  endif
  END FUNCTION


  FUNCTION GIJ(i,j,k,qq2,ss,tt,m2,mm2)
  real(kind=prec) :: gij
  integer, intent(in) :: i,j,k
  real(kind=prec), intent(in) :: qq2,ss,tt,m2,mm2
  real(kind=prec) :: kernel

  if(i==0) then
    kernel = - d(j,qq2,ss,tt,m2,mm2) + qq2**2*d(1,2,k,qq2,ss,tt,m2,mm2)
  else
    kernel = qq2*d(i,j,qq2,ss,tt,m2,mm2) + qq2**2*d(1,2,k,qq2,ss,tt,m2,mm2)
  endif
  gij = vpconvolute(kernel,1._prec,tt,qq2)
  END FUNCTION

  FUNCTION G012K(k,qq2,ss,tt,m2,mm2)
  real(kind=prec) :: g012k
  integer, intent(in) :: k
  real(kind=prec), intent(in) :: qq2,ss,tt,m2,mm2
  real(kind=prec) :: kernel, sub

  kernel = - d(1,2,k,qq2,ss,tt,m2,mm2)
  sub = 2*qq2/(qq2+abs(tt))
  g012k = vpconvolute(kernel,sub,tt,qq2)
  END FUNCTION

  FUNCTION GP13(qq2,ss,tt,m2,mm2)
  real(kind=prec) :: gp13
  real(kind=prec), intent(in) :: qq2,ss,tt,m2,mm2
  real(kind=prec) :: kernel

  kernel = -((2*m2 + 2*mm2 - 2*ss - tt)*d(2,qq2,ss,tt,m2,mm2) + (-m2 + mm2 + ss)*d(3&
       &,qq2,ss,tt,m2,mm2) - ((-m2 + mm2 + ss)*tt + qq2*(-m2 - 3*mm2 + ss + tt))*d&
       &(2,3,qq2,ss,tt,m2,mm2))/(2.*(4*mm2 - tt))
  gp13 = vpconvolute(kernel,0._prec,tt,qq2)
  END FUNCTION

  FUNCTION GP14(qq2,ss,tt,m2,mm2)
  real(kind=prec) :: gp14
  real(kind=prec), intent(in) :: qq2,ss,tt,m2,mm2
  real(kind=prec) :: uu, kernel

  uu = 2*m2+2*mm2-ss-tt
  kernel = -((2*m2 + 2*mm2 - tt - 2*uu)*d(2,qq2,ss,tt,m2,mm2) + (-m2 + mm2 + uu)*&
       &d(4,qq2,ss,tt,m2,mm2) - (tt*(-m2 + mm2 + uu) + qq2*(-m2 - 3*mm2 + tt +&
       & uu))*d(2,4,qq2,ss,tt,m2,mm2))/(2.*(4*mm2 - tt))
  gp14 = vpconvolute(kernel,0._prec,tt,qq2)
  END FUNCTION

  FUNCTION GP2(qq2,ss,tt,m2,mm2)
  real(kind=prec) :: gp2
  real(kind=prec), intent(in) :: qq2,ss,tt,m2,mm2
  real(kind=prec) :: kernel

  kernel = ((m2 - mm2 + ss)*d(1,qq2,ss,tt,m2,mm2) + (2*m2 + 2*mm2 - 2*ss - tt)*d(2,q&
       &q2,ss,tt,m2,mm2) - ((m2 - mm2 + ss)*tt + qq2*(-3*m2 - mm2 + ss + tt))*d(1,&
       &2,qq2,ss,tt,m2,mm2))/(2.*(4*m2 - tt))
  gp2 = vpconvolute(kernel,0._prec,tt,qq2)
  END FUNCTION

  FUNCTION GP4(qq2,ss,tt,m2,mm2)
  real(kind=prec) :: gp4
  real(kind=prec), intent(in) :: qq2,ss,tt,m2,mm2
  real(kind=prec) :: uu,kernel

  uu = 2*m2+2*mm2-ss-tt
  kernel = -((m2 - mm2 + uu)*d(1,qq2,ss,tt,m2,mm2) + (2*m2 + 2*mm2 - tt - 2*uu)*d(2,&
       &qq2,ss,tt,m2,mm2) - (tt*(m2 - mm2 + uu) + qq2*(-3*m2 - mm2 + tt + uu))*d(1&
       &,2,qq2,ss,tt,m2,mm2))/(2.*(4*m2 - tt))
  gp4 = vpconvolute(kernel,0._prec,tt,qq2)
  END FUNCTION

  FUNCTION GP133(qq2,ss,tt,m2,mm2)
  real(kind=prec) :: gp133
  real(kind=prec), intent(in) :: qq2,ss,tt,m2,mm2
  real(kind=prec) :: kernel

  kernel = (tt*((m2 - mm2 + ss)*d(1,qq2,ss,tt,m2,mm2) + (-m2 + mm2 + ss)*d(3,qq2,ss,&
       &tt,m2,mm2) + 2*qq2*ss*d(1,3,qq2,ss,tt,m2,mm2)))/(2.*sq_lambda(ss,sqrt(m2),sqrt(mm2))**2)
  gp133 = vpconvolute(kernel,0._prec,tt,qq2)
  END FUNCTION

  FUNCTION GP134(qq2,ss,tt,m2,mm2)
  real(kind=prec) :: gp134
  real(kind=prec), intent(in) :: qq2,ss,tt,m2,mm2
  real(kind=prec) :: uu,kernel

  uu = 2*m2+2*mm2-ss-tt
  kernel = (tt*((m2 - mm2 + uu)*d(1,qq2,ss,tt,m2,mm2) + (-m2 + mm2 + uu)*d(4,qq2,ss,&
       &tt,m2,mm2) + 2*qq2*uu*d(1,4,qq2,ss,tt,m2,mm2)))/(2.*sq_lambda(uu,sqrt(m2),sqrt(mm2))**2)
  gp134 = vpconvolute(kernel,0._prec,tt,qq2)
  END FUNCTION


  FUNCTION DI(i,qq2,ss,tt,m2,mm2)
  real(kind=prec) :: di
  integer, intent(in) :: i
  real(kind=prec), intent(in) :: qq2,ss,tt,m2,mm2

  if(i==1) then
    di = d1(qq2,ss,tt,m2,mm2)
  elseif(i==2) then
    di = d2(qq2,ss,tt,m2,mm2)
  elseif(i==3) then
    di = d1(qq2,ss,tt,mm2,m2)
  elseif(i==4) then
    di = d1(qq2,ss,tt,mm2,m2)
  else
    call crash("DI")
  endif
  END FUNCTION

  FUNCTION DIJ(i,j,qq2,ss,tt,m2,mm2)
  real(kind=prec) :: dij
  integer, intent(in) :: i,j
  real(kind=prec), intent(in) :: qq2,ss,tt,m2,mm2

  if(i==1 .and. j==2) then
    dij = d12(qq2,ss,tt,m2,mm2)
  elseif(i==2 .and. j==3) then
    dij = d12(qq2,ss,tt,mm2,m2)
  elseif(i==2 .and. j==4) then
    dij = d12(qq2,ss,tt,mm2,m2)
  elseif(i==1 .and. j==3) then
    dij = d13(qq2,ss,tt,m2,mm2)
  elseif(i==1 .and. j==4) then
    dij = d14(qq2,ss,tt,m2,mm2)
  else
    call crash("DIJ")
  endif
  END FUNCTION



  FUNCTION DIJK(i,j,k,qq2,ss,tt,m2,mm2)
  real(kind=prec) :: dijk
  integer, intent(in) :: i,j,k
  real(kind=prec), intent(in) :: qq2,ss,tt,m2,mm2

  if(i==1 .and. j==2 .and. k==3) then
    dijk = d123(qq2,ss,tt,m2,mm2)
  elseif(i==1 .and. j==2 .and. k==4) then
    dijk = d124(qq2,ss,tt,m2,mm2)
  else
    call crash("DIJK")
  endif
  END FUNCTION

  FUNCTION LL(z)
  real(kind=precnf) :: ll, arg
  real(kind=precnf), intent(in) :: z

  arg = (1+z)/(1-z)
  ll = log(abs(arg))/2
  END FUNCTION


  FUNCTION D1(qq2prec,ssprec,ttprec,m2prec,mm2prec)
  real(kind=prec) :: d1
  real(kind=prec),intent(in) :: qq2prec,ssprec,ttprec,m2prec,mm2prec
  real(kind=precnf) :: qq2,ss,tt,m2,mm2

  qq2 = real(qq2prec,kind=precnf)
  ss = real(ssprec,kind=precnf)
  tt = real(ttprec,kind=precnf)
  m2 = real(m2prec,kind=precnf)
  mm2 = real(mm2prec,kind=precnf)

  d1 = real((1-sqrt(1+4*m2/qq2))/2./m2,kind=prec)
  END FUNCTION

  FUNCTION D2(qq2,ss,tt,m2,mm2)
  real(kind=prec) :: d2
  real(kind=prec), intent(in) :: qq2,ss,tt,m2,mm2

  if(qq2 + tt < 0) then
    d2 = 1/tt
  else
    d2 = -1/qq2
  end if
  END FUNCTION

  FUNCTION D12(qq2prec,ssprec,ttprec,m2prec,mm2prec)
  real(kind=prec) :: d12
  real(kind=prec), intent(in) :: qq2prec,ssprec,ttprec,m2prec,mm2prec
  real(kind=precnf) :: qq2,ss,tt,m2,mm2,z1,z2,d12precnf

  qq2 = real(qq2prec,kind=precnf)
  ss = real(ssprec,kind=precnf)
  tt = real(ttprec,kind=precnf)
  m2 = real(m2prec,kind=precnf)
  mm2 = real(mm2prec,kind=precnf)

  z1 = (Sqrt(qq2*(4*m2 + qq2))*Sqrt(tt*(-4*m2 + tt)))/(-2*m2*qq2 + 2*m2*tt + &
   &qq2*tt)
  z2 = Sqrt(1 - (4*m2)/tt)

  d12precnf = ll(z1)
  if(qq2 + tt > 0) d12precnf = d12precnf + 2*ll(1/z2)
  d12 = real(d12precnf,kind=prec)
  d12 = -d12/qq2prec/sqrt(ttprec*(ttprec-4*m2prec))
  END FUNCTION

  FUNCTION D13(qq2prec,ssprec,ttprec,m2prec,mm2prec)
  real(kind=prec) :: d13
  real(kind=prec), intent(in) :: qq2prec,ssprec,ttprec,m2prec,mm2prec
  real(kind=precnf) :: qq2,ss,tt,m2,mm2,z5,z6,z7

  qq2 = real(qq2prec,kind=precnf)
  ss = real(ssprec,kind=precnf)
  tt = real(ttprec,kind=precnf)
  m2 = real(m2prec,kind=precnf)
  mm2 = real(mm2prec,kind=precnf)

  z5 = Sqrt(1 + (4*m2)/qq2)*Sqrt(1 - (4*m2*ss)/(m2 - mm2 + ss)**2)
  z6 = Sqrt(1 + (4*mm2)/qq2)*Sqrt(1 - (4*mm2*ss)/(-m2 + mm2 + ss)**2)
  z7 = Sqrt(1 - (4*m2*mm2)/(-m2 - mm2 + ss)**2)

  d13 = real(ll(z5)+ll(z6)-ll(z7),kind=prec)
  d13 = d13/qq2prec/sq_lambda(ssprec,sqrt(m2prec),sqrt(mm2prec))
  END FUNCTION


  FUNCTION D14(qq2prec,ssprec,ttprec,m2prec,mm2prec)
  real(kind=prec) :: d14
  real(kind=prec), intent(in) :: qq2prec,ssprec,ttprec,m2prec,mm2prec
  real(kind=precnf) :: qq2,ss,tt,m2,mm2,uu,z8,z9,z10

  qq2 = real(qq2prec,kind=precnf)
  ss = real(ssprec,kind=precnf)
  tt = real(ttprec,kind=precnf)
  m2 = real(m2prec,kind=precnf)
  mm2 = real(mm2prec,kind=precnf)

  uu = 2*m2+2*mm2-ss-tt
  z8 = Sqrt(1 - (4*m2*mm2)/(-m2 - mm2 + uu)**2)
  z9 = Sqrt(1 + (4*m2)/qq2)*Sqrt(1 - (4*m2*uu)/(m2 - mm2 + uu)**2)
  z10 = Sqrt(1 + (4*mm2)/qq2)*Sqrt(1 - (4*mm2*uu)/(-m2 + mm2 + uu)**2)

  d14 = real(ll(z8)-ll(z9)+ll(sign(-1._precnf,uu+mm2-m2)*z10),kind=prec)
  d14 = d14/qq2prec/sq_lambda(real(uu,kind=prec),sqrt(m2prec),sqrt(mm2prec))
  END FUNCTION


  FUNCTION D123(qq2prec,ssprec,ttprec,m2prec,mm2prec)
  real(kind=prec) :: d123
  real(kind=prec), intent(in) :: qq2prec,ssprec,ttprec,m2prec,mm2prec
  real(kind=precnf) :: qq2,ss,tt,m2,mm2,z5,z6,z7

  qq2 = real(qq2prec,kind=precnf)
  ss = real(ssprec,kind=precnf)
  tt = real(ttprec,kind=precnf)
  m2 = real(m2prec,kind=precnf)
  mm2 = real(mm2prec,kind=precnf)

  z5 = Sqrt(1 + (4*m2)/qq2)*Sqrt(1 - (4*m2*ss)/(m2 - mm2 + ss)**2)
  z6 = Sqrt(1 + (4*mm2)/qq2)*Sqrt(1 - (4*mm2*ss)/(-m2 + mm2 + ss)**2)
  z7 = Sqrt(1 - (4*m2*mm2)/(-m2 - mm2 + ss)**2)

  d123 = real(&
          ll(sign(-1._precnf,qq2+tt)*z5)&
          +ll(sign(-1._precnf,qq2+tt)*z6)&
          -ll(z7),kind=prec)
  d123 = -d123/qq2prec/abs(qq2prec+ttprec)/sq_lambda(ssprec,sqrt(m2prec),sqrt(mm2prec))
  END FUNCTION


  FUNCTION D124(qq2prec,ssprec,ttprec,m2prec,mm2prec)
  real(kind=prec) :: d124
  real(kind=prec), intent(in) :: qq2prec,ssprec,ttprec,m2prec,mm2prec
  real(kind=precnf) :: qq2,ss,tt,m2,mm2,uu,z8,z9,z10

  qq2 = real(qq2prec,kind=precnf)
  ss = real(ssprec,kind=precnf)
  tt = real(ttprec,kind=precnf)
  m2 = real(m2prec,kind=precnf)
  mm2 = real(mm2prec,kind=precnf)

  uu = 2*m2+2*mm2-ss-tt
  z8 = Sqrt(1 - (4*m2*mm2)/(-m2 - mm2 + uu)**2)
  z9 = Sqrt(1 + (4*m2)/qq2)*Sqrt(1 - (4*m2*uu)/(m2 - mm2 + uu)**2)
  z10 = Sqrt(1 + (4*mm2)/qq2)*Sqrt(1 - (4*mm2*uu)/(-m2 + mm2 + uu)**2)

  d124 = real(&
          -ll(sign(-1._precnf,qq2+tt)*z9)&
          +ll(sign(-1._precnf,qq2+tt)*sign(-1._precnf,uu+mm2-m2)*z10)&
          +ll(z8),kind=prec)
  d124 = -d124/qq2prec/abs(qq2prec+ttprec)&
          /sq_lambda(real(uu,kind=prec),sqrt(m2prec),sqrt(mm2prec))
  END FUNCTION


                     !!!!!!!!!!!!!!!!!!!!!!
                     END MODULE MUE_EM2EM_NFEM
                     !!!!!!!!!!!!!!!!!!!!!!
