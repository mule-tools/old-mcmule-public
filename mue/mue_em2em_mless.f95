                  !!!!!!!!!!!!!!!!!!!!!!
                      MODULE mue_EM2EM_MLESS
                   !!!!!!!!!!!!!!!!!!!!!!

  use functions
  implicit none

contains

  FUNCTION EM2EM_MLESS(p1,p2,p3,p4)
  real(kind=prec) :: p1(4),p2(4),p3(4),p4(4)
  real(kind=prec) :: em2em_mless
  real(kind=prec) :: ss,tt,mm2

  ss = sq(p1+p2)
  tt = sq(p1-p3)
  mm2=sq(p2)

  em2em_mless = (32*alpha**2*Pi**2*(2*mm2**2 - 4*mm2*ss + 2*ss**2 + 2*ss*tt + tt**2))/tt**2
  END FUNCTION

  FUNCTION EM2EML_EE_mless(p1,p2,q1,q2,pole2,pole1)
  real (kind=prec)::p1(4),p2(4),q1(4),q2(4)
  real (kind=prec)::em2eml_ee_mless
  real (kind=prec)::ss,tt,mm2,mm,mu2,logmut
  real (kind=prec),optional::pole2,pole1

  ss=sq(p1+p2);tt=sq(p1-q1)
  mm2=sq(p2)
  mm=sqrt(mm2)
  mu2=musq
  logmut=log(-mu2/tt)

  em2eml_ee_mless = -(((7 + 3*logmut + logmut**2)*(2*mm2**2 - 4*mm2*ss + 2*ss**2 + 2*ss*tt + tt**2))&
                  &/tt**2)
  em2eml_ee_mless = 16*pi*alpha**3*em2eml_ee_mless

  if ((present(pole2)).and.(present(pole1))) then
    pole2 = (-2*(2*(mm2 - ss)**2 + 2*ss*tt + tt**2))/tt**2
    pole1 = -(((3 + 2*logmut)*(2*mm2**2 - 4*mm2*ss + 2*ss**2 + 2*ss*tt + tt**2))/tt**2)
    pole2 = 16*pi*alpha**3*pole2
    pole1 = 16*pi*alpha**3*pole1
  endif

  END FUNCTION EM2EML_EE_mless

  FUNCTION EM2EML_MM_mless(p1,p2,q1,q2,pole)
  real (kind=prec)::p1(4),p2(4),q1(4),q2(4)
  real (kind=prec)::em2eml_mm_mless
  real (kind=prec)::ss,tt,mm2,mm,mu2
  real (kind=prec)::logmum,discbtmm,scalarc0ir6tmm
  real (kind=prec),optional::pole

  ss=sq(p1+p2);tt=sq(p1-q1)
  mm2=sq(p2)
  mm=sqrt(mm2)
  mu2=musq
  logmum=log(mu2/mm2)
  discbtmm=DiscB(tt,mm)
  scalarc0ir6tmm=ScalarC0IR6(tt,mm)

  em2eml_mm_mless = (-32*mm2**4*scalarc0ir6tmm + 8*mm2**3*(4 + 2*logmum + discbtmm*(2 + logmum) + 8*&
                  &scalarc0ir6tmm*ss + 3*scalarc0ir6tmm*tt) - tt*(2*ss**2 + 2*ss*tt + tt**2)*(discb&
                  &tmm*(3 + 2*logmum) + 2*(2 + logmum + scalarc0ir6tmm*tt)) + 4*mm2*(2*ss**2*(4 + 2&
                  &*logmum + discbtmm*(2 + logmum) + 3*scalarc0ir6tmm*tt) + tt**2*(4 + 2*logmum + d&
                  &iscbtmm*(3 + logmum) + 3*scalarc0ir6tmm*tt) + ss*tt*(12 + 6*logmum + discbtmm*(7&
                  & + 4*logmum) + 8*scalarc0ir6tmm*tt)) - 2*mm2**2*(16*scalarc0ir6tmm*ss**2 + 8*ss*&
                  &(4 + 2*logmum + discbtmm*(2 + logmum) + 5*scalarc0ir6tmm*tt) + tt*(discbtmm*(3 +&
                  & 2*logmum) + 2*(2 + logmum + 5*scalarc0ir6tmm*tt))))/(tt**2*(-4*mm2 + tt))
  em2eml_mm_mless = 16*pi*alpha**3*em2eml_mm_mless

  if (present(pole)) then
    pole = (-2*(-2*(2 + discbtmm)*mm2 + (1 + discbtmm)*tt)*(2*mm2**2 - 4*mm2*ss + 2*ss**2 +&
       & 2*ss*tt + tt**2))/(tt**2*(-4*mm2 + tt))
    pole = 16*pi*alpha**3*pole
  endif

  END FUNCTION EM2EML_MM_mless

  FUNCTION EM2EML_EM_mless(p1,p2,q1,q2,pole)
  real (kind=prec)::p1(4),p2(4),q1(4),q2(4)
  real (kind=prec)::em2eml_em_mless
  real (kind=prec)::ss,tt,mm2,mm,mu2
  complex (kind=prec)::logs_c,logs2_c
  real (kind=prec)::logs,logs2,logt,logu,logmum,li2s,li2u,img
  real (kind=prec)::scalarc0tmm
  real (kind=prec),optional::pole

  ss=sq(p1+p2);tt=sq(p1-q1)
  mm2=sq(p2)
  mm=sqrt(mm2)
  mu2=musq
  if(mm2<ss) then
    logs_c = log(abs(mm2/(mm2-ss))) + 1*imag*pi
    logs2_c = logs_c**2
  else
    logs_c=log(mm2/(mm2-ss))
    logs2_c=logs_c**2
  endif
  logs=real(logs_c)
  logs2=real(logs2_c)
  logt=log(-mm2/tt)
  logu=log(-mm2/(mm2-ss-tt))
  logmum=log(mu2/mm2)
  li2s=li2(ss/(-mm2+ss),img)
  li2u=li2(1-mm2/(ss+tt-mm2))
  scalarc0tmm=ScalarC0(tt,mm)

  em2eml_em_mless = (2*li2u*(4*mm2 - 2*ss - tt))/tt - (2*li2s*(2*ss + tt))/tt + (logs2*(2*ss + tt)&
                  &)/tt + (logu**2*(-4*mm2 + 2*ss + tt))/tt + (2*logt*(-2*mm2 + 2*ss + tt))/(4*mm2 &
                  &- tt) + (logt**2*(-2*mm2 + 2*ss + tt))/tt + (4*Pi**2*(-2*mm2 + 2*ss + tt))/(3.*t&
                  &t) + (2*logs*((mm2 - ss)**2 + (mm2 + ss)*tt))/(ss*tt) + (2*logu*((mm2 - ss)**2 +&
                  & (mm2 + ss)*tt))/(tt*(-2*mm2 + ss + tt)) + (2*scalarc0tmm*(2*mm2 - 2*ss - tt)*(8&
                  &*mm2**2 - 8*mm2*tt + tt**2))/((4*mm2 - tt)*tt) + (4*logmum*logs*(2*(mm2 - ss)**2&
                  & + 2*ss*tt + tt**2))/tt**2 - (4*logmum*logu*(2*(mm2 - ss)**2 + 2*ss*tt + tt**2))&
                  &/tt**2 + (2*logs*logt*(4*(mm2 - ss)**2 + 2*ss*tt + tt**2))/tt**2 - (2*logt*logu*&
                  &(4*(mm2 - ss)**2 - 4*mm2*tt + 6*ss*tt + 3*tt**2))/tt**2
  em2eml_em_mless = 16*pi*alpha**3*em2eml_em_mless

  if (present(pole)) then
    pole = (4*(logs - logu)*(2*mm2**2 - 4*mm2*ss + 2*ss**2 + 2*ss*tt + tt**2))/tt**2
    pole = 16*pi*alpha**3*pole
  endif

  END FUNCTION EM2EML_EM_mless

  FUNCTION EM2EMG_EE_MLESS(p1,p2,p3,p4,p5)
  real(kind=prec) :: p1(4),p2(4),p3(4),p4(4),p5(4)
  real(kind=prec) :: em2emg_ee_mless
  real(kind=prec) :: ss,tt,s15,s25,s35,mm2

  ss = sq(p1+p2)
  tt = sq(p2-p4)
  s15 = s(p1,p5)
  s25 = s(p2,p5)
  s35 = s(p3,p5)
  mm2=sq(p2)

  em2emg_ee_mless = (-256*alpha**3*Pi**3*(4*mm2**2*tt + 2*mm2*(s15**2 + s35**2 + 3*s15*tt + 2*s25*tt&
                  & - s35*tt - 4*ss*tt) + tt*(s15**2 + 2*s15*s25 + 2*s25**2 + s35**2 + 4*ss**2 - 2*&
                  &ss*(s15 + 2*s25 + s35 - 2*tt) - 2*s25*tt - 2*s35*tt + 2*tt**2)))/(s15*s35*tt**2)
  END FUNCTION

  FUNCTION EM2EMGL_EEEE_MLESS(p1,p2,p3,p4,p5,pole2,pole1)
  real(kind=prec) :: p1(4),p2(4),p3(4),p4(4),p5(4)
  real(kind=prec), optional :: pole1,pole2
  real(kind=prec) :: em2emgl_eeee_mless
  real(kind=prec) :: ss,tt,s15,s25,s35,mm2,img
  complex(kind=prec) :: logttcplx,logs15cplx,logs35cplx,logs135tcplx
  real(kind=prec) :: logtt,logs15,logs35,logs135t,logs15Sq,logs35Sq
  real(kind=prec) :: logs135tSq,logs15logs135t,logs35logs135t
  real(kind=prec) :: li2s15t,li2s35t,li2s135

  ss = sq(p1+p2)
  tt = sq(p2-p4)
  s15 = s(p1,p5)
  s25 = s(p2,p5)
  s35 = s(p3,p5)
  mm2=sq(p2)

  logttcplx = log(cmplx(-musq/tt))
  logs15cplx = log(cmplx(musq/s15))
  logs35cplx = log(cmplx(-musq/s35))
  logs135tcplx = log(cmplx(-musq/(s15-s35+tt)))

  logtt = real(logttcplx,kind=prec)
  logs15 = real(logs15cplx,kind=prec)
  logs35 = real(logs35cplx,kind=prec)
  logs135t = real(logs135tcplx,kind=prec)
  logs15Sq = real(logs15cplx**2,kind=prec)
  logs35Sq = real(logs35cplx**2,kind=prec)
  logs135tSq = real(logs135tcplx**2,kind=prec)
  logs15logs135t = real(logs15cplx*logs135t)
  logs35logs135t = real(logs35cplx*logs135t)

  li2s15t = li2((s15+tt)/s15,img)
  li2s35t = li2((s35-tt)/s35,img)
  li2s135 = li2((s15-s35)/(s15-s35+tt),img)

  em2emgl_eeee_mless = (128*alpha**4*Pi**2*(-6*li2s35t*s35**2*(mm2**2*(3*s15**2 - 2*s15*s35 + s35**2)*t&
                     &t + mm2*(2*s15**4 + s15*s35*(-4*s25 + 3*s35 + 4*ss - tt)*tt + s15**3*(-4*s35 + 5&
                     &*tt) - 2*s35*tt*(s35*ss + s25*(-s35 + tt)) + s15**2*(4*s35**2 - 10*s35*tt + tt*(&
                     &2*s25 - 6*ss + tt))) + tt*(s15**4 + s15**3*(2*s25 - 2*s35 - ss + tt) + (s35*ss +&
                     & s25*(-s35 + tt))**2 + s15**2*(2*s25**2 + 2*s35**2 + 3*ss**2 - 3*s35*tt + 3*ss*t&
                     &t + 2*tt**2 + s25*(-3*s35 - 2*ss + tt)) + s15*(-2*s25**2*(s35 - tt) - s35*ss*(s3&
                     &5 + 2*ss + tt) + s25*(s35**2 + 4*s35*ss - 2*s35*tt + tt**2)))) + 6*logs35logs135&
                     &t*s35**2*(mm2**2*(3*s15**2 - 2*s15*s35 + s35**2)*tt + mm2*(2*s15**4 + s15*s35*(-&
                     &4*s25 + 3*s35 + 4*ss - tt)*tt + s15**3*(-4*s35 + 5*tt) - 2*s35*tt*(s35*ss + s25*&
                     &(-s35 + tt)) + s15**2*(4*s35**2 - 10*s35*tt + tt*(2*s25 - 6*ss + tt))) + tt*(s15&
                     &**4 + s15**3*(2*s25 - 2*s35 - ss + tt) + (s35*ss + s25*(-s35 + tt))**2 + s15**2*&
                     &(2*s25**2 + 2*s35**2 + 3*ss**2 - 3*s35*tt + 3*ss*tt + 2*tt**2 + s25*(-3*s35 - 2*&
                     &ss + tt)) + s15*(-2*s25**2*(s35 - tt) - s35*ss*(s35 + 2*ss + tt) + s25*(s35**2 +&
                     & 4*s35*ss - 2*s35*tt + tt**2)))) - 3*logs35Sq*s35**2*(mm2**2*(3*s15**2 - 2*s15*s&
                     &35 + s35**2)*tt + mm2*(2*s15**4 + s15*s35*(-4*s25 + 3*s35 + 4*ss - tt)*tt + s15*&
                     &*3*(-4*s35 + 5*tt) - 2*s35*tt*(s35*ss + s25*(-s35 + tt)) + s15**2*(4*s35**2 - 10&
                     &*s35*tt + tt*(2*s25 - 6*ss + tt))) + tt*(s15**4 + s15**3*(2*s25 - 2*s35 - ss + t&
                     &t) + (s35*ss + s25*(-s35 + tt))**2 + s15**2*(2*s25**2 + 2*s35**2 + 3*ss**2 - 3*s&
                     &35*tt + 3*ss*tt + 2*tt**2 + s25*(-3*s35 - 2*ss + tt)) + s15*(-2*s25**2*(s35 - tt&
                     &) - s35*ss*(s35 + 2*ss + tt) + s25*(s35**2 + 4*s35*ss - 2*s35*tt + tt**2)))) - 6&
                     &*li2s15t*s15**2*(mm2**2*(s15**2 - 2*s15*s35 + 3*s35**2)*tt + mm2*(s35**2*(2*s35*&
                     &*2 - 3*s35*tt + tt*(4*s25 - 6*ss + tt)) + s15**2*(4*s35**2 - s35*tt - 2*tt*(ss +&
                     & tt)) + s15*(-4*s35**3 + 10*s35**2*tt - 2*s25*tt**2 + s35*tt*(4*ss + tt))) + tt*&
                     &(-(s25*s35*(s35**2 + 4*s35*ss + tt**2)) + s25**2*(3*s35**2 - 2*s35*tt + tt**2) +&
                     & s15**2*(2*s35**2 - s35*(ss + tt) + (ss + tt)**2) + s35**2*(s35**2 + 3*ss**2 + 3&
                     &*ss*tt + 2*tt**2 - s35*(ss + 2*tt)) + s15*(-(s35*(2*s35**2 + 2*ss**2 - 3*s35*tt &
                     &+ 3*ss*tt + tt**2)) + s25*(3*s35**2 - 3*s35*tt + 2*tt*(ss + tt))))) + 6*logs15lo&
                     &gs135t*s15**2*(mm2**2*(s15**2 - 2*s15*s35 + 3*s35**2)*tt + mm2*(s35**2*(2*s35**2&
                     & - 3*s35*tt + tt*(4*s25 - 6*ss + tt)) + s15**2*(4*s35**2 - s35*tt - 2*tt*(ss + t&
                     &t)) + s15*(-4*s35**3 + 10*s35**2*tt - 2*s25*tt**2 + s35*tt*(4*ss + tt))) + tt*(-&
                     &(s25*s35*(s35**2 + 4*s35*ss + tt**2)) + s25**2*(3*s35**2 - 2*s35*tt + tt**2) + s&
                     &15**2*(2*s35**2 - s35*(ss + tt) + (ss + tt)**2) + s35**2*(s35**2 + 3*ss**2 + 3*s&
                     &s*tt + 2*tt**2 - s35*(ss + 2*tt)) + s15*(-(s35*(2*s35**2 + 2*ss**2 - 3*s35*tt + &
                     &3*ss*tt + tt**2)) + s25*(3*s35**2 - 3*s35*tt + 2*tt*(ss + tt))))) - 3*logs15Sq*s&
                     &15**2*(mm2**2*(s15**2 - 2*s15*s35 + 3*s35**2)*tt + mm2*(s35**2*(2*s35**2 - 3*s35&
                     &*tt + tt*(4*s25 - 6*ss + tt)) + s15**2*(4*s35**2 - s35*tt - 2*tt*(ss + tt)) + s1&
                     &5*(-4*s35**3 + 10*s35**2*tt - 2*s25*tt**2 + s35*tt*(4*ss + tt))) + tt*(-(s25*s35&
                     &*(s35**2 + 4*s35*ss + tt**2)) + s25**2*(3*s35**2 - 2*s35*tt + tt**2) + s15**2*(2&
                     &*s35**2 - s35*(ss + tt) + (ss + tt)**2) + s35**2*(s35**2 + 3*ss**2 + 3*ss*tt + 2&
                     &*tt**2 - s35*(ss + 2*tt)) + s15*(-(s35*(2*s35**2 + 2*ss**2 - 3*s35*tt + 3*ss*tt &
                     &+ tt**2)) + s25*(3*s35**2 - 3*s35*tt + 2*tt*(ss + tt))))) + (6*logs135t*s15*s35*&
                     &(mm2**2*(s15 - s35)**2*(s15**2 + s35**2)*tt + mm2*(-2*s35**3*tt*(s35*ss + s25*(-&
                     &s35 + tt)) + 2*s15**2*s35*(2*s35**3 - 6*s35**2*tt + 2*s25*tt**2 + s35*tt*(s25 - &
                     &2*ss + tt)) + s15**4*(4*s35**2 - 2*tt*(ss + tt)) + s15**3*(-8*s35**3 + 8*s35**2*&
                     &tt - 2*s25*tt**2 + s35*tt*(2*s25 + 4*ss + 3*tt)) + s15*s35**2*tt*(s35*(4*s35 + 4&
                     &*ss - 3*tt) + s25*(-6*s35 + 4*tt))) + tt*(s35**2*(s35*ss + s25*(-s35 + tt))**2 +&
                     & s15**4*(2*s35**2 - 2*s35*(ss + tt) + (ss + tt)**2) + s15*s35*(-4*s25**2*(s35 - &
                     &tt)**2 + s35**2*ss*(-2*s35 - 2*ss + tt) + s25*s35*(2*s35**2 + 6*s35*ss - 5*s35*t&
                     &t - 4*ss*tt + 3*tt**2)) + s15**2*(s25**2*(s35**2 - 4*s35*tt + tt**2) - s25*s35*(&
                     &4*s35**2 + 2*s35*ss - 13*s35*tt + 4*ss*tt + 7*tt**2) + 2*s35**2*(s35**2 + s35*(s&
                     &s - tt) + ss*(ss + tt))) + s15**3*(-(s35*(4*s35**2 + 2*ss**2 + 5*ss*tt + 3*tt**2&
                     & - 2*s35*(ss + 2*tt))) + 2*s25*(s35**2 + tt*(ss + tt) - s35*(ss + 3*tt))))))/(s1&
                     &5 - s35)**2 - 6*li2s135*(mm2**2*(s15**4 - 2*s15**3*s35 + 6*s15**2*s35**2 - 2*s15&
                     &*s35**3 + s35**4)*tt + mm2*(s15*s35**3*(-4*s25 + 3*s35 + 4*ss - tt)*tt - 2*s35**&
                     &3*tt*(s35*ss + s25*(-s35 + tt)) + s15**2*s35**2*(6*s35**2 - 13*s35*tt + 2*tt*(3*&
                     &s25 - 6*ss + tt)) + s15**4*(6*s35**2 - s35*tt - 2*tt*(ss + tt)) + s15**3*(-8*s35&
                     &**3 + 15*s35**2*tt - 2*s25*tt**2 + s35*tt*(4*ss + tt))) + tt*(s35**2*(s35*ss + s&
                     &25*(-s35 + tt))**2 + s15**4*(3*s35**2 - s35*(ss + tt) + (ss + tt)**2) + s15*s35*&
                     &*2*(-2*s25**2*(s35 - tt) - s35*ss*(s35 + 2*ss + tt) + s25*(s35**2 + 4*s35*ss - 2&
                     &*s35*tt + tt**2)) + s15**3*(-(s35*(4*s35**2 + 2*ss**2 + s35*(ss - 4*tt) + 3*ss*t&
                     &t + tt**2)) + s25*(5*s35**2 - 3*s35*tt + 2*tt*(ss + tt))) + s15**2*(s25**2*(5*s3&
                     &5**2 - 2*s35*tt + tt**2) - s25*s35*(4*s35**2 + 6*s35*ss - s35*tt + tt**2) + s35*&
                     &*2*(3*s35**2 + 6*ss**2 + 6*ss*tt + 4*tt**2 - s35*(ss + 5*tt))))) + (3*logs35*s15&
                     &*s35**2*(-(mm2**2*tt*(s15**3 - 2*s15**2*(s35 - tt) + 2*s35**2*(s35 - tt) + s15*s&
                     &35*(-3*s35 + 4*tt))) + mm2*(s15**3*(6*s35**2 - 5*s35*tt + 2*ss*tt) - 2*s15*s35*t&
                     &t*(-3*s25*s35 + 3*s35**2 + 3*s35*ss + 3*s25*tt - 4*s35*tt - 4*ss*tt + tt**2) + 4&
                     &*s35*(s35 - tt)*tt*(s35*ss + s25*(-s35 + tt)) + s15**2*(-4*s35**3 + 17*s35**2*tt&
                     & + 4*s35*(s25 - ss - 4*tt)*tt + 2*tt**2*(-s25 + 2*ss + tt))) + tt*(-2*(s35 - tt)&
                     &*(s35*ss + s25*(-s35 + tt))**2 + s15**3*(3*s35**2 - ss**2 + tt**2 - s35*(ss + 5*&
                     &tt)) + s15*(3*s25**2*(s35 - tt)**2 - 2*s25*(s35 - tt)*(s35**2 + 3*s35*ss - 2*s35&
                     &*tt + tt**2) + s35*ss*(2*s35**2 + 3*s35*ss - 2*tt*(2*ss + tt))) + s15**2*(-2*s35&
                     &**3 - 2*ss*tt*(ss + tt) + s35**2*(-3*ss + 4*tt) + 2*s35*(ss**2 + 3*ss*tt - tt**2&
                     &) + s25*(5*s35**2 + 2*tt*(ss + 2*tt) - s35*(4*ss + 9*tt))))))/(s35 - tt)**2 - (3&
                     &*logtt*s15*s35*tt*(-(s15**7*s35*(ss**2 + s35*(ss - tt) + 2*tt**2)) - 2*s35**2*(s&
                     &35 - tt)*tt**3*(s35*ss + s25*(-s35 + tt))**2 + s15**5*(-2*s35**4*(ss - 4*tt) - 3&
                     &*s25**2*s35*(s35 - tt)**2 + 2*tt**3*(ss + tt)**2 + 2*s25*s35*(s35 - tt)*(s35**2 &
                     &+ 5*s35*ss - 2*s35*tt - 6*ss*tt + tt**2) - 11*s35**3*(ss**2 + 3*tt**2) + 4*s35**&
                     &2*tt*(7*ss**2 + 4*ss*tt + 11*tt**2) - s35*tt**2*(18*ss**2 + 16*ss*tt + 19*tt**2)&
                     &) - mm2**2*(s15 - s35)**2*(s15**5*s35 - 2*s15**4*s35*(s35 - 2*tt) + 2*s35**2*(s3&
                     &5 - tt)*tt**3 + 2*s15**3*(3*s35**3 - 10*s35**2*tt + 9*s35*tt**2 - tt**3) - 2*s15&
                     &**2*(s35**4 - 10*s35**3*tt + 20*s35**2*tt**2 - 13*s35*tt**3 + tt**4) + s15*s35*(&
                     &s35**4 - 4*s35**3*tt + 18*s35**2*tt**2 - 26*s35*tt**3 + 12*tt**4)) - s15**6*s35*&
                     &(s35**2*(-3*ss + 4*tt) + 2*tt*(2*ss**2 - ss*tt + 3*tt**2) - 2*s35*(2*ss**2 - 2*s&
                     &s*tt + 5*tt**2) + s25*(s35**2 + s35*(4*ss - 3*tt) + 2*tt*(-ss + tt))) + 2*s15**4&
                     &*(2*s25**2*s35*(s35 - 2*tt)*(s35 - tt)**2 + tt**4*(ss + tt)**2 - s35**5*(ss + 5*&
                     &tt) - s35*tt**3*(15*ss**2 + 19*ss*tt + 13*tt**2) + s35**4*(8*ss**2 + 8*ss*tt + 2&
                     &5*tt**2) + s35**2*tt**2*(38*ss**2 + 38*ss*tt + 39*tt**2) - s35**3*tt*(32*ss**2 +&
                     & 29*ss*tt + 47*tt**2) + s25*(2*s35**3*(14*ss - 5*tt)*tt - 3*s35**2*(8*ss - 5*tt)&
                     &*tt**2 + s35*(4*ss - 9*tt)*tt**3 + 2*tt**4*(ss + tt) + s35**4*(-8*ss + 2*tt))) -&
                     & s15*s35*(s25**2*(s35 - tt)**2*(s35**4 + 8*s35**2*tt**2 - 14*s35*tt**3 + 8*tt**4&
                     &) - 2*s25*s35*(s35 - tt)*((4*ss - 3*tt)*tt**4 + s35**4*(ss + tt) - 2*s35**3*tt*(&
                     &ss + tt) + s35**2*tt**2*(10*ss + tt) + s35*tt**3*(-16*ss + 3*tt)) + s35**2*(s35*&
                     &*4*(ss**2 + 2*ss*tt + 3*tt**2) + 2*tt**4*(8*ss**2 + 5*ss*tt + 3*tt**2) - 2*s35**&
                     &3*tt*(2*ss**2 + 5*ss*tt + 6*tt**2) - 2*s35*tt**3*(15*ss**2 + 11*ss*tt + 9*tt**2)&
                     & + s35**2*tt**2*(18*ss**2 + 20*ss*tt + 21*tt**2))) + s15**2*(-2*s25**2*(s35 - tt&
                     &)**2*tt*(6*s35**3 - 12*s35**2*tt + 9*s35*tt**2 - tt**3) + s25*s35*(s35**6 + 2*s3&
                     &5**2*(54*ss - 19*tt)*tt**3 + 2*s35**3*tt**2*(-52*ss + tt) + 14*s35*tt**4*(-2*ss &
                     &+ 3*tt) - s35**5*(4*ss + 5*tt) + 2*s35**4*tt*(17*ss + 6*tt) - 2*tt**5*(4*ss + 7*&
                     &tt)) + s35**2*(-(s35**5*(ss + 2*tt)) + 4*tt**4*(7*ss**2 + 7*ss*tt + 3*tt**2) + 2&
                     &*s35**4*(2*ss**2 + 6*ss*tt + 9*tt**2) - 4*s35**3*tt*(7*ss**2 + 10*ss*tt + 14*tt*&
                     &*2) - 2*s35*tt**3*(40*ss**2 + 37*ss*tt + 25*tt**2) + 2*s35**2*tt**2*(38*ss**2 + &
                     &38*ss*tt + 39*tt**2))) + s15**3*(-2*s25**2*(s35 - tt)**2*(2*s35**3 - 8*s35**2*tt&
                     & + 8*s35*tt**2 - tt**3) - 2*s25*(s35 - tt)*(s35**5 + 5*s35**3*(6*ss - tt)*tt + 2&
                     &*tt**4*(ss + tt) - 2*s35**4*(3*ss + tt) - s35*tt**3*(2*ss + 13*tt) + s35**2*tt**&
                     &2*(-28*ss + 17*tt)) + s35*(s35**5*(3*ss + 7*tt) - 11*s35**4*(ss**2 + 2*ss*tt + 4&
                     &*tt**2) - 2*tt**4*(8*ss**2 + 11*ss*tt + 6*tt**2) - 4*s35**2*tt**2*(29*ss**2 + 29&
                     &*ss*tt + 27*tt**2) + 2*s35*tt**3*(40*ss**2 + 43*ss*tt + 28*tt**2) + 2*s35**3*tt*&
                     &(32*ss**2 + 35*ss*tt + 50*tt**2))) + mm2*(s15**7*s35*(7*s35 + 2*ss - 6*tt) + s15&
                     &**6*s35*(4*s25*s35 - 21*s35**2 - 8*s35*ss - 2*s25*tt + 46*s35*tt + 8*ss*tt - 26*&
                     &tt**2) + 4*s35**3*(s35 - tt)*tt**3*(s35*ss + s25*(-s35 + tt)) - 2*s15**5*(-17*s3&
                     &5**4 + 4*s35**2*(7*ss - 15*tt)*tt - 3*s35*(6*ss - 5*tt)*tt**2 + 2*tt**3*(ss + tt&
                     &) + s35**3*(-11*ss + 61*tt) + s25*s35*(5*s35**2 - 11*s35*tt + 6*tt**2)) - 2*s15*&
                     &*4*(15*s35**5 + 2*s35**4*(8*ss - 39*tt) + 2*s35**2*(38*ss - 33*tt)*tt**2 + 3*s35&
                     &*tt**3*(-10*ss + tt) + 2*tt**4*(ss + tt) + s35**3*tt*(-64*ss + 123*tt) + s25*(-8&
                     &*s35**4 + 28*s35**3*tt - 24*s35**2*tt**2 + 4*s35*tt**3 + 2*tt**4)) - 2*s15*s35**&
                     &2*(s25*(s35**5 - 3*s35**4*tt + 12*s35**3*tt**2 - 26*s35**2*tt**3 + 20*s35*tt**4 &
                     &- 4*tt**5) + s35*(-(s35**4*(ss - 2*tt)) + s35**3*(4*ss - 7*tt)*tt + s35*(30*ss -&
                     & 11*tt)*tt**3 + tt**4*(-16*ss + 3*tt) + s35**2*tt**2*(-18*ss + 13*tt))) + s15**2&
                     &*s35*(2*s25*(2*s35**5 - 17*s35**4*tt + 52*s35**3*tt**2 - 54*s35**2*tt**3 + 14*s3&
                     &5*tt**4 + 4*tt**5) + s35*(-5*s35**5 + 8*s35**3*(7*ss - 12*tt)*tt - 4*s35**2*(38*&
                     &ss - 33*tt)*tt**2 + 2*s35*(80*ss - 33*tt)*tt**3 + 4*tt**4*(-14*ss + tt) + s35**4&
                     &*(-8*ss + 30*tt))) + s15**3*(-4*s25*(3*s35**5 - 18*s35**4*tt + 29*s35**3*tt**2 -&
                     & 13*s35**2*tt**3 - 2*s35*tt**4 + tt**5) + s35*(15*s35**5 + 2*s35**4*(11*ss - 50*&
                     &tt) + 8*s35**2*(29*ss - 26*tt)*tt**2 + 2*tt**4*(16*ss + 3*tt) + 2*s35*tt**3*(-80&
                     &*ss + 27*tt) + 2*s35**3*tt*(-64*ss + 117*tt))))))/((s15 - s35)**2*(s35 - tt)**2*&
                     &(s15 + tt)**2) - 3*logs135tSq*(mm2**2*(s15 - s35)**2*(s15**2 + s35**2)*tt + mm2*&
                     &(s15*s35**3*(-4*s25 + 3*s35 + 4*ss - tt)*tt - 2*s35**3*tt*(s35*ss + s25*(-s35 + &
                     &tt)) + s15**2*s35**2*(4*s35**2 - 11*s35*tt + 2*tt*(s25 - 2*ss + tt)) + s15**4*(4&
                     &*s35**2 - s35*tt - 2*tt*(ss + tt)) + s15**3*(-8*s35**3 + 9*s35**2*tt - 2*s25*tt*&
                     &*2 + s35*tt*(4*ss + tt))) + tt*(s35**2*(s35*ss + s25*(-s35 + tt))**2 + s15**4*(2&
                     &*s35**2 - s35*(ss + tt) + (ss + tt)**2) + s15*s35**2*(-2*s25**2*(s35 - tt) - s35&
                     &*ss*(s35 + 2*ss + tt) + s25*(s35**2 + 4*s35*ss - 2*s35*tt + tt**2)) + s15**3*(s2&
                     &5*(3*s35**2 - 3*s35*tt + 2*tt*(ss + tt)) - s35*(4*s35**2 + 2*ss**2 + 3*ss*tt + t&
                     &t**2 - s35*(ss + 4*tt))) + s15**2*(-(s25*s35*(4*s35**2 + 2*s35*ss - 3*s35*tt + t&
                     &t**2)) + s25**2*(3*s35**2 - 2*s35*tt + tt**2) + s35**2*(2*s35**2 + s35*(ss - 3*t&
                     &t) + 2*(ss**2 + ss*tt + tt**2))))) - (3*logs15*s15**2*s35*(mm2**2*tt*(2*s15**3 +&
                     & s35**2*(s35 - 2*tt) + s15**2*(-3*s35 + 2*tt) - 2*s15*s35*(s35 + 2*tt)) + mm2*(s&
                     &15**3*(4*s35**2 - 2*s35*tt - 4*tt*(ss + tt)) - s15**2*(6*s35**3 - 11*s35**2*tt +&
                     & 2*s35*tt*(-3*ss + tt) + 4*tt**2*(s25 + ss + tt)) + 2*s35**2*tt*(s25*(s35 - tt) &
                     &- s35*(ss + tt) + tt*(2*ss + tt)) + s15*tt*(-7*s35**3 - 4*s25*tt**2 + 2*s35*tt*(&
                     &-s25 + 4*ss + tt) + 4*s35**2*(ss + 2*tt))) + tt*(s25**2*(s35 - tt)**2*(s35 + 2*t&
                     &t) - 2*s25*s35*(s35 - tt)*(-tt**2 + s35*(ss + tt)) + 2*s15**3*(s35**2 - s35*(ss &
                     &+ tt) + (ss + tt)**2) + s35**2*ss*(-2*tt*(ss + tt) + s35*(ss + 2*tt)) + s15**2*(&
                     &-3*s35**3 - 3*s35*(ss + tt)**2 + 2*tt*(ss + tt)**2 + s35**2*(3*ss + 7*tt) + 2*s2&
                     &5*(s35**2 - 3*s35*tt + 2*tt*(ss + tt))) + s15*(2*s25**2*(s35 - tt)**2 + s25*(-s3&
                     &5**3 + 3*s35**2*tt + 2*s35*(ss - 3*tt)*tt + 4*tt**2*(ss + tt)) + s35*(s35**2*(ss&
                     & - 4*tt) - 2*tt*(2*ss**2 + 3*ss*tt + tt**2) + s35*(-2*ss**2 + 2*ss*tt + 6*tt**2)&
                     &)))))/(s15 + tt)**2 + (Pi**2*s35**3*(s35 - tt)*tt**2*(s35*ss + s25*(-s35 + tt))*&
                     &*2 + s15**6*(-3*(-6 + Pi**2)*s35**3*tt + Pi**2*tt**2*(ss + tt)**2 - Pi**2*s35*tt&
                     &*(ss**2 + 3*ss*tt + 2*tt**2) + s35**2*(-3*ss**2 + (-3 + Pi**2)*ss*tt + (-21 + 4*&
                     &Pi**2)*tt**2)) - mm2**2*(s15 - s35)*(Pi**2*s35**4*(s35 - tt)*tt**2 + Pi**2*s15*s&
                     &35**3*tt*(s35**2 - 3*s35*tt + 2*tt**2) + s15**5*(3*s35**2 + Pi**2*s35*tt - Pi**2&
                     &*tt**2) + s15**4*(3*s35**3 - 2*Pi**2*s35**2*tt + 3*Pi**2*s35*tt**2 - Pi**2*tt**3&
                     &) + s15**3*s35*(-3*s35**3 + 6*(-12 + Pi**2)*s35**2*tt + 2*(39 - 4*Pi**2)*s35*tt*&
                     &*2 + 2*Pi**2*tt**3) - s15**2*s35**2*(3*s35**3 + 2*Pi**2*s35**2*tt + 2*(39 - 4*Pi&
                     &**2)*s35*tt**2 + 6*(-14 + Pi**2)*tt**3)) + Pi**2*s15*s35**2*(s35 - tt)*tt*(s25**&
                     &2*(s35**3 - 5*s35**2*tt + 5*s35*tt**2 - tt**3) + s25*s35*(2*s35*(4*ss - tt)*tt +&
                     & s35**2*(-2*ss + tt) + tt**2*(-2*ss + tt)) + s35**2*ss*(s35*(ss - tt) - tt*(3*ss&
                     & + tt))) + s15**4*(-(s25**2*(s35 - tt)*(3*s35**3 + (-39 + 5*Pi**2)*s35**2*tt - 2&
                     &*Pi**2*s35*tt**2 + Pi**2*tt**3)) - s25*(s35 - tt)*(2*Pi**2*tt**3*(ss + tt) - 2*P&
                     &i**2*s35*tt**2*(ss + 3*tt) + s35**3*(6*ss + 33*tt - 9*Pi**2*tt) + 3*s35**2*tt*(3&
                     &0*ss - 2*Pi**2*ss + 5*tt + 3*Pi**2*tt)) + s35*((18 - 7*Pi**2)*s35**4*tt - Pi**2*&
                     &tt**3*(3*ss**2 + 5*ss*tt + 2*tt**2) + s35**2*tt*(-8*(-9 + Pi**2)*ss**2 - 9*(-3 +&
                     & Pi**2)*ss*tt + 2*(48 - 13*Pi**2)*tt**2) + s35*tt**2*((-78 + 11*Pi**2)*ss**2 + 2&
                     &*(-15 + 7*Pi**2)*ss*tt + 3*(-11 + 4*Pi**2)*tt**2) + s35**3*(6*ss**2 + 6*ss*tt + &
                     &(-78 + 23*Pi**2)*tt**2))) + s15**3*(-(s25**2*(s35 - tt)*(6*s35**4 + (36 - 7*Pi**&
                     &2)*s35**3*tt + 3*(-10 + 3*Pi**2)*s35**2*tt**2 - 3*Pi**2*s35*tt**3 + Pi**2*tt**4)&
                     &) + s25*s35*(-(Pi**2*tt**4*(2*ss + 3*tt)) - 2*s35**2*tt**2*(78*ss - 8*Pi**2*ss +&
                     & 39*tt + 9*Pi**2*tt) + s35*tt**3*(96*ss - 4*Pi**2*ss + 60*tt + 9*Pi**2*tt) + s35&
                     &**4*(6*ss + (3 - 5*Pi**2)*tt) + s35**3*tt*(-10*(-6 + Pi**2)*ss + (15 + 17*Pi**2)&
                     &*tt)) + s35**2*tt*(3*(-6 + Pi**2)*s35**4 + 3*s35**3*(14*ss - 5*(-5 + Pi**2)*tt) &
                     &- 2*s35*tt*((-78 + 8*Pi**2)*ss**2 + 2*(-39 + 4*Pi**2)*ss*tt + 9*(-7 + Pi**2)*tt*&
                     &*2) + tt**2*((-84 + 8*Pi**2)*ss**2 + 3*(-28 + 3*Pi**2)*ss*tt + (-42 + 5*Pi**2)*t&
                     &t**2) + s35**2*(8*(-9 + Pi**2)*ss**2 + (-117 + 7*Pi**2)*ss*tt + (-141 + 25*Pi**2&
                     &)*tt**2))) + mm2*(-2*Pi**2*s35**4*(s35 - tt)*tt**2*(s35*ss + s25*(-s35 + tt)) + &
                     &s15**6*(-6*(-7 + Pi**2)*s35**3 - 2*Pi**2*tt**2*(ss + tt) + Pi**2*s35*tt*(2*ss + &
                     &tt) + s35**2*(6*ss + (-39 + 7*Pi**2)*tt)) + Pi**2*s15*s35**3*(s35 - tt)*tt*(2*s2&
                     &5*(s35**2 - 4*s35*tt + tt**2) - s35*(2*s35*ss - 3*s35*tt - 6*ss*tt + tt**2)) + s&
                     &15**2*s35**3*(s35**2*tt*(Pi**2*(6*ss - 26*tt) + 75*tt) + tt**3*(8*(-21 + 2*Pi**2&
                     &)*ss - 3*Pi**2*tt) + s35**3*(6*ss - 39*tt + 9*Pi**2*tt) + 2*s35*tt**2*((78 - 11*&
                     &Pi**2)*ss + 2*(-9 + 5*Pi**2)*tt) - 6*s25*(s35 - tt)*(s35**2 + (1 + Pi**2)*s35*tt&
                     & - 2*(-6 + Pi**2)*tt**2)) + s15**5*(2*(-15 + 7*Pi**2)*s35**4 + 12*(16 - 3*Pi**2)&
                     &*s35**3*tt - 2*Pi**2*tt**3*(ss + tt) + 4*Pi**2*s35*tt**2*(2*ss + tt) + 2*s25*(3*&
                     &s35**3 + Pi**2*s35*tt**2 - Pi**2*tt**3) + s35**2*tt*(-165*tt + Pi**2*(-6*ss + 20&
                     &*tt))) + s15**4*(2*s25*(s35 - tt)*(3*s35**3 - 3*(-15 + Pi**2)*s35**2*tt - Pi**2*&
                     &s35*tt**2 + Pi**2*tt**3) + s35*((30 - 14*Pi**2)*s35**4 + 3*Pi**2*tt**3*(2*ss + t&
                     &t) + s35**2*tt*(16*(-9 + Pi**2)*ss + (321 - 59*Pi**2)*tt) - 2*s35**3*(6*ss + (11&
                     &1 - 28*Pi**2)*tt) + 2*s35*tt**2*((78 - 11*Pi**2)*ss + (-66 + 7*Pi**2)*tt))) + s1&
                     &5**3*s35*(-2*s25*(3*s35**4 - 5*(-6 + Pi**2)*s35**3*tt + 2*(-39 + 4*Pi**2)*s35**2&
                     &*tt**2 - 2*(-24 + Pi**2)*s35*tt**3 - Pi**2*tt**4) + s35*(6*(-7 + Pi**2)*s35**4 -&
                     & 36*(-3 + Pi**2)*s35**3*tt + tt**3*(-8*(-21 + 2*Pi**2)*ss + Pi**2*tt) + 8*s35*tt&
                     &**2*((-39 + 4*Pi**2)*ss + (21 - 4*Pi**2)*tt) + s35**2*tt*(-16*(-9 + Pi**2)*ss + &
                     &(-231 + 61*Pi**2)*tt)))) - s15**2*s35*(s25**2*(s35 - tt)*(3*s35**4 + 3*(1 + Pi**&
                     &2)*s35**3*tt + (30 - 11*Pi**2)*s35**2*tt**2 + (12 + 5*Pi**2)*s35*tt**3 - Pi**2*t&
                     &t**4) - s25*s35*(s35 - tt)*(-2*Pi**2*tt**4 + s35**3*(6*ss + (3 + Pi**2)*tt) + 4*&
                     &s35*tt**2*(-3*(-6 + Pi**2)*ss + (12 + Pi**2)*tt) + s35**2*tt*(6*(1 + Pi**2)*ss -&
                     & (3 + 7*Pi**2)*tt)) + s35**2*(s35*tt**2*((78 - 11*Pi**2)*ss**2 + 2*(63 - 4*Pi**2&
                     &)*ss*tt - 9*(-9 + Pi**2)*tt**2) + s35**3*(3*ss**2 + (3 + Pi**2)*ss*tt - 3*(-7 + &
                     &Pi**2)*tt**2) + tt**3*((-84 + 8*Pi**2)*ss**2 + 7*(-12 + Pi**2)*ss*tt + 2*(-21 + &
                     &2*Pi**2)*tt**2) + s35**2*tt*(-15*tt*(3*ss + 4*tt) + Pi**2*(3*ss**2 + 8*tt**2))))&
                     & + s15**5*(s25*((-39 + 8*Pi**2)*s35**2*tt**2 + 2*Pi**2*tt**3*(ss + tt) - Pi**2*s&
                     &35*tt**2*(2*ss + 5*tt) + s35**3*(-6*ss + 39*tt - 5*Pi**2*tt)) + tt*((-18 + 7*Pi*&
                     &*2)*s35**4 - 4*Pi**2*s35*tt*(ss + tt)**2 + Pi**2*tt**2*(ss + tt)**2 - 3*s35**3*(&
                     &14*ss + (-11 + 5*Pi**2)*tt) + s35**2*(15*(3*ss - tt)*tt + Pi**2*(3*ss**2 + 6*ss*&
                     &tt + 11*tt**2)))))/((s15 - s35)*(s35 - tt)*(s15 + tt))))/(3.*s15**3*s35**3*tt**2&
                     &)

  if(present(pole2) .and. present(pole1)) then
    pole2 = (256*alpha**4*Pi**2*(4*mm2**2*tt + 2*mm2*(s15**2 + s35**2 + 3*s15*tt - s35*tt + &
        &2*(s25 - 2*ss)*tt) + tt*(s15**2 + 2*s15*s25 + 2*s25**2 + s35**2 + 4*ss**2 - 2*ss&
        &*(s15 + 2*s25 + s35 - 2*tt) - 2*s25*tt - 2*s35*tt + 2*tt**2)))/(s15*s35*tt**2)
    pole1 = (128*alpha**4*(3 + 2*logs135t)*Pi**2*(4*mm2**2*tt + 2*mm2*(s15**2 + s35**2 + 3*s&
        &15*tt - s35*tt + 2*(s25 - 2*ss)*tt) + tt*(s15**2 + 2*s15*s25 + 2*s25**2 + s35**2&
        & + 4*ss**2 - 2*ss*(s15 + 2*s25 + s35 - 2*tt) - 2*s25*tt - 2*s35*tt + 2*tt**2)))/&
        &(s15*s35*tt**2)
  endif
  END FUNCTION
                          !!!!!!!!!!!!!!!!!!!!!!
                          END MODULE mue_EM2EM_MLESS
                          !!!!!!!!!!!!!!!!!!!!!!
