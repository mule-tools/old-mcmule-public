                 !!!!!!!!!!!!!!!!!!!!!!!!!
                      MODULE MUE_TEST
                 !!!!!!!!!!!!!!!!!!!!!!!!!
  use testtools

contains
  SUBROUTINE TESTMUE(ONLYFAST)
  logical onlyfast

  call testmueff
  call testmuematel(onlyfast)
  call testmuenfem
  call testmuesoftn1
  call testmuentsoftn1
  call testmuesoftn2
  if(.not.onlyfast) then
    call testmuespeed
    call testmuevegas
  endif
  call testmuepointwise

  call testmupmatel
  call testmupsoftn1
  call testmupsoftn2
  call testmupntsoftn1
  if(.not.onlyfast) then
     call testmupspeed
     call testmupvegas
  endif
  call testmuppointwise

  call testmupairsoftn1
  call testmupairsoftn2
  END SUBROUTINE TESTMUE

  SUBROUTINE TESTMUEFF
  use ff_heavyquark
  implicit none
  real(kind=prec) :: x
  complex(kind=prec) :: F1a1(-1:1), F1a2(-2:0), F2a1(-1:1), F2a2(-2:0)
  call blockstart("mu-e: hqff")

  x = 0.3_prec

  call hqff(x, 0., oneloopF1=F1a1, oneloopF2=F2a1, twoloopF1=F1a2, twoloopF2=F2a2)
  call check("F1 a^1 ep^-1", real(F1a1(-1),prec),  0.442121271115681573581091)
  call check("F1 a^1 ep^ 0", real(F1a1( 0),prec),  0.234594784829855224209627)
  call check("F1 a^1 ep^+1", real(F1a1(+1),prec),  1.274494642665142496985853)
  call check("F1 a^2 ep^-2", real(F1a2(-2),prec),  0.097735609186473004849643)
  call check("F1 a^2 ep^-1", real(F1a2(-1),prec),  0.103719344466085404336585)
  call check("F1 a^2 ep^ 0", real(F1a2( 0),prec), -1.344874879032958801331997)

  call check("F2 a^1 ep^ 0", real(F2a1( 0),prec),  0.793828222632485269861151)
  call check("F2 a^1 ep^+1", real(F2a1(+1),prec),  2.202309677167230247514140)
  call check("F2 a^2 ep^-1", real(F2a2(-1),prec),  0.350968342837776651346997)
  call check("F2 a^2 ep^ 0", real(F2a2( 0),prec), -0.232631299413720632476900)

  call blockend(10)
  END SUBROUTINE



  SUBROUTINE TESTMUEMATEL(onlyfast)
  use olinterface
  use mue_mp2mpgl_pvred, only: em2emgl_eeee_pvred
  use mue_mp2mpgl_coll, only: em2emgl_eeee_coll
  use mue_em2emgl_lbk, only: em2emgl_lbk, em2emgl_lbk_eeee, em2emgl_lbk11_eeee, em2emgl_lbk_mmmm
  use mue_mp2mpgg_nts
  use mue
  use olinterface
  implicit none
  logical onlyfast
  real (kind=prec) :: x(2),y(5),y8(8)
  real (kind=prec) :: q1(4),q2(4),q3(4),q4(4),q1Rest(4),q2Rest(4),q3Rest(4),q4Rest(4),q5Rest(4)
  real (kind=prec) :: single, finite, lin, pole, pole1, pole2, sing
  real (kind=prec) :: weight, pol0(4), n1(4), n2(4)
  real (kind=prec) :: olTree, olSingle, olFinite
  real (kind=prec) :: sing_mm, sing_ee, sing_em, sing_emc
  real (kind=prec) :: finite2
  real (kind=prec) :: tot_sh, tot_hs, tot_ss, ggmixd, ggmixd_ol
  real (kind=prec) :: exact_em31,exact_em22,exact_em13,exact_em,exact_em2
  integer ido

  pol1 = (/ 0.2, 0.1, 0.8, 0.2 /)
  pol2 = (/ 0.4, 0.3, 0.5, 0.2 /)
  pol0 = (/0._prec, 0._prec, 0._prec, 0._prec/)

  call blockstart("mu-e matrix elements")
  call initflavour("mu-eOld")

  musq = Mm**2
  xieik1 = 0.79983638976286975
  xieik2 = 0.3
  nel = 1
  nmu = 1
  ntau = 1
  nhad = 1

  print*, "----------------------------------------------"
  print*, "   Test Born and NLO Matrixelements"
  print*, "----------------------------------------------"
  call initflavour("mu-eOld", mys=100000._prec)
  x = (/0.75,0.5/)
  call psx2(x,p1,me,p2,me,q1,mm,q2,mm,weight)
  call check("ee2mm0" ,ee2mm(p1,p2,q1,q2), 228.431299598518,threshold=2e-8)
  call check("ee2mml fin",ee2mml(p1,p2,q1,q2,pole), 5342.29166320650,threshold=2e-8)
  call check("ee2mml sin",pole, 934.984197022799,threshold=2e-8)
  call check("ee2mmf",ee2mmf(p1,p2,q1,q2), 1529.1135080339554,threshold=2e-8)
  call check("pepe2mm", pepe2mm(p1,pol1,p2,pol2,q1,q2), 0.25*955.480090161492, threshold=2e-8)

  call check("pepe2mml fin", pepe2mml(p1,pol1,p2,pol2,q1,q2,sing),5461.443064725386,threshold=2e-8)
  call check("pepe2mml sin", sing, 949.6122016961215, threshold=2e-8)
  call check("pepe2mml_mm fin", pepe2mml_mm(p1,pol1,p2,pol2,q1,q2,sing_mm),439.36476553122486,threshold=2e-8)
  call check("pepe2mml_mm sin", sing_mm,76.26641795759647,threshold=2e-8)
  call check("pepe2mml_ee fin", pepe2mml_ee(p1,pol1,p2,pol2,q1,q2,sing_ee),5143.622209435307,threshold=2e-8)
  call check("pepe2mml_ee sin", sing_ee,901.4441523988382 ,threshold=2e-8)
  call check("pepe2mml_em fin", pepe2mml_em(p1,pol1,p2,pol2,q1,q2,sing_em),3907.9917970750903,threshold=2e-8)
  call check("pepe2mml_em sin", sing_em,952.6269142460902,threshold=2e-8)
  call check("pepe2mml_emc fin", pepe2mml_emc(p1,pol1,p2,pol2,q1,q2,sing_emc),-4029.5357073162368 ,threshold=2e-8)
  call check("pepe2mml_emc sin", sing_emc,-980.7252829064034, threshold=2e-8)

  call check("pepe2mml pol vs. unpol fin", pepe2mml(p1,pol0,p2,pol0,q1,q2,sing),ee2mml(p1,p2,q1,q2),threshold=2e-8)
  call check("pepe2mml pol vs. unpol sin", sing, pole, threshold=2e-8)
  call check("pepezmm", pepezmm(p1,pol1,p2,pol2,q1,q2),-0.000011298908488377291,threshold=2e-8)
  call check("pepezmmx", pepezmmx(p1,pol1,p2,pol2,q1,q2),-0.00002260035541788531*0.5,threshold=2e-8)
  call initflavour("muone+", mys=100000._prec)
  call check("emzemx",emzemx(p1,p2,q1,q2),-0.00367123331227382,threshold=2e-8)
  call check("pepe2mmll", pepe2mmll_eeee(p1,pol1,p2,pol2,q1,q2), 129039.80962070714, threshold=2e-8)
  call check("pepe2mmff", pepe2mmff_eeee(p1,pol1,p2,pol2,q1,q2), 996.3944834640097, threshold=2e-8)

  call initflavour("mu-eOld", mys=10e3**2)
  x = (/0.75,0.3/)
  call psx2(x,p1,me,p2,me,q1,mm,q2,mm,weight)

  Nhad = 0
  Ntau = 0
  call check("ee2mml/a", &
    pepe2mml(p1,pol0,p2,pol0,q1,q2)+pepe2mma(p1,pol0,p2,pol0,q1,q2), &
    1122.36859431186661150, threshold=1e-3)

  call check("pepeZmma", pepeZmmax(p1,pol0,p2,pol0,q1,q2), 5.296955877842147/2, threshold=2e-8)
  call check("eeZmmLx", pepezmmlx(p1,pol0,p2,pol0,q1,q2), 10.244190185482138, threshold=5e-4)

  call check("eeZmml/a", &
    pepeZmmlx(p1,pol0,p2,pol0,q1,q2)+pepeZmmax(p1,pol0,p2,pol0,q1,q2), &
    12.892733842934557, threshold=1e-3)
  call check("pepeZmml/a", &
    pepeZmmlx(p1,pol1,p2,pol2,q1,q2)+pepeZmmax(p1,pol1,p2,pol2,q1,q2), &
    11.27891289757456, threshold=1e-3)
  ! call check("pepeZmmLx", pepezmmlx(p1,pol1,p2,pol2,q1,q2),40.63891236052386,threshold=2e-3)


  nel = 1
  nmu = 1
  ntau = 1
  nhad = 1
  call initflavour("mu-eOld", mys=40000.)
  x = (/0.75,0.5/)
  call psx2(x,p1,me,p2,mm,q1,me,q2,mm,weight)
  q1Rest = boost_rf(p1,p1)
  q2Rest = boost_rf(p1,p2)
  q3Rest = boost_rf(p1,q1)
  q4Rest = boost_rf(p1,q2)
  !print*,"q1Rest",q1Rest
  !print*,"q2Rest",q2Rest
  !print*,"q3Rest",q3Rest
  !print*,"q4Rest",q4Rest
  !print*,"theta_e",acos(cos_th(q2Rest,q3Rest))
  !print*,"theta_m", acos(cos_th(q2Rest,q4Rest))
  !print*,"E_e", q3Rest(4)
  !print*,"E_m",q4Rest(4)
  !print*,"s:", sq(p1+p2)
  !print*,"t:", sq(p1-q1)
  !print*,"u:", sq(p1-q2)
  call check("born" ,em2em       (p1,p2,q1,q2           ), 2746.780043847221,threshold=2e-8)
  call check("NLOEE",em2eml_ee(p1,p2,q1,q2             ), 53962.06218413274,threshold=2e-8)
  call check("NLOMM",em2eml_mm(p1,p2,q1,q2           ), 234.43246950023965,threshold=2e-8)
  call check("NLOEM",em2eml_em(p1,p2,q1,q2           ),-3771.205716336125,threshold=2e-8)

  call check("a",      em2em_a     (p1,p2,q1,q2),  5372.13218519927,    threshold=2e-8)
  call check("aa",     em2em_aa    (p1,p2,q1,q2),  10021.17653897321,   threshold=4e-8)
  call check("alee",     em2em_alee  (p1,p2,q1,q2),  105538.60389675523,   threshold=3e-8)
  call check("nfee",     em2em_nfee_interpol  (p1,p2,q1,q2),  -9048.728983123658,    threshold=1e-7)

  call check("em2em_mless", em2em_mless(p1,p2,q1,q2),2746.868077501856)

  print*, "----------------------------------------------"
  print*, "   Test 2 loop matrixelement"
  print*, "----------------------------------------------"
  musq = me**2
  call check("2loop",     em2emll_eeee  (p1,p2,q1,q2), 2.4105736994236756e7 / 4. / pi**2,    threshold=2e-4)
  musq = mm**2
  call check("2loop",     em2emll_eeee  (p1,p2,q1,q2), 4.788164395939787e7  / 4. / pi**2,    threshold=2e-4)
  xieik2 = 0.5_prec
  call check("2loop-ff",     em2emff_eeee  (p1,p2,q1,q2), 6467.095657687751,    threshold=5e-8)

  call initflavour("mu-e", mys=100000._prec)
  x = (/0.75,0.7/)
  musq = 0.04*mm**2
  call psx2(x,p1,me,p2,mm,q1,me,q2,mm,weight)
  call check("em2eml0", em2eml0(p1, p2, q1, q2), -14443.856226632397,threshold=1e-7)
  if(.not.onlyfast) &
  call check("em2emll0", em2emll0(p1, p2, q1, q2), 86442.61649828177,threshold=1e-7)
  call check("em2emffz", em2emffz(p1, p2, q1, q2), 13383.220059938874,threshold=1e-7)
  call check("em2emffz_eeee", em2emffz_eeee(p1, p2, q1, q2), 6853.3493272154592,threshold=1e-7)

  x = (/0.2,0.5/)
  musq = (0.1*mm)**2
  call psx2(x,p1,me,p2,mm,q1,me,q2,mm,weight)
  call check("em2eml0", em2eml0(p1, p2, q1, q2), -7481.607637299625)
  if(.not.onlyfast) then
  call check("em2emll0", em2emll0(p1, p2, q1, q2), 70591.47473213251,1e-8)
  call check("em2emffz", em2emffz(p1, p2, q1, q2), 4861.213357372568,1e-7)
  call check("em2emffz_mixd", em2emffz_mixd(p1, p2, q1, q2), 3039.9763925041407,1e-7)
  endif

  x = (/0.4,0.99/)
  musq = (0.5*mm)**2
  call psx2(x,p1,me,p2,mm,q1,me,q2,mm,weight)
  call check("em2eml0", em2eml0(p1, p2, q1, q2), -1.680031966714869e7)
  if(.not.onlyfast) then
  call check("em2emll0", em2emll0(p1, p2, q1, q2), 5.479798461102545e7,1e-8)
  call check("em2emffz", em2emffz(p1, p2, q1, q2), 3.288647384060813e6,1e-7)
  call check("em2emffz_mixd", em2emffz_mixd(p1, p2, q1, q2), -459155.05167752155,1e-6)
  endif

  finite2 = em2emffz_e3m1(p1, p2, q1, q2) &
          + em2emffz_e2m2(p1, p2, q1, q2) &
          + em2emffz_e1m3(p1, p2, q1, q2)
  call check("em2emFFz_MIXD-split_S", finite2, em2emffz_mixd(p1, p2, q1, q2))

  ! call psx2(x,p1,me,p2,me,q1,mm,q2,mm,weight)
  ! call check("ee2mmll0", em2emll0(p1, -q1, -p2, q2), -109.3319196665665)

  call initflavour("mu-eOld", mys=40000.)
  musq = Mm**2
  x = (/0.75,0.999/)
  call psx2(x,p1,me,p2,mm,q1,me,q2,mm,weight)
  !print*,"s:", sq(p1+p2)
  !print*,"t:", sq(p1-q1)
  !print*,"u:", sq(p1-q2)
  ! These are a bit too lax
  call check("born:" , em2em       (p1,p2,q1,q2           ),1.21424E9            ,threshold=2e-5)
  call check("NLOEE:", em2eml_ee(p1,p2,q1,q2           ),1.2665657434343555E10,threshold=2e-8)
  call check("NLOMM:", em2eml_mm(p1,p2,q1,q2           ),179903.10339936582   ,threshold=2e-5)
  call check("NLOEM:", em2eml_em(p1,p2,q1,q2           ),-7.279726741301247E7 ,threshold=2e-8)

  call check("a",      em2em_a   (p1,p2,q1,q2),  7.16812818671185E8,   threshold=4e-8)
  call check("aa",     em2em_aa  (p1,p2,q1,q2),  7.930934218163091E8,   threshold=4e-8)
  call check("alee",     em2em_alee(p1,p2,q1,q2),  7.4770015363548765E9,   threshold=4e-8)

  call psx2(x,p1,me,p2,mm,q1,me,q2,mm,weight)
  call check("NLOEEmless:", em2eml_ee_mless(p1,p2,q1,q2           ),-1.263371163362475e10,threshold=2e-8)
  call check("NLOMMmless:", em2eml_mm_mless(p1,p2,q1,q2           ), 179906.8838359179   ,threshold=2e-5)
  call check("NLOEMmless:", em2eml_em_mless(p1,p2,q1,q2           ),-7.271223966544222e7   ,threshold=2e-8)

  print*, "----------------------------------------------"
  print*, "   Test Real Matrixelement"
  print*, "----------------------------------------------"
  call initflavour("mu-eOld", mys=100000._prec)
  y = (/0.3,0.6,0.8,0.4,0.9/)
  call psx3_fks(y,p1,me,p2,me,q1,mm,q2,mm,q3,weight)
  call check("ee2mmg", ee2mmg(p1,p2,q2,q1,q3), 14.7004376744835,threshold=2e-8)
  call check("ee2mmg", ee2mmg(p1,p2,q1,q2,q3), 2.8166451609362255,threshold=2e-8)

  call check("pepe2mmg", pepe2mmg(p1,pol1,p2,pol2,q1,q2,q3),1.54367481787137,threshold=2e-8)
  call check("pepezmmgx", pepezmmgx(p1,pol1,p2,pol2,q1,q2,q3),2.7391241826982986e-6,threshold=2e-8)

  call check("ee2mmg pol vs. unpol",&
              ee2mmg(p1,p2,q1,q2,q3),pepe2mmg(p1,pol0,p2,pol0,q1,q2,q3),threshold=2e-8)

  call initflavour("mu-eOld", mys=40000.)
  y = (/0.3,0.6,0.8,0.4,0.9/)
  call psx3_fks(y,p1,me,p2,mm,q1,me,q2,mm,q3,weight)
  !print*,"s12:",s(p1,p2)
  !print*,"s13:",s(p1,q1)
  !print*,"s14:",s(p1,q2)
  !print*,"s15:",s(p1,q3)
  !print*,"s23:",s(p2,q1)
  !print*,"s24:",s(p2,q2)
  !print*,"s25:",s(p2,q3)
  !print*,"s34:",s(q1,q2)
  !print*,"s35:",s(q1,q3)
  !print*,"s45:",s(q2,q3)
  call check("MatrixelementTestEE", em2emg_ee(p1,p2,q1,q2,q3,lin), 593.7420767086396,   threshold=1e-8)
  call check("MatrixelementTestMM", em2emg_mm(p1,p2,q1,q2,q3    ), 68.01192438278314,   threshold=1e-8)
  call check("MatrixelementTestEM", em2emg_em(p1,p2,q1,q2,q3    ),-343.3094424320352,   threshold=1e-8)
  call check("MatrixelementTestEElin", lin,-66.72551154843468)
  call check("vp3",             em2emg_aee(p1,p2,q1,q2,q3),  863.0115655417991,   threshold=2e-8)
  finite = em2emgl_lbk(p1,p2,q1,q2,q3,pole)
  call check("MatrixelementTestRV_lbk-fin", finite, 6933.082010957219, threshold=1e-8)
  call check("MatrixelementTestRV_lbk-sin",   pole, 928.5833508291142, threshold=1e-8)
  finite = em2emgl_lbk_eeee(p1,p2,q1,q2,q3,pole)
  call check("MatrixelementTestRV_eeee_lbk-fin", finite, 12930.78985225921, threshold=1e-8)
  call check("MatrixelementTestRV_eeee_lbk-sin",   pole, 1721.805826839798, threshold=1e-8)
  finite = em2emgl_lbk11_eeee(p1,p2,q1,q2,q3,pole)
  call check("MatrixelementTestRV_eeee_lbk11-fin", finite, 12930.78985225921, threshold=1e-8)
  call check("MatrixelementTestRV_eeee_lbk11-sin",   pole, 1721.805826839798, threshold=1e-8)

  finite = em2emgl_lbk_mmmm(p1,p2,q1,q2,q3,pole)
  call check("MatrixelementTestRV_mmmm_lbk-fin", finite, 0.828332801170105, threshold=1e-8)
  call check("MatrixelementTestRV_mmmm_lbk-sin",   pole, 1.066117271864752, threshold=1e-8)
  finite = em2emgl_lbk_mixd(p1,p2,q1,q2,q3,pole)
  call check("MatrixelementTestRV_mixd_lbk-fin", finite, -5998.536174103160, threshold=1e-8)
  call check("MatrixelementTestRV_mixd_lbk-sin",   pole, -794.2885932825481, threshold=1e-8)
  finite = (em2emgl_lbk_e3m1(p1,p2,q1,q2,q3,pole) + &
         &  em2emgl_lbk_e2m2(p1,p2,q1,q2,q3,pole) + &
         &  em2emgl_lbk_e1m3(p1,p2,q1,q2,q3,pole)   )/em2emgl_lbk_mixd(p1,p2,q1,q2,q3,pole)
  call check("MatrixelementTestRV_mixd_lbk-split", finite, 1._prec, threshold=1e-8)

  call check("em2emg_ee_mless",em2emg_ee_mless(p1,p2,q1,q2,q3),593.7667904533329)
  call check("em2emg_ee_nts",em2emg_ee_nts(p1,p2,q1,q2,q3),723.3293270122617)

  print*, "----------------------------------"
  print*, "   Test RV   Matrixelement"
  print*, "----------------------------------"
  q1Rest = boost_rf(p1,p1)
  q2Rest = boost_rf(p1,p2)
  q3Rest = boost_rf(p1,q1)
  q4Rest = boost_rf(p1,q2)
  q5Rest = boost_rf(p1,q3)
  finite=EM2EMGl_eeee_old(p1, p2, q1, q2, q3,single)
  call check("finite:",finite,11075.183615627779,threshold=2e-9)
  call check("single:",single,1508.0369816372852,threshold=2e-9)

  finite=EM2EMGl_eeee(p1, p2, q1, q2, q3,single)+lin/em2emg_ee(p1,p2,q1,q2,q3)*single
  call check("FDFfin:",finite,11075.183615627779,threshold=2e-9)
  call check("FDFsin:",single,1508.0369816372852,threshold=2e-9)

  finite=EM2EMGl_eeee_pvred(p1, p2, q1, q2, q3,single)+lin/em2emg_ee(p1,p2,q1,q2,q3)*single
  call check("Red fin:",finite,11075.183615627779,threshold=2e-9)
  call check("Red sin:",single,1508.0369816372852,threshold=2e-9)

  finite=EM2EMGl_eeee_Coll(p1, p2, q1, q2, q3,single)+lin/em2emg_ee(p1,p2,q1,q2,q3)*single
  call check("Coll fin:",finite,11075.183615627779,threshold=2e-9)
  call check("Coll sin:",single,1508.0369816372852,threshold=2e-9)

  finite=EM2EMGl_eeee_fullcoll(p1, p2, q1, q2, q3,single)+lin/em2emg_ee(p1,p2,q1,q2,q3)*single
  call check("FullColl fin:",finite,11075.183615627779,threshold=2e-9)
  call check("FullColl sin:",single,1508.0369816372852,threshold=2e-9)

  call check("em2emgl_eeee_mless fin",em2emgl_eeee_mless(p1,p2,q1,q2,q3,pole2,pole1),-1103.3709957464305)
  call check("em2emgl_eeee_mless pole2",pole2,-189.0018394889151)
  call check("em2emgl_eeee_mless pole1",pole1,-601.8127948653366)

  print*, "----------------------------------"
  print*, "   FULL MU-E Matrixelement"
  print*, "----------------------------------"
  finite = EM2EMGl(p1, p2, q1, q2, q3, single)
  call check('em2emgl 1/ep', single, 803.43320530875133)
  call check('em2emgl ep^0', finite, 5886.6755462005740)

  call check("em2emglEE", em2emgl_eeee(p1, p2, q1, q2, q3), 1.1244658788166011e4)
  call check("em2emglMM", em2emgl_mmmm(p1, p2, q1, q2, q3), 7.1549646907659508e0)
  call check("em2emglEM", em2emgl_mixd(p1, p2, q1, q2, q3),-5.3651382066562028e3)
  finite2 = em2emgl_eeee(p1, p2, q1, q2, q3) &
          + em2emgl_mmmm(p1, p2, q1, q2, q3) &
          + em2emgl_mixd(p1, p2, q1, q2, q3)

  call openloops("em2emgEM",   p1, p2, q1, q2, q3, fin=exact_em, reload=.true.)
  call openloops("em2emgEM31", p1, p2, q1, q2, q3, fin=exact_em31, reload=.true.)
  call openloops("em2emgEM22", p1, p2, q1, q2, q3, fin=exact_em22, reload=.true.)
  call openloops("em2emgEM13", p1, p2, q1, q2, q3, fin=exact_em13, reload=.true.)
  exact_em2 = exact_em31+exact_em22+exact_em13

  call check("EE+MM+EM=full", finite2, finite)
  call check("qe3qm1+qe2qm2+qe1qm3=mixd", exact_em2, exact_em)

  finite2 = em2emgl_e3m1(p1, p2, q1, q2, q3) &
          + em2emgl_e2m2(p1, p2, q1, q2, q3) &
          + em2emgl_e1m3(p1, p2, q1, q2, q3)
  call check("em2emGL_MIXD-split", finite2, em2emgl_mixd(p1, p2, q1, q2, q3))

  finite2 = em2emgl_e3m1_s(p1, p2, q1, q2) &
          + em2emgl_e2m2_s(p1, p2, q1, q2) &
          + em2emgl_e1m3_s(p1, p2, q1, q2)
  call check("em2emGL_MIXD-split_S", finite2, em2emgl_mixd_s(p1, p2, q1, q2))


  call initflavour("mu-eOld", mys=100000._prec)
  y = (/0.3,0.6,0.8,0.4,0.9/)
  call psx3_fks(y,p1,me,p2,me,q1,mm,q2,mm,q3,weight)

  pol1 = (/0._prec, 0._prec, p1(4)/sqrt(sq(p1)), p1(3)/sqrt(sq(p1)) /)
  pol2 = (/0._prec, 0._prec, p2(4)/sqrt(sq(p1)), p2(3)/sqrt(sq(p1)) /)

  call check("pepe2mmgl fin",pepe2mmgl_eeee_pvred(p1,pol1,p2,pol2,q1,q2,q3,sing),&
             306.68582423154,threshold=2e-8)
  call check("pepe2mmgl sin", sing,53.37624486541006695850,threshold=2e-8)

  call check("pepe2mmgl NTS", pepe2mmgl_nts_eeee(p1,pol1,p2,pol2,q1,q2,q3), &
            -686.4428653549571)

  call switchoffcachesystem_cll
  call check("pepe2mmglCOLL fin",pepe2mmgl_eeee_coll(p1,pol1,p2,pol2,q1,q2,q3,sing),&
             306.68582423154,threshold=2e-8)

  call check("pepe2mmglCOLL sin", sing,53.37624486541006695850,threshold=2e-8)
  call check("pepe2mmgf", pepe2mmgf_eeee(p1,pol1,p2,pol2,q1,q2,q3), 70.904393168680059, 1e-9)


  print*, "----------------------------------"
  print*, "   Test fRV  Matrixelement"
  print*, "----------------------------------"

  call initflavour("mu-eOld", mys=40000.)
  y = (/0.3,0.6,0.8,0.4,0.9/)
  call psx3_fks(y,p1,me,p2,mm,q1,me,q2,mm,q3,weight)

  xieik1 = 0.3
  finite = -4125.0198705752236_prec
  call check("agreement mu=mm", em2emgf_eeee_pv(q1rest,q2rest,q3rest,q4rest,q5rest), finite,threshold=1e-9)
  musq = me**2
  call check("agreement mu=me", em2emgf_eeee_pv(q1rest,q2rest,q3rest,q4rest,q5rest), finite,threshold=1e-9)
  musq = mm**2

  finite2 = em2emgf_e3m1(p1, p2, q1, q2, q3) &
          + em2emgf_e2m2(p1, p2, q1, q2, q3) &
          + em2emgf_e1m3(p1, p2, q1, q2, q3)
  call check("em2emGF_MIXD-split", finite2, em2emgf_mixd(p1, p2, q1, q2, q3))

  finite2 = em2emgf_e3m1_s(p1, p2, q1, q2) &
          + em2emgf_e2m2_s(p1, p2, q1, q2) &
          + em2emgf_e1m3_s(p1, p2, q1, q2)
  call check("em2emGF_MIXD-split_S", finite2, em2emgf_mixd_s(p1, p2, q1, q2))

  print*, "----------------------------------"
  print*, "   Test RR   Matrixelement"
  print*, "----------------------------------"

  ran_seed = 23312
  do ido = 1,8
    y8(ido) = ran2(ran_seed)
  enddo
  call psx4_fkss(y8,p1,me,p2,mm,q1,me,q2,mm,q3,q4,weight)
  call check("RR matrix element", em2emgg_eeee(p1,p2,q1,q2,q3,q4), 0.7542796938033984)
  call check("RR NTS matrix element", em2emgg_eeee_nts(p1,p2,q1,q2,q3,q4), -10.156646038377698 )
  call check("RR NTSS matrix element", em2emgg_eeee_ntss(p1,p2,q1,q2,q3,q4), -16.29463377650214 )
  call check("full RR matrix element", em2emgg(p1,p2,q1,q2,q3,q4), 0.67596393974333657)

  finite2 = em2emgg_e3m1(p1, p2, q1, q2, q3, q4) &
          + em2emgg_e2m2(p1, p2, q1, q2, q3, q4) &
          + em2emgg_e1m3(p1, p2, q1, q2, q3, q4)
  call check("em2emGG_MIXD-split", finite2, em2emgg_mixd(p1, p2, q1, q2, q3, q4))

  finite2 = em2emgg_e3m1_sh(p1, p2, q1, q2, q4) &
          + em2emgg_e2m2_sh(p1, p2, q1, q2, q4) &
          + em2emgg_e1m3_sh(p1, p2, q1, q2, q4)
  call check("em2emGG_MIXD-split_SH", finite2, em2emgg_mixd_sh(p1, p2, q1, q2, q4))

  finite2 = em2emgg_e3m1_hs(p1, p2, q1, q2, q3) &
          + em2emgg_e2m2_hs(p1, p2, q1, q2, q3) &
          + em2emgg_e1m3_hs(p1, p2, q1, q2, q3)
  call check("em2emGG_MIXD-split_HS", finite2, em2emgg_mixd_hs(p1, p2, q1, q2, q3))

  finite2 = em2emgg_e3m1_ss(p1, p2, q1, q2) &
          + em2emgg_e2m2_ss(p1, p2, q1, q2) &
          + em2emgg_e1m3_ss(p1, p2, q1, q2)
  call check("em2emGG_MIXD-split_SS", finite2, em2emgg_mixd_ss(p1, p2, q1, q2))

  call initflavour("mu-eOld", mys=100000._prec)

  ran_seed = 23312
  do ido = 1,8
    y8(ido) = ran2(ran_seed)
  enddo

  call psx4_fkss(y8,p1,me,p2,me,q1,mm,q2,mm,q3,q4,weight)

  call check("pepe2mmgg", pepe2mmgg_eeee(p1,pol1,p2,pol2,q1,q2,q3,q4),3.314908017840935, threshold=2e-5)
  call check("pepe2mmgg polar vs. unpolar",em2emgg_eeee(p1,-q1,-p2,q2,q3,q4),&
    pepe2mmgg_eeee(p1,pol0,p2,pol0,q1,q2,q3,q4),threshold=2e-5)

  print*, "----------------------------------"
  print*, "   Test OL               "
  print*, "----------------------------------"

  call initflavour("mu-eOld", mys=40000.)

  ran_seed =720620554
  do ido = 1,8
    y8(ido) = ran2(ran_seed)
  enddo
  call psx4(y8, p1, me, p2, mm, q1, me, q2, mm, q3, me, q4, me, weight)

  !print*,"s12->",s(p1,p2)
  !print*,"s13->",s(p1,q1)
  !print*,"s14->",s(p1,q2)
  !print*,"s15->",s(p1,q3)
  !print*,"s16->",s(p1,q4)
  !print*,"s23->",s(p2,q1)
  !print*,"s24->",s(p2,q2)
  !print*,"s25->",s(p2,q3)
  !print*,"s26->",s(p2,q4)
  !print*,"s34->",s(q1,q2)
  !print*,"s35->",s(q1,q3)
  !print*,"s36->",s(q1,q4)
  !print*,"s45->",s(q2,q3)
  !print*,"s46->",s(q2,q4)
  !print*,"s56->",s(q3,q4)
  !print*,"m->",me,",M->",mm,"S->",scms

  call check("open electrons",em2emeeee(p1,p2,q1,q2,q3,q4) , 61.55058430918309 )

  call initflavour("muone-")
  ran_seed = 23312
  do ido = 1,8
    y8(ido) = ran2(ran_seed)
  enddo
  call psx4_fkss(y8,p1,me,p2,mm,q1,me,q2,mm,q3,q4,weight)
  ggmixd = em2emgg_mixd(p1,p2,q1,q2,q3,q4)
  ggmixd_ol = em2emgg_mixd_ol(p1,p2,q1,q2,q3,q4)
  call check("gg-ol-muone-",ggmixd/ggmixd_ol,1._prec)

  call initflavour("muone+")
  ran_seed = 23312
  do ido = 1,8
    y8(ido) = ran2(ran_seed)
  enddo
  call psx4_fkss(y8,p1,me,p2,mm,q1,me,q2,mm,q3,q4,weight)
  ggmixd = em2emgg_mixd(p1,p2,q1,q2,q3,q4)
  ggmixd_ol = em2emgg_mixd_ol(p1,p2,q1,q2,q3,q4)
  call check("gg-ol-muone+",ggmixd/ggmixd_ol,1._prec)

  call blockend(87)

  call initflavour("mu-eOld", mys=100000._prec)

  x = (/0.75,0.2/)
  call psx2(x,p1,me,p2,me,q1,mm,q2,mm,weight)
  musq = Mm**2
  n1 = boost_back(p1, (/0.,0.,+1.,0./))
  n2 = boost_back(p2, (/0.,0.,+1.,0./))
  pol1 = (/0._prec, 0._prec, p1(4)/sqrt(sq(p1)), p1(3)/sqrt(sq(p1)) /)
  pol2 = (/0._prec, 0._prec, p2(4)/sqrt(sq(p1)), p2(3)/sqrt(sq(p1)) /)

  call openloops("ee2mm",p1,p2,q1,q2, tree=olTree,fin=olFinite, sin=olSingle, &
       hel1=int(cos_th(n1,p1)),hel2=int(cos_th(-n2,p2)))
  call check("open pepe2mm tree", olTree, pepe2mm(p1,pol1,p2,pol2,q1,q2)*0.25,threshold=1e-6)
  call check("open pepe2mml fin", olFinite,pepe2mml(p1,pol1,p2,pol2,q1,q2,sing)*0.25,threshold=1e-6)
  call check("open pepe2mml sin", olSingle,sing*0.25,threshold=1e-6)

  y = (/0.3,0.6,0.8,0.4,0.9/)
  call psx3_fks(y,p1,me,p2,me,q1,mm,q2,mm,q3,weight)

  pol1 = (/0._prec, 0._prec, p1(4)/sqrt(sq(p1)), p1(3)/sqrt(sq(p1)) /)
  pol2 = (/0._prec, 0._prec, p2(4)/sqrt(sq(p1)), p2(3)/sqrt(sq(p1)) /)
  n1 = boost_back(p1, (/0.,0.,+1.,0./))
  n2 = boost_back(p2, (/0.,0.,+1.,0./))

  call openloops("ee2mmg", p1,p2,q1,q2,q3,tree=olTree, &
     hel1=int(cos_th(n1,p1)),hel2=int(cos_th(-n2,p2)))
  call check("open pepe2mmg tree",pepe2mmg(p1,pol1,p2,pol2,q1,q2,q3),&
             4*olTree,threshold=2e-5)

  call openloops("ee2mmgEE", p1,p2,q1,q2,q3,tree=olTree, fin=olFinite, sin=olSingle, &
     hel1=int(cos_th(n1,p1)),hel2=int(cos_th(-n2,p2)))
  call check("open pepe2mmgl fin",pepe2mmgl_eeee_pvred(p1,pol1,p2,pol2,q1,q2,q3,sing),&
             4*olFinite,threshold=2e-5)
  call check("open pepe2mmgl sin",sing,4*olSingle,threshold=2e-5)

  y = (/1e-3,0.6,0.8,0.4,0.9/)
  call psx3_fks(y,p1,me,p2,me,q1,mm,q2,mm,q3,weight)

  pol1 = (/0._prec, 0._prec, p1(4)/sqrt(sq(p1)), p1(3)/sqrt(sq(p1)) /)
  pol2 = (/0._prec, 0._prec, p2(4)/sqrt(sq(p1)), p2(3)/sqrt(sq(p1)) /)
  n1 = boost_back(p1, (/0.,0.,+1.,0./))
  n2 = boost_back(p2, (/0.,0.,+1.,0./))
  call openloops("ee2mmgEE", p1,p2,q1,q2,q3,tree=olTree, fin=olFinite, sin=olSingle, &
     hel1=int(cos_th(n1,p1)),hel2=int(cos_th(-n2,p2)))
  call check("open pepe2mmgl fin soft",pepe2mmgl_eeee_pvred(p1,pol1,p2,pol2,q1,q2,q3,sing),&
             4*olFinite,threshold=2e-5)
  call check("open pepe2mmgl sin soft",sing,4*olSingle,threshold=2e-5)

  y = (/0.3,0.999,0.8,0.4,0.9/)
  call psx3_fks(y,p1,me,p2,me,q1,mm,q2,mm,q3,weight)

  pol1 = (/0._prec, 0._prec, p1(4)/sqrt(sq(p1)), p1(3)/sqrt(sq(p1)) /)
  pol2 = (/0._prec, 0._prec, p2(4)/sqrt(sq(p1)), p2(3)/sqrt(sq(p1)) /)
  n1 = boost_back(p1, (/0.,0.,+1.,0./))
  n2 = boost_back(p2, (/0.,0.,+1.,0./))
  call openloops("ee2mmgEE", p1,p2,q1,q2,q3,tree=olTree, fin=olFinite, sin=olSingle, &
     hel1=int(cos_th(n1,p1)),hel2=int(cos_th(-n2,p2)))
  call check("open pepe2mmgl fin collinear",pepe2mmgl_eeee_pvred(p1,pol1,p2,pol2,q1,q2,q3,sing),&
             4*olFinite,threshold=2e-5)
  call check("open pepe2mmgl sin collinear",sing,4*olSingle,threshold=2e-5)


  ran_seed =23312
  do ido = 1,8
    y8(ido) = ran2(ran_seed)
  enddo

  call psx4_fkss(y8,p1,me,p2,me,q1,mm,q2,mm,q3,q4,weight)

  pol1 = (/0._prec, 0._prec, p1(4)/sqrt(sq(p1)), p1(3)/sqrt(sq(p1)) /)
  pol2 = (/0._prec, 0._prec, p2(4)/sqrt(sq(p1)), p2(3)/sqrt(sq(p1)) /)
  n1 = boost_back(p1, (/0.,0.,+1.,0./))
  n2 = boost_back(p2, (/0.,0.,+1.,0./))

  call openloops("ee2mmggEE", p1,p2,q2,q1,q3,q4,tree=olTree,&
     hel1=int(cos_th(n1,p1)),hel2=int(cos_th(-n2,p2)))
  call check("open pepe2mmgg", 8*olTree,pepe2mmgg_eeee(p1,pol1,p2,pol2,q1,q2,q3,q4), threshold=2e-5)

  call blockend(69)

  END SUBROUTINE



  SUBROUTINE TESTMUENFEM
  use mue_em2em_nfem
  implicit none
  real (kind=prec) :: x(2)
  real (kind=prec) :: q1(4),q2(4),q3(4),q4(4)
  real (kind=prec) :: weight,m2,mm2,ss,tt,qq2,pole

  call blockstart("mue nf-box matrix element")
  Nel = 1
  Nmu = 0
  Ntau = 0
  Nhad = 0

  print*, "----------------------------------------------"
  print*, "   Basis Functions"
  print*, "----------------------------------------------"

  m2 = mel**2; mm2 = mmu**2
  ss = 400.**2; tt = -1000.

  print*, "small qq2:"
  qq2 = 1.e-10
  call check("d1:",d1(qq2,ss,tt,m2,mm2),-195693.203536319)
  call check("d2:",d2(qq2,ss,tt,m2,mm2),-0.001)
  call check("d12:",d12(qq2,ss,tt,m2,mm2),195.69511835280218)
  call check("d13:",d13(qq2,ss,tt,m2,mm2),-532248.0436600138)
  call check("d14:",d14(qq2,ss,tt,m2,mm2),535392.2949568821)
  call check("d123:",d123(qq2,ss,tt,m2,mm2),532.2493658151434)
  call check("d124:",d124(qq2,ss,tt,m2,mm2),-535.3936241188206)

  print*, "moderate qq2:"
  qq2 = 0.1
  call check("d1:",d1(qq2,ss,tt,m2,mm2),-4.563068015616737)
  call check("d2:",d2(qq2,ss,tt,m2,mm2),-0.001)
  call check("d12:",d12(qq2,ss,tt,m2,mm2),0.006093902300872959)
  call check("d13:",d13(qq2,ss,tt,m2,mm2),-0.0005116618009265426)
  call check("d14:",d14(qq2,ss,tt,m2,mm2),0.0005146974276898515)
  call check("d123:",d123(qq2,ss,tt,m2,mm2),5.528908976383256e-7)
  call check("d124:",d124(qq2,ss,tt,m2,mm2),-5.561441057963782e-7)

  print*, "high qq2:"
  qq2 = 500.
  call check("d1:",d1(qq2,ss,tt,m2,mm2),-0.0019989566098080067)
  call check("d2:",d2(qq2,ss,tt,m2,mm2),-0.001)
  call check("d12:",d12(qq2,ss,tt,m2,mm2),0.0000164955799824012)
  call check("d13:",d13(qq2,ss,tt,m2,mm2),-5.4007665116915996e-8)
  call check("d14:",d14(qq2,ss,tt,m2,mm2),5.47608111251097e-8)
  call check("d123:",d123(qq2,ss,tt,m2,mm2),3.1778363355620957e-10)
  call check("d124:",d124(qq2,ss,tt,m2,mm2),-3.187927453800403e-10)

  print*, "ultra-high qq2:"
  qq2 = 1.e9
  call check("d1:",d1(qq2,ss,tt,m2,mm2),-9.9999999973888e-10)
  call check("d2:",d2(qq2,ss,tt,m2,mm2),-1.e-9)
  call check("d12:",d12(qq2,ss,tt,m2,mm2),1.0000004997392133e-18)
  call check("d13:",d13(qq2,ss,tt,m2,mm2),1.0000632600423407e-18)
  call check("d14:",d14(qq2,ss,tt,m2,mm2),9.9991492720709e-19)
  call check("d123:",d123(qq2,ss,tt,m2,mm2),-1.0000642601066007e-27)
  call check("d124:",d124(qq2,ss,tt,m2,mm2),-9.99915927123017e-28)

  print*, "qq2~-tt:"
  qq2 = -tt + 1.e-5
  call check("d1:",d1(qq2,ss,tt,m2,mm2),-0.0009997390063565942)
  call check("d2:",d2(qq2,ss,tt,m2,mm2),-0.00099999999)
  call check("d12:",d12(qq2,ss,tt,m2,mm2),0.000018411589712439248,1e-9)
  call check("d13:",d13(qq2,ss,tt,m2,mm2),-2.4327121514092376e-8)
  call check("d14:",d14(qq2,ss,tt,m2,mm2),2.4799870864695356e-8)
  call check("d123:",d123(qq2,ss,tt,m2,mm2),0.0024327121514092376,1e-8)
  call check("d124:",d124(qq2,ss,tt,m2,mm2),-0.0024799870864695354,1e-8)

  print*, "----------------------------------------------"
  print*, "   Kernels for nel"
  print*, "----------------------------------------------"

  print*, "small qq2:"
  qq2 = 1.e-10
  call check("gijk_012:",gijk(0,1,2,qq2,ss,tt,m2,mm2),-1.5903724541773617e-9)
  call check("gijk_013:",gijk(0,1,3,qq2,ss,tt,m2,mm2),4.325466238256601e-6)
  call check("gijk_014:",gijk(0,1,4,qq2,ss,tt,m2,mm2),-4.351018897380862e-6)
  call check("gijk_023:",gijk(0,2,3,qq2,ss,tt,m2,mm2),-7.691568739406814e-12)
  call check("gijk_024:",gijk(0,2,4,qq2,ss,tt,m2,mm2),-7.691568739406814e-12)
  call check("gijk_123:",gijk(1,2,3,qq2,ss,tt,m2,mm2),-3.719015436537368e-8)
  call check("gijk_124:",gijk(1,2,4,qq2,ss,tt,m2,mm2),3.7409854865155914e-8)
  call check("g012k_3:",g012k(3,qq2,ss,tt,m2,mm2),-4.25109667439751e-9)
  call check("g012k_4:",g012k(4,qq2,ss,tt,m2,mm2),4.2762099894650414e-9)
  call check("gp13:",gp13(qq2,ss,tt,m2,mm2),-2.708688872515786e-14)
  call check("gp14:",gp14(qq2,ss,tt,m2,mm2),2.690489675887544e-14)
  call check("gp133:",gp133(qq2,ss,tt,m2,mm2),5.372383765357889e-9)
  call check("gp134:",gp134(qq2,ss,tt,m2,mm2),-5.400854825413843e-9)
  call check("gp2:",gp2(qq2,ss,tt,m2,mm2),1.1580415417959103e-9)
  call check("gp4:",gp4(qq2,ss,tt,m2,mm2),1.15026088930336e-9)
  call check("gij_013:",gij(0,1,3,qq2,ss,tt,m2,mm2),-136737.794635492)
  call check("gij_014:",gij(0,1,4,qq2,ss,tt,m2,mm2),-136737.794635492)
  call check("gij_023:",gij(0,2,3,qq2,ss,tt,m2,mm2),-0.0006987355317637046)
  call check("gij_024:",gij(0,2,4,qq2,ss,tt,m2,mm2),-0.0006987355317636971)
  call check("gij_033:",gij(0,3,3,qq2,ss,tt,m2,mm2),-661.3157723634416)
  call check("gij_044:",gij(0,4,4,qq2,ss,tt,m2,mm2),-661.3157723634416)
  call check("gij_123:",gij(1,2,3,qq2,ss,tt,m2,mm2),-1.3673913262299577e-8)
  call check("gij_124:",gij(1,2,4,qq2,ss,tt,m2,mm2),-1.3673913254839576e-8)
  call check("gij_133:",gij(1,3,3,qq2,ss,tt,m2,mm2),0.0000371900619816932)
  call check("gij_144:",gij(1,4,4,qq2,ss,tt,m2,mm2),-0.000037409761991884786)
  call check("gij_233:",gij(2,3,3,qq2,ss,tt,m2,mm2),-6.613158408486186e-11)
  call check("gij_244:",gij(2,4,4,qq2,ss,tt,m2,mm2),-6.613157662486095e-11)

  print*, "moderate qq2:"
  qq2 = 0.1
  call check("gijk_012:",gijk(0,1,2,qq2,ss,tt,m2,mm2),-0.00004760003833780495)
  call check("gijk_013:",gijk(0,1,3,qq2,ss,tt,m2,mm2),3.996637973110408e-6)
  call check("gijk_014:",gijk(0,1,4,qq2,ss,tt,m2,mm2),-4.020349536436925e-6)
  call check("gijk_023:",gijk(0,2,3,qq2,ss,tt,m2,mm2),-2.337879933023094e-7)
  call check("gijk_024:",gijk(0,2,4,qq2,ss,tt,m2,mm2),-2.337879933023094e-7)
  call check("gijk_123:",gijk(1,2,3,qq2,ss,tt,m2,mm2),-3.820058330863286e-8)
  call check("gijk_124:",gijk(1,2,4,qq2,ss,tt,m2,mm2),3.842535541067478e-8)
  call check("g012k_3:",g012k(3,qq2,ss,tt,m2,mm2),-4.241425109432918e-9)
  call check("g012k_4:",g012k(4,qq2,ss,tt,m2,mm2),4.266381640326654e-9)
  call check("gp13:",gp13(qq2,ss,tt,m2,mm2),-0.000026016490311941204)
  call check("gp14:",gp14(qq2,ss,tt,m2,mm2),0.000025826157039301342)
  call check("gp133:",gp133(qq2,ss,tt,m2,mm2),0.00012064282023753278)
  call check("gp134:",gp134(qq2,ss,tt,m2,mm2),-0.00012121494040958902)
  call check("gp2:",gp2(qq2,ss,tt,m2,mm2),0.8897333386536544)
  call check("gp4:",gp4(qq2,ss,tt,m2,mm2),0.8837522140748362)
  call check("gij_013:",gij(0,1,3,qq2,ss,tt,m2,mm2),-3.1527352110390336)
  call check("gij_014:",gij(0,1,4,qq2,ss,tt,m2,mm2),-3.15273520337644)
  call check("gij_023:",gij(0,2,3,qq2,ss,tt,m2,mm2),-0.0006909282587099117)
  call check("gij_024:",gij(0,2,4,qq2,ss,tt,m2,mm2),-0.0006909205961160398)
  call check("gij_033:",gij(0,3,3,qq2,ss,tt,m2,mm2),-0.02064794345126251)
  call check("gij_044:",gij(0,4,4,qq2,ss,tt,m2,mm2),-0.020647935788668637)
  call check("gij_123:",gij(1,2,3,qq2,ss,tt,m2,mm2),-0.0004210464227011532)
  call check("gij_124:",gij(1,2,4,qq2,ss,tt,m2,mm2),-0.0004210387601072813)
  call check("gij_133:",gij(1,3,3,qq2,ss,tt,m2,mm2),0.00003534814420013196)
  call check("gij_144:",gij(1,4,4,qq2,ss,tt,m2,mm2),-0.00003555786059466123)
  call check("gij_233:",gij(2,3,3,qq2,ss,tt,m2,mm2),-2.071774383663164e-6)
  call check("gij_244:",gij(2,4,4,qq2,ss,tt,m2,mm2),-2.064111789791233e-6)

  print*, "high qq2:"
  qq2 = 500.
  call check("gijk_012:",gijk(0,1,2,qq2,ss,tt,m2,mm2),-0.000010315602449458954)
  call check("gijk_013:",gijk(0,1,3,qq2,ss,tt,m2,mm2),3.377399298260499e-8)
  call check("gijk_014:",gijk(0,1,4,qq2,ss,tt,m2,mm2),-3.4244977016825796e-8)
  call check("gijk_023:",gijk(0,2,3,qq2,ss,tt,m2,mm2),-3.291969191720055e-7)
  call check("gijk_024:",gijk(0,2,4,qq2,ss,tt,m2,mm2),-3.291969191720055e-7)
  call check("gijk_123:",gijk(1,2,3,qq2,ss,tt,m2,mm2),-1.165947903310629e-8)
  call check("gijk_124:",gijk(1,2,4,qq2,ss,tt,m2,mm2),1.1696503338040904e-8)
  call check("g012k_3:",g012k(3,qq2,ss,tt,m2,mm2),-5.0696613994214654e-11)
  call check("g012k_4:",g012k(4,qq2,ss,tt,m2,mm2),5.08575994799593e-11)
  call check("gp13:",gp13(qq2,ss,tt,m2,mm2),-0.001976236740656532)
  call check("gp14:",gp14(qq2,ss,tt,m2,mm2),0.001848418861986297)
  call check("gp133:",gp133(qq2,ss,tt,m2,mm2),5.241532216790767e-6)
  call check("gp134:",gp134(qq2,ss,tt,m2,mm2),-4.8046024118048525e-6)
  call check("gp2:",gp2(qq2,ss,tt,m2,mm2),0.385748958787779)
  call check("gp4:",gp4(qq2,ss,tt,m2,mm2),0.37863728625315546)
  call check("gij_013:",gij(0,1,3,qq2,ss,tt,m2,mm2),-0.0001525131443182871)
  call check("gij_014:",gij(0,1,4,qq2,ss,tt,m2,mm2),-0.0001408351531327135)
  call check("gij_023:",gij(0,2,3,qq2,ss,tt,m2,mm2),-0.00007920972389541489)
  call check("gij_024:",gij(0,2,4,qq2,ss,tt,m2,mm2),-0.00006753173270984129)
  call check("gij_033:",gij(0,3,3,qq2,ss,tt,m2,mm2),-0.00003377568062501637)
  call check("gij_044:",gij(0,4,4,qq2,ss,tt,m2,mm2),-0.000022097689439442773)
  call check("gij_123:",gij(1,2,3,qq2,ss,tt,m2,mm2),-0.0006110524402309854)
  call check("gij_124:",gij(1,2,4,qq2,ss,tt,m2,mm2),-0.0005993744490454118)
  call check("gij_133:",gij(1,3,3,qq2,ss,tt,m2,mm2),-3.848198705244099e-6)
  call check("gij_144:",gij(1,4,4,qq2,ss,tt,m2,mm2),3.8390779365532775e-6)
  call check("gij_233:",gij(2,3,3,qq2,ss,tt,m2,mm2),-0.000025143923972992884)
  call check("gij_244:",gij(2,4,4,qq2,ss,tt,m2,mm2),-0.000013465932787419289)

  print*, "ultra-high qq2:"
  qq2 = 1.e9
  call check("gijk_012:",gijk(0,1,2,qq2,ss,tt,m2,mm2),-2.1644419569645497e-18,1e-7)
  call check("gijk_013:",gijk(0,1,3,qq2,ss,tt,m2,mm2),-2.1645777979299854e-18,1e-7)
  call check("gijk_014:",gijk(0,1,4,qq2,ss,tt,m2,mm2),-2.1642567402782184e-18,1e-7)
  call check("gijk_023:",gijk(0,2,3,qq2,ss,tt,m2,mm2),-2.164417794901372e-18,1e-7)
  call check("gijk_024:",gijk(0,2,4,qq2,ss,tt,m2,mm2),-2.164417794901372e-18,1e-7)
  call check("gijk_123:",gijk(1,2,3,qq2,ss,tt,m2,mm2),-1.465799529918363e-18,1e-7)
  call check("gijk_124:",gijk(1,2,4,qq2,ss,tt,m2,mm2),-1.4655821174717014e-18,1e-7)
  call check("g012k_3:",g012k(3,qq2,ss,tt,m2,mm2),7.670204948862455e-28,1e-7)
  call check("g012k_4:",g012k(4,qq2,ss,tt,m2,mm2),7.669067277584569e-28,1e-7)
  call check("gp13:",gp13(qq2,ss,tt,m2,mm2),-8.107604105760318e-14,1e-7)
  call check("gp14:",gp14(qq2,ss,tt,m2,mm2),7.945272963477259e-14,1e-7)
  call check("gp133:",gp133(qq2,ss,tt,m2,mm2),-1.082311720479192e-15,1e-7)
  call check("gp134:",gp134(qq2,ss,tt,m2,mm2),-1.0820976826237387e-15,1e-7)
  call check("gp2:",gp2(qq2,ss,tt,m2,mm2),8.10778431672826e-14,1e-7)
  call check("gp4:",gp4(qq2,ss,tt,m2,mm2),7.945451160942116e-14,1e-7)
  call check("gij_013:",gij(0,1,3,qq2,ss,tt,m2,mm2),-9.418676434586211e-14,1e-7)
  call check("gij_014:",gij(0,1,4,qq2,ss,tt,m2,mm2),1.2322568231562971e-13,1e-7)
  call check("gij_023:",gij(0,2,3,qq2,ss,tt,m2,mm2),-9.418638162099015e-14,1e-7)
  call check("gij_024:",gij(0,2,4,qq2,ss,tt,m2,mm2),1.2322606504050168e-13,1e-7)
  call check("gij_033:",gij(0,3,3,qq2,ss,tt,m2,mm2),-1.1054869961760382e-13,1e-7)
  call check("gij_044:",gij(0,4,4,qq2,ss,tt,m2,mm2),1.06863747043888e-13,1e-7)
  call check("gij_123:",gij(1,2,3,qq2,ss,tt,m2,mm2),-9.345391118578007e-14,1e-7)
  call check("gij_124:",gij(1,2,4,qq2,ss,tt,m2,mm2),1.2395853547571175e-13,1e-7)
  call check("gij_133:",gij(1,3,3,qq2,ss,tt,m2,mm2),-1.465799529918363e-15,1e-7)
  call check("gij_144:",gij(1,4,4,qq2,ss,tt,m2,mm2),-1.4655821174717014e-15,1e-7)
  call check("gij_233:",gij(2,3,3,qq2,ss,tt,m2,mm2),-1.0981585736545636e-13,1e-7)
  call check("gij_244:",gij(2,4,4,qq2,ss,tt,m2,mm2),1.0759658929603546e-13,1e-7)

  print*, "qq2~-tt:"
  qq2 = -tt + 1.e-5
  call check("gijk_012:",gijk(0,1,2,qq2,ss,tt,m2,mm2),-0.000012864831947990778,2e-5)
  call check("gijk_013:",gijk(0,1,3,qq2,ss,tt,m2,mm2),1.6998224213398817e-8,2e-5)
  call check("gijk_014:",gijk(0,1,4,qq2,ss,tt,m2,mm2),-1.7328550982787998e-8,2e-5)
  call check("gijk_023:",gijk(0,2,3,qq2,ss,tt,m2,mm2),-2.019727567447302e-6,2e-5)
  call check("gijk_024:",gijk(0,2,4,qq2,ss,tt,m2,mm2),-2.019727567447302e-6,2e-5)
  call check("gijk_123:",gijk(1,2,3,qq2,ss,tt,m2,mm2),2.577161192267457e-9,2e-5)
  call check("gijk_124:",gijk(1,2,4,qq2,ss,tt,m2,mm2),-2.6272432079032925e-9,2e-5)
  call check("g012k_3:",g012k(3,qq2,ss,tt,m2,mm2),5.921950884822197e-12,2e-5)
  call check("g012k_4:",g012k(4,qq2,ss,tt,m2,mm2),-6.037032253305545e-12,2e-5)
  call check("gp13:",gp13(qq2,ss,tt,m2,mm2),-0.002942394842114637,2e-5)
  call check("gp14:",gp14(qq2,ss,tt,m2,mm2),0.0010127472862128616,2e-5)
  call check("gp133:",gp133(qq2,ss,tt,m2,mm2),3.1655269228354176e-6,2e-5)
  call check("gp134:",gp134(qq2,ss,tt,m2,mm2),-2.771523051032663e-6,2e-5)
  call check("gp2:",gp2(qq2,ss,tt,m2,mm2),0.05804103956600475,2e-5)
  call check("gp4:",gp4(qq2,ss,tt,m2,mm2),0.04552548413733917,2e-5)
  call check("gij_013:",gij(0,1,3,qq2,ss,tt,m2,mm2),2.5771622771403452e-6,2e-5)
  call check("gij_014:",gij(0,1,4,qq2,ss,tt,m2,mm2),-2.6272421750744485e-6,2e-5)
  call check("gij_023:",gij(0,2,3,qq2,ss,tt,m2,mm2),2.5771622774168253e-6,2e-5)
  call check("gij_024:",gij(0,2,4,qq2,ss,tt,m2,mm2),-2.627242174797968e-6,2e-5)
  call check("gij_033:",gij(0,3,3,qq2,ss,tt,m2,mm2),2.5771614911861116e-6,2e-5)
  call check("gij_044:",gij(0,4,4,qq2,ss,tt,m2,mm2),-2.627242961028682e-6,2e-5)
  call check("gij_123:",gij(1,2,3,qq2,ss,tt,m2,mm2),2.577180722868059e-6,2e-5)
  call check("gij_124:",gij(1,2,4,qq2,ss,tt,m2,mm2),-2.6272237293467346e-6,2e-5)
  call check("gij_133:",gij(1,3,3,qq2,ss,tt,m2,mm2),2.577161192267457e-6,2e-5)
  call check("gij_144:",gij(1,4,4,qq2,ss,tt,m2,mm2),-2.6272432079032926e-6,2e-5)
  call check("gij_233:",gij(2,3,3,qq2,ss,tt,m2,mm2),2.577164280219824e-6,2e-5)
  call check("gij_244:",gij(2,4,4,qq2,ss,tt,m2,mm2),-2.6272401719949695e-6,2e-5)


  print*, "----------------------------------------------"
  print*, "   matrix element"
  print*, "----------------------------------------------"
  call initflavour("muone")
  x = (/0.75_prec,0.5_prec/)
  call psx2(x,q1,me,q2,mm,q3,me,q4,mm,weight)
  call check("z=0.01:",em2em_nfem(0.01,q1,q2,q3,q4),-32.40453151890733,1E-8)
  call check("z=0.1:",em2em_nfem(0.1,q1,q2,q3,q4),-254749.95234957398)
  call check("z=0.2:",em2em_nfem(0.2,q1,q2,q3,q4),-3846.5572858482383)
  call check("z=0.9:",em2em_nfem(0.9,q1,q2,q3,q4),-3298.7863595167473)
  call check("z=0.999999:",em2em_nfem(0.999999,q1,q2,q3,q4),-3828.5478028919592)

  call check("nfem_sin:",em2em_nfem_sin(q1,q2,q3,q4,pole),2*7931.24652334488,1e-8)
  call check("nfem_sin pole:",pole,-2*353.48754811453,1e-9)

  x = (/0.75,0.999/)
  call psx2(x,q1,me,q2,mm,q3,me,q4,mm,weight)
  call check("z=0.01:",em2em_nfem(0.01,q1,q2,q3,q4),-495.64675187909296,1e-9)
  call check("z=0.1:",em2em_nfem(0.1,q1,q2,q3,q4),-5.990375641990295e7)
  call check("z=0.2:",em2em_nfem(0.2,q1,q2,q3,q4),-7.296375811627465e7)
  call check("z=0.9:",em2em_nfem(0.9,q1,q2,q3,q4),-915698.2981072481)
  call check("z=0.999999:",em2em_nfem(0.999999,q1,q2,q3,q4),-1.0686675748256166e6)

  call check("nfbx_sin:",em2em_nfem_sin(q1,q2,q3,q4,pole),-2*1.423044907963e7,1e-8)
  call check("nfbx_sin pole:",pole,-2*215588.3839664915,1e-6)

  x = (/0.75,0.001/)
  call psx2(x,q1,me,q2,mm,q3,me,q4,mm,weight)
  call check("z=0.01:",em2em_nfem(0.01,q1,q2,q3,q4),-22.04868249885327,1e-8)
  call check("z=0.1:",em2em_nfem(0.1,q1,q2,q3,q4),-47760.8683807417)
  call check("z=0.2:",em2em_nfem(0.2,q1,q2,q3,q4),2741.6557833655897)
  call check("z=0.9:",em2em_nfem(0.9,q1,q2,q3,q4),-1189.0344981150647)
  call check("z=0.999999:",em2em_nfem(0.999999,q1,q2,q3,q4),-1420.30464669056)

  call check("nfem_sin:",em2em_nfem_sin(q1,q2,q3,q4,pole),2*2968.0297703422757,1e-8)
  call check("nfem_sin pole:",pole,-2*269.65235993042006,1e-9)


  Nhad = 1
  Nhad = 1
  Nmu = 1
  Ntau = 1

  END SUBROUTINE TESTMUENFEM


  SUBROUTINE TESTMUESOFTN1
  call initflavour("muone")
  musq = mm**2
  xinormcut1 = 0.3
  xinormcut2 = 0.3

  call blockstart("mu-e \xi->0 NLO")
  call test_softlimit( &
    (/0.8,0.6,0.8,0.4,0.9/), &
    [ "em2emREE", "em2emREM", "em2emRMM" ] )

  call blockstart("mu-e \xi->0 RF")
  call test_softlimit( &
    (/0.8,0.6,0.8,0.4,0.9/), &
    [ "em2emRFEEEE", "em2emRFMMMM", "em2emRFMIXD", "em2emRF    "])

  call blockstart("mu-e \xi->0 NF")
  call test_softlimit( &
    (/0.8,0.6,0.8,0.4,0.9/), &
    [ "em2emAREE  ", "em2emARMM  ", "em2emAREM  ", "em2emAR    " ] )
  END SUBROUTINE

  SUBROUTINE TESTMUENTSOFTN1
  use olinterface
  use mue_mat_el, only:  em2emgl_nts, em2emgl_nts_eeee, em2emgl_nts_mmmm, em2emgl_nts_mixd, &
                         em2emgl_ol,  em2emgl_eeee_pv,  em2emgl_mmmm_pv,  em2emgl_mixd_ol
  implicit none
  integer, parameter :: ntest = 4
  integer ido,j,lastgood(ntest),eexpectation
  real(kind=prec) :: y(5), weight, pole
  real(kind=prec) :: q1(4), q2(4), q3(4)
  real(kind=prec) :: full(ntest), lim(ntest), lastratio(ntest), ratio(ntest)
  real(kind=prec), parameter :: xicut = .8
  character(len=5) :: red = char(27)//'[31m'
  character(len=5) :: green = char(27)//'[32m'
  character(len=4) :: norm = char(27)//'[0m'
  character(len=5+7+4) :: anscol


  call blockstart("mu-e \xi->0 NTS")
  call initflavour("muone")
  musq = mm**2
  xieik1 = 0.3
  y = (/0.3,0.6,0.8,0.4,0.9/)
  lastratio = 2.

  write(*,900, advance='no')
  write(*,*) "    gle_pv-NTS      glm_pv-NTS      glx_ol-NTS      gl_ol-NTS"
  write(*,*) repeat("-", 8 + 15 * ntest)

  do ido=1,10
    y(1) = 10.**(-ido)
    call psx3_fks(y,p1,me,p2,mm,q1,me,q2,mm,q3,weight)
    xieik1 = xicut
    full = (/ em2emgl_eeee_pv(p1,p2,q1,q2,q3),       &
              em2emgl_mmmm_pv(p1,p2,q1,q2,q3),       &
              em2emgl_mixd_ol(p1,p2,q1,q2,q3),       &
              em2emgl_ol     (p1,p2,q1,q2,q3)        /)
    lim =  (/ em2emgl_nts_eeee(p1,p2,q1,q2,q3,pole), &
              em2emgl_nts_mmmm(p1,p2,q1,q2,q3,pole), &
              em2emgl_nts_mixd(p1,p2,q1,q2,q3,pole), &
              em2emgl_nts     (p1,p2,q1,q2,q3,pole)  /)

    ratio = abs(1-lim/full)

    write(*,902, advance="no") xiout
    do j=1,ntest
      if (ratio(j).le.lastratio(j)) then
        lastgood(j) = ido
        lastratio(j) = ratio(j)
        write(*,903, advance="no") norm, ratio(j), norm
      else
        write(*,903, advance="no") red, ratio(j), norm
      endif
    enddo
    write(*,*)
  enddo

  eexpectation = 5
  do j=1,ntest
    if(lastgood(j) >= eexpectation) then
      anscol = green // '[PASS]' // norm
      npass = npass + 1
    else
      anscol = red   // '[FAIL]' // norm
      nfail = nfail + 1
    endif
    write(*,904) anscol, j, lastgood(j), eexpectation
  enddo

  call blockend(ntest)

900 format(2x,"xi",3x)
902 format(2x,ES6.0)
903 format(A,ES13.3,3x,A)
904 format(A,' ',I2,': ', I2, ' > ', I2)
  END SUBROUTINE

  SUBROUTINE TESTMUPAIRSOFTN1
  implicit none
  call initflavour("muone")
  musq = mm**2
  xinormcut1 = 0.3
  xinormcut2 = 0.3
  call blockstart("mu-pair \xi->0")
  call test_softlimit( &
    (/0.8,0.6,0.8,0.4,0.9/), &
    [ "ee2mmR     ", "ee2mmRFEEEE", "eeZmmRX    " ] )
  END SUBROUTINE




  SUBROUTINE TESTMUESOFTN2
  implicit none
  real (kind=prec) :: yold(8)
  integer ido

  call blockstart("mu-e \xi_{1,2} -> 0")
  call initflavour("muone")

  ran_seed = 23312
  do ido = 1,8
    yold(ido) = ran2(ran_seed)
  enddo

  call test_softlimit2(yold, "em2emRREEEE", (/7,7,7/))
  call test_softlimit2(yold, "em2emRRMMMM", (/7,7,7/))
  call test_softlimit2(yold, "em2emRRMIXD", (/7,7,7/))

  call blockend(9)
  ENDSUBROUTINE TESTMUESOFTN2


  SUBROUTINE TESTMUPAIRSOFTN2
  implicit none
  real (kind=prec) :: yold(8)
  integer ido

  call blockstart("mu-pair \xi_{1,2} -> 0")
  call initflavour("muone")
  pol1 = 0.
  pol2 = 0.

  ran_seed = 23312
  do ido = 1,8
    yold(ido) = ran2(ran_seed)
  enddo

  call test_softlimit2(yold, "ee2mmRREEEE", (/7,7,7/))

  call blockend(3)
  ENDSUBROUTINE

  SUBROUTINE TESTMUPMATEL
  use mue_mp2mpgg_nts
  use mue, only: mp2mp_nuc, mp2mpl_mp, mp2mpg_mp, mp2mpl_mp_mono
  use mue_em2emgl_lbk
  implicit none
  real (kind=prec) :: x(2),y(5),y8(8)
  real (kind=prec) :: q1(4),q2(4),q3(4),q4(4)
  real (kind=prec) :: single, finite
  real (kind=prec) :: weight, nuc, nucpole

  call blockstart("mu-p matrix elements")

  print*, "----------------------------------------------"
  print*, "   Test Born and NLO Matrixelements"
  print*, "----------------------------------------------"
  !muse setup
  call initflavour(flav='museOld')
  musq = 0.4*Mm**2
  Nel = 1
  Nmu = 1
  Ntau = 1
  Nhad = 1

  x = (/0.75,0.5/)
  call psx2(x,p1,me,p2,mm,q1,me,q2,mm,weight)
  call check("born",    mp2mp           (p1,p2,q1,q2),  18837.406746645658,     threshold=2e-8)
  call check("born_nuc",mp2mp_nuc       (p1,p2,q1,q2),  18837.406746645689,     threshold=2e-8)
  call check("vertex",  mp2mpl          (p1,p2,q1,q2),  21927.285286760474,     threshold=2e-8)

  nuc = mp2mpl_mp(p1,p2,q1,q2,nucpole)
  call check("nucleon fin", nuc   ,  15785.969376146051)
  call check("nucleon sin", nucpole,  1414.932782207703)
  nuc = mp2mpl_mp_mono(p1,p2,q1,q2,nucpole)
  call check("monopole fin", nuc   ,  1.326078770281548e16)
  call check("monopole sin", nucpole,  8.35137022983398e14)

  call check("a",      mp2mp_a         (p1,p2,q1,q2),  47871.8724358059,     threshold=3e-8)
  call check("aa",     mp2mp_aa        (p1,p2,q1,q2),  109630.15362116879,     threshold=6e-8)
  call check("al",     mp2mp_al        (p1,p2,q1,q2),  55724.24157047197,     threshold=3e-8)
  call initflavour(flav='muse210')
  call check("nf",     mp2mp_nf_interpol        (p1,p2,q1,q2),  -30840.848957943635,      threshold=1e-5)

  !mesa setup
  call initflavour(flav='mesaOld')
  musq = 0.4*Mm**2
  x = (/ .2, 0.8831901644527639_prec /)
  call psx2(x,p1,me,p2,mm,q1,me,q2,mm,weight)
  call check("born"     ,mp2mp          (p1,p2,q1,q2),  644977.9103846863,      threshold=2e-8)
  call check("vertex"   ,mp2mpl         (p1,p2,q1,q2),  1.9247985754475035E7,   threshold=2e-8)

  call check("a",      mp2mp_a         (p1,p2,q1,q2),  1.2257522389288011E6,     threshold=3e-8)
  call check("aa",     mp2mp_aa        (p1,p2,q1,q2),  2.2385120923089967E6,     threshold=3e-8)
  call check("al",     mp2mp_al        (p1,p2,q1,q2),  3.657995297753011E7,     threshold=3e-8)
  call check("nf",     mp2mp_nf_interpol        (p1,p2,q1,q2),  -1.9831514232263186E6,    threshold=1e-6)

  musq = Me**2
  call check("2loop",   mp2mpll         (p1,p2,q1,q2), 5.17979802434469086789e9 / 4 / pi**2)
  musq = 0.4*Mm**2
  call check("2loop",   mp2mpll         (p1,p2,q1,q2), 2.3995497614231266e10 / 4 / pi**2)
  xieik2 = 0.5_prec
  call check("2loop-ff",   mp2mpff         (p1,p2,q1,q2), 2.303599628847155e7, threshold=1e-8)


!  !hadronic vacuum polarization for muse
!  open(1, file = 'had_vp.dat')
!  qq2min = log(1E-3_prec); qq2max=log(1._prec)
!  do ido=1,1000
!    qq2 = exp(qq2min+ido*(qq2max-qq2min)/1000)
!    call hadr5n12(-sqrt(qq2),st2,der,errder,deg,errdeg)
!    write(1,*) qq2, der
!  end do
!  close(1)


  print*, "----------------------------------------------"
  print*, "   Test Real Matrixelement"
  print*, "----------------------------------------------"
  !muse setup
  call initflavour(flav='museOld')
  musq = 0.4*Mm**2
  y = (/0.01,0.6,0.8,0.999,0.01/)
  call psx3_fks(y,p1,me,p2,mm,q1,me,q2,mm,q3,weight)
  call check("real",    mp2mpg(p1,p2,q1,q2,q3),         3232.2957734744705,     threshold=1e-8)
  call check("vp3",     mp2mpg_a(p1,p2,q1,q2,q3),       9236.986206838808,      threshold=3e-8)
  call check("nucleon", mp2mpg_mp(p1,p2,q1,q2,q3),      1203.0713754540307,     threshold=1e-8)

  finite=mp2mpgl(p1, p2, q1, q2, q3,single)
  call check("finite:",finite,5123.0906294102315,threshold=2e-9)
  call check("single:",single,1556.5959984999608,threshold=2e-9)

  !mesa setup
  call initflavour(flav='mesaOld')
  musq = 0.4*Mm**2
  y = (/0.01,0.6,0.8,0.999,0.01/)
  call psx3_fks(y,p1,me,p2,mm,q1,me,q2,mm,q3,weight)
  call check("real",    mp2mpg(p1,p2,q1,q2,q3),         3745.5539945814576,     threshold=1e-8)
  call check("vp2",     mp2mpg_a(p1,p2,q1,q2,q3),      9797.457512535439,       threshold=4e-8)

  finite=mp2mpgl(p1, p2, q1, q2, q3,single)
  call check("finite:",finite,122276.90242421204,threshold=2e-9)
  call check("single:",single,13692.78485167788,threshold=2e-9)

  !call cpu_time(t1)
  !do ido1 = 1, 100000
  !  do ido2 = 1,5
  !    y(ido2) = ran2(ran_seed)
  !  enddo
  !  call psx3_fks(y,scms,p1,me,p2,mm,q1,me,q2,mm,q3,weight)
  !  res=mp2mpgl(p1,p2,q1,q2,q3,single)
  !enddo
  !call cpu_time(t2)
  !print*, "time:", t2-t1

  print*, "----------------------------------------------"
  print*, "   Test RV Matrixelement - LBK"
  print*, "----------------------------------------------"
  !muse setup
  call initflavour(flav='museOld')
  musq = Mm**2
  y = (/0.01,0.6,0.8,0.999,0.01/)

  finite=mp2mpgl_lbk(p1, p2, q1, q2, q3,single)
  call check("finite:",finite,134421.5746178126,threshold=2e-9)
  call check("single:",single,13651.74099919805,threshold=2e-9)

  print*, "----------------------------------"
  print*, "   Test RR   Matrixelement"
  print*, "----------------------------------"
  !muse setup
  call initflavour(flav='museOld')
  y8 = (/ 1.4402016072721237E-002, 5.4684134225679658E-002, 7.6243930997440604E-002, 0.43174827398348081,&
  &0.39324084035737505, 0.19880388639811619, 0.29691869313685182, 0.31247555106527919 /)
  call psx4_fkss(y8,p1,me,p2,mm,q1,me,q2,mm,q3,q4,weight)
  call check("RR matrix element", mp2mpgg(p1,p2,q1,q2,q3,q4), 50.30347548212909)
  call check("RR NTS matrix element", mp2mpgg_nts(p1,p2,q1,q2,q3,q4), -20.371118795534898)
  call check("RR NTSS matrix element", mp2mpgg_ntss(p1,p2,q1,q2,q3,q4), -38.60625092144048)


  !mesa setup
  call initflavour(flav='mesaOld')
  y8 = (/ 1.4402016072721237E-002, 5.4684134225679658E-002, 7.6243930997440604E-002, 0.43174827398348081,&
  &0.39324084035737505, 0.19880388639811619, 0.29691869313685182, 0.31247555106527919 /)
  call psx4_fkss(y8,p1,me,p2,mm,q1,me,q2,mm,q3,q4,weight)
  call check("RR matrix element", mp2mpgg(p1,p2,q1,q2,q3,q4), 213.1622536973318)

  call blockend(25)
  END SUBROUTINE

  SUBROUTINE EVENTSMESA
  implicit none
  real (kind=prec) :: y2(2),y5(5),y8(8)
  real (kind=prec) :: q1(4),q2(4),q3(4),q4(4)
  real (kind=prec) :: p1lab(4),p2lab(4),q1lab(4),q2lab(4),q3lab(4)
  real (kind=prec) :: weight,fi,si,li, xicut
  integer ido, ido1
  call initflavour('mesaOld')
  musq = Me**2

  xicut = 2/sqrt(scms)*10._prec !MeV

  ran_seed = 2331
  do ido1 = 1,4
    weight = 0.
    print*,"(* Event id=",ran_seed,"*)"
    print*, "{"
    do while (weight .eq.0)
      do ido = 1,2
        y2(ido) = ran2(ran_seed)
      enddo
      call psx2(y2,p1,me,p2,mm,q1,me,q2,mm,weight)
    end do
    p1lab = boost_rf(p2,p1)
    p2lab = boost_rf(p2,p2)
    q1lab = boost_rf(p2,q1)
    q2lab = boost_rf(p2,q2)
    write(*,900) 1,p1, sqrt(abs(sq(p1)))
    write(*,900) 2,p2, sqrt(abs(sq(p2)))
    write(*,900) 3,q1, sqrt(abs(sq(q1)))
    write(*,900) 4,q2, sqrt(abs(sq(q2)))
    write(*,901) 1,p1lab
    write(*,901) 2,p2lab
    write(*,901) 3,q1lab
    write(*,901) 4,q2lab

    print*,sq(p1+p2),sq(p1-q1)

    write(*,902) "F1 & F2, born", mp2mp(p1,p2,q1,q2),','
    write(*,902) "F1=1, F2=0, born", em2em(p1,p2,q1,q2,li), ','
    fi = mp2mpl(p1,p2,q1,q2,si)
    write(*,903) "F1 & F2, OL", si,fi,','
    fi = em2eml_ee(p1,p2,q1,q2,si)
    write(*,903) "F1=1, F2=0, OL", si,fi - li/em2em(p1,p2,q1,q2) * si

    fi = em2emeik_ee(p1, p2, q1, q2, xicut, si)
    write(*,903) "F1 & F2, eik, cms", si*mp2mp(p1,p2,q1,q2), fi*mp2mp(p1,p2,q1,q2)
    write(*,903) "F1=1, F2=0, eik, cms", si*em2em(p1,p2,q1,q2), fi*em2em(p1,p2,q1,q2)

    fi = em2emeik_ee(p1lab, p2lab, q1lab, q2lab, xicut, si)
    write(*,903) "F1 & F2, eik lab", si*mp2mp(p1lab,p2lab,q1lab,q2lab), fi*mp2mp(p1lab,p2lab,q1lab,q2lab)
    write(*,903) "F1=1, F2=0, eik lab", si*em2em(p1lab,p2lab,q1lab,q2lab), fi*em2em(p1lab,p2lab,q1lab,q2lab)
    print*, "},"
    print*
  enddo

  ran_seed = 2331
  do ido1 = 1,4
    weight = 0.
    print*,"(* Event id=",ran_seed,"*)"
    print*, "{"
    do while (weight .eq.0)
      do ido = 1,5
        y5(ido) = ran2(ran_seed)
      enddo
      select case(ido1)
        case(3)
          y5(2) = 0.999
          call psx3_fks(y5,p1,me,p2,mm,q1,me,q2,mm,q3,weight)
        case(4)
          y5(2) = 0.99
          call psx3_35_fks(y5,p1,me,p2,mm,q1,q2,q3,weight)
        case default
          call psx3_35_fks(y5,p1,me,p2,mm,q1,q2,q3,weight)
      end select
    end do
    p1lab = boost_rf(p2,p1)
    p2lab = boost_rf(p2,p2)
    q1lab = boost_rf(p2,q1)
    q2lab = boost_rf(p2,q2)
    q3lab = boost_rf(p2,q3)
    write(*,900) 1,p1, sqrt(abs(sq(p1)))
    write(*,900) 2,p2, sqrt(abs(sq(p2)))
    write(*,900) 3,q1, sqrt(abs(sq(q1)))
    write(*,900) 4,q2, sqrt(abs(sq(q2)))
    write(*,900) 5,q3, sqrt(abs(sq(q3)))
    write(*,901) 1,p1lab
    write(*,901) 2,p2lab
    write(*,901) 3,q1lab
    write(*,901) 4,q2lab
    write(*,901) 5,q3lab

    write(*,902) "F1 & F2, born", mp2mpg(p1,p2,q1,q2,q3),','
    write(*,902) "F1=1, F2=0, born", em2emg_ee(p1,p2,q1,q2,q3), ','
    fi = mp2mpgl(p1,p2,q1,q2,q3,si)
    write(*,903) "F1 & F2, OL", si,fi,','
    fi = em2emgl_eeee(p1,p2,q1,q2,q3,si)
    write(*,903) "F1=1, F2=0, OL", si,fi

    fi = em2emeik_ee(p1, p2, q1, q2, xicut, si)
    write(*,903) "F1 & F2, eik, cms", si*mp2mpg(p1,p2,q1,q2,q3), fi*mp2mpg(p1,p2,q1,q2,q3)
    write(*,903) "F1=1, F2=0, eik, cms", si*em2emg_ee(p1,p2,q1,q2,q3), fi*em2emg_ee(p1,p2,q1,q2,q3)

    fi = em2emeik_ee(p1lab, p2lab, q1lab, q2lab, xicut, si)
    write(*,903) "F1 & F2, eik, lab", si*mp2mpg(p1lab,p2lab,q1lab,q2lab,q3lab), fi*mp2mpg(p1lab,p2lab,q1lab,q2lab,q3lab)
    write(*,903) "F1=1, F2=0, eik, lab", si*em2emg_ee(p1lab,p2lab,q1lab,q2lab,q3lab), fi*em2emg_ee(p1lab,p2lab,q1lab,q2lab,q3lab)
    print*, "},"
    print*
  enddo

  ran_seed = 2331
  do ido1 = 1,4
    weight = 0.
    print*,"(* Event id=",ran_seed,"*)"
    print*, "{"
    do while (weight .eq.0)
      do ido = 1,8
        y8(ido) = ran2(ran_seed)
      enddo
      call psx4_fkss(y8,p1,me,p2,mm,q1,me,q2,mm,q3,q4,weight)
    end do
    write(*,900) 1,p1, sqrt(abs(sq(p1)))
    write(*,900) 2,p2, sqrt(abs(sq(p2)))
    write(*,900) 3,q1, sqrt(abs(sq(q1)))
    write(*,900) 4,q2, sqrt(abs(sq(q2)))
    write(*,900) 5,q3, sqrt(abs(sq(q3)))
    write(*,900) 6,q4, sqrt(abs(sq(q4)))
    write(*,901) 1,boost_rf(p2,p1)
    write(*,901) 2,boost_rf(p2,p2)
    write(*,901) 3,boost_rf(p2,q1)
    write(*,901) 4,boost_rf(p2,q2)
    write(*,901) 5,boost_rf(p2,q3)
    write(*,901) 6,boost_rf(p2,q4)
    write(*,902) "F1 & F2", mp2mpgg(p1,p2,q1,q2,q3,q4),','
    write(*,902) "F1=1, F2=0", em2emgg_eeee(p1,p2,q1,q2,q3,q4)
    print*, "},"
    print*
  enddo

900 FORMAT("  p",I1,"cms -> { ",F20.14,",",F20.14,",",F20.14,",",F20.14," }, (* sqrt(p^2) = ",F14.10," *)")
901 FORMAT("  p",I1,"lab -> { ",F20.14,",",F20.14,",",F20.14,",",F20.14," },")
902 FORMAT('  ans["',A,'"] -> ',F23.14,A)
903 FORMAT('  ans["',A,'"] -> ',F25.14,"/ep + ", F25.14,A)
  END SUBROUTINE



  SUBROUTINE TESTMUPSOFTN1
  implicit none
  call blockstart("mu-p \xi->0")
  !mesa setup
  call initflavour("mesaOld")
  musq = 0.4*Mm**2

  xinormcut1 = 0.8
  xinormcut2 = 0.8
  call test_softlimit( &
    (/0.8,0.6,0.8,0.999,0.01/), &
    [ "mp2mpR  ", "mp2mpRMP", "mp2mpRF ", "mp2mpAR " ], &
    (/ 10       ,  10       ,  8        ,  10 /))
  END SUBROUTINE



  SUBROUTINE TESTMUPSOFTN2
  implicit none
  real (kind=prec) :: yold(8)

  call blockstart("mu-p \xi_{1,2} -> 0")
  !mesa setup
  call initflavour('mesaOld')
  musq = 0.4*Mm**2
  !muse setup
  call initflavour('museOld')
  musq = 0.4*Mm**2

  yold = (/ 1.4402016072721237E-002, 5.4684134225679658E-002, 7.6243930997440604E-002, 0.43174827398348081,&
  &0.39324084035737505, 0.19880388639811619, 0.29691869313685182, 0.31247555106527919 /)

  call test_softlimit2(yold, "mp2mpRR", (/7,7,7/))
  call blockend(3)
  ENDSUBROUTINE


  SUBROUTINE TESTMUPNTSOFTN1
  use mue_mat_el, only:  mp2mpgl_nts, mp2mpgl, mp2mpgl_pv, mp2mpgl_co
  use mue, only: mp2mpgl_s

  implicit none
  integer, parameter :: ntest = 4
  integer ido,j,lastgood(ntest),eexpectation
  real(kind=prec) :: y(5), weight, pole
  real(kind=prec) :: q1(4), q2(4), q3(4)
  real(kind=prec) :: full(ntest), lim(ntest), lastratio(ntest), ratio(ntest)
  real(kind=prec), parameter :: xicut = .8
  character(len=5) :: red = char(27)//'[31m'
  character(len=5) :: green = char(27)//'[32m'
  character(len=4) :: norm = char(27)//'[0m'
  character(len=5+7+4) :: anscol

  call blockstart("mu-p \xi->0 NTS")
  call initflavour("muse210")
  musq = mm**2
  xieik1 = 0.3
  y = (/0.3,0.6,0.8,0.4,0.9/)
  lastratio = 2.

  write(*,900, advance='no')
  write(*,*) "    gl_co-S         gl_co-NTS       gl_pv-S         gl_pv-NTS"
  write(*,*) repeat("-", 8 + 15 * ntest)

  do ido=1,10
    y(1) = 10.**(-ido)
    call psx3_fks(y,p1,me,p2,mm,q1,me,q2,mm,q3,weight)
    xieik1 = xicut
    full = (/ mp2mpgl_co(p1,p2,q1,q2,q3),       &
              mp2mpgl_co(p1,p2,q1,q2,q3),       &
              mp2mpgl_pv(p1,p2,q1,q2,q3),       &
              mp2mpgl_pv(p1,p2,q1,q2,q3)        /)
    lim =  (/ mp2mpgl_s(p1,p2,q1,q2)/xiout**2,  &
              mp2mpgl_nts(p1,p2,q1,q2,q3,pole), &
              mp2mpgl_s(p1,p2,q1,q2)/xiout**2,  &
              mp2mpgl_nts(p1,p2,q1,q2,q3,pole)  /)

    ratio = abs(1-lim/full)

    write(*,902, advance="no") xiout
    do j=1,ntest
      if (ratio(j).le.lastratio(j)) then
        lastgood(j) = ido
        lastratio(j) = ratio(j)
        write(*,903, advance="no") norm, ratio(j), norm
      else
        write(*,903, advance="no") red, ratio(j), norm
      endif
    enddo
    write(*,*)
  enddo

  eexpectation = 7
  do j=1,ntest
    if(lastgood(j) >= eexpectation) then
      anscol = green // '[PASS]' // norm
      npass = npass + 1
    else
      anscol = red   // '[FAIL]' // norm
      nfail = nfail + 1
    endif
    write(*,904) anscol, j, lastgood(j), eexpectation
  enddo

  call blockend(ntest)

900 format(2x,"xi",3x)
902 format(2x,ES6.0)
903 format(A,ES13.3,3x,A)
904 format(A,' ',I2,': ', I2, ' > ', I2)
  END SUBROUTINE


  SUBROUTINE TESTMUESPEED
  use mue_mat_el
  use olinterface
  implicit none
  integer, parameter :: niter = 50000
  integer, parameter :: niterN = 20
  integer, parameter :: niterRV = 5000
  real (kind=prec) :: q1(4),q2(4),q3(4)
  real (kind=prec) :: weight, arr(5), finite
  real (kind=prec) :: startt, endt, ratepv, ratecol, rateol, rateold, rateNTS
  integer i, j, ranseed, ncall, itmx
  logical itest


  call blockstart("mu-e performance")
  call initflavour('muone')
  musq = mm**2

  ! On my notebook I observed the following event rates
  !   CDR (ref)   :  5.6 kEv/s
  !   FDF (MMA+O3): 53.0 kEv/s
  !   FDF (O3)    : 52.0 kEv/s
  ! It seems that O3 optimisation suffices!
  call switchoffcachesystem_cll

  ranseed = 2332
  call cpu_time(startt)
  do j=1,niterN
    do i=1,2
      arr(i) = ran2(ranseed)
    enddo
    call psx2(arr(1:2),p1,me,p2,mm,q1,me,q2,mm,weight)
    if (weight.lt.zero) cycle
    finite=em2emll0(p1, p2, q1, q2)
  enddo
  call cpu_time(endt)
  rateold=niterN/(endt-startt)
  print*,"two-loop rate is",rateold,"Ev/s"



  ranseed = 2332
  call cpu_time(startt)
  do j=1,niterRV
    do i=1,5
      arr(i) = ran2(ranseed)
    enddo
    arr(1) = arr(1)**4
    call psx3_fks(arr,p1,me,p2,mm,q1,me,q2,mm,q3,weight)
    if (weight.lt.zero) cycle
    call openloops("em2emg", p1, p2, q1, q2, q3, fin=finite)
  enddo
  call cpu_time(endt)
  rateOL=niterRV/(endt-startt)
  print*,"OL RV rate is",rateOL,"Ev/s"

  ranseed = 2332
  ntsSwitch = 1.e-3
  call cpu_time(startt)
  do j=1,niterRV
    do i=1,5
      arr(i) = ran2(ranseed)
    enddo
    arr(1) = arr(1)**4
    call psx3_fks(arr,p1,me,p2,mm,q1,me,q2,mm,q3,weight)
    if (weight.lt.zero) cycle
    finite = em2emgl(p1,p2,q1,q2,q3)
  enddo
  call cpu_time(endt)
  rateNTS=niterRV/(endt-startt)
  print*,"OL+NTS RV rate is",rateNTS,"Ev/s"
  print*, "Speedup is ", rateNTS/rateOL

  ranseed = 2332

  call cpu_time(startt)
  do j=1,niter
    do i=1,5
      arr(i) = ran2(ranseed)
    enddo
    call psx3_fks(arr,p1,me,p2,mm,q1,me,q2,mm,q3,weight)
    if (weight.lt.zero) cycle
    finite=EM2EMGl_eeee_pv(p1, p2, q1, q2, q3)
  enddo
  call cpu_time(endt)
  ratepv=niter/(endt-startt)
  print*,"HYB rate is",ratepv,"Ev/s"

  ranseed = 2332
  call cpu_time(startt)
  do j=1,niter
    do i=1,5
      arr(i) = ran2(ranseed)
    enddo
    call psx3_fks(arr,p1,me,p2,mm,q1,me,q2,mm,q3,weight)
    if (weight.lt.zero) cycle
    finite=EM2EMGl_eeee_co(p1, p2, q1, q2, q3)
  enddo
  call cpu_time(endt)
  ratecol=niter/(endt-startt)
  print*,"COL rate is",ratecol,"Ev/s"

  ranseed = 2332
  call cpu_time(startt)
  do j=1,niter
    do i=1,5
      arr(i) = ran2(ranseed)
    enddo
    call psx3_fks(arr,p1,me,p2,mm,q1,me,q2,mm,q3,weight)
    if (weight.lt.zero) cycle
    finite=EM2EMGl_eeee(p1, p2, q1, q2, q3)
  enddo
  call cpu_time(endt)
  rateol=niter/(endt-startt)
  print*,"OL2 rate is",rateol,"Ev/s"

  call checkrange("speed-up HYB/COL", ratepv/ratecol, 2.,100.)
  call checkrange("speed-up HYB/OL2", ratepv/rateol,  2.,100.)
  call checkrange("speed-up COL/OL2", ratecol/rateol, 2.,100.)
  call blockend(3)

  !! integration speed tests
  itest=.false.

  if(itest) then
     ncall = 1000
     itmx  = 2
     xinormcut = 0.1
     xinormcut1 = 0.2
     xinormcut2 = 0.3

     call cpu_time(startt)
     call calc_INT('em2emRFEEEEpv' , ncall,  itmx)
     call cpu_time(endt)
     ratepv=ncall*1000/(endt-startt)
     print*,"HYB integration time is",ratepv,"call/s"

     ntsSwitch = 0._prec
     call cpu_time(startt)
     call calc_INT('em2emRFEEEE' , ncall,  itmx)
     call cpu_time(endt)
     rateol=ncall*1000*itmx/(endt-startt)
     print*,"OL2 integration time is",rateol,"call/s"
     call checkrange("speed-up [OL2 w/o NTS]", ratepv/rateol, 2.,100.)

     ntsSwitch = 1.e-3
     call cpu_time(startt)
     call calc_INT('em2emRFEEEE' , ncall,  itmx)
     call cpu_time(endt)
     rateol=ncall*1000*itmx/(endt-startt)
     print*,"OL2 integration time is",rateol,"call/s"
     call checkrange("speed-up [OL2 w/ NTS3]", ratepv/rateol, 2.,100.)

     ntsSwitch = 1.e-2
     call cpu_time(startt)
     call calc_INT('em2emRFEEEE' , ncall,  itmx)
     call cpu_time(endt)
     rateol=ncall*1000*itmx/(endt-startt)
     print*,"OL2 integration time is",rateol,"call/s"
     call checkrange("speed-up [OL2 w/ NTS2]", ratepv/rateol, 2.,100.)

     ntsSwitch = 0._prec
     ! COL integration breaks down @ 2nd iteration for ncall>3 ?
     ! call cpu_time(startt)
     ! call calc_INT('em2emRFEEEEcl', ncall,  itmx)
     ! call cpu_time(endt)
     ! ratecol=ncall*1000/(endt-startt)
     ! print*,"COL integration time is",ratecol,"call/s"
     ! call checkrange("speed-up", ratecol/rateol, 2.,100.)

     call blockend(4)
  endif
  END SUBROUTINE

  SUBROUTINE TESTMUPSPEED
  use mue_mat_el
  use olinterface
  implicit none
  integer, parameter :: niter = 50000
  integer, parameter :: niterRV = 5000
  real (kind=prec) :: q1(4),q2(4),q3(4)
  real (kind=prec) :: weight, arr(5), finite
  real (kind=prec) :: startt, endt, ratepv, ratecol, rateol, rateold, rateNTS
  integer i, j, ranseed, ncall, itmx
  logical itest


  call blockstart("mu-p performance")
  call initflavour('muse210')
  musq = mm**2
  call switchoffcachesystem_cll

  ranseed = 2332
  call cpu_time(startt)
  do j=1,niterRV
    do i=1,5
      arr(i) = ran2(ranseed)
    enddo
    arr(1) = arr(1)**4
    call psx3_fks(arr,p1,me,p2,mm,q1,me,q2,mm,q3,weight)
    if (weight.lt.zero) cycle
    finite = mp2mpgl_co(p1, p2, q1, q2, q3)
  enddo
  call cpu_time(endt)
  ratecol=niterRV/(endt-startt)
  print*,"COL RV rate is",ratecol,"Ev/s"

  ranseed = 2332
  call cpu_time(startt)
  do j=1,niter
    do i=1,5
      arr(i) = ran2(ranseed)
    enddo
    arr(1) = arr(1)**4
    call psx3_fks(arr,p1,me,p2,mm,q1,me,q2,mm,q3,weight)
    if (weight.lt.zero) cycle
    finite = mp2mpgl_pv(p1, p2, q1, q2, q3)
  enddo
  call cpu_time(endt)
  ratepv=niter/(endt-startt)
  print*,"HYB RV rate is",ratepv,"Ev/s"

  ranseed = 2332
  call cpu_time(startt)
  do j=1,niterRV
    do i=1,5
      arr(i) = ran2(ranseed)
    enddo
    arr(1) = arr(1)**4
    call psx3_fks(arr,p1,me,p2,mm,q1,me,q2,mm,q3,weight)
    if (weight.lt.zero) cycle
    finite = mp2mpgl_nts(p1,p2,q1,q2,q3)
  enddo
  call cpu_time(endt)
  rateNTS=niterRV/(endt-startt)
  print*,"NTS RV rate is",rateNTS,"Ev/s"

  call checkrange("speed-up HYB/COL", ratepv /ratecol, 2.,100.)
  call checkrange("speed-up NTS/COL", rateNTS/ratecol, 2.,100.)
  call blockend(2)

  !! integration speed tests
  itest=.false.

  if(itest) then
     ncall = 1000
     itmx  = 2
     xinormcut = 0.1
     xinormcut1 = 0.2
     xinormcut2 = 0.3

     call cpu_time(startt)
     call calc_INT('mp2mpRFcol' , ncall,  itmx)
     call cpu_time(endt)
     ratecol=ncall*1000/(endt-startt)
     print*,"COL integration time is",ratecol,"call/s"

     ntsSwitch = 0._prec
     call cpu_time(startt)
     call calc_INT('mp2mpRFpv' , ncall,  itmx)
     call cpu_time(endt)
     ratepv=ncall*1000*itmx/(endt-startt)
     print*,"HYB integration time is",ratepv,"call/s"

     ntsSwitch = 1.e-3
     call cpu_time(startt)
     call calc_INT('mp2mpRF' , ncall,  itmx)
     call cpu_time(endt)
     ratents=ncall*1000*itmx/(endt-startt)
     print*,"COL+NTS integration time is",ratents,"call/s"
     call blockend(2)
  endif
  END SUBROUTINE

  SUBROUTINE CALC_INT(piece, ncall, itmx)
  implicit none
  character (len=*) :: piece
  real(kind=prec) :: avgi, sd, chi2a
  real(kind=prec) :: arr(maxdim)
  real(kind=prec) :: threshold
  integer, optional :: ncall, itmx
  integer ran_seed, ndim, i, j
  procedure(integrand), pointer :: fxn

  ran_seed = 12145
  which_piece = piece
  call initpiece(ndim, fxn)
  call vegas(ndim,1000*ncall,itmx,fxn,avgi,sd,chi2a,ran_seed, silent=.true.)
  print*, avgi, sd, ", chi2 = ",chi2a
  END SUBROUTINE


  SUBROUTINE TESTMUEVEGAS
  xinormcut = 0.1
  xinormcut1 = 0.2
  xinormcut2 = 0.3

  Nel = 1
  Nmu = 1
  Ntau = 1
  Nhad = 1

  flavour = 'muone'

  call blockstart("mu-e VEGAS test")
  call initflavour('muone')
  musq = Mm**2
  ntsSwitch = 1.e-3

  call test_INT("em2em0"             , 10, 10,  1.1828190320282846E-02)

  call test_INT("em2emC"             , 10, 10, -3.1837803940814191E-01)
  call test_INT("em2emV"             , 10, 10,  2.1718958240719444E-01)
  call test_INT("em2emFEE"           , 10, 10, -9.8182293423156555E-02)
  call test_INT("em2emREE"           , 10, 10,  8.9793211079415527E-02, tol=1.E-6)
  call test_INT("em2emREE15"         , 10, 10,  9.9069827746005770E-02, tol=1.E-7)
  call test_INT("em2emREE35"         , 10, 10,  2.8401993592570753E-02)
  call test_INT("em2emREM15"         , 10, 10, -4.5472578440805886E-04)
  call test_INT("em2emREM35"         , 10, 10, -5.3027026090027719E-04)
  call test_INT("em2emFEM"           , 10, 10, -1.3995791247170839E-03)
  call test_INT("em2emREM"           , 10, 10, -9.9824165503435746E-04)
  call test_INT("em2emFMM"           , 10, 10, -1.6553505967280009E-03)
  call test_INT("em2emRMM"           , 10, 10,  1.2100151733008188E-03)

  call test_INT("em2emFFEEEE"        , 10, 10,  4.1721179300339167E-01)
  call test_INT("em2emFFEEEEz"       , 10, 10,  4.1703895613078940E-01)
  ! call test_INT("em2emFFMIXDz"       ,  1,  1,  4.1721179300339167E-01)
  call test_INT("em2emFFMMMM"        , 10, 10,  6.2305317294193463E-05)
  ! call test_INT("em2emFFz"           ,  1,  1,  4.1721179300339167E-01)
  call test_INT("em2emRFEEEE"        , 50,  2,  1.9790880220977414E-01, tol=1.E-7)
  call test_INT("em2emRFEEEE15"      , 50,  2,  1.2804998912855775E-01, tol=1.E-7)
  call test_INT("em2emRFEEEE35"      , 50,  2,  3.5952725449300681E-03, tol=1.E-5)
  call test_INT("em2emRFMIXD"        , 10,  2, -5.8395193653247805E-03, tol=1.E-7)
  call test_INT("em2emRFMIXD15"      ,  1,  1, -1.5066445594392852E-03, tol=1.E-7)
  call test_INT("em2emRFMIXD35"      ,  1,  1, -1.5575823928316347E-03, tol=1.E-7)
  call test_INT("em2emRFMMMM"        , 50,  2,  1.5278071672642754E-04, tol=1.E-7)
  call test_INT("em2emRFMMMM"        , 50,  2,  1.5278071672642754E-04, tol=1.E-7)
  call test_INT("em2emRF"            ,  1,  1, -3.6744793800357607E-03, tol=1.E-7)
  call test_INT("em2emRF15"          ,  1,  1, -3.7343197460904531E-03, tol=1.E-7)
  call test_INT("em2emRF35"          ,  1,  1,  1.2955943414645109E-03, tol=1.E-7)
  call test_INT("em2emRREEEE"        ,100, 10, -1.0417588502684634E-01, tol=1.E-1)
  call test_INT("em2emRREEEE1516"    ,100, 10, -1.0913340771191980E-01, tol=1.E-3)
  call test_INT("em2emRREEEE3536"    ,100, 10, -5.9013050720738383E-02, tol=1.E-5)
  call test_INT("em2emRRMIXD"        ,100, 10, -9.1868559066880350E-04, tol=1.E-1)
  call test_INT("em2emRRMIXD1516"    ,100, 10,  8.5348275946470588E-04, tol=1.E-3)
  call test_INT("em2emRRMIXD3536"    ,100, 10,  4.5094095188346039E-04, tol=1.E-5)
  call test_INT("em2emRRMMMM"        ,100, 10, -6.5913893937307876E-05, tol=1.E-1)

  call test_INT("em2emA"             , 10, 10,  1.9196483482967701E-02)
  call test_INT("em2emAA"            , 10, 10,  3.1847666434564673E-02)
  call test_INT("em2emAFEE"          , 10, 10, -1.6146714668455509E-01)
  call test_INT("em2emAFEM"          , 10, 10,  2.4427868898292998E-03)
  call test_INT("em2emAFMM"          , 10, 10, -3.1538929495060221E-03)
  call test_INT("em2emAF"            , 10, 10, -1.6211476973636807E-01)
  call test_INT("em2emAREE"          , 10, 10,  1.1106892744817531E-01, tol=1.E-4)
  call test_INT("em2emAREE15"        , 10, 10,  1.4568049768271454E-01, tol=1.E-7)
  call test_INT("em2emAREE35"        , 10, 10,  4.8674558499990181E-02)
  call test_INT("em2emAREM"          , 10, 10, -1.9733310226774992E-03)
  call test_INT("em2emARMM"          , 10, 10,  2.2727890923935545E-03)
  call test_INT("em2emAR"            , 10, 10,  1.4160595573873599E-01)
  call test_INT("em2emAR15"          , 10, 10,  1.5552556946968793E-01)
  call test_INT("em2emAR35"          , 10, 10,  4.9429545709474297E-02)
  call test_INT("em2emNFEE"          , 10, 10, -2.4830274907217065E-02)
  call test_INT("em2emNFEM"          , 10, 10, -6.5512238141363218E-03)
  call test_INT("em2emNFEMCT"        , 10, 10,  3.5004944545691742E-03)
  call test_INT("em2emNFMM"          , 10, 10, -1.8624142033332220E-03)
  call test_INT("em2emNF"            , 10, 10, -3.3219413473060601E-02)
  call blockend(40)
  END SUBROUTINE


  SUBROUTINE TESTMUEPOINTWISE
  xinormcut = 1
  xinormcut1 = 1
  xinormcut2 = 1

  Nel = 1
  Nmu = 1
  Ntau = 1
  Nhad = 1

  flavour = 'muone'

  call blockstart("mu-e VEGAS test")
  call initflavour('muone')
  musq = Mm**2
  ntsSwitch = 1.e-3

  call test_INT("em2em0"             , ans= 2.4464166786260526E-04)

  call test_INT("em2emC"             , ans=-3.7524331702516920E-03)
  call test_INT("em2emV"             , ans= 4.8088819095193089E-03)
  call test_INT("em2emFEE"           , ans= 1.2191662259964499E-03, tol=2.E-8)
  call test_INT("em2emREE"           , ans=-7.8003661339328453E-05)
  call test_INT("em2emREE15"         , ans=-9.7737833142204223E-06)
  call test_INT("em2emREE35"         , ans=-1.7521920841767456E-04)
  call test_INT("em2emFEM"           , ans=-2.1607647845861775E-04)
  call test_INT("em2emREM"           , ans= 1.2054787998591740E-04)
  call test_INT("em2emREM15"         , ans= 9.5956006482299548E-05)
  call test_INT("em2emREM35"         , ans= 8.3957283270143343E-04)
  call test_INT("em2emFMM"           , ans= 5.3358992051081596E-05)
  call test_INT("em2emRMM"           , ans=-4.6043288910698485E-05)

  call test_INT("em2emFFEEEE"        , ans= 3.2574700799409221E-03)
  call test_INT("em2emFFEEEEz"       , ans= 3.2577448378380259E-03)
  call test_INT("em2emFFMIXDz"       , ans=-3.0842029025898504E-04)
  call test_INT("em2emFFMMMM"        , ans=-3.9047890920774967E-05)
  call test_INT("em2emFFz"           , ans= 2.9102764119079464E-03)
  call test_INT("em2emRFEEEE"        , ans=-9.9053586000782609E-05)
  call test_INT("em2emRFEEEE15"      , ans=-3.9815329248633793E-06)
  call test_INT("em2emRFEEEE35"      , ans=-6.9670326236212065E-04)
  call test_INT("em2emRFMIXD"        , ans= 7.7406829956502311E-05)
  call test_INT("em2emRF"            , ans= 7.0373754691987491E-06)
  call test_INT("em2emRF15"          , ans=-3.0078066192121001E-04)
  call test_INT("em2emRF35"          , ans=-7.8972611897450314E-05)
  call test_INT("em2emRFMMMM"        , ans= 2.8684131513500731E-05)
  call test_INT("em2emRREEEE"        , ans=-3.4272527120708879E-04)
  call test_INT("em2emRREEEE1516"    , ans=-3.4272527120708879E-04)
  call test_INT("em2emRREEEE3536"    , ans=-3.5556097489341750E-05)
  call test_INT("em2emRRMIXD"        , ans= 2.6120693556480113E-04)
  call test_INT("em2emRRMIXD1516"    , ans= 2.6120693556480113E-04)
  call test_INT("em2emRRMIXD3536"    , ans=-2.1434241047402604E-04)
  call test_INT("em2emRRMMMM"        , ans=-2.5834728858936082E-04)

  call test_INT("em2emA"             , ans= 6.3813277703549532E-04)
  call test_INT("em2emAA"            , ans= 1.4923542332920727E-03)
  call test_INT("em2emAFEE"          , ans= 3.1801202806544400E-03)
  call test_INT("em2emAFEM"          , ans= 2.2394716920108759E-03)
  call test_INT("em2emAFMM"          , ans= 1.3918365614027265E-04)
  call test_INT("em2emAF"            , ans= 5.5587756288055885E-03)
  call test_INT("em2emAREE"          , ans=-2.4462187392154406E-04)
  call test_INT("em2emAREE15"        , ans=-3.9319427475270047E-05)
  call test_INT("em2emAREE35"        , ans=-3.7703790479926393E-04)
  call test_INT("em2emAREM"          , ans= 3.7920639570366427E-04)
  call test_INT("em2emARMM"          , ans=-1.4554608184126324E-04)
  call test_INT("em2emAR"            , ans=-1.0961560059142867E-05)
  call test_INT("em2emAR15"          , ans=-2.1563560100371564E-04)
  call test_INT("em2emAR35"          , ans=-8.4540811238618648E-04)
  call test_INT("em2emNFEE"          , ans=-2.0746553570694952E-04)
  call test_INT("em2emNFEM"          , ans=-4.4987599849928512E-04)
  call test_INT("em2emNFMM"          , ans=-6.4556021851858256E-04)
  call test_INT("em2emNFEMCT"        , ans= 2.5212828087669286E-03)
  call test_INT("em2emNF"            , ans=-1.3029017527248265E-03)

  call blockend(40)
  END SUBROUTINE


  SUBROUTINE TESTMUPVEGAS
  ! set kappa and lambda
  call initflavour(flav='museOld')
  call initflavour('mesa')
  musq = Mm**2
  xinormcut = 0.1
  xinormcut1 = 0.2
  xinormcut2 = 0.3

  Nel = 1
  Nmu = 1
  Ntau = 1
  Nhad = 1

  flavour = 'mesa'

  call blockstart("mu-p VEGAS test")

  call test_INT("mp2mp0"             , 10, 10,  1.1288760501500964E-02)
  call test_INT("mp2mp0nuc"          , 10, 10,  1.1288760501500964E-02)

  call test_INT("mp2mpF"             , 10, 10, -9.3350531463556194E-02)
  call test_INT("mp2mpR"             , 10, 10,  1.0744517250477321E-01)
  call test_INT("mp2mpR15"           , 10, 10,  7.9209596989839703E-02)
  call test_INT("mp2mpR35"           , 10, 10,  2.7054301212070489E-02)

  call test_INT("mp2mpFMP"           , 10, 10,  4.5688464055550536E-03)
  call test_INT("mp2mpRMP"           , 10, 10,  2.8622662749542264E-04)
  call test_INT("mp2mpRMP15"         , 10, 10,  1.2406187274854974E-04)
  call test_INT("mp2mpRMP35"         , 10, 10,  1.6392945556012315E-04)

  call test_INT("mp2mpFF"            , 50,  2,  3.9432973059556387E-01)
  call test_INT("mp2mpRF"            , 50,  2,  9.7970185614514760E-02)
  call test_INT("mp2mpRF15"          , 50,  2,  7.8273655663961064E-02)
  call test_INT("mp2mpRF35"          , 50,  2,  5.7626705429340190E-03)
  call test_INT("mp2mpRR"            ,100, 10, -1.3107286879361443E-01)
  call test_INT("mp2mpRR1516"        ,100, 10, -8.7864570380921367E-02)
  call test_INT("mp2mpRR3536"        ,100, 10, -3.6599220698317933E-02)

  call test_INT("mp2mpA"             , 10, 10,  1.8099652778700664E-02)
  call test_INT("mp2mpAA"            , 10, 10,  2.9759398791662801E-02)
  call test_INT("mp2mpAF"            , 10, 10, -1.5149425206903894E-01)
  call test_INT("mp2mpAR"            , 10, 10,  1.6028889326446977E-01)
  call test_INT("mp2mpAR15"          , 10, 10,  1.1744913584277478E-01)
  call test_INT("mp2mpAR35"          , 10, 10,  4.7022245663826463E-02)
  call test_INT("mp2mpNF"            , 10, 10, -2.2992211670642918E-02)
  call blockend(19)
  END SUBROUTINE


  SUBROUTINE TESTMUPPOINTWISE
  call initflavour(flav='museOld')
  call initflavour('mesa')
  musq = Mm**2
  xinormcut = 1.0
  xinormcut1 = 1.0
  xinormcut2 = 1.0

  Nel = 1
  Nmu = 1
  Ntau = 1
  Nhad = 1

  flavour = 'mesa'
  call blockstart("mu-p pointwise test")

  call test_INT("mp2mp0"             , ans= 3.9511734121007040E-04)
  call test_INT("mp2mp0nuc"          , ans= 3.9511734121007040E-04)

  call test_INT("mp2mpF"             , ans= 1.8334429288797897E-03)
  call test_INT("mp2mpR"             , ans=-3.0920661743903070E-04)
  call test_INT("mp2mpR15"           , ans= 2.0415802186715213E-03)
  call test_INT("mp2mpR35"           , ans= 2.4624782299683941E-04)

  call test_INT("mp2mpFMP"           , ans= 4.2039807956150880E-04)
  call test_INT("mp2mpRMP"           , ans=-1.2766603710752373E-04)
  call test_INT("mp2mpRMP15"         , ans=-2.5209221746564465E-05)
  call test_INT("mp2mpRMP35"         , ans= 9.2328778533745610E-05)

  call test_INT("mp2mpFF"            , ans= 4.5723855612176375E-03)
  call test_INT("mp2mpRF"            , ans= 1.0150303607914819E-03)
  call test_INT("mp2mpRF15"          , ans= 2.6957817689421640E-02)
  call test_INT("mp2mpRF35"          , ans= 1.9709415396528059E-03)
  call test_INT("mp2mpRR"            , ans= 1.5206301976153228E-04)
  call test_INT("mp2mpRR1516"        , ans= 4.4656209229695952E-02)
  call test_INT("mp2mpRR3536"        , ans=-9.6456234099790198E-05)

  call test_INT("mp2mpA"             , ans= 9.1980275722949509E-04)
  call test_INT("mp2mpAA"            , ans= 1.9636495618017688E-03)
  call test_INT("mp2mpAF"            , ans= 4.2681140140339935E-03)
  call test_INT("mp2mpAR"            , ans=-9.3117441475751381E-04)
  call test_INT("mp2mpAR15"          , ans= 2.6138396455272633E-03)
  call test_INT("mp2mpAR35"          , ans= 5.6402339026568053E-04)
  call test_INT("mp2mpNF"            , ans=-3.1302172583685326E-04)


  call blockend(19)
  END SUBROUTINE





                 !!!!!!!!!!!!!!!!!!!!!!!!!
                    END MODULE MUE_TEST
                 !!!!!!!!!!!!!!!!!!!!!!!!!
