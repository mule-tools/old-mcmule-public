
                          !!!!!!!!!!!!!!!!!!!!!!
                              MODULE mudec_TLEPS1
                          !!!!!!!!!!!!!!!!!!!!!!
  contains
      function TLEPS1(EL,GF,MOU2,MIN2,p1p2,p1p4,p2p4,asym,np2,np3, &
         np4,mu2)
!
      use global_def, only: pi, prec
      implicit none
!

    complex(kind=prec) A0, B0i
    complex(kind=prec) B0
    complex(kind=prec) DB0
    complex(kind=prec) C0, C0i
    complex(kind=prec) D0, D0i

    integer, parameter :: bb0  = 1
    integer, parameter :: bb1  = 4
    integer, parameter :: cc0  = 1
    integer, parameter :: cc1  = 4
    integer, parameter :: cc2  = 7
    integer, parameter :: cc00 = 10
    integer, parameter :: cc11 = 13
    integer, parameter :: cc12 = 16
    integer, parameter :: cc22 = 19
    integer, parameter :: dd0  = 1
    integer, parameter :: dd1  = 4
    integer, parameter :: dd2  = 7
    integer, parameter :: dd3  = 10
    integer, parameter :: dd00 = 13



    external A0, B0i
    external B0
    external DB0
    external C0, C0i
    external D0, D0i


!
      real(kind=prec) TLEPS1,EL,GF,MOU2,MIN2, &
         p1p2,p1p4,p2p4,asym, &
         np2,np3,np4,mu2, &
         aa,bb
!
      aa=0._prec
      bb=-1._prec
!

!
!      call setlambda(-1._prec)
!      call setdelta(0._prec)
!      call setmudim(mu2)
!      call setminmass(0._prec)
!

!
      TLEPS1=real((16*EL**2*GF**2*(2*MOU2**2*p1p4* &
           (7*MIN2*p1p4 - 9*sqrt(MIN2)*np4*p2p4 +  &
             9*p1p4*(-p1p2 + p2p4)) +  &
          2*sqrt(MIN2)*np2* &
           (9*MOU2**2*p1p4**2 +  &
             (p1p4 - p2p4)*(9*MIN2 + 16*(-p1p2 + p2p4))* &
              (p1p4*(-2*p1p2 + p2p4) + MIN2*(p1p4 + p2p4)) +  &
             MOU2*(9*MIN2*(-2*p1p4**2 + p2p4**2) +  &
                p1p4*(34*p1p2*p1p4 - 18*p1p2*p2p4 -  &
                   25*p1p4*p2p4 + 9*p2p4**2))) +  &
          MOU2*(18*MIN2**1.5*np4*(2*p1p4 - p2p4)*p2p4 +  &
             14*MIN2**2*(-2*p1p4**2 + p2p4**2) -  &
             4*p1p4*(8*p1p4**3 +  &
                p1p2**2*(17*p1p4 - 9*p2p4) -  &
                16*p1p4**2*p2p4 + 25*p1p4*p2p4**2 -  &
                9*p2p4**3 + p1p2*p2p4*(-25*p1p4 + 9*p2p4)) -  &
             2*MIN2*(32*p1p4**2*p2p4 - 9*p2p4**3 +  &
                p1p2*(-46*p1p4**2 + 14*p1p4*p2p4 + 9*p2p4**2) &
                ) + sqrt(MIN2)* &
              (3*CMPLX(aa,bb)*asym*p2p4*(-p1p4 + p2p4) +  &
                2*np4*(16*p1p4**3 - 32*p1p4**2*p2p4 -  &
                   25*p1p4*(p1p2 - 2*p2p4)*p2p4 +  &
                   9*(p1p2 - 2*p2p4)*p2p4**2))) +  &
          (p1p4 - p2p4)* &
           (-18*MIN2**2.5*np4*p2p4 +  &
             14*MIN2**3*(p1p4 + p2p4) -  &
             8*p1p4*(p1p2 - p2p4)* &
              (8*p1p2**2 + 8*p1p4**2 - 8*p1p2*p2p4 -  &
                8*p1p4*p2p4 + 7*p2p4**2) +  &
             4*MIN2*(8*p1p4**3 - 8*p1p4**2*p2p4 +  &
                13*p1p4*p2p4**2 + 8*p2p4**3 +  &
                p1p2**2*(31*p1p4 + 8*p2p4) -  &
                p1p2*p2p4*(39*p1p4 + 16*p2p4)) +  &
             MIN2**2*(46*p2p4*(p1p4 + p2p4) -  &
                2*p1p2*(37*p1p4 + 23*p2p4)) +  &
             MIN2**1.5*(3*CMPLX(aa,bb)*asym*p2p4 +  &
                np4*(-32*p1p4**2 + 50*p1p2*p2p4 +  &
                   32*p1p4*p2p4 - 68*p2p4**2)) +  &
             sqrt(MIN2)* &
              (3*CMPLX(aa,bb)*asym*p1p4*(-p1p2 + p1p4 + p2p4) -  &
                8*np4*(-8*p1p2*p1p4**2 + 4*p1p2**2*p2p4 +  &
                   8*p1p2*p1p4*p2p4 + 8*p1p4**2*p2p4 -  &
                   11*p1p2*p2p4**2 - 8*p1p4*p2p4**2 +  &
                   7*p2p4**3)))))/ &
      (9.*p1p4**2*(p1p4 - p2p4)**2))
!
!	  T1BOXAC=T1BOXAC+real(
!
      return
!
      end function

                          !!!!!!!!!!!!!!!!!!!!!!
                            END MODULE mudec_TLEPS1
                          !!!!!!!!!!!!!!!!!!!!!!
