                          !!!!!!!!!!!!!!!!!!!!!!
                             MODULE mudec_h20ff
                          !!!!!!!!!!!!!!!!!!!!!!
    use functions
    implicit none

    real(kind=prec), parameter :: zeta(2:5) = (/1.6449340668482264365, &
                                                1.2020569031595942854, &
                                                1.0823232337111381915, &
                                                1.0369277551433699263 /)

    real(kind=prec), parameter :: ln2 = 0.69314718055994530942

    integer, parameter :: fac(1:7) = (/1, 2, 6, 24, 120, 720, 5040 /)
  contains

  SUBROUTINE GETHPLS(x, Hr1, Hr2, Hr3, Hr4, H2)
  implicit none
  integer, parameter :: n1 = -1
  integer, parameter :: n2 = +1
  integer, parameter :: nw =  4
  complex(kind=16) Hc1(-1:+1)
  complex(kind=16) Hc2(-1:+1,-1:+1)
  complex(kind=16) Hc3(-1:+1,-1:+1,-1:+1)
  complex(kind=16) Hc4(-1:+1,-1:+1,-1:+1,-1:+1)

  real(kind=8) Hi1(-1:+1)
  real(kind=8) Hi2(-1:+1,-1:+1)
  real(kind=8) Hi3(-1:+1,-1:+1,-1:+1)
  real(kind=8) Hi4(-1:+1,-1:+1,-1:+1,-1:+1)

  real(kind=8) Hr1(-1:+1)
  real(kind=8) Hr2(-1:+1,-1:+1)
  real(kind=8) Hr3(-1:+1,-1:+1,-1:+1)
  real(kind=8) Hr4(-1:+1,-1:+1,-1:+1,-1:+1)
  real(kind=prec) :: H2(2)
  real(kind=prec), intent(in) :: x
  call hplog(x, nw, Hc1, Hc2, Hc3, Hc4, &
                    Hr1, Hr2, Hr3, Hr4, &
                    Hi1, Hi2, Hi3, Hi4, n1, n2)

  H2(1) = log(1-0.5*x)
  H2(2) = 0.5*log(1-x)**2 * log(2-x) + log(1-x) * Li2(x-1) - Li3(x-1) - 3. * zeta(3) / 4.
  END SUBROUTINE

  SUBROUTINE H20FF(x, Ls, oneloop1, twoloop1, oneloop2, twoloop2)
  implicit none
  real(kind=prec), intent(in) :: x, Ls
  real(kind=prec), optional, intent(out) :: oneloop1(:), twoloop1(:)
  real(kind=prec), optional, intent(out) :: oneloop2(:), twoloop2(:)
  real(kind=prec) :: xpow(-1:4), omx(-4:-1)
  real(kind=prec) :: bit(-4:2), Lbit(-4:2)
  integer i, j

  real(kind=8) Hr1(-1:+1), H2(1:2)
  real(kind=8) Hr2(-1:+1,-1:+1)
  real(kind=8) Hr3(-1:+1,-1:+1,-1:+1)
  real(kind=8) Hr4(-1:+1,-1:+1,-1:+1,-1:+1)

  call gethpls(x, Hr1, Hr2, Hr3, Hr4, H2)

  xpow = x**(/-1, 0, 1, 2, 3, 4/)
  omx = (1-x)**(/-4, -3, -2, -1/)

  if (present(oneloop1)) then
    bit = 0.
    bit(-2) = -1

    bit(-1) = -2.5
    bit(-1) = bit(-1) + Hr1(1) * (-2)

    bit(0) = -5.5
    bit(0) = bit(0) + Hr1(1) * (-3)
    bit(0) = bit(0) + Hr2(0,1) * (-2)
    bit(0) = bit(0) + Hr2(1,1) * (-4)
    bit(0) = bit(0) + zeta(2) * (-1)

    if(size(oneloop1) > 3) then
      bit(1) = -11.5
      bit(1) = bit(1) + Hr1(1) * (-7)
      bit(1) = bit(1) + Hr1(1)*zeta(2) * (-2)
      bit(1) = bit(1) + Hr2(0,1) * (-3)
      bit(1) = bit(1) + Hr2(1,1) * (-6)
      bit(1) = bit(1) + Hr3(0,0,1) * (-2)
      bit(1) = bit(1) + Hr3(0,1,1) * (-4)
      bit(1) = bit(1) + Hr3(1,0,1) * (-4)
      bit(1) = bit(1) + Hr3(1,1,1) * (-8)
      bit(1) = bit(1) + zeta(2) * (-2.5)

      bit(2) = -23.5
      bit(2) = bit(2) + Hr1(1) * (-15)
      bit(2) = bit(2) + Hr1(1)*zeta(2) * (-3)
      bit(2) = bit(2) + Hr2(0,1) * (-7)
      bit(2) = bit(2) + Hr2(0,1)*zeta(2) * (-2)
      bit(2) = bit(2) + Hr2(1,1) * (-14)
      bit(2) = bit(2) + Hr2(1,1)*zeta(2) * (-4)
      bit(2) = bit(2) + Hr3(0,0,1) * (-3)
      bit(2) = bit(2) + Hr3(0,1,1) * (-6)
      bit(2) = bit(2) + Hr3(1,0,1) * (-6)
      bit(2) = bit(2) + Hr3(1,1,1) * (-12)
      bit(2) = bit(2) + Hr4(0,0,0,1) * (-2)
      bit(2) = bit(2) + Hr4(0,0,1,1) * (-4)
      bit(2) = bit(2) + Hr4(0,1,0,1) * (-4)
      bit(2) = bit(2) + Hr4(0,1,1,1) * (-8)
      bit(2) = bit(2) + Hr4(1,0,0,1) * (-4)
      bit(2) = bit(2) + Hr4(1,0,1,1) * (-8)
      bit(2) = bit(2) + Hr4(1,1,0,1) * (-8)
      bit(2) = bit(2) + Hr4(1,1,1,1) * (-16)
      bit(2) = bit(2) + zeta(2) * (-5.5)
      bit(2) = bit(2) + zeta(4) * (-1.75)
    endif

    bit = bit / 2.

    Lbit = 0.
    do i=1,7
      do j=1,i-1
        Lbit(i-5) = Lbit(i-5) + (1*Ls)**j / fac(j) * bit(i-5-j)
      enddo
    enddo
    select case(size(oneloop1))
      case(5)
        oneloop1 = bit(-2:2) + Lbit(-2:2)
      case(4)
        oneloop1 = bit(-2:1) + Lbit(-2:1)
      case(3)
        oneloop1 = bit(-2:0) + Lbit(-2:0)
      case(1)
        oneloop1 = bit(0:0) + Lbit(0:0)
      case default
        call crash("H20FF")
    end select
  endif
  if (present(twoloop1)) then
    bit(-4) = 0.5

    bit(-3) = 2.5
    bit(-3) = bit(-3) + Hr1(1) * (2)

    bit(-2) = 8.625
    bit(-2) = bit(-2) + Hr1(1) * (8)
    bit(-2) = bit(-2) + Hr2(0,1) * (2)
    bit(-2) = bit(-2) + Hr2(1,1) * (8)
    bit(-2) = bit(-2) + zeta(2) * (1)

    bit(-1) = 24.375
    bit(-1) = bit(-1) + Hr1(1) * (25.5)
    bit(-1) = bit(-1) + Hr1(1)*zeta(2) * (4)
    bit(-1) = bit(-1) + Hr2(0,1) * (8)
    bit(-1) = bit(-1) + Hr2(1,1) * (28)
    bit(-1) = bit(-1) + Hr3(0,0,1) * (2)
    bit(-1) = bit(-1) + Hr3(0,1,1) * (12)
    bit(-1) = bit(-1) + Hr3(1,0,1) * (8)
    bit(-1) = bit(-1) + Hr3(1,1,1) * (32)
    bit(-1) = bit(-1) + zeta(2) * (8)
    bit(-1) = bit(-1) + zeta(3) * (-6)

    bit(0) = 76.8125
    bit(0) = bit(0) + H2(1)*zeta(2) * (-6*omx(-4)*(-12 + 6*x - 5*xpow(2) + 2*xpow(3)))
    bit(0) = bit(0) + H2(2) * (-2*omx(-4)*(-12 + 6*x - 5*xpow(2) + 2*xpow(3)))
    bit(0) = bit(0) + Hr1(1) * ((omx(-1)*xpow(-1)*(2 + 135*x - 141*xpow(2)))/2.)
    bit(0) = bit(0) + Hr1(1)*zeta(2) * (-4*omx(-3)*(-28 - 44*x - 37*xpow(2) + 7*xpow(3)))
    bit(0) = bit(0) + Hr1(-1)*zeta(2) * (-4*omx(-3)*(9 + x - 3*xpow(2) + 5*xpow(3)))
    bit(0) = bit(0) + Hr1(1)*zeta(3) * (-8)
    bit(0) = bit(0) + Hr2(0,1) * ((omx(-3)*xpow(-1)*(-4 + 41*x + 69*xpow(2) + 171*xpow(3) + 23*xpow(4)))/2.)
    bit(0) = bit(0) + Hr2(0,1)*zeta(2) * (4*omx(-4)*(4 + 52*x + 43*xpow(2) + 2*xpow(3) + xpow(4)))
    bit(0) = bit(0) + Hr2(0,-1)*zeta(2) * (-8*omx(-4)*(1 + 8*xpow(2) - 4*xpow(3) + xpow(4)))
    bit(0) = bit(0) + Hr2(1,1) * (omx(-2)*(-23 - 178*x + 51*xpow(2)))
    bit(0) = bit(0) + Hr2(1,1)*zeta(2) * (16)
    bit(0) = bit(0) + Hr3(0,0,1) * (-2*omx(-4)*(5 + 22*x - 4*xpow(2) - 20*xpow(3) + 6*xpow(4)))
    bit(0) = bit(0) + Hr3(0,1,1) * (2*omx(-4)*(-14 - 138*x + 209*xpow(2) - 78*xpow(3) + 30*xpow(4)))
    bit(0) = bit(0) + Hr3(1,0,1) * (-4*omx(-3)*(-14 - 10*x - 25*xpow(2) + 7*xpow(3)))
    bit(0) = bit(0) + Hr3(-1,0,1) * (-8*omx(-3)*(9 + x - 3*xpow(2) + 5*xpow(3)))
    bit(0) = bit(0) + Hr3(1,1,1) * (104)
    bit(0) = bit(0) + Hr4(0,0,0,1) * (-2*omx(-4)*(5 + 4*x + 32*xpow(2) - 8*xpow(3) + 3*xpow(4)))
    bit(0) = bit(0) + Hr4(0,0,1,1) * (4*omx(-4)*(1 - 60*x - 12*xpow(2) - 16*xpow(3) + 3*xpow(4)))
    bit(0) = bit(0) + Hr4(0,-1,0,1) * (-16*omx(-4)*(1 + 8*xpow(2) - 4*xpow(3) + xpow(4)))
    bit(0) = bit(0) + Hr4(0,1,0,1) * (4*omx(-4)*(4 + 12*x + 33*xpow(2) - 10*xpow(3) + 3*xpow(4)))
    bit(0) = bit(0) + Hr4(0,1,1,1) * (56)
    bit(0) = bit(0) + Hr4(1,0,0,1) * (-8)
    bit(0) = bit(0) + Hr4(1,0,1,1) * (48)
    bit(0) = bit(0) + Hr4(1,1,0,1) * (32)
    bit(0) = bit(0) + Hr4(1,1,1,1) * (128)
    bit(0) = bit(0) + ln2*zeta(2) * (-6*omx(-4)*(-32 + 62*x - 53*xpow(2) + 10*xpow(3) + 4*xpow(4)))
    bit(0) = bit(0) + Ls * (1)
    bit(0) = bit(0) + zeta(2) * (-(omx(-3)*(171 - 1161*x - 503*xpow(2) + 77*xpow(3)))/4.)
    bit(0) = bit(0) + zeta(3) * ((omx(-4)*(-74 + 214*x - 169*xpow(2) + 42*xpow(3) + 14*xpow(4)))/2.)
    bit(0) = bit(0) + zeta(4) * (-(omx(-4)*(81 + 284*x + 655*xpow(2) - 162*xpow(3) + 54*xpow(4))))

    bit = bit / 4.

    Lbit = 0.
    do i=1,7
      do j=1,i-1
        Lbit(i-5) = Lbit(i-5) + (2*Ls)**j / fac(j) * bit(i-5-j)
      enddo
    enddo
    select case(size(twoloop1))
      case(5)
        twoloop1 = bit(-4:0) + Lbit(-4:0)
      case(1)
        twoloop1 = bit(0:0) + Lbit(0:0)
      case default
        call crash("H20FF")
    end select
  endif
  if (present(oneloop2)) then
    bit = 0.
    bit(-2) = 0

    bit(-1) = 0

    bit(0) = Hr1(1) * (-2)

    if(size(oneloop2) > 3) then
      bit(1) = Hr1(1) * (-6)
      bit(1) = bit(1) + Hr2(0,1) * (-2)
      bit(1) = bit(1) + Hr2(1,1) * (-4)

      bit(2) = Hr1(1) * (-14)
      bit(2) = bit(2) + Hr1(1)*zeta(2) * (-2)
      bit(2) = bit(2) + Hr2(0,1) * (-6)
      bit(2) = bit(2) + Hr2(1,1) * (-12)
      bit(2) = bit(2) + Hr3(0,0,1) * (-2)
      bit(2) = bit(2) + Hr3(0,1,1) * (-4)
      bit(2) = bit(2) + Hr3(1,0,1) * (-4)
      bit(2) = bit(2) + Hr3(1,1,1) * (-8)
    endif
    bit = bit/x/2.
    Lbit = 0.
    do i=1,7
      do j=1,i-1
        Lbit(i-5) = Lbit(i-5) + (1*Ls)**j / fac(j) * bit(i-5-j)
      enddo
    enddo
    select case(size(oneloop2))
      case(5)
        oneloop2 = bit(-2:2) + Lbit(-2:2)
      case(4)
        oneloop2 = bit(-2:1) + Lbit(-2:1)
      case(3)
        oneloop2 = bit(-2:0) + Lbit(-2:0)
      case(1)
        oneloop2 = bit(0:0) + Lbit(0:0)
      case default
        call crash("H20FF")
    end select
  endif
  if (present(twoloop2)) then
    bit(-4) = 0

    bit(-3) = 0

    bit(-2) = Hr1(1) * (2)

    bit(-1) = Hr1(1) * (11)
    bit(-1) = bit(-1) + Hr2(0,1) * (2)
    bit(-1) = bit(-1) + Hr2(1,1) * (12)

    bit(0) = H2(1)*zeta(2) * (-12*x*omx(-4)*(-14 + 6*x - 2*xpow(2) + xpow(3)))
    bit(0) = bit(0) + H2(2) * (-4*x*omx(-4)*(-14 + 6*x - 2*xpow(2) + xpow(3)))
    bit(0) = bit(0) + Hr1(1) * (-((-43 + 47*x)*omx(-1)))
    bit(0) = bit(0) + Hr1(1)*zeta(2) * (8*omx(-3)*(-1 + 44*x + 56*xpow(2) + 3*xpow(3)))
    bit(0) = bit(0) + Hr1(-1)*zeta(2) * (-8*omx(-3)*(1 + 5*x + 5*xpow(2) + xpow(3)))
    bit(0) = bit(0) + Hr2(0,1) * (-(omx(-3)*(-9 + 55*x - 299*xpow(2) - 47*xpow(3))))
    bit(0) = bit(0) + Hr2(0,1)*zeta(2) * (48*x*omx(-4)*(2 + 11*x + 4*xpow(2)))
    bit(0) = bit(0) + Hr2(0,-1)*zeta(2) * (-96*omx(-4)*xpow(2))
    bit(0) = bit(0) + Hr2(1,1) * (-2*omx(-2)*(-19 + 162*x + 7*xpow(2)))
    bit(0) = bit(0) + Hr3(0,0,1) * (2*omx(-4)*(-1 - 36*x - 8*xpow(2) + 24*xpow(3) + 3*xpow(4)))
    bit(0) = bit(0) + Hr3(0,1,1) * (4*omx(-4)*(5 - 78*x + 4*xpow(2) + 70*xpow(3) + 8*xpow(4)))
    bit(0) = bit(0) + Hr3(-1,0,1) * (-16*omx(-3)*(1 + 5*x + 5*xpow(2) + xpow(3)))
    bit(0) = bit(0) + Hr3(1,0,1) * (-8*omx(-3)*(-1 - 12*x - 32*xpow(2) + 3*xpow(3)))
    bit(0) = bit(0) + Hr3(1,1,1) * (56)
    bit(0) = bit(0) + Hr4(0,0,0,1) * (-16*x*omx(-4)*(2 + 3*x + 4*xpow(2)))
    bit(0) = bit(0) + Hr4(0,0,1,1) * (-32*x*omx(-4)*(2 + 15*x + 4*xpow(2)))
    bit(0) = bit(0) + Hr4(0,1,0,1) * (16*x*omx(-4)*(2 + 15*x + 4*xpow(2)))
    bit(0) = bit(0) + Hr4(0,-1,0,1) * (-192*omx(-4)*xpow(2))
    bit(0) = bit(0) + ln2*zeta(2) * (-12*x*omx(-4)*(-22 + 30*x - 26*xpow(2) + 9*xpow(3)))
    bit(0) = bit(0) + zeta(2) * (12*(57 + 2*x)*omx(-3)*xpow(2))
    bit(0) = bit(0) + zeta(3) * (x*omx(-4)*(-22 + 90*x - 82*xpow(2) + 41*xpow(3)))
    bit(0) = bit(0) + zeta(4) * (-24*x*omx(-4)*(9 + 49*x + 18*xpow(2)))

    bit = bit/x/4.
    Lbit = 0.
    do i=1,7
      do j=1,i-1
        Lbit(i-5) = Lbit(i-5) + (2*Ls)**j / fac(j) * bit(i-5-j)
      enddo
    enddo
    select case(size(twoloop2))
      case(5)
        twoloop2 = bit(-4:0) + Lbit(-4:0)
      case(1)
        twoloop2 = bit(0:0) + Lbit(0:0)
      case default
        call crash("H20FF")
    end select
  endif
  END SUBROUTINE

                          !!!!!!!!!!!!!!!!!!!!!!
                           END MODULE mudec_h20ff
                          !!!!!!!!!!!!!!!!!!!!!!

