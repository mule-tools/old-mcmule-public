                          !!!!!!!!!!!!!!!!!!!!!!
                             MODULE mudec_h2lff
                          !!!!!!!!!!!!!!!!!!!!!!
    use functions
    implicit none
    real(kind=prec), parameter :: zeta(2:5) = (/1.6449340668482264365, &
                                                1.2020569031595942854, &
                                                1.0823232337111381915, &
                                                1.0369277551433699263 /)
    real(kind=8) Hr1(-1:+1)
    real(kind=8) Hr2(-1:+1,-1:+1)
    real(kind=8) Hr3(-1:+1,-1:+1,-1:+1)
    real(kind=8) Hr4(-1:+1,-1:+1,-1:+1,-1:+1)
    real(kind=8) Hex(2)
    real(kind=prec) :: logz(1:4)
    real(kind=prec) :: xpow(-3:5)
    real(kind=prec) :: xm1(2:4)

  contains

  SUBROUTINE INIT_HPLS(x, z)
  real(kind=prec), intent(in) :: x, z
  integer, parameter :: n1 = -1
  integer, parameter :: n2 = +1
  integer, parameter :: nw =  4
  complex(kind=16) Hc1(-1:+1)
  complex(kind=16) Hc2(-1:+1,-1:+1)
  complex(kind=16) Hc3(-1:+1,-1:+1,-1:+1)
  complex(kind=16) Hc4(-1:+1,-1:+1,-1:+1,-1:+1)

  real(kind=8) Hi1(-1:+1)
  real(kind=8) Hi2(-1:+1,-1:+1)
  real(kind=8) Hi3(-1:+1,-1:+1,-1:+1)
  real(kind=8) Hi4(-1:+1,-1:+1,-1:+1,-1:+1)

  call hplog(x, nw, Hc1, Hc2, Hc3, Hc4, &
                    Hr1, Hr2, Hr3, Hr4, &
                    Hi1, Hi2, Hi3, Hi4, n1, n2)

  xpow = x**(/-3,-2,-1,0,1,2,3,4,5/)
  xm1 = 1._prec/(x-1)**(/2,3,4/)
  logz = log(z)**(/1,2,3,4/)
  Hex(1) = log(1-0.5*x)
  Hex(2) = 0.5*log(1-x)**2 * log(2-x) + log(1-x) * Li2(x-1) - Li3(x-1) - 3. * zeta(3) / 4.
  END SUBROUTINE


  FUNCTION FF1A1ES()
  ! Calculate the alpha^1 e^-1 coeff. of F1
  implicit none
  real(kind=prec) :: ff1a1es
  ff1a1es = logz(1)*(-1)
  ff1a1es = ff1a1es -1
  ff1a1es = ff1a1es + Hr1(1)*(-1)
  END FUNCTION FF1A1ES

  FUNCTION FF1A1EF()
  ! Calculate the alpha^1 e^0 coeff. of F1
  implicit none
  real(kind=prec) :: ff1a1ef
  ff1a1ef = logz(1)*(-0.5)
  ff1a1ef = ff1a1ef + logz(2)*(1)
  ff1a1ef = ff1a1ef -2
  ff1a1ef = ff1a1ef + Hr2(1,1)*(-2)
  ff1a1ef = ff1a1ef + Hr1(1)*(-1.5)
  ff1a1ef = ff1a1ef + Hr2(0,1)*(-1)
  END FUNCTION FF1A1EF

  FUNCTION FF1A1EL()
  ! Calculate the alpha^1 e^1 coeff. of F1
  implicit none
  real(kind=prec) :: ff1a1el
  ff1a1el = Hr3(0,0,1)*(-1)
  ff1a1el = ff1a1el + logz(3)*(-(2./3.))
  ff1a1el = ff1a1el + logz(2)*(0.5)
  ff1a1el = ff1a1el + Hr1(1)*((-7 - 2*zeta(2))/2.)
  ff1a1el = ff1a1el + logz(1)*((-3 - 2*zeta(2))/2.)
  ff1a1el = ff1a1el -4 - zeta(2)
  ff1a1el = ff1a1el + Hr3(1,1,1)*(-4)
  ff1a1el = ff1a1el + Hr2(1,1)*(-3)
  ff1a1el = ff1a1el + Hr3(0,1,1)*(-2)
  ff1a1el = ff1a1el + Hr3(1,0,1)*(-2)
  ff1a1el = ff1a1el + Hr2(0,1)*(-1.5)
  END FUNCTION FF1A1EL


  FUNCTION FF1A2ED()
  ! Calculate the alpha^2 e^-2 coeff. of F1
  implicit none
  real(kind=prec) :: ff1a2ed
  ff1a2ed = logz(1)*(1)
  ff1a2ed = ff1a2ed + Hr1(1)*logz(1)*(1)
  ff1a2ed = ff1a2ed + 0.5
  ff1a2ed = ff1a2ed + logz(2)*(0.5)
  ff1a2ed = ff1a2ed + Hr1(1)*(1)
  ff1a2ed = ff1a2ed + Hr2(1,1)*(1)
  END FUNCTION FF1A2ED

  FUNCTION FF1A2ES()
  ! Calculate the alpha^2 e^-1 coeff. of F1
  implicit none
  real(kind=prec) :: ff1a2es
  ff1a2es = Hr2(1,1)*(5)
  ff1a2es = ff1a2es + Hr3(1,1,1)*(6)
  ff1a2es = ff1a2es + 2
  ff1a2es = ff1a2es + Hr1(1)*logz(2)*(-1)
  ff1a2es = ff1a2es + logz(3)*(-1)
  ff1a2es = ff1a2es + logz(2)*(-0.5)
  ff1a2es = ff1a2es + Hr2(0,1)*(1)
  ff1a2es = ff1a2es + Hr3(1,0,1)*(1)
  ff1a2es = ff1a2es + Hr2(0,1)*logz(1)*(1)
  ff1a2es = ff1a2es + Hr3(0,1,1)*(2)
  ff1a2es = ff1a2es + Hr1(1)*logz(1)*(2)
  ff1a2es = ff1a2es + Hr2(1,1)*logz(1)*(2)
  ff1a2es = ff1a2es + logz(1)*(2.5)
  ff1a2es = ff1a2es + Hr1(1)*(3.5)
  END FUNCTION FF1A2ES

  FUNCTION FF1A2EF()
  ! Calculate the alpha^2 e^0 coeff. of F1
  implicit none
  real(kind=prec) :: ff1a2ef
  ff1a2ef = Hr3(0,1,1)*logz(1)*(2)
  ff1a2ef = ff1a2ef + Hr3(1,0,1)*logz(1)*(2)
  ff1a2ef = ff1a2ef + Hr2(1,1)*logz(1)*(4)
  ff1a2ef = ff1a2ef + Hr3(1,1,1)*logz(1)*(4)
  ff1a2ef = ff1a2ef + Hr4(1,1,0,1)*(6)
  ff1a2ef = ff1a2ef + Hr4(1,0,1,1)*(10)
  ff1a2ef = ff1a2ef + Hr4(0,1,1,1)*(12)
  ff1a2ef = ff1a2ef + Hr3(1,1,1)*(22)
  ff1a2ef = ff1a2ef + Hr4(1,1,1,1)*(28)
  ff1a2ef = ff1a2ef + Hr3(-1,0,1)*(2*xm1(3)*(1 + xpow(1))*(9 - 8*xpow(1) + 5*xpow(2)))
  ff1a2ef = ff1a2ef + Hr4(0,0,1,1)*(2*xm1(4)*xpow(1)*(-28 - 9*xpow(1) - 6*xpow(2) + xpow(3&
          &)))
  ff1a2ef = ff1a2ef + Hex(2)*(-(xm1(4)*(-12 + 6*xpow(1) - 5*xpow(2) + 2*xpow(3)))/2.)
  ff1a2ef = ff1a2ef + Hr3(1,0,1)*(xm1(3)*(-12 - 16*xpow(1) - 19*xpow(2) + 5*xpow(3)))
  ff1a2ef = ff1a2ef + Hr4(0,-1,0,1)*(-4*xm1(4)*(1 + 8*xpow(2) - 4*xpow(3) + xpow(4)))
  ff1a2ef = ff1a2ef + Hr4(0,0,0,1)*(-(xm1(4)*(3 + 19*xpow(2) - 6*xpow(3) + 2*xpow(4))))
  ff1a2ef = ff1a2ef + Hr4(0,1,0,1)*(xm1(4)*(3 + 16*xpow(1) + 27*xpow(2) - 6*xpow(3) + 2*xp&
          &ow(4)))
  ff1a2ef = ff1a2ef + Hr3(0,0,1)*(-(xm1(4)*(7 + 14*xpow(1) + 8*xpow(2) - 28*xpow(3) + 8*xp&
          &ow(4)))/2.)
  ff1a2ef = ff1a2ef + Hr3(0,1,1)*((xm1(4)*(-18 - 122*xpow(1) + 185*xpow(2) - 62*xpow(3) + &
          &26*xpow(4)))/2.)
  ff1a2ef = ff1a2ef + Hr1(-1)*(xm1(3)*(1 + xpow(1))*(9 - 8*xpow(1) + 5*xpow(2))*zeta(2))
  ff1a2ef = ff1a2ef + Hex(1)*((-3*xm1(4)*(-12 + 6*xpow(1) - 5*xpow(2) + 2*xpow(3))*zeta(2)&
          &)/2.)
  ff1a2ef = ff1a2ef + Hr2(0,-1)*(-2*xm1(4)*(1 + 8*xpow(2) - 4*xpow(3) + xpow(4))*zeta(2))
  ff1a2ef = ff1a2ef + logz(2)*((-7 + 8*zeta(2))/8.)
  ff1a2ef = ff1a2ef + Hr1(1)*logz(1)*((23 + 8*zeta(2))/4.)
  ff1a2ef = ff1a2ef + Hr2(1,1)*((xm1(2)*(-23 - 66*xpow(1) + 14*xpow(2) + 4*zeta(2) - 8*xpo&
          &w(1)*zeta(2) + 4*xpow(2)*zeta(2)))/2.)
  ff1a2ef = ff1a2ef + Hr2(0,1)*(-(xm1(4)*xpow(-1)*(2 - 11*xpow(1) - 60*xpow(2) + 18*xpow(3&
          &) + 28*xpow(4) + 23*xpow(5) - 12*xpow(1)*zeta(2) - 224*xpow(2)*zeta(2) - 148*xpow(3)*zeta(&
          &2) - 24*xpow(4)*zeta(2)))/4.)
  ff1a2ef = ff1a2ef + logz(1)*((49 + 40*zeta(2) - 48*zeta(3))/8.)
  ff1a2ef = ff1a2ef + Hr1(1)*((xm1(3)*xpow(-1)*(-2 - 71*xpow(1) + 229*xpow(2) - 237*xpow(3&
          &) + 81*xpow(4) - 208*xpow(1)*zeta(2) - 400*xpow(2)*zeta(2) - 248*xpow(3)*zeta(2) + 40*xpow&
          &(4)*zeta(2) + 16*xpow(1)*zeta(3) - 48*xpow(2)*zeta(3) + 48*xpow(3)*zeta(3) - 16*xpow(4)*ze&
          &ta(3)))/8.)
  ff1a2ef = ff1a2ef -(xm1(4)*(-92 + 368*xpow(1) - 552*xpow(2) + 368*xpow(3) - 92*xpow(4) +&
          & 88*zeta(2) - 336*log2*zeta(2) - 676*xpow(1)*zeta(2) + 552*log2*xpow(1)*zeta(2) + 344*xpow&
          &(2)*zeta(2) - 348*log2*xpow(2)*zeta(2) + 280*xpow(3)*zeta(2) - 72*log2*xpow(3)*zeta(2) - 3&
          &6*xpow(4)*zeta(2) + 96*log2*xpow(4)*zeta(2) + 80*zeta(3) - 238*xpow(1)*zeta(3) + 205*xpow(&
          &2)*zeta(3) - 66*xpow(3)*zeta(3) - 8*xpow(4)*zeta(3) + 210*zeta(4) + 376*xpow(1)*zeta(4) + &
          &1598*xpow(2)*zeta(4) - 516*xpow(3)*zeta(4) + 156*xpow(4)*zeta(4)))/8.
  ff1a2ef = ff1a2ef + Hr4(1,0,0,1)*(-3)
  ff1a2ef = ff1a2ef + Hr1(1)*logz(2)*(-2)
  ff1a2ef = ff1a2ef + Hr2(1,1)*logz(2)*(-2)
  ff1a2ef = ff1a2ef + Hr2(0,1)*logz(2)*(-1)
  ff1a2ef = ff1a2ef + logz(3)*(-(1./3.))
  ff1a2ef = ff1a2ef + Hr1(1)*logz(3)*((2./3.))
  ff1a2ef = ff1a2ef + Hr3(0,0,1)*logz(1)*(1)
  ff1a2ef = ff1a2ef + logz(4)*((7./6.))
  ff1a2ef = ff1a2ef + Hr2(0,1)*logz(1)*(2)
  END FUNCTION FF1A2EF


  FUNCTION FF2A1EF()
  ! Calculate the alpha^1 e^0 coeff. of F2
  implicit none
  real(kind=prec) :: ff2a1ef
  ff2a1ef = Hr1(1)*(-xpow(-1))
  END FUNCTION FF2A1EF

  FUNCTION FF2A1EL()
  ! Calculate the alpha^1 e^1 coeff. of F2
  implicit none
  real(kind=prec) :: ff2a1el
  ff2a1el = Hr2(0,1)*(-xpow(-1))
  ff2a1el = ff2a1el + Hr1(1)*(-3*xpow(-1))
  ff2a1el = ff2a1el + Hr2(1,1)*(-2*xpow(-1))
  END FUNCTION FF2A1EL


  FUNCTION FF2A2ES()
  ! Calculate the alpha^2 e^-1 coeff. of F2
  implicit none
  real(kind=prec) :: ff2a2es
  ff2a2es = Hr2(1,1)*(2*xpow(-1))
  ff2a2es = ff2a2es + Hr1(1)*(xpow(-1))
  ff2a2es = ff2a2es + Hr1(1)*logz(1)*(xpow(-1))
  END FUNCTION FF2A2ES

  FUNCTION FF2A2EF()
  ! Calculate the alpha^2 e^0 coeff. of F2
  implicit none
  real(kind=prec) :: ff2a2ef
  ff2a2ef = Hr4(0,1,0,1)*(4*xm1(4)*(2 + 15*xpow(1) + 4*xpow(2)))
  ff2a2ef = ff2a2ef + Hr2(1,1)*(-(xm1(2)*xpow(-1)*(-6 + 74*xpow(1) + 7*xpow(2))))
  ff2a2ef = ff2a2ef + Hex(2)*(-(xm1(4)*(-14 + 6*xpow(1) - 2*xpow(2) + xpow(3))))
  ff2a2ef = ff2a2ef + Hr3(1,0,1)*(xm1(3)*xpow(-1)*(-1 - 27*xpow(1) - 61*xpow(2) + 5*xpow(3&
          &)))
  ff2a2ef = ff2a2ef + Hr3(0,0,1)*(xm1(4)*xpow(-1)*(-1 - 16*xpow(1) - 7*xpow(2) + 14*xpow(3&
          &) + xpow(4)))
  ff2a2ef = ff2a2ef + Hr3(0,1,1)*(xm1(4)*xpow(-1)*(4 - 74*xpow(1) - 2*xpow(2) + 74*xpow(3)&
          & + 7*xpow(4)))
  ff2a2ef = ff2a2ef + Hr2(0,-1)*(-24*xm1(4)*xpow(1)*zeta(2))
  ff2a2ef = ff2a2ef + Hr1(-1)*(2*xm1(3)*xpow(-1)*(1 + xpow(1))*(1 + 4*xpow(1) + xpow(2))*z&
          &eta(2))
  ff2a2ef = ff2a2ef + Hex(1)*(-3*xm1(4)*(-14 + 6*xpow(1) - 2*xpow(2) + xpow(3))*zeta(2))
  ff2a2ef = ff2a2ef + Hr2(0,1)*(-(xm1(4)*xpow(-1)*(-1 + 18*xpow(1) - 156*xpow(2) + 112*xpo&
          &w(3) + 27*xpow(4) - 48*xpow(1)*zeta(2) - 264*xpow(2)*zeta(2) - 96*xpow(3)*zeta(2)))/2.)
  ff2a2ef = ff2a2ef + Hr1(1)*(-(xm1(3)*xpow(-1)*(23 - 73*xpow(1) + 77*xpow(2) - 27*xpow(3)&
          & - 12*zeta(2) + 364*xpow(1)*zeta(2) + 436*xpow(2)*zeta(2) + 28*xpow(3)*zeta(2)))/4.)
  ff2a2ef = ff2a2ef -(xm1(4)*(-264*log2*zeta(2) - 684*xpow(1)*zeta(2) + 360*log2*xpow(1)*z&
          &eta(2) + 660*xpow(2)*zeta(2) - 312*log2*xpow(2)*zeta(2) + 24*xpow(3)*zeta(2) + 108*log2*xp&
          &ow(3)*zeta(2) + 22*zeta(3) - 90*xpow(1)*zeta(3) + 82*xpow(2)*zeta(3) - 41*xpow(3)*zeta(3) &
          &+ 216*zeta(4) + 1176*xpow(1)*zeta(4) + 432*xpow(2)*zeta(4)))/4.
  ff2a2ef = ff2a2ef + Hr1(1)*logz(2)*(-xpow(-1))
  ff2a2ef = ff2a2ef + Hr2(0,1)*logz(1)*(xpow(-1))
  ff2a2ef = ff2a2ef + Hr2(1,1)*logz(1)*(2*xpow(-1))
  ff2a2ef = ff2a2ef + Hr1(1)*logz(1)*((7*xpow(-1))/2.)
  ff2a2ef = ff2a2ef + Hr3(1,1,1)*(12*xpow(-1))
  ff2a2ef = ff2a2ef + Hr4(0,-1,0,1)*(-48*xm1(4)*xpow(1))
  ff2a2ef = ff2a2ef + Hr3(-1,0,1)*(4*xm1(3)*xpow(-1)*(1 + xpow(1))*(1 + 4*xpow(1) + xpow(2&
          &)))
  ff2a2ef = ff2a2ef + Hr4(0,0,0,1)*(-4*xm1(4)*(2 + 3*xpow(1) + 4*xpow(2)))
  ff2a2ef = ff2a2ef + Hr4(0,0,1,1)*(-8*xm1(4)*(2 + 15*xpow(1) + 4*xpow(2)))
  END FUNCTION FF2A2EF


  FUNCTION FF3A1EF()
  ! Calculate the alpha^1 e^0 coeff. of F3
  implicit none
  real(kind=prec) :: ff3a1ef
  ff3a1ef = 2*xpow(-1)
  ff3a1ef = ff3a1ef + Hr1(1)*(xpow(-2)*(-2 + 3*xpow(1)))
  END FUNCTION FF3A1EF

  FUNCTION FF3A1EL()
  ! Calculate the alpha^1 e^1 coeff. of F3
  implicit none
  real(kind=prec) :: ff3a1el
  ff3a1el = 6*xpow(-1)
  ff3a1el = ff3a1el + Hr2(0,1)*(xpow(-2)*(-2 + 3*xpow(1)))
  ff3a1el = ff3a1el + Hr2(1,1)*(2*xpow(-2)*(-2 + 3*xpow(1)))
  ff3a1el = ff3a1el + Hr1(1)*(xpow(-2)*(-4 + 7*xpow(1)))
  END FUNCTION FF3A1EL


  FUNCTION FF3A2ES()
  ! Calculate the alpha^2 e^-1 coeff. of F3
  implicit none
  real(kind=prec) :: ff3a2es
  ff3a2es = Hr2(1,1)*(-2*xpow(-2)*(-2 + 3*xpow(1)))
  ff3a2es = ff3a2es + Hr1(1)*logz(1)*(-(xpow(-2)*(-2 + 3*xpow(1))))
  ff3a2es = ff3a2es + Hr1(1)*(-(xpow(-2)*(-2 + 5*xpow(1))))
  ff3a2es = ff3a2es -2*xpow(-1)
  ff3a2es = ff3a2es + logz(1)*(-2*xpow(-1))
  END FUNCTION FF3A2ES

  FUNCTION FF3A2EF()
  ! Calculate the alpha^2 e^0 coeff. of F3
  implicit none
  real(kind=prec) :: ff3a2ef
  ff3a2ef = Hr3(1,0,1)*(-(xm1(3)*xpow(-2)*(2 - 17*xpow(1) - 59*xpow(2) - 13*xpow&
          &(3) + 3*xpow(4))))
  ff3a2ef = ff3a2ef + Hr2(1,1)*(-(xm1(2)*xpow(-3)*(-4 + 8*xpow(1) - 22*xpow(2) - 76*xpow(3&
          &) + 19*xpow(4))))
  ff3a2ef = ff3a2ef + Hr3(0,0,1)*(xm1(4)*xpow(-2)*(-2 + 19*xpow(1) - 14*xpow(2) + 37*xpow(&
          &3) - 40*xpow(4) + 9*xpow(5)))
  ff3a2ef = ff3a2ef + Hr3(0,1,1)*(-(xm1(4)*xpow(-2)*(-8 + 28*xpow(1) - 210*xpow(2) + 250*x&
          &pow(3) - 72*xpow(4) + 21*xpow(5))))
  ff3a2ef = ff3a2ef + Hr2(0,-1)*(8*xm1(4)*(2 + xpow(1))*zeta(2))
  ff3a2ef = ff3a2ef + Hex(1)*(3*xm1(4)*(-30 + 34*xpow(1) - 16*xpow(2) + 3*xpow(3))*zeta(2)&
          &)
  ff3a2ef = ff3a2ef + Hr1(-1)*(-2*xm1(3)*xpow(-2)*(1 + xpow(1))*(-2 + 11*xpow(1) - 6*xpow(&
          &2) + 3*xpow(3))*zeta(2))
  ff3a2ef = ff3a2ef + Hr2(0,1)*((xm1(4)*xpow(-2)*(10 - 15*xpow(1) - 82*xpow(2) + 28*xpow(3&
          &) + 50*xpow(4) + 9*xpow(5) - 224*xpow(2)*zeta(2) - 184*xpow(3)*zeta(2)))/2.)
  ff3a2ef = ff3a2ef + Hr1(1)*((xm1(3)*xpow(-2)*(-22 + 135*xpow(1) - 277*xpow(2) + 237*xpow&
          &(3) - 73*xpow(4) + 24*zeta(2) - 12*xpow(1)*zeta(2) + 844*xpow(2)*zeta(2) - 76*xpow(3)*zeta&
          &(2) + 36*xpow(4)*zeta(2)))/4.)
  ff3a2ef = ff3a2ef + (xm1(4)*xpow(-1)*(-42 + 168*xpow(1) - 252*xpow(2) + 168*xpow(3) - 42&
          &*xpow(4) + 40*zeta(2) - 520*xpow(1)*zeta(2) - 456*log2*xpow(1)*zeta(2) + 164*xpow(2)*zeta(&
          &2) + 696*log2*xpow(2)*zeta(2) + 364*xpow(3)*zeta(2) - 480*log2*xpow(3)*zeta(2) - 48*xpow(4&
          &)*zeta(2) + 132*log2*xpow(4)*zeta(2) + 30*xpow(1)*zeta(3) - 110*xpow(2)*zeta(3) + 80*xpow(&
          &3)*zeta(3) - 27*xpow(4)*zeta(3) + 1000*xpow(1)*zeta(4) + 824*xpow(2)*zeta(4)))/4.
  ff3a2ef = ff3a2ef + logz(1)*(-7*xpow(-1))
  ff3a2ef = ff3a2ef + logz(2)*(2*xpow(-1))
  ff3a2ef = ff3a2ef + Hr4(0,-1,0,1)*(16*xm1(4)*(2 + xpow(1)))
  ff3a2ef = ff3a2ef + Hr3(1,1,1)*(-12*xpow(-2)*(-2 + 3*xpow(1)))
  ff3a2ef = ff3a2ef + Hr2(1,1)*logz(1)*(-2*xpow(-2)*(-2 + 3*xpow(1)))
  ff3a2ef = ff3a2ef + Hr2(0,1)*logz(1)*(-(xpow(-2)*(-2 + 3*xpow(1))))
  ff3a2ef = ff3a2ef + Hr1(1)*logz(2)*(xpow(-2)*(-2 + 3*xpow(1)))
  ff3a2ef = ff3a2ef + Hr4(0,1,0,1)*(-12*xm1(4)*(4 + 3*xpow(1)))
  ff3a2ef = ff3a2ef + Hr4(0,0,1,1)*(24*xm1(4)*(4 + 3*xpow(1)))
  ff3a2ef = ff3a2ef + Hr4(0,0,0,1)*(4*xm1(4)*(4 + 5*xpow(1)))
  ff3a2ef = ff3a2ef + Hr1(1)*logz(1)*(-(xpow(-2)*(-10 + 17*xpow(1)))/2.)
  ff3a2ef = ff3a2ef + Hex(2)*(xm1(4)*(-30 + 34*xpow(1) - 16*xpow(2) + 3*xpow(3)))
  ff3a2ef = ff3a2ef + Hr3(-1,0,1)*(-4*xm1(3)*xpow(-2)*(1 + xpow(1))*(-2 + 11*xpow(1) - 6*x&
          &pow(2) + 3*xpow(3)))
  END FUNCTION FF3A2EF
                          !!!!!!!!!!!!!!!!!!!!!!
                           END MODULE mudec_h2lff
                          !!!!!!!!!!!!!!!!!!!!!!

