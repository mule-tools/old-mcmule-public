                 !!!!!!!!!!!!!!!!!!!!!!!!!
                  MODULE MUDECRARE_TEST
                 !!!!!!!!!!!!!!!!!!!!!!!!!
  use testtools

contains

  SUBROUTINE TESTMUDECRARE(ONLYFAST)
  logical onlyfast

  call testmudecrarematel
  call testmudecraren1
  if(.not.onlyfast) call testmudecrarespeed
  call testmudecsoftrare
  if(.not.onlyfast) call testmudecvegasrare
  call testmudecpointwiserare
  END SUBROUTINE TESTMUDECRARE


  SUBROUTINE TESTMUDECRAREMATEL
  implicit none
  real(kind=prec) :: arr(11), weight, finite, single
  real(kind=prec) s12,s13,s14,s23,s24,s34,m12,m22,m32, deltaren
   real(prec),parameter :: massMu  = 1000.*       0.105658372000000_prec
   real(prec),parameter :: massEl  = 1000.*       0.000510998930000_prec
   real(prec),parameter :: massTau = 1000.*       1.776820000000000_prec
  real(kind=prec), parameter :: MtauOld = 1776.82_prec
  real(kind=prec), parameter :: MelOld = 0.510998928_prec
  real(kind=prec), parameter :: MmuOld = 105.6583715_prec
  integer :: ndim
  procedure(integrand), pointer :: fxn

  Nel = 1
  Nmu = 1
  Ntau = 1
  Nhad = 0
  call initflavour("mu-eOld")
  call blockstart("mudec rare")

  arr = (/  0.24198828748173506 , 0.29688027594821942 , 0.16379459616382441 , 0.64612797092642082 , &
            0.42594211662333048 , 0.66550862851375925 , 0.99940334884282045 , 0.99344043621461031 , &
            0.04421843807993386 , 0.14852028093172465 , 0.59387643773429266  /)
  call psd6_23_24_34(arr,p1,massTau,p2,massMu,p5,0._prec,p6,0._prec,p3,massEl,p4,massEl,weight)
  s12=s(p1,p2) ; s13=s(p1,p3) ; s14 = s(p1,p4)
  s23=s(p2,p3) ; s24=s(p2,p4)
  s34=s(p3,p4)
  m12 = sq(p1) ; m22 = sq(p2) ; m32 = sq(p3)

  musq = masstau**2

  pol1 = 0.
  finite = pt2mnneeav(p1,pol1,p2,p5,p6,p3,p4)
  deltaren = -2 * alpha * ( log(MelOld**2/MtauOld**2) + log(MmuOld**2/MtauOld**2) ) / 3 / pi * finite
  call check("GoSam polarised tau LO",finite,31804.075255941159, threshold=8e-8)
  finite = pt2mnneelav(p1,pol1,p2,p5,p6,p3,p4,single)
  ! fudge factor due to change in mass
  finite = finite + 0.9999999691696316* pt2mnneeav_a(p1,pol1,p2,p5,p6,p3,p4)
  call check("GoSam tau finite",finite,1386504.3246603629 + deltaren, threshold=1e-10)
  call check("GoSam tau single",single,138111.92455647807, threshold=1e-9)

  pol1 = (/ 0., 0., 0.85, 0. /)

  finite = pt2mnneeav(p1,pol1,p2,p5,p6,p3,p4)
  deltaren = -2 * alpha * ( log(MelOld**2/MtauOld**2) + log(MmuOld**2/MtauOld**2) ) / 3 / pi * finite
  call check("GoSam polarised tau LO",finite,36539.897503867433, threshold=4e-8)
  finite = pt2mnneelav(p1,pol1,p2,p5,p6,p3,p4,single)
  finite = finite +  pt2mnneeav_a(p1,pol1,p2,p5,p6,p3,p4)
  call check("GoSam polarised tau finite",finite,1592169.4648204616 + deltaren, threshold=4e-8)
  call check("GoSam polarised tau single",single,158677.62695853604, threshold=4e-8)
  pol1 = 0.

  call psd6_23_24_34(arr,p1,Mm,p2,Me,p5,0._prec,p6,0._prec,p3,Me,p4,Me,weight)
  musq = mm**2
  call SetDeltaUV_cll(0.) ; call SetDeltaIR_cll(0.,0.)
  call SetMuUV2_cll(musq) ; call SetMuIR2_cll(musq)

  Ntau = 0
  finite = pm2enneeav(p1,pol1,p2,p5,p6,p3,p4)
  deltaren = -2 * alpha * log(MelOld**2/MmuOld**2) / 3 / pi * finite
  call check("Math  mu LO    ",finite,4 * 34127.704521729924, threshold=1e-9)
  finite = pm2enneelav(p1,pol1,p2,p5,p6,p3,p4)
  ! fudge factor due to changed mass
  finite = finite + 1.0000000139985779 * pm2enneeav_a(p1,pol1,p2,p5,p6,p3,p4)
  finite = finite + pm2enneeav_ai(p1,pol1,p2,p5,p6,p3,p4)
  call check("GoSam mu finite",finite,2 * 1786955.3118638927 + deltaren, threshold=1e-9)
  call check("Math  mu finite",finite,2 * 1786955.3112681818 + deltaren)


  call psd6_26_2x5(arr, p1, Mm, p2, Me, p5, 0._prec, p6, 0._prec, p3, Me, p4, Me, weight)
  finite = pm2enneeav(p1,pol1,p2,p5,p6,p3,p4)
  deltaren = -2 * alpha * log(Me**2/Mm**2) / 3 / pi * finite

  which_piece = 'm2enneeV'
  call initpiece(ndim, fxn)
  finite = fxn(arr, 1._prec, ndim)
  which_piece = 'm2enneeA'
  call initpiece(ndim, fxn)
  finite = finite + fxn(arr, 1._prec, ndim)
  which_piece = 'm2enneeAi'
  call initpiece(ndim, fxn)
  finite = finite + fxn(arr, 1._prec, ndim)
  which_piece = 'm2ennee0b'
  call initpiece(ndim, fxn)
  finite = finite - fxn(arr, 1._prec, ndim) * (-2 * alpha * log(Me**2/Mm**2) / 3 / pi)

  !print*,finite
  finite = pm2enneelav(p1,pol1,p2,p5,p6,p3,p4) + pm2enneeav_a(p1,pol1,p2,p5,p6,p3,p4) + pm2enneeav_ai(p1,pol1,p2,p5,p6,p3,p4)&
    -deltaren
  !print*,finite * weight * convfac


  call psd6_26_2x5(arr, p1, MmuOld, p2, MelOld, p5, 0._prec, p6, 0._prec, p3, MelOld, p4, MelOld, weight)
  finite = pm2enneeav(p1,pol1,p2,p5,p6,p3,p4)
  deltaren = -2 * alpha * log(Me**2/Mm**2) / 3 / pi * finite
  finite = pm2enneelav(p1,pol1,p2,p5,p6,p3,p4) + pm2enneeav_a(p1,pol1,p2,p5,p6,p3,p4) + pm2enneeav_ai(p1,pol1,p2,p5,p6,p3,p4)&
  -deltaren


  !print*,finite * weight * convfac
  !pol1 = (/ 0., 0., 0.85, 0. /)
  !finite = pm2enneeav(p1,pol1,p2,p5,p6,p3,p4)
  !deltaren = -2 * alpha * log(MelOld**2/MmuOld**2) / 3 / pi * finite
  !call check("Math  polarised mu LO    ",finite,4 * 32036.4789853319, threshold=2e-9)
  !finite = pm2enneelav(p1,pol1,p2,p5,p6,p3,p4) + pm2enneeav_a(p1,pol1,p2,p5,p6,p3,p4) + pm2enneeav_ai(p1,pol1,p2,p5,p6,p3,p4)
  !call check("GoSam polarised mu finite",finite,2 * 1673902.3152368125 + deltaren, threshold=1e-9)
  !call check("Math  polarised mu finite",finite,2 * 1673902.3146805929 + deltaren)
  !pol1 = 0.
  call blockend(9)
  Ntau = 1
  Nhad = 1


  END SUBROUTINE

  SUBROUTINE TESTMUDECRAREN1
  implicit none
  real(kind=prec) :: arr(14), weight
  integer ranseed, i
  real(prec),parameter :: massMu  = 1000.*       0.105658372000000_prec
  real(prec),parameter :: massEl  = 1000.*       0.000510998930000_prec
  real(prec),parameter :: massTau = 1000.*       1.776820000000000_prec
  real(kind=prec), parameter :: MtauOld = 1776.82_prec
  real(kind=prec), parameter :: MelOld = 0.510998928_prec
  real(kind=prec), parameter :: MmuOld = 105.6583715_prec
  real(kind=prec) :: ans

  arr = (/ 0.49236414417476559, 0.74261882343084062, 0.06857487863144673, &
           0.43968827667712695, 0.16877849338194051, 0.35107011611948979, &
           0.71298814734328664, 0.02663140860819268, 0.48311756563250341, &
           0.90734947217492223, 0.42003407667164250, 0.40900599802920279, &
           0.26408923713766908, 0.47278811840281654 /)

  call initflavour("mu-e")
  call blockstart("mudec rare n+1")
  call psd7_27_37_47_fks(arr,p1,massTau,p2,massmu,p3,massel,p4,massel,p6,0._prec,p5,0._prec,p7,weight)

  pol1 = 0.
  ans = pt2mnneegav(p1, pol1, p2, p5,p6, p3, p4, p7)
  call check("two flavour unpolarised", ans, 7.884885920418635, threshold=1e-7)


  pol1 = (/ 0., 0.,  0.85, 0. /)
  ans = pt2mnneegav(p1, pol1, p2, p5,p6, p3, p4, p7)
  call check("two flavour polarised", ans, 10.394629796346027, threshold=1e-7)
  pol1 = 0.

  ranseed = 185548976
  weight = 0.
  do while (weight < zero)
    do i=1,14
      arr(i) = ran2(ranseed)
    enddo
    call psd7_27_37_47_e56_fks(arr,p1,MtauOld,p2,Mel,p3,MelOld,p4,MelOld,p6,0._prec,p5,0._prec,p7,weight)
  enddo

  ans = pm2enneegav(p1, pol1, p2, p5,p6, p3, p4, p7)
  call check("one flavour unpolarised", ans, 2*29847.869327681507, threshold=1e-7)

  pol1 = (/ 0., 0.,  0.85, 0. /)
  ans = pm2enneegav(p1, pol1, p2, p5,p6, p3, p4, p7)
  call check("one flavour polarised", ans, 2*33258.882371870175, threshold=1e-7)
  pol1 = 0.
  call blockend(4)

  END SUBROUTINE



  SUBROUTINE TESTMUDECRARESPEED
  implicit none
  integer, parameter :: niter = 2000
  real (kind=prec) :: weight, arr(11), finite
  real (kind=prec) :: startt, endt, rate
  integer i, j, ranseed

  call initflavour("mu-e")
  musq = mm**2
  call blockstart("mudec rare performance")
  ! on my notebook I saw the following rates
  !   no optimisation:     ~32 kEv/s
  !   -O3 optimisation:   ~122 kEv/s
  !   MMA               : ~121 kEv/s
  !   MMA + -O3         : ~123 kEv/s
  ! -> use only O3 optimisation and don't bother with MMA optimisation

  ranseed = 2332
  call cpu_time(startt)
  do j=1,niter
    do i=1,11
      arr(i) = ran2(ranseed)
    enddo
    call psd6_23_24_34(arr,p1,Mtau,p2,Mm,p5,0._prec,p6,0._prec,p3,Me,p4,Me,weight)
    if (weight.lt.zero) cycle
    finite = pt2mnneelav(p1,pol1,p2,p5,p6,p3,p4)
  enddo
  call cpu_time(endt)
  rate = niter/(endt-startt)
  print*,"tau->mu e e nu nu at ",rate,"ev/s"


  ranseed = 2332
  call cpu_time(startt)
  do j=1,niter
    do i=1,11
      arr(i) = ran2(ranseed)
    enddo
    call psd6_23_24_34(arr,p1,Mtau,p2,Mm,p5,0._prec,p6,0._prec,p3,Me,p4,Me,weight)
    if (weight.lt.zero) cycle
    finite = pt2mnneelav(p1,pol1,p2,p5,p6,p3,p4)
  enddo
  call cpu_time(endt)
  rate = niter/(endt-startt)
  print*," mu-> e e e nu nu at ",rate,"ev/s"



  END SUBROUTINE




  SUBROUTINE TESTMUDECSOFTRARE
  use vegas_m
  implicit none
  integer i,ranseed,j
  real(kind=prec) :: arr(14)

  call initflavour("tau-mue")

  ranseed = 1093503073
  do i=1,14
    arr(i) = ran2(ranseed)
  enddo

  xinormcut1 = 0.3
  xinormcut2 = 0.3

  call blockstart("mudec rare \xi->0")
  call test_softlimit(arr, [ "m2enneeR", "t2mnneeR" ], (/ 8, 8/) )
  END SUBROUTINE


  SUBROUTINE TESTMUDECVEGASRARE
  implicit none
  Nel = 1
  Nmu = 1
  Ntau = 1
  Nhad = 1
  xinormcut = 0.8
  call blockstart("mudec rare VEGAS test")

  call initflavour("mu-e")
  musq = mm**2
  call test_INT("m2ennee0"           ,100,  5,  1.4919043150986880E+06)
  call test_INT("m2enneeA"           ,  1,  5,  3.5280208249102853E+05)
  call test_INT("m2enneeV"           ,  1,  5,  1.9275081352732234E+07)
  call test_INT("m2enneeC"           ,100,  5, -1.3679366314706657E+07)
  call test_INT("m2enneeR"           , 10,  5, -1.4553651404246762E+06)

  call initflavour("tau-mue")
  call test_INT("t2mnnee0"           ,100,  5,  6.2087029762042441E+12)
  call test_INT("t2mnneeA"           ,  1,  5,  2.4857212544240747E+12)
  call test_INT("t2mnneeV"           ,  1,  5,  4.0248783613916375E+13)
  call test_INT("t2mnneeC"           ,  1,  5, -5.6825485904423936E+12)
  call test_INT("t2mnneeR"           , 10,  5, -6.2924588743112314E+12)

  call blockend(10)
  END SUBROUTINE TESTMUDECVEGASRARE

  SUBROUTINE TESTMUDECPOINTWISERARE
  implicit none
  Nel = 1
  Nmu = 1
  Ntau = 1
  Nhad = 1
  call blockstart("mudec rare point-wise test")
  xinormcut = 1
  xinormcut1 = 1
  xinormcut2 = 1
  call initflavour("mu-e")
  musq = mm**2
  call test_INT("m2ennee0"           , ans= 7.7109117285267534E+06)
  call test_INT("m2enneeA"           , ans= 1.0253777690204389E+07)
  call test_INT("m2enneeV"           , ans= 2.9755182444776058E+08)
  call test_INT("m2enneeC"           , ans=-4.9853934482465327E+07)
  call test_INT("m2enneeR"           , ans=-8.9313909590705726E+06)

  call initflavour("tau-mue")
  call test_INT("t2mnnee0"           , ans= 1.4539979599001707E+12)
  call test_INT("t2mnneeA"           , ans= 2.6599108804466499E+12)
  call test_INT("t2mnneeC"           , ans=-7.8008904732265176E+12)
  call test_INT("t2mnneeV"           , ans= 3.0546631710154895E+13)
  call test_INT("t2mnneeR"           , ans=-5.0104565532852207E+12)

  call blockend(10)
  END SUBROUTINE



                 !!!!!!!!!!!!!!!!!!!!!!!!!
                 END MODULE MUDECRARE_TEST
                 !!!!!!!!!!!!!!!!!!!!!!!!
