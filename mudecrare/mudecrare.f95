
                 !!!!!!!!!!!!!!!!!!!!!!!!!
                    MODULE MUDECRARE
                 !!!!!!!!!!!!!!!!!!!!!!!!!

  use functions
  use phase_space, only: ksoft, ksoftA, ksoftB
  use mudecrare_mat_el

!!!  use amplitude_lo, only: amplitude_n_LO

  implicit none

  contains


  FUNCTION M2ENNEE_part(p1,p2,pA,pB,p3,p4)
    !! mu+(p1) -> e+(p2) e-(p3) e+(p4) 2nu
    !! for massive electron
    !! average over neutrino tensor taken
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4), pA(4), pB(4)
  type(particles) :: m2ennee_part
  m2ennee_part = parts((/ part(p1, -1, 1),  part(p2, -1, -1),  part(p3, 1, -1),  part(p4, -1, -1) /))
  END FUNCTION M2ENNEE_part

  FUNCTION T2MNNEE_part(p1,p2,pA,pB,p3,p4)
    !!  tau+(p1)->mu+(p2) e-(p3) e+(p4) 2nu
    !! for massive electron
    !! average over neutrino tensor taken
  real (kind=prec) :: p1(4),p2(4),p3(4),p4(4), pA(4), pB(4)
  type(particles) :: t2mnnee_part
  t2mnnee_part = parts((/ part(p1, -1, 1),  part(p2, -1, -1),  part(p3, 1, -1),  part(p4, -1, -1) /))
  END FUNCTION T2MNNEE_part


          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          !!!!                                    !!!!
          !!!!        SINGULAR    LIMITS          !!!!
          !!!!                                    !!!!
          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!



  !  M1^+(p1)->M2^+(p2),M2^-(p3),M2^+(p4),2\nu,\gamma(p7)
  FUNCTION PM2ENNEEGav_S(q1,n1,q2,qA,qB,q3,q4)

  real (kind=prec) :: q1(4),q2(4),q3(4),q4(4),n1(4)
  real (kind=prec) :: qA(4), qB(4)
  real (kind=prec) :: pm2enneegav_s

!  nn1 = (/ 0._prec,0._prec,0._prec,0._prec /)

  pm2enneegav_s = eik(ksoft, parts((/ part(q1, -1, 1),  part(q2, -1, -1),      &
                                      part(q3, 1, -1),  part(q4, -1, -1) /)))  &
             *pm2enneeav(q1,n1,q2,qA,qB,q3,q4)
  pm2enneegav_s = 2*(4.*pi*alpha)*pm2enneegav_s

  END FUNCTION PM2ENNEEGav_S

  !  M1^+(p1)->M2^+(p2),M3^-(p3),M3^+(p4),2\nu,\gamma(p7)
  FUNCTION PT2MNNEEGav_S(q1,n1,q2,qA,qB,q3,q4)

  real (kind=prec) :: q1(4),q2(4),q3(4),q4(4),n1(4)
  real (kind=prec) :: qA(4), qB(4)
  real (kind=prec) :: pt2mnneegav_s

!  nn1 = (/ 0._prec,0._prec,0._prec,0._prec /)

  pt2mnneegav_s = 2*eik(ksoft, parts((/ part(q1, -1, 1), part(q2, -1, -1),      &
                                        part(q3, 1, -1), part(q4, -1, -1) /)))  &
             *pt2mnneeav(q1,n1,q2,qA,qB,q3,q4)
  pt2mnneegav_s = (4.*pi*alpha)*pt2mnneegav_s

  END FUNCTION PT2MNNEEGav_S





                 !!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    END MODULE MUDECRARE
                 !!!!!!!!!!!!!!!!!!!!!!!!!!!!
