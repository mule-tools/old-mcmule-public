  PROGRAM MCMULE_EXE
  use mcmule

  implicit none

  integer :: ncall_ad, itmx_ad, ncall, itmx, nenter_ad, nenter
  integer :: initial_ran_seed
  character(len=25) :: piece
  character(len=15) :: flav
  real(kind=prec) :: xi1, xi2

#ifndef SHARED
#include "include-user.f95"
#else
  character(len=900) :: arg
  call get_command_argument(1, arg)
  if (len_trim(arg) == 0) then
    arg = "./user.so"
  endif
  call load_quant(trim(arg))
#endif

  read(5,*) nenter_ad
  read(5,*) itmx_ad
  read(5,*) nenter
  read(5,*) itmx
  read(5,*) initial_ran_seed
  read(5,*) xi1
  read(5,*) xi2
  read(5,*) piece
  read(5,*) flav

  ncall_ad = 1000*nenter_ad
  ncall = 1000*nenter

  call runmcmule(ncall_ad, itmx_ad, ncall, itmx, initial_ran_seed, xi1, xi2, piece, flav)

  END PROGRAM MCMULE_EXE
