                 !!!!!!!!!!!!!!!!!!!!!
                     MODULE MCMULE
                 !!!!!!!!!!!!!!!!!!!!!
  use global_def, only: prec, pi, zero, &
    GF, alpha, sW2, &
    Mmu, Mel, Mtau, Mproton, MZ, &
    Mm, Me, Mt, scms, CLj, CRj, Mj, &
    pol1, pol2, musq, &
    xicut1, xicut2, xieik1, xieik2, &
    softcut, collcut, sSwitch, ntsSwitch, pcSwitch, &
    nel, nmu, ntau, nhad, &
    which_piece, flavour, initflavour

  use functions, only: s, sq, asymtensor, &
    boost_back, boost_rfrec, boost_rf, sq_lambda, euler_mat, &
    eta, rap, pt, absvec, rij, cos_th, phi

  use phase_space, only: &
    xiout, yout, qsoft, &
    xioutA, youtA, qsoftA, &
    xioutB, youtB, qsoftB

#ifdef SHARED
  use user_dummy, only: pass_cut, userweight, names, filenamesuffix, set_observable, nr_q, load_quant
#else
  use user_dummy, only: pass_cut, userweight, names, filenamesuffix, set_observable, nr_q
#endif

  implicit none

contains

  SUBROUTINE RUNMCMULE(ncall_ad, itmx_ad, ncall, itmx, initial_ran_seed, xi1, xi2, piece, flav, tfilename)
#ifndef HEADER_ONLY
  use collier
  use integrands
#endif
  use vegas_m
  use user_dummy

  implicit none
  integer, intent(in) :: ncall_ad, itmx_ad, ncall, itmx
  integer, intent(in) :: initial_ran_seed
  real(kind=prec), intent(in) :: xi1, xi2
  character(len=*), intent(in) :: piece, flav
  character(len=*), intent(in), optional :: tfilename

  integer :: ndim
  procedure(integrand), pointer :: fxn
  integer :: ad_print, fu_print, nsp
  character (len=1) ::  ad_char, fu_char
  real (kind=prec) :: avgi, sd, chi2a
  character(len=900) :: filename

  ran_seed = initial_ran_seed

  which_piece = piece
  flavour = flav

  xinormcut = xi1
  delcut = xi2
  xinormcut1 = xi1
  xinormcut2 = xi2

  call initflavour
  call initpiece(ndim, fxn)
  call constants
  call rdata

      !!!!!!!!!!      OUTPUT     !!!!!!!!!

  ad_print = ncall_ad
  ad_char = ' '
  if(ncall_ad > 999999999) then
    ad_print = ncall_ad/1000000000
    ad_char = 'G'
  elseif(ncall_ad > 999999) then
    ad_print = ncall_ad/1000000
    ad_char = 'M'
  elseif(ncall_ad > 999) then
    ad_print = ncall_ad/1000
    ad_char = 'K'
  endif

  fu_print = ncall
  fu_char = ' '
  if(ncall > 999999999) then
    fu_print = ncall/1000000000
    fu_char = 'G'
  elseif(ncall > 999999) then
    fu_print = ncall/1000000
    fu_char = 'M'
  elseif(ncall > 999) then
    fu_print = ncall/1000
    fu_char = 'K'
  endif

#ifndef HEADER_ONLY
  call init_cll(5,folder_name="")
#endif

  write(6,*) '  - * - * - * - * - * - * - * -     '
  write(6,*) '       Version information          '
  write(6,*) " Full SHA: ", fullsha(1:7)
  write(6,*) " Git  SHA: ", gitrev (1:7)
  do nsp = 1,len(gitbranch)
    if (iachar(gitbranch(nsp:nsp)) == 0) exit
  enddo
  write(6,*) ' Git branch: ', gitbranch(1:nsp-1)
  write(6,*) '  - * - * - * - * - * - * - * -     '

  call inituser
  call initvegas
  if(present(tfilename)) then
    filename = tfilename
  else
    if (iachar(filenamesuffix(1:1)) == 0) then
      write (filename, 900) trim(which_piece),trim(flavour),ran_seed,xinormcut,delcut,itmx,fu_print,fu_char
    else
      write (filename, 901) trim(which_piece),trim(flavour),ran_seed,xinormcut,delcut,itmx,fu_print,fu_char,trim(filenamesuffix)
    endif
  endif

  write(6,*) '  - * - * - * - * - * - * - * -     '
  write(6,*) '        '

  if(itmx_ad .gt. 0) then
    call vegas (ndim,ncall_ad,itmx_ad,fxn,avgi,sd,chi2a,ran_seed)
    call vegas1(ndim,ncall   ,itmx   ,fxn,avgi,sd,chi2a,ran_seed,filename)
  else
    call vegas (ndim,ncall,itmx,fxn,avgi,sd,chi2a,ran_seed, filename)
  endif


  write(6,"(a,i3,a,i3,a,a,i3,a,i3,a,a,i10)")             &
      ' points:   ',itmx_ad,'*',ad_print,ad_char,' + ',   &
      itmx,'*',fu_print,fu_char,                         &
      '         random seed: ', initial_ran_seed
  if ( (which_piece == "ee2nnRV").or.(which_piece == "ee2nnRR") ) then
  write(6,"(a,a,a,f7.5,a,f7.5)")     &
      ' part: ', which_piece,    &
      '    xicut1: ', xinormcut1,'    xicut2: ', xinormcut2
  else
  write(6,"(a,a,a,f7.5,a,f7.5)")     &
      ' part: ', which_piece,    &
      '    xicut: ', xinormcut,'    delcut: ', delcut
  endif
  if (flavour == "e-s") then
  write(6,"(a,es11.5,a,es11.5)")     &
      ' S:  ', Scms,'   Mout:  ', Me
  else
  write(6,"(a,i10,a,i10)")     &
      ' points on phase space ',sum(pcounter), ' thereof fucked up ',pcounter(2)
  endif
  write(6,*) '        '
  write(6,*) '        '
  write(6,*) '        '
!  write(6,"(a,es13.5,a,es13.5,a,f4.2)")     &
!      ' result, error:  {' ,conv*avgi ,',', conv*sd,' };   chisq:  ', chi2a
  write(6,"(a,es13.5,a,es13.5,a,f4.2)")     &
      ' result, error:  {' ,avgi ,',', sd,' };   chisq:  ', chi2a
  write(6,*) '        '
  write(6,*) '  - * - * - * - * - * - * - * -     '


900  format("out/",A,'_',A,'_S',I0.10,"X",f7.5,"D",f7.5,'_ITMX',I0.3,'x',I0.3,A,".vegas")
901  format("out/",A,'_',A,'_S',I0.10,"X",f7.5,"D",f7.5,'_ITMX',I0.3,'x',I0.3,A,"_O",A,".vegas")
  END SUBROUTINE RUNMCMULE

                 !!!!!!!!!!!!!!!!!!!!!
                   END MODULE MCMULE
                 !!!!!!!!!!!!!!!!!!!!!
