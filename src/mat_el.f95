                 !!!!!!!!!!!!!!!!!!!!!!!!!
                       MODULE MAT_EL
                 !!!!!!!!!!!!!!!!!!!!!!!!!

  use functions
#ifdef HAVE_MISC

  !! From file misc_ee2nngg.f95
  use misc, only: ee2nnggav!!(p1, p2,p3,p4, q2,q3)
    !! e+(p1) e-(p2) -> y*(p1+p2) -> nu(p3) nu(p4) g(q2) g(q3)
    !! e-(p1) e+(p2) -> y*(p1+p2) -> nu(p3) nu(p4) g(q2) g(q3)
    !! for massive electrons
    !! average over neutrino tensor taken


  !! From file misc_mat_el.f95
  use misc, only: ee2nneik!!(p1, p2, xicut, pole, lin)
    !! e+(p1) e-(p2) -> whatever
    !! e-(p1) e+(p2) -> whatever
    !! for massive electrons

  use misc, only: ee2nn!!(p1, p2, q1, q2, linear)
    !! e+(p1) e-(p2) -> y*(p1+p2) -> nu(q1) nu(q2)
    !! e-(p1) e+(p2) -> y*(p1+p2) -> nu(q1) nu(q2)
    !! for massive electrons

  use misc, only: ee2nnl!!(p1, p2, q1, q2, pole, lin)
    !! e+(p1) e-(p2) -> y*(p1+p2) -> nu(q1) nu(q2)
    !! e-(p1) e+(p2) -> y*(p1+p2) -> nu(q1) nu(q2)
    !! for massive electrons
    !! Package-X convention
    !! This is most likely CDR

  use misc, only: ee2nnlav!!(p1, p2, p3, p4, pole, lin)
    !! e+(p1) e-(p2) -> y*(p1+p2) -> nu(p3) nu(p4)
    !! e-(p1) e+(p2) -> y*(p1+p2) -> nu(p3) nu(p4)
    !! for massive electrons
    !! Package-X convention, m>0
    !! average over neutrino tensor taken
    !! This is most likely CDR

  use misc, only: ee2nng!!(p1, p2, q1, q2, q3)
    !! e+(p1) e-(p2) -> y*(p1+p2) -> nu(q1) nu(q2) g(q3)
    !! e-(p1) e+(p2) -> y*(p1+p2) -> nu(q1) nu(q2) g(q3)
    !! for massive electrons

  use misc, only: ee2nngav!!(p1, p2, q1, q2, q3,lin)
    !! e+(p1) e-(p2) -> y*(p1+p2) -> nu(q1) nu(q2) g(q3)
    !! e-(p1) e+(p2) -> y*(p1+p2) -> nu(q1) nu(q2) g(q3)
    !! for massive electrons
    !! average over neutrino tensor taken

  use misc, only: ee2nnav!!(p1, p2, q1, q2, lin)
    !! e+(p1) e-(p2) -> y*(p1+p2) -> nu(q1) nu(q2)
    !! e-(p1) e+(p2) -> y*(p1+p2) -> nu(q1) nu(q2)
    !! for massive electrons
    !! average over neutrino tensor taken

  use misc, only: ee2nnglav!!(p1, p2, q1, q3, q2,pole)
    !! e+(p1) e-(p2) -> y*(p1+p2) -> nu(q1) nu(q3) g(q2)
    !! e-(p1) e+(p2) -> y*(p1+p2) -> nu(q1) nu(q3) g(q2)
    !! for massive electrons
    !! average over neutrino tensor taken
    !! This is most likely CDR

  use misc, only: ee2nngf!!(p1, p2, p3, p4, q2)
    !! e+(p1) e-(p2) -> y*(p1+p2) -> nu(p3) nu(p4) g(q2)
    !! e-(p1) e+(p2) -> y*(p1+p2) -> nu(p3) nu(p4) g(q2)
    !! for massive electrons
    !! average over neutrino tensor taken

  use misc, only: ee2nnf!!(p1, p2, q1, q2)
    !! e+(p1) e-(p2) -> y*(p1+p2) -> nu(q1) nu(q2)
    !! e-(p1) e+(p2) -> y*(p1+p2) -> nu(q1) nu(q2)
    !! for massive electrons

  use misc, only: ee2nns!!(p1, p2, q1, q2)
    !! e+(p1) e-(p2) -> y*(p1+p2) -> nu(q1) nu(q2)
    !! e-(p1) e+(p2) -> y*(p1+p2) -> nu(q1) nu(q2)
    !! for massive electrons
    !! This is CDR

  use misc, only: ee2nnss!!(p1, p2, q1, q2)
    !! e+(p1) e-(p2) -> y*(p1+p2) -> nu(q1) nu(q2)
    !! e-(p1) e+(p2) -> y*(p1+p2) -> nu(q1) nu(q2)
    !! for massive electrons
    !! This is CDR

  use misc, only: ee2nncc!!(p1, p2, q1, q2)
    !! e+(p1) e-(p2) -> y*(p1+p2) -> nu(q1) nu(q2)
    !! e-(p1) e+(p2) -> y*(p1+p2) -> nu(q1) nu(q2)
    !! for massive electrons
    !! This is CDR


  !! From file misc.f95
  use misc, only: ee2nn_part!!(p1, p2, p3, p4)
    !! e+(p1) e-(p2) -> y*(p1+p2) -> nu(q1) nu(q2)
    !! e-(p1) e+(p2) -> y*(p1+p2) -> nu(q1) nu(q2)
    !! for massive electrons

  use misc, only: ee2nng_s!!(p1,p2,q1,q2,lin)
    !! e+(p1) e-(p2) -> y*(p1+p2) -> nu(q1) nu(q2) g(ksoft)
    !! e-(p1) e+(p2) -> y*(p1+p2) -> nu(q1) nu(q2) g(ksoft)
    !! for massive electrons
    !! average over neutrino tensor taken

  use misc, only: ee2nngl_s!!(p1,p2,q1,q2)
    !! e+(p1) e-(p2) -> y*(p1+p2) -> nu(q1) nu(q2) g(ksoft)
    !! e-(p1) e+(p2) -> y*(p1+p2) -> nu(q1) nu(q2) g(ksoft)
    !! for massive electrons
    !! average over neutrino tensor taken

  use misc, only: ee2nngf_s!!(p1,p2,q1,q2)
    !! e+(p1) e-(p2) -> y*(p1+p2) -> nu(q1) nu(q2) g(ksoft)
    !! e-(p1) e+(p2) -> y*(p1+p2) -> nu(q1) nu(q2) g(ksoft)
    !! for massive electrons
    !! average over neutrino tensor taken


#endif
#ifdef HAVE_MUE

  !! From file mue_em2emggeeee.f95
  use mue, only: em2emgg_eeee_old!!(p1,p2,p3,p4,p5,p6)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4) g(p5) g(p6)
    !! for massive (and massless) electrons


  !! From file mue_em2emeeee.f95
  use mue, only: em2emeeee!!(p1,p2,p3,p4,p5,p6)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4) e-(p5) e-(p6)
    !! for massive (and massless) electrons


  !! From file mue_em2emgleeee.f95
  use mue, only: em2emgl_eeee_old!!(p1, p2, p3, p4, p5, pole)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4) g(p5)
    !! for massive electrons, cdr?


  !! From file mue_pepe2mm.f95
  use mue, only: pepe2mmgg_eeee!!(p1,pol1,p2,pol2,p3,p4,p5,p6)
    !! e-(p1) e+(p2) -> g* -> mu-(p3) mu+(p4) g(p5) g(p6)
    !! for massive electrons & positrons

  use mue, only: pepe2mmgl_eeee_pvred!!(p1,pol1,p2,pol2,p3,p4,p5,sing)
    !! e-(p1) e+(p2) -> g* -> mu-(p3) mu+(p4) g(p5)
    !! for massive electrons & positrons

  use mue, only: pepe2mmgl_eeee_coll!!(p1,pol1,p2,pol2,p3,p4,p5,sing)
    !! e-(p1) e+(p2) -> g* -> mu-(p3) mu+(p4) g(p5)
    !! for massive electrons & positrons

  use mue, only: pepe2mml!!(p1,pol1,p2,pol2,p3,p4,sing)
    !! e-(p1) e+(p2) -> g* -> mu-(p3) mu+(p4)
    !! for massive electrons & positrons

  use mue, only: pepe2mml_emc!!(p1,pol1,p2,pol2,p3,p4,sing_emc)
    !! e-(p1) e+(p2) -> g* -> mu-(p3) mu+(p4)
    !! for massive electrons & positrons

  use mue, only: pepe2mml_ee_cplx!!(p1,pol1,p2,pol2,p3,p4,sing_ee)
    !! e-(p1) e+(p2) -> g* -> mu-(p3) mu+(p4)
    !! for massive electrons & positrons

  use mue, only: pepe2mml_ee!!(p1,pol1,p2,pol2,p3,p4,sing_ee)
    !! e-(p1) e+(p2) -> g* -> mu-(p3) mu+(p4)
    !! for massive electrons & positrons

  use mue, only: pepe2mml_mm!!(p1,pol1,p2,pol2,p3,p4,sing_mm)
    !! e-(p1) e+(p2) -> g* -> mu-(p3) mu+(p4)
    !! for massive electrons & positrons

  use mue, only: pepe2mml_em!!(p1,pol1,p2,pol2,p3,p4,sing_em)
    !! e-(p1) e+(p2) -> g* -> mu-(p3) mu+(p4)
    !! for massive electrons & positrons

  use mue, only: pepe2mmg_ee!!(p1,pol1,p2,pol2,p3,p4,p5)
    !! e-(p1) e+(p2) -> g* -> mu-(p3) mu+(p4) g(p5)
    !! for massive electrons

  use mue, only: pepe2mmg!!(p1,pol1,p2,pol2,p3,p4,p5)
    !! e-(p1) e+(p2) -> g* -> mu-(p3) mu+(p4) g(p5)
    !! for massive electrons

  use mue, only: pepezmmgx!!(p1,pol1,p2,pol2,p3,p4,p5)
    !! e-(p1) e+(p2) -> Z -> mu-(p3) mu+(p4) g(p5)
    !! for massive electrons, expanded

  use mue, only: emzemgx!!(p1,p2,p3,p4,p5)
    !! e-(p1) mu+(p2) -> Z -> e-(p3) mu+(p4)
    !! for massive electrons, expanded


  !! From file mue_mp2mp_nuc.f95
  use mue, only: mp2mpg_mp_nuc!!(p1, p2, p3, p4, p5)
   !! m-(p1) p(p2) -> mu-(p3) p(p4) y(p5)
   !! for massive muons

  use mue, only: mp2mpl_mp_nuc!!(p1, p2, p3, p4, pole)
   !! m-(p1) p(p2) -> mu-(p3) p(p4)
   !! for massive muons


  !! From file mue_em2em_nfem.f95
  use mue, only: em2em_nfem!!(z,p1,p2,q1,q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2)
    !! for massive electrons and muons


  !! From file mue_em2emgl_lbk.f95
  use mue, only: em2emgl_lbk!!(p1, p2, p3, p4, p5,pole)
   !! e-(p1) m-(p2) -> e-(p3) m-(p4) g(p5)
   !! for massive electrons

  use mue, only: em2emgl_lbk_eeee!!(p1, p2, p3, p4, p5,pole)
   !! e-(p1) m-(p2) -> e-(p3) m-(p4) g(p5)
   !! for massive electrons

  use mue, only: em2emgl_lbk11_eeee!!(p1, p2, p3, p4, p5,pole)
   !! e-(p1) m-(p2) -> e-(p3) m-(p4) g(p5)
   !! for massive electrons

  use mue, only: mp2mpgl_lbk!!(p1, p2, p3, p4, p5,pole)
   !! e-(p1) m-(p2) -> e-(p3) m-(p4) g(p5)
   !! for massive electrons

  use mue, only: em2emgl_lbk_mixd!!(p1, p2, p3, p4, p5,pole)
   !! e-(p1) m-(p2) -> e-(p3) m-(p4) g(p5)
   !! for massive electrons

  use mue, only: em2emgl_lbk_e3m1!!(p1, p2, p3, p4, p5,pole)
   !! e-(p1) m-(p2) -> e-(p3) m-(p4) g(p5)
   !! for massive electrons

  use mue, only: em2emgl_lbk_e2m2!!(p1, p2, p3, p4, p5,pole)
   !! e-(p1) m-(p2) -> e-(p3) m-(p4) g(p5)
   !! for massive electrons

  use mue, only: em2emgl_lbk_e1m3!!(p1, p2, p3, p4, p5,pole)
   !! e-(p1) m-(p2) -> e-(p3) m-(p4) g(p5)
   !! for massive electrons

  use mue, only: em2emgl_ls_mixd!!(p1, p2, p3, p4, p5,pole)
   !! e-(p1) m-(p2) -> e-(p3) m-(p4) g(p5)
   !! for massive electrons

  use mue, only: em2emgl_ls_e3m1!!(p1, p2, p3, p4, p5,pole)
   !! e-(p1) m-(p2) -> e-(p3) m-(p4) g(p5)
   !! for massive electrons

  use mue, only: em2emgl_ls_e2m2!!(p1, p2, p3, p4, p5,pole)
   !! e-(p1) m-(p2) -> e-(p3) m-(p4) g(p5)
   !! for massive electrons

  use mue, only: em2emgl_ls_e1m3!!(p1, p2, p3, p4, p5,pole)
   !! e-(p1) m-(p2) -> e-(p3) m-(p4) g(p5)
   !! for massive electrons

  use mue, only: em2emgl_lbk_mmmm!!(p1, p2, p3, p4, p5,pole)
   !! e-(p1) m-(p2) -> e-(p3) m-(p4) g(p5)
   !! for massive electrons


  !! From file mue_mat_el.f95
  use mue, only: em2em!!(p1,p2,q1,q2, lin)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2)
    !! for massive (and massless) electrons

  use mue, only: em2em_a!!(p1,p2,q1,q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2)
    !! for massive (and massless) electrons

  use mue, only: em2eml_ee!!(p1,p2,q1,q2, pole)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2)
    !! for massive electrons

  use mue, only: em2emg_ee!!(p1,p2,q1,q2,q3, lin)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(q3)
    !! for massive (and massless) electrons

  use mue, only: em2eml_mm!!(p1,p2,q1,q2,pole)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2)
    !! for massive electrons

  use mue, only: em2emg_mm!!(p1,p2,q1,q2,q3,lin)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(q3)
    !! for massive (and massless) electrons

  use mue, only: em2eml_em!!(p1,p2,q1,q2,pole)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2)
    !! for massive electrons

  use mue, only: em2emg_em!!(p1,p2,q1,q2,q3)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(q3)
    !! for massive (and massless) electrons

  use mue, only: em2emeik_ee!!(p1,p2,p3,p4, xicut, pole, lin)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) whatever
    !! for massive electrons

  use mue, only: em2emeik_em!!(p1,p2,p3,p4, xicut, pole, lin)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) whatever
    !! for massive electrons

  use mue, only: em2emeik!!(p1,p2,p3,p4, xicut, pole, lin)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) whatever
    !! for massive electrons

  use mue, only: em2emll_eeee!!(p1, p2, p3, p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! for massive electrons

  use mue, only: em2emff_eeee!!(p1, p2, p3, p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! for massive electrons, requires xieik1 = xieik2

  use mue, only: em2emgl_eeee_pv!!(p1, p2, p3, p4, p5, pole)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(q3)
    !! for massive electrons

  use mue, only: em2emgl_eeee_co!!(p1, p2, p3, p4, p5, pole)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(q3)
    !! for massive electrons

  use mue, only: em2em_aa!!(p1,p2,q1,q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2)
    !! for massive electrons, numerically interpolated

  use mue, only: em2em_alee!!(p1,p2,q1,q2,pole)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2)
    !! for massive electrons

  use mue, only: em2em_almm!!(p1,p2,q1,q2,pole)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2)
    !! for massive electrons

  use mue, only: em2em_alem!!(p1,p2,q1,q2,pole)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2)
    !! for massive electrons

  use mue, only: em2emg_aee!!(p1,p2,q1,q2,q3)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(q3)
    !! for massive electrons

  use mue, only: em2emg_amm!!(p1,p2,q1,q2,q3)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(q3)
    !! for massive electrons

  use mue, only: em2emg_aem!!(p1,p2,q1,q2,q3)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(q3)
    !! for massive electrons

  use mue, only: em2emg_a!!(p1,p2,q1,q2,q3)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(q3)
    !! for massive electrons

  use mue, only: em2em_nfee!!(y, p1,p2,q1,q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2)
    !! for massive electrons

  use mue, only: em2em_nfmm!!(y, p1,p2,q1,q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2)
    !! for massive electrons

  use mue, only: em2em_nf!!(y, p1,p2,q1,q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2)
    !! for massive electrons

  use mue, only: em2em_nfee_interpol!!(p1,p2,q1,q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2)
    !! for massive electrons, numerically interpolated

  use mue, only: ee2mm!!(p1,p2,p3,p4)
    !! e-(p1) e+(p2) -> mu+(p3) mu-(p4)
    !! for massive electrons & positrons

  use mue, only: ee2mml!!(p1, p2, p3, p4,sing)
    !! e-(p1) e+(p2) -> mu+(p3) mu-(p4)
    !! for massive electrons & positrons

  use mue, only: ee2mmg!!(p1,p2,p3,p4,p5)
    !! e-(p1) e+(p2) -> mu-(p3) mu+(p4) g(p5)
    !! for massive electrons & positrons

  use mue, only: pepe2mm!!(p1,pol1,p2,pol2,p3,p4)
    !! e-(p1) e+(p2) -> g* -> mu-(p3) mu+(p4)
    !! for massive electrons

  use mue, only: pepezmm!!(p1,pol1,p2,pol2,p3,p4)
    !! e-(p1) e+(p2) -> Z -> mu-(p3) mu+(p4)
    !! for massive electrons

  use mue, only: pepezmmx!!(p1,pol1,p2,pol2,p3,p4)
    !! e-(p1) e+(p2) -> Z -> mu-(p3) mu+(p4)
    !! for massive electrons, expanded

  use mue, only: emzemx!!(p1,p2,p3,p4)
    !! e-(p1) mu+(p2) -> Z -> e-(p3) mu+(p4)
    !! for massive electrons, expanded

  use mue, only: pepe2mma!!(p1, pol1, p2, pol2, p3, p4)
    !! e-(p1) e+(p2) -> g* -> mu-(p3) mu+(p4)
    !! for massive electrons & positrons

  use mue, only: pepezmmax!!(p1, pol1, p2, pol2, p3, p4)
    !! e-(p1) e+(p2) -> g* -> mu-(p3) mu+(p4)
    !! for massive electrons & positrons

  use mue, only: pepezmmlx!!(p1, pol1, p2, pol2, p3, p4)
    !! e-(p1) e+(p2) -> g* -> mu-(p3) mu+(p4)
    !! for massive electrons & positrons

  use mue, only: emzemlx!!(p1,p2,p3,p4)
    !! e-(p1) mu+(p2) -> Z -> e-(p3) mu+(p4)
    !! for massive electrons, expanded

  use mue, only: pepe2mmgl_nts_eeee!!(p1,pol1,p2,pol2,p3,p4,p5)
    !! e-(p1) e+(p2) -> g* -> mu-(p3) mu+(p4) g(p5)
    !! for massive electrons & positrons

  use mue, only: pepe2mmgl_eeee!!(p1,pol1,p2,pol2,p3,p4,p5,pole)
    !! e-(p1) e+(p2) -> g* -> mu-(p3) mu+(p4) g(p5)
    !! for massive electrons & positrons

  use mue, only: pepe2mmgf_eeee!!(p1,n1,p2,n2,q1,q2,q3)
    !! e-(p1) e+(p2) -> g* -> mu-(q1) mu+(q2) g(q3)
    !! for massive electrons & positrons

  use mue, only: pepe2mmll_eeee!!(p1, n1, p2, n2, p3, p4)
    !! e-(p1) e+(p2) -> g* -> mu-(p3) mu+(p4) g(p5)
    !! for massive electrons & positrons

  use mue, only: pepe2mmff_eeee!!(p1, n1, p2, n2, p3, p4)
    !! e-(p1) e+(p2) -> g* -> mu-(p3) mu+(p4)
    !! for massive electrons & positrons

  use mue, only: pepe2mm_aa!!(p1,pol1,p2, pol2, q1,q2)
    !! e-(p1) e+(p2) -> mu-(q1) mu+(q2)
    !! for massive electrons, numerically interpolated

  use mue, only: pepe2mm_alee!!(p1,pol1, p2,pol2, q1,q2)
    !! e-(p1) e+(p2) -> mu-(q1) mu+(q2)
    !! for massive electrons

  use mue, only: pepe2mmg_aee!!(p1,pol1, p2,pol2, q1,q2,q3)
    !! e-(p1) e+(p2) -> mu-(q1) mu+(q2) g(q3)
    !! for massive electrons

  use mue, only: pepe2mm_nfee!!(y, p1,pol1, p2,pol2, q1,q2)
    !! e-(p1) e+(p2) -> mu-(q1) mu+(q2)
    !! for massive electrons

  use mue, only: mp2mp!!(p1,p2,q1,q2)
    !! mu-(p1) p(p2) -> mu(q1) p(q2)
    !! for massive (and massless) muons

  use mue, only: mp2mp_nuc!!(p1,p2,q1,q2)
    !! mu-(p1) p(p2) -> mu(q1) p(q2)
    !! for massive (and massless) muons

  use mue, only: mp2mp_a!!(p1,p2,q1,q2)
    !! mu-(p1) p(p2) -> mu(q1) p(q2)
    !! for massive muons

  use mue, only: mp2mpl!!(p1,p2,q1,q2,pole)
    !! mu-(p1) p(p2) -> mu(q1) p(q2)
    !! for massive muons

  use mue, only: mp2mpl_mp!!(p1,p2,q1,q2,pole)
    !! mu-(p1) p(p2) -> mu-(q1) p(q2)
    !! for massive muons

  use mue, only: mp2mpg!!(p1,p2,q1,q2,q3)
    !! mu-(p1) p(p2) -> mu(q1) p(q2) g(q3)
    !! for massive (and massless) muons

  use mue, only: mp2mpg_mp!!(p1,p2,q1,q2,q3)
    !! mu-(p1) p(p2) -> mu-(q1) p(q2) y(q3)
    !! for massive muons

  use mue, only: mp2mpll!!(p1, p2, p3, p4)
    !! mu-(p1) p(p2) -> mu(q1) p(q2)
    !! for massive muons

  use mue, only: mp2mpff!!(p1, p2, p3, p4)
    !! mu-(p1) p(p2) -> mu(p3) p(p4)
    !! for massive muons

  use mue, only: mp2mp_aa!!(p1,p2,q1,q2)
    !! mu-(p1) p(p2) -> mu(q1) p(q2)
    !! for massive muons, numerically interpolated

  use mue, only: mp2mp_al!!(p1,p2,q1,q2)
    !! mu-(p1) p(p2) -> mu(q1) p(q2)
    !! for massive muons, numerically interpolated

  use mue, only: mp2mpg_a!!(p1,p2,q1,q2,q3)
    !! mu-(p1) p(p2) -> mu(q1) p(q2) g(q3)
    !! for massive muons, numerically interpolated

  use mue, only: mp2mp_nf!!(y,p1,p2,q1,q2)
    !! mu-(p1) p(p2) -> mu(q1) p(q2)
    !! for massive muons

  use mue, only: mp2mp_nf_interpol!!(p1,p2,q1,q2)
    !! mu-(p1) p(p2) -> mu(q1) p(q2)
    !! for massive muons, numerically interpolated

  use mue, only: mp2mpgl_pv!!(p1, p2, p3, p4, p5, pole)
    !! mu-(p1) p(p2) -> mu(p3) p(p4) y(p5)
    !! for massive muons

  use mue, only: mp2mpgf_pv!!(p1,p2,q1,q2,q3)
    !! mu-(p1) p(p2) -> mu(q1) p(q2) y(q3)
    !! for massive muons

  use mue, only: mp2mpgl_co!!(p1, p2, p3, p4, p5, pole)
    !! mu-(p1) p(p2) -> mu(p3) p(p4) y(p5)
    !! for massive muons

  use mue, only: mp2mpgf_co!!(p1,p2,q1,q2,q3)
    !! mu-(p1) p(p2) -> mu(q1) p(q2) y(q3)
    !! for massive muons

  use mue, only: mp2mpgl_nts!!(p1,p2,q1,q2,q3,pole)
    !! mu-(p1) p(p2) -> mu(q1) p(q2) y(q3)
    !! for massive electrons

  use mue, only: mp2mpgl!!(p1, p2, p3, p4, p5, pole)
    !! mu-(p1) p(p2) -> mu(p3) p(p4) g(p5)
    !! for massive muons

  use mue, only: mp2mpgf!!(p1,p2,q1,q2,q3)
    !! mu-(p1) p(p2) -> mu(q1) p(q2) g(q3)
    !! for massive muons

  use mue, only: mp2mpgg!!(p1, p2, p3, p4, p5, p6)
    !! mu-(p1) p(p2) -> mu(p3) p(p4) g(p5) g(p6)
    !! for massive (and massless) electrons & muons

  use mue, only: em2emgg_eeee!!(p1, p2, p3, p4, p5, p6)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4) g(p5) g(p6)
    !! for massive (and massless) electrons & muons

  use mue, only: em2emgg_mixd!!(p1, p2, p3, p4, p5, p6)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4) g(p5) g(p6)
    !! for massive (and massless) electrons & muons

  use mue, only: em2emgg_e3m1!!(p1, p2, p3, p4, p5, p6)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4) g(p5) g(p6)
    !! for massive (and massless) electrons & muons

  use mue, only: em2emgg_e2m2!!(p1, p2, p3, p4, p5, p6)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4) g(p5) g(p6)
    !! for massive (and massless) electrons & muons

  use mue, only: em2emgg_e1m3!!(p1, p2, p3, p4, p5, p6)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4) g(p5) g(p6)
    !! for massive (and massless) electrons & muons

  use mue, only: em2emgg_mixd_ol!!(p1, p2, p3, p4, p5, p6)
    !! mu-(p1) p(p2) -> mu(p3) p(p4) g(p5) g(p6)
    !! for massive (and massless) muons and electrons

  use mue, only: em2emgg_mmmm!!(p1, p2, p3, p4, p5, p6)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4) g(p5) g(p6)
    !! for massive (and massless) electrons & muons

  use mue, only: em2emgg!!(p1, p2, p3, p4, p5, p6)
    !! mu-(p1) p(p2) -> mu(p3) p(p4) g(p5) g(p6)
    !! for massive (and massless) muons and electrons

  use mue, only: em2eml0!!(p1, p2, p3, p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! for massless electrons

  use mue, only: em2emll0!!(p1, p2, p3, p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! for massless electrons

  use mue, only: em2emgl_nts!!(p1,p2,q1,q2,q3,pole)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(q3)
    !! for massive electrons

  use mue, only: em2emgl_nts_eeee!!(p1,p2,q1,q2,q3,pole)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(q3)
    !! for massive electrons

  use mue, only: em2emgl_nts_mixd!!(p1,p2,q1,q2,q3,pole)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(q3)
    !! for massive electrons

  use mue, only: em2emgl_nts_e3m1!!(p1,p2,q1,q2,q3,pole)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(q3)
    !! for massive electrons

  use mue, only: em2emgl_nts_e2m2!!(p1,p2,q1,q2,q3,pole)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(q3)
    !! for massive electrons

  use mue, only: em2emgl_nts_e1m3!!(p1,p2,q1,q2,q3,pole)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(q3)
    !! for massive electrons

  use mue, only: em2emgl_nts_mmmm!!(p1,p2,q1,q2,q3,pole)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(q3)
    !! for massive electrons

  use mue, only: em2emgl_ol!!(p1, p2, p3, p4, p5, pole)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4) y(p5)
    !! for massive (and massless) electrons & muons

  use mue, only: em2emgl!!(p1, p2, p3, p4, p5, pole)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4) g(p5)
    !! for massive (and massless) electrons & muons

  use mue, only: em2emgl_eeee!!(p1, p2, p3, p4, p5, pole)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4) g(p5)
    !! for massive (and massless) electrons & muons

  use mue, only: em2emgl_mixd_ol!!(p1, p2, p3, p4, p5)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4) y(p5)
    !! for massive (and massless) electrons & muons

  use mue, only: em2emgl_mixd!!(p1, p2, p3, p4, p5)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4) g(p5)
    !! for massive (and massless) electrons & muons

  use mue, only: em2emgl_e3m1!!(p1, p2, p3, p4, p5)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4) g(p5)
    !! for massive (and massless) electrons & muons

  use mue, only: em2emgl_e2m2!!(p1, p2, p3, p4, p5)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4) g(p5)
    !! for massive (and massless) electrons & muons

  use mue, only: em2emgl_e1m3!!(p1, p2, p3, p4, p5)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4) g(p5)
    !! for massive (and massless) electrons & muons

  use mue, only: em2emgl_mmmm!!(p1, p2, p3, p4, p5)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4) g(p5)
    !! for massive (and massless) electrons & muons

  use mue, only: em2emf_ee!!(p1,p2,p3,p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! for massive electrons

  use mue, only: em2emf_em!!(p1,p2,p3,p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! for massive electrons

  use mue, only: em2emf_mm!!(p1,p2,p3,p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! for massive electrons

  use mue, only: em2emf!!(p1,p2,p3,p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! for massive electrons

  use mue, only: em2em_afee!!(p1,p2,p3,p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! for massive electrons

  use mue, only: em2em_afmm!!(p1,p2,p3,p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! for massive electrons

  use mue, only: em2em_afem!!(p1,p2,p3,p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! for massive electrons

  use mue, only: em2em_af!!(p1,p2,p3,p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! for massive electrons

  use mue, only: em2em_nfem_ct!!(p1,p2,p3,p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! for massive electrons

  use mue, only: ee2mmf!!(p1,p2,p3,p4)
    !! e-(p1) e+(p2) -> mu+(p3) mu-(p4)
    !! for massive electrons and positrons

  use mue, only: pepe2mmf_ee!!(p1,pol1,p2,pol2,p3,p4)
    !! e-(p1) e+(p2) -> g* -> mu-(p3) mu+(p4)
    !! for massive electrons and positrons

  use mue, only: pepe2mmf!!(p1,pol1,p2,pol2,p3,p4)
    !! e-(p1) e+(p2) -> g* -> mu-(p3) mu+(p4)
    !! for massive electrons and positrons

  use mue, only: pepezmmfx!!(p1,pol1,p2,pol2,p3,p4)
    !! e-(p1) e+(p2) -> Z -> mu-(p3) mu+(p4)
    !! for massive electrons and positrons

  use mue, only: emzemfx!!(p1,p2,p3,p4)
    !! e-(p1) mu+(p2) -> Z -> e-(p3) mu+(p4)
    !! for massive electrons and positrons

  use mue, only: pepe2mm_afee!!(p1,pol1,p2,pol2,p3,p4)
    !! e-(p1) e+(p2) -> mu-(p3) mu+(p4)
    !! for massive electrons and positrons

  use mue, only: mp2mpf!!(p1,p2,p3,p4)
    !! mu-(p1) p(p2) -> mu(p3) p(p4)
    !! for massive muons

  use mue, only: mp2mpf_mp!!(p1,p2,p3,p4)
    !! mu-(p1) p(p2) -> mu(p3) p(p4)
    !! for massive muons

  use mue, only: mp2mp_af!!(p1,p2,p3,p4)
    !! mu-(p1) p(p2) -> mu(p3) p(p4)
    !! for massive muons

  use mue, only: em2eml!!(p1,p2,p3,p4,sing)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! for massive electrons

  use mue, only: em2emc!!(p1,p2,p3,p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! for massive electrons

  use mue, only: em2emg!!(p1,p2,p3,p4,p5)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4) g(p5)
    !! for massive electrons

  use mue, only: em2emgf!!(p1,p2,q1,q2,q3)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(q3)
    !! for massive electrons

  use mue, only: em2emgf_eeee!!(p1,p2,q1,q2,q3)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(q3)
    !! for massive electrons

  use mue, only: em2emgf_eeee_pv!!(p1,p2,q1,q2,q3)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(q3)
    !! for massive electrons

  use mue, only: em2emgf_eeee_co!!(p1,p2,q1,q2,q3)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(q3)
    !! for massive electrons

  use mue, only: em2emgf_mixd!!(p1,p2,p3,p4,p5)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4) g(p5)
    !! for massive electrons

  use mue, only: em2emgf_e3m1!!(p1,p2,p3,p4,p5)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4) g(p5)
    !! for massive electrons

  use mue, only: em2emgf_e2m2!!(p1,p2,p3,p4,p5)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4) g(p5)
    !! for massive electrons

  use mue, only: em2emgf_e1m3!!(p1,p2,p3,p4,p5)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4) g(p5)
    !! for massive electrons

  use mue, only: em2emgf_mmmm!!(p1,p2,q1,q2,q3)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(q3)
    !! for massive electrons

  use mue, only: em2emffz!!(p1, p2, p3, p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! for massified electrons

  use mue, only: em2emffz_eeee!!(p1, p2, p3, p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! for massified electrons

  use mue, only: em2emff_mmmm!!(p1,p2,p3,p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! for massive electrons

  use mue, only: em2emffz_mixd!!(p1, p2, p3, p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! for massified electrons

  use mue, only: em2emffz_e3m1!!(p1, p2, p3, p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! for massified electrons

  use mue, only: em2emffz_e2m2!!(p1, p2, p3, p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! for massified electrons

  use mue, only: em2emffz_e1m3!!(p1, p2, p3, p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! for massified electrons


  !! From file mue.f95
  use mue, only: em2em_ee_part!!(p1, p2, p3, p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! both massive and massless electrons

  use mue, only: em2em_mm_part!!(p1, p2, p3, p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! both massive and massless electrons

  use mue, only: em2em_part!!(p1, p2, p3, p4)
    !! e-(p1) mu-(p2) -> e-(p3) mu-(p4)
    !! both massive and massless electrons

  use mue, only: mp2mp_part!!(p1, p2, p3, p4)
    !! mu-(p1) p(p2) -> mu-(p3) p(p4)
    !! for massive (and massless) muons

  use mue, only: ee2mm_ee_part!!(p1, p2, p3, p4)
    !! e-(p1) e+(p2) -> g* -> mu-(p3) mu+(p4)
    !! for massive electrons & positrons

  use mue, only: ee2mm_part!!(p1, p2, p3, p4)
    !! e-(p1) e+(p2) -> g* -> mu-(p3) mu+(p4)
    !! for massive electrons & positrons

  use mue, only: em2emg_em_s!!(p1,p2,q1,q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(ksoft)
    !! for massive (and massless) electrons

  use mue, only: mp2mpg_mp_s!!(p1,p2,q1,q2)
    !! mu-(p1) p(p2) -> mu-(q1) p(q2) g(ksoft)
    !! for massive (and massless) muons

  use mue, only: em2emg_aem_s!!(p1,p2,q1,q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(ksoft)
    !! for massive (and massless) electrons

  use mue, only: em2emgl_mixd_s!!(p1,p2,q1,q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(ksoft)
    !! for massive electrons

  use mue, only: em2emgl_e3m1_s!!(p1,p2,q1,q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(ksoft)
    !! for massive electrons

  use mue, only: em2emgl_e2m2_s!!(p1,p2,q1,q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(ksoft)
    !! for massive electrons

  use mue, only: em2emgl_e1m3_s!!(p1,p2,q1,q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(ksoft)
    !! for massive electrons

  use mue, only: mp2mpgl_s!!(p1,p2,q1,q2)
    !! mu-(p1) p(p2) -> mu(q1) p(q2) y(ksoft)
    !! for massive muons

  use mue, only: em2emgf_mixd_s!!(p1,p2,q1,q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(ksoft)
    !! for massive electrons

  use mue, only: em2emgf_e3m1_s!!(p1,p2,q1,q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(ksoft)
    !! for massive electrons

  use mue, only: em2emgf_e2m2_s!!(p1,p2,q1,q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(ksoft)
    !! for massive electrons

  use mue, only: em2emgf_e1m3_s!!(p1,p2,q1,q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(ksoft)
    !! for massive electrons

  use mue, only: em2emgg_mixd_sh!!(p1, p2, q1, q2, q4)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(ksoft) g(q4)
    !! for massive electrons

  use mue, only: em2emgg_e3m1_sh!!(p1, p2, q1, q2, q4)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(ksoft) g(q4)
    !! for massive electrons

  use mue, only: em2emgg_e2m2_sh!!(p1, p2, q1, q2, q4)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(ksoft) g(q4)
    !! for massive electrons

  use mue, only: em2emgg_e1m3_sh!!(p1, p2, q1, q2, q4)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(ksoft) g(q4)
    !! for massive electrons

  use mue, only: em2emgg_mixd_hs!!(p1, p2, q1, q2, q3)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(q3) g(ksoft)
    !! for massive electrons

  use mue, only: em2emgg_e3m1_hs!!(p1, p2, q1, q2, q3)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(ksoft) g(q4)
    !! for massive electrons

  use mue, only: em2emgg_e2m2_hs!!(p1, p2, q1, q2, q3)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(ksoft) g(q4)
    !! for massive electrons

  use mue, only: em2emgg_e1m3_hs!!(p1, p2, q1, q2, q3)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(ksoft) g(q4)
    !! for massive electrons

  use mue, only: em2emgg_mixd_ss!!(p1, p2, q1, q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(ksoft) g(ksoft)
    !! for massive electrons

  use mue, only: em2emgg_e3m1_ss!!(p1, p2, q1, q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(ksoft) g(ksoft)
    !! for massive electrons

  use mue, only: em2emgg_e2m2_ss!!(p1, p2, q1, q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(ksoft) g(ksoft)
    !! for massive electrons

  use mue, only: em2emgg_e1m3_ss!!(p1, p2, q1, q2)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(ksoft) g(ksoft)
    !! for massive electrons


  !! From file mue_mp2mpgl_coll.opt.f95
  use mue, only: em2emgl_eeee_coll!!(p1, p2, p3, p4, p5, pole)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(p5)
    !! for massive electrons

  use mue, only: mp2mpgl_coll!!(p1, p2, p3, p4, p5, pole)
    !! mu-(p1) p(p2) -> mu(p3) p(p4) g(p5)
    !! for massive muons


  !! From file mue_mp2mpgl_pvred.opt.f95
  use mue, only: em2emgl_eeee_pvred!!(p1, p2, p3, p4, p5, pole)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(p5)
    !! for massive electrons

  use mue, only: mp2mpgl_pvred!!(p1, p2, p3, p4, p5, pole)
    !! mu-(p1) p(p2) -> mu(p3) p(p4) g(p5)
    !! for massive muons


  !! From file mue_mp2mpgl_fullcoll.opt.f95
  use mue, only: em2emgl_eeee_fullcoll!!(p1, p2, p3, p4, p5, pole)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) g(p5)
    !! for massive electrons

  use mue, only: mp2mpgl_fullcoll!!(p1, p2, p3, p4, p5, pole)
    !! mu-(p1) p(p2) -> mu-(q1) p(q2) g(p5)
    !! for massive muons


#endif
#ifdef HAVE_MUDECRARE

  !! From file mudecrare_mat_el.f95
  use mudecrare, only: pt2mnneeav!!(p1,n1,p2,qA,qB,p3,p4)
    !! tau+(p1) -> mu+(p2) nu_mu \bar{nu}_tau e-(p3) e+(p4)
    !! tau-(p1) -> mu-(p2) \bar{nu}_e   nu_tau e+(p3) e-(p4)

  use mudecrare, only: pm2enneeav!!(p1,n1,p2,qa,qb,p3,p4)
    !! mu+(p1) -> e+(p2) nu_mu \bar{nu}_mu e-(p3) e+(p4)
    !! mu-(p1) -> e-(p2) \bar{nu}_e  nu_tau e+(p3) e-(p4)

  use mudecrare, only: pt2mnneelav!!(p1,n1,p2,qA,qB,p3,p4, single)
    !! tau+(p1) -> mu+(p2) nu_mu \bar{nu}_tau e-(p3) e+(p4)
    !! tau-(p1) -> mu-(p2) \bar{nu}_e   nu_tau e+(p3) e-(p4)

  use mudecrare, only: pm2enneelav!!(p1,n1,p2,qA,qB,p3,p4, single)
    !! mu+(p1) -> e+(p2) nu_mu \bar{nu}_tau e-(p3) e+(p4)
    !! mu-(p1) -> e-(p2) \bar{nu}_e   nu_tau e+(p3) e-(p4)

  use mudecrare, only: pt2mnneegav!!(p1,n1,p2,qA,qB,p3,p4,p7)
    !! tau+(p1)->mu+(p2) nu_tau \bar{nu}_e e-(p3) e+(p4) g(p7)

  use mudecrare, only: pm2enneegav!!(p1,n1,p2,qA,qB,p3,p4,p7)
    !!  mu+(p1)->e+(p2) nu_mu \bar{nu}_e e-(p3) e+(p4) g(p7)

  use mudecrare, only: pm2enneecav!!(p1, n1, p2, p3, p4, p5, p6)
    !! mu+(p1) -> e+(p2) nu_mu \bar{nu}_e e-(p5) e+(p6)
    !! mu-(p1) -> e-(p2) \bar{nu}_mu  nu_e e+(p5) e-(p6)
    !! for massive electron

  use mudecrare, only: pt2mnneecav!!(p1, n1, p2, p3, p4, p5, p6)
    !! tau+(p1) -> mu+(p2) nu_tau \bar{nu}_mu e-(p5) e+(p6)
    !! tau-(p1) -> mu-(p2) \bar{nu}_tau  nu_mu e+(p5) e-(p6)
    !! for massive electron

  use mudecrare, only: pt2mnneeav_a!!(p1,n1,p2,qA,qB,p3,p4)
    !! tau+(p1) -> mu+(p2) nu_mu \bar{nu}_tau e-(p3) e+(p4)
    !! tau-(p1) -> mu-(p2) \bar{nu}_e   nu_tau e+(p3) e-(p4)

  use mudecrare, only: pm2enneeav_a!!(p1,n1,p2,qA,qB,p3,p4)
    !! mu+(p1) -> e+(p2) nu_mu \bar{nu}_tau  e-(p3) e+(p4)
    !! mu-(p1) -> e-(p2) \bar{nu}_e   nu_tau e+(p3) e-(p4)
    !! for massive electron

  use mudecrare, only: pm2enneeav_ai!!(p1,n1,p2,qA,qB,p3,p4)
    !! mu+(p1) -> e+(p2) nu_mu \bar{nu}_tau  e-(p3) e+(p4)
    !! mu-(p1) -> e-(p2) \bar{nu}_e   nu_tau e+(p3) e-(p4)
    !! for massive electron


  !! From file mudecrare.f95
  use mudecrare, only: m2ennee_part!!(p1,p2,pA,pB,p3,p4)
    !! mu+(p1) -> e+(p2) e-(p3) e+(p4) 2nu
    !! for massive electron
    !! average over neutrino tensor taken

  use mudecrare, only: t2mnnee_part!!(p1,p2,pA,pB,p3,p4)
    !!  tau+(p1)->mu+(p2) e-(p3) e+(p4) 2nu
    !! for massive electron
    !! average over neutrino tensor taken


#endif
#ifdef HAVE_MUDEC

  !! From file mudec_pm2ennggav.f95
  use mudec, only: pm2ennggav!!(p1, n1, p2, p3, p4, p5, p6)
    !! mu+(p1) -> e+(p2) nu_e \bar{nu}_mu g(p5) g(p6)
    !! mu-(p1) -> e-(p2) \bar{nu}_e  nu_mu g(p5) g(p6)
    !! for massive (and massless) electron
    !! average over neutrino tensor taken


  !! From file mudec_mat_el.f95
  use mudec, only: pm2ej!!(p1,n1,p2,p3)
  !! mu+(p1) -> e+(p2) J(p3)
  !! mu-(p1) -> e-(p2) J(p3)

  use mudec, only: pm2ejl!!(p1,n1,p2,p3,sing)
  !! mu+(p1) -> e+(p2) J(p3)
  !! mu-(p1) -> e-(p2) J(p3)

  use mudec, only: pm2ejg!!(p1,n1,p2,p3,p4)
  !! mu+(p1) -> e+(p2) J(p3) g(p4)
  !! mu-(p1) -> e-(p2) J(p3) g(p4)

  use mudec, only: pm2ennav!!(q1,n1,q2, q3, q4, lin)
    !! mu+(q1) -> e+(q2) nu_e \bar{nu}_mu
    !! mu-(q1) -> e-(q2) \bar{nu}_e  nu_mu
    !! for massive (and massless) electron
    !! average over neutrino tensor taken

  use mudec, only: pm2ennlav!!(q1, n1, q2, q3, q4, pole,cdr)
    !! mu+(q1) -> e+(q2) nu_e \bar{nu}_mu
    !! mu-(q1) -> e-(q2) \bar{nu}_e  nu_mu
    !! average over neutrino tensor taken
    !! for massive electron

  use mudec, only: pm2enngav!!(q1,n,q2,q3,q4,q5, linear)
    !! mu+(q1) -> e+(q2) nu_e \bar{nu}_mu g(q5)
    !! mu-(q1) -> e-(q2) \bar{nu}_e nu_mu g(q5)
    !! for massive electron
    !! average over neutrino tensor taken

  use mudec, only: pm2ennllavz!!(p1, n1, p2, p3, p4, double, single)
    !! mu+(p1) -> e+(p2) nu_e \bar{nu}_mu
    !! mu-(p1) -> e-(p2) \bar{nu}_e  nu_mu
    !! for massified electrons
    !! average over neutrino tensor taken

  use mudec, only: pm2ennffavz!!(p1, n1, p2, p3, p4)
    !! mu+(p1) -> e+(p2) nu_e \bar{nu}_mu
    !! mu-(p1) -> e-(p2) \bar{nu}_e  nu_mu
    !! for massified electrons
    !! average over neutrino tensor taken

  use mudec, only: pm2ennllav!!(p1, n1, p2, p3, p4)
    !! mu+(p1) -> e+(p2) nu_e \bar{nu}_mu
    !! mu-(p1) -> e-(p2) \bar{nu}_e  nu_mu
    !! for massified electrons
    !! average over neutrino tensor taken

  use mudec, only: pm2ennffav!!(p1, n1, p2, p3, p4)
    !! mu+(p1) -> e+(p2) nu_e \bar{nu}_mu
    !! mu-(p1) -> e-(p2) \bar{nu}_e  nu_mu
    !! for massified electrons
    !! average over neutrino tensor taken

  use mudec, only: pm2ennav_nf!!(y,p1,n1,p2,p3,p4)
    !! mu+(p1) -> e+(p2) nu_e \bar{nu}_mu
    !! mu-(p1) -> e-(p2) \bar{nu}_e  nu_mu
    !! hyperspherical method
    !! average over neutrino tensor taken

  use mudec, only: pm2ennav_nf_interpol!!(p1,n1,p2,p3,p4)
    !! mu+(p1) -> e+(p2) \nu_e \bar{\nu}_\mu
    !! mu-(p1) -> e-(p2) \bar{nu}_e  \nu_\mu
    !! hyperspherical method
    !! average over neutrino tensor taken
    !! numerically interpolated

  use mudec, only: pm2ennfav!!(p1, n1, p2, p3, p4)
    !! mu+(p1) -> e+(p2) nu_e \bar{nu}_mu
    !! mu-(p1) -> e-(p2) \bar{nu}_e  nu_mu
    !! for massive electron
    !! average over neutrino tensor taken

  use mudec, only: pm2enngcav!!(p1, n1, p2, p3, p4, p5)
    !! mu+(p1) -> e+(p2) nu_e \bar{nu}_mu g(p5)
    !! mu-(p1) -> e-(p2) \bar{nu}_e  nu_mu g(p5)
    !! for massive electron

  use mudec, only: pm2ejf!!(p1,n1,p2,p3)
  !! mu+(p1) -> e+(p2) J(p3)
  !! mu-(p1) -> e-(p2) J(p3)

  use mudec, only: pm2enngfavz!!(p1,n,p2,p3,p4,p5)
  !! mu+(p1) -> e+(p2) \nu_e \bar{\nu}_\mu g(p5)
  !! mu-(p1) -> e-(p2) \bar{nu}_e  \nu_\mu g(p5)
  !! for massified electron, finite matrix element
  !! average over neutrino tensor taken


  !! From file mudec.f95
  use mudec, only: m2enn_part!!(q1, q2, q3, q4)
    !! mu+(q1) -> e+(q2) nu_e(q3) \bar{nu}_mu(q4)
    !! mu-(q1) -> e-(q2) \bar{nu}_e(q3)  nu_mu(q4)
    !! for massive electron
    !! average over neutrino tensor taken

  use mudec, only: m2ej_part!!(p1, p2, p3)
    !! mu+(p1) -> e+(p2) J(p3)
    !! mu-(p1) -> e-(p2) J(p3)

  use mudec, only: pm2ennggav_s!!(q1,n1,q2,q3,q4,q5)
    !! mu+(q1) -> e+(q2) nu_e(q3) \bar{nu}_mu(q4) g(q5) g(ksoft)
    !! mu-(q1) -> e-(q2) \bar{nu}_e(q3)  nu_mu(q4) g(q5) g(ksoft)
    !! for massive (and massless) electron

  use mudec, only: pm2enngfavz_s!!(q1,n1,q2,q3,q4)
    !! mu+(q1) -> e+(q2) \nu_e(q3) \bar{\nu}_\mu(q4) y(ksoft)
    !! mu-(q1) -> e-(q2) \bar{nu}_e(q3)  \nu_\mu(q4) y(ksoft)
    !! for massified electron
    !! average over neutrino tensor taken


  !! From file mudec_pm2ennglav.flex.opt.f95
  use mudec, only: pm2ennglav!!(p1,n,p2,p3,p4,p5,pole)
    !! mu+(p1) -> e+(p2) nu_e \bar{nu}_mu g(p5)
    !! mu-(p1) -> e-(p2) \bar{nu}_e  nu_mu g(p5)
    !! for massive electron
    !! average over neutrino tensor taken

  use mudec, only: pm2enngfav!!(p1,n,p2,p3,p4,p5)
    !! mu+(p1) -> e+(p2) nu_e \bar{nu}_mu g(p5)
    !! mu-(p1) -> e-(p2) \bar{nu}_e  nu_mu g(p5)
    !! for massive electron finite matrix element
    !! average over neutrino tensor taken

  use mudec, only: pm2ennglav0!!(p1,n,p2,p3,p4,p5,single, double)
    !! mu+(p1) -> e+(p2) nu_e \bar{nu}_mu g(p5)
    !! mu-(p1) -> e-(p2) \bar{nu}_e  nu_mu g(p5)
    !! for massless electron
    !! average over neutrino tensor taken

  use mudec, only: pm2ennglavz!!(p1,n,p2,p3,p4,p5,single)
    !! mu+(p1) -> e+(p2) nu_e \bar{nu}_mu g(p5)
    !! mu-(p1) -> e-(p2) \bar{nu}_e  nu_mu g(p5)
    !! for massified electron
    !! average over neutrino tensor taken

  use mudec, only: pm2enngfavz!!(p1,n,p2,p3,p4,p5)
    !! mu+(p1) -> e+(p2) nu_e \bar{nu}_mu g(p5)
    !! mu-(p1) -> e-(p2) \bar{nu}_e  nu_mu g(p5)
    !! for massified electron, finite matrix element
    !! average over neutrino tensor taken


#endif
#ifdef HAVE_EE

  !! From file ee_ee2eel.f95
  use ee, only: ee2eel!!(p1, p2, p3, p4, pole, img)
    !! e-(q1) e-(q2) -> e-(q1) e-(q2)
    !! for massive electrons


  !! From file ee_ee2eeg.f95
  use ee, only: ee2eeg!!(p1,p2,p3,p4,p5)
    !! e-(p1) e-(p2) -> e-(p3) e-(p4) g(p5)


  !! From file ee_ee2ee_nfbx.f95
  use ee, only: ee2ee_nfbx!!(z,p1,p2,q1,q2)
    !! e-(p1) e-(p2) -> e-(q1) e-(q2)
    !! for massive electrons


  !! From file ee_ee2eegl_nts.f95
  use ee, only: ee2eegl_nts!!(p1, p2, p3, p4, p5)
   !! e-(p1) e-(p2) -> e-(p3) e-(p4) g(p5)
   !! for massive electrons


  !! From file ee_mat_el.f95
  use ee, only: ee2ee!!(p1,p2,q1,q2)
    !! e-(p1) e-(p2) -> e-(q1) e-(q2)
    !! for massive electrons

  use ee, only: ee2ee_a!!(p1,p2,p3,p4)
    !! e-(p1) e-(p2) --> e-(p3) e-(p4)
    !! massive electrons

  use ee, only: eb2eb!!(p1,p2,q1,q2)
    !! e-(p1) e+(p2) -> e-(q1) e+(q2)
    !! for massive electrons & positrons

  use ee, only: eb2ebl!!(p1, p2, q1, q2,pole,img)
    !! e-(p1) e+(p2) -> e-(q1) e+(q2)
    !! for massive electrons & positrons

  use ee, only: eb2eb_a!!(p1, p2, q1, q2)
    !! e-(p1) e+(p2) -> e-(q1) e+(q2)
    !! for massive electrons & positrons

  use ee, only: eb2ebg!!(p1,p2,p3,p4,p5)
    !! e-(p1) e+(p2) -> e-(p3) e+(p4) g(p5)
    !! for massive electrons & positrons

  use ee, only: eb2ebgl_nts!!(p1,p2,p3,p4,p5)
    !! e-(p1) e+(p2) -> e-(p3) e+(p4) g(p5)
    !! for massive electrons & positrons

  use ee, only: ee2eellz!!(p1, p2, p3, p4, doub, sing)
    !! e-(p1) e-(p2) -> e-(p3) e-(p4)
    !! for massified electrons

  use ee, only: ee2eeffz!!(p1, p2, p3, p4)
    !! e-(p1) e-(p2) -> e-(p3) e-(p4)
    !! for massified electrons

  use ee, only: ee2eegl!!(p1, p2, p3, p4, p5)
    !! e-(p1) e-(p2) -> e-(p3) e-(p4) g(p5)
    !! for massive electrons

  use ee, only: ee2ee_aa!!(p1,p2,p3,p4)
    !! e-(p1) e-(p2) --> e-(p3) e-(p4)
    !! massive electrons

  use ee, only: ee2ee_al!!(p1,p2,p3,p4,pole)
    !! e-(p1) e-(p2) --> e-(q1) e-(q2)
    !! massive electrons

  use ee, only: ee2eeg_a!!(p1,p2,p3,p4,p5)
    !! e-(p1) e-(p2) --> e-(p3) e-(p4) g(p5)
    !! massive electrons

  use ee, only: ee2ee_nf!!(y,p1,p2,p3,p4)
    !! e-(p1) e-(p2) --> e-(p3) e-(p4)
    !! massive electrons

  use ee, only: ee2ee_nftr!!(y,p1,p2,p3,p4)
    !! e-(p1) e-(p2) --> e-(p3) e-(p4)
    !! massive electrons

  use ee, only: eb2ebllz!!(p1, p2, p3, p4, doub, sing)
    !! e-(p1) e+(p2) -> e-(p3) e+(p4)
    !! for massified electrons

  use ee, only: eb2ebffz!!(p1, p2, p3, p4)
    !! e-(p1) e+(p2) -> e-(p3) e+(p4)
    !! for massified electrons

  use ee, only: eb2eb_aa!!(p1, p2, q1, q2)
    !! e-(p1) e+(p2) -> e-(q1) e+(q2)
    !! for massive electrons & positrons

  use ee, only: eb2ebg_a!!(p1,p2,p3,p4,p5)
    !! e-(p1) e+(p2) -> e-(p3) e+(p4) g(p5)
    !! for massive electrons & positrons

  use ee, only: eb2ebgg!!(p1,p2,p3,p4,p5,p6)
    !! e-(p1) e+(p2) -> e-(p3) e+(p4) g(p5) g(p6)
    !! for massive electrons & positrons

  use ee, only: eb2ebgl!!(p1, p2, p3, p4, p5)
    !! e-(p1) e+(p2) -> e-(p3) e+(p4) g(p5)
    !! for massive electrons & positrons

  use ee, only: ee2eef!!(p1,p2,q1,q2)
    !! e-(p1) e-(p2) --> e-(q1) e-(q2)
    !! massive electrons

  use ee, only: ee2ee_af!!(p1,p2,q1,q2)
    !! e-(p1) e-(p2) --> e-(q1) e-(q2)
    !! massive electrons

  use ee, only: eb2ebf!!(p1,p2,q1,q2)
    !! e-(p1) e+(p2) --> e-(q1) e+(q2)
    !! massive electrons & positrons

  use ee, only: ee2eegf!!(p1,p2,q1,q2,q3)
    !! e-(p1) e-(p2) --> e-(q1) e-(q2) g(q3)
    !! massive electrons

  use ee, only: eb2ebgf!!(p1,p2,q1,q2,q3)
    !! e-(p1) e+(p2) --> e-(q1) e+(q2) g(q3)
    !! massive electrons & positrons


  !! From file ee.f95
  use ee, only: ee2ee_part!!(p1, p2, p3, p4)
    !! e-(p1) e-(p2) --> e-(p3) e-(p4)
    !! both massive and massless electrons

  use ee, only: eb2eb_part!!(p1, p2, p3, p4)
    !! e-(p1) e+(p2) -> e-(p3) e+(p4)
    !! both massive and massless electrons


  !! From file ee_ee2eegg.opt.f95
  use ee, only: ee2eegg!!(p1,p2,p3,p4,p5,p6)
    !! e-(p1) e-(p2) -> e-(p3) e-(p4) g(p5) g(p6)


#endif
                 !!!!!!!!!!!!!!!!!!!!!!!!!!!!
                       END MODULE MAT_EL
                 !!!!!!!!!!!!!!!!!!!!!!!!!!!!
