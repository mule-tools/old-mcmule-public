  PROGRAM TEST
  use integrands
#ifdef HAVE_MUDEC
  use mudec_test, only: testmudec
#endif
#ifdef HAVE_MUDECRARE
  use mudecrare_test, only: testmudecrare
#endif
#ifdef HAVE_MUE
  use mue_test, only: testmue
#endif
#ifdef HAVE_MISC
  use misc_test, only: testmisc
#endif
#ifdef HAVE_EE
  use ee_test, only: testee
#endif
  use user_dummy
  use testtools, only: nfail, npass, timing, &
                       vegastruezero, verboseint, &
                       printhead, cleanslate
  implicit none
  character(len=32) :: arg
  integer i
  logical onlyfast

#define USERMOD testtools
#include "include-user.f95"

  call printhead
  call init_cll(5,folder_name="")
  call initcachesystem_cll(1,5)
  call constants
  call rdata

  call initflavour("muone")
  musq = Mm**2
  npass=0 ; nfail= 0
  call cpu_time(timing(1))
  pol1 = (/ 0., 0., 0., 0. /)


  onlyfast = .false.
  i = 1
  do
    call get_command_argument(i, arg)
    if (len_trim(arg) == 0) exit
    select case(trim(arg))
      case('-fast')
        onlyfast = .true.
      case('-ci')
        onlyfast = .false.
        vegastruezero = .true.
      case('-verbose')
        verboseint = .true.
    end select
    i = i+1
  enddo
  call cleanslate


  ! Global tests
  call testloop
  call testsplit
  call testNTSsoft
  call testHyperS
#ifdef HAVE_MUDEC
#ifdef HAVE_MUDECRARE
  call testmudecdixon
  call testmudecpol
#endif
#endif

  ! PG tests
  call switchoffcachesystem_cll
#ifdef HAVE_MUDECRARE
  call testmudecrare(onlyfast)
#endif
#ifdef HAVE_MUDEC
  call testmudec(onlyfast)
#endif

#ifdef HAVE_MUE
#ifdef HAVE_EE
  call testOpenLoops
  call testOpenLoopsRef
#endif
#endif

  ! PG tests (cont.)
#ifdef HAVE_MUE
  call testmue(onlyfast)
#endif
#ifdef HAVE_MISC
  call testmisc(onlyfast)
#endif
#ifdef HAVE_EE
  call testee(onlyfast)
#endif

  call cpu_time(timing(2))
  print*
  write(*,'(A,F5.1,A)')"Test suite completed in ",timing(2)-timing(1),"s."
  write(*,'(A,I4,A,I4,A,I2)') "passing ",npass(1),'/',npass(1)+nfail(1),' tests, failing',nfail(1)

  if (nfail(1).gt.0) then
    write(*,'(A,I2,A)')char(27)//'[31mWe have failed ',nfail(1),' tests!'//char(27)//'[0m'
    stop 9
  endif


contains


  SUBROUTINE TESTLOOP
  use testtools
  implicit none
  real(kind=prec) :: x, xmin, xmax, t1, t2 ,t3
  real(kind=prec) :: m, m1, m2, thresh1, thresh2, thresh3
  real(kind=prec) :: res, e(7), v(7), a_cheb(10)

  call initflavour("muone")
  call blockstart("Test Loop Functions against Collier")

  musq = me**2

  print*, "-------------------------------------------------------------"
  print*, "discb_cplx(x,m) & scalarc0ir6_cplx(x,m) & scalarc0_cplx(x,m) & scalard0ir16(x,y,m):"
  print*, "-------------------------------------------------------------"
  m = 1.
  print*, "z=x/4m^2<<0:"
  x = -1000.
  call checkcplx("discb_cplx(x,m):", discb_cplx(x,m),cmplx(-6.92355799826815,0))
  call checkcplx("scalarc0ir6_cplx(x,m)", scalarc0ir6_cplx(x,m), cmplx(-0.0221988314727077,0))
  call checkcplx("scalarc0_cplx(x,m)", scalarc0_cplx(x,m), cmplx(-0.0303933450752562,0))
  call check("scalard0ir16(x,y,m), x < m^2", scalard0ir16(-100.,x,m), 0.000581762910407013)
  call check("scalard0ir16(x,y,m), x > m^2", scalard0ir16(100.,x,m), -0.000590729351432828)
  print*, "z=x/4m^2~0:"
  x = -1.E-10
  call checkcplx("discb_cplx(x,m):", discb_cplx(x,m),cmplx(-2.00000000001667,0))
  call checkcplx("scalarc0ir6_cplx(x,m)", scalarc0ir6_cplx(x,m), cmplx(-8.33333333308333E-12,0))
  call checkcplx("scalarc0_cplx(x,m)", scalarc0_cplx(x,m), cmplx(-493467.707122835,0))
  call check("scalard0ir16(x,y,m), x < m^2", scalard0ir16(-100.,x,m), 0.0357932724434313)
  call check("scalard0ir16(x,y,m), x > m^2", scalard0ir16(100.,x,m), -0.0363143419200023)
  print*, "z=x/4m^2~1:"
  x = 4.+1.E-10
  call checkcplx("discb_cplx(x,m):", discb_cplx(x,m),cmplx(-5E-11,0.0000157079632677526),1E-7)
  call checkcplx("scalarc0ir6_cplx(x,m)", scalarc0ir6_cplx(x,m), cmplx(-493479.22004830,3.61689220616252E6),1E-7)
  call checkcplx("scalarc0_cplx(x,m)", scalarc0_cplx(x,m), cmplx(0.69314718055256,-1.57079632676872),1E-7)
  call check("scalard0ir16(x,y,m), x < m^2", scalard0ir16(-100.,x,m), 4885.8871775028,1E-7)
  call check("scalard0ir16(x,y,m), x > m^2", scalard0ir16(100.,x,m), -14953.8895457076,1E-7)
  print*, "z=x/4m^2>1:"
  x = 1000.
  call checkcplx("discb_cplx(x,m):", discb_cplx(x,m),cmplx(-6.89192692855652,3.13530317249943))
  call checkcplx("scalarc0ir6_cplx(x,m)", scalarc0ir6_cplx(x,m), cmplx(0.0172837223901430,-0.0217322697578707))
  call checkcplx("scalarc0_cplx(x,m)", scalarc0_cplx(x,m), cmplx(0.0255387664947113,-0.0217385813331635))
  call check("scalard0ir16(x,y,m), x < m^2", scalard0ir16(-100.,x,m), -0.000583100730081945)
  call check("scalard0ir16(x,y,m), x > m^2", scalard0ir16(100.,x,m), 0.000392298729761387)
  print*, "z=x/4m^2>>1:"
  x = 1000000.
  call checkcplx("discb_cplx(x,m):", discb_cplx(x,m),cmplx(-13.8154809269165,3.1415863703982))
  call checkcplx("scalarc0ir6_cplx(x,m)", scalarc0ir6_cplx(x,m), cmplx(0.0000888545501683450,-0.0000434027807137453))
  call checkcplx("scalarc0_cplx(x,m)", scalarc0_cplx(x,m), cmplx(0.0000970792645831187,-0.0000434027869969589))
  call check("scalard0ir16(x,y,m), x < m^2", scalard0ir16(-100.,x,m), -1.213721363387617e-6)
  call check("scalard0ir16(x,y,m), x > m^2", scalard0ir16(100.,x,m), 1.03327241971770e-6)
  print*, "0<z=x/4m^2<1:"
  x = .5
  call checkcplx("discb_cplx(x,m):", discb_cplx(x,m),cmplx(-1.91217508370363,0))
  call checkcplx("scalarc0ir6_cplx(x,m):", scalarc0ir6_cplx(x,m),cmplx(0.0488531473905737,0))
  call checkcplx("scalarc0_cplx(x,m):", scalarc0_cplx(x,m),cmplx(1.45542479459910,-5.74435531100865))
  call check("scalard0ir16(x,y,m), x < m^2", scalard0ir16(-100.,x,m), 0.0387843929058806)
  call check("scalard0ir16(x,y,m), x > m^2", scalard0ir16(100.,x,m), -0.0393471669239853)
  x = 3.5
  call checkcplx("discb_cplx(x,m):", discb_cplx(x,m),cmplx(-0.914242542623208,0))
  call checkcplx("scalarc0ir6_cplx(x,m):", scalarc0ir6_cplx(x,m),cmplx(2.72283407613356,0))
  call checkcplx("scalarc0_cplx(x,m):", scalarc0_cplx(x,m),cmplx(0.73328232857441,-1.71636434152593))
  call check("scalard0ir16(x,y,m), x < m^2", scalard0ir16(-100.,x,m), 0.1102037946075958)
  call check("scalard0ir16(x,y,m), x > m^2", scalard0ir16(100.,x,m), -0.111691327412433)


  print*, "-------------------------------------------------------------"
  print*, "discb_cplx(x,m1,m2) & scalarc0ir6_cplx(x,m1,m2):"
  print*, "-------------------------------------------------------------"
  m1 = .5
  m2 = 105.
  thresh1 = (m1-m2)**2
  thresh2 = m1**2+m2**2
  thresh3 = (m1+m2)**2
  print*, "x << 0:"
  x = -1000.
  call checkcplx("discb_cplx(x,m1,m2):", discb_cplx(x,m1,m2),cmplx(-65.3418956075475,0))
  call checkcplx("scalarc0ir6_cplx(x,m1,m2):", scalarc0ir6_cplx(x,m1,m2),cmplx(-0.0000318581951789787,0))
  print*, "x ~ 0:"
  x = 1.E-4
  call checkcplx("discb_cplx(x,m1,m2):", discb_cplx(x,m1,m2),cmplx(5.89505231145424e8,0))
  call checkcplx("scalarc0ir6_cplx(x,m1,m2):", scalarc0ir6_cplx(x,m1,m2),cmplx(3.57673788449575e-12,0))
  print*, "x ~ (m1-m2)^2:"
  x = thresh1 - 1.E-4
  call checkcplx("discb_cplx(x,m1,m2):", discb_cplx(x,m1,m2),cmplx(1.83146020995386e-8,0),1e-8)
  call checkcplx("scalarc0ir6_cplx(x,m1,m2):", scalarc0ir6_cplx(x,m1,m2),cmplx(0.0323645193534269,0))
  print*, "x ~ (m1+m2)^2:"
  x = thresh3 + 1.E-4
  call checkcplx("discb_cplx(x,m1,m2):", discb_cplx(x,m1,m2),cmplx(-1.79690510051e-8,0.0000409029564984832),1e-8)
  call checkcplx("scalarc0ir6_cplx(x,m1,m2):", scalarc0ir6_cplx(x,m1,m2),cmplx(-68.138119100012,371.610622872121),1e-8)
  print*, "x >> (m1+m2)^2:"
  x = 100000.
  call checkcplx("discb_cplx(x,m1,m2):", discb_cplx(x,m1,m2),cmplx(-6.61552998229413,2.79522226315099))
  call checkcplx("scalarc0ir6_cplx(x,m1,m2):", scalarc0ir6_cplx(x,m1,m2),cmplx(0.000071953483066019,-0.000258406859662600))
  print*, "(m1-m2)^2 < s < m1^2+m2^2 :"
  x = (thresh1+thresh2)/2
  call checkcplx("discb_cplx(x,m1,m2):", discb_cplx(x,m1,m2),cmplx(-0.00867826812989419,0))
  call checkcplx("scalarc0ir6_cplx(x,m1,m2):", scalarc0ir6_cplx(x,m1,m2),cmplx(0.0420077444380518,0))
  print*, "m1^2+m2^2 < s < (m1+m2)^2:"
  x = (thresh2+thresh3)/2
  call checkcplx("discb_cplx(x,m1,m2):", discb_cplx(x,m1,m2),cmplx(-0.0171920230411945,0))
  call checkcplx("scalarc0ir6_cplx(x,m1,m2):", scalarc0ir6_cplx(x,m1,m2),cmplx(0.105978278039367,0))

  if(onlyfast) goto 123

  print*, "----------------------------------------------"
  print*,    "stability check loop functions:"
  print*, "----------------------------------------------"
  xmin = -1000._prec; xmax = 1000._prec
  m1 = 0.01_prec; m2 = 10._prec
  m = m1;
  do i=1,100000
    x = xmin+i*(xmax-xmin)/100000
    v(1) = abs(discb_coll(x,m,m)-discb_cplx(x,m))/abs(discb_cplx(x,m))
    v(2) = abs(scalarc0ir6_coll(x,m,m)-scalarc0ir6_cplx(x,m))/abs(scalarc0ir6_cplx(x,m))
    v(3) = abs(scalarc0_coll(m**2,m**2,x,0.,m,0.)-scalarc0_cplx(x,m))/abs(scalarc0_cplx(x,m))
    v(4) = abs(real(scalard0ir16_coll(x,-10.,m))-scalard0ir16(x,-10.,m))/abs(scalard0ir16(x,-10.,m))
    v(5) = abs(real(scalard0ir16_coll(x,10.,m))-scalard0ir16(x,10.,m))/abs(scalard0ir16(x,10.,m))
    v(6) = abs(discb_coll(x,m1,m2)-discb_cplx(x,m1,m2))/abs(discb_cplx(x,m1,m2))
    v(7) = abs(scalarc0ir6_coll(x,m1,m2)-scalarc0ir6_cplx(x,m1,m2))/abs(scalarc0ir6_cplx(x,m1,m2))
    where (v > e) &
      e = v
  end do
  call check("discb(x,m) stability check:", e(1),threshold=1e-8)
  call check("scalarc0ir6(x,m) stability check:", e(2),threshold=1e-8)
  call check("scalarc0(x,m) stability check:", e(3),threshold=1e-8)
  call check("scalard0ir16(x,y,m) stability check, y>0:", e(4),threshold=1e-8)
  call check("scalard0ir16(x,y,m) stability check, y>0:", e(5),threshold=1e-8)
  call check("discb(x,m1,m2) stability check:", e(6),threshold=1e-8)
  call check("scalarc0ir6(x,m1,m2) stability check:", e(7),threshold=1e-8)

!  print*, "----------------------------------------------"
!  print*,             "performance test (scalarc0ir6):"
!  print*, "----------------------------------------------"
!  xmin = -1000._prec; xmax = 1000._prec
!  m1 = 0.01_prec; m2 = 10._prec
!  m = m1
!  call cpu_time(t1)
!  do i=1,100000
!    x = xmin+i*(xmax-xmin)/100000
!    res = discb_cplx(x,m) ! ->  100x faster
!    !res = scalarc0ir6_cplx(x,m) ! -> 20x faster
!    !res = scalarc0_cplx(x,m) ! -> 3x faster
!    !res = scalard0ir16(x,-10.,m) ! -> 13x faster
!    !res = discb_cplx(x,m1,m2) ! -> 65x faster
!    !res = scalarc0ir6_cplx(x,m1,m2) ! -> 10x faster
!  end do
!  call cpu_time(t2)
!  do i=1,100000
!    x = xmin+i*(xmax-xmin)/100000
!    res = discb_coll(x,m,m)
!    !res = scalarc0ir6_coll(x,m,m)
!    !res = scalarc0_coll(m**2,m**2,x,0.,m,0.)
!    !res = scalard0ir16_coll(x,-10.,m)
!    !res = discb_coll(x,m1,m2)
!    !res = scalarc0ir6_coll(x,m1,m2)
!  end do
!  call cpu_time(t3)
!  print*, "collier/in-house:", (t3-t2)/(t2-t1)

123 continue

  print*, "----------------------------------------------"
  print*,      "deltalph leptonic"
  print*, "----------------------------------------------"
  m = 1.
  print*, "z=x/4m^2<<0:"
  x = -1000.
  call checkcplx("deltalph_l_0_cplx:", deltalph_l_0_cplx(x,m), cmplx(0.5567286823568426,0._prec) ,1e-11)
  call checkcplx("deltalph_l_1_cplx:", deltalph_l_1_cplx(x,m), cmplx(0.27357675243403656,0._prec) ,1e-11)
  print*, "z=x/4m^2=-0.11: (before exp.)"
  x = -0.11
  call checkcplx("deltalph_l_0_cplx:", deltalph_l_0_cplx(x,m), cmplx(0.002307201519451925,0._prec) ,1e-10)
  call checkcplx("deltalph_l_1_cplx:", deltalph_l_1_cplx(x,m), cmplx(0.002770800846381304,0._prec) ,1e-10)
  print*, "z=x/4m^2=-0.09 (start exp.):"
  x = -0.09
  call checkcplx("deltalph_l_0_cplx:", deltalph_l_0_cplx(x,m), cmplx(0.0018916846643887914,0._prec) ,1e-10)
  call checkcplx("deltalph_l_1_cplx:", deltalph_l_1_cplx(x,m), cmplx(0.002274322479083728,0._prec),1e-10)
  print*, "z=x/4m^2~0:"
  x = -1.E-10
  call checkcplx("deltalph_l_0_cplx:", deltalph_l_0_cplx(x,m), cmplx(2.1220659078692015E-12,0._prec) ,1e-11)
  call checkcplx("deltalph_l_1_cplx:", deltalph_l_1_cplx(x,m), cmplx(2.5643015612763018E-12,0._prec),1e-11)
  print*, "z=x/4m^2~1:"
  x = 4.+1.E-10
  call checkcplx("deltalph_l_0_cplx:", deltalph_l_0_cplx(x,m), cmplx(-0.28294212104164806,-2.5000001033733783E-6) ,1e-11)
  call checkcplx("deltalph_l_1_cplx:", deltalph_l_1_cplx(x,m), cmplx(0.22668111624373138,-0.7853949803115434),1e-11)
  print*, "z=x/4m^2>>1:"
  x = 1000.
  call checkcplx("deltalph_l_0_cplx:", deltalph_l_0_cplx(x,m), cmplx(0.5554554322168466,-0.3333313306606506) ,1e-11)
  call checkcplx("deltalph_l_1_cplx:", deltalph_l_1_cplx(x,m), cmplx(0.2777752163456045,-0.08054641600969224) ,1e-11)
  print*, "z=x/4m^2>>>>1:"
  x = 100000.
  call checkcplx("deltalph_l_0_cplx:", deltalph_l_0_cplx(x,m), cmplx(1.0447141388455519,-0.3333333331333307) ,1e-11)
  call checkcplx("deltalph_l_1_cplx:", deltalph_l_1_cplx(x,m), cmplx(0.3923460563996019,-0.07958702312096247) ,1e-11)
  print*, "0<z=x/4m^2<1 (deltalph_l_0_cplx only):"
  x = .5; m = 1.
  call checkcplx("deltalph_l_0_cplx:", deltalph_l_0_cplx(x,m), cmplx(-0.011224800052467782,0._prec) ,1e-11)



  print*, "----------------------------------------------"
  print*,      "clenshaw"
  print*, "----------------------------------------------"
  a_cheb = (/1.88877557e-01,   4.64127930e-16,  -2.47975524e-01,  -4.14821613e-16, 1.57872017e-01,&
             3.52356831e-16,  -9.30280327e-02,  -1.83143107e-16,   4.30685337e-02, 8.34423280e-17 /)
  call check("clenshaw", clenshaw(a_cheb, -5., 5., -0.995995995996,10), 0.558903091114, 1E-9)


  call blockend(70)
  END SUBROUTINE


  SUBROUTINE TESTSPLIT
  use testtools
  implicit none
  real(kind=prec) :: y(5), weight, doub, sing

  call blockstart("massive splitting functions")

  call initflavour("daphne")
  musq = me**2

  y = (/0.1,1.-1e-6,0.8,0.3,0.6/)
  call psx3_fks(y,p1,me,p2,me,p3,me,p4,me,p5,weight)
  call check("split0_isr",split0_isr(p1,p5),3108.6836485780404)
  call check("split1_ac_isr fin",split1_ac_isr(p1,p5,doub,sing),1555.9950298291724)
  call check("split1_ac_isr sing",sing,247.38118459007177)
  call check("split1_ac_isr doub",doub,494.76236918014354)
  call check("split1_isr fin",split1_isr(p1,p5,doub,sing),1953.6190544065857)
  call check("split1_isr sing",sing,351.63802127988725)
  call check("split1_isr doub",doub,494.76236918014354)

  call psx3_35_fks(y,p1,me,p2,me,p3,p4,p5,weight,1)
  call check("split0_fsr",split0_fsr(p3,p5),2836.442365030206)
  call check("split1_ac_fsr fin",split1_ac_fsr(p3,p5,doub,sing),1419.7296094393057, threshold=1e-9)
  call check("split1_ac_fsr sing",sing,225.71691151909423, threshold=1e-9)
  call check("split1_ac_fsr doub",doub,451.43382318982344)
  call check("split1_fsr fin",split1_fsr(p3,p5,doub,sing),1110.9680935539618, threshold=1e-9)
  call check("split1_fsr sing",sing,130.59032255199344, threshold=1e-9)
  call check("split1_fsr doub",doub,451.43382318982344)

  call blockend(14)
  END SUBROUTINE



  SUBROUTINE TESTHYPERS
  use testtools
  implicit none
  call blockstart("Hyperspherical form factors")
  call check("F1(0.2, -20)", f1_kernel(0.2, -20.), -10.489767244290432)
  call check("F2(0.2, -20)", f2_kernel(0.2, -20.), 0.15753638485424032)
  call check("G1(0.2, -20)", g1_kernel(0.2, -20.), -10.513557406404046)
  call check("G2(0.2, -20)", g2_kernel(0.2, -20.), 0.19687171969668002)
  call check("F1(1-2e-7, -20)", f1_kernel(1-9e-7, -20.),-3.3333348332442355106)
  call check("F2(1-2e-7, -20)", f2_kernel(1-9e-7, -20.),3.8999190015357316976e-6)
  call check("G1(1-2e-7, -20)", g1_kernel(1-9e-7, -20.),-4.333318933503431156)
  call check("G2(1-2e-7, -20)", g2_kernel(1-9e-7, -20.),1.3333180335504100014)
  call blockend(8)
  END SUBROUTINE


  SUBROUTINE TESTNTSsoft
  use testtools
  implicit none
  real(kind=prec) :: weight, arr(8)
  integer ido
  real(kind=prec) :: fin, sin
  real(kind=prec) :: nt, nte, ntm, ntx, ntx1, ntx2, ntx3

  call blockstart("next-to-soft soft function")

  call initflavour('prad')
  musq = me**2
  ran_seed = 23312
  do ido = 1,8
    arr(ido) = ran2(ran_seed)
  enddo
  call psx4_fkss(arr,p1,me,p2,me,p3,me,p4,me,p5,p6,weight)

  fin = softfunc(part(p1,+1,+1),part(p2,+1,+1), p6, sin)
  call check("S12 ep^-1", sin, +0.042615417530362996142)
  call check("S12 ep^ 0", fin, -0.547210785290053271148)

  fin = softfunc(part(p2,+1,+1),part(p4,+1,-1), p6, sin)
  call check("S24 ep^-1", sin, +1.223039925770344161196)
  call check("S24 ep^ 0", fin, -8.607687877619865135426)

  fin = ntssoft(parts((/part(p1,+1,+1), part(p2,+1,+1), part(p3,+1,-1), part(p4,+1,-1)/)), p6, sin)
  call check("NTS soft Moller",fin, -2.8629571041563155550556)

  fin = ntssoft(parts((/part(p1,+1,+1), part(p2,-1,+1), part(p3,+1,-1), part(p4,-1,-1)/)), p6, sin)
  call check("NTS soft Bhabha",fin, -3.9467404861470856991230)

  call initflavour('muone')
  call psx3_fks(arr,p1,me,p2,mm,p3,me,p4,mm,p5,weight)

  fin = softfunc(part(p1,+1,+1),part(p2,+1,+1), p5, sin)
  call check("S12 ep^-1", sin, +0.17578465928209425)
  call check("S12 ep^ 0", fin, -3.5353285281716635)

  fin = softfunc(part(p2,+1,+1),part(p3,+1,-1), p5, sin)
  call check("S24 ep^-1", sin, +0.15804928766215035)
  call check("S24 ep^ 0", fin, -1.4286726855561291)

  fin = ntssoft(parts((/part(p1,+1,+1), part(p2,+1,+1), part(p3,+1,-1), part(p4,+1,-1)/)), p5, sin)
  call check("NTS soft Muone",fin, 0.010396312432997712)

  nt  = ntssoft(parts((/part(p1,+1,+1), part(p2,+1,+1), part(p3,+1,-1), part(p4,+1,-1)/)), p5, sin)
  nte = ntssoft(parts((/part(p1,+1,+1), part(p3,+1,-1)/)), p5, sin)
  ntm = ntssoft(parts((/part(p2,+1,+1), part(p4,+1,-1)/)), p5, sin)
  ntx = ntssoft(parts((/part(p1,+1,+1,1), part(p2,+1,+1,2), &
                        part(p3,+1,-1,1), part(p4,+1,-1,2)/),"x"), p5, sin)

  ntx1 = ntssoftx(parts((/part(p1,+1,+1,1), part(p2,+1,+1,2), &
                        part(p3,+1,-1,1), part(p4,+1,-1,2)/),"1"), p5, sin)
  ntx2 = ntssoftx(parts((/part(p1,+1,+1,1), part(p2,+1,+1,2), &
                        part(p3,+1,-1,1), part(p4,+1,-1,2)/),"2"), p5, sin)
  ntx3 = ntssoftx(parts((/part(p1,+1,+1,1), part(p2,+1,+1,2), &
                        part(p3,+1,-1,1), part(p4,+1,-1,2)/),"3"), p5, sin)

  call check("NTS soft Muone :: (E+M+X)/TOT", nt, nte+ntm+ntx)
  call check("NTS soft Muone MIXD :: (31+22+13)/MIXD", ntx, ntx1+ntx2+ntx3)

  call initflavour('muse210')
  musq = me**2
  ran_seed = 3312
  do ido = 1,8
    arr(ido) = ran2(ran_seed)
  enddo
  call psx3_fks(arr,p1,me,p2,mm,p3,me,p4,mm,p5,weight)

  fin = ntssoft(parts((/part(p1,+1,+1), part(p3,+1,-1)/)), p5, sin)
  call check("NTS soft MUSE",fin, 0.001802333443639812)

  call blockend(14)
  END SUBROUTINE TESTNTSsoft


#ifdef HAVE_MUDEC
#ifdef HAVE_MUDECRARE

  SUBROUTINE TESTMUDECDIXON
  use testtools
  use mudec, only: pm2enn, m2enn, m2ennl, pm2enng, m2enngav, pm2enngg
  use mudecrare, only: pm2ennee, pt2mnnee, pm2enneeg
  implicit none
  integer ranseed, i, j
  real(kind=prec) arr(14), matrix(4,4,3), weight, ans(3)

  call initflavour("mu-e")
  musq = Mm**2
  call blockstart("mudec Dixon averaging")

  pol1 = (/ 0., 0., 0.8, 0. /)
  pol2 = (/ 0., 0., 0.0, 0. /)

  ranseed = 23313
  do i=1,5
    arr(i) = ran2(ranseed)
  enddo

  do i=1,4
    do j=1,4
      arr(4:5) = (/real(j)/4., real(i)/4. /)
      call psd4(arr(:5),p1,Mm,p2,Me,p3,0._prec,p4,0._prec,weight)
      matrix(i,j,1) = m2enn(p1, p2, p3, p4)
      matrix(i,j,2) = pm2enn(p1, pol1, p2, p3, p4)
      matrix(i,j,3) = m2ennl(p1, p2, p3, p4)
    enddo
  enddo
  ans =   (5*matrix(2,2,:))/6. + (5*matrix(2,4,:))/6. + (2*matrix(3,1,:))/9. &
        - (8*matrix(3,2,:))/9. + (2*matrix(3,3,:))/9. - (8*matrix(3,4,:))/9. &
        + (2*matrix(4,4,:))/3.

  call check(" M2ENN" , ans(1), pm2ennav (p1, pol2, p2, p3, p4))
  call check("PM2ENN" , ans(2), pm2ennav (p1, pol1, p2, p3, p4))
  call check(" M2ENNL", ans(3), pm2ennlav(p1, pol2, p2, p3, p4))


  ranseed = 23313
  do i=1,8
    arr(i) = ran2(ranseed)
  enddo

  do i=1,4
    do j=1,4
      arr(7:8) = (/real(j)/4., real(i)/4. /)
      call psd5_fks(arr(:8),p1,Mm,p2,Me,p3,0._prec,p4,0._prec,p5,weight)
      matrix(i,j,1) = pm2enng(p1, pol2, p2, p3, p4, p5)
      matrix(i,j,2) = pm2enng(p1, pol1, p2, p3, p4, p5)
    enddo
  enddo
  ans =   (5*matrix(2,2,:))/6. + (5*matrix(2,4,:))/6. + (2*matrix(3,1,:))/9. &
        - (8*matrix(3,2,:))/9. + (2*matrix(3,3,:))/9. - (8*matrix(3,4,:))/9. &
        + (2*matrix(4,4,:))/3.

  call check(" M2ENNG", ans(1),  m2enngav(p1,       p2, p3,p4,p5))
  call check("PM2ENNG", ans(2), pm2enngav(p1, pol1, p2, p3,p4,p5))

  ranseed = 2105103485
  do i=1,11
    arr(i) = ran2(ranseed)
  enddo

  do i=1,4
    do j=1,4
      arr(10:11) = (/real(j)/4., real(i)/4. /)
      call psd6_fkss(arr(:11),p1,Mm,p2,Me,p3,0._prec,p4,0._prec,p5,p6,weight)
      matrix(i,j,1) = pm2enngg(p1, pol2, p2, p3, p4, p5,p6)
      matrix(i,j,2) = pm2enngg(p1, pol1, p2, p3, p4, p5,p6)
    enddo
  enddo
  ans =   (5*matrix(2,2,:))/6. + (5*matrix(2,4,:))/6. + (2*matrix(3,1,:))/9. &
        - (8*matrix(3,2,:))/9. + (2*matrix(3,3,:))/9. - (8*matrix(3,4,:))/9. &
        + (2*matrix(4,4,:))/3.

  call check(" M2ENNGG", ans(1), pm2ennggav(p1, pol2, p2, p3,p4,p5, p6))
  call check("PM2ENNGG", ans(2), pm2ennggav(p1, pol1, p2, p3,p4,p5, p6))

  weight = 0.
  ranseed = 142169178
  do i=1,11
    arr(i) = ran2(ranseed)
  enddo

  do i=1,4
    do j=1,4
      arr(10:11) = (/real(j)/4., real(i)/4. /)
      call psd6_23_24_34_e56(arr(:11),p1,Mm,p2,Me,p3,0._prec,p4,0._prec,p5,Me,p6,Me,weight)
      matrix(i,j,1) = pm2ennee(p1, pol2, p2, p3, p4, p6,p5)
      matrix(i,j,2) = pm2ennee(p1, pol1, p2, p3, p4, p6,p5)
    enddo
  enddo
  ans =   (5*matrix(2,2,:))/6. + (5*matrix(2,4,:))/6. + (2*matrix(3,1,:))/9. &
        - (8*matrix(3,2,:))/9. + (2*matrix(3,3,:))/9. - (8*matrix(3,4,:))/9. &
        + (2*matrix(4,4,:))/3.

  call check(" M2ENNEE", ans(1), pm2enneeav(p1, pol2, p2, p3,p4,p5, p6))
  call check("PM2ENNEE", ans(2), pm2enneeav(p1, pol1, p2, p3,p4,p5, p6))


  weight = 0.
  ranseed = 142169178
  do i=1,11
    arr(i) = ran2(ranseed)
  enddo

  do i=1,4
    do j=1,4
      arr(10:11) = (/real(j)/4., real(i)/4. /)
      call psd6_23_24_34_e56(arr(:11),p1,Mtau,p2,Mmu,p3,0._prec,p4,0._prec,p5,Mel,p6,Mel,weight)
      matrix(i,j,1) = pt2mnnee(p1, pol2, p2, p3, p4, p6,p5)
      matrix(i,j,2) = pt2mnnee(p1, pol1, p2, p3, p4, p6,p5)
    enddo
  enddo
  ans =   (5*matrix(2,2,:))/6. + (5*matrix(2,4,:))/6. + (2*matrix(3,1,:))/9. &
        - (8*matrix(3,2,:))/9. + (2*matrix(3,3,:))/9. - (8*matrix(3,4,:))/9. &
        + (2*matrix(4,4,:))/3.

  call check(" T2MNNEE", ans(1), pt2mnneeav(p1, pol2, p2, p3,p4,p5, p6))
  call check("PT2MNNEE", ans(2), pt2mnneeav(p1, pol1, p2, p3,p4,p5, p6))


  weight = 0.
  ranseed = 1599259816
  do i=1,14
    arr(i) = ran2(ranseed)
  enddo

  do i=1,4
    do j=1,4
      arr(13:14) = (/real(j)/4., real(i)/4. /)
      call psd7_27_37_47_2x5_fks(arr,p1,Mm,p2,Me,p3,0._prec,p4,0._prec,p5,Me,p6,Me,p7,weight)
      matrix(i,j,1) = pm2enneeg(p1, pol2, p2, p3, p4, p6,p5,p7)
      matrix(i,j,2) = pm2enneeg(p1, pol1, p2, p3, p4, p6,p5,p7)
    !! mu+(p1) -> e+(p2) \nu_e(p3) \bar{\nu}_\mu(p4) e+(p5) e-(p6) g(p7)
    !!M1+(p1)->  M2+(p2)                          ,M2-(p5),M2+(p6),2\nu,\gamma(p7)
    enddo
  enddo
  ans =   (5*matrix(2,2,:))/6. + (5*matrix(2,4,:))/6. + (2*matrix(3,1,:))/9. &
        - (8*matrix(3,2,:))/9. + (2*matrix(3,3,:))/9. - (8*matrix(3,4,:))/9. &
        + (2*matrix(4,4,:))/3.

  call check(" M2ENNEEG", ans(1), pm2enneegav(p1, pol2, p2, p3,p4,p5, p6, p7), 1e-5)
  call check("PM2ENNEEG", ans(2), pm2enneegav(p1, pol1, p2, p3,p4,p5, p6, p7), 1e-5)

  call blockend(13)
  END SUBROUTINE





  SUBROUTINE TESTMUDECPOL
  use testtools
  use mudec
  use mudecrare
  implicit none
  real(kind=prec) :: xx(14), weight
  real(kind=prec) :: sing, ctsing, fin, ctfin, polN(4), polO(4)
  real(kind=prec) :: z, mat0, lim
  integer i, ranseed, ido

  polN = (/ 0., 0., 0.85, 0. /)
  polO = (/ 0., 0., 0.85, 0. /)


  call initflavour("mu-e")
  call blockstart("mudec polarisation")
  musq = Mm**2
  ! Step 0: Assume that pm2ennav is correct
  ! Step 1: pole

  ranseed = 23313
  weight = 0.
  do while(weight < zero)
    do i=1,5
      xx(i) = ran2(ranseed)
    enddo
    call psd4(xx(1:5),p1,Mm,p2,Me,p3,0._prec,p4,0._prec,weight)
  enddo
  ctfin = Ieik(1.0,Mm,p1,p2, ctsing)
  fin = pm2ennlav(p1,polN,p2,p3,p4, sing)
  call check("pm2enn   -> pm2ennl  ", (alpha/2./pi) * ctsing * pm2ennav(p1, polO, p2, p3, p4) / sing, -1.)

  ! Step 2: construct soft limit
  weight = 0.
  do while(weight < zero)
    xx(1) = 1.e-8
    do i=2,8
      xx(i) = ran2(ranseed)
    enddo
    call psd5_fks(xx(1:8),p1,Mm,p2,Me,p3,0._prec,p4,0._prec,p5,weight)
  enddo
  call check("pm2enn   -> pm2enng  ",pm2enngav (p1, polN, p2, p3, p4, p5)*xiout**2 / pm2enngav_s (p1, polO, p2, p3, p4), 1., 1e-6)
  call check("pm2ennl  -> pm2enngl ",pm2ennglav(p1, polN, p2, p3, p4, p5)*xiout**2 / pm2ennglav_s(p1, polO, p2, p3, p4), 1., 1e-6)


  ! Step 3: construct double-soft limit
  weight = 0.
  do while(weight < zero)
    xx(1) = 1.e-8
    xx(2) = 2.e-8
    do i=3,11
      xx(i) = ran2(ranseed)
    enddo
    call psd6_fkss(xx(1:11),p1,Mm,p2,Me,p3,0._prec,p4,0._prec,p5,p6,weight)
  enddo
  call check("pm2enn   -> pm2enngg ", pm2ennggav   (p1, polN, p2, p3, p4, p5, p6) * xioutA**2 * xioutB**2 /&
                                     pm2ennggav_ss(p1, polO, p2, p3, p4), 1., 1e-6)

  ! Step 4: pm2enng -> tm2mnnee
  weight = 0.
  do while(weight < zero)
    do i=1,11
      xx(i) = ran2(ranseed)
    enddo
    xx(2) = 0.7
    ! Test this at phi=pi/4. This doesn't matter as the difference
    ! integrates to zero (cf. (B.28) of [hep-ph/9512328])
    xx(4) = 1./8.

    ! This only works for p6||n = (0,0,1,0).
    xx(7) = 0.
    call psd6_omega_m50_fks(xx(1:11),p1,Mm,p5,0.,p3,0._prec,p4,0._prec,p2,p6,weight)
  enddo
  call check("n||p6", cos_th(p6, polN), 1.)
  do ido=25,25
    xx(2) = 1-0.5**ido
    call psd6_omega_m50_fks(xx(1:11),p1,Mm,p5,0.,p3,0._prec,p4,0._prec,p2,p6,weight)

    z = p5(4)/(p6(4)+p5(4))
    lim = (4.*pi*alpha)* 1._prec/p6(4)/p5(4)* (1-2.*z+2*z**2)
    lim = lim * pm2enngav(p1,polO,p2,p3,p4,p5+p6)
    call check("pm2enng  -> pt2mnnee ", (1-yout) * pt2mnneeav(p1,polN, p2, p3, p4, p5, p6) / lim, 1._prec, 1.e-4)
    call check("pm2enng  -> pm2ennee ", (1-yout) * pm2enneeav(p1,polN, p2, p3, p4, p5, p6) / lim, 1._prec, 1.e-3)
  enddo


  ! Step 5a: pt2mnnee -> pt2mnneel
  weight = 0.
  do while(weight < zero)
    do i=1,5
      xx(i) = ran2(ranseed)
    enddo
    call psd6_23_24_34_e56(xx(1:11),p1,Mtau,p2,Mmu,p3,0._prec,p4,0._prec,p5, Mel, p6, Mel, weight)
  enddo

  ctsing = - Isin(xieik1,Mtau,p1) - Isin(xieik1,Mtau,p2) - &
           Isin(xieik1,Mtau,p6) - Isin(xieik1,Mtau,p5) + &
           2*Isin(xieik1,Mtau,p1,p2) + 2*Isin(xieik1,Mtau,p1,p6) - &
           2*Isin(xieik1,Mtau,p1,p5) - 2*Isin(xieik1,Mtau,p2,p6) + &
           2*Isin(xieik1,Mtau,p2,p5) + 2*Isin(xieik1,Mtau,p6,p5)
  fin = pt2mnneelav(p1,polN,p2,p3,p4,p5,p6, sing)
  call check("pt2mnnee -> pt2mnneel", (alpha/2./pi) * ctsing * pt2mnneeav(p1, polO, p2, p3, p4, p5, p6) / sing, -1.)

  ! Step 5b: pm2ennee -> pm2enneel
  weight = 0.
  do while(weight < zero)
    do i=1,5
      xx(i) = ran2(ranseed)
    enddo
    call psd6_23_24_34_e56(xx(1:11),p1,Mm,p2,Me,p3,0._prec,p4,0._prec,p5, Me, p6, Me, weight)
  enddo

  ctsing = - Isin(xieik1,Mm,p1) - Isin(xieik1,Mm,p2) - &
           Isin(xieik1,Mm,p6) - Isin(xieik1,Mm,p5) + &
           2*Isin(xieik1,Mm,p1,p2) + 2*Isin(xieik1,Mm,p1,p6) - &
           2*Isin(xieik1,Mm,p1,p5) - 2*Isin(xieik1,Mm,p2,p6) + &
           2*Isin(xieik1,Mm,p2,p5) + 2*Isin(xieik1,Mm,p6,p5)
  fin = pm2enneelav(p1,polN,p2,p3,p4,p5,p6, sing)
  call check("pm2ennee -> pm2enneel", (alpha/2./pi) * ctsing * pm2enneeav(p1, polO, p2, p3, p4, p5, p6) / sing, -1.)

  ! Step 6a: pm2ennee -> pm2enneeg
  weight = 0.
  do while(weight < zero)
    xx(1) = 0.8e-9_prec
    do i=2,14
      xx(i) = ran2(ranseed)
    enddo
    call psd7_27_37_47_e56_fks(xx,p1,Mm,p2,Me,p3,Me,p4,Me,p6,0._prec,p5,0._prec,p7,weight)
  enddo

  call check('pm2ennee -> pm2enneeg', xiout**2*pm2enneegav  (p1, polN, p2, p5, p6, p3, p4, p7) / &
                                               pm2enneegav_s(p1, polO, p2, p5, p6, p3, p4), 1.,1e-7)

  ! Step 6b: pt2mnnee -> pt2mnneeg
  weight = 0.
  do while(weight < zero)
    xx(1) = 0.8e-9_prec
    do i=2,14
      xx(i) = ran2(ranseed)
    enddo
    call psd7_27_37_47_e56_fks(xx,p1,Mtau,p2,Mmu,p3,Mel,p4,Mel,p6,0._prec,p5,0._prec,p7,weight)
  enddo

  call check('pt2mnnee -> pt2mnneeg', xiout**2*pt2mnneegav  (p1, polN, p2, p5, p6, p3, p4, p7) / &
                                               pt2mnneegav_s(p1, polO, p2, p5, p6, p3, p4), 1.,1e-6)

  call blockend(11)
  END SUBROUTINE

#endif
#endif

  SUBROUTINE TESTVP
  use testtools
  implicit none
  real(kind=prec) :: q2, e
  complex (kind=prec) :: cerror, cerrorsta, cerrorsys, der
  complex (kind=prec) :: cggvapx, cegvapx
  external cggvapx, cegvapx
  character*3 LEPTONflag
  integer iLEP,iwarnings
  common /lepton/iLEP,iwarnings,LEPTONflag
  real(kind=prec) :: SW, CW
  real(kind=prec) :: PiP(2:3)

  call blockstart("VP")
  SW = sqrt(sw2)
  CW = sqrt(1-SW2)

  q2 = 10.e3
  nel=1
  nmu=0
  ntau=0
  nhad = 0

  Pip = deltalph2p_0_tot(q2)

  call check("Pi' ~ Pi(MZ**2)", PiP(2),  real( &
    (-deltalph2_0_tot_cplx(MZ**2)+ SW**2*deltalph_0_tot_cplx(MZ**2)) / SW/CW,&
    kind=prec))

  call check("Pi3g / Pigg ~ 2", &
    real(deltalph2_h_cplx(q2) / deltalph_h_cplx(q2), kind=prec), &
    2., threshold=3e-2)

  LEPTONflag = 'lep'
  nel=1
  nmu=1
  ntau=1
  nhad = 0
  call checkcplx("cggvapx(lep) = deltalph(lep)", &
    alpha*137.035999_prec*cggvapx(1e-6*q2, cerror, cerrorsta, cerrorsys), &
    deltalph_0_tot_cplx(q2), threshold=1e-2)
  LEPTONflag = 'had'
  nel=0
  nmu=0
  ntau=0
  nhad = 1
  call checkcplx("cggvapx(had) = deltalph(had)", &
    alpha*137.035999_prec*cggvapx(1e-6*q2, cerror, cerrorsta, cerrorsys), &
    deltalph_0_tot_cplx(q2), threshold=1e-10)
  call checkcplx("cegvapx(had) = deltalph2(had)", &
    alpha*137.035999_prec*cegvapx(1e-6*q2, cerror, cerrorsta, cerrorsys), &
    deltalph2_0_tot_cplx(q2), threshold=1e-2)

  print*, deltalph_0_tot_cplx(MZ**2)
  stop

  print*,  (-deltalph2_0_tot_cplx(MZ**2)+ SW**2*deltalph_0_tot_cplx(MZ**2)) / SW/CW

  Nel = 1
  Nmu = 1
  Ntau = 1
  Nhad = 0
  LEPTONflag = 'lep'
  call check("Pi' ~ Pi(MZ**2)", PiP(2),  real(( &
    -alpha*137.035999_prec*cegvapx(1e-6*MZ**2, cerror, cerrorsta, cerrorsys)&
     +SW**2*alpha*137.035999_prec*cggvapx(1e-6*MZ**2, cerror, cerrorsta, cerrorsys)&
    ) / SW/CW, kind=prec))

  stop

  !(-1.3759064151724603,0.46853317962689062)  -2.470999916574323
  !(-1.3759064151724603,0.46853317962689062)  -2.282254506471652
  !(-1.3759064151724603,0.46853317962689062)  -2.659745326676995
  !(-1.3759064151724603,0.46853317962689062)  -2.597136843012765

  Nel = 1
  Nhad = 0
  PiP = deltalph2p_0_tot(q2)
  print*, (-deltalph2_0_tot_cplx(MZ**2)+ SW**2*deltalph_0_tot_cplx(MZ**2)) / SW/CW
  print*,CW*SW*PiP(2)

  Nel = 1
  Nhad = 0

  PiP = deltalph2p_0_tot(q2)
  print*, (-deltalph2_0_tot_cplx(MZ**2)+ SW**2*deltalph_0_tot_cplx(MZ**2)) / SW/CW
  print*, PiP(2)

  ! stop

  print*, deltalph2_0_tot_cplx(q2) / deltalph_0_tot_cplx(q2)

  e = sign(0.001*sqrt(abs(q2)),q2)
  LEPTONflag = 'lep'
  print*, alpha*137.035999_prec*cggvapx(1e-6*q2, cerror, cerrorsta, cerrorsys)

  ! stop

  END SUBROUTINE TESTVP

#ifdef HAVE_EE
#ifdef HAVE_MUE

  SUBROUTINE TESTOPENLOOPS
  use openloops
  use ol_parameters_init_dp
  use olinterface
  use testtools
  implicit none
  real (kind=prec) :: x(2),y(5),y8(8)
  integer ido
  real (kind=prec) :: q1(4),q2(4),q3(4),q4(4), vecs(0:3,4)
  real (kind=prec) :: n1(4), n2(4)
  real (kind=prec) :: weight
  real (kind=prec) :: olTree, olSingle, olFinite
  real (kind=prec) ::   Tree,   Single,   Finite

  call initflavour("muone")

  musq = Mm**2

  call blockstart("OpenLoops")

  x = (/0.75,0.5/)
  y = (/0.3,0.6,0.8,0.4,0.9/)
  ran_seed = 23312
  do ido = 1,8
    y8(ido) = ran2(ran_seed)
  enddo


  print*, "--------------------"
  print*, "    e mu -> e mu    "
  print*, "--------------------"


  call psx2(x,p1,me,p2,mm,q1,me,q2,mm,weight)

  call openloops("em2emEE", p1, p2, q1, q2, tree=olTree, sin=olSingle, fin=olFinite)
  tree = em2em(p1, p2, q1, q2)
  finite = em2eml_ee(p1,p2,q1,q2, single)

  call check("em2em0", olTree, tree)
  call check("em2emlee ep^-1", olSingle, single)
  call check("em2emlee ep^ 0", olFinite, finite)


  call psx3_fks(y,p1,me,p2,mm,q1,me,q2,mm,q3,weight)

  call openloops("em2emgEE", p1, p2, q1, q2, q3, tree=olTree)
  tree = em2emg_ee(p1, p2, q1, q2, q3)
  call check("em2emg0", olTree, tree)

  call openloops("em2emgEE", p1, p2, q1, q2, q3, fin=olFinite)
  finite = em2emgl_eeee(p1, p2, q1, q2, q3)
  call check("em2emgl", olfinite, finite)


  call psx4_fkss(y8,p1,me,p2,mm,q1,me,q2,mm,q3,q4,weight)

  call openloops("em2emggEE", p1, p2, q1, q2, q3, q4, tree=olTree)
  tree = em2emgg_eeee(p1, p2, q1, q2, q3, q4)
  call check("em2emgg", olTree, tree)



  print*, "--------------------"
  print*, "    e e -> mu mu    "
  print*, "--------------------"

  call initflavour("mu-eOld", mys=100000._prec)

  call psx2(x,p1,me,p2,me,q1,mm,q2,mm,weight)

  n1 = boost_back(p1, (/0.,0.,+1.,0./))
  n2 = boost_back(p2, (/0.,0.,+1.,0./))

  call openloops("ee2mm", p1, p2, q1, q2, tree=olTree, fin=olFinite)
  tree = ee2mm(p1, p2, q2, q1)
  finite = ee2mml(p1,p2,q2,q1)

  call check("ee2mm0", olTree, tree)
  call check("ee2mml", olFinite, finite)

  call openloops("ee2mm",p1,p2,q1,q2, tree=olTree,fin=olFinite, sin=olSingle, &
       hel1=int(cos_th(n1,p1)),hel2=int(cos_th(-n2,p2)))


  call psx3_fks(y,p1,me,p2,me,q1,mm,q2,mm,q3,weight)

  call openloops("ee2mmg", p1, p2, q1, q2, q3, tree=olTree)
  tree = ee2mmg(p1, p2, q1, q2, q3)

  call check("ee2mmg", olTree, tree)

  call openloops("ee2mmg",p1,p2,q1,q2,q3, tree=olTree, &
       hel1=int(cos_th(n1,p1)),hel2=int(cos_th(-n2,p2)))

  call openloops("ee2mmgEE", p1,p2,q1,q2,q3,tree=olTree, fin=olFinite, sin=olSingle, &
     hel1=int(cos_th(n1,p1)),hel2=int(cos_th(-n2,p2)))

  print*, "--------------------"
  print*, "     e e -> e e     "
  print*, "--------------------"
  musq = me**2

  call psx2(x,p1,me,p2,me,q1,me,q2,me,weight)
  call openloops("ee2ee", p1, p2, q1, q2, tree=olTree, sin=olSingle, fin=olFinite)
  tree = ee2ee(p1, p2, q1, q2)
  finite = ee2eel(p1,p2,q1,q2, single)

  call check("ee2ee0", olTree, tree)
  call check("ee2eel ep^-1", olSingle, single)
  call check("ee2eel ep^ 0", olFinite, finite)


  call openloops("eb2eb", p1, p2, q1, q2, tree=olTree, sin=olSingle, fin=olFinite)
  tree = eb2eb(p1, p2, q1, q2)
  finite = eb2ebl(p1,p2,q1,q2, single)

  call check("eb2eb0", olTree, tree)
  call check("eb2ebl ep^-1", olSingle, single)
  call check("eb2ebl ep^ 0", olFinite, finite)

  call psx3_fks(y,p1,me,p2,me,q1,me,q2,me,q3,weight)
  call openloops("ee2eeg", p1, p2, q1, q2, q3, tree=olTree, fin=olFinite)

  tree = ee2eeg(p1, p2, q1, q2, q3)
  finite = ee2eegl(p1,p2,q1,q2, q3)
  call check("ee2eeg0", olTree, tree)
  call check("ee2eegl", olFinite, finite, 1e-8)

  call openloops("eb2ebg", p1, p2, q1, q2, q3, tree=olTree, fin=olFinite)
  tree = eb2ebg(p1, p2, q1, q2, q3)
  call check("eb2ebg0", olTree, tree)

  ran_seed = 23312
  do ido = 1,8
    y8(ido) = ran2(ran_seed)
  enddo
  call psx4_fkss(y8,p1,me,p2,me,q1,me,q2,me,q3,q4,weight)

  call openloops("ee2eegg", p1, p2, q1, q2, q3, q4, tree=olTree)
  tree = ee2eegg(p1, p2, q1, q2, q3, q4)
  call check("ee2eegg", olTree, tree)

  END SUBROUTINE

  SUBROUTINE TESTOPENLOOPSREF
  use testtools
  use olinterface
  implicit none
  real (kind=prec) :: arr2(2),arr5(5),arr8(8)
  real (kind=prec) :: tr, si, fi
  real (kind=prec) :: weight
  integer ido, seed

  seed = 1234
  do ido = 1,2
    arr2(ido) = ran2(seed)
  enddo
  do ido = 1,5
    arr5(ido) = ran2(seed)
  enddo
  seed = 1264
  do ido = 1,8
    arr8(ido) = ran2(seed)
  enddo

  call initflavour("muone")
  musq = Mm**2
  call blockstart("OpenLoops Reference")

  call checkol("em2emEE"  , arr2, (/me,mm,me,mm/),(/811.75809549839448   , 3056.8777822928969   , 16209.048252088454   /))
  call checkol("ee2mm"    , arr2, (/me,me,mm,mm/),(/216.09484045057556   , 1046.2883998962695   , 4838.5180435306211   /))
  call checkol("ee2mm"    , arr2, (/me,me,mm,mm/),(/108.04696738055966   , 523.14200736368889   , 2419.2501832522503   /), hel1=+1, hel2=+1)
  call checkol("ee2mm"    , arr2, (/me,me,mm,mm/),(/4.5284472812414923E-4, 2.1925844458042226E-3, 8.8385130589165618E-3/), hel1=+1, hel2=-1)
  call checkol("ee2mm"    , arr2, (/me,me,mm,mm/),(/4.5284472812414923E-4, 2.1925844458042226E-3, 8.8385130589165549E-3/), hel1=-1, hel2=+1)
  call checkol("ee2mm"    , arr2, (/me,me,mm,mm/),(/108.04696738055966   , 523.14200736368889   , 2419.2501832522530   /), hel1=-1, hel2=-1)
  call checkol("ee2ee"    , arr2, (/me,me,me,me/),(/8249.1259753627601   , 56843.768795068638   , 328751.32322691637   /))
  call checkol("eb2eb"    , arr2, (/me,me,me,me/),(/832.24586061485388   , 6950.1772916216214   , 33445.317504323037   /))
  call checkol("em2emgEE" , arr5, (/me,mm,me,mm/),(/27.918099851867492   , 107.61803669711543   , 575.91614117522477   /))
  call checkol("ee2mmg"   , arr5, (/me,me,mm,mm/),(/22.352575946100099   , 116.65928570273900   , 518.65601203400274   /))
  call checkol("ee2mmg"   , arr5, (/me,me,mm,mm/),(/11.176148399155142   , 58.328914407769879   , 259.31407349574539   /), hel1=+1, hel2=+1)
  call checkol("ee2mmg"   , arr5, (/me,me,mm,mm/),(/1.3957389490860801E-4, 7.2844359961242975E-4, 8.3343322985303522E-4/), hel1=+1, hel2=-1)
  call checkol("ee2mmg"   , arr5, (/me,me,mm,mm/),(/1.3957389490860803E-4, 7.2844359961242986E-4, 8.5911952191361669E-4/), hel1=-1, hel2=+1)
  call checkol("ee2mmg"   , arr5, (/me,me,mm,mm/),(/11.176148399155144   , 58.328914407769894   , 259.34024598550610   /), hel1=-1, hel2=-1)
  call checkol("ee2mmgEE" , arr5, (/me,me,mm,mm/),(/24.050321381169024   , 94.569498320322992   , 529.40953232671450   /))
  call checkol("ee2mmgEE" , arr5, (/me,me,mm,mm/),(/12.025017305908703   , 47.284185349946938   , 264.70058644437137   /), hel1=+1, hel2=+1)
  call checkol("ee2mmgEE" , arr5, (/me,me,mm,mm/),(/1.4338467580708332E-4, 5.6381021454936231E-4, 1.4109835649270225E-3/), hel1=+1, hel2=-1)
  call checkol("ee2mmgEE" , arr5, (/me,me,mm,mm/),(/1.4338467580708332E-4, 5.6381021454936231E-4, 1.4101481320933475E-3/), hel1=-1, hel2=+1)
  call checkol("ee2mmgEE" , arr5, (/me,me,mm,mm/),(/12.025017305908703   , 47.284185349946938   , 264.70612475064570   /), hel1=-1, hel2=-1)
  call checkol("ee2eeg"   , arr5, (/me,me,me,me/),(/118668.58457611562   , 688031.83816686179   , 4561070.8830678007   /))
  call checkol("eb2ebg"   , arr5, (/me,me,me,me/),(/27.220914732111545   , 259.05330285007182   , 921.18277189460878   /))
  call checkol("em2emggEE", arr8, (/me,mm,me,mm/),(/173.79056357734868   , 666.43102436380946   , 3493.8496936632355   /))
  call checkol("ee2eegg"  , arr8, (/me,me,me,me/),(/42876.146502967531   , 262644.19113137783   , 1676793.4266930725   /))

  ! A few manual tests

  call psx3_fks(arr5, p1, me, p2, mm, p3, me, p4, mm, p5, weight)
  call check("em2emgEE", em2emgl_eeee(p1, p2, p3, p4, p5), 575.91614117522477)
  call check("em2emgMM", em2emgl_mmmm(p1, p2, p3, p4, p5),-8.2048654875130911E-002)
  call check("em2emgEM", em2emgl_mixd(p1, p2, p3, p4, p5), 3.4831468290340432E+001)

  call blockend(3*23+3)

  END SUBROUTINE TESTOPENLOOPSREF

#endif
#endif

  SUBROUTINE CHECKOL(process, arr, masses, ref, hel1, hel2)
  use olinterface
  use testtools
  implicit none
  character(len=*), intent(in) :: process
  real(kind=prec), intent(in) :: arr(:)
  real(kind=prec), intent(in) :: masses(:)
  real(kind=prec), intent(in) :: ref(3)
  integer, optional, intent(in) :: hel1, hel2

  real (kind=prec) :: weight
  real (kind=prec) :: tr, si, fi, mymasses(7)
  integer npart

  npart = (size(arr)+4)/3
  select case(npart)
    case(2)
      call psx2(arr, p1, masses(1), p2, masses(2), p3, masses(3), p4, masses(4), weight)
    case(3)
      call psx3_fks(arr, p1, masses(1), p2, masses(2), p3, masses(3), p4, masses(4), p5, weight)
    case(4)
      call psx4_fkss(arr, p1, masses(1), p2, masses(2), p3, masses(3), p4, masses(4), p5, p6, weight)
  end select

  if (weight < zero) call crash("checkol")

  call openloops(process, p1, p2, p3, p4, p5, p6, p7, &
       tree=tr, sin=si, fin=fi, reload=.true., &
       hel1=hel1, hel2=hel2)

  call check(trim(process) // " tree", tr, ref(1))
  call check(trim(process) // " pole", si, ref(2))
  call check(trim(process) // " finite", fi, ref(3))

  END SUBROUTINE CHECKOL


  END PROGRAM TEST
