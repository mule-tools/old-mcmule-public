#ifndef USERMOD
#define USERMOD user
#endif

  BLOCK
  use USERMOD, only: the_nr_q => nr_q, &
                  the_nr_bins => nr_bins, &
                  the_min_val => min_val, &
                  the_max_val => max_val, &
                  the_inituser => inituser, &
                  the_userevent => userevent, &
                  the_quant => quant, &
                  the_userdim => userdim, &
                  the_bin_kind => bin_kind, &
                  namesLen, filenamesuffixLen

  nr_q = the_nr_q
  nr_bins = the_nr_bins
  allocate(min_val(size(the_min_val)))
  min_val = the_min_val
  allocate(max_val(size(the_max_val)))
  max_val = the_max_val

  userdim = the_userdim
  bin_kind = the_bin_kind

  inituser => the_inituser
  userevent => the_userevent
  call set_quant(the_quant)

  allocate(pass_cut(nr_q))
  allocate(character(namesLen) :: names(nr_q))
  allocate(character(filenamesuffixLen) :: filenamesuffix)

  END BLOCK
