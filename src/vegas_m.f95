



                 !!!!!!!!!!!!!!!!!!!!!!!
                     MODULE  VEGAS_M
                 !!!!!!!!!!!!!!!!!!!!!!!

!  use global_def
  use global_def, only: prec, integrand

  integer, parameter :: vegas_version = 3

  real(kind=prec), dimension(:,:), allocatable ::  quant_list

  integer, parameter :: maxdim = 17
  real(kind=prec), private:: xi(50,maxdim)
         ! as  this  line is absent in old version

  ! other cumulative variables absent in the old version (this used to be the
  ! common block bveg2
  integer, private :: it, ndo
  real(kind=prec), private:: si, swgt, schi

  real(kind=prec), dimension(:,:), allocatable :: quantsum, quantsumsq
  real(kind=prec), private :: starttime
  real(kind=prec) :: alph
  integer, private :: ndmx, mds
  real(kind=prec), private :: xl(maxdim), xu(maxdim), acc
  character(len=5) :: oldsha


  character(len=40) :: fullsha
  character(len=40) :: gitrev
  character(len=40) :: gitbranch
  common/version/fullsha,gitrev,gitbranch


!==!      common/bveg2/it,ndo,si,swgt,schi,xi(50,maxdim)

  contains


!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! Copyright (c) 1976/1988/1995 G.P. Lepage, Cornell University c
! Permission is granted for anyone to use vegas software       c
! for any purpose on any computer. The author is not           c
! responsible for the consequences of such use. The            c
! software is meant to be freely distributed.                  c
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!    <<<<<<<<<<< CHANGED VERSION Adrian Signer >>>>>>>>>>>>>

!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! the following routines are essential to vegas()
!
! N.B. Integrals up to dimension "maxdim" can be done with the code
!     currently maxdim = 17 :: if maxdim is changed
!     explicit change  x(17) -> x(??) needed in line 64



      subroutine vegas(ndim,ncall,itmx,fxn,avgi,sd,chi2a,randy,filename, silent)
!
!   ndim-dimensional Monte Carlo integration of fxn
!        - gp lepage  sept 1976/(rev)aug 1979  (j comp phys 27,192(1978))
!
!   arguments:
!       ndim = no of dimensions
!       fxn  = integrand f(x,wgt) where x(i)=integ'n pt (can ignore wgt)
!       avgi = Monte Carlo estimate of integral
!       sd   = error estimate for answer
!       chi2a= chi^2/dof test that different iterations agree
!              (should be of order 1 or less)
!
!   implicit arguments (via common block; all have default values):
!       ncall = number of fxn evaluations per iteration
!       itmx  = number of iterations
!       xl(i) = lower limit of integration in direction i
!       xu(i) = upper limit of integration in direction i
!       acc   = accuracy desired
!
      implicit real(kind=prec)(a-h,o-z)
      character(len=*), optional :: filename
      logical, optional :: silent
      logical resume
!==!      common/bveg1/ndev,xl(maxdim),xu(maxdim),acc
!==!      common/bveg2/it,ndo,si,swgt,schi,xi(50,maxdim)
!==!      common/bveg3/alph,ndmx,mds
      dimension d(50,maxdim),di(50,maxdim),xin(50),r(50),dx(maxdim), &
                ia(maxdim),kg(maxdim),dt(maxdim),                    &
                x(maxdim) !! as see fist line !! ,xl(maxdim),xu(maxdim),xi(50,maxdim)
      integer randy
!      real ran2
      real(kind=prec) smth
      procedure(integrand) :: fxn
!      external fxn
      data smth/2/

!  initiallize non needed random variables to any value

!      do i= ndim+1,12
!         x(i) = 0.3_prec
!      enddo

!
!  I've put the default assignments which will NOT be overridden here
!
      do i = 1,maxdim
        xl(i) = 0._prec
        xu(i) = 1._prec
        do ii = 1,50
          xi(ii,i) = 1._prec
        enddo
      enddo

      alph = 1.5_prec
      ndmx = 50
      mds = 1
      ndev = 6
      ndo = 1
      it = 0
      si = 0._prec
      swgt = 0._prec
      schi = 0._prec
      acc = -1._prec
!
!     end default assignments
!
      ndo=1
      do j=1,ndim
        xi(1,j)=1._prec
      enddo
!
      entry vegas1(ndim,ncall,itmx,fxn,avgi,sd,chi2a,randy,filename)
      call cpu_time(starttime)
!         - initializes cummulative variables, but not the grid
      if (present(filename)) then
        oldsha="newcc"
        quant_list = 0.
        quantsum=0.
        quantsumsq=0.
        INQUIRE(FILE=trim(filename), EXIST=resume)
        if (resume) then
            call loadvegas(trim(filename),itmx,randy)
            if (it.lt.itmx) goto 998
            goto 999
        endif
      endif
      call flush(6)
      it=0
      si=0._prec
      swgt=si
      schi=si
!
      entry vegas2(ndim,ncall,itmx,fxn,avgi,sd,chi2a,randy)
!         - no initialization
998   nd=ndmx
      ng=1
      if(mds.eq.0) go to 2
      ng=int((ncall/2.)**(1./ndim))
      mds=1
      if((2*ng-ndmx).lt.0) go to 2
      mds=-1
      npg=ng/ndmx+1
      nd=ng/npg
      ng=npg*nd
 2    k=ng**ndim
      npg=ncall/k
      if(npg.lt.2) npg=2
      calls=npg*k
      dxg=1._prec/ng
      dv2g=(calls*dxg**ndim)**2/npg/npg/(npg-1._prec)
      xnd=nd
      ndm=nd-1
      dxg=dxg*xnd
      xjac=1./calls
      do j=1,ndim
        dx(j)=xu(j)-xl(j)
        xjac=xjac*dx(j)
      enddo
!
!   rebin, preserving densities
      if(nd.eq.ndo) go to 8
      rc=ndo/xnd
      do  j=1,ndim
        k=0
        xn=0
        dr=xn
        i=k
 4      k=k+1
        dr=dr+1.
        xo=xn
        xn=xi(k,j)
 5      if(rc.gt.dr) go to 4
        i=i+1
        dr=dr-rc
        xin(i)=xn-(xn-xo)*dr
        if(i.lt.ndm) go to 5
        do  i=1,ndm
         xi(i,j)=xin(i)
        enddo
        xi(nd,j)=1._prec
      enddo
      ndo=nd
!
 8    continue
! if(nprn.ge.0) write(ndev,200) ndim,calls,it,itmx,acc,nprn, &
!                          alph,mds,nd,(j,xl(j),j,xu(j),j=1,ndim)
!
      entry vegas3(ndim,fxn,avgi,sd,chi2a)
!         - main integration loop
 9    it=it+1
      ti=0.
      tsi=ti
      do  j=1,ndim
        kg(j)=1
        do  i=1,nd
          d(i,j)=ti
          di(i,j)=ti
        enddo
      enddo

 11   fb=0.
      f2b=fb
      k=0
 12   k=k+1
      wgt=xjac
      do  j=1,ndim
        xn=(kg(j)-ran2(randy))*dxg+1.
        ia(j)=int(xn)
        if(ia(j).gt.1) go to 13
        xo=xi(ia(j),j)
        rc=(xn-ia(j))*xo
        go to 14
 13     xo=xi(ia(j),j)-xi(ia(j)-1,j)
        rc=xi(ia(j)-1,j)+(xn-ia(j))*xo
 14     x(j)=xl(j)+rc*dx(j)
      wgt=wgt*xo*xnd
      enddo
!
      f=wgt
      f=f*fxn(x,wgt,ndim)
      f2=f*f
      fb=fb+f
      f2b=f2b+f2
      do  j=1,ndim
        di(ia(j),j)=di(ia(j),j)+f
        if(mds.ge.0) d(ia(j),j)=d(ia(j),j)+f2
      enddo
      if(k.lt.npg) go to 12
!
      f2b=sqrt(f2b*npg)
      f2b=(f2b-fb)*(f2b+fb)
      ti=ti+fb
      tsi=tsi+f2b
      if(mds.ge.0) go to 18
      do  j=1,ndim
        d(ia(j),j)=d(ia(j),j)+f2b
      enddo
 18   k=ndim
 19   kg(k)=mod(kg(k),ng)+1
      if(kg(k).ne.1) go to 11
      k=k-1
      if(k.gt.0) go to 19
!
!   compute final results for this iteration
      tsi=tsi*dv2g
      ti2=ti*ti
      wgt=1./tsi
      si=si+ti*wgt
      swgt=swgt+wgt
      schi=schi+ti2*wgt
      avgi=si/swgt
      chi2a=(schi-si*avgi)/(it-.9999)
      sd=sqrt(1./swgt)
      tsi=sqrt(tsi)
      if (.not.present(silent)) then
        print*,"internal avgi, sd: ", avgi, sd  !control!
        call flush(6)
      endif
!
!
!   refine grid
      do  j=1,ndim
        xo=d(1,j)
        xn=d(2,j)
        d(1,j)=(smth*xo+xn)/(smth+1)
        dt(j)=d(1,j)
        do  i=2,ndm
          d(i,j)=xo+smth*xn
          xo=xn
          xn=d(i+1,j)
          d(i,j)=(d(i,j)+xn)/(smth+2)
          dt(j)=dt(j)+d(i,j)
        enddo
      d(nd,j)=(xo+smth*xn)/(smth+1)
        dt(j)=dt(j)+d(nd,j)
      enddo
!
      do  j=1,ndim
      rc=0.
      do  i=1,nd
        r(i)=0.
        if(d(i,j).le.0.) go to 24
        xo=dt(j)/d(i,j)
        r(i)=((xo-1.)/xo/log(xo))**alph
 24     continue
        rc=rc+r(i)
      enddo
      rc=rc/xnd
      k=0
      xn=0.
      dr=xn
      i=k
 25   k=k+1
      dr=dr+r(k)
      xo=xn
      xn=xi(k,j)
 26   if(rc.gt.dr) go to 25
      i=i+1
      dr=dr-rc
      xin(i)=xn-(xn-xo)*dr/r(k)
      if(i.lt.ndm) go to 26
        do  i=1,ndm
          xi(i,j)=xin(i)
        enddo
        xi(nd,j)=1.
      enddo

      if (present(filename)) call savevegas(filename,randy)
!
      if(it.lt.itmx.and.acc*abs(avgi).lt.sd) go to 9
!
      !if (present(filename))  call deletevegas(filename)
999   return
      end subroutine vegas


  SUBROUTINE INITVEGAS
  use user_dummy, only: nr_q,nr_bins
  implicit none
  allocate(quant_list(nr_q,0:nr_bins+1))
  allocate(quantsum  (nr_q,0:nr_bins+1))
  allocate(quantsumsq(nr_q,0:nr_bins+1))
  END SUBROUTINE INITVEGAS


  SUBROUTINE SAVEVEGAS(FILENAME,RANDY)
  use user_dummy, only: nr_q,nr_bins, min_val,max_val,names
  implicit none
  integer randy, namelen
  real(kind=prec) :: time
  character(len=*) :: filename
  character(len=300) :: msg
  character(len=10) :: vversion
  character(len=1) :: intyness

  quantsum = quantsum+quant_list
  quantsumsq = quantsumsq+quant_list**2
  quant_list=0.
  call cpu_time(time)
  if (oldsha.eq."newcc") then
    msg = "Uninterupted integration. Program SHA is"//fullsha//" (git:"//gitrev//")"
  elseif (oldsha.ne.fullsha(:5)) then
    msg = "Warning! SHA changed! "//oldsha//" -> "//fullsha//" (git:"//gitrev//")"
  else
    msg = "Interupted integration. Program SHA agrees: "//fullsha//" (git:"//gitrev//")"
  endif
  !==!      common/bveg2/it,ndo,si,swgt,schi,xi(50,maxdim)
  select case(sizeof(it))
    case(4)
      intyness = "N"
    case(8)
      intyness = "L"
  end select

  write(vversion,900) vegas_version, intyness
  namelen = int(sizeof(names(1)), 4)

  open(                             &
      unit=8,                       &
      action='WRITE',               &
      file=trim(adjustl(filename)), &
      form='UNFORMATTED'            &
  )
  write(8) " McMule  "
  write(8) vversion

  write(8) fullsha(:5)
  write(8) it
  write(8) ndo
  write(8) si
  write(8) swgt
  write(8) schi
  write(8) xi
  write(8) randy
  write(8) nr_q,nr_bins,namelen
  write(8) min_val,max_val
  write(8) names
  write(8) quantsum,quantsumsq
  write(8) time-starttime
  write(8) msg
  close(8)

900     FORMAT("v",I1,A1)
  END SUBROUTINE SAVEVEGAS

  SUBROUTINE LOADVEGAS(FILENAME,ITMX,RANDY)
  use user_dummy, only: nr_q, nr_bins
  use global_def, only: crash
  implicit none
  character(len=*) :: filename
  character(len=5) :: sha
  character(len=300) :: msg
  integer itmx,randy, my_nr_q,my_nr_bins, myversion, namelen
  real(kind=prec) :: avgi, chi2a,sd, time, now
  real(kind=prec) ::  max_val(nr_q),min_val(nr_q)
  character (len = 6), dimension(nr_q) :: names
  character(len=10) :: vversion
  character(len=1) :: intyness, myintyness
  call cpu_time(now)

  select case(sizeof(it))
    case(4)
      myintyness = "N"
    case(8)
      myintyness = "L"
  end select

  open(                             &
      unit=8,                       &
      action='READ',                &
      file=trim(adjustl(filename)), &
      form='UNFORMATTED'            &
  )
  read(8) vversion(1:9)
  if (vversion(1:9) .ne. " McMule  ") call crash("load vegas")
  read(8) vversion
  read(vversion(2:10), "(I1,A1)") myversion, intyness
  if ( (myversion .ne. vegas_version).or.(intyness.ne.myintyness) ) call crash("load vegas")

  read(8) sha
  read(8) it
  read(8) ndo
  read(8) si
  read(8) swgt
  read(8) schi
  read(8) xi
  read(8) randy
  read(8) my_nr_q,my_nr_bins, namelen
  if (namelen .ne. sizeof(names(1))) call crash("load vegas")
  read(8) min_val,max_val
  read(8) names

  if ( (my_nr_q == nr_q).and.(my_nr_bins == nr_bins) ) then
    read(8) quantsum,quantsumsq
  endif
  read(8) time
  read(8) msg
  close(8)
  starttime = now-time

  avgi=si/swgt
  chi2a=(schi-si*avgi)/(it-.9999)
  sd=sqrt(1./swgt)

  write(*,900)
  write(*,901)
  write(*,902) it,itmx
  write(*,903) avgi,sd,chi2a
  oldsha=sha
  if (sha.ne.fullsha(:5)) write(*,906) sha,fullsha(:5)
  write(*,904) int(time)
  write(*,*) trim(msg)
  write(*,905)

900     format('         Resuming vegas            ')
901     format('   - * - * - * - * - * - * - * -   ')
902     format(' current iteration ',I6,'/',I6,'   ')
903     format(' result, error:  {',ES13.5,',',ES13.5,' };   chisq:  ',F4.2 )
904     format(' integration took ',I7,'s. Message was')
906     format(' WARNING: SHA1 hash changed ',A5,' --> ',A5, ". results may be incorrect")
905     format('   - * - * - * - * - * - * - * -   ')
  END SUBROUTINE LOADVEGAS

  SUBROUTINE DELETEVEGAS(FILENAME)
  character(len=*) :: filename
  integer stat
  open(unit=1234, iostat=stat, file=filename, status='old')
  if (stat == 0) close(1234, status='delete')
  END SUBROUTINE DELETEVEGAS

  SUBROUTINE COMBINE_QUANT(ITMX,RES_LIST,ERR_LIST)
  use user_dummy, only: nr_q,nr_bins
  implicit none
  integer, intent(in) :: itmx
  real (kind=prec), intent(out):: res_list(nr_q,0:nr_bins+1), err_list(nr_q,0:nr_bins+1)

  res_list = quantsum/itmx
  err_list = quantsumsq - quantsum**2/itmx
  err_list = sqrt(err_list/itmx/(itmx-1))
  END SUBROUTINE COMBINE_QUANT


  SUBROUTINE QUANT2MMA(ITMX)
  use user_dummy, only: nr_q,nr_bins, names
  implicit none
  integer, intent(in) :: itmx
  integer iq, ic
  real (kind=prec) :: res_list(nr_q,0:nr_bins+1), err_list(nr_q,0:nr_bins+1)
  call combine_quant(itmx,res_list,err_list)
  do iq = 1,nr_q
    write(6,*) names(iq),' =  Drop[ImportString["  '
    do ic = 1,nr_bins - 1
      write(6,"(a,es11.4,a,es11.4,a)")      &
        '   ',res_list(iq,ic), '  ',err_list(iq,ic),'  '
    enddo
    write(6,"(a,es11.4,a,es11.4,a)")        &
      '   ',res_list(iq,nr_bins), '  ',err_list(iq,nr_bins),' ","Table"],1]; '
    write(6,*) '     '
    write(6,*) names(iq),'ovflow = ImportString["  '
    write(6,"(a,es11.4,a,es11.4,a)")      &
      '   ',res_list(iq,0), '  ',err_list(iq,0),'  '
    write(6,"(a,es11.4,a,es11.4,a)")      &
      '   ',res_list(iq,nr_bins+1), '  ',err_list(iq,nr_bins+1),' ","Table"],1]; '
  enddo

  END SUBROUTINE



  FUNCTION RAN2(randy)

        ! This is the usual "random"

  implicit none
  real(kind=prec) :: MINV,RAN2
  integer m,a,Qran,r,hi,lo,randy
  PARAMETER(M=2147483647,A=16807,Qran=127773,R=2836)
  PARAMETER(MINV=0.46566128752458e-09_prec)
  HI = RANDY/Qran
  LO = MOD(RANDY,Qran)
  RANDY = A*LO - R*HI
  IF(RANDY.LE.0) RANDY = RANDY + M
  RAN2 = RANDY*MINV
  END FUNCTION RAN2







                  !!------------------------- !!
                  !!         BINNING          !!
                  !!------------------------- !!

            !! ----------------------------------------- !!
            !!     There are two versions of binning     !!
            !!     One for computing   d \sigma/ d Q     !!
            !!     One for computing  Q d \sigma/ d Q    !!
            !!  choose by setting the variable bin_kind  !!
            !! ----------------------------------------- !!




  SUBROUTINE BIN_IT(wg,var)
  use global_def, only: zero
  use user_dummy, only: min_val, max_val, nr_q, nr_bins, bin_kind, pass_cut
  implicit none
  real (kind=prec) :: bin_length
  real (kind=prec) :: wg,  var(nr_q)
  real (kind=prec) :: Bvar(nr_q)
  integer :: bin_nr, iy

  select case(bin_kind)
  case(0)
           !!-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-!!
           !!   version for computing  d \sigma/ d Q    !!
           !!-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-!!
    Bvar = 1._prec

  case(1)
           !!-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-!!
           !!  version for computing  Q d \sigma/ d Q   !!
           !!-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-!!
    Bvar = var

  case default
    print*, ' illegal choice for variable bin_kind '
    stop
  end select


  do iy = 1, nr_q
    if(.not.pass_cut(iy)) cycle   !! event has been thrown away

    if(abs(var(iy)-min_val(iy)).lt.zero) then
      ! first bin, corner
      bin_nr = 1
    elseif(abs(var(iy)-max_val(iy)).lt.zero) then
      ! last bin, corner
      bin_nr = nr_bins
    elseif(var(iy) > min_val(iy) .and. var(iy) < max_val(iy)) then
      ! somewhere in the middle
      bin_length = (max_val(iy) - min_val(iy))/nr_bins
      bin_nr = ceiling((var(iy) - min_val(iy))/bin_length )
    elseif (var(iy) < min_val(iy)) then
      ! underflow
      bin_nr = 0
    elseif (var(iy) > max_val(iy)) then
      ! overflow
      bin_nr = nr_bins+1
    endif
    quant_list(iy,bin_nr) = quant_list(iy,bin_nr) + wg*Bvar(iy)
  enddo



  END SUBROUTINE BIN_IT




                 !!!!!!!!!!!!!!!!!!!!!!!!!!
                     END MODULE  VEGAS_M
                 !!!!!!!!!!!!!!!!!!!!!!!!!!

