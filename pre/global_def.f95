 MODULE GLOBAL_DEF


 implicit none

        !!  ----------
        !!  parameters
        !!  ----------

 integer, parameter :: prec = selected_real_kind(15,32)
 integer, parameter :: precnf = 16

 abstract interface
   function integrand(x,wgt,ndim)
     import prec
     integer :: ndim
     real(kind=prec) :: x(ndim),wgt
     real(kind=prec) :: integrand
   end function integrand
 end interface

 integer, parameter :: maxparticles = 7

 real (kind=prec), parameter :: pi = 3.14159265358979323846_prec
 real (kind=prec), parameter :: z3 = 1.20205690315959428540_prec
 real (kind=prec), parameter :: log2 = 0.693147180559945309417_prec
 real (kind=prec), parameter :: xsc = 0._prec  ! FDH=>1 vs HV=>0
 real (kind=prec), parameter :: Nc = 3._prec
 real (kind=prec), parameter :: Tf = 0.5_prec
 real (kind=prec), parameter :: Cf = (Nc**2-1)/(2*Nc)
 real (kind=prec), parameter :: Nf = 5._prec

 complex (kind=prec), parameter :: imag = (0.0_prec,1.0_prec)
 real (kind=prec), parameter :: zero = 1.0E-50_prec

! real (kind=prec), parameter :: GF = 1.1663787e-11_prec
! real (kind=prec), parameter :: alpha = 1/137.035999084_prec
 real (kind=prec), parameter :: GF = 1._prec
 real (kind=prec), parameter :: alpha = 1._prec
 real (kind=prec) :: sW2 = 0.23122_prec

 real (kind=prec), parameter :: Mmu = 105.658375_prec     ! MeV
 real (kind=prec), parameter :: Mel = 0.510998950_prec    ! MeV
 real (kind=prec), parameter :: Mtau = 1776.86_prec       ! MeV
 real (kind=prec), parameter :: Mproton = 938.272088_prec ! MeV
 real (kind=prec), parameter :: MZ = 91187.6_prec ! MeV

 character (len=3), parameter :: cgamma = "gam"

 integer throw_away
 integer(kind=8) pcounter(2)
 logical :: runninglab = .false.



        !!  ---------
        !!  variables
        !!  ---------

 integer :: ran_seed = 1

 real (kind=prec) :: pol1(4), pol2(4)
 real (kind=prec) :: musq, delcut, xinormcut
 real (kind=prec) :: xinormcut1, xinormcut2
 real(kind=prec) :: xicut1, xicut2
 real(kind=prec) :: xieik1, xieik2

 real(kind=prec) :: softcut = 0.
 real(kind=prec) :: collcut = 0.

 real(kind=prec) :: sSwitch   = 0.  ! when to switch to eikonal
 real(kind=prec) :: ntsSwitch = 0.  ! when to switch to next-to-soft
 real(kind=prec) :: pcSwitch = 0.   ! when to switch to col. approx.

 integer :: nel = 1
 integer :: nmu = 1
 integer :: ntau = 1
 integer :: nhad = 1

 real (kind=prec), protected :: Mm ! MeV
 real (kind=prec), protected :: Me ! MeV
 real (kind=prec), protected :: Mt ! MeV
 real (kind=prec), protected :: scms
 real (kind=prec), protected :: lambda = 0.71E6
 real (kind=prec), protected :: kappa = 2.79284734_prec
 character (len=25) :: which_piece
 character (len=15) :: flavour

 !!-------------!!
 !!   MAJORON   !!
 !!-------------!!

 real (kind=prec) :: CLj = 0._prec
 real (kind=prec) :: CRj = 1._prec
 real (kind=prec) :: Mj ! MeV

contains

  SUBROUTINE CRASH(function_name)

  character(len=*) :: function_name

  write(6,*) "Program crashes because of a call to the function ", &
            function_name
  stop

  END SUBROUTINE CRASH


  FUNCTION SACHS_GEL(Q2)
  real(kind=prec) sachs_GEL, Q2
  sachs_GEL = (1.+Q2/lambda)**(-2)
  END FUNCTION
  FUNCTION SACHS_GMAG(Q2)
  real(kind=prec) sachs_GMAG, Q2
  sachs_GMAG = kappa*(1.+Q2/lambda)**(-2)
  END FUNCTION

  FUNCTION SACHS_GEL1(Q2)
  real(kind=prec) sachs_GEL1, Q2
  sachs_GEL1 = (1.+Q2/lambda)**(-1)
  END FUNCTION
  FUNCTION SACHS_GMAG1(Q2)
  real(kind=prec) sachs_GMAG1, Q2
  sachs_GMAG1 = kappa*(1.+Q2/lambda)**(-1)
  END FUNCTION

  SUBROUTINE INITFLAVOUR(flav, mys)
  character (len=*), optional, intent(in) :: flav
  real(kind=prec), optional, intent(in) :: mys
  if (present(flav)) flavour = flav

  select case(flavour)
  case("mu-e")
    Mm = Mmu
    Me = Mel
  case("tau-mu")
    Mm = Mtau
    Me = Mmu
  case("tau-e")
    Mm = Mtau
    Me = Mel
  case("tau-mue")
    Mt = Mtau
    Mm = Mmu
    Me = Mel
  case("tau-emu")
    Mt = Mtau
    Mm = Mel
    Me = Mmu
  case("mu-0")
    Mm = Mmu
    Me = 0._prec
  case("tau-0")
    Mm = Mtau
    Me = 0._prec
  case("e-s")
    Me = mel
    Mm = mmu
    read(5,*) scms
  case("muone")
    Me = mel
    Mm = mmu
    scms = me**2+mm**2+2*me*150.e3_prec ! MeV
  case("muone-")
    Me = mel
    Mm = mmu
    scms = me**2+mm**2+2*me*150.e3_prec ! MeV
  case("muone+")
    Me = mel
    Mm = mmu
    scms = me**2+mm**2+2*me*150.e3_prec ! MeV
  case("muone160")
    Me = mel
    Mm = mmu
    scms = me**2+mm**2+2*me*160.e3_prec ! MeV
  case("muone160-")
    Me = mel
    Mm = mmu
    scms = me**2+mm**2+2*me*160.e3_prec ! MeV
  case("muone160+")
    Me = mel
    Mm = mmu
    scms = me**2+mm**2+2*me*160.e3_prec ! MeV
  case("mesa")
    Me = mel
    Mm = mproton
    scms = me**2+mm**2+2*mm*155._prec !MeV
  case("muse210")
    Me = mmu
    Mm = mproton
    scms = me**2+mm**2+2*mm*sqrt(me**2+(210._prec)**2)
  case("muse210e")
    Me = mel
    Mm = mproton
    scms = me**2+mm**2+2*mm*sqrt(me**2+(210._prec)**2)
  case("ee1GeV")
    Me = Mel
    scms = 2*me**2+2*me*1000._prec
  case("prad")
    Me = Mel
    scms = 2*me**2+2*me*1100._prec
  case("prad2.2")
    Me = Mel
    scms = 2*me**2+2*me*2200._prec
  case("daphne")
    Me = Mel
    scms = (1020._prec)**2
  case("muse153")
    Me = mmu
    Mm = mproton
    scms = me**2+mm**2+2*mm*sqrt(me**2+(153._prec)**2)
  case("muse115")
    Me = mmu
    Mm = mproton
    scms = me**2+mm**2+2*mm*sqrt(me**2+(115._prec)**2)
  case("dafne")
    Me = mel
    Mm = mmu
    scms = 1.1e3**2
  case("belleTau")
    Me = Mel
    Mm = Mtau
   !scms = (E1+E2)**2 - (sqrt(E1**2-Me**2) - sqrt(E2**2-Me**2))**2
    scms = (4e3+7e3)**2 - (sqrt(4e3**2-Me**2) - sqrt(7e3**2-Me**2))**2
    scms = (10.5830052e3)**2  ! Y(4S)
  case("muoneOld")
    Me = 0.510998928_prec
    Mm = 105.6583715_prec
    scms = 164463.6370519
  case("mu-eOld")
    Me = 0.510998928_prec
    Mm = 105.6583715_prec
  case("museOld")
    Me = 105.6583715_prec
    Mm = 938.2720813_prec
    scms = me**2+mm**2+2*mm*sqrt(me**2+(210._prec)**2)
    kappa = 2.7928
  case("mesaOld")
    Me = 0.510998928_prec
    Mm = 938.2720813_prec
    scms = me**2+mm**2+2*mm*155.!MeV
    kappa = 2.7928
  case("tau-mueO")
    Mt = 1776.82_prec
    Me = 0.510998928_prec
    Mm = 105.6583715_prec
  case("clean")
    Mm = 0.
    Me = 0.
    Mt = 0.
    scms = 0.
  case default
    print*, "input value flavour = ", flavour, " not implemented"
    stop
  end select

  if (present(mys)) scms = mys

  END SUBROUTINE


 END MODULE GLOBAL_DEF











