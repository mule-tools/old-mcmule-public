                 !!!!!!!!!!!!!!!!!!!!!!!!!
                   MODULE MASSIFICATION
                 !!!!!!!!!!!!!!!!!!!!!!!!!

    use functions
    implicit none

    real(kind=prec), parameter :: zeta(2:5) = (/1.6449340668482264365, &
                                                1.2020569031595942854, &
                                                1.0823232337111381915, &
                                                1.0369277551433699263 /)

    real(kind=prec), parameter :: ln2 = 0.69314718055994530942


    contains

  SUBROUTINE MASSIFY(Lm, power, &
      treelevel0, oneloop0, twoloop0, &
      oneloopZ, twoloopZ, &
      Lr, checkpole)
  ! This assume FDH matrix elements. If you give it anything else, it
  ! will fail
  implicit none
  real(kind=prec), intent(in) :: Lm, power
  real(kind=prec), intent(in ) :: treelevel0
  real(kind=prec), intent(in ) :: oneloop0(-2:2)
  real(kind=prec), intent(out) :: oneloopZ(-1:1)

  real(kind=prec), intent(in ), optional :: twoloop0(-4:0)
  real(kind=prec), intent(out), optional :: twoloopZ(-2:0)

  real(kind=prec), intent(in), optional :: Lr
  real(kind=prec), intent(in), optional :: checkpole

  real(kind=prec) :: oneloopZfull(-2:1)
  real(kind=prec) :: twoloopZfull(-4:0)

  real(kind=prec), parameter :: Z10(-2:2) = &
     (/ 2., 1., 3+2*zeta(2), 7+zeta(2), 15+3*zeta(2) + 7/2.*zeta(4) /)

  real(kind=prec), parameter :: Z20(-4:0) = &
     (/ 2., 2., 13/2. + 4*zeta(2), 75/4. - 2*zeta(2) + 12*zeta(3), &
        533/8. + 37*zeta(2) - 6*zeta(3) - 30*zeta(4) - 8*pi**2*ln2 /)

  real(kind=prec) :: Z1(-2:2), Z2(-4:0)

  Z1 = Z10 - [0., Z10(-2:1)] * Lm &
           + [0., 0., Z10(-2:0)] * Lm**2 / 2. &
           - [0., 0., 0., Z10(-2:-1)] * Lm**3 / 6. &
           + [0., 0., 0., 0., Z10(-2)] * Lm**4 / 24.

  Z2 = Z20 - [0., Z20(-4:-1)] * (2*Lm) &
           + [0., 0., Z20(-4:-2)] * (2*Lm)**2 / 2. &
           - [0., 0., 0., Z20(-4:-3)] * (2*Lm)**3 / 6. &
           + [0., 0., 0., 0., Z20(-4)] * ((2*Lm)**4 / 24. + Lm)

  if(present(Lr)) then
    Z2(0) = Z2(0) - Z20(-4) * Lr
  endif

  Z1 = Z1/2
  Z2 = Z2/4

  oneloopZfull = oneloop0(-2:1) + power*treelevel0 * Z1(-2:1)
  oneloopZ = oneloopZfull(-1:1)
  if(present(checkpole)) then
    if(abs(oneloopZfull(-2)) > checkpole) then
      print*, "The pole did not cancel"
    endif
  endif


  if(present(twoloop0).and.present(twoloopZ)) then
    twoloopZfull = twoloop0 + power * treelevel0 * Z2 &
          + oneloop0(-2) * power * Z1(-2:2) &
          + oneloop0(-1) * power * [0., Z1(-2:1)] &
          + oneloop0( 0) * power * [0., 0., Z1(-2:0)] &
          + oneloop0( 1) * power * [0., 0., 0., Z1(-2:-1)] &
          + oneloop0( 2) * power * [0., 0., 0., 0., Z1(-2)]
    twoloopZfull = twoloopZfull + power*(power - 1)*treelevel0 * ( &
      0.5* (/ Z1(-2)**2, 0., Z1(-1)**2, 0., Z1(0)**2 /)            &
      +    (/ 0., Z1(-2)*Z1(-1), 0., Z1(-1)*Z1(0), Z1(-1)*Z1(1) /) &
      +    (/ 0., 0.,  Z1(-2)*Z1(0), Z1(-2)*Z1(1), Z1(-2)*Z1(2) /))
    if(present(checkpole)) then
      if(any(abs(twoloopZfull(-4:-3)) > checkpole)) then
        print*, "The two-loop pole did not cancel"
      endif
    endif
    twoloopZ = twoloopZfull(-2:0)
  endif

  END SUBROUTINE MASSIFY



  SUBROUTINE EIKONALSUBTRACTION(ct, treelevel, &
      oneloop, oneloopE, twoloop, twoloopE,    &
      checkpole)
  ! This assume FDH matrix elements. If you give it anything else, it
  ! will fail
  real(kind=prec), intent(in ) :: ct(-1:0)
  real(kind=prec), intent(in ) :: treelevel
  real(kind=prec), intent(in ) :: oneloop(-1:1)
  real(kind=prec), intent(out) :: oneloopE

  real(kind=prec), intent(in ), optional :: twoloop(-2:0)
  real(kind=prec), intent(out), optional :: twoloopE

  real(kind=prec), intent(in), optional :: checkpole

  real(kind=prec) :: oneloopEfull(-1:0)
  real(kind=prec) :: twoloopEfull(-2:0)

  oneloopEfull = oneloop(-1:0) + treelevel * ct
  oneloopE = oneloopEfull(0)

  if(present(checkpole)) then
    if(abs(oneloopEfull(-1)) > checkpole) then
      print*, "The pole did not cancel"
    endif
  endif

  if(present(twoloop).and.present(twoloopE)) then
    twoloopEfull = twoloop &
          + treelevel * (/ ct(-1)**2, 2*ct(-1)*ct(0), ct(0) /) / 2. &
          + ct(-1) * oneloop                                        &
          + ct( 0) * [0., oneloop(-1:0)]
    if(present(checkpole)) then
      if(any(abs(twoloopEfull(-2:-1)) > checkpole)) then
        print*, "The two-loop pole did not cancel"
      endif
    endif
    twoloopE = twoloopEfull(0)
  endif

  END SUBROUTINE EIKONALSUBTRACTION

                 !!!!!!!!!!!!!!!!!!!!!!!!!
                 END MODULE MASSIFICATION
                 !!!!!!!!!!!!!!!!!!!!!!!!!

