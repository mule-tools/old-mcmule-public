                          !!!!!!!!!!!!!!!!!
                         MODULE ff_heavyquark
                          !!!!!!!!!!!!!!!!!
    use functions
    implicit none
    real(kind=prec), parameter :: zeta(2:5) = (/1.6449340668482264365, &
                                                1.2020569031595942854, &
                                                1.0823232337111381915, &
                                                1.0369277551433699263 /)
   integer, parameter :: fac(1:7) = (/1, 2, 6, 24, 120, 720, 5040 /)

contains
  SUBROUTINE GETHPLS(x, Hc1, Hc2, Hc3, Hc4)
  implicit none
  integer, parameter :: n1 = -1
  integer, parameter :: n2 = +1
  integer, parameter :: nw =  4
  complex(kind=8) Hc1(-1:+1)
  complex(kind=8) Hc2(-1:+1,-1:+1)
  complex(kind=8) Hc3(-1:+1,-1:+1,-1:+1)
  complex(kind=8) Hc4(-1:+1,-1:+1,-1:+1,-1:+1)

  real(kind=8) Hi1(-1:+1)
  real(kind=8) Hi2(-1:+1,-1:+1)
  real(kind=8) Hi3(-1:+1,-1:+1,-1:+1)
  real(kind=8) Hi4(-1:+1,-1:+1,-1:+1,-1:+1)

  real(kind=8) Hr1(-1:+1)
  real(kind=8) Hr2(-1:+1,-1:+1)
  real(kind=8) Hr3(-1:+1,-1:+1,-1:+1)
  real(kind=8) Hr4(-1:+1,-1:+1,-1:+1,-1:+1)
  real(kind=prec), intent(in) :: x
  call hplog(x, nw, Hc1, Hc2, Hc3, Hc4, &
                    Hr1, Hr2, Hr3, Hr4, &
                    Hi1, Hi2, Hi3, Hi4, n1, n2)
  END SUBROUTINE


  SUBROUTINE HQFF(x, Ls, oneloopF1, twoloopF1, oneloopF2, twoloopF2, &
                         oneloopG1, twoloopG1, oneloopG2, twoloopG2)
  implicit none
  real(kind=prec), intent(in) :: x, Ls
  complex(kind=prec), optional, intent(out) :: oneloopF1(:), twoloopF1(:)
  complex(kind=prec), optional, intent(out) :: oneloopF2(:), twoloopF2(:)
  complex(kind=prec), optional, intent(out) :: oneloopG1(:), twoloopG1(:)
  complex(kind=prec), optional, intent(out) :: oneloopG2(:), twoloopG2(:)
  complex(kind=prec) Hr1(-1:+1)
  complex(kind=prec) Hr2(-1:+1,-1:+1)
  complex(kind=prec) Hr3(-1:+1,-1:+1,-1:+1)
  complex(kind=prec) Hr4(-1:+1,-1:+1,-1:+1,-1:+1)
  real(kind=prec) :: xpow(1:7)
  real(kind=prec) :: xp1(1:5)
  real(kind=prec) :: xm1(1:5)
  real(kind=prec) :: xp1xm1
  complex(kind=prec) :: bit(-4:2), Lbit(-4:2)
  integer i, j

  call gethpls(x, Hr1, Hr2, Hr3, Hr4)
  xpow = x**(/1,2,3,4,5,6,7/)
  xp1 = 1._prec/(x+1)**(/1,2,3,4,5/)
  xm1 = 1._prec/(x-1)**(/1,2,3,4,5/)
  xp1xm1 = (xpow(2)+1)**2 / (xpow(2)+1)**2

  if (present(oneloopF1)) then
    bit = 0.

    ! The alpha^1 e^-1 coeff. of F1
    bit(-1) = bit(-1) + (-1)
    bit(-1) = bit(-1) + Hr1(0)*(xm1(1)*xp1(1)*(1 + xpow(2)))

    ! The alpha^1 e^0 coeff. of F1
    bit(0) = bit(0) + zeta(2)*(-(xm1(1)*xp1(1)*(1 + xpow(2))))
    bit(0) = bit(0) + Hr2(0,0)*(xm1(1)*xp1(1)*(1 + xpow(2)))
    bit(0) = bit(0) + Hr1(0)*((xm1(1)*xp1(1)*(3 + 2*xpow(1) + 3*xpow(2)))/2.)
    bit(0) = bit(0) + (-2)
    bit(0) = bit(0) + Hr2(-1,0)*(-2*xm1(1)*xp1(1)*(1 + xpow(2)))

    if(size(oneloopF1) > 2) then
      ! The alpha^1 e^1 coeff. of F1
      bit(1) = bit(1) + Hr3(-1,-1,0)*(4*xm1(1)*xp1(1)*(1 + xpow(2)))
      bit(1) = bit(1) + Hr2(-1,0)*(-(xm1(1)*xp1(1)*(3 + 2*xpow(1) + 3*xpow(2))))
      bit(1) = bit(1) + Hr2(0,0)*((xm1(1)*xp1(1)*(3 + 2*xpow(1) + 3*xpow(2)))/2.)
      bit(1) = bit(1) + zeta(2)*(-(xm1(1)*xp1(1)*(1 + 2*xpow(1) + 5*xpow(2)))/2.)
      bit(1) = bit(1) + Hr1(0)*((xm1(1)*xp1(1)*(7 + 2*xpow(1) + 7*xpow(2)))/2.)
      bit(1) = bit(1) + (-4)
      bit(1) = bit(1) + Hr3(-1,0,0)*(-2*xm1(1)*xp1(1)*(1 + xpow(2)))
      bit(1) = bit(1) + Hr3(0,-1,0)*(-2*xm1(1)*xp1(1)*(1 + xpow(2)))
      bit(1) = bit(1) + zeta(3)*(-2*xm1(1)*xp1(1)*(1 + xpow(2)))
      bit(1) = bit(1) + Hr3(0,0,0)*(xm1(1)*xp1(1)*(1 + xpow(2)))
      bit(1) = bit(1) + Hr1(-1)*zeta(2)*(2*xm1(1)*xp1(1)*(1 + xpow(2)))

      !FIXME
      bit(1) = bit(1) - 0.5*zeta(2)*bit(-1)
    endif
    Lbit = 0.
    do i=1,4
      do j=1,i-1
        Lbit(i-3) = Lbit(i-3) + (1*Ls)**j / fac(j) * bit(i-3-j)
      enddo
    enddo
    select case(size(oneloopF1))
      case(3)
        oneloopF1 = bit(-1:1) + Lbit(-1:1)
      case(2)
        oneloopF1 = bit(-1:0) + Lbit(-1:0)
      case(1)
        oneloopF1 = bit(0:0) + Lbit(0:0)
      case default
        call crash("HQFF")
    end select
  endif

  if (present(oneloopF2)) then
    bit = 0.

    ! The alpha^1 e^-1 coeff. of F2
    bit(-1) = 0

    ! The alpha^1 e^0 coeff. of F2
    bit(0) = bit(0) + Hr1(0)*(2*xm1(1)*xp1(1)*xpow(1))

    if(size(oneloopF2) > 2) then
      ! The alpha^1 e^1 coeff. of F2
      bit(1) = bit(1) + Hr2(-1,0)*(-4*xm1(1)*xp1(1)*xpow(1))
      bit(1) = bit(1) + zeta(2)*(-2*xm1(1)*xp1(1)*xpow(1))
      bit(1) = bit(1) + Hr2(0,0)*(2*xm1(1)*xp1(1)*xpow(1))
      bit(1) = bit(1) + Hr1(0)*(6*xm1(1)*xp1(1)*xpow(1))
    endif
    Lbit = 0.
    do i=1,4
      do j=1,i-1
        Lbit(i-3) = Lbit(i-3) + (1*Ls)**j / fac(j) * bit(i-3-j)
      enddo
    enddo
    select case(size(oneloopF2))
      case(3)
        oneloopF2 = bit(-1:1) + Lbit(-1:1)
      case(2)
        oneloopF2 = bit(-1:0) + Lbit(-1:0)
      case(1)
        oneloopF2 = bit(0:0) + Lbit(0:0)
      case default
        call crash("HQFF")
    end select
  endif

  if (present(oneloopG1)) then
    bit = 0.

    ! The alpha^1 e^-1 coeff. of G1
    bit(-1) = bit(-1) + (-1)
    bit(-1) = bit(-1) + Hr1(0)*(xm1(1)*xp1(1)*(1 + xpow(2)))

    ! The alpha^1 e^0 coeff. of G1
    bit(0) = bit(0) + zeta(2)*(-(xm1(1)*xp1(1)*(1 + xpow(2))))
    bit(0) = bit(0) + Hr2(0,0)*(xm1(1)*xp1(1)*(1 + xpow(2)))
    bit(0) = bit(0) + Hr1(0)*((xm1(1)*xp1(1)*(3 - 2*xpow(1) + 3*xpow(2)))/2.)
    bit(0) = bit(0) + (-2)
    bit(0) = bit(0) + Hr2(-1,0)*(-2*xm1(1)*xp1(1)*(1 + xpow(2)))

    if(size(oneloopG1) > 2) then
      ! The alpha^1 e^1 coeff. of G1
      bit(1) = bit(1) + Hr3(-1,-1,0)*(4*xm1(1)*xp1(1)*(1 + xpow(2)))
      bit(1) = bit(1) + Hr2(-1,0)*(-(xm1(1)*xp1(1)*(3 - 2*xpow(1) + 3*xpow(2))))
      bit(1) = bit(1) + Hr2(0,0)*((xm1(1)*xp1(1)*(3 - 2*xpow(1) + 3*xpow(2)))/2.)
      bit(1) = bit(1) + zeta(2)*(-(xm1(1)*xp1(1)*(1 - 2*xpow(1) + 5*xpow(2)))/2.)
      bit(1) = bit(1) + Hr1(0)*((xm1(1)*xp1(1)*(7 - 2*xpow(1) + 7*xpow(2)))/2.)
      bit(1) = bit(1) + (-4)
      bit(1) = bit(1) + Hr3(-1,0,0)*(-2*xm1(1)*xp1(1)*(1 + xpow(2)))
      bit(1) = bit(1) + Hr3(0,-1,0)*(-2*xm1(1)*xp1(1)*(1 + xpow(2)))
      bit(1) = bit(1) + zeta(3)*(-2*xm1(1)*xp1(1)*(1 + xpow(2)))
      bit(1) = bit(1) + Hr3(0,0,0)*(xm1(1)*xp1(1)*(1 + xpow(2)))
      bit(1) = bit(1) + Hr1(-1)*zeta(2)*(2*xm1(1)*xp1(1)*(1 + xpow(2)))
      !FIXME
      bit(1) = bit(1) - 0.5*zeta(2)*bit(-1)
    endif
    Lbit = 0.
    do i=1,4
      do j=1,i-1
        Lbit(i-3) = Lbit(i-3) + (1*Ls)**j / fac(j) * bit(i-3-j)
      enddo
    enddo
    select case(size(oneloopG1))
      case(3)
        oneloopG1 = bit(-1:1) + Lbit(-1:1)
      case(2)
        oneloopG1 = bit(-1:0) + Lbit(-1:0)
      case(1)
        oneloopG1 = bit(0:0) + Lbit(0:0)
      case default
        call crash("HQFF")
    end select
  endif

  if (present(oneloopG2)) then
    bit = 0.

    ! The alpha^1 e^-1 coeff. of G2
    bit(-1) = 0

    ! The alpha^1 e^0 coeff. of G2
    bit(0) = bit(0) + (-4*xm1(2)*xpow(1))
    bit(0) = bit(0) + Hr1(0)*(2*xm1(3)*xp1(1)*xpow(1)*(3 - 2*xpow(1) + 3*xpow(2)))

    if(size(oneloopG2) > 2) then
      ! The alpha^1 e^1 coeff. of G2
      bit(1) = bit(1) + zeta(2)*(-2*xm1(3)*xp1(1)*xpow(1)*(3 - 2*xpow(1) + &
                 3*xpow(2)))
      bit(1) = bit(1) + Hr2(0,0)*(2*xm1(3)*xp1(1)*xpow(1)*(3 - 2*xpow(1) + &
                 3*xpow(2)))
      bit(1) = bit(1) + Hr1(0)*(2*xm1(3)*xp1(1)*xpow(1)*(7 - 2*xpow(1) + 7*xpow(2)))
      bit(1) = bit(1) + (-12*xm1(2)*xpow(1))
      bit(1) = bit(1) + Hr2(-1,0)*(-4*xm1(3)*xp1(1)*xpow(1)*(3 - 2*xpow(1) + &
                 3*xpow(2)))
    endif
    Lbit = 0.
    do i=1,4
      do j=1,i-1
        Lbit(i-3) = Lbit(i-3) + (1*Ls)**j / fac(j) * bit(i-3-j)
      enddo
    enddo
    select case(size(oneloopG2))
      case(3)
        oneloopG2 = bit(-1:1) + Lbit(-1:1)
      case(2)
        oneloopG2 = bit(-1:0) + Lbit(-1:0)
      case(1)
        oneloopG2 = bit(0:0) + Lbit(0:0)
      case default
        call crash("HQFF")
    end select
  endif

  if (present(twoloopF1)) then
    bit = 0.

    ! The alpha^2 e^-2 coeff. of F1
    bit(-2) = bit(-2) + Hr2(0,0)*(xm1(2)*xp1(2)*(1 + xpow(2))**2)
    bit(-2) = bit(-2) + 0.5
    bit(-2) = bit(-2) + Hr1(0)*(-(xm1(1)*xp1(1)*(1 + xpow(2))))

    ! The alpha^2 e^-1 coeff. of F1
    bit(-1) = bit(-1) + Hr2(-1,0)*(2*xm1(1)*xp1(1)*(1 + xpow(2)))
    bit(-1) = bit(-1) + Hr3(-1,0,0)*(-4*xm1(2)*xp1(2)*(1 + xpow(2))**2)
    bit(-1) = bit(-1) + Hr3(0,-1,0)*(-2*xm1(2)*xp1(2)*(1 + xpow(2))**2)
    bit(-1) = bit(-1) + Hr1(0)*zeta(2)*(-(xm1(2)*xp1(2)*(1 + xpow(2))**2))
    bit(-1) = bit(-1) + Hr3(0,0,0)*(3*xm1(2)*xp1(2)*(1 + xpow(2))**2)
    bit(-1) = bit(-1) + Hr2(0,0)*(2*xm1(2)*xp1(2)*(1 + xpow(2))*(2 + xpow(1) + &
               xpow(2)))
    bit(-1) = bit(-1) + Hr1(0)*(-(xm1(1)*xp1(1)*(7 + 2*xpow(1) + 7*xpow(2)))/2.)
    bit(-1) = bit(-1) + 2
    bit(-1) = bit(-1) + zeta(2)*(xm1(1)*xp1(1)*(1 + xpow(2)))

    ! The alpha^2 e^0 coeff. of F1
    bit(0) = bit(0) + Hr1(1)*zeta(3)*(4*xm1(2)*xp1(2)*(1 + xpow(2))**2)
    bit(0) = bit(0) + Hr4(-1,0,-1,0)*(8*xm1(2)*xp1(2)*(1 + xpow(2))**2)
    bit(0) = bit(0) + Hr4(1,0,1,0)*(8*xm1(2)*xp1(2)*(1 + xpow(2))**2)
    bit(0) = bit(0) + Hr4(-1,-1,0,0)*(16*xm1(2)*xp1(2)*(1 + xpow(2))**2)
    bit(0) = bit(0) + log2*zeta(2)*(-12*xp1(2)*(1 - 4*xpow(1) + xpow(2)))
    bit(0) = bit(0) + Hr1(0)*(-(xm1(1)*xp1(1)*(81 - 10*xpow(1) + 81*xpow(2)))/8.)
    bit(0) = bit(0) + Hr2(1,0)*(8*xm1(1)*xp1(3)*(2 + xpow(1) + 10*xpow(2) + xpow(3) &
               + 2*xpow(4)))
    bit(0) = bit(0) + Hr3(1,0,0)*(2*xp1(4)*(8 + 15*xpow(1) + 194*xpow(2) + &
               15*xpow(3) + 8*xpow(4)))
    bit(0) = bit(0) + Hr2(-1,0)*(-(xm1(1)*xp1(3)*(55 - 48*xpow(1) + 562*xpow(2) - &
               48*xpow(3) + 55*xpow(4)))/2.)
    bit(0) = bit(0) + Hr1(-1)*zeta(2)*(2*xm1(1)*xp1(4)*(-7 + 24*xpow(1) - &
               61*xpow(2) + 53*xpow(3) - 30*xpow(4) + 5*xpow(5)))
    bit(0) = bit(0) + Hr3(0,-1,0)*(2*xm1(1)*xp1(4)*(-6 + 11*xpow(1) - 173*xpow(2) + &
               181*xpow(3) - 5*xpow(4) + 8*xpow(5)))
    bit(0) = bit(0) + zeta(2)*((xm1(1)*xp1(4)*(-41 - 13*xpow(1) + 746*xpow(2) - &
               1070*xpow(3) + 191*xpow(4) + 59*xpow(5)))/4.)
    bit(0) = bit(0) + Hr4(0,1,0,0)*(-4*xm1(1)*xp1(5)*(1 + xpow(1) + 30*xpow(2) - &
               120*xpow(3) + 30*xpow(4) + xpow(5) + xpow(6)))
    bit(0) = bit(0) + Hr1(0)*zeta(3)*(4*xm1(1)*xp1(5)*(2 + 11*xpow(1) - 9*xpow(2) + &
               48*xpow(3) - 9*xpow(4) + 11*xpow(5) + 2*xpow(6)))
    bit(0) = bit(0) + Hr3(-1,0,0)*(-2*xm1(2)*xp1(4)*(5 + 9*xpow(1) + 155*xpow(2) - &
               202*xpow(3) + 153*xpow(4) + 5*xpow(5) + 3*xpow(6)))
    bit(0) = bit(0) + Hr3(0,1,0)*(-2*xm1(2)*xp1(4)*(5 + 4*xpow(1) + 59*xpow(2) - &
               72*xpow(3) + 59*xpow(4) + 4*xpow(5) + 5*xpow(6)))
    bit(0) = bit(0) + zeta(3)*(-(xm1(2)*xp1(4)*(11 + 48*xpow(1) - 159*xpow(2) + &
               280*xpow(3) - 163*xpow(4) + 40*xpow(5) + 7*xpow(6))))
    bit(0) = bit(0) + Hr2(0,0)*((xm1(2)*xp1(4)*(51 + 4*xpow(1) + 251*xpow(2) - &
               968*xpow(3) + 1521*xpow(4) - 60*xpow(5) + 225*xpow(6)))/4.)
    bit(0) = bit(0) + Hr2(1,0)*zeta(2)*(4*xm1(2)*xp1(5)*(1 + 5*xpow(1) - 19*xpow(2) &
               + 53*xpow(3) - 39*xpow(4) + 29*xpow(5) + xpow(6) + xpow(7)))
    bit(0) = bit(0) + Hr4(1,0,0,0)*(8*xm1(2)*xp1(5)*(1 + 4*xpow(1) - 7*xpow(2) + &
               30*xpow(3) - 16*xpow(4) + 17*xpow(5) + 2*xpow(6) + xpow(7)))
    bit(0) = bit(0) + Hr4(0,0,1,0)*(4*xm1(2)*xp1(5)*(-1 - 3*xpow(1) - 5*xpow(2) + &
               49*xpow(3) - 35*xpow(4) + 15*xpow(5) + 9*xpow(6) + 3*xpow(7)))
    bit(0) = bit(0) + Hr3(0,0,0)*(-2*xm1(2)*xp1(5)*(-5 - 10*xpow(1) - 5*xpow(2) + &
               7*xpow(3) + 45*xpow(4) - 94*xpow(5) - 7*xpow(6) + 5*xpow(7)))
    bit(0) = bit(0) + Hr2(0,-1)*zeta(2)*(-2*xm1(2)*xp1(5)*(-7 - 15*xpow(1) - &
               53*xpow(2) + 131*xpow(3) - 145*xpow(4) + 43*xpow(5) + 9*xpow(6) + 5*xpow(7)))
    bit(0) = bit(0) + Hr4(0,-1,0,0)*(-2*xm1(2)*xp1(5)*(3 + 3*xpow(1) + 77*xpow(2) - &
               155*xpow(3) + 225*xpow(4) - 27*xpow(5) + 27*xpow(6) + 7*xpow(7)))
    bit(0) = bit(0) + Hr4(0,0,-1,0)*(-2*xm1(2)*xp1(5)*(-1 + 3*xpow(1) - 49*xpow(2) &
               + 351*xpow(3) - 281*xpow(4) + 99*xpow(5) + 27*xpow(6) + 11*xpow(7)))
    bit(0) = bit(0) + Hr2(0,0)*zeta(2)*(xm1(2)*xp1(5)*(-1 + xpow(1) - 27*xpow(2) + &
               131*xpow(3) - 33*xpow(4) + 97*xpow(5) + 41*xpow(6) + 15*xpow(7)))
    bit(0) = bit(0) + Hr1(0)*zeta(2)*(-(xm1(2)*xp1(5)*(-1 - 4*xpow(1) + 96*xpow(2) &
               + 75*xpow(3) + 151*xpow(4) - 82*xpow(5) + 2*xpow(6) + 19*xpow(7))))
    bit(0) = bit(0) + Hr4(0,0,0,0)*(xm1(2)*xp1(5)*(7 + 19*xpow(1) + 53*xpow(2) + &
               121*xpow(3) + 117*xpow(4) + 117*xpow(5) + 83*xpow(6) + 27*xpow(7)))
    bit(0) = bit(0) + zeta(4)*((xm1(2)*xp1(5)*(-59 - 113*xpow(1) - 745*xpow(2) + &
               2973*xpow(3) - 2119*xpow(4) + 1355*xpow(5) + 479*xpow(6) + 181*xpow(7)))/4.)
    bit(0) = bit(0) + 11.5
    bit(0) = bit(0) + Hr3(-1,-1,0)*(-4*xm1(1)*xp1(1)*(1 + xpow(2)))
    bit(0) = bit(0) + Hr4(-1,0,0,0)*(-12*xm1(2)*xp1(2)*(1 + xpow(2))**2)
    bit(0) = bit(0) + Hr4(1,0,-1,0)*(-8*xm1(2)*xp1(2)*(1 + xpow(2))**2)
    bit(0) = bit(0) + Hr4(0,-1,-1,0)*(4*xm1(2)*xp1(2)*(1 + xpow(2))**2)
    bit(0) = bit(0) + Hr2(-1,0)*zeta(2)*(4*xm1(2)*xp1(2)*(1 + xpow(2))**2)

    !FIXME
    bit(0) = bit(0) - zeta(2)*bit(-2)

    Lbit = 0.
    do i=1,4
      do j=1,i-1
        Lbit(i-3) = Lbit(i-3) + (2*Ls)**j / fac(j) * bit(i-3-j)
      enddo
    enddo
    select case(size(twoloopF1))
      case(3)
        twoloopF1 = bit(-2:0) + Lbit(-2:0)
      case(1)
        twoloopF1 = bit(0:0) + Lbit(0:0)
      case default
        call crash("HQFF")
    end select
  endif

  if (present(twoloopF2)) then
    bit = 0.

    ! The alpha^2 e^-2 coeff. of F2
    bit(-2) = 0

    ! The alpha^2 e^-1 coeff. of F2
    bit(-1) = bit(-1) + Hr1(0)*(-2*xm1(1)*xp1(1)*xpow(1))
    bit(-1) = bit(-1) + Hr2(0,0)*(4*xm1(2)*xp1(2)*xpow(1)*(1 + xpow(2)))

    ! The alpha^2 e^0 coeff. of F2
    bit(0) = bit(0) + Hr2(0,0)*zeta(2)*(-16*xm1(3)*xp1(5)*xpow(2)*(2 - 10*xpow(1) + &
               9*xpow(2) - 10*xpow(3) + 2*xpow(4)))
    bit(0) = bit(0) + Hr4(0,0,-1,0)*(16*xm1(3)*xp1(5)*xpow(2)*(5 - 49*xpow(1) + &
               84*xpow(2) - 49*xpow(3) + 5*xpow(4)))
    bit(0) = bit(0) + Hr1(-1)*zeta(2)*(12*xm1(2)*xp1(4)*xpow(1)*(5 - 12*xpow(1) + &
               26*xpow(2) - 12*xpow(3) + 5*xpow(4)))
    bit(0) = bit(0) + Hr4(0,-1,0,0)*(-16*xm1(3)*xp1(5)*xpow(2)*(7 - 31*xpow(1) + &
               50*xpow(2) - 31*xpow(3) + 7*xpow(4)))
    bit(0) = bit(0) + zeta(4)*(-12*xm1(3)*xp1(5)*xpow(2)*(11 - 77*xpow(1) + &
               109*xpow(2) - 77*xpow(3) + 11*xpow(4)))
    bit(0) = bit(0) + Hr3(0,0,0)*(-4*xm1(2)*xp1(5)*xpow(1)*(-3 - xpow(1) + xpow(2) &
               - 23*xpow(3) + 46*xpow(4) + 8*xpow(5)))
    bit(0) = bit(0) + Hr1(0)*zeta(2)*(-4*xm1(2)*xp1(5)*xpow(1)*(3 - 4*xpow(1) + &
               22*xpow(2) - 2*xpow(3) + 43*xpow(4) + 14*xpow(5)))
    bit(0) = bit(0) + Hr1(0)*((-27*xm1(1)*xp1(1)*xpow(1))/2.)
    bit(0) = bit(0) + log2*zeta(2)*(-48*xp1(2)*xpow(1))
    bit(0) = bit(0) + Hr4(0,1,0,0)*(80*xm1(1)*xp1(5)*xpow(2)*(1 - 7*xpow(1) + &
               xpow(2)))
    bit(0) = bit(0) + Hr3(0,1,0)*(-8*xm1(2)*xp1(4)*xpow(1)*(1 - 6*xpow(1) + &
               xpow(2))*(1 - 4*xpow(1) + xpow(2)))
    bit(0) = bit(0) + Hr2(1,0)*(32*xm1(1)*xp1(3)*xpow(1)*(1 - xpow(1) + xpow(2)))
    bit(0) = bit(0) + Hr4(1,0,0,0)*(-96*xm1(1)*xp1(5)*xpow(2)*(1 - xpow(1) + &
               xpow(2)))
    bit(0) = bit(0) + Hr2(1,0)*zeta(2)*(-96*xm1(1)*xp1(5)*xpow(2)*(1 - xpow(1) + &
               xpow(2)))
    bit(0) = bit(0) + Hr1(0)*zeta(3)*(112*xm1(1)*xp1(5)*xpow(2)*(1 - xpow(1) + &
               xpow(2)))
    bit(0) = bit(0) + Hr2(0,-1)*zeta(2)*(48*xm1(3)*xp1(5)*xpow(2)*(1 - 8*xpow(1) + &
               xpow(2))*(1 - xpow(1) + xpow(2)))
    bit(0) = bit(0) + Hr4(0,0,0,0)*(8*xm1(3)*xp1(5)*xpow(2)*(1 - xpow(1) + &
               xpow(2))*(1 + 4*xpow(1) + xpow(2)))
    bit(0) = bit(0) + Hr2(-1,0)*(-6*xm1(1)*xp1(3)*xpow(1)*(13 - 38*xpow(1) + &
               13*xpow(2)))
    bit(0) = bit(0) + Hr4(0,0,1,0)*(192*xm1(1)*xp1(5)*xpow(3))
    bit(0) = bit(0) + zeta(2)*(xm1(1)*xp1(4)*xpow(1)*(-15 - 173*xpow(1) + &
               323*xpow(2) + xpow(3)))
    bit(0) = bit(0) + Hr2(0,0)*(xm1(1)*xp1(4)*xpow(1)*(-13 + 49*xpow(1) - &
               199*xpow(2) + 123*xpow(3)))
    bit(0) = bit(0) + Hr3(-1,0,0)*(-4*xm1(2)*xp1(4)*xpow(1)*(1 - 64*xpow(1) + &
               122*xpow(2) - 64*xpow(3) + xpow(4)))
    bit(0) = bit(0) + zeta(3)*(4*xm1(2)*xp1(4)*xpow(1)*(1 - 52*xpow(1) + 62*xpow(2) &
               - 52*xpow(3) + xpow(4)))
    bit(0) = bit(0) + Hr3(0,-1,0)*(8*xm1(2)*xp1(4)*xpow(1)*(1 - 49*xpow(1) + &
               92*xpow(2) - 49*xpow(3) + xpow(4)))
    bit(0) = bit(0) + Hr3(1,0,0)*(-8*xm1(2)*xp1(4)*xpow(1)*(1 + 47*xpow(1) - &
               88*xpow(2) + 47*xpow(3) + xpow(4)))

    Lbit = 0.
    do i=1,4
      do j=1,i-1
        Lbit(i-3) = Lbit(i-3) + (2*Ls)**j / fac(j) * bit(i-3-j)
      enddo
    enddo
    select case(size(twoloopF2))
      case(3)
        twoloopF2 = bit(-2:0) + Lbit(-2:0)
      case(1)
        twoloopF2 = bit(0:0) + Lbit(0:0)
      case default
        call crash("HQFF")
    end select
  endif

  if (present(twoloopG1)) then
    bit = 0.

    ! The alpha^2 e^-2 coeff. of G1
    bit(-2) = bit(-2) + Hr2(0,0)*(xm1(2)*xp1(2)*(1 + xpow(2))**2)
    bit(-2) = bit(-2) + 0.5
    bit(-2) = bit(-2) + Hr1(0)*(-(xm1(1)*xp1(1)*(1 + xpow(2))))

    ! The alpha^2 e^-1 coeff. of G1
    bit(-1) = bit(-1) + Hr2(-1,0)*(2*xm1(1)*xp1(1)*(1 + xpow(2)))
    bit(-1) = bit(-1) + Hr3(-1,0,0)*(-4*xm1(2)*xp1(2)*(1 + xpow(2))**2)
    bit(-1) = bit(-1) + Hr3(0,-1,0)*(-2*xm1(2)*xp1(2)*(1 + xpow(2))**2)
    bit(-1) = bit(-1) + Hr1(0)*zeta(2)*(-(xm1(2)*xp1(2)*(1 + xpow(2))**2))
    bit(-1) = bit(-1) + Hr3(0,0,0)*(3*xm1(2)*xp1(2)*(1 + xpow(2))**2)
    bit(-1) = bit(-1) + Hr2(0,0)*(2*xm1(2)*xp1(2)*(1 + xpow(2))*(2 - xpow(1) + &
               xpow(2)))
    bit(-1) = bit(-1) + Hr1(0)*(-(xm1(1)*xp1(1)*(7 - 2*xpow(1) + 7*xpow(2)))/2.)
    bit(-1) = bit(-1) + 2
    bit(-1) = bit(-1) + zeta(2)*(xm1(1)*xp1(1)*(1 + xpow(2)))

    ! The alpha^2 e^0 coeff. of G1
    bit(0) = bit(0) + Hr1(1)*zeta(3)*(4*xm1(2)*xp1(2)*(1 + xpow(2))**2)
    bit(0) = bit(0) + Hr4(-1,0,-1,0)*(8*xm1(2)*xp1(2)*(1 + xpow(2))**2)
    bit(0) = bit(0) + Hr4(1,0,1,0)*(8*xm1(2)*xp1(2)*(1 + xpow(2))**2)
    bit(0) = bit(0) + Hr4(-1,-1,0,0)*(16*xm1(2)*xp1(2)*(1 + xpow(2))**2)
    bit(0) = bit(0) + log2*zeta(2)*(-12*xp1(2)*(1 - 4*xpow(1) + xpow(2)))
    bit(0) = bit(0) + Hr2(1,0)*(8*xm1(1)*xp1(1)*(2 - xpow(1) + 2*xpow(2)))
    bit(0) = bit(0) + Hr2(-1,0)*(-(xm1(1)*xp1(1)*(55 - 82*xpow(1) + &
               55*xpow(2)))/2.)
    bit(0) = bit(0) + Hr1(0)*(-(xm1(1)*xp1(1)*(81 - 14*xpow(1) + 81*xpow(2)))/8.)
    bit(0) = bit(0) + Hr3(-1,0,0)*(-2*xm1(2)*xp1(2)*(5 - 15*xpow(1) + 76*xpow(2) - &
               15*xpow(3) + 3*xpow(4)))
    bit(0) = bit(0) + Hr1(-1)*zeta(2)*(2*xm1(2)*xp1(2)*(7 - 27*xpow(1) + 36*xpow(2) &
               - 27*xpow(3) + 5*xpow(4)))
    bit(0) = bit(0) + Hr3(0,1,0)*(-2*xm1(2)*xp1(2)*(5 - 10*xpow(1) + 18*xpow(2) - &
               10*xpow(3) + 5*xpow(4)))
    bit(0) = bit(0) + zeta(3)*(-(xm1(2)*xp1(2)*(11 + 30*xpow(1) - 142*xpow(2) + &
               30*xpow(3) + 7*xpow(4))))
    bit(0) = bit(0) + Hr3(0,-1,0)*(2*xm1(2)*xp1(2)*(6 - 27*xpow(1) + 60*xpow(2) - &
               27*xpow(3) + 8*xpow(4)))
    bit(0) = bit(0) + Hr3(1,0,0)*(2*xm1(2)*xp1(2)*(8 - 19*xpow(1) + 46*xpow(2) - &
               19*xpow(3) + 8*xpow(4)))
    bit(0) = bit(0) + zeta(2)*((xm1(2)*xp1(2)*(41 - 146*xpow(1) - 4*xpow(2) - &
               46*xpow(3) + 59*xpow(4)))/4.)
    bit(0) = bit(0) + Hr2(0,0)*((xm1(2)*xp1(2)*(51 - 78*xpow(1) + 236*xpow(2) - &
               274*xpow(3) + 225*xpow(4)))/4.)
    bit(0) = bit(0) + Hr2(1,0)*zeta(2)*(4*xm1(2)*xp1(3)*(1 + 7*xpow(1) - 30*xpow(2) &
               + 34*xpow(3) - 5*xpow(4) + xpow(5)))
    bit(0) = bit(0) + Hr4(1,0,0,0)*(8*xm1(2)*xp1(3)*(1 + 4*xpow(1) - 14*xpow(2) + &
               18*xpow(3) - 2*xpow(4) + xpow(5)))
    bit(0) = bit(0) + Hr4(0,0,1,0)*(4*xm1(2)*xp1(3)*(-1 - xpow(1) - 2*xpow(2) + &
               6*xpow(3) + 3*xpow(4) + 3*xpow(5)))
    bit(0) = bit(0) + Hr4(0,1,0,0)*(-4*xm1(3)*xp1(3)*(1 - xpow(1) + 14*xpow(2) - &
               20*xpow(3) + 14*xpow(4) - xpow(5) + xpow(6)))
    bit(0) = bit(0) + Hr1(0)*zeta(3)*(4*xm1(3)*xp1(3)*(2 + 5*xpow(1) - 33*xpow(2) + &
               48*xpow(3) - 33*xpow(4) + 5*xpow(5) + 2*xpow(6)))
    bit(0) = bit(0) + Hr3(0,0,0)*(-2*xm1(3)*xp1(3)*(5 - 7*xpow(1) - 16*xpow(2) + &
               59*xpow(3) - 60*xpow(4) - 10*xpow(5) + 5*xpow(6)))
    bit(0) = bit(0) + Hr2(0,-1)*zeta(2)*(-2*xm1(3)*xp1(3)*(7 + 6*xpow(1) - &
               29*xpow(2) + 24*xpow(3) - 31*xpow(4) + 6*xpow(5) + 5*xpow(6)))
    bit(0) = bit(0) + Hr4(0,-1,0,0)*(-2*xm1(3)*xp1(3)*(-3 + 10*xpow(1) - 75*xpow(2) &
               + 104*xpow(3) - 65*xpow(4) + 10*xpow(5) + 7*xpow(6)))
    bit(0) = bit(0) + Hr4(0,0,-1,0)*(-2*xm1(3)*xp1(3)*(1 - 2*xpow(1) + 23*xpow(2) - &
               56*xpow(3) + 33*xpow(4) - 2*xpow(5) + 11*xpow(6)))
    bit(0) = bit(0) + Hr2(0,0)*zeta(2)*(xm1(3)*xp1(3)*(1 - 4*xpow(1) + 19*xpow(2) - &
               80*xpow(3) + 33*xpow(4) - 4*xpow(5) + 15*xpow(6)))
    bit(0) = bit(0) + Hr1(0)*zeta(2)*(-(xm1(3)*xp1(3)*(1 + 3*xpow(1) - 79*xpow(2) + &
               118*xpow(3) - 73*xpow(4) - 37*xpow(5) + 19*xpow(6))))
    bit(0) = bit(0) + Hr4(0,0,0,0)*(xm1(3)*xp1(3)*(-7 - 2*xpow(1) - 7*xpow(2) - &
               56*xpow(3) + 27*xpow(4) - 2*xpow(5) + 27*xpow(6)))
    bit(0) = bit(0) + zeta(4)*((xm1(3)*xp1(3)*(59 - 72*xpow(1) + 477*xpow(2) - &
               1328*xpow(3) + 599*xpow(4) - 72*xpow(5) + 181*xpow(6)))/4.)
    bit(0) = bit(0) + 11.5
    bit(0) = bit(0) + Hr3(-1,-1,0)*(-4*xm1(1)*xp1(1)*(1 + xpow(2)))
    bit(0) = bit(0) + Hr4(-1,0,0,0)*(-12*xm1(2)*xp1(2)*(1 + xpow(2))**2)
    bit(0) = bit(0) + Hr4(1,0,-1,0)*(-8*xm1(2)*xp1(2)*(1 + xpow(2))**2)
    bit(0) = bit(0) + Hr4(0,-1,-1,0)*(4*xm1(2)*xp1(2)*(1 + xpow(2))**2)
    bit(0) = bit(0) + Hr2(-1,0)*zeta(2)*(4*xm1(2)*xp1(2)*(1 + xpow(2))**2)

    !FIXME
    bit(0) = bit(0) - zeta(2)*bit(-2)

    Lbit = 0.
    do i=1,4
      do j=1,i-1
        Lbit(i-3) = Lbit(i-3) + (2*Ls)**j / fac(j) * bit(i-3-j)
      enddo
    enddo
    select case(size(twoloopG1))
      case(3)
        twoloopG1 = bit(-2:0) + Lbit(-2:0)
      case(1)
        twoloopG1 = bit(0:0) + Lbit(0:0)
      case default
        call crash("HQFF")
    end select
  endif

  if (present(twoloopG2)) then
    bit = 0.

    ! The alpha^2 e^-2 coeff. of G2
    bit(-2) = 0

    ! The alpha^2 e^-1 coeff. of G2
    bit(-1) = bit(-1) + Hr1(0)*(-2*xm1(3)*xp1(1)*xpow(1)*(5 - 2*xpow(1) + &
               5*xpow(2)))
    bit(-1) = bit(-1) + 4*xm1(2)*xpow(1)
    bit(-1) = bit(-1) + Hr2(0,0)*(4*xm1(4)*xp1(2)*xpow(1)*(1 + xpow(2))*(3 - &
               2*xpow(1) + 3*xpow(2)))

    ! The alpha^2 e^0 coeff. of G2
    bit(0) = bit(0) + Hr3(0,1,0)*(-8*xm1(4)*xp1(2)*xpow(1)*(3 - 6*xpow(1) + &
               14*xpow(2) - 6*xpow(3) + 3*xpow(4)))
    bit(0) = bit(0) + Hr3(-1,0,0)*(-4*xm1(4)*xp1(2)*xpow(1)*(15 - 16*xpow(1) + &
               198*xpow(2) - 16*xpow(3) + 15*xpow(4)))
    bit(0) = bit(0) + zeta(2)*(xm1(4)*xp1(2)*xpow(1)*(33 - 102*xpow(1) - &
               272*xpow(2) + 6*xpow(3) + 47*xpow(4)))
    bit(0) = bit(0) + Hr2(0,0)*(xm1(4)*xp1(2)*xpow(1)*(47 - 18*xpow(1) + &
               320*xpow(2) - 222*xpow(3) + 129*xpow(4)))
    bit(0) = bit(0) + Hr3(0,0,0)*(4*xm1(5)*xp1(3)*xpow(1)*(-9 + 6*xpow(1) + &
               14*xpow(2) - 56*xpow(3) + 175*xpow(4) + 14*xpow(5)))
    bit(0) = bit(0) + Hr1(0)*zeta(2)*(-4*xm1(5)*xp1(3)*xpow(1)*(-3 + 7*xpow(1) - &
               60*xpow(2) + 56*xpow(3) - 129*xpow(4) - 27*xpow(5) + 12*xpow(6)))
    bit(0) = bit(0) + 21*xm1(2)*xpow(1)
    bit(0) = bit(0) + log2*zeta(2)*(-48*xp1(2)*xpow(1))
    bit(0) = bit(0) + Hr4(1,0,0,0)*(-32*xm1(3)*xp1(3)*xpow(2)*(1 - 17*xpow(1) + &
               xpow(2)))
    bit(0) = bit(0) + Hr2(1,0)*zeta(2)*(-32*xm1(3)*xp1(3)*xpow(2)*(1 - 17*xpow(1) + &
               xpow(2)))
    bit(0) = bit(0) + Hr2(0,-1)*zeta(2)*(-48*xm1(5)*xp1(3)*xpow(2)*(1 + xpow(2))*(1 &
               - 5*xpow(1) + xpow(2)))
    bit(0) = bit(0) + Hr1(-1)*zeta(2)*(36*xm1(4)*xp1(2)*xpow(1)*(1 + xpow(2))*(1 - &
               4*xpow(1) + xpow(2)))
    bit(0) = bit(0) + Hr2(1,0)*(32*xm1(3)*xp1(1)*xpow(1)*(1 - xpow(1) + xpow(2)))
    bit(0) = bit(0) + Hr2(-1,0)*(-2*xm1(3)*xp1(1)*xpow(1)*(25 - 86*xpow(1) + &
               25*xpow(2)))
    bit(0) = bit(0) + Hr1(0)*(-(xm1(3)*xp1(1)*xpow(1)*(73 - 14*xpow(1) + &
               73*xpow(2)))/2.)
    bit(0) = bit(0) + Hr4(0,0,1,0)*(64*xm1(3)*xp1(3)*xpow(3))
    bit(0) = bit(0) + Hr2(0,0)*zeta(2)*(48*xm1(5)*xp1(3)*(2 - 9*xpow(1) + &
               2*xpow(2))*xpow(3))
    bit(0) = bit(0) + zeta(4)*(-4*xm1(5)*xp1(3)*xpow(2)*(1 - 145*xpow(1) + &
               405*xpow(2) - 145*xpow(3) + xpow(4)))
    bit(0) = bit(0) + Hr4(0,-1,0,0)*(-16*xm1(5)*xp1(3)*xpow(2)*(1 - 37*xpow(1) + &
               54*xpow(2) - 37*xpow(3) + xpow(4)))
    bit(0) = bit(0) + Hr1(0)*zeta(3)*(16*xm1(5)*xp1(3)*xpow(2)*(1 - 33*xpow(1) + &
               52*xpow(2) - 33*xpow(3) + xpow(4)))
    bit(0) = bit(0) + Hr4(0,0,0,0)*(-8*xm1(5)*xp1(3)*xpow(2)*(1 - 5*xpow(1) + &
               38*xpow(2) - 5*xpow(3) + xpow(4)))
    bit(0) = bit(0) + Hr4(0,0,-1,0)*(-16*xm1(5)*xp1(3)*xpow(2)*(1 + 19*xpow(1) - &
               28*xpow(2) + 19*xpow(3) + xpow(4)))
    bit(0) = bit(0) + Hr4(0,1,0,0)*(-16*xm1(5)*xp1(3)*xpow(2)*(1 + 19*xpow(1) - &
               16*xpow(2) + 19*xpow(3) + xpow(4)))
    bit(0) = bit(0) + zeta(3)*(4*xm1(4)*xp1(2)*xpow(1)*(3 - 16*xpow(1) + &
               170*xpow(2) - 16*xpow(3) + 3*xpow(4)))
    bit(0) = bit(0) + Hr3(0,-1,0)*(8*xm1(4)*xp1(2)*xpow(1)*(3 - 13*xpow(1) + &
               64*xpow(2) - 13*xpow(3) + 3*xpow(4)))
    bit(0) = bit(0) + Hr3(1,0,0)*(8*xm1(4)*xp1(2)*xpow(1)*(3 - 7*xpow(1) + &
               64*xpow(2) - 7*xpow(3) + 3*xpow(4)))

    Lbit = 0.
    do i=1,4
      do j=1,i-1
        Lbit(i-3) = Lbit(i-3) + (2*Ls)**j / fac(j) * bit(i-3-j)
      enddo
    enddo
    select case(size(twoloopG2))
      case(3)
        twoloopG2 = bit(-2:0) + Lbit(-2:0)
      case(1)
        twoloopG2 = bit(0:0) + Lbit(0:0)
      case default
        call crash("HQFF")
    end select
  endif


  END SUBROUTINE HQFF

                          !!!!!!!!!!!!!!!!!
                      END MODULE ff_heavyquark
                          !!!!!!!!!!!!!!!!!
