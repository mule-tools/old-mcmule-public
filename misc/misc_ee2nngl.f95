
                          !!!!!!!!!!!!!!!!!!!!!!
                              MODULE misc_EE2NNGL
                          !!!!!!!!!!!!!!!!!!!!!!
    use global_def
    use functions
    implicit none
    real(kind=prec) :: m2, y, tscms, xi, ls
    real(kind=prec), parameter :: ln2 = 0.693147180559945309417232121458

  contains

function rvbub(b)
    implicit none
    real rvbub
    real b
    real tmp12, tmp15, tmp19, tmp10, tmp16
    real tmp55, tmp47, tmp49, tmp62, tmp53
    real tmp56, tmp88, tmp4, tmp64, tmp65
    real tmp67, tmp118, tmp147, tmp90, tmp39
    real tmp128, tmp129, tmp52, tmp7, tmp8

    tmp12 = xi**2
    tmp15 = b**4
    tmp19 = b**2
    tmp10 = b**6
    tmp16 = 13*xi
    tmp55 = xi**3
    tmp47 = y**2
    tmp49 = b**3
    tmp62 = 2*xi
    tmp53 = y**3
    tmp56 = y**4
    tmp88 = xi**4
    tmp4 = b*y
    tmp64 = 2*b*xi*y
    tmp65 = -1 + tmp19 + tmp62 + tmp64
    tmp67 = b**8
    tmp118 = 4*xi
    tmp147 = 3*tmp12
    tmp90 = 19*xi
    tmp39 = 2*tmp12
    tmp128 = b**5
    tmp129 = y**5
    tmp52 = -1 + tmp19
    tmp7 = b*xi*y
    tmp8 = tmp7 + xi
    rvbub = (-3*ls*(6*tmp10 + tmp15*(-24 + tmp16) + tmp12*tmp49*tmp52*tmp53 + tmp15*tmp55*tm&
           &p56 + tmp19*(18 - 7*tmp12 - 9*xi) + tmp12*(3 + xi) - tmp19*tmp47*xi*(6 + 3*tmp15&
           & + tmp39 + tmp19*(-13 + xi) + 3*xi) + b*(18 + tmp12 - 2*tmp15*(-3 + xi) - 18*xi &
           &+ tmp19*(-24 - 9*tmp12 + 28*xi))*y) + (2*tmp12*(-9 + 7*tmp12 + tmp16) + tmp12*tm&
           &p49*tmp53*(55 + tmp118 - 28*tmp12 + 19*tmp15 - 6*tmp19*(11 + tmp62)) - 18*tmp67 &
           &+ 2*tmp128*tmp129*tmp88 + tmp19*(18 + 76*tmp12 - 34*tmp55 - 63*xi) - 2*tmp15*(27&
           & + 25*tmp12 - 55*xi) + tmp10*(54 - 47*xi) - 2*tmp15*tmp55*tmp56*(-3 + 3*tmp19 + &
           &5*xi) + tmp19*tmp47*xi*(-72 + 9*tmp10 + 16*tmp12 - 4*tmp55 + tmp19*(137 - 40*tmp&
           &12 - 124*xi) + 126*xi + tmp15*(-74 + 22*xi)) + b*(18 + 53*tmp12 + 44*tmp55 + 26*&
           &tmp88 - 2*tmp10*(9 + tmp90) - 72*xi + tmp15*(54 - 47*tmp12 + 36*xi) + tmp19*(-54&
           & + 18*tmp12 - 68*tmp55 + 74*xi))*y)/tmp65 - (12*(1 + tmp4)*xi*(-(tmp12*(5 + tmp1&
           &18 + 2*tmp15 - 11*tmp19 + tmp39)*tmp49*tmp53) + tmp12*(-1 + tmp12 + tmp62) + 2*t&
           &mp67 + tmp128*tmp129*tmp88 - tmp19*(6 + tmp147 + 6*tmp55 - 17*xi) + 2*tmp15*(7 +&
           & 4*tmp12 - 13*xi) - tmp19*tmp47*(-16 + tmp10 + 2*tmp55 + 2*tmp15*(-5 + tmp62) + &
           &tmp90 + tmp19*(25 + 12*tmp12 - 35*xi))*xi + tmp15*tmp55*tmp56*(-2 + 2*tmp19 + xi&
           &) + tmp10*(-10 + 9*xi) + b*(-6 + tmp10*(2 + tmp118) - 15*tmp12 + 4*tmp55 + tmp88&
           & + tmp19*(14 + 21*tmp12 - 16*tmp55 - 20*xi) + 2*tmp15*(-5 + tmp147 - 2*xi) + 20*&
           &xi)*y)*Log(-tmp52/(2.*tmp8)))/tmp65**2)/(9.*(-1 + tmp4)*tmp8**3)

end function rvbub

function rvtrin(b)
    implicit none
    integer, parameter :: cc0  = 1
    real rvtrin
    real(kind=prec) :: ca
    complex(kind=prec) :: pvc
    real b
    real tmp5, tmp12, tmp16, tmp6, tmp7
    real tmp8, tmp9, tmp34, tmp10, tmp56
    real tmp66, tmp47, tmp25, tmp27, tmp28
    real tmp124, tmp77, tmp17, tmp13, tmp39
    real tmp38, tmp23, tmp103, tmp135, tmp19
    real tmp222, tmp211, tmp212, tmp189, tmp129
    real tmp131, tmp160, tmp133, tmp136, tmp153
    real tmp154, tmp139, tmp140, tmp141, tmp142
    real tmp143, tmp150, tmp99, tmp247, tmp287
    real tmp313, tmp274, tmp215, tmp298, tmp299
    real tmp300, tmp35, tmp36, tmp147, tmp79
    real tmp102, tmp348, tmp67, tmp330, tmp308
    real tmp310, tmp184, tmp380, tmp293, tmp295

    tmp5 = b*y
    tmp12 = b**2
    tmp16 = xi**2
    tmp6 = -1 + tmp5
    tmp7 = 1/tmp6
    tmp8 = 1 + tmp5
    tmp9 = tmp8**(-2)
    tmp34 = 2*xi
    tmp10 = b**4
    tmp56 = b**6
    tmp66 = xi**3
    tmp47 = xi**4
    tmp25 = y**2
    tmp27 = b**3
    tmp28 = y**3
    tmp124 = -50*xi
    tmp77 = -7*xi
    tmp17 = -2 + xi
    tmp13 = -4*xi
    tmp39 = -1 + xi
    tmp38 = b**8
    tmp23 = -xi
    tmp103 = -41*tmp16
    tmp135 = xi**5
    tmp19 = -5*xi
    tmp222 = 36*tmp47
    tmp211 = 3*tmp16
    tmp212 = 2 + tmp19 + tmp211
    tmp189 = 16*xi
    tmp129 = y**4
    tmp131 = b**5
    tmp160 = -39*tmp16
    tmp133 = y**5
    tmp136 = y**6
    tmp153 = xi**6
    tmp154 = 4*xi
    tmp139 = -2*b*tmp17*xi*y
    tmp140 = tmp16*tmp25
    tmp141 = 4 + tmp13 + tmp140
    tmp142 = tmp12*tmp141
    tmp143 = tmp139 + tmp142 + tmp16
    tmp150 = tmp39**2
    tmp99 = 6*xi
    tmp247 = 9*tmp66
    tmp287 = 3*xi
    tmp313 = 9*tmp16
    tmp274 = -4*tmp10*tmp39
    tmp215 = 15*tmp16
    tmp298 = tmp143**(-2)
    tmp299 = -(tscms*xi)
    tmp300 = tscms + tmp299
    tmp35 = 2*b*xi*y
    tmp36 = -1 + tmp12 + tmp34 + tmp35
    tmp147 = b**10
    tmp79 = 28*tmp66
    tmp102 = 36*xi
    tmp348 = 17*tmp16
    tmp67 = 31*tmp66
    tmp330 = -2 + tmp287
    tmp308 = 5*xi
    tmp310 = -4 + tmp308
    tmp184 = 20*tmp66
    tmp380 = 80*xi
    tmp293 = b**7
    tmp295 = y**7

    call C0_cll(pvc, &
        cmplx(m2),cmplx(tmp300),cmplx(m2 - 0.5*tscms*tmp8*xi)  ,  &
        cmplx(0.),cmplx(m2),cmplx(m2) )
    ca = real(pvc)

    rvtrin = (6*ls*tmp7*tmp9*(-2*tmp10 + tmp12*(6 + tmp13) + tmp16 + tmp16*tmp27*tmp28 + tmp1&
            &2*(-1 + tmp12 + tmp23)*tmp25*xi + b*(6 - tmp16 + tmp12*tmp17 + tmp19)*y) - (2*tm&
            &p7*tmp9*(8*tmp135*tmp136*tmp56 + tmp10*tmp129*(43 + tmp10 + tmp124 + tmp12*(44 +&
            & tmp124) + 4*tmp16)*tmp66 + 4*tmp131*tmp133*tmp47*(11 + tmp77) - tmp12*tmp25*(72&
            & + tmp10*(8 + tmp102 + tmp103) + 147*tmp16 + 32*tmp47 - 60*tmp66 + tmp56*(-4 + t&
            &mp99) - 2*tmp12*(102 - 99*tmp16 + 70*tmp66 - 91*xi) - 224*xi)*xi + tmp16*tmp27*t&
            &mp28*(106 - 120*tmp16 + 56*tmp66 + tmp12*(20 + 32*tmp16 - 82*xi) - 47*xi + tmp10&
            &*(2 + xi)) + 2*(4*tmp38*tmp39 + 5*(-1 + tmp34)*tmp47 + tmp56*(40 + 27*tmp16 - 68&
            &*xi) + tmp12*tmp16*(-29 - 45*tmp16 + 77*xi) + tmp10*(-36 - 126*tmp16 + tmp67 + 1&
            &28*xi)) - b*(tmp16*(18 - 76*tmp16 + tmp77 + tmp79) + 2*tmp12*(36 + 22*tmp16 + 16&
            &*tmp47 + 23*tmp66 - 94*xi) + 4*tmp56*(2 + 2*tmp16 - 3*xi) - tmp10*(80 - 314*tmp1&
            &6 + 167*tmp66 + 56*xi))*y))/(tmp143*tmp36) + 3*tscms*tmp298*tmp7*tmp9*(64*tmp147*&
            &tmp150 + tmp153*(-5 + tmp154) + tmp153*(-5 + tmp12 + tmp154)*tmp293*tmp295 - tmp&
            &16*tmp27*tmp28*(192 + 20*tmp135 + 692*tmp16 + 7*tmp47 + 8*tmp212*tmp56 - 300*tmp&
            &66 + tmp12*(416 + 504*tmp16 - 27*tmp47 - 48*tmp66 - 856*xi) - 4*tmp10*(28 - 15*t&
            &mp16 + tmp247 - 24*xi) - 608*xi) - 16*tmp56*(12 + 89*tmp16 + 12*tmp47 - 57*tmp66&
            & - 56*xi) - 16*tmp38*(8 + 7*tmp16 + tmp66 - 16*xi) + tmp135*tmp136*tmp56*(-42 - &
            &20*tmp16 + tmp12*(2 + tmp287) + 57*xi) + tmp12*tmp47*(-44 + tmp160 + 86*xi) + 2*&
            &tmp10*tmp16*(-96 - 250*tmp16 + 73*tmp66 + 272*xi) + tmp131*tmp133*tmp47*(-112 - &
            &157*tmp16 + tmp274 + 36*tmp66 + 224*xi + tmp12*(-52 + tmp160 + 100*xi)) + tmp10*&
            &tmp129*tmp66*(-192 - 432*tmp16 + tmp10*(16 - 34*tmp16 + tmp189) - 20*tmp47 + 8*t&
            &mp39*tmp56 + 153*tmp66 + 484*xi + tmp12*(-168 - 246*tmp16 + 67*tmp66 + 356*xi)) &
            &+ tmp12*tmp25*xi*(-16*tmp212*tmp38 + 8*tmp56*(32 + tmp215 + 7*tmp66 - 54*xi) + 4&
            &*tmp10*(-88 + 44*tmp16 + tmp222 - 131*tmp66 + 136*xi) + tmp16*(-96 - 38*tmp16 + &
            &tmp222 - 77*tmp66 + 172*xi) + tmp12*(-384 - 159*tmp135 - 1240*tmp16 + 414*tmp47 &
            &+ 168*tmp66 + 1216*xi)) + b*(8*tmp56*(-16 + 26*tmp16 + tmp189 + 11*tmp47 - 37*tm&
            &p66) + 32*tmp38*(2 - 3*tmp16 + tmp23 + 2*tmp66) - 8*tmp10*(24 + 36*tmp135 - 30*t&
            &mp16 - 129*tmp47 + 152*tmp66 - 52*xi) - tmp47*(12 + tmp103 + tmp184 + 12*xi) + t&
            &mp12*tmp16*(-96 + 332*tmp16 + 139*tmp47 - 404*tmp66 + 40*xi))*y)*         Ca    &
            &                               + 12*tmp298*tmp39*tmp7*tmp9*(-3*tmp135 + 32*tmp38*&
            &tmp39 - 4*tmp131*tmp133*(-7 + tmp287)*tmp47 + 3*tmp135*tmp136*tmp56 + 4*tmp12*tm&
            &p310*tmp66 + 8*tmp56*(16 + tmp313 - 26*xi) - 64*tmp10*tmp150*xi + tmp12*tmp25*(-&
            &8*tmp330*tmp56 + 3*tmp16*(4 - 5*tmp16 + tmp99) + 2*tmp12*(128 + 8*tmp16 + 19*tmp&
            &66 - 164*xi) - 4*tmp10*(32 + tmp313 - 38*xi))*xi + tmp10*tmp129*tmp66*(84 + 4*tm&
            &p10 + tmp215 - 66*xi - 10*tmp12*xi) + 2*tmp16*tmp27*tmp28*(64 + tmp274 + tmp348 &
            &- 64*xi + tmp12*(-16 + tmp287)*xi) + 2*b*(8*(-2 + tmp16)*tmp56 + tmp47*(-7 + tmp&
            &99) + 4*tmp10*(16 - 14*tmp16 + tmp247 - 12*xi) + tmp12*tmp16*(-20 - 27*tmp16 + 4&
            &8*xi))*y)*DiscB(tmp300,Sqrt(m2)) - (12*tmp298*xi*(16*tmp147*tmp212 + tmp153*(11 &
            &+ tmp12 + tmp154)*tmp293*tmp295 + tmp12*tmp66*(-16 - 132*tmp16 + tmp380 + 65*tmp&
            &66) + 16*tmp56*(10 + 80*tmp16 + 9*tmp47 - 50*tmp66 - 49*xi) + tmp135*(-3 - 10*tm&
            &p16 + 11*xi) + 4*tmp38*(-48 - 184*tmp16 + 63*tmp66 + 168*xi) + tmp131*tmp133*tmp&
            &47*(-70 - 137*tmp16 + tmp184 + tmp12*(-58 - 59*tmp16 + tmp380) + 2*tmp56 + tmp10&
            &*(-2 + tmp99) + 282*xi) + tmp10*xi*(-64 - 780*tmp16 - 193*tmp47 + 672*tmp66 + 36&
            &8*xi) + tmp135*tmp136*tmp56*(-5 + 5*tmp10 - 28*tmp16 + 85*xi - tmp12*(8 + xi)) -&
            & tmp10*tmp129*tmp66*(292 + 73*tmp16 - 40*tmp47 + 6*tmp310*tmp56 + 127*tmp66 + tm&
            &p10*(204 + 91*tmp16 - 282*xi) - 542*xi + tmp12*(-216 + 148*tmp16 - 35*tmp66 + 15&
            &4*xi)) - tmp16*tmp27*tmp28*(-160 + 52*tmp135 - 962*tmp16 + 4*tmp330*tmp38 - 209*&
            &tmp47 + 532*tmp66 + tmp12*(936 + 1550*tmp16 - 147*tmp47 + 96*tmp66 - 2396*xi) + &
            &2*tmp56*(68 + 61*tmp16 - 114*xi) + 712*xi + 2*tmp10*(-292 - 355*tmp16 + 54*tmp66&
            & + 598*xi)) + tmp12*tmp25*xi*(4*(-4 + tmp13 + 5*tmp16)*tmp38 + tmp12*(320 - 99*t&
            &mp135 + 2256*tmp16 + 416*tmp47 - 1414*tmp66 - 1504*xi) + tmp56*(288 + 48*tmp16 +&
            & 30*tmp66 - 400*xi) + tmp16*(28 + tmp348 - 4*tmp47 + tmp67 - 78*xi) + tmp10*(-84&
            &8 - 1520*tmp16 + 215*tmp47 - 74*tmp66 + 2240*xi)) + b*(4*tmp56*(-48 + tmp154 + 3&
            &18*tmp16 + 122*tmp47 - 399*tmp66) + 4*tmp38*(8 + tmp102 - 102*tmp16 + 55*tmp66) &
            &+ 16*tmp147*tmp39*xi + tmp47*(-12 - 83*tmp16 + tmp79 + 58*xi) + tmp12*tmp16*(-24&
            & - 488*tmp16 - 89*tmp47 + 400*tmp66 + 188*xi) - 2*tmp10*(-80 + 45*tmp135 - 372*t&
            &mp16 - 70*tmp47 + 142*tmp66 + 328*xi))*y + 2*tmp38*xi**7*y**8)*Log(-(-1 + tmp12)&
            &/(2.*(xi + b*xi*y))))/((-1 + tmp12*tmp25)*tmp36**2))/(18.*xi**2)

end function rvtrin

function rvtrig(b)
    implicit none
    integer, parameter :: cc0  = 1
    real rvtrig
    real b
    real tmp5, tmp8, tmp12, tmp16, tmp10
    real tmp25, tmp27, tmp28, tmp64, tmp17
    real tmp34, tmp35, tmp36, tmp38, tmp81
    real tmp110, tmp70, tmp83, tmp116, tmp87
    real tmp89, tmp90, tmp91, tmp92, tmp93
    real tmp94, tmp162, tmp163, tmp164, tmp165
    real tmp166

    tmp5 = b*y
    tmp8 = 1 + tmp5
    tmp12 = b**2
    tmp16 = xi**2
    tmp10 = b**4
    tmp25 = y**2
    tmp27 = b**3
    tmp28 = y**3
    tmp64 = xi**3
    tmp17 = -2 + xi
    tmp34 = 2*xi
    tmp35 = 2*b*xi*y
    tmp36 = -1 + tmp12 + tmp34 + tmp35
    tmp38 = b**6
    tmp81 = -1 + xi
    tmp110 = 7*tmp64
    tmp70 = 8*tmp16
    tmp83 = y**4
    tmp116 = xi**4
    tmp87 = -1 + tmp12
    tmp89 = -(b*xi*y)
    tmp90 = tmp27*tmp28*xi
    tmp91 = tmp17*tmp25
    tmp92 = 2 + tmp91
    tmp93 = -(tmp12*tmp92)
    tmp94 = tmp89 + tmp90 + tmp93 + xi
    tmp162 = b*xi*y
    tmp163 = tmp162 + xi
    tmp164 = 1/tmp163
    tmp165 = -(tmp164*tmp87)/2.
    tmp166 = Log(tmp165)
    rvtrig = -(Pi**2*tmp87*tmp94 - 6*ls*tmp8*(-2*tmp10 + tmp16 + tmp16*tmp27*tmp28 + tmp12*(6&
            & - 4*xi) + tmp12*tmp25*(-1 + tmp12 - xi)*xi + b*(6 - tmp16 + tmp12*tmp17 - 5*xi)&
            &*y) + (2*tmp8*(-12*tmp16*tmp27*tmp28*tmp81 + 8*tmp10*tmp64*tmp83 + tmp12*tmp25*x&
            &i*(7 + tmp10 - 28*tmp16 + 10*xi - 6*tmp12*xi) - 2*(tmp38 + tmp12*(9 + 5*tmp16 - &
            &17*xi) + 5*tmp16*(1 - 2*xi) + tmp10*(-10 + 13*xi)) + b*(-18 - 12*tmp16 + 12*tmp6&
            &4 + 35*xi + tmp10*(-2 + 3*xi) - 2*tmp12*(-10 + tmp70 + 11*xi))*y))/tmp36 + (12*t&
            &mp166*tmp8*(-4*b**8 - 20*tmp38*tmp81 + tmp10*(7 + tmp12 + tmp34)*tmp64*tmp83 + t&
            &mp12*(12 + tmp110 + 14*tmp16 - 36*xi) + tmp16*(3 + 2*tmp16 - 7*xi) + tmp16*tmp27&
            &*tmp28*(1 + 5*tmp10 - 4*tmp16 + 2*tmp12*(-7 + xi) + 22*xi) + tmp10*(-28 - 25*tmp&
            &16 + 56*xi) + tmp12*tmp25*xi*(-30 + 16*tmp16 + 2*tmp38 - 4*tmp64 + tmp12*(46 + t&
            &mp70 - 58*xi) + 27*xi + tmp10*(-18 + 7*xi)) + b*(12 + 2*tmp116 + 29*tmp16 - 4*(1&
            & + tmp34)*tmp38 - 6*tmp64 - 40*xi + tmp10*(20 - 23*tmp16 + 8*xi) + 2*tmp12*(-14 &
            &+ tmp110 - 15*tmp16 + 20*xi))*y + 2*b**5*tmp116*y**5))/tmp36**2 + 3*tmp87*tmp94*&
            &(tmp166**2 + 2*Li2((tmp164*tmp36)/2.)))/(18.*(-1 + tmp5)*tmp8**3*xi**2)

end function rvtrig

function rvbox(b)
    implicit none
    integer, parameter :: cc0  = 1
    real(kind=prec) :: cA, cB, cC
    complex(kind=prec) :: pvc
    real(kind=prec) :: realMusq
    real rvbox
    real b
    real tmp10, tmp7, tmp8, tmp9, tmp6
    real tmp26, tmp19, tmp25, tmp23, tmp17
    real tmp33, tmp49, tmp52, tmp53, tmp34
    real tmp75, tmp76, tmp86, tmp94, tmp103
    real tmp68, tmp69, tmp38, tmp39, tmp46
    real tmp24, tmp73, tmp80, tmp144, tmp92
    real tmp167, tmp168, tmp174, tmp100, tmp158
    real tmp163, tmp164, tmp81, tmp74, tmp77
    real tmp78, tmp79, tmp11, tmp12, tmp13
    real tmp28, tmp245, tmp197, tmp212, tmp198
    real tmp16, tmp30, tmp160
    real tmp161
    realMusq = musq

    tmp10 = b**2
    tmp7 = b*y
    tmp8 = -1 + tmp7
    tmp9 = 1/tmp8
    tmp6 = b**3
    tmp26 = xi**2
    tmp19 = y**2
    tmp25 = -3*xi
    tmp23 = b**4
    tmp17 = -1 + xi
    tmp33 = -xi
    tmp49 = tmp10*tmp19
    tmp52 = -1 + tmp49
    tmp53 = 1/tmp52
    tmp34 = tmp19*tmp26
    tmp75 = -4*xi
    tmp76 = 4 + tmp34 + tmp75
    tmp86 = xi**3
    tmp94 = y**4
    tmp103 = xi**4
    tmp68 = -(tscms*xi)
    tmp69 = tscms + tmp68
    tmp38 = 1 + tmp7
    tmp39 = -(tscms*tmp38*xi)/2.
    tmp46 = m2 + tmp39
    tmp24 = -2*tmp23
    tmp73 = -2 + xi
    tmp80 = -5*xi
    tmp144 = tmp38**(-2)
    tmp92 = b**6
    tmp167 = -5*tmp26
    tmp168 = 5*xi
    tmp174 = 4*tmp26
    tmp100 = b**5
    tmp158 = y**3
    tmp163 = Sqrt(m2)
    tmp164 = DiscB(tscms,tmp163)
    tmp81 = 3*tmp26
    tmp74 = -2*b*tmp73*xi*y
    tmp77 = tmp10*tmp76
    tmp78 = tmp26 + tmp74 + tmp77
    tmp79 = 1/tmp78
    tmp11 = 2*xi
    tmp12 = 2*b*xi*y
    tmp13 = -1 + tmp10 + tmp11 + tmp12
    tmp28 = -2*xi
    tmp245 = 7*tmp86
    tmp197 = -6*xi
    tmp212 = 6*tmp26
    tmp198 = 5*tmp26
    tmp16 = -1 + tmp10
    tmp30 = 5 + tmp28
    tmp160 = tmp158*tmp26*tmp6
    tmp161 = -m2 / tmp39

    !C0(0,m2,tmp46,m2,m2,0)
    ca = pi**2/6 + 0.5 * log(tmp161)**2 + Li2(1-tmp161)
    ca = ca / tmp39

    !C0(0,tscms,tmp69,m2,m2,m2)
    !if (b**2 > xi) then
    !  cb = log((1-b)/(1+b))**2 - log((1 + b**2 - 2*Sqrt((b**2 - xi)*(1 - xi)) - 2*xi)/(1 - b**2))**2
    !else
    !  cb = log((1-b)/(1+b))**2 - pi**2 - atan2(2*Sqrt((1-xi)*(xi-b**2)), 2*xi-1.-b**2)**2
    !endif
    !cb = cb * 0.5/tscms/xi
    call C0_cll(pvc, &
        cmplx(0.),cmplx(tscms),cmplx(tmp69)  ,  &
        cmplx(m2),cmplx(m2),cmplx(m2) )
    cb = real(pvc)


    !C0(m2,tmp69,tmp46,0,m2,m2)
    call C0_cll(pvc, &
        cmplx(m2),cmplx(tmp69),cmplx(tmp46)  ,  &
        cmplx(0.),cmplx(m2),cmplx(m2) )
    cc = real(pvc)

    rvbox = -((12*tmp6*tmp9*xi*(2*b*xi - 2*b*tmp19*xi + tmp16*tmp17*y))/tmp13 + 6*ls*(1 + tm&
           &p10)*tmp144*tmp164*tmp9*(tmp160 + tmp24 + tmp26 + tmp10*(6 + tmp75) + tmp10*tmp1&
           &9*(-1 + tmp10 + tmp33)*xi + b*(6 - tmp26 + tmp10*tmp73 + tmp80)*y) - 2*tmp144*tm&
           &p164*tmp9*(2*tmp158*(tmp100*(3 + tmp197 + tmp198) - tmp26*tmp6) - 2*(tmp167 + tm&
           &p10*(-9 + tmp167 + tmp168) + 5*tmp17*tmp23 + tmp92) - tmp10*tmp19*(tmp10*(42 + t&
           &mp174 - 36*xi) + tmp23*(-12 + 11*xi) + xi*(1 + 16*xi)) - b*(-18 + tmp174 + (-4 +&
           & tmp168)*tmp23 + tmp10*(32 + tmp174 - 38*xi) + 17*xi)*y) + 3*tscms*tmp10*tmp9*xi*&
           &(6 + tmp24 + tmp25 + tmp26 + tmp10*(4 + tmp33 + tmp34) + b*tmp30*xi*y - tmp6*xi*&
           &y)*     ca                - 6*tscms*tmp10*tmp53*xi*(6 + (-1 + tmp17*tmp19)*tmp23 &
           &+ tmp25 + tmp26 + tmp10*(3 + tmp33 + tmp19*(1 + tmp26 + tmp33)) - 2*tmp6*xi*y - &
           &2*b*(-3 + xi)*xi*y)*     cb                    + 3*tscms*tmp10*tmp53*tmp79*xi*(tmp&
           &26*(6 + tmp80 + tmp81) + tmp23*(56 + (6 - 8*tmp19)*tmp26 + tmp19*(-2 + 7*tmp19)*&
           &tmp86 - tmp103*tmp94 + 16*(-4 + tmp19)*xi) - tmp92*(16 - 2*tmp19*tmp26 + tmp86*t&
           &mp94 + 8*(-2 + tmp19)*xi) + tmp10*(24 + 14*tmp103*tmp19 - 6*(-8 + tmp19)*tmp26 -&
           & (13 + 34*tmp19)*tmp86 + 8*(-7 + 6*tmp19)*xi) - 2*b**7*tmp76*y + 2*tmp6*(24 - 3*&
           &tmp103*tmp19 + (-30 + 9*tmp19)*tmp26 + 2*(5 + tmp19)*tmp86)*y + tmp100*(24 + 2*(&
           &9 + 2*tmp19)*tmp26 - 4*tmp19*tmp86 + tmp103*tmp94 - 48*xi)*y + b*(24 + 28*tmp26 &
           &- 11*tmp86 - 26*xi)*xi*y)*         cc                + 12*tmp10*tmp17*tmp53*tmp7&
           &9*(4*tmp23 - 2*tmp86 + tmp17*tmp23*tmp26*tmp94 + 5*tmp10*(-4 + xi)*xi - tmp158*t&
           &mp6*xi*(4 + tmp81 + 2*(-7 + tmp10)*xi) + tmp10*tmp19*(-(tmp10*(4 + tmp26)) + (32&
           & + tmp26 - 15*xi)*xi) + b*(8*tmp17*tmp23 + 3*tmp26*tmp73 + tmp10*(32 + tmp212 - &
           &40*xi))*y)*DiscB(tmp69,tmp163) + (24*tmp10*tmp79*tmp9*xi*(4*b**8*tmp17 + (1 + tm&
           &p28)**2*tmp86 + tmp92*(24 + 20*tmp26 - 46*xi) + tmp23*tmp86*tmp94*(-13 + 2*tmp10&
           &*(-6 + tmp168) + tmp23 + 8*tmp26 - 28*xi) - 2*tmp10*xi*(-5 + tmp245 - 28*tmp26 +&
           & 22*xi) - 2*tmp158*tmp26*tmp6*(-27 + (-3 + tmp168)*tmp23 - 20*tmp26 + 2*tmp86 + &
           &2*tmp10*(17 + tmp26 - 19*xi) + 53*xi) + tmp23*(-20 + tmp245 - 48*tmp26 + 64*xi) &
           &- 2*tmp10*tmp19*xi*(10 + 5*tmp103 + 56*tmp26 - 32*tmp86 + (-1 + tmp11)*tmp92 + 2&
           &*tmp23*(10 + tmp198 - 13*xi) - 42*xi + tmp10*(-45 - 94*tmp26 + 14*tmp86 + 110*xi&
           &)) + 2*b*(tmp26*(1 + tmp212 + tmp80 + tmp86) + tmp23*(12 - 11*tmp26 + tmp75 + 3*&
           &tmp86) + (-2 + tmp197 + tmp212)*tmp92 + tmp10*(-10 - 15*tmp103 - 72*tmp26 + 70*t&
           &mp86 + 42*xi))*y + 2*tmp100*tmp103*(-10 + tmp10 + xi)*y**5 - 2*tmp92*xi**5*y**6)&
           &*Log(-tmp16/(2.*(xi + b*xi*y))))/(tmp13**2*tmp38) + 6*tscms*tmp53*tmp6*xi*(tmp19*&
           &tmp6*xi + b*(-5 + tmp10 + xi) + (-1 - 5*tmp10 + 2*tmp23 + tmp28)*y)*ScalarC0IR6(&
           &tscms,tmp163) - 3*tscms**2*tmp10*tmp53*xi*(tmp160 + tmp26 + tmp23*(4 + tmp80) - 2*&
           &tmp92 + tmp10*(6 + xi) + tmp10*tmp19*xi*(-tmp23 + tmp10*tmp30 + xi) + b*(6 + tmp&
           &26 + tmp75 - 2*tmp23*(1 + xi) + tmp10*(4 - 2*tmp26 + 6*xi))*y)*ScalarD0IR16(0.,t&
           &mp69,tmp46,tscms,tmp163,tmp163,tmp163))/(18.*b**2*xi**2)

   musq = realMusq
end function rvbox

function rvren(b)
    implicit none
    real rvren
    real b
    real tmp6, tmp12, tmp7, tmp17, tmp18
    real tmp27, tmp23, tmp24, tmp20, tmp11
    real tmp25, tmp28

    tmp6 = b*y
    tmp12 = b**2
    tmp7 = -1 + tmp6
    tmp17 = -(tscms*tmp12)
    tmp18 = tscms + tmp17
    tmp27 = y**2
    tmp23 = tmp7**2
    tmp24 = -4*xi
    tmp20 = b*xi*y
    tmp11 = tscms**2
    tmp25 = xi**2
    tmp28 = tmp12*tmp27
    rvren = -((tmp11*(4*tscms*(-1 + tmp12)**2*(-1 + xi) + tmp18*tmp7*(-2 + xi)*(2 + tmp20 + x&
           &i) - tscms*tmp23*xi*(4 + tmp24 + tmp25*(3 + tmp28 + 4*b*y))) + (2*tmp18**3 + tscms&
           &**3*tmp23*xi*(4 + tmp24 + (tmp20 + xi)**2) + tscms*tmp18**2*(2 - 7*xi + tmp12*tmp&
           &27*xi + 2*b*(1 + xi)*y) + tmp11*tmp18*tmp7*(4 + tmp25*(-3 + tmp28 - 2*b*y) + xi*&
           &(-3 + 5*b*y)))*(1 + 2*ln2 + Log(Musq) - Log(tmp18)))/(tscms**3*(1 + tmp6)*tmp7**3&
           &*xi**3))

end function rvren

function rvpole(b)
    implicit none
    real rvpole
    real b
    real tmp6, tmp11, tmp17, tmp12, tmp24
    real tmp39, tmp20, tmp7, tmp9, tmp13
    real tmp14, tmp15, tmp16, tmp47, tmp49
    real tmp53, tmp68

    tmp6 = b*y
    tmp11 = b**2
    tmp17 = xi**2
    tmp12 = b**4
    tmp24 = xi**3
    tmp39 = 4*xi
    tmp20 = b**6
    tmp7 = -1 + tmp6
    tmp9 = 1 + tmp6
    tmp13 = -2*tmp12
    tmp14 = -4*xi
    tmp15 = 6 + tmp14
    tmp16 = tmp11*tmp15
    tmp47 = -tmp17
    tmp49 = y**2
    tmp53 = b**3
    tmp68 = y**3
    rvpole = -(2*tmp11*((tmp13 + tmp16 + tmp17)*xi + tmp11*tmp49*(-6 + 2*tmp12 + tmp39 + tmp4&
            &7 + 4*tmp11*(-1 + xi))*xi + 2*tmp53*tmp68*(9 - 6*tmp17 - 4*tmp24 + tmp11*(-12 + &
            &6*tmp17 - 11*xi) + 6*xi + tmp12*(3 + 5*xi)) - 2*b*(-9 - 5*tmp17 + 6*tmp20 - 2*tm&
            &p24 + 5*tmp11*(6 + tmp17 - 7*xi) + 18*xi + tmp12*(-27 + 17*xi))*y - tmp12*(-6 + &
            &2*tmp11 + tmp17 + tmp39)*xi*y**4 - 2*b**5*tmp17*(-1 + tmp11 - 2*xi)*y**5 + tmp20&
            &*tmp24*y**6) + (1 + tmp11)*tmp7**2*tmp9*xi*(tmp13 + tmp16 + tmp17 + tmp17*tmp53*&
            &tmp68 + tmp11*tmp49*(-1 + tmp11 - xi)*xi + b*(6 + tmp47 + tmp11*(-2 + xi) - 5*xi&
            &)*y)*DiscB(tscms,Sqrt(m2)))/(3.*b**2*tmp7**3*tmp9**3*xi**3)

end function rvpole



                          !!!!!!!!!!!!!!!!!!!!!!
                            END MODULE misc_EE2NNGL
                          !!!!!!!!!!!!!!!!!!!!!!
