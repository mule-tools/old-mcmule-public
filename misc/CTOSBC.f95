
                          !!!!!!!!!!!!!!!!!!!!!!
                              MODULE rad_CTOSBC
                          !!!!!!!!!!!!!!!!!!!!!!
  contains
      function CTOSBC(EL,GF,MOU2,MIN2,p1p2,p1p4,p2p4,asym,np2,np3, & 
         np4,mu2)
!
      use global_def, only: pi, prec
      implicit none
!

    complex(kind=prec) A0, B0i
    complex(kind=prec) B0
    complex(kind=prec) DB0
    complex(kind=prec) C0, C0i
    complex(kind=prec) D0, D0i

    integer, parameter :: bb0  = 1
    integer, parameter :: bb1  = 4
    integer, parameter :: cc0  = 1
    integer, parameter :: cc1  = 4
    integer, parameter :: cc2  = 7
    integer, parameter :: cc00 = 10
    integer, parameter :: cc11 = 13
    integer, parameter :: cc12 = 16
    integer, parameter :: cc22 = 19
    integer, parameter :: dd0  = 1
    integer, parameter :: dd1  = 4
    integer, parameter :: dd2  = 7
    integer, parameter :: dd3  = 10
    integer, parameter :: dd00 = 13
    
    

    external A0, B0i
    external B0
    external DB0
    external C0, C0i
    external D0, D0i
    
    complex(kind=prec) :: Acache(2)
    complex(kind=prec) :: Bcache(2)
    complex(kind=prec) :: DBcache(2)

!
      real(kind=prec) CTOSBC,EL,GF,MOU2,MIN2, & 
         p1p2,p1p4,p2p4,asym, & 
         np2,np3,np4,mu2, & 
         aa,bb
!
      aa=0._prec
      bb=-1._prec
!

!    
      call setlambda(-1._prec)
      call setdelta(0._prec)
      call setmudim(mu2)
      call setminmass(0._prec)
!     
    Acache(1) = A0(MIN2)
    Acache(2) = A0(MOU2)
    Bcache(1) = B0i(bb0,MIN2,0._prec,MIN2)
    Bcache(2) = B0i(bb0,MOU2,0._prec,MOU2)
    DBcache(1) = DB0(MIN2,0._prec,MIN2)
    DBcache(2) = DB0(MOU2,0._prec,MOU2)

!
      CTOSBC=real((EL**4*GF**2*(-42*CMPLX(aa,bb)* & 
         asym*MIN2**2.5*p1p4**2 +  & 
           42*CMPLX(aa,bb)*asym*MIN2**1.5*MOU2*p1p4**2 +  & 
           36*MIN2**3.5*np4*p1p4**2 -  & 
           72*MIN2**2.5*MOU2*np4*p1p4**2 +  & 
           36*MIN2**1.5*MOU2**2*np4*p1p4**2 +  & 
           59*CMPLX(aa,bb)*asym*MIN2**1.5*p1p2*p1p4**2 -  & 
           40*MIN2**3*p1p2*p1p4**2 +  & 
           40*MIN2**2*MOU2*p1p2*p1p4**2 -  & 
           72*MIN2**2.5*np2*p1p2*p1p4**2 +  & 
           72*MIN2**1.5*MOU2*np2*p1p2*p1p4**2 -  & 
           92*MIN2**2.5*np4*p1p2*p1p4**2 +  & 
           92*MIN2**1.5*MOU2*np4*p1p2*p1p4**2 +  & 
           152*MIN2**2*p1p2**2*p1p4**2 -  & 
           72*MIN2*MOU2*p1p2**2*p1p4**2 +  & 
           112*MIN2**1.5*np2*p1p2**2*p1p4**2 +  & 
           56*MIN2**1.5*np4*p1p2**2*p1p4**2 -  & 
           112*MIN2*p1p2**3*p1p4**2 +  & 
           35*CMPLX(aa,bb)*asym*MIN2**1.5*p1p4**3 & 
         + 20*MIN2**3*p1p4**3 -  & 
           10*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p4**3 -  & 
           20*MIN2*MOU2**2*p1p4**3 +  & 
           36*MIN2**2.5*np2*p1p4**3 -  & 
           36*sqrt(MIN2)*MOU2**2*np2*p1p4**3 -  & 
           44*MIN2**2.5*np4*p1p4**3 +  & 
           44*MIN2**1.5*MOU2*np4*p1p4**3 -  & 
           29*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p1p4**3 -  & 
           32*MIN2**2*p1p2*p1p4**3 -  & 
           84*MIN2*MOU2*p1p2*p1p4**3 +  & 
           36*MOU2**2*p1p2*p1p4**3 +  & 
           32*MIN2**1.5*np2*p1p2*p1p4**3 -  & 
           128*sqrt(MIN2)*MOU2*np2*p1p2*p1p4**3 +  & 
           44*MIN2**1.5*np4*p1p2*p1p4**3 +  & 
           28*sqrt(MIN2)*MOU2*np4*p1p2*p1p4**3 -  & 
           136*MIN2*p1p2**2*p1p4**3 +  & 
           128*MOU2*p1p2**2*p1p4**3 -  & 
           112*sqrt(MIN2)*np2*p1p2**2*p1p4**3 +  & 
           56*sqrt(MIN2)*np4*p1p2**2*p1p4**3 +  & 
           112*p1p2**3*p1p4**3 + 9*CMPLX(aa,bb)*asym* & 
         sqrt(MIN2)*p1p4**4 +  & 
           44*MIN2**2*p1p4**4 - 44*MIN2*MOU2*p1p4**4 +  & 
           12*MIN2**1.5*np2*p1p4**4 -  & 
           28*sqrt(MIN2)*MOU2*np2*p1p4**4 +  & 
           56*MIN2**1.5*np4*p1p4**4 -  & 
           56*sqrt(MIN2)*MOU2*np4*p1p4**4 -  & 
           56*MIN2*p1p2*p1p4**4 -  & 
           56*sqrt(MIN2)*np2*p1p2*p1p4**4 -  & 
           112*sqrt(MIN2)*np4*p1p2*p1p4**4 -  & 
           56*MIN2*p1p4**5 + 56*MOU2*p1p4**5 +  & 
           112*p1p2*p1p4**5 + 54*CMPLX(aa,bb)*asym*MIN2**2.5* & 
         p1p4*p2p4 +  & 
           40*MIN2**4*p1p4*p2p4 -  & 
           54*CMPLX(aa,bb)*asym*MIN2**1.5*MOU2*p1p4*p2p4 -  & 
           40*MIN2**3*MOU2*p1p4*p2p4 +  & 
           72*MIN2**3.5*np2*p1p4*p2p4 -  & 
           72*MIN2**2.5*MOU2*np2*p1p4*p2p4 -  & 
           108*MIN2**3.5*np4*p1p4*p2p4 +  & 
           144*MIN2**2.5*MOU2*np4*p1p4*p2p4 -  & 
           36*MIN2**1.5*MOU2**2*np4*p1p4*p2p4 -  & 
           71*CMPLX(aa,bb)*asym*MIN2**1.5*p1p2*p1p4*p2p4 -  & 
           112*MIN2**3*p1p2*p1p4*p2p4 +  & 
           32*MIN2**2*MOU2*p1p2*p1p4*p2p4 -  & 
           40*MIN2**2.5*np2*p1p2*p1p4*p2p4 -  & 
           72*MIN2**1.5*MOU2*np2*p1p2*p1p4*p2p4 +  & 
           276*MIN2**2.5*np4*p1p2*p1p4*p2p4 -  & 
           164*MIN2**1.5*MOU2*np4*p1p2*p1p4*p2p4 -  & 
           40*MIN2**2*p1p2**2*p1p4*p2p4 +  & 
           72*MIN2*MOU2*p1p2**2*p1p4*p2p4 -  & 
           112*MIN2**1.5*np2*p1p2**2*p1p4*p2p4 -  & 
           168*MIN2**1.5*np4*p1p2**2*p1p4*p2p4 +  & 
           112*MIN2*p1p2**3*p1p4*p2p4 -  & 
           100*CMPLX(aa,bb)*asym*MIN2**1.5*p1p4**2*p2p4 -  & 
           64*MIN2**3*p1p4**2*p2p4 +  & 
           13*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p4**2*p2p4 +  & 
           64*MIN2**2*MOU2*p1p4**2*p2p4 -  & 
           124*MIN2**2.5*np2*p1p4**2*p2p4 +  & 
           108*MIN2**1.5*MOU2*np2*p1p4**2*p2p4 +  & 
           298*MIN2**2.5*np4*p1p4**2*p2p4 -  & 
           318*MIN2**1.5*MOU2*np4*p1p4**2*p2p4 +  & 
           36*sqrt(MIN2)*MOU2**2*np4*p1p4**2*p2p4 +  & 
           29*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p1p4**2*p2p4 +  & 
           72*MIN2**2*p1p2*p1p4**2*p2p4 +  & 
           8*MIN2*MOU2*p1p2*p1p4**2*p2p4 -  & 
           32*MIN2**1.5*np2*p1p2*p1p4**2*p2p4 +  & 
           72*sqrt(MIN2)*MOU2*np2*p1p2*p1p4**2*p2p4 -  & 
           438*MIN2**1.5*np4*p1p2*p1p4**2*p2p4 +  & 
           46*sqrt(MIN2)*MOU2*np4*p1p2*p1p4**2*p2p4 +  & 
           248*MIN2*p1p2**2*p1p4**2*p2p4 -  & 
           72*MOU2*p1p2**2*p1p4**2*p2p4 +  & 
           112*sqrt(MIN2)*np2*p1p2**2*p1p4**2*p2p4 -  & 
           24*sqrt(MIN2)*np4*p1p2**2*p1p4**2*p2p4 -  & 
           112*p1p2**3*p1p4**2*p2p4 +  & 
           14*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4**3*p2p4 -  & 
           58*MIN2**2*p1p4**3*p2p4 +  & 
           174*MIN2*MOU2*p1p4**3*p2p4 -  & 
           36*MOU2**2*p1p4**3*p2p4 -  & 
           10*MIN2**1.5*np2*p1p4**3*p2p4 +  & 
           138*sqrt(MIN2)*MOU2*np2*p1p4**3*p2p4 -  & 
           196*MIN2**1.5*np4*p1p4**3*p2p4 +  & 
           112*sqrt(MIN2)*MOU2*np4*p1p4**3*p2p4 +  & 
           328*MIN2*p1p2*p1p4**3*p2p4 -  & 
           184*MOU2*p1p2*p1p4**3*p2p4 +  & 
           248*sqrt(MIN2)*np2*p1p2*p1p4**3*p2p4 +  & 
           168*sqrt(MIN2)*np4*p1p2*p1p4**3*p2p4 -  & 
           224*p1p2**2*p1p4**3*p2p4 +  & 
           140*MIN2*p1p4**4*p2p4 - 112*MOU2*p1p4**4*p2p4 +  & 
           56*sqrt(MIN2)*np2*p1p4**4*p2p4 +  & 
           112*sqrt(MIN2)*np4*p1p4**4*p2p4 -  & 
           224*p1p2*p1p4**4*p2p4 - 112*p1p4**5*p2p4 -  & 
           12*CMPLX(aa,bb)*asym*MIN2**2.5*p2p4**2 & 
         - 40*MIN2**4*p2p4**2 +  & 
           12*CMPLX(aa,bb)*asym*MIN2**1.5*MOU2*p2p4**2 +  & 
           40*MIN2**3*MOU2*p2p4**2 -  & 
           72*MIN2**3.5*np2*p2p4**2 +  & 
           72*MIN2**2.5*MOU2*np2*p2p4**2 +  & 
           72*MIN2**3.5*np4*p2p4**2 -  & 
           72*MIN2**2.5*MOU2*np4*p2p4**2 +  & 
           12*CMPLX(aa,bb)*asym*MIN2**1.5*p1p2*p2p4**2 +  & 
           152*MIN2**3*p1p2*p2p4**2 -  & 
           72*MIN2**2*MOU2*p1p2*p2p4**2 +  & 
           112*MIN2**2.5*np2*p1p2*p2p4**2 -  & 
           184*MIN2**2.5*np4*p1p2*p2p4**2 +  & 
           72*MIN2**1.5*MOU2*np4*p1p2*p2p4**2 -  & 
           112*MIN2**2*p1p2**2*p2p4**2 +  & 
           112*MIN2**1.5*np4*p1p2**2*p2p4**2 +  & 
           77*CMPLX(aa,bb)*asym*MIN2**1.5*p1p4*p2p4**2 +  & 
           196*MIN2**3*p1p4*p2p4**2 -  & 
           3*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p4*p2p4**2 -  & 
           116*MIN2**2*MOU2*p1p4*p2p4**2 +  & 
           200*MIN2**2.5*np2*p1p4*p2p4**2 -  & 
           72*MIN2**1.5*MOU2*np2*p1p4*p2p4**2 -  & 
           474*MIN2**2.5*np4*p1p4*p2p4**2 +  & 
           346*MIN2**1.5*MOU2*np4*p1p4*p2p4**2 -  & 
           264*MIN2**2*p1p2*p1p4*p2p4**2 +  & 
           666*MIN2**1.5*np4*p1p2*p1p4*p2p4**2 -  & 
           18*sqrt(MIN2)*MOU2*np4*p1p2*p1p4*p2p4**2 -  & 
           112*MIN2*p1p2**2*p1p4*p2p4**2 -  & 
           32*sqrt(MIN2)*np4*p1p2**2*p1p4*p2p4**2 -  & 
           23*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4**2*p2p4**2 -  & 
           152*MIN2**2*p1p4**2*p2p4**2 -  & 
           36*MIN2*MOU2*p1p4**2*p2p4**2 -  & 
           114*MIN2**1.5*np2*p1p4**2*p2p4**2 -  & 
           54*sqrt(MIN2)*MOU2*np2*p1p4**2*p2p4**2 +  & 
           526*MIN2**1.5*np4*p1p4**2*p2p4**2 -  & 
           166*sqrt(MIN2)*MOU2*np4*p1p4**2*p2p4**2 -  & 
           240*MIN2*p1p2*p1p4**2*p2p4**2 +  & 
           72*MOU2*p1p2*p1p4**2*p2p4**2 -  & 
           192*sqrt(MIN2)*np2*p1p2*p1p4**2*p2p4**2 -  & 
           168*sqrt(MIN2)*np4*p1p2*p1p4**2*p2p4**2 +  & 
           224*p1p2**2*p1p4**2*p2p4**2 -  & 
           218*MIN2*p1p4**3*p2p4**2 +  & 
           166*MOU2*p1p4**3*p2p4**2 -  & 
           136*sqrt(MIN2)*np2*p1p4**3*p2p4**2 -  & 
           224*sqrt(MIN2)*np4*p1p4**3*p2p4**2 +  & 
           304*p1p2*p1p4**3*p2p4**2 + 224*p1p4**4*p2p4**2 -  & 
           12*CMPLX(aa,bb)*asym*MIN2**1.5*p2p4**3 & 
         - 152*MIN2**3*p2p4**3 +  & 
           72*MIN2**2*MOU2*p2p4**3 -  & 
           112*MIN2**2.5*np2*p2p4**3 +  & 
           220*MIN2**2.5*np4*p2p4**3 -  & 
           108*MIN2**1.5*MOU2*np4*p2p4**3 +  & 
           224*MIN2**2*p1p2*p2p4**3 -  & 
           272*MIN2**1.5*np4*p1p2*p2p4**3 +  & 
           278*MIN2**2*p1p4*p2p4**3 -  & 
           18*MIN2*MOU2*p1p4*p2p4**3 +  & 
           112*MIN2**1.5*np2*p1p4*p2p4**3 -  & 
           546*MIN2**1.5*np4*p1p4*p2p4**3 +  & 
           54*sqrt(MIN2)*MOU2*np4*p1p4*p2p4**3 -  & 
           32*MIN2*p1p2*p1p4*p2p4**3 +  & 
           112*sqrt(MIN2)*np4*p1p2*p1p4*p2p4**3 +  & 
           102*MIN2*p1p4**2*p2p4**3 -  & 
           54*MOU2*p1p4**2*p2p4**3 +  & 
           80*sqrt(MIN2)*np2*p1p4**2*p2p4**3 +  & 
           192*sqrt(MIN2)*np4*p1p4**2*p2p4**3 -  & 
           192*p1p2*p1p4**2*p2p4**3 - 192*p1p4**3*p2p4**3 -  & 
           112*MIN2**2*p2p4**4 + 160*MIN2**1.5*np4*p2p4**4 +  & 
           32*MIN2*p1p4*p2p4**4 -  & 
           80*sqrt(MIN2)*np4*p1p4*p2p4**4 +  & 
           80*p1p4**2*p2p4**4)*Acache(1))/ & 
       (18.*MIN2*p1p4**3*(p1p4 - p2p4)**2*Pi**2) -  & 
      (EL**4*GF**2*(42*CMPLX(aa,bb)*asym*MIN2**1.5*MOU2*p1p4**2 +  & 
           40*MIN2**3*MOU2*p1p4**2 -  & 
           42*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2**2*p1p4**2 -  & 
           80*MIN2**2*MOU2**2*p1p4**2 +  & 
           40*MIN2*MOU2**3*p1p4**2 +  & 
           72*MIN2**2.5*MOU2*np2*p1p4**2 -  & 
           144*MIN2**1.5*MOU2**2*np2*p1p4**2 +  & 
           72*sqrt(MIN2)*MOU2**3*np2*p1p4**2 -  & 
           36*MIN2**2.5*MOU2*np4*p1p4**2 +  & 
           72*MIN2**1.5*MOU2**2*np4*p1p4**2 -  & 
           36*sqrt(MIN2)*MOU2**3*np4*p1p4**2 -  & 
           68*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p2*p1p4**2 -  & 
           192*MIN2**2*MOU2*p1p2*p1p4**2 +  & 
           264*MIN2*MOU2**2*p1p2*p1p4**2 -  & 
           72*MOU2**3*p1p2*p1p4**2 -  & 
           184*MIN2**1.5*MOU2*np2*p1p2*p1p4**2 +  & 
           184*sqrt(MIN2)*MOU2**2*np2*p1p2*p1p4**2 +  & 
           92*MIN2**1.5*MOU2*np4*p1p2*p1p4**2 -  & 
           92*sqrt(MIN2)*MOU2**2*np4*p1p2*p1p4**2 +  & 
           264*MIN2*MOU2*p1p2**2*p1p4**2 -  & 
           184*MOU2**2*p1p2**2*p1p4**2 +  & 
           112*sqrt(MIN2)*MOU2*np2*p1p2**2*p1p4**2 -  & 
           56*sqrt(MIN2)*MOU2*np4*p1p2**2*p1p4**2 -  & 
           112*MOU2*p1p2**3*p1p4**2 + 20*MIN2**3*p1p4**3 -  & 
           16*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p4**3 -  & 
           44*MIN2**2*MOU2*p1p4**3 -  & 
           12*MIN2*MOU2**2*p1p4**3 + 36*MOU2**3*p1p4**3 +  & 
           36*MIN2**2.5*np2*p1p4**3 -  & 
           160*MIN2**1.5*MOU2*np2*p1p4**3 +  & 
           124*sqrt(MIN2)*MOU2**2*np2*p1p4**3 -  & 
           12*MIN2**1.5*MOU2*np4*p1p4**3 +  & 
           12*sqrt(MIN2)*MOU2**2*np4*p1p4**3 -  & 
           3*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p1p4**3 -  & 
           116*MIN2**2*p1p2*p1p4**3 +  & 
           228*MIN2*MOU2*p1p2*p1p4**3 -  & 
           32*MOU2**2*p1p2*p1p4**3 -  & 
           128*MIN2**1.5*np2*p1p2*p1p4**3 +  & 
           256*sqrt(MIN2)*MOU2*np2*p1p2*p1p4**3 +  & 
           40*sqrt(MIN2)*MOU2*np4*p1p2*p1p4**3 +  & 
           208*MIN2*p1p2**2*p1p4**3 -  & 
           200*MOU2*p1p2**2*p1p4**3 +  & 
           112*sqrt(MIN2)*np2*p1p2**2*p1p4**3 -  & 
           112*p1p2**3*p1p4**3 + 3*CMPLX(aa,bb)*asym* & 
         sqrt(MIN2)*p1p4**4 +  & 
           12*MIN2*MOU2*p1p4**4 - 12*MOU2**2*p1p4**4 +  & 
           16*sqrt(MIN2)*MOU2*np2*p1p4**4 -  & 
           56*MIN2**1.5*np4*p1p4**4 +  & 
           56*sqrt(MIN2)*MOU2*np4*p1p4**4 -  & 
           56*MOU2*p1p2*p1p4**4 +  & 
           112*sqrt(MIN2)*np4*p1p2*p1p4**4 +  & 
           56*MIN2*p1p4**5 - 56*MOU2*p1p4**5 -  & 
           112*p1p2*p1p4**5 -  & 
           42*CMPLX(aa,bb)*asym*MIN2**1.5*MOU2*p1p4*p2p4 -  & 
           40*MIN2**3*MOU2*p1p4*p2p4 +  & 
           42*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2**2*p1p4*p2p4 +  & 
           40*MIN2**2*MOU2**2*p1p4*p2p4 -  & 
           72*MIN2**2.5*MOU2*np2*p1p4*p2p4 +  & 
           72*MIN2**1.5*MOU2**2*np2*p1p4*p2p4 +  & 
           36*MIN2**2.5*MOU2*np4*p1p4*p2p4 -  & 
           36*sqrt(MIN2)*MOU2**3*np4*p1p4*p2p4 +  & 
           68*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p2*p1p4*p2p4 +  & 
           192*MIN2**2*MOU2*p1p2*p1p4*p2p4 -  & 
           112*MIN2*MOU2**2*p1p2*p1p4*p2p4 +  & 
           184*MIN2**1.5*MOU2*np2*p1p2*p1p4*p2p4 -  & 
           72*sqrt(MIN2)*MOU2**2*np2*p1p2*p1p4*p2p4 -  & 
           92*MIN2**1.5*MOU2*np4*p1p2*p1p4*p2p4 -  & 
           20*sqrt(MIN2)*MOU2**2*np4*p1p2*p1p4*p2p4 -  & 
           264*MIN2*MOU2*p1p2**2*p1p4*p2p4 +  & 
           72*MOU2**2*p1p2**2*p1p4*p2p4 -  & 
           112*sqrt(MIN2)*MOU2*np2*p1p2**2*p1p4*p2p4 +  & 
           56*sqrt(MIN2)*MOU2*np4*p1p2**2*p1p4*p2p4 +  & 
           112*MOU2*p1p2**3*p1p4*p2p4 +  & 
           3*CMPLX(aa,bb)*asym*MIN2**1.5*p1p4**2*p2p4 -  & 
           20*MIN2**3*p1p4**2*p2p4 +  & 
           81*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p4**2*p2p4 +  & 
           180*MIN2**2*MOU2*p1p4**2*p2p4 -  & 
           196*MIN2*MOU2**2*p1p4**2*p2p4 +  & 
           36*MOU2**3*p1p4**2*p2p4 -  & 
           36*MIN2**2.5*np2*p1p4**2*p2p4 +  & 
           360*MIN2**1.5*MOU2*np2*p1p4**2*p2p4 -  & 
           308*sqrt(MIN2)*MOU2**2*np2*p1p4**2*p2p4 -  & 
           36*MIN2**2.5*np4*p1p4**2*p2p4 +  & 
           22*MIN2**1.5*MOU2*np4*p1p4**2*p2p4 -  & 
           2*sqrt(MIN2)*MOU2**2*np4*p1p4**2*p2p4 +  & 
           6*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p1p4**2*p2p4 +  & 
           156*MIN2**2*p1p2*p1p4**2*p2p4 -  & 
           644*MIN2*MOU2*p1p2*p1p4**2*p2p4 +  & 
           328*MOU2**2*p1p2*p1p4**2*p2p4 +  & 
           200*MIN2**1.5*np2*p1p2*p1p4**2*p2p4 -  & 
           568*sqrt(MIN2)*MOU2*np2*p1p2*p1p4**2*p2p4 +  & 
           92*MIN2**1.5*np4*p1p2*p1p4**2*p2p4 -  & 
           92*sqrt(MIN2)*MOU2*np4*p1p2*p1p4**2*p2p4 -  & 
           360*MIN2*p1p2**2*p1p4**2*p2p4 +  & 
           512*MOU2*p1p2**2*p1p4**2*p2p4 -  & 
           224*sqrt(MIN2)*np2*p1p2**2*p1p4**2*p2p4 -  & 
           56*sqrt(MIN2)*np4*p1p2**2*p1p4**2*p2p4 +  & 
           224*p1p2**3*p1p4**2*p2p4 -  & 
           3*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4**3*p2p4 +  & 
           76*MIN2**2*p1p4**3*p2p4 -  & 
           158*MIN2*MOU2*p1p4**3*p2p4 +  & 
           2*MOU2**2*p1p4**3*p2p4 +  & 
           92*MIN2**1.5*np2*p1p4**3*p2p4 -  & 
           252*sqrt(MIN2)*MOU2*np2*p1p4**3*p2p4 +  & 
           168*MIN2**1.5*np4*p1p4**3*p2p4 -  & 
           196*sqrt(MIN2)*MOU2*np4*p1p4**3*p2p4 -  & 
           264*MIN2*p1p2*p1p4**3*p2p4 +  & 
           344*MOU2*p1p2*p1p4**3*p2p4 -  & 
           168*sqrt(MIN2)*np2*p1p2*p1p4**3*p2p4 -  & 
           336*sqrt(MIN2)*np4*p1p2*p1p4**3*p2p4 +  & 
           224*p1p2**2*p1p4**3*p2p4 -  & 
           168*MIN2*p1p4**4*p2p4 + 196*MOU2*p1p4**4*p2p4 -  & 
           112*sqrt(MIN2)*np4*p1p4**4*p2p4 +  & 
           336*p1p2*p1p4**4*p2p4 + 112*p1p4**5*p2p4 -  & 
           6*CMPLX(aa,bb)*asym*MIN2**1.5*p1p4*p2p4**2 -  & 
           20*MIN2**3*p1p4*p2p4**2 -  & 
           62*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p4*p2p4**2 -  & 
           116*MIN2**2*MOU2*p1p4*p2p4**2 +  & 
           56*MIN2*MOU2**2*p1p4*p2p4**2 -  & 
           36*MIN2**2.5*np2*p1p4*p2p4**2 -  & 
           164*MIN2**1.5*MOU2*np2*p1p4*p2p4**2 +  & 
           72*sqrt(MIN2)*MOU2**2*np2*p1p4*p2p4**2 +  & 
           72*MIN2**2.5*np4*p1p4*p2p4**2 -  & 
           46*MIN2**1.5*MOU2*np4*p1p4*p2p4**2 +  & 
           102*sqrt(MIN2)*MOU2**2*np4*p1p4*p2p4**2 -  & 
           3*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p1p4*p2p4**2 +  & 
           36*MIN2**2*p1p2*p1p4*p2p4**2 +  & 
           380*MIN2*MOU2*p1p2*p1p4*p2p4**2 -  & 
           72*MOU2**2*p1p2*p1p4*p2p4**2 -  & 
           16*MIN2**1.5*np2*p1p2*p1p4*p2p4**2 +  & 
           312*sqrt(MIN2)*MOU2*np2*p1p2*p1p4*p2p4**2 -  & 
           184*MIN2**1.5*np4*p1p2*p1p4*p2p4**2 +  & 
           88*sqrt(MIN2)*MOU2*np4*p1p2*p1p4*p2p4**2 +  & 
           96*MIN2*p1p2**2*p1p4*p2p4**2 -  & 
           312*MOU2*p1p2**2*p1p4*p2p4**2 +  & 
           112*sqrt(MIN2)*np2*p1p2**2*p1p4*p2p4**2 +  & 
           112*sqrt(MIN2)*np4*p1p2**2*p1p4*p2p4**2 -  & 
           112*p1p2**3*p1p4*p2p4**2 -  & 
           3*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4**2*p2p4**2 -  & 
           76*MIN2**2*p1p4**2*p2p4**2 +  & 
           230*MIN2*MOU2*p1p4**2*p2p4**2 -  & 
           102*MOU2**2*p1p4**2*p2p4**2 -  & 
           128*MIN2**1.5*np2*p1p4**2*p2p4**2 +  & 
           400*sqrt(MIN2)*MOU2*np2*p1p4**2*p2p4**2 -  & 
           278*MIN2**1.5*np4*p1p4**2*p2p4**2 +  & 
           310*sqrt(MIN2)*MOU2*np4*p1p4**2*p2p4**2 +  & 
           416*MIN2*p1p2*p1p4**2*p2p4**2 -  & 
           488*MOU2*p1p2*p1p4**2*p2p4**2 +  & 
           336*sqrt(MIN2)*np2*p1p2*p1p4**2*p2p4**2 +  & 
           472*sqrt(MIN2)*np4*p1p2*p1p4**2*p2p4**2 -  & 
           448*p1p2**2*p1p4**2*p2p4**2 +  & 
           250*MIN2*p1p4**3*p2p4**2 -  & 
           310*MOU2*p1p4**3*p2p4**2 +  & 
           56*sqrt(MIN2)*np2*p1p4**3*p2p4**2 +  & 
           336*sqrt(MIN2)*np4*p1p4**3*p2p4**2 -  & 
           528*p1p2*p1p4**3*p2p4**2 - 336*p1p4**4*p2p4**2 +  & 
           3*CMPLX(aa,bb)*asym*MIN2**1.5*p2p4**3 & 
         + 20*MIN2**3*p2p4**3 -  & 
           3*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p2p4**3 -  & 
           20*MIN2**2*MOU2*p2p4**3 +  & 
           36*MIN2**2.5*np2*p2p4**3 -  & 
           36*MIN2**1.5*MOU2*np2*p2p4**3 -  & 
           36*MIN2**2.5*np4*p2p4**3 +  & 
           36*MIN2**1.5*MOU2*np4*p2p4**3 -  & 
           76*MIN2**2*p1p2*p2p4**3 +  & 
           36*MIN2*MOU2*p1p2*p2p4**3 -  & 
           56*MIN2**1.5*np2*p1p2*p2p4**3 +  & 
           92*MIN2**1.5*np4*p1p2*p2p4**3 -  & 
           36*sqrt(MIN2)*MOU2*np4*p1p2*p2p4**3 +  & 
           56*MIN2*p1p2**2*p2p4**3 -  & 
           56*sqrt(MIN2)*np4*p1p2**2*p2p4**3 +  & 
           3*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4*p2p4**3 -  & 
           76*MIN2**2*p1p4*p2p4**3 -  & 
           48*MIN2*MOU2*p1p4*p2p4**3 -  & 
           20*MIN2**1.5*np2*p1p4*p2p4**3 -  & 
           164*sqrt(MIN2)*MOU2*np2*p1p4*p2p4**3 +  & 
           276*MIN2**1.5*np4*p1p4*p2p4**3 -  & 
           224*sqrt(MIN2)*MOU2*np4*p1p4*p2p4**3 -  & 
           40*MIN2*p1p2*p1p4*p2p4**3 +  & 
           200*MOU2*p1p2*p1p4*p2p4**3 -  & 
           168*sqrt(MIN2)*np2*p1p2*p1p4*p2p4**3 -  & 
           384*sqrt(MIN2)*np4*p1p2*p1p4*p2p4**3 +  & 
           224*p1p2**2*p1p4*p2p4**3 -  & 
           164*MIN2*p1p4**2*p2p4**3 +  & 
           224*MOU2*p1p4**2*p2p4**3 -  & 
           112*sqrt(MIN2)*np2*p1p4**2*p2p4**3 -  & 
           416*sqrt(MIN2)*np4*p1p4**2*p2p4**3 +  & 
           496*p1p2*p1p4**2*p2p4**3 + 416*p1p4**3*p2p4**3 +  & 
           76*MIN2**2*p2p4**4 - 36*MIN2*MOU2*p2p4**4 +  & 
           56*MIN2**1.5*np2*p2p4**4 -  & 
           110*MIN2**1.5*np4*p2p4**4 +  & 
           54*sqrt(MIN2)*MOU2*np4*p2p4**4 -  & 
           112*MIN2*p1p2*p2p4**4 +  & 
           136*sqrt(MIN2)*np4*p1p2*p2p4**4 -  & 
           30*MIN2*p1p4*p2p4**4 - 54*MOU2*p1p4*p2p4**4 +  & 
           56*sqrt(MIN2)*np2*p1p4*p2p4**4 +  & 
           272*sqrt(MIN2)*np4*p1p4*p2p4**4 -  & 
           192*p1p2*p1p4*p2p4**4 - 272*p1p4**2*p2p4**4 +  & 
           56*MIN2*p2p4**5 - 80*sqrt(MIN2)*np4*p2p4**5 +  & 
           80*p1p4*p2p4**5)*Acache(2))/ & 
       (18.*MOU2*p1p4**2*(p1p4 - p2p4)**3*Pi**2) +  & 
      (EL**4*GF**2*sqrt(MIN2)* & 
         (-24*CMPLX(aa,bb)*asym*MIN2**2*p1p4 & 
         + 24*CMPLX(aa,bb)*asym*MIN2*MOU2*p1p4 +  & 
           18*MIN2**3*np4*p1p4 - 36*MIN2**2*MOU2*np4*p1p4 +  & 
           18*MIN2*MOU2**2*np4*p1p4 +  & 
           35*CMPLX(aa,bb)*asym*MIN2*p1p2*p1p4 & 
         - 28*MIN2**2.5*p1p2*p1p4 +  & 
           28*MIN2**1.5*MOU2*p1p2*p1p4 -  & 
           36*MIN2**2*np2*p1p2*p1p4 +  & 
           36*MIN2*MOU2*np2*p1p2*p1p4 -  & 
           50*MIN2**2*np4*p1p2*p1p4 +  & 
           50*MIN2*MOU2*np4*p1p2*p1p4 +  & 
           92*MIN2**1.5*p1p2**2*p1p4 -  & 
           36*sqrt(MIN2)*MOU2*p1p2**2*p1p4 +  & 
           64*MIN2*np2*p1p2**2*p1p4 +  & 
           32*MIN2*np4*p1p2**2*p1p4 -  & 
           64*sqrt(MIN2)*p1p2**3*p1p4 +  & 
           11*CMPLX(aa,bb)*asym*MIN2*p1p4**2 + 28*MIN2**2.5*p1p4**2 +  & 
           2*CMPLX(aa,bb)*asym*MOU2*p1p4**2 & 
         - 28*MIN2**1.5*MOU2*p1p4**2 +  & 
           36*MIN2**2*np2*p1p4**2 -  & 
           36*MIN2*MOU2*np2*p1p4**2 -  & 
           20*MIN2**2*np4*p1p4**2 +  & 
           20*MIN2*MOU2*np4*p1p4**2 & 
         - 8*CMPLX(aa,bb)*asym*p1p2*p1p4**2 -  & 
           82*MIN2**1.5*p1p2*p1p4**2 +  & 
           26*sqrt(MIN2)*MOU2*p1p2*p1p4**2 -  & 
           60*MIN2*np2*p1p2*p1p4**2 +  & 
           20*MIN2*np4*p1p2*p1p4**2 +  & 
           16*MOU2*np4*p1p2*p1p4**2 +  & 
           36*sqrt(MIN2)*p1p2**2*p1p4**2 +  & 
           32*np4*p1p2**2*p1p4**2 + 12*CMPLX(aa,bb)*asym*p1p4**3 +  & 
           20*MIN2**1.5*p1p4**3 -  & 
           20*sqrt(MIN2)*MOU2*p1p4**3 +  & 
           12*MIN2*np2*p1p4**3 - 16*MOU2*np2*p1p4**3 -  & 
           32*sqrt(MIN2)*p1p2*p1p4**3 -  & 
           32*np2*p1p2*p1p4**3 + 12*CMPLX(aa,bb)*asym*MIN2**2*p2p4 +  & 
           28*MIN2**3.5*p2p4 - 12*CMPLX(aa,bb)*asym*MIN2*MOU2*p2p4 -  & 
           28*MIN2**2.5*MOU2*p2p4 + 36*MIN2**3*np2*p2p4 -  & 
           36*MIN2**2*MOU2*np2*p2p4 - 36*MIN2**3*np4*p2p4 +  & 
           36*MIN2**2*MOU2*np4*p2p4 -  & 
           12*CMPLX(aa,bb)*asym*MIN2*p1p2*p2p4 & 
         - 92*MIN2**2.5*p1p2*p2p4 +  & 
           36*MIN2**1.5*MOU2*p1p2*p2p4 -  & 
           64*MIN2**2*np2*p1p2*p2p4 +  & 
           100*MIN2**2*np4*p1p2*p2p4 -  & 
           36*MIN2*MOU2*np4*p1p2*p2p4 +  & 
           64*MIN2**1.5*p1p2**2*p2p4 -  & 
           64*MIN2*np4*p1p2**2*p2p4 -  & 
           38*CMPLX(aa,bb)*asym*MIN2*p1p4*p2p4 & 
         - 24*MIN2**2.5*p1p4*p2p4 +  & 
           24*MIN2**1.5*MOU2*p1p4*p2p4 -  & 
           22*MIN2**2*np2*p1p4*p2p4 +  & 
           18*MIN2*MOU2*np2*p1p4*p2p4 +  & 
           122*MIN2**2*np4*p1p4*p2p4 -  & 
           118*MIN2*MOU2*np4*p1p4*p2p4 -  & 
           18*MIN2**1.5*p1p2*p1p4*p2p4 +  & 
           18*sqrt(MIN2)*MOU2*p1p2*p1p4*p2p4 -  & 
           32*MIN2*np2*p1p2*p1p4*p2p4 -  & 
           170*MIN2*np4*p1p2*p1p4*p2p4 -  & 
           18*MOU2*np4*p1p2*p1p4*p2p4 +  & 
           96*sqrt(MIN2)*p1p2**2*p1p4*p2p4 -  & 
           24*np4*p1p2**2*p1p4*p2p4 & 
         + 2*CMPLX(aa,bb)*asym*p1p4**2*p2p4 +  & 
           38*MIN2**1.5*p1p4**2*p2p4 +  & 
           18*sqrt(MIN2)*MOU2*p1p4**2*p2p4 +  & 
           46*MIN2*np2*p1p4**2*p2p4 +  & 
           18*MOU2*np2*p1p4**2*p2p4 -  & 
           48*MIN2*np4*p1p4**2*p2p4 +  & 
           20*sqrt(MIN2)*p1p2*p1p4**2*p2p4 +  & 
           24*np2*p1p2*p1p4**2*p2p4 -  & 
           32*np4*p1p2*p1p4**2*p2p4 +  & 
           16*sqrt(MIN2)*p1p4**3*p2p4 +  & 
           32*np2*p1p4**3*p2p4 + 12*CMPLX(aa,bb)*asym*MIN2*p2p4**2 +  & 
           92*MIN2**2.5*p2p4**2 -  & 
           36*MIN2**1.5*MOU2*p2p4**2 +  & 
           64*MIN2**2*np2*p2p4**2 -  & 
           136*MIN2**2*np4*p2p4**2 +  & 
           72*MIN2*MOU2*np4*p2p4**2 -  & 
           128*MIN2**1.5*p1p2*p2p4**2 +  & 
           176*MIN2*np4*p1p2*p2p4**2 -  & 
           54*MIN2**1.5*p1p4*p2p4**2 -  & 
           18*sqrt(MIN2)*MOU2*p1p4*p2p4**2 -  & 
           32*MIN2*np2*p1p4*p2p4**2 +  & 
           168*MIN2*np4*p1p4*p2p4**2 -  & 
           56*sqrt(MIN2)*p1p2*p1p4*p2p4**2 +  & 
           24*np4*p1p2*p1p4*p2p4**2 -  & 
           40*sqrt(MIN2)*p1p4**2*p2p4**2 -  & 
           24*np2*p1p4**2*p2p4**2 + 64*MIN2**1.5*p2p4**3 -  & 
           112*MIN2*np4*p2p4**3 + 24*sqrt(MIN2)*p1p4*p2p4**3) & 
          *Bcache(1))/ & 
       (9.*p1p4**3*(p1p4 - p2p4)*Pi**2) -  & 
      (2*EL**4*GF**2*MOU2* & 
         (12*CMPLX(aa,bb)*asym*MIN2**1.5*p1p4 + 14*MIN2**3*p1p4 -  & 
           12*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p4 -  & 
           28*MIN2**2*MOU2*p1p4 + 14*MIN2*MOU2**2*p1p4 +  & 
           18*MIN2**2.5*np2*p1p4 -  & 
           36*MIN2**1.5*MOU2*np2*p1p4 +  & 
           18*sqrt(MIN2)*MOU2**2*np2*p1p4 -  & 
           9*MIN2**2.5*np4*p1p4 +  & 
           18*MIN2**1.5*MOU2*np4*p1p4 -  & 
           9*sqrt(MIN2)*MOU2**2*np4*p1p4 -  & 
           22*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p1p4 -  & 
           60*MIN2**2*p1p2*p1p4 + 78*MIN2*MOU2*p1p2*p1p4 -  & 
           18*MOU2**2*p1p2*p1p4 -  & 
           50*MIN2**1.5*np2*p1p2*p1p4 +  & 
           50*sqrt(MIN2)*MOU2*np2*p1p2*p1p4 +  & 
           25*MIN2**1.5*np4*p1p2*p1p4 -  & 
           25*sqrt(MIN2)*MOU2*np4*p1p2*p1p4 +  & 
           78*MIN2*p1p2**2*p1p4 - 50*MOU2*p1p2**2*p1p4 +  & 
           32*sqrt(MIN2)*np2*p1p2**2*p1p4 -  & 
           16*sqrt(MIN2)*np4*p1p2**2*p1p4 -  & 
           32*p1p2**3*p1p4 - 2*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4**2 -  & 
           5*MIN2**2*p1p4**2 - 4*MIN2*MOU2*p1p4**2 +  & 
           9*MOU2**2*p1p4**2 - 20*MIN2**1.5*np2*p1p4**2 +  & 
           20*sqrt(MIN2)*MOU2*np2*p1p4**2 -  & 
           6*MIN2**1.5*np4*p1p4**2 +  & 
           6*sqrt(MIN2)*MOU2*np4*p1p4**2 +  & 
           23*MIN2*p1p2*p1p4**2 + 5*MOU2*p1p2*p1p4**2 +  & 
           34*sqrt(MIN2)*np2*p1p2*p1p4**2 +  & 
           14*sqrt(MIN2)*np4*p1p2*p1p4**2 -  & 
           18*p1p2**2*p1p4**2 + 6*MIN2*p1p4**3 -  & 
           6*MOU2*p1p4**3 + 2*sqrt(MIN2)*np2*p1p4**3 -  & 
           16*p1p2*p1p4**3 - 12*CMPLX(aa,bb)*asym*MIN2**1.5*p2p4 -  & 
           14*MIN2**3*p2p4 + 12*CMPLX(aa,bb)*asym* & 
         sqrt(MIN2)*MOU2*p2p4 +  & 
           14*MIN2**2*MOU2*p2p4 - 18*MIN2**2.5*np2*p2p4 +  & 
           18*MIN2**1.5*MOU2*np2*p2p4 +  & 
           9*MIN2**2.5*np4*p2p4 -  & 
           9*sqrt(MIN2)*MOU2**2*np4*p2p4 +  & 
           22*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p2p4 +  & 
           60*MIN2**2*p1p2*p2p4 - 32*MIN2*MOU2*p1p2*p2p4 +  & 
           50*MIN2**1.5*np2*p1p2*p2p4 -  & 
           18*sqrt(MIN2)*MOU2*np2*p1p2*p2p4 -  & 
           25*MIN2**1.5*np4*p1p2*p2p4 -  & 
           7*sqrt(MIN2)*MOU2*np4*p1p2*p2p4 -  & 
           78*MIN2*p1p2**2*p2p4 + 18*MOU2*p1p2**2*p2p4 -  & 
           32*sqrt(MIN2)*np2*p1p2**2*p2p4 +  & 
           16*sqrt(MIN2)*np4*p1p2**2*p2p4 +  & 
           32*p1p2**3*p2p4 + 24*CMPLX(aa,bb)*asym* & 
         sqrt(MIN2)*p1p4*p2p4 +  & 
           49*MIN2**2*p1p4*p2p4 - 58*MIN2*MOU2*p1p4*p2p4 +  & 
           9*MOU2**2*p1p4*p2p4 +  & 
           72*MIN2**1.5*np2*p1p4*p2p4 -  & 
           70*sqrt(MIN2)*MOU2*np2*p1p4*p2p4 -  & 
           13*MIN2**1.5*np4*p1p4*p2p4 +  & 
           11*sqrt(MIN2)*MOU2*np4*p1p4*p2p4 -  & 
           133*MIN2*p1p2*p1p4*p2p4 +  & 
           77*MOU2*p1p2*p1p4*p2p4 -  & 
           100*sqrt(MIN2)*np2*p1p2*p1p4*p2p4 +  & 
           84*p1p2**2*p1p4*p2p4 - 17*MIN2*p1p4**2*p2p4 -  & 
           11*MOU2*p1p4**2*p2p4 -  & 
           38*sqrt(MIN2)*np2*p1p4**2*p2p4 -  & 
           8*sqrt(MIN2)*np4*p1p4**2*p2p4 +  & 
           38*p1p2*p1p4**2*p2p4 + 8*p1p4**3*p2p4 -  & 
           22*CMPLX(aa,bb)*asym*sqrt(MIN2)*p2p4**2 & 
         - 44*MIN2**2*p2p4**2 +  & 
           16*MIN2*MOU2*p2p4**2 - 52*MIN2**1.5*np2*p2p4**2 +  & 
           18*sqrt(MIN2)*MOU2*np2*p2p4**2 +  & 
           19*MIN2**1.5*np4*p2p4**2 +  & 
           15*sqrt(MIN2)*MOU2*np4*p2p4**2 +  & 
           110*MIN2*p1p2*p2p4**2 - 18*MOU2*p1p2*p2p4**2 +  & 
           66*sqrt(MIN2)*np2*p1p2*p2p4**2 -  & 
           14*sqrt(MIN2)*np4*p1p2*p2p4**2 -  & 
           66*p1p2**2*p2p4**2 + 35*MIN2*p1p4*p2p4**2 -  & 
           15*MOU2*p1p4*p2p4**2 +  & 
           70*sqrt(MIN2)*np2*p1p4*p2p4**2 +  & 
           4*sqrt(MIN2)*np4*p1p4*p2p4**2 -  & 
           56*p1p2*p1p4*p2p4**2 - 4*p1p4**2*p2p4**2 -  & 
           24*MIN2*p2p4**3 - 34*sqrt(MIN2)*np2*p2p4**3 +  & 
           4*sqrt(MIN2)*np4*p2p4**3 + 34*p1p2*p2p4**3 -  & 
           4*p1p4*p2p4**3)*Bcache(2))/ & 
       (9.*p1p4*(p1p4 - p2p4)**3*Pi**2) +  & 
      (2*EL**4*GF**2*MIN2* & 
         ((-2*MOU2**2*p1p4* & 
               (-(MIN2*p1p4) + 3*p1p2*p1p4 +  & 
                 3*sqrt(MIN2)*np4*p2p4 - 3*p1p4*p2p4) -  & 
              (p1p4 - p2p4)* & 
               (14*MIN2**2*p1p2*p1p4 -  & 
                 28*MIN2*p1p2**2*p1p4 - 8*MIN2*p1p4**3 +  & 
                 6*MIN2**2.5*np4*p2p4 +  & 
                 10*MIN2**2*p1p2*p2p4 -  & 
                 8*MIN2*p1p2**2*p2p4 -  & 
                 10*MIN2**2*p1p4*p2p4 +  & 
                 36*MIN2*p1p2*p1p4*p2p4 +  & 
                 8*MIN2*p1p4**2*p2p4 - 10*MIN2**2*p2p4**2 +  & 
                 16*MIN2*p1p2*p2p4**2 -  & 
                 10*MIN2*p1p4*p2p4**2 - 8*MIN2*p2p4**3 -  & 
                 2*MIN2**3*(p1p4 + p2p4) +  & 
                 8*p1p4*(p1p2 - p2p4)* & 
                  (2*p1p2**2 + 2*p1p4**2 - 2*p1p2*p2p4 -  & 
                    2*p1p4*p2p4 + p2p4**2) -  & 
                 8*sqrt(MIN2)*np4*(p1p2 - p2p4)* & 
                  (2*p1p4**2 - p1p2*p2p4 - 2*p1p4*p2p4 +  & 
                    p2p4**2) +  & 
                 MIN2**1.5* & 
                  (8*np4*p1p4**2 - 14*np4*p1p2*p2p4 -  & 
                    8*np4*p1p4*p2p4 + 14*np4*p2p4**2)) -  & 
              MOU2*(4*MIN2**2*p1p4**2 -  & 
                 20*MIN2*p1p2*p1p4**2 + 20*p1p2**2*p1p4**2 +  & 
                 8*p1p4**4 - 12*MIN2**1.5*np4*p1p4*p2p4 +  & 
                 4*MIN2*p1p2*p1p4*p2p4 -  & 
                 12*p1p2**2*p1p4*p2p4 +  & 
                 16*MIN2*p1p4**2*p2p4 -  & 
                 28*p1p2*p1p4**2*p2p4 - 16*p1p4**3*p2p4 -  & 
                 2*MIN2**2*p2p4**2 +  & 
                 6*MIN2**1.5*np4*p2p4**2 +  & 
                 6*MIN2*p1p2*p2p4**2 +  & 
                 12*p1p2*p1p4*p2p4**2 + 22*p1p4**2*p2p4**2 -  & 
                 6*MIN2*p2p4**3 - 6*p1p4*p2p4**3 +  & 
                 sqrt(MIN2)* & 
                  (-8*np4*p1p4**3 + 14*np4*p1p2*p1p4*p2p4 +  & 
                    16*np4*p1p4**2*p2p4 -  & 
                    6*np4*p1p2*p2p4**2 -  & 
                    22*np4*p1p4*p2p4**2 + 6*np4*p2p4**3)) -  & 
              2*sqrt(MIN2)*np2* & 
               (3*MIN2 - 3*MOU2 - 4*p1p2 + 4*p2p4)* & 
               (MOU2*p1p4**2 -  & 
                 (p1p4 - p2p4)* & 
                  (p1p4*(-2*p1p2 + p2p4) +  & 
                    MIN2*(p1p4 + p2p4))))/9. +  & 
           (2*MOU2**2*p1p4* & 
               (2*MIN2*p1p4 - 2*p1p2*p1p4 -  & 
                 2*sqrt(MIN2)*np4*p2p4 + 2*p1p4*p2p4) +  & 
              MOU2*(-8*MIN2**2*p1p4**2 +  & 
                 24*MIN2*p1p2*p1p4**2 - 16*p1p2**2*p1p4**2 -  & 
                 8*p1p4**4 + 8*MIN2**1.5*np4*p1p4*p2p4 -  & 
                 8*MIN2*p1p2*p1p4*p2p4 +  & 
                 8*p1p2**2*p1p4*p2p4 -  & 
                 16*MIN2*p1p4**2*p2p4 +  & 
                 24*p1p2*p1p4**2*p2p4 + 16*p1p4**3*p2p4 +  & 
                 4*MIN2**2*p2p4**2 -  & 
                 4*MIN2**1.5*np4*p2p4**2 -  & 
                 4*MIN2*p1p2*p2p4**2 - 8*p1p2*p1p4*p2p4**2 -  & 
                 26*p1p4**2*p2p4**2 + 4*MIN2*p2p4**3 +  & 
                 10*p1p4*p2p4**3 +  & 
                 sqrt(MIN2)* & 
                  (8*np4*p1p4**3 - CMPLX(aa,bb)*asym*p1p4*p2p4 -  & 
                    12*np4*p1p2*p1p4*p2p4 -  & 
                    16*np4*p1p4**2*p2p4 + CMPLX(aa,bb)*asym*p2p4**2 +  & 
                    4*np4*p1p2*p2p4**2 +  & 
                    26*np4*p1p4*p2p4**2 - 10*np4*p2p4**3)) +  & 
              2*sqrt(MIN2)*np2* & 
               (-2*MIN2 + 2*MOU2 + 4*p1p2 - 4*p2p4)* & 
               (MOU2*p1p4**2 -  & 
                 (p1p4 - p2p4)* & 
                  (p1p4*(-2*p1p2 + p2p4) +  & 
                    MIN2*(p1p4 + p2p4))) +  & 
              (p1p4 - p2p4)* & 
               (-20*MIN2**2*p1p2*p1p4 +  & 
                 32*MIN2*p1p2**2*p1p4 + 8*MIN2*p1p4**3 -  & 
                 4*MIN2**2.5*np4*p2p4 -  & 
                 12*MIN2**2*p1p2*p2p4 +  & 
                 8*MIN2*p1p2**2*p2p4 +  & 
                 12*MIN2**2*p1p4*p2p4 -  & 
                 40*MIN2*p1p2*p1p4*p2p4 -  & 
                 8*MIN2*p1p4**2*p2p4 + 12*MIN2**2*p2p4**2 -  & 
                 16*MIN2*p1p2*p2p4**2 +  & 
                 14*MIN2*p1p4*p2p4**2 + 8*MIN2*p2p4**3 +  & 
                 4*MIN2**3*(p1p4 + p2p4) +  & 
                 8*p1p4*(p1p2 - p2p4)* & 
                  (-2*p1p2**2 - 2*p1p4**2 + 2*p1p2*p2p4 +  & 
                    2*p1p4*p2p4 - 2*p2p4**2) +  & 
                 MIN2**1.5* & 
                  (-8*np4*p1p4**2 + CMPLX(aa,bb)*asym*p2p4 +  & 
                    12*np4*p1p2*p2p4 + 8*np4*p1p4*p2p4 -  & 
                    18*np4*p2p4**2) +  & 
                 sqrt(MIN2)* & 
                  (-(CMPLX(aa,bb)*asym*p1p2*p1p4) & 
         + CMPLX(aa,bb)*asym*p1p4**2 +  & 
                    CMPLX(aa,bb)*asym*p1p4*p2p4 -  & 
                    8*np4*(p1p2 - p2p4)* & 
                     (-2*p1p4**2 + p1p2*p2p4 + 2*p1p4*p2p4 -  & 
                       2*p2p4**2))))/3.)*DBcache(1))/ & 
       (p1p4**2*(p1p4 - p2p4)**2*Pi**2) +  & 
      (2*EL**4*GF**2*MOU2* & 
         ((-2*MOU2**2*p1p4* & 
               (-(MIN2*p1p4) + 3*p1p2*p1p4 +  & 
                 3*sqrt(MIN2)*np4*p2p4 - 3*p1p4*p2p4) -  & 
              (p1p4 - p2p4)* & 
               (14*MIN2**2*p1p2*p1p4 -  & 
                 28*MIN2*p1p2**2*p1p4 - 8*MIN2*p1p4**3 +  & 
                 6*MIN2**2.5*np4*p2p4 +  & 
                 10*MIN2**2*p1p2*p2p4 -  & 
                 8*MIN2*p1p2**2*p2p4 -  & 
                 10*MIN2**2*p1p4*p2p4 +  & 
                 36*MIN2*p1p2*p1p4*p2p4 +  & 
                 8*MIN2*p1p4**2*p2p4 - 10*MIN2**2*p2p4**2 +  & 
                 16*MIN2*p1p2*p2p4**2 -  & 
                 10*MIN2*p1p4*p2p4**2 - 8*MIN2*p2p4**3 -  & 
                 2*MIN2**3*(p1p4 + p2p4) +  & 
                 8*p1p4*(p1p2 - p2p4)* & 
                  (2*p1p2**2 + 2*p1p4**2 - 2*p1p2*p2p4 -  & 
                    2*p1p4*p2p4 + p2p4**2) -  & 
                 8*sqrt(MIN2)*np4*(p1p2 - p2p4)* & 
                  (2*p1p4**2 - p1p2*p2p4 - 2*p1p4*p2p4 +  & 
                    p2p4**2) +  & 
                 MIN2**1.5* & 
                  (8*np4*p1p4**2 - 14*np4*p1p2*p2p4 -  & 
                    8*np4*p1p4*p2p4 + 14*np4*p2p4**2)) -  & 
              MOU2*(4*MIN2**2*p1p4**2 -  & 
                 20*MIN2*p1p2*p1p4**2 + 20*p1p2**2*p1p4**2 +  & 
                 8*p1p4**4 - 12*MIN2**1.5*np4*p1p4*p2p4 +  & 
                 4*MIN2*p1p2*p1p4*p2p4 -  & 
                 12*p1p2**2*p1p4*p2p4 +  & 
                 16*MIN2*p1p4**2*p2p4 -  & 
                 28*p1p2*p1p4**2*p2p4 - 16*p1p4**3*p2p4 -  & 
                 2*MIN2**2*p2p4**2 +  & 
                 6*MIN2**1.5*np4*p2p4**2 +  & 
                 6*MIN2*p1p2*p2p4**2 +  & 
                 12*p1p2*p1p4*p2p4**2 + 22*p1p4**2*p2p4**2 -  & 
                 6*MIN2*p2p4**3 - 6*p1p4*p2p4**3 +  & 
                 sqrt(MIN2)* & 
                  (-8*np4*p1p4**3 + 14*np4*p1p2*p1p4*p2p4 +  & 
                    16*np4*p1p4**2*p2p4 -  & 
                    6*np4*p1p2*p2p4**2 -  & 
                    22*np4*p1p4*p2p4**2 + 6*np4*p2p4**3)) -  & 
              2*sqrt(MIN2)*np2* & 
               (3*MIN2 - 3*MOU2 - 4*p1p2 + 4*p2p4)* & 
               (MOU2*p1p4**2 -  & 
                 (p1p4 - p2p4)* & 
                  (p1p4*(-2*p1p2 + p2p4) +  & 
                    MIN2*(p1p4 + p2p4))))/9. +  & 
           (2*MOU2**2*p1p4* & 
               (2*MIN2*p1p4 - 2*p1p2*p1p4 -  & 
                 2*sqrt(MIN2)*np4*p2p4 + 2*p1p4*p2p4) +  & 
              MOU2*(-8*MIN2**2*p1p4**2 +  & 
                 24*MIN2*p1p2*p1p4**2 - 16*p1p2**2*p1p4**2 -  & 
                 8*p1p4**4 + 8*MIN2**1.5*np4*p1p4*p2p4 -  & 
                 8*MIN2*p1p2*p1p4*p2p4 +  & 
                 8*p1p2**2*p1p4*p2p4 -  & 
                 16*MIN2*p1p4**2*p2p4 +  & 
                 24*p1p2*p1p4**2*p2p4 + 16*p1p4**3*p2p4 +  & 
                 4*MIN2**2*p2p4**2 -  & 
                 4*MIN2**1.5*np4*p2p4**2 -  & 
                 4*MIN2*p1p2*p2p4**2 - 8*p1p2*p1p4*p2p4**2 -  & 
                 26*p1p4**2*p2p4**2 + 4*MIN2*p2p4**3 +  & 
                 10*p1p4*p2p4**3 +  & 
                 sqrt(MIN2)* & 
                  (8*np4*p1p4**3 - CMPLX(aa,bb)*asym*p1p4*p2p4 -  & 
                    12*np4*p1p2*p1p4*p2p4 -  & 
                    16*np4*p1p4**2*p2p4 + CMPLX(aa,bb)*asym*p2p4**2 +  & 
                    4*np4*p1p2*p2p4**2 +  & 
                    26*np4*p1p4*p2p4**2 - 10*np4*p2p4**3)) +  & 
              2*sqrt(MIN2)*np2* & 
               (-2*MIN2 + 2*MOU2 + 4*p1p2 - 4*p2p4)* & 
               (MOU2*p1p4**2 -  & 
                 (p1p4 - p2p4)* & 
                  (p1p4*(-2*p1p2 + p2p4) +  & 
                    MIN2*(p1p4 + p2p4))) +  & 
              (p1p4 - p2p4)* & 
               (-20*MIN2**2*p1p2*p1p4 +  & 
                 32*MIN2*p1p2**2*p1p4 + 8*MIN2*p1p4**3 -  & 
                 4*MIN2**2.5*np4*p2p4 -  & 
                 12*MIN2**2*p1p2*p2p4 +  & 
                 8*MIN2*p1p2**2*p2p4 +  & 
                 12*MIN2**2*p1p4*p2p4 -  & 
                 40*MIN2*p1p2*p1p4*p2p4 -  & 
                 8*MIN2*p1p4**2*p2p4 + 12*MIN2**2*p2p4**2 -  & 
                 16*MIN2*p1p2*p2p4**2 +  & 
                 14*MIN2*p1p4*p2p4**2 + 8*MIN2*p2p4**3 +  & 
                 4*MIN2**3*(p1p4 + p2p4) +  & 
                 8*p1p4*(p1p2 - p2p4)* & 
                  (-2*p1p2**2 - 2*p1p4**2 + 2*p1p2*p2p4 +  & 
                    2*p1p4*p2p4 - 2*p2p4**2) +  & 
                 MIN2**1.5* & 
                  (-8*np4*p1p4**2 + CMPLX(aa,bb)*asym*p2p4 +  & 
                    12*np4*p1p2*p2p4 + 8*np4*p1p4*p2p4 -  & 
                    18*np4*p2p4**2) +  & 
                 sqrt(MIN2)* & 
                  (-(CMPLX(aa,bb)*asym*p1p2*p1p4) & 
         + CMPLX(aa,bb)*asym*p1p4**2 +  & 
                    CMPLX(aa,bb)*asym*p1p4*p2p4 -  & 
                    8*np4*(p1p2 - p2p4)* & 
                     (-2*p1p4**2 + p1p2*p2p4 + 2*p1p4*p2p4 -  & 
                       2*p2p4**2))))/3.)*DBcache(2))/ & 
       (p1p4**2*(p1p4 - p2p4)**2*Pi**2))
!
!	  T1BOXAC=T1BOXAC+real(
!
      return
!
      end function

                          !!!!!!!!!!!!!!!!!!!!!!
                            END MODULE rad_CTOSBC
                          !!!!!!!!!!!!!!!!!!!!!!
