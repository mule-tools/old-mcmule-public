
                          !!!!!!!!!!!!!!!!!!!!!!
                              MODULE rad_T2TRIAD
                          !!!!!!!!!!!!!!!!!!!!!!
  contains
      function T2TRIAD(EL,GF,MOU2,MIN2,p1p2,p1p4,p2p4,asym,np2,np3, & 
         np4,mu2)
!
      use global_def, only: pi, prec
#ifdef COLLIER
      use collier
#endif
      implicit none
!

#ifndef COLLIER
    complex(kind=prec) A0, B0i
    complex(kind=prec) B0
    complex(kind=prec) DB0
    complex(kind=prec) C0, C0i
    complex(kind=prec) D0, D0i

    integer, parameter :: bb0  = 1
    integer, parameter :: bb1  = 4
    integer, parameter :: cc0  = 1
    integer, parameter :: cc1  = 4
    integer, parameter :: cc2  = 7
    integer, parameter :: cc00 = 10
    integer, parameter :: cc11 = 13
    integer, parameter :: cc12 = 16
    integer, parameter :: cc22 = 19
    integer, parameter :: dd0  = 1
    integer, parameter :: dd1  = 4
    integer, parameter :: dd2  = 7
    integer, parameter :: dd3  = 10
    integer, parameter :: dd00 = 13
    
    

    external A0, B0i
    external B0
    external DB0
    external C0, C0i
    external D0, D0i
#else
    complex(kind=prec) :: uvdump(7)
#endif
    
    complex(kind=prec) :: Ccache(7)
    complex(kind=prec) :: Bcache(1)

!
      real(kind=prec) T2TRIAD,EL,GF,MOU2,MIN2, & 
         p1p2,p1p4,p2p4,asym, & 
         np2,np3,np4,mu2, & 
         aa,bb
!
      aa=0._prec
      bb=  & 
           -1._prec
!

#ifndef COLLIER
!    
      call setlambda(0._prec)
      call setdelta(0._prec)
      call setmudim(mu2)
      call setminmass(0._prec)
!     
    Bcache(1) = B0i(bb0,MIN2,MIN2,0._prec)
    Ccache(1) = C0i(cc0,MIN2,MOU2+2*p1p4-2*p2p4,-MIN2+MOU2+2*p1p2-2*p2p4,MIN2,0._prec,MOU2)
    Ccache(4) = C0i(cc00,MIN2,MOU2+2*p1p4-2*p2p4,-MIN2+MOU2+2*p1p2-2*p2p4,MIN2,0._prec,MOU2)
    Ccache(2) = C0i(cc1,MIN2,MOU2+2*p1p4-2*p2p4,-MIN2+MOU2+2*p1p2-2*p2p4,MIN2,0._prec,MOU2)
    Ccache(5) = C0i(cc11,MIN2,MOU2+2*p1p4-2*p2p4,-MIN2+MOU2+2*p1p2-2*p2p4,MIN2,0._prec,MOU2)
    Ccache(6) = C0i(cc12,MIN2,MOU2+2*p1p4-2*p2p4,-MIN2+MOU2+2*p1p2-2*p2p4,MIN2,0._prec,MOU2)
    Ccache(3) = C0i(cc2,MIN2,MOU2+2*p1p4-2*p2p4,-MIN2+MOU2+2*p1p2-2*p2p4,MIN2,0._prec,MOU2)
    Ccache(7) = C0i(cc22,MIN2,MOU2+2*p1p4-2*p2p4,-MIN2+MOU2+2*p1p2-2*p2p4,MIN2,0._prec,MOU2)
#else
    call SetDeltaUV_cll(0.) ; call SetDeltaIR_cll(0.,0.)
    call SetMuUV2_cll(mu2) ; call SetMuIR2_cll(mu2)

    call C_cll(Ccache,uvdump(:7), &
        cmplx(MIN2),cmplx(MOU2+2*p1p4-2*p2p4),cmplx(-MIN2+MOU2+2*p1p2-2*p2p4),&
        cmplx(MIN2),cmplx(0._prec),cmplx(MOU2),2)

    call B0_cll(Bcache(1), cmplx(MIN2), cmplx(0.), cmplx(MIN2))
#endif
!
      T2TRIAD=real(        (2*EL**4*GF**2*(2*MIN2**3*p1p4   & 
           - 4*MIN2**2*MOU2*p1p4   & 
           +  & 
           2*MIN2*MOU2**2*p1p4   & 
           + MIN2**2.5*np4*p1p4   & 
           -  & 
           2*MIN2**1.5*MOU2*np4*p1p4   & 
           +  & 
           sqrt(MIN2)*MOU2**2*np4*p1p4   & 
           -  & 
           4*MIN2**2*p1p2*p1p4   & 
           + 2*MIN2*MOU2*p1p2*p1p4   & 
           +  & 
           2*MOU2**2*p1p2*p1p4   & 
           - MIN2**1.5*np4*p1p2*p1p4   & 
           +  & 
           sqrt(MIN2)*MOU2*np4*p1p2*p1p4   & 
           +  & 
           2*MIN2*p1p2**2*p1p4   & 
           + 2*MOU2*p1p2**2*p1p4   & 
           -  & 
           3*MIN2**2*p1p4**2   & 
           + 4*MIN2*MOU2*p1p4**2   & 
           -  & 
           MOU2**2*p1p4**2   & 
           - 2*MIN2**1.5*np4*p1p4**2   & 
           +  & 
           2*sqrt(MIN2)*MOU2*np4*p1p4**2   & 
           +  & 
           2*MIN2*p1p2*p1p4**2   & 
           + 2*MOU2*p1p2*p1p4**2   & 
           +  & 
           2*sqrt(MIN2)*np4*p1p2*p1p4**2   & 
           + 2*MIN2*p1p4**3   & 
           -  & 
           2*MOU2*p1p4**3   & 
           -  & 
           CMPLX(aa,bb)*asym*sqrt(MIN2)*(MIN2   & 
           - MOU2   & 
           - 2*p1p4)* & 
            (p1p4   & 
           - p2p4)   & 
           - 2*MIN2**3*p2p4   & 
           +  & 
           2*MIN2**2*MOU2*p2p4   & 
           - MIN2**2.5*np4*p2p4   & 
           +  & 
           sqrt(MIN2)*MOU2**2*np4*p2p4   & 
           +  & 
           4*MIN2**2*p1p2*p2p4   & 
           + MIN2**1.5*np4*p1p2*p2p4   & 
           -  & 
           sqrt(MIN2)*MOU2*np4*p1p2*p2p4   & 
           -  & 
           2*MIN2*p1p2**2*p2p4   & 
           - 2*MOU2*p1p2**2*p2p4   & 
           +  & 
           8*MIN2**2*p1p4*p2p4   & 
           - 7*MIN2*MOU2*p1p4*p2p4   & 
           -  & 
           MOU2**2*p1p4*p2p4   & 
           + 4*MIN2**1.5*np4*p1p4*p2p4   & 
           -  & 
           2*sqrt(MIN2)*MOU2*np4*p1p4*p2p4   & 
           -  & 
           6*MIN2*p1p2*p1p4*p2p4   & 
           - 4*MOU2*p1p2*p1p4*p2p4   & 
           -  & 
           2*sqrt(MIN2)*np4*p1p2*p1p4*p2p4   & 
           -  & 
           6*MIN2*p1p4**2*p2p4   & 
           + 2*MOU2*p1p4**2*p2p4   & 
           -  & 
           5*MIN2**2*p2p4**2   & 
           + MIN2*MOU2*p2p4**2   & 
           -  & 
           2*MIN2**1.5*np4*p2p4**2   & 
           + 4*MIN2*p1p2*p2p4**2   & 
           +  & 
           2*MOU2*p1p2*p2p4**2   & 
           + 8*MIN2*p1p4*p2p4**2   & 
           -  & 
           4*MIN2*p2p4**3   & 
           -  & 
           sqrt(MIN2)*np2* & 
            (  & 
           -4*MIN2*MOU2*p1p4   & 
           + 2*MOU2**2*p1p4   & 
           +  & 
              2*MOU2*p1p2*p1p4   & 
           + 3*MOU2*p1p4**2   & 
           +  & 
              2*MIN2*MOU2*p2p4   & 
           - 2*MOU2*p1p2*p2p4   & 
           -  & 
              5*MOU2*p1p4*p2p4   & 
           + 2*MOU2*p2p4**2   & 
           +  & 
              (p1p4   & 
           - p2p4)* & 
               (2*MIN2**2   & 
           - 2*MIN2*p1p2   & 
           - 3*MIN2*p1p4   & 
           +  & 
                 2*p1p4**2   & 
           + 4*MIN2*p2p4)))* & 
         Bcache(1))/ & 
       (3.*p1p4*(p1p4   & 
           - p2p4)**2*Pi**2)   & 
           -  & 
      (2*EL**4*GF**2*(6*MIN2*MOU2**3*p1p4   & 
           +  & 
           (p1p4   & 
           - p2p4)*(MIN2   & 
           - 2*p1p2   & 
           + 2*p2p4)* & 
            (  & 
           -2*MIN2**3   & 
           + 4*MIN2**2*p1p2   & 
           - 2*MIN2*p1p2**2   & 
           +  & 
              3*MIN2**2*p1p4   & 
           - 2*MIN2*p1p2*p1p4   & 
           -  & 
              2*MIN2*p1p4**2   & 
           - 5*MIN2**2*p2p4   & 
           +  & 
              4*MIN2*p1p2*p2p4   & 
           + 4*MIN2*p1p4*p2p4   & 
           -  & 
              4*MIN2*p2p4**2   & 
           +  & 
              sqrt(MIN2)*np4* & 
               (  & 
           -MIN2**2   & 
           + MIN2*p1p2   & 
           + 2*MIN2*p1p4   & 
           -  & 
                 2*p1p2*p1p4   & 
           - 2*MIN2*p2p4)   & 
           +  & 
              sqrt(MIN2)*np2* & 
               (2*MIN2**2   & 
           - 2*MIN2*p1p2   & 
           - 3*MIN2*p1p4   & 
           +  & 
                 2*p1p4**2   & 
           + 4*MIN2*p2p4))   & 
           +  & 
           CMPLX(aa,bb)*asym*sqrt(MIN2)* & 
            (  & 
           -4*MIN2*MOU2*p1p4   & 
           + 10*MOU2*p1p2*p1p4   & 
           -  & 
              2*MOU2*p1p4**2   & 
           + 3*MOU2**2*(p1p4   & 
           - p2p4)   & 
           +  & 
              4*MIN2*MOU2*p2p4   & 
           - 10*MOU2*p1p2*p2p4   & 
           -  & 
              8*MOU2*p1p4*p2p4   & 
           + 10*MOU2*p2p4**2   & 
           +  & 
              (p1p4   & 
           - p2p4)* & 
               (MIN2**2   & 
           - 2*MIN2*p1p2   & 
           - 2*MIN2*p1p4   & 
           +  & 
                 4*p1p2*p1p4   & 
           + 2*MIN2*p2p4   & 
           - 4*p1p4*p2p4))   & 
           +  & 
           MOU2**2*(  & 
           -14*MIN2**2*p1p4   & 
           + 20*MIN2*p1p2*p1p4   & 
           +  & 
              4*p1p2**2*p1p4   & 
           + 7*MIN2*p1p4**2   & 
           -  & 
              2*p1p2*p1p4**2   & 
           + 6*MIN2**2*p2p4   & 
           -  & 
              6*MIN2*p1p2*p2p4   & 
           - 24*MIN2*p1p4*p2p4   & 
           -  & 
              6*p1p2*p1p4*p2p4   & 
           + 2*p1p4**2*p2p4   & 
           +  & 
              3*MIN2*p2p4**2   & 
           + 2*p1p4*p2p4**2   & 
           +  & 
              sqrt(MIN2)*np2*p1p4* & 
               (2*MIN2   & 
           - 4*p1p2   & 
           + 3*p1p4   & 
           + p2p4)   & 
           +  & 
              sqrt(MIN2)*np4* & 
               (3*MIN2*p1p4   & 
           - p1p2*p1p4   & 
           - 5*MIN2*p2p4   & 
           +  & 
                 5*p1p2*p2p4   & 
           - 2*p1p4*p2p4   & 
           - 2*p2p4**2))   & 
           -  & 
           2*MOU2*(  & 
           -5*MIN2**3*p1p4   & 
           + 14*MIN2**2*p1p2*p1p4   & 
           -  & 
              7*MIN2*p1p2**2*p1p4   & 
           - 2*p1p2**3*p1p4   & 
           +  & 
              5*MIN2**2*p1p4**2   & 
           - 9*MIN2*p1p2*p1p4**2   & 
           -  & 
              2*p1p2**2*p1p4**2   & 
           - MIN2*p1p4**3   & 
           +  & 
              2*p1p2*p1p4**3   & 
           + 4*MIN2**3*p2p4   & 
           -  & 
              11*MIN2**2*p1p2*p2p4   & 
           + 5*MIN2*p1p2**2*p2p4   & 
           +  & 
              2*p1p2**3*p2p4   & 
           - 18*MIN2**2*p1p4*p2p4   & 
           +  & 
              22*MIN2*p1p2*p1p4*p2p4   & 
           + 6*p1p2**2*p1p4*p2p4   & 
           +  & 
              11*MIN2*p1p4**2*p2p4   & 
           - 2*p1p4**3*p2p4   & 
           +  & 
              10*MIN2**2*p2p4**2   & 
           - 9*MIN2*p1p2*p2p4**2   & 
           -  & 
              4*p1p2**2*p2p4**2   & 
           - 15*MIN2*p1p4*p2p4**2   & 
           -  & 
              4*p1p2*p1p4*p2p4**2   & 
           + 2*p1p4**2*p2p4**2   & 
           +  & 
              3*MIN2*p2p4**3   & 
           + 2*p1p2*p2p4**3   & 
           +  & 
              sqrt(MIN2)*np4* & 
               (MIN2**2*p1p4   & 
           - 3*MIN2*p1p2*p1p4   & 
           +  & 
                 3*p1p2**2*p1p4   & 
           + MIN2*p1p4**2   & 
           -  & 
                 3*p1p2*p1p4**2   & 
           - 2*MIN2**2*p2p4   & 
           +  & 
                 5*MIN2*p1p2*p2p4   & 
           - 3*p1p2**2*p2p4   & 
           +  & 
                 2*p1p4**2*p2p4   & 
           - 3*MIN2*p2p4**2   & 
           +  & 
                 3*p1p2*p2p4**2   & 
           - 2*p1p4*p2p4**2)   & 
           +  & 
              sqrt(MIN2)*np2* & 
               (2*MIN2**2*p1p4   & 
           - 5*MIN2*p1p2*p1p4   & 
           +  & 
                 2*p1p2**2*p1p4   & 
           - p1p2*p1p4**2   & 
           + p1p4**3   & 
           -  & 
                 MIN2**2*p2p4   & 
           + 3*MIN2*p1p2*p2p4   & 
           -  & 
                 2*p1p2**2*p2p4   & 
           + 6*MIN2*p1p4*p2p4   & 
           -  & 
                 3*p1p2*p1p4*p2p4   & 
           - 4*MIN2*p2p4**2   & 
           +  & 
                 4*p1p2*p2p4**2   & 
           + p1p4*p2p4**2   & 
           - 2*p2p4**3))) & 
          *Ccache(1))/ & 
       (3.*p1p4*(p1p4   & 
           - p2p4)**2*Pi**2)   & 
           -  & 
      (4*EL**4*GF**2*(6*MIN2**3*p1p4   & 
           -  & 
           12*MIN2**2*MOU2*p1p4   & 
           + 6*MIN2*MOU2**2*p1p4   & 
           -  & 
           MIN2**2.5*np4*p1p4   & 
           + 2*MIN2**1.5*MOU2*np4*p1p4   & 
           -  & 
           sqrt(MIN2)*MOU2**2*np4*p1p4   & 
           -  & 
           20*MIN2**2*p1p2*p1p4   & 
           + 22*MIN2*MOU2*p1p2*p1p4   & 
           -  & 
           2*MOU2**2*p1p2*p1p4   & 
           + 5*MIN2**1.5*np4*p1p2*p1p4   & 
           -  & 
           5*sqrt(MIN2)*MOU2*np4*p1p2*p1p4   & 
           +  & 
           22*MIN2*p1p2**2*p1p4   & 
           - 10*MOU2*p1p2**2*p1p4   & 
           -  & 
           4*sqrt(MIN2)*np4*p1p2**2*p1p4   & 
           - 8*p1p2**3*p1p4   & 
           -  & 
           5*MIN2**2*p1p4**2   & 
           + 4*MIN2*MOU2*p1p4**2   & 
           +  & 
           MOU2**2*p1p4**2   & 
           - 6*MIN2**1.5*np4*p1p4**2   & 
           +  & 
           6*sqrt(MIN2)*MOU2*np4*p1p4**2   & 
           +  & 
           6*MIN2*p1p2*p1p4**2   & 
           + 6*MOU2*p1p2*p1p4**2   & 
           +  & 
           10*sqrt(MIN2)*np4*p1p2*p1p4**2   & 
           + 6*MIN2*p1p4**3   & 
           -  & 
           6*MOU2*p1p4**3   & 
           - 8*p1p2*p1p4**3   & 
           -  & 
           CMPLX(aa,bb)*asym*sqrt(MIN2)* & 
            (  & 
           -MIN2   & 
           + MOU2   & 
           + 4*p1p2   & 
           - 2*p1p4   & 
           - 4*p2p4)* & 
            (p1p4   & 
           - p2p4)   & 
           - 6*MIN2**3*p2p4   & 
           +  & 
           6*MIN2**2*MOU2*p2p4   & 
           + MIN2**2.5*np4*p2p4   & 
           -  & 
           sqrt(MIN2)*MOU2**2*np4*p2p4   & 
           +  & 
           20*MIN2**2*p1p2*p2p4   & 
           - 8*MIN2*MOU2*p1p2*p2p4   & 
           -  & 
           5*MIN2**1.5*np4*p1p2*p2p4   & 
           -  & 
           3*sqrt(MIN2)*MOU2*np4*p1p2*p2p4   & 
           -  & 
           22*MIN2*p1p2**2*p2p4   & 
           + 2*MOU2*p1p2**2*p2p4   & 
           +  & 
           4*sqrt(MIN2)*np4*p1p2**2*p2p4   & 
           + 8*p1p2**3*p2p4   & 
           +  & 
           24*MIN2**2*p1p4*p2p4   & 
           - 25*MIN2*MOU2*p1p4*p2p4   & 
           +  & 
           MOU2**2*p1p4*p2p4   & 
           + 4*MIN2**1.5*np4*p1p4*p2p4   & 
           -  & 
           2*sqrt(MIN2)*MOU2*np4*p1p4*p2p4   & 
           -  & 
           42*MIN2*p1p2*p1p4*p2p4   & 
           + 12*MOU2*p1p2*p1p4*p2p4   & 
           -  & 
           6*sqrt(MIN2)*np4*p1p2*p1p4*p2p4   & 
           +  & 
           16*p1p2**2*p1p4*p2p4   & 
           - 14*MIN2*p1p4**2*p2p4   & 
           +  & 
           2*MOU2*p1p4**2*p2p4   & 
           -  & 
           8*sqrt(MIN2)*np4*p1p4**2*p2p4   & 
           +  & 
           8*p1p2*p1p4**2*p2p4   & 
           + 8*p1p4**3*p2p4   & 
           -  & 
           19*MIN2**2*p2p4**2   & 
           + 7*MIN2*MOU2*p2p4**2   & 
           +  & 
           2*MIN2**1.5*np4*p2p4**2   & 
           +  & 
           4*sqrt(MIN2)*MOU2*np4*p2p4**2   & 
           +  & 
           36*MIN2*p1p2*p2p4**2   & 
           - 2*MOU2*p1p2*p2p4**2   & 
           -  & 
           4*sqrt(MIN2)*np4*p1p2*p2p4**2   & 
           -  & 
           16*p1p2**2*p2p4**2   & 
           + 24*MIN2*p1p4*p2p4**2   & 
           -  & 
           4*MOU2*p1p4*p2p4**2   & 
           +  & 
           8*sqrt(MIN2)*np4*p1p4*p2p4**2   & 
           -  & 
           8*p1p2*p1p4*p2p4**2   & 
           - 8*p1p4**2*p2p4**2   & 
           -  & 
           16*MIN2*p2p4**3   & 
           + 8*p1p2*p2p4**3   & 
           -  & 
           sqrt(MIN2)*np2* & 
            (  & 
           -2*MOU2**2*p1p4   & 
           +  & 
              (p1p4   & 
           - p2p4)* & 
               (  & 
           -2*MIN2**2   & 
           + 10*MIN2*p1p2   & 
           - 8*p1p2**2   & 
           -  & 
                 MIN2*p1p4   & 
           - 4*p1p2*p1p4   & 
           + 2*p1p4**2   & 
           -  & 
                 8*MIN2*p2p4   & 
           + 16*p1p2*p2p4   & 
           + 4*p1p4*p2p4   & 
           -  & 
                 8*p2p4**2)   & 
           +  & 
              MOU2*(4*MIN2*p1p4   & 
           - 10*p1p2*p1p4   & 
           + p1p4**2   & 
           -  & 
                 2*MIN2*p2p4   & 
           + 2*p1p2*p2p4   & 
           + 9*p1p4*p2p4   & 
           -  & 
                 2*p2p4**2)))* & 
         Ccache(4))/ & 
       (3.*p1p4*(p1p4   & 
           - p2p4)**2*Pi**2)   & 
           +  & 
      (2*EL**4*GF**2*(  & 
           -6*MIN2*MOU2**3*p1p4   & 
           +  & 
           MOU2**2*(10*MIN2**2*p1p4   & 
           +  & 
              10*MIN2**1.5*np2*p1p4   & 
           - 9*MIN2**1.5*np4*p1p4   & 
           -  & 
              32*MIN2*p1p2*p1p4   & 
           -  & 
              8*sqrt(MIN2)*np2*p1p2*p1p4   & 
           +  & 
              7*sqrt(MIN2)*np4*p1p2*p1p4   & 
           + 8*p1p2**2*p1p4   & 
           +  & 
              MIN2*p1p4**2   & 
           + 5*sqrt(MIN2)*np2*p1p4**2   & 
           -  & 
              4*sqrt(MIN2)*np4*p1p4**2   & 
           - 12*p1p2*p1p4**2   & 
           +  & 
              4*p1p4**3   & 
           - 6*MIN2**2*p2p4   & 
           -  & 
              MIN2**1.5*np4*p2p4   & 
           + 6*MIN2*p1p2*p2p4   & 
           +  & 
              sqrt(MIN2)*np4*p1p2*p2p4   & 
           + 28*MIN2*p1p4*p2p4   & 
           +  & 
              3*sqrt(MIN2)*np2*p1p4*p2p4   & 
           -  & 
              4*sqrt(MIN2)*np4*p1p4*p2p4   & 
           -  & 
              4*p1p2*p1p4*p2p4   & 
           + 4*p1p4**2*p2p4   & 
           -  & 
              3*MIN2*p2p4**2)   & 
           +  & 
           CMPLX(aa,bb)*asym*sqrt(MIN2)* & 
            (  & 
           -6*MIN2*MOU2*p1p4   & 
           - 3*MOU2**2*p1p4   & 
           -  & 
              2*MOU2*p1p2*p1p4   & 
           + 4*MOU2*p1p4**2   & 
           +  & 
              6*MIN2*MOU2*p2p4   & 
           + 3*MOU2**2*p2p4   & 
           +  & 
              2*MOU2*p1p2*p2p4   & 
           + 2*MOU2*p1p4*p2p4   & 
           -  & 
              6*MOU2*p2p4**2   & 
           -  & 
              (p1p4   & 
           - p2p4)* & 
               (  & 
           -9*MIN2**2   & 
           + 22*MIN2*p1p2   & 
           - 16*p1p2**2   & 
           +  & 
                 4*MIN2*p1p4   & 
           + 4*p1p2*p1p4   & 
           - 4*p1p4**2   & 
           -  & 
                 18*MIN2*p2p4   & 
           + 16*p1p2*p2p4))   & 
           +  & 
           (p1p4   & 
           - p2p4)* & 
            (  & 
           -2*MIN2**4   & 
           - 8*MIN2**3*p1p2   & 
           +  & 
              38*MIN2**2*p1p2**2   & 
           - 44*MIN2*p1p2**3   & 
           +  & 
              16*p1p2**4   & 
           + 9*MIN2**3*p1p4   & 
           -  & 
              14*MIN2**2*p1p2*p1p4   & 
           + 20*MIN2*p1p2**2*p1p4   & 
           -  & 
              16*p1p2**3*p1p4   & 
           - 8*MIN2**2*p1p4**2   & 
           -  & 
              16*MIN2*p1p2*p1p4**2   & 
           + 32*p1p2**2*p1p4**2   & 
           +  & 
              12*MIN2*p1p4**3   & 
           - 16*p1p2*p1p4**3   & 
           +  & 
              MIN2**3*p2p4   & 
           - 52*MIN2**2*p1p2*p2p4   & 
           +  & 
              84*MIN2*p1p2**2*p2p4   & 
           - 32*p1p2**3*p2p4   & 
           +  & 
              24*MIN2**2*p1p4*p2p4   & 
           -  & 
              28*MIN2*p1p2*p1p4*p2p4   & 
           +  & 
              16*p1p2**2*p1p4*p2p4   & 
           - 4*MIN2*p1p4**2*p2p4   & 
           -  & 
              32*p1p2*p1p4**2*p2p4   & 
           + 16*p1p4**3*p2p4   & 
           +  & 
              10*MIN2**2*p2p4**2   & 
           - 44*MIN2*p1p2*p2p4**2   & 
           +  & 
              16*p1p2**2*p2p4**2   & 
           + 20*MIN2*p1p4*p2p4**2   & 
           -  & 
              sqrt(MIN2)*np4* & 
               (MIN2**3   & 
           + MIN2**2*p1p2   & 
           - 2*MIN2*p1p2**2   & 
           +  & 
                 4*MIN2**2*p1p4   & 
           - 24*MIN2*p1p2*p1p4   & 
           +  & 
                 20*p1p2**2*p1p4   & 
           + 12*MIN2*p1p4**2   & 
           -  & 
                 20*p1p2*p1p4**2   & 
           + 4*MIN2**2*p2p4   & 
           +  & 
                 2*MIN2*p1p2*p2p4   & 
           + 8*MIN2*p1p4*p2p4   & 
           -  & 
                 16*p1p2*p1p4*p2p4   & 
           + 16*p1p4**2*p2p4   & 
           +  & 
                 4*MIN2*p2p4**2)   & 
           -  & 
              sqrt(MIN2)*np2* & 
               (  & 
           -10*MIN2**3   & 
           + 30*MIN2**2*p1p2   & 
           -  & 
                 36*MIN2*p1p2**2   & 
           + 16*p1p2**3   & 
           +  & 
                 5*MIN2**2*p1p4   & 
           + 14*MIN2*p1p2*p1p4   & 
           -  & 
                 16*p1p2**2*p1p4   & 
           - 16*MIN2*p1p4**2   & 
           +  & 
                 12*p1p2*p1p4**2   & 
           + 4*p1p4**3   & 
           -  & 
                 34*MIN2**2*p2p4   & 
           + 60*MIN2*p1p2*p2p4   & 
           -  & 
                 32*p1p2**2*p2p4   & 
           + 6*MIN2*p1p4*p2p4   & 
           +  & 
                 16*p1p2*p1p4*p2p4   & 
           - 16*p1p4**2*p2p4   & 
           -  & 
                 28*MIN2*p2p4**2   & 
           + 16*p1p2*p2p4**2))   & 
           +  & 
           2*MOU2*(  & 
           -(MIN2**3*p1p4)   & 
           + 20*MIN2**2*p1p2*p1p4   & 
           -  & 
              31*MIN2*p1p2**2*p1p4   & 
           + 12*p1p2**3*p1p4   & 
           -  & 
              5*MIN2**2*p1p4**2   & 
           + 5*MIN2*p1p2*p1p4**2   & 
           -  & 
              18*p1p2**2*p1p4**2   & 
           + 2*MIN2*p1p4**3   & 
           +  & 
              16*p1p2*p1p4**3   & 
           - 6*p1p4**4   & 
           + 2*MIN2**3*p2p4   & 
           -  & 
              15*MIN2**2*p1p2*p2p4   & 
           + 17*MIN2*p1p2**2*p2p4   & 
           -  & 
              4*p1p2**3*p2p4   & 
           - 10*MIN2**2*p1p4*p2p4   & 
           +  & 
              45*MIN2*p1p2*p1p4*p2p4   & 
           -  & 
              10*p1p2**2*p1p4*p2p4   & 
           - 10*MIN2*p1p4**2*p2p4   & 
           +  & 
              6*p1p2*p1p4**2*p2p4   & 
           + 10*MIN2**2*p2p4**2   & 
           -  & 
              22*MIN2*p1p2*p2p4**2   & 
           + 4*p1p2**2*p2p4**2   & 
           -  & 
              9*MIN2*p1p4*p2p4**2   & 
           + 2*p1p2*p1p4*p2p4**2   & 
           -  & 
              2*p1p4**2*p2p4**2   & 
           + 3*MIN2*p2p4**3   & 
           +  & 
              sqrt(MIN2)*np2* & 
               (  & 
           -10*MIN2**2*p1p4   & 
           + 19*MIN2*p1p2*p1p4   & 
           -  & 
                 12*p1p2**2*p1p4   & 
           + 9*p1p2*p1p4**2   & 
           -  & 
                 4*p1p4**3   & 
           + 5*MIN2**2*p2p4   & 
           -  & 
                 9*MIN2*p1p2*p2p4   & 
           + 4*p1p2**2*p2p4   & 
           -  & 
                 17*MIN2*p1p4*p2p4   & 
           + 11*p1p2*p1p4*p2p4   & 
           -  & 
                 p1p4**2*p2p4   & 
           + 7*MIN2*p2p4**2   & 
           -  & 
                 4*p1p2*p2p4**2   & 
           - 3*p1p4*p2p4**2)   & 
           +  & 
              sqrt(MIN2)*np4* & 
               (5*MIN2**2*p1p4   & 
           - 11*MIN2*p1p2*p1p4   & 
           +  & 
                 9*p1p2**2*p1p4   & 
           + 4*MIN2*p1p4**2   & 
           -  & 
                 12*p1p2*p1p4**2   & 
           + 6*p1p4**3   & 
           +  & 
                 MIN2*p1p2*p2p4   & 
           - p1p2**2*p2p4   & 
           +  & 
                 6*MIN2*p1p4*p2p4   & 
           - 5*p1p2*p1p4*p2p4   & 
           +  & 
                 p1p2*p2p4**2   & 
           + 2*p1p4*p2p4**2)))* & 
         Ccache(2))/ & 
       (3.*p1p4*(p1p4   & 
           - p2p4)**2*Pi**2)   & 
           +  & 
      (4*EL**4*GF**2*sqrt(MIN2)* & 
         (8*MIN2**2.5*MOU2*p1p4   & 
           - 2*MIN2**2*MOU2*np4*p1p4   & 
           -  & 
           16*MIN2**1.5*MOU2*p1p2*p1p4   & 
           -  & 
           2*MIN2*MOU2*np2*p1p2*p1p4   & 
           +  & 
           5*MIN2*MOU2*np4*p1p2*p1p4   & 
           +  & 
           8*sqrt(MIN2)*MOU2*p1p2**2*p1p4   & 
           -  & 
           MOU2*np4*p1p2**2*p1p4   & 
           - MIN2*MOU2*np4*p1p4**2   & 
           -  & 
           7*sqrt(MIN2)*MOU2*p1p2*p1p4**2   & 
           +  & 
           MOU2*np2*p1p2*p1p4**2   & 
           - MOU2*np4*p1p2*p1p4**2   & 
           +  & 
           5*sqrt(MIN2)*MOU2*p1p4**3   & 
           + MOU2*np2*p1p4**3   & 
           -  & 
           2*sqrt(MIN2)*MOU2**2*p1p4* & 
            (2*MIN2   & 
           - p1p2   & 
           + p1p4)   & 
           - 4*MIN2**2.5*MOU2*p2p4   & 
           +  & 
           2*MIN2**2*MOU2*np4*p2p4   & 
           +  & 
           6*MIN2**1.5*MOU2*p1p2*p2p4   & 
           -  & 
           3*MIN2*MOU2*np4*p1p2*p2p4   & 
           -  & 
           2*sqrt(MIN2)*MOU2*p1p2**2*p2p4   & 
           +  & 
           MOU2*np4*p1p2**2*p2p4   & 
           +  & 
           14*MIN2**1.5*MOU2*p1p4*p2p4   & 
           +  & 
           2*MIN2*MOU2*np2*p1p4*p2p4   & 
           -  & 
           MIN2*MOU2*np4*p1p4*p2p4   & 
           -  & 
           6*sqrt(MIN2)*MOU2*p1p2*p1p4*p2p4   & 
           -  & 
           MOU2*np2*p1p2*p1p4*p2p4   & 
           +  & 
           MOU2*np4*p1p2*p1p4*p2p4   & 
           - MOU2*np2*p1p4**2*p2p4   & 
           -  & 
           4*MIN2**1.5*MOU2*p2p4**2   & 
           +  & 
           sqrt(MIN2)*MOU2*p1p2*p2p4**2   & 
           +  & 
           sqrt(MIN2)*MOU2*p1p4*p2p4**2   & 
           -  & 
           CMPLX(aa,bb)*asym*(p1p4   & 
           - p2p4)* & 
            (  & 
           -2*MIN2**2   & 
           + 2*MIN2*MOU2   & 
           + 5*MIN2*p1p2   & 
           -  & 
              MOU2*p1p2   & 
           - 4*p1p2**2   & 
           + MIN2*p1p4   & 
           -  & 
              MOU2*p1p4   & 
           - 4*MIN2*p2p4   & 
           + 4*p1p2*p2p4)   & 
           -  & 
           (p1p4   & 
           - p2p4)* & 
            (4*MIN2**3.5   & 
           - 2*MIN2**3*np4   & 
           -  & 
              2*MIN2**2*np2*p1p2   & 
           + 9*MIN2**2*np4*p1p2   & 
           +  & 
              2*MIN2*np2*p1p2**2   & 
           - 11*MIN2*np4*p1p2**2   & 
           +  & 
              4*np4*p1p2**3   & 
           - MIN2**2*np4*p1p4   & 
           +  & 
              5*MIN2*np2*p1p2*p1p4   & 
           + MIN2*np4*p1p2*p1p4   & 
           -  & 
              4*np2*p1p2**2*p1p4   & 
           - MIN2*np2*p1p4**2   & 
           -  & 
              2*MIN2**2.5*(7*p1p2   & 
           + p1p4   & 
           - 6*p2p4)   & 
           -  & 
              4*MIN2**2*np4*p2p4   & 
           - 2*MIN2*np2*p1p2*p2p4   & 
           +  & 
              10*MIN2*np4*p1p2*p2p4   & 
           - 4*np4*p1p2**2*p2p4   & 
           -  & 
              2*MIN2*np2*p1p4*p2p4   & 
           - 2*MIN2*np4*p1p4*p2p4   & 
           +  & 
              4*np2*p1p2*p1p4*p2p4   & 
           -  & 
              MIN2**1.5* & 
               (  & 
           -16*p1p2**2   & 
           - p1p2*p1p4   & 
           - 5*p1p4**2   & 
           +  & 
                 23*p1p2*p2p4   & 
           + 3*p1p4*p2p4   & 
           - 8*p2p4**2)   & 
           -  & 
              2*sqrt(MIN2)* & 
               (3*p1p2**3   & 
           - p1p2**2*p1p4   & 
           + 4*p1p2*p1p4**2   & 
           -  & 
                 5*p1p2**2*p2p4   & 
           - 3*p1p4**2*p2p4   & 
           +  & 
                 2*p1p2*p2p4**2)))* & 
         Ccache(5))/ & 
       (3.*p1p4*(p1p4   & 
           - p2p4)**2*Pi**2)   & 
           -  & 
      (4*EL**4*GF**2*(  & 
           -2*MIN2*MOU2**3*p1p4   & 
           -  & 
           CMPLX(aa,bb)*asym*sqrt(MIN2)*(p1p4   & 
           - p2p4)* & 
            (3*MIN2**2   & 
           - 4*MIN2*MOU2   & 
           + MOU2**2   & 
           -  & 
              9*MIN2*p1p2   & 
           + 5*MOU2*p1p2   & 
           + 8*p1p2**2   & 
           -  & 
              3*MIN2*p1p4   & 
           + 3*MOU2*p1p4   & 
           + 2*p1p2*p1p4   & 
           +  & 
              2*p1p4**2   & 
           + 8*MIN2*p2p4   & 
           - 4*MOU2*p2p4   & 
           -  & 
              12*p1p2*p2p4   & 
           - 4*p1p4*p2p4   & 
           + 4*p2p4**2)   & 
           -  & 
           MOU2**2*(  & 
           -6*MIN2**2*p1p4   & 
           + 2*MIN2**1.5*np2*p1p4   & 
           +  & 
              MIN2**1.5*np4*p1p4   & 
           + 2*MIN2*p1p2*p1p4   & 
           -  & 
              2*sqrt(MIN2)*np2*p1p2*p1p4   & 
           + 2*p1p2**2*p1p4   & 
           +  & 
              7*MIN2*p1p4**2   & 
           + 3*sqrt(MIN2)*np2*p1p4**2   & 
           -  & 
              3*sqrt(MIN2)*np4*p1p4**2   & 
           - 3*p1p2*p1p4**2   & 
           +  & 
              3*p1p4**3   & 
           + 2*MIN2**2*p2p4   & 
           -  & 
              3*MIN2**1.5*np4*p2p4   & 
           - 2*MIN2*p1p2*p2p4   & 
           +  & 
              2*sqrt(MIN2)*np4*p1p2*p2p4   & 
           -  & 
              8*MIN2*p1p4*p2p4   & 
           - sqrt(MIN2)*np2*p1p4*p2p4   & 
           +  & 
              sqrt(MIN2)*np4*p1p4*p2p4   & 
           - p1p2*p1p4*p2p4   & 
           -  & 
              p1p4**2*p2p4   & 
           + MIN2*p2p4**2)   & 
           -  & 
           MOU2*(6*MIN2**3*p1p4   & 
           - 4*MIN2**2*p1p2*p1p4   & 
           -  & 
              12*MIN2*p1p2**2*p1p4   & 
           + 10*p1p2**3*p1p4   & 
           -  & 
              14*MIN2**2*p1p4**2   & 
           + 20*MIN2*p1p2*p1p4**2   & 
           -  & 
              16*p1p2**2*p1p4**2   & 
           + 20*p1p2*p1p4**3   & 
           -  & 
              6*p1p4**4   & 
           - 4*MIN2**3*p2p4   & 
           +  & 
              4*MIN2**2*p1p2*p2p4   & 
           + 2*MIN2*p1p2**2*p2p4   & 
           -  & 
              2*p1p2**3*p2p4   & 
           + 22*MIN2**2*p1p4*p2p4   & 
           -  & 
              MIN2*p1p2*p1p4*p2p4   & 
           - 10*p1p2**2*p1p4*p2p4   & 
           -  & 
              21*MIN2*p1p4**2*p2p4   & 
           - 2*p1p2*p1p4**2*p2p4   & 
           -  & 
              2*p1p4**3*p2p4   & 
           - 8*MIN2**2*p2p4**2   & 
           +  & 
              MIN2*p1p2*p2p4**2   & 
           + 2*p1p2**2*p2p4**2   & 
           +  & 
              15*MIN2*p1p4*p2p4**2   & 
           + 6*p1p2*p1p4*p2p4**2   & 
           -  & 
              4*MIN2*p2p4**3   & 
           +  & 
              sqrt(MIN2)*np4* & 
               (  & 
           -2*MIN2**2*p1p4   & 
           + 4*MIN2*p1p2*p1p4   & 
           +  & 
                 p1p2**2*p1p4   & 
           + 6*MIN2*p1p4**2   & 
           -  & 
                 15*p1p2*p1p4**2   & 
           + 6*p1p4**3   & 
           +  & 
                 4*MIN2**2*p2p4   & 
           - 10*MIN2*p1p2*p2p4   & 
           +  & 
                 7*p1p2**2*p2p4   & 
           - 8*MIN2*p1p4*p2p4   & 
           +  & 
                 7*p1p2*p1p4*p2p4   & 
           + 2*p1p4**2*p2p4   & 
           +  & 
                 8*MIN2*p2p4**2   & 
           - 8*p1p2*p2p4**2)   & 
           +  & 
              sqrt(MIN2)*np2* & 
               (  & 
           -4*MIN2**2*p1p4   & 
           + 10*MIN2*p1p2*p1p4   & 
           -  & 
                 10*p1p2**2*p1p4   & 
           - 4*MIN2*p1p4**2   & 
           +  & 
                 15*p1p2*p1p4**2   & 
           - 5*p1p4**3   & 
           +  & 
                 2*MIN2**2*p2p4   & 
           - 4*MIN2*p1p2*p2p4   & 
           +  & 
                 2*p1p2**2*p2p4   & 
           - 6*MIN2*p1p4*p2p4   & 
           +  & 
                 3*p1p2*p1p4*p2p4   & 
           - 5*p1p4**2*p2p4   & 
           +  & 
                 4*MIN2*p2p4**2   & 
           - 2*p1p2*p2p4**2   & 
           +  & 
                 2*p1p4*p2p4**2))   & 
           -  & 
           (p1p4   & 
           - p2p4)* & 
            (  & 
           -2*MIN2**4   & 
           + 2*MIN2**3*p1p2   & 
           +  & 
              10*MIN2**2*p1p2**2   & 
           - 18*MIN2*p1p2**3   & 
           +  & 
              8*p1p2**4   & 
           + 7*MIN2**3*p1p4   & 
           -  & 
              17*MIN2**2*p1p2*p1p4   & 
           + 16*MIN2*p1p2**2*p1p4   & 
           -  & 
              8*p1p2**3*p1p4   & 
           - 3*MIN2**2*p1p4**2   & 
           -  & 
              8*MIN2*p1p2*p1p4**2   & 
           + 16*p1p2**2*p1p4**2   & 
           +  & 
              6*MIN2*p1p4**3   & 
           - 8*p1p2*p1p4**3   & 
           -  & 
              7*MIN2**3*p2p4   & 
           - 3*MIN2**2*p1p2*p2p4   & 
           +  & 
              28*MIN2*p1p2**2*p2p4   & 
           - 16*p1p2**3*p2p4   & 
           +  & 
              19*MIN2**2*p1p4*p2p4   & 
           -  & 
              20*MIN2*p1p2*p1p4*p2p4   & 
           + 8*p1p2**2*p1p4*p2p4   & 
           -  & 
              4*MIN2*p1p4**2*p2p4   & 
           - 16*p1p2*p1p4**2*p2p4   & 
           +  & 
              8*p1p4**3*p2p4   & 
           - 8*MIN2**2*p2p4**2   & 
           -  & 
              8*MIN2*p1p2*p2p4**2   & 
           + 8*p1p2**2*p2p4**2   & 
           +  & 
              12*MIN2*p1p4*p2p4**2   & 
           - 4*MIN2*p2p4**3   & 
           +  & 
              sqrt(MIN2)*np4* & 
               (MIN2**3   & 
           - 4*MIN2**2*p1p2   & 
           + 3*MIN2*p1p2**2   & 
           -  & 
                 3*MIN2**2*p1p4   & 
           + 15*MIN2*p1p2*p1p4   & 
           -  & 
                 14*p1p2**2*p1p4   & 
           - 6*MIN2*p1p4**2   & 
           +  & 
                 10*p1p2*p1p4**2   & 
           + 4*MIN2**2*p2p4   & 
           -  & 
                 10*MIN2*p1p2*p2p4   & 
           + 4*p1p2**2*p2p4   & 
           -  & 
                 6*MIN2*p1p4*p2p4   & 
           + 12*p1p2*p1p4*p2p4   & 
           -  & 
                 8*p1p4**2*p2p4   & 
           + 4*MIN2*p2p4**2   & 
           -  & 
                 4*p1p2*p2p4**2)   & 
           +  & 
              sqrt(MIN2)*np2* & 
               (2*MIN2**3   & 
           - 8*MIN2**2*p1p2   & 
           +  & 
                 14*MIN2*p1p2**2   & 
           - 8*p1p2**3   & 
           +  & 
                 MIN2**2*p1p4   & 
           - 11*MIN2*p1p2*p1p4   & 
           +  & 
                 8*p1p2**2*p1p4   & 
           + 5*MIN2*p1p4**2   & 
           -  & 
                 2*p1p2*p1p4**2   & 
           - 2*p1p4**3   & 
           +  & 
                 8*MIN2**2*p2p4   & 
           - 20*MIN2*p1p2*p2p4   & 
           +  & 
                 16*p1p2**2*p2p4   & 
           + 4*MIN2*p1p4*p2p4   & 
           -  & 
                 12*p1p2*p1p4*p2p4   & 
           + 4*p1p4**2*p2p4   & 
           +  & 
                 8*MIN2*p2p4**2   & 
           - 8*p1p2*p2p4**2   & 
           +  & 
                 4*p1p4*p2p4**2)))* & 
         Ccache(6))/ & 
       (3.*p1p4*(p1p4   & 
           - p2p4)**2*Pi**2)   & 
           +  & 
      (2*EL**4*GF**2*(  & 
           -MIN2   & 
           + MOU2   & 
           + 2*p1p2   & 
           - 2*p2p4)* & 
         (  & 
           -6*MIN2**3*p1p4   & 
           + 12*MIN2**2*MOU2*p1p4   & 
           -  & 
           6*MIN2*MOU2**2*p1p4   & 
           + MIN2**2.5*np4*p1p4   & 
           -  & 
           2*MIN2**1.5*MOU2*np4*p1p4   & 
           +  & 
           sqrt(MIN2)*MOU2**2*np4*p1p4   & 
           +  & 
           20*MIN2**2*p1p2*p1p4   & 
           - 22*MIN2*MOU2*p1p2*p1p4   & 
           +  & 
           2*MOU2**2*p1p2*p1p4   & 
           - 5*MIN2**1.5*np4*p1p2*p1p4   & 
           +  & 
           5*sqrt(MIN2)*MOU2*np4*p1p2*p1p4   & 
           -  & 
           22*MIN2*p1p2**2*p1p4   & 
           + 10*MOU2*p1p2**2*p1p4   & 
           +  & 
           4*sqrt(MIN2)*np4*p1p2**2*p1p4   & 
           + 8*p1p2**3*p1p4   & 
           +  & 
           5*MIN2**2*p1p4**2   & 
           - 4*MIN2*MOU2*p1p4**2   & 
           -  & 
           MOU2**2*p1p4**2   & 
           + 6*MIN2**1.5*np4*p1p4**2   & 
           -  & 
           6*sqrt(MIN2)*MOU2*np4*p1p4**2   & 
           -  & 
           6*MIN2*p1p2*p1p4**2   & 
           - 6*MOU2*p1p2*p1p4**2   & 
           -  & 
           10*sqrt(MIN2)*np4*p1p2*p1p4**2   & 
           - 6*MIN2*p1p4**3   & 
           +  & 
           6*MOU2*p1p4**3   & 
           + 8*p1p2*p1p4**3   & 
           +  & 
           6*MIN2**3*p2p4   & 
           - 6*MIN2**2*MOU2*p2p4   & 
           -  & 
           MIN2**2.5*np4*p2p4   & 
           +  & 
           sqrt(MIN2)*MOU2**2*np4*p2p4   & 
           -  & 
           20*MIN2**2*p1p2*p2p4   & 
           + 8*MIN2*MOU2*p1p2*p2p4   & 
           +  & 
           5*MIN2**1.5*np4*p1p2*p2p4   & 
           +  & 
           3*sqrt(MIN2)*MOU2*np4*p1p2*p2p4   & 
           +  & 
           22*MIN2*p1p2**2*p2p4   & 
           - 2*MOU2*p1p2**2*p2p4   & 
           -  & 
           4*sqrt(MIN2)*np4*p1p2**2*p2p4   & 
           - 8*p1p2**3*p2p4   & 
           -  & 
           24*MIN2**2*p1p4*p2p4   & 
           + 25*MIN2*MOU2*p1p4*p2p4   & 
           -  & 
           MOU2**2*p1p4*p2p4   & 
           - 4*MIN2**1.5*np4*p1p4*p2p4   & 
           +  & 
           2*sqrt(MIN2)*MOU2*np4*p1p4*p2p4   & 
           +  & 
           42*MIN2*p1p2*p1p4*p2p4   & 
           - 12*MOU2*p1p2*p1p4*p2p4   & 
           +  & 
           6*sqrt(MIN2)*np4*p1p2*p1p4*p2p4   & 
           -  & 
           16*p1p2**2*p1p4*p2p4   & 
           + 14*MIN2*p1p4**2*p2p4   & 
           -  & 
           2*MOU2*p1p4**2*p2p4   & 
           +  & 
           8*sqrt(MIN2)*np4*p1p4**2*p2p4   & 
           -  & 
           8*p1p2*p1p4**2*p2p4   & 
           - 8*p1p4**3*p2p4   & 
           +  & 
           19*MIN2**2*p2p4**2   & 
           - 7*MIN2*MOU2*p2p4**2   & 
           -  & 
           2*MIN2**1.5*np4*p2p4**2   & 
           -  & 
           4*sqrt(MIN2)*MOU2*np4*p2p4**2   & 
           -  & 
           36*MIN2*p1p2*p2p4**2   & 
           + 2*MOU2*p1p2*p2p4**2   & 
           +  & 
           4*sqrt(MIN2)*np4*p1p2*p2p4**2   & 
           +  & 
           16*p1p2**2*p2p4**2   & 
           - 24*MIN2*p1p4*p2p4**2   & 
           +  & 
           4*MOU2*p1p4*p2p4**2   & 
           -  & 
           8*sqrt(MIN2)*np4*p1p4*p2p4**2   & 
           +  & 
           8*p1p2*p1p4*p2p4**2   & 
           + 8*p1p4**2*p2p4**2   & 
           +  & 
           16*MIN2*p2p4**3   & 
           - 8*p1p2*p2p4**3   & 
           +  & 
           CMPLX(aa,bb)*asym*sqrt(MIN2)* & 
            (MOU2*p1p4   & 
           - MOU2*p2p4   & 
           -  & 
              (p1p4   & 
           - p2p4)*(MIN2   & 
           - 4*p1p2   & 
           + 2*p1p4   & 
           + 4*p2p4) & 
              )   & 
           + sqrt(MIN2)*np2* & 
            (4*MIN2*MOU2*p1p4   & 
           - 2*MOU2**2*p1p4   & 
           -  & 
              10*MOU2*p1p2*p1p4   & 
           + MOU2*p1p4**2   & 
           -  & 
              2*MIN2*MOU2*p2p4   & 
           + 2*MOU2*p1p2*p2p4   & 
           +  & 
              9*MOU2*p1p4*p2p4   & 
           - 2*MOU2*p2p4**2   & 
           +  & 
              (p1p4   & 
           - p2p4)* & 
               (  & 
           -2*MIN2**2   & 
           + 10*MIN2*p1p2   & 
           - 8*p1p2**2   & 
           -  & 
                 MIN2*p1p4   & 
           - 4*p1p2*p1p4   & 
           + 2*p1p4**2   & 
           -  & 
                 8*MIN2*p2p4   & 
           + 16*p1p2*p2p4   & 
           + 4*p1p4*p2p4   & 
           -  & 
                 8*p2p4**2)))* & 
         Ccache(3))/ & 
       (3.*p1p4*(p1p4   & 
           - p2p4)**2*Pi**2)   & 
           +  & 
      (4*EL**4*GF**2*(  & 
           -MIN2   & 
           + MOU2   & 
           + 2*p1p2   & 
           - 2*p2p4)**2* & 
         (CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4   & 
           + 2*MIN2**2*p1p4   & 
           -  & 
           2*MIN2*MOU2*p1p4   & 
           + 2*MIN2**1.5*np2*p1p4   & 
           -  & 
           2*sqrt(MIN2)*MOU2*np2*p1p4   & 
           - MIN2**1.5*np4*p1p4   & 
           +  & 
           sqrt(MIN2)*MOU2*np4*p1p4   & 
           - 4*MIN2*p1p2*p1p4   & 
           +  & 
           2*MOU2*p1p2*p1p4   & 
           - 2*sqrt(MIN2)*np2*p1p2*p1p4   & 
           +  & 
           sqrt(MIN2)*np4*p1p2*p1p4   & 
           + 2*p1p2**2*p1p4   & 
           -  & 
           MIN2*p1p4**2   & 
           - MOU2*p1p4**2   & 
           -  & 
           sqrt(MIN2)*np2*p1p4**2   & 
           -  & 
           2*sqrt(MIN2)*np4*p1p4**2   & 
           + 2*p1p4**3   & 
           -  & 
           CMPLX(aa,bb)*asym*sqrt(MIN2)*p2p4   & 
           - 2*MIN2**2*p2p4   & 
           -  & 
           2*MIN2**1.5*np2*p2p4   & 
           + MIN2**1.5*np4*p2p4   & 
           +  & 
           sqrt(MIN2)*MOU2*np4*p2p4   & 
           + 4*MIN2*p1p2*p2p4   & 
           +  & 
           2*sqrt(MIN2)*np2*p1p2*p2p4   & 
           -  & 
           sqrt(MIN2)*np4*p1p2*p2p4   & 
           - 2*p1p2**2*p2p4   & 
           +  & 
           4*MIN2*p1p4*p2p4   & 
           - MOU2*p1p4*p2p4   & 
           +  & 
           3*sqrt(MIN2)*np2*p1p4*p2p4   & 
           +  & 
           2*sqrt(MIN2)*np4*p1p4*p2p4   & 
           - 2*p1p2*p1p4*p2p4   & 
           -  & 
           2*p1p4**2*p2p4   & 
           - 3*MIN2*p2p4**2   & 
           -  & 
           2*sqrt(MIN2)*np2*p2p4**2   & 
           + 2*p1p2*p2p4**2)* & 
         Ccache(7))/ & 
       (3.*p1p4*(p1p4   & 
           - p2p4)**2*Pi**2))
!
!
      return
!
      end function

                          !!!!!!!!!!!!!!!!!!!!!!
                            END MODULE rad_T2TRIAD
                          !!!!!!!!!!!!!!!!!!!!!!
