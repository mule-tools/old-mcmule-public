                          !!!!!!!!!!!!!!!!!!!!!!
                              MODULE misc_EE2NNGG
                          !!!!!!!!!!!!!!!!!!!!!!
    use functions
    implicit none
    real(kind=prec) :: tscms, m2

  contains

  FUNCTION IFD11(se2, sp2, se3, sp3, s23)
  real(kind=prec) :: s23, se2, se3, sp2, sp3
  real(kind=prec) :: ifd11,if11
  real(kind=prec) :: den1
  if11 = 2*(16*m2**3*tscms + 8*m2**2*tscms**2 + 16*m2**3*s23 + 24*m2**2*tscms*s23 + 12*m2**2&
         &*s23**2 - 2*m2*tscms*s23**2 - 16*m2**3*se2 - 16*m2**2*tscms*se2 - 24*m2**2*s23*se2&
         & + 2*m2*s23**2*se2 + 8*m2**2*se2**2 - 16*m2**3*se3 - 16*m2**2*tscms*se3 - 24*m2**&
         &2*s23*se3 + 4*m2*tscms*s23*se3 + 4*m2*s23**2*se3 + 16*m2**2*se2*se3 - 4*m2*s23*se&
         &2*se3 + 8*m2**2*se3**2 - 4*m2*s23*se3**2 - 16*m2**3*sp2 - 32*m2**2*tscms*sp2 - 4*&
         &m2*tscms**2*sp2 - 32*m2**2*s23*sp2 - 20*m2*tscms*s23*sp2 - 2*tscms**2*s23*sp2 - 16*&
         &m2*s23**2*sp2 - 2*tscms*s23**2*sp2 + 32*m2**2*se2*sp2 + 8*m2*tscms*se2*sp2 + 20*m2&
         &*s23*se2*sp2 + 2*tscms*s23*se2*sp2 + s23**2*se2*sp2 - 4*m2*se2**2*sp2 + 32*m2**2*&
         &se3*sp2 + 4*m2*tscms*se3*sp2 + 16*m2*s23*se3*sp2 + 4*tscms*s23*se3*sp2 + 2*s23**2*&
         &se3*sp2 - 4*m2*se2*se3*sp2 - 2*s23*se2*se3*sp2 - 2*s23*se3**2*sp2 + 20*m2**2*sp2&
         &**2 + 22*m2*tscms*sp2**2 + 2*tscms**2*sp2**2 + 32*m2*s23*sp2**2 + 4*tscms*s23*sp2**&
         &2 - 22*m2*se2*sp2**2 - 2*tscms*se2*sp2**2 - 2*s23*se2*sp2**2 - 20*m2*se3*sp2**2 -&
         & 4*tscms*se3*sp2**2 - 4*s23*se3*sp2**2 + 2*se2*se3*sp2**2 + 2*se3**2*sp2**2 - 16*&
         &m2*sp2**3 - 2*tscms*sp2**3 + se2*sp2**3 + 2*se3*sp2**3 - 16*m2**3*sp3 - 24*m2**2*&
         &tscms*sp3 - 24*m2**2*s23*sp3 + 4*m2*tscms*s23*sp3 + 24*m2**2*se2*sp3 - 4*m2*s23*se&
         &2*sp3 + 24*m2**2*se3*sp3 - 4*m2*tscms*se3*sp3 - 8*m2*s23*se3*sp3 + 4*m2*se2*se3*s&
         &p3 + 4*m2*se3**2*sp3 + 32*m2**2*sp2*sp3 + 16*m2*tscms*sp2*sp3 + 28*m2*s23*sp2*sp3&
         & + 2*tscms*s23*sp2*sp3 - 16*m2*se2*sp2*sp3 + 2*tscms*se2*sp2*sp3 - 2*se2**2*sp2*sp&
         &3 - 12*m2*se3*sp2*sp3 - 2*s23*se3*sp2*sp3 - 2*se2*se3*sp2*sp3 - 28*m2*sp2**2*sp3&
         & - 2*tscms*sp2**2*sp3 + 2*se3*sp2**2*sp3 + 12*m2**2*sp3**2 - 2*m2*tscms*sp3**2 + 2&
         &*m2*se2*sp3**2 + 4*m2*se3*sp3**2 - 12*m2*sp2*sp3**2 - se2*sp2*sp3**2)
  den1 = sp2*(-s23 + sp2 + sp3)
  ifd11 = if11/den1**2
  END FUNCTION


  FUNCTION IFD12(se2, sp2, se3, sp3, s23)
  real(kind=prec) :: s23, se2, se3, sp2, sp3
  real(kind=prec) :: ifd12,if12
  real(kind=prec) :: den1,den2
  if12 = -4*(16*m2**3*tscms + 8*m2**2*tscms**2 + 16*m2**3*s23 + 24*m2**2*tscms*s23 + 12*m2**&
         &2*s23**2 + m2*tscms*s23**2 + 2*tscms**2*s23**2 + m2*s23**3 + tscms*s23**3 - 16*m2**&
         &3*se2 - 16*m2**2*tscms*se2 - 24*m2**2*s23*se2 - m2*s23**2*se2 - 2*tscms*s23**2*se2&
         & + 8*m2**2*se2**2 - 16*m2**3*se3 - 16*m2**2*tscms*se3 - 24*m2**2*s23*se3 - m2*s23&
         &**2*se3 - 2*tscms*s23**2*se3 + 16*m2**2*se2*se3 + 8*m2**2*se3**2 - 16*m2**3*sp2 -&
         & 28*m2**2*tscms*sp2 - 2*m2*tscms**2*sp2 - 28*m2**2*s23*sp2 - 4*m2*tscms*s23*sp2 - 2&
         &*tscms**2*s23*sp2 - 2*m2*s23**2*sp2 - tscms*s23**2*sp2 + 28*m2**2*se2*sp2 + 4*m2*tS&
         &cms*se2*sp2 + 4*m2*s23*se2*sp2 - 2*s23**2*se2*sp2 - 2*m2*se2**2*sp2 + 2*s23*se2*&
         &*2*sp2 + 28*m2**2*se3*sp2 + 4*m2*tscms*se3*sp2 + 4*m2*s23*se3*sp2 - 2*s23**2*se3*&
         &sp2 - 4*m2*se2*se3*sp2 + 4*s23*se2*se3*sp2 - 2*m2*se3**2*sp2 + 2*s23*se3**2*sp2 &
         &+ 16*m2**2*sp2**2 + 3*m2*tscms*sp2**2 + m2*s23*sp2**2 - tscms*s23*sp2**2 - 3*m2*se&
         &2*sp2**2 + 2*tscms*se2*sp2**2 + 4*s23*se2*sp2**2 - 2*se2**2*sp2**2 - 3*m2*se3*sp2&
         &**2 + 2*tscms*se3*sp2**2 + 4*s23*se3*sp2**2 - 4*se2*se3*sp2**2 - 2*se3**2*sp2**2 &
         &+ tscms*sp2**3 - 2*se2*sp2**3 - 2*se3*sp2**3 - 16*m2**3*sp3 - 28*m2**2*tscms*sp3 -&
         & 2*m2*tscms**2*sp3 - 28*m2**2*s23*sp3 - 4*m2*tscms*s23*sp3 - 2*tscms**2*s23*sp3 - 2&
         &*m2*s23**2*sp3 - tscms*s23**2*sp3 + 28*m2**2*se2*sp3 + 4*m2*tscms*se2*sp3 + 4*m2*s&
         &23*se2*sp3 - 2*s23**2*se2*sp3 - 2*m2*se2**2*sp3 + 2*s23*se2**2*sp3 + 28*m2**2*se&
         &3*sp3 + 4*m2*tscms*se3*sp3 + 4*m2*s23*se3*sp3 - 2*s23**2*se3*sp3 - 4*m2*se2*se3*s&
         &p3 + 4*s23*se2*se3*sp3 - 2*m2*se3**2*sp3 + 2*s23*se3**2*sp3 + 32*m2**2*sp2*sp3 +&
         & 6*m2*tscms*sp2*sp3 + 2*m2*s23*sp2*sp3 - 2*tscms*s23*sp2*sp3 - 6*m2*se2*sp2*sp3 + &
         &4*tscms*se2*sp2*sp3 + 8*s23*se2*sp2*sp3 - 4*se2**2*sp2*sp3 - 6*m2*se3*sp2*sp3 + 4&
         &*tscms*se3*sp2*sp3 + 8*s23*se3*sp2*sp3 - 8*se2*se3*sp2*sp3 - 4*se3**2*sp2*sp3 + 3&
         &*tscms*sp2**2*sp3 - 6*se2*sp2**2*sp3 - 6*se3*sp2**2*sp3 + 16*m2**2*sp3**2 + 3*m2*&
         &tscms*sp3**2 + m2*s23*sp3**2 - tscms*s23*sp3**2 - 3*m2*se2*sp3**2 + 2*tscms*se2*sp3&
         &**2 + 4*s23*se2*sp3**2 - 2*se2**2*sp3**2 - 3*m2*se3*sp3**2 + 2*tscms*se3*sp3**2 +&
         & 4*s23*se3*sp3**2 - 4*se2*se3*sp3**2 - 2*se3**2*sp3**2 + 3*tscms*sp2*sp3**2 - 6*s&
         &e2*sp2*sp3**2 - 6*se3*sp2*sp3**2 + tscms*sp3**3 - 2*se2*sp3**3 - 2*se3*sp3**3)
  den1 = sp2*(-s23 + sp2 + sp3)
  den2 = (s23 - sp2 - sp3)*sp3
  ifd12 = if12/den1/den2
  END FUNCTION


  FUNCTION IFD13(se2, sp2, se3, sp3, s23)
  real(kind=prec) :: s23, se2, se3, sp2, sp3
  real(kind=prec) :: ifd13,if13
  real(kind=prec) :: den1,den3
  if13 = 4*(16*m2**3*tscms - 8*m2**2*tscms**2 - 4*m2*tscms**3 + 2*tscms**4 + 16*m2**3*s23 - 1&
         &6*m2**2*tscms*s23 - 14*m2*tscms**2*s23 + 6*tscms**3*s23 - 4*m2**2*s23**2 - 8*m2*tScm&
         &s*s23**2 + 3*tscms**2*s23**2 + 2*m2*s23**3 - 16*m2**3*se2 + 16*m2**2*tscms*se2 + 1&
         &2*m2*tscms**2*se2 - 6*tscms**3*se2 + 12*m2**2*s23*se2 + 24*m2*tscms*s23*se2 - 11*tSc&
         &ms**2*s23*se2 + 3*m2*s23**2*se2 - 3*tscms*s23**2*se2 - 8*m2**2*se2**2 - 8*m2*tscms&
         &*se2**2 + 6*tscms**2*se2**2 - 5*m2*s23*se2**2 + 5*tscms*s23*se2**2 - 2*tscms*se2**3&
         & - 16*m2**3*se3 + 4*m2**2*tscms*se3 + 10*m2*tscms**2*se3 - 4*tscms**3*se3 + 16*m2*tS&
         &cms*s23*se3 - 5*tscms**2*s23*se3 - 2*m2*s23**2*se3 - 4*m2**2*se2*se3 - 16*m2*tscms&
         &*se2*se3 + 8*tscms**2*se2*se3 - 7*m2*s23*se2*se3 + 5*tscms*s23*se2*se3 + 2*m2*se2*&
         &*2*se3 - 4*tscms*se2**2*se3 + 4*m2**2*se3**2 - 6*m2*tscms*se3**2 + 2*tscms**2*se3**&
         &2 + 2*m2*se2*se3**2 - 2*tscms*se2*se3**2 - 16*m2**3*sp2 + 16*m2**2*tscms*sp2 + 12*&
         &m2*tscms**2*sp2 - 6*tscms**3*sp2 + 12*m2**2*s23*sp2 + 24*m2*tscms*s23*sp2 - 11*tscms&
         &**2*s23*sp2 + 3*m2*s23**2*sp2 - 3*tscms*s23**2*sp2 - 12*m2**2*se2*sp2 - 24*m2*tScm&
         &s*se2*sp2 + 11*tscms**2*se2*sp2 - 18*m2*s23*se2*sp2 + 10*tscms*s23*se2*sp2 + 7*m2*&
         &se2**2*sp2 - 5*tscms*se2**2*sp2 - 20*m2*tscms*se3*sp2 + 7*tscms**2*se3*sp2 - 11*m2*&
         &s23*se3*sp2 + 5*tscms*s23*se3*sp2 + 15*m2*se2*se3*sp2 - 7*tscms*se2*se3*sp2 + 6*m2&
         &*se3**2*sp2 - 2*tscms*se3**2*sp2 - 8*m2**2*sp2**2 - 8*m2*tscms*sp2**2 + 6*tscms**2*&
         &sp2**2 - 5*m2*s23*sp2**2 + 5*tscms*s23*sp2**2 + 7*m2*se2*sp2**2 - 5*tscms*se2*sp2*&
         &*2 + 5*m2*se3*sp2**2 - 3*tscms*se3*sp2**2 - 2*tscms*sp2**3 - 16*m2**3*sp3 + 4*m2**&
         &2*tscms*sp3 + 10*m2*tscms**2*sp3 - 4*tscms**3*sp3 + 16*m2*tscms*s23*sp3 - 5*tscms**2*&
         &s23*sp3 - 2*m2*s23**2*sp3 - 20*m2*tscms*se2*sp3 + 7*tscms**2*se2*sp3 - 11*m2*s23*s&
         &e2*sp3 + 5*tscms*s23*se2*sp3 + 5*m2*se2**2*sp3 - 3*tscms*se2**2*sp3 + 12*m2**2*se3&
         &*sp3 - 12*m2*tscms*se3*sp3 + 3*tscms**2*se3*sp3 + 7*m2*se2*se3*sp3 - 3*tscms*se2*se&
         &3*sp3 - 4*m2**2*sp2*sp3 - 16*m2*tscms*sp2*sp3 + 8*tscms**2*sp2*sp3 - 7*m2*s23*sp2*&
         &sp3 + 5*tscms*s23*sp2*sp3 + 15*m2*se2*sp2*sp3 - 7*tscms*se2*sp2*sp3 + 7*m2*se3*sp2&
         &*sp3 - 3*tscms*se3*sp2*sp3 + 2*m2*sp2**2*sp3 - 4*tscms*sp2**2*sp3 + 4*m2**2*sp3**2&
         & - 6*m2*tscms*sp3**2 + 2*tscms**2*sp3**2 + 6*m2*se2*sp3**2 - 2*tscms*se2*sp3**2 + 2&
         &*m2*sp2*sp3**2 - 2*tscms*sp2*sp3**2)
  den1 = sp2*(-s23 + sp2 + sp3)
  den3 = se2*(-s23 + se2 + se3)
  ifd13 = if13/den1/den3
  END FUNCTION


  FUNCTION IFD14(se2, sp2, se3, sp3, s23)
  real(kind=prec) :: s23, se2, se3, sp2, sp3
  real(kind=prec) :: ifd14,if14
  real(kind=prec) :: den1,den4
  if14 = 4*(16*m2**3*tscms - 4*m2*tscms**3 + 16*m2**3*s23 + 12*m2**2*tscms*s23 - 10*m2*tscms*&
         &*2*s23 + 12*m2**2*s23**2 - 6*m2*tscms*s23**2 - 16*m2**3*se2 - 4*m2**2*tscms*se2 + &
         &8*m2*tscms**2*se2 - 20*m2**2*s23*se2 + 13*m2*tscms*s23*se2 + tscms**2*s23*se2 + 3*m&
         &2*s23**2*se2 + 4*m2**2*se2**2 - 4*m2*tscms*se2**2 - 3*m2*s23*se2**2 - tscms*s23*se&
         &2**2 - 16*m2**3*se3 - 8*m2**2*tscms*se3 + 8*m2*tscms**2*se3 - 20*m2**2*s23*se3 + 1&
         &2*m2*tscms*s23*se3 - tscms**2*s23*se3 + 2*m2*s23**2*se3 - tscms*s23**2*se3 + 12*m2*&
         &*2*se2*se3 - 10*m2*tscms*se2*se3 - 7*m2*s23*se2*se3 + 2*m2*se2**2*se3 + 8*m2**2*s&
         &e3**2 - 4*m2*tscms*se3**2 - 2*m2*s23*se3**2 + tscms*s23*se3**2 + 2*m2*se2*se3**2 -&
         & 16*m2**3*sp2 - 8*m2**2*tscms*sp2 + 12*m2*tscms**2*sp2 - 20*m2**2*s23*sp2 + 16*m2*&
         &tscms*s23*sp2 + 2*m2*s23**2*sp2 + 16*m2**2*se2*sp2 - 11*m2*tscms*se2*sp2 - 2*tscms*&
         &*2*se2*sp2 - 3*m2*s23*se2*sp2 - tscms*s23*se2*sp2 - m2*se2**2*sp2 + 2*tscms*se2**2&
         &*sp2 + 16*m2**2*se3*sp2 - 18*m2*tscms*se3*sp2 - 10*m2*s23*se3*sp2 + tscms*s23*se3*&
         &sp2 + 7*m2*se2*se3*sp2 + 3*tscms*se2*se3*sp2 + s23*se2*se3*sp2 - se2**2*se3*sp2 +&
         & 6*m2*se3**2*sp2 + tscms*se3**2*sp2 + s23*se3**2*sp2 - 2*se2*se3**2*sp2 - se3**3*&
         &sp2 + 8*m2**2*sp2**2 - 8*m2*tscms*sp2**2 - 2*m2*s23*sp2**2 - 2*m2*se2*sp2**2 + tSc&
         &ms*se2*sp2**2 + 6*m2*se3*sp2**2 - se2*se3*sp2**2 - se3**2*sp2**2 - 16*m2**3*sp3 &
         &- 16*m2**2*tscms*sp3 + 12*m2*tscms**2*sp3 - 24*m2**2*s23*sp3 + 16*m2*tscms*s23*sp3 &
         &- 2*tscms**2*s23*sp3 + 2*m2*s23**2*sp3 - tscms*s23**2*sp3 + 24*m2**2*se2*sp3 - 15*&
         &m2*tscms*se2*sp3 - 8*m2*s23*se2*sp3 + 3*tscms*s23*se2*sp3 + 3*m2*se2**2*sp3 - tscms&
         &*se2**2*sp3 - s23*se2**2*sp3 + se2**3*sp3 + 24*m2**2*se3*sp3 - 16*m2*tscms*se3*sp&
         &3 + 2*tscms**2*se3*sp3 - 8*m2*s23*se3*sp3 + 4*tscms*s23*se3*sp3 + 9*m2*se2*se3*sp3&
         & - 3*tscms*se2*se3*sp3 - s23*se2*se3*sp3 + 2*se2**2*se3*sp3 + 4*m2*se3**2*sp3 - 2&
         &*tscms*se3**2*sp3 + se2*se3**2*sp3 + 20*m2**2*sp2*sp3 - 18*m2*tscms*sp2*sp3 + tscms&
         &**2*sp2*sp3 - 7*m2*s23*sp2*sp3 + 2*tscms*s23*sp2*sp3 + 3*m2*se2*sp2*sp3 - tscms*se&
         &2*sp2*sp3 + se2**2*sp2*sp3 + 12*m2*se3*sp2*sp3 - 3*tscms*se3*sp2*sp3 - s23*se3*sp&
         &2*sp3 + se2*se3*sp2*sp3 + 3*m2*sp2**2*sp3 - tscms*sp2**2*sp3 + se3*sp2**2*sp3 + 1&
         &2*m2**2*sp3**2 - 8*m2*tscms*sp3**2 + tscms**2*sp3**2 - 2*m2*s23*sp3**2 + tscms*s23*&
         &sp3**2 + 3*m2*se2*sp3**2 - tscms*se2*sp3**2 + s23*se2*sp3**2 + 4*m2*se3*sp3**2 - &
         &2*tscms*se3*sp3**2 + 3*m2*sp2*sp3**2 - tscms*sp2*sp3**2 - se2*sp2*sp3**2 + se3*sp2&
         &*sp3**2 - se2*sp3**3)
  den1 = sp2*(-s23 + sp2 + sp3)
  den4 = se2*sp3
  ifd14 = if14/den1/den4
  END FUNCTION


  FUNCTION IFD15(se2, sp2, se3, sp3, s23)
  real(kind=prec) :: s23, se2, se3, sp2, sp3
  real(kind=prec) :: ifd15,if15
  real(kind=prec) :: den1,den5
  if15 = 2*(32*m2**3*tscms - 16*m2**2*tscms**2 - 8*m2*tscms**3 + 4*tscms**4 + 32*m2**3*s23 - &
         &32*m2**2*tscms*s23 - 28*m2*tscms**2*s23 + 12*tscms**3*s23 - 8*m2**2*s23**2 - 18*m2*&
         &tscms*s23**2 + 10*tscms**2*s23**2 - 2*m2*s23**3 + tscms*s23**3 - 32*m2**3*se2 + 24*&
         &m2**2*tscms*se2 + 20*m2*tscms**2*se2 - 12*tscms**3*se2 + 16*m2**2*s23*se2 + 38*m2*tS&
         &cms*s23*se2 - 22*tscms**2*s23*se2 + 10*m2*s23**2*se2 - 7*tscms*s23**2*se2 - 8*m2**&
         &2*se2**2 - 16*m2*tscms*se2**2 + 12*tscms**2*se2**2 - 12*m2*s23*se2**2 + 10*tscms*s2&
         &3*se2**2 + 4*m2*se2**3 - 4*tscms*se2**3 - 32*m2**3*se3 + 16*m2**2*tscms*se3 + 24*m&
         &2*tscms**2*se3 - 8*tscms**3*se3 + 8*m2**2*s23*se3 + 38*m2*tscms*s23*se3 - 12*tscms**&
         &2*s23*se3 + 4*m2*s23**2*se3 - tscms*s23**2*se3 - 8*m2**2*se2*se3 - 36*m2*tscms*se2&
         &*se3 + 16*tscms**2*se2*se3 - 18*m2*s23*se2*se3 + 10*tscms*s23*se2*se3 + 12*m2*se2*&
         &*2*se3 - 8*tscms*se2**2*se3 - 16*m2*tscms*se3**2 + 4*tscms**2*se3**2 - 2*m2*s23*se3&
         &**2 + 8*m2*se2*se3**2 - 4*tscms*se2*se3**2 - 32*m2**3*sp2 + 16*m2**2*tscms*sp2 + 2&
         &4*m2*tscms**2*sp2 - 8*tscms**3*sp2 + 8*m2**2*s23*sp2 + 38*m2*tscms*s23*sp2 - 12*tScm&
         &s**2*s23*sp2 + 4*m2*s23**2*sp2 - tscms*s23**2*sp2 - 30*m2*tscms*se2*sp2 + 12*tscms*&
         &*2*se2*sp2 - 16*m2*s23*se2*sp2 + 5*tscms*s23*se2*sp2 + 8*m2*se2**2*sp2 - 4*tscms*s&
         &e2**2*sp2 + 8*m2**2*se3*sp2 - 34*m2*tscms*se3*sp2 + 4*tscms**2*se3*sp2 - 10*m2*s23&
         &*se3*sp2 - 7*tscms*s23*se3*sp2 - s23**2*se3*sp2 + 14*m2*se2*se3*sp2 + 2*tscms*se2*&
         &se3*sp2 + 3*s23*se2*se3*sp2 - 2*se2**2*se3*sp2 + 2*m2*se3**2*sp2 + 4*tscms*se3**2&
         &*sp2 + s23*se3**2*sp2 - 2*se2*se3**2*sp2 - 16*m2*tscms*sp2**2 + 4*tscms**2*sp2**2 &
         &- 2*m2*s23*sp2**2 + 2*m2*se2*sp2**2 + 2*m2*se3*sp2**2 + 4*tscms*se3*sp2**2 + s23*&
         &se3*sp2**2 - se2*se3*sp2**2 + se3**2*sp2**2 - 32*m2**3*sp3 + 24*m2**2*tscms*sp3 +&
         & 20*m2*tscms**2*sp3 - 12*tscms**3*sp3 + 16*m2**2*s23*sp3 + 38*m2*tscms*s23*sp3 - 22&
         &*tscms**2*s23*sp3 + 10*m2*s23**2*sp3 - 7*tscms*s23**2*sp3 - 8*m2**2*se2*sp3 - 26*m&
         &2*tscms*se2*sp3 + 24*tscms**2*se2*sp3 - 22*m2*s23*se2*sp3 + 21*tscms*s23*se2*sp3 + &
         &3*s23**2*se2*sp3 + 8*m2*se2**2*sp3 - 14*tscms*se2**2*sp3 - 5*s23*se2**2*sp3 + 2*s&
         &e2**3*sp3 - 30*m2*tscms*se3*sp3 + 12*tscms**2*se3*sp3 - 16*m2*s23*se3*sp3 + 5*tscms&
         &*s23*se3*sp3 + 14*m2*se2*se3*sp3 - 12*tscms*se2*se3*sp3 - 3*s23*se2*se3*sp3 + 2*s&
         &e2**2*se3*sp3 + 2*m2*se3**2*sp3 - 8*m2**2*sp2*sp3 - 36*m2*tscms*sp2*sp3 + 16*tscms&
         &**2*sp2*sp3 - 18*m2*s23*sp2*sp3 + 10*tscms*s23*sp2*sp3 + 14*m2*se2*sp2*sp3 - 12*tS&
         &cms*se2*sp2*sp3 - 3*s23*se2*sp2*sp3 + 3*se2**2*sp2*sp3 + 14*m2*se3*sp2*sp3 + 2*tS&
         &cms*se3*sp2*sp3 + 3*s23*se3*sp2*sp3 - 2*se2*se3*sp2*sp3 - se3**2*sp2*sp3 + 8*m2*&
         &sp2**2*sp3 - 4*tscms*sp2**2*sp3 - 2*se3*sp2**2*sp3 - 8*m2**2*sp3**2 - 16*m2*tscms*&
         &sp3**2 + 12*tscms**2*sp3**2 - 12*m2*s23*sp3**2 + 10*tscms*s23*sp3**2 + 8*m2*se2*sp&
         &3**2 - 14*tscms*se2*sp3**2 - 5*s23*se2*sp3**2 + 5*se2**2*sp3**2 + 8*m2*se3*sp3**2&
         & - 4*tscms*se3*sp3**2 + 3*se2*se3*sp3**2 + 12*m2*sp2*sp3**2 - 8*tscms*sp2*sp3**2 +&
         & 2*se2*sp2*sp3**2 - 2*se3*sp2*sp3**2 + 4*m2*sp3**3 - 4*tscms*sp3**3 + 2*se2*sp3**&
         &3)
  den1 = sp2*(-s23 + sp2 + sp3)
  den5 = se3*(-s23 + se2 + se3)
  ifd15 = if15/den1/den5
  END FUNCTION


  FUNCTION IFD16(se2, sp2, se3, sp3, s23)
  real(kind=prec) :: s23, se2, se3, sp2, sp3
  real(kind=prec) :: ifd16,if16
  real(kind=prec) :: den1,den6
  if16 = 2*(32*m2**3*tscms - 8*m2*tscms**3 + 32*m2**3*s23 + 8*m2**2*tscms*s23 - 16*m2*tscms**&
         &2*s23 + 8*m2**2*s23**2 - 8*m2*tscms*s23**2 - 32*m2**3*se2 + 24*m2*tscms**2*se2 - 8&
         &*m2**2*s23*se2 + 32*m2*tscms*s23*se2 + 8*m2*s23**2*se2 - 24*m2*tscms*se2**2 - 16*m&
         &2*s23*se2**2 + 8*m2*se2**3 - 32*m2**3*se3 - 8*m2**2*tscms*se3 + 16*m2*tscms**2*se3&
         & - 24*m2**2*s23*se3 + 12*m2*tscms*s23*se3 + 8*m2**2*se2*se3 - 32*m2*tscms*se2*se3 &
         &- 12*m2*s23*se2*se3 + 16*m2*se2**2*se3 + 8*m2**2*se3**2 - 8*m2*tscms*se3**2 + 8*m&
         &2*se2*se3**2 - 32*m2**3*sp2 - 24*m2**2*tscms*sp2 + 16*m2*tscms**2*sp2 - 24*m2**2*s&
         &23*sp2 + 18*m2*tscms*s23*sp2 - 2*m2*s23**2*sp2 - tscms*s23**2*sp2 + 24*m2**2*se2*s&
         &p2 - 32*m2*tscms*se2*sp2 + 4*tscms**2*se2*sp2 - 16*m2*s23*se2*sp2 + 8*tscms*s23*se2&
         &*sp2 + 4*s23**2*se2*sp2 + 16*m2*se2**2*sp2 - 8*tscms*se2**2*sp2 - 8*s23*se2**2*sp&
         &2 + 4*se2**3*sp2 + 40*m2**2*se3*sp2 - 16*m2*tscms*se3*sp2 + 2*m2*s23*se3*sp2 + 16&
         &*m2*se2*se3*sp2 - 8*tscms*se2*se3*sp2 - 6*s23*se2*se3*sp2 + 8*se2**2*se3*sp2 + 4*&
         &m2*se3**2*sp2 + 4*se2*se3**2*sp2 + 16*m2**2*sp2**2 - 10*m2*tscms*sp2**2 + tscms*s2&
         &3*sp2**2 + 8*m2*se2*sp2**2 - 4*tscms*se2*sp2**2 - 4*s23*se2*sp2**2 + 4*se2**2*sp2&
         &**2 - 2*m2*se3*sp2**2 + s23*se3*sp2**2 + 2*se2*se3*sp2**2 + 2*m2*sp2**3 - se3*sp&
         &2**3 - 32*m2**3*sp3 - 8*m2**2*tscms*sp3 + 16*m2*tscms**2*sp3 - 16*m2**2*s23*sp3 + &
         &16*m2*tscms*s23*sp3 + 8*m2**2*se2*sp3 - 32*m2*tscms*se2*sp3 - 16*m2*s23*se2*sp3 + &
         &16*m2*se2**2*sp3 + 24*m2**2*se3*sp3 - 12*m2*tscms*se3*sp3 + 12*m2*se2*se3*sp3 + 2&
         &4*m2**2*sp2*sp3 - 18*m2*tscms*sp2*sp3 + 2*m2*s23*sp2*sp3 + tscms*s23*sp2*sp3 + 16*&
         &m2*se2*sp2*sp3 - 8*tscms*se2*sp2*sp3 - 7*s23*se2*sp2*sp3 + 8*se2**2*sp2*sp3 - 2*m&
         &2*se3*sp2*sp3 + 6*se2*se3*sp2*sp3 + 2*m2*sp2**2*sp3 + 3*se2*sp2**2*sp3 - se3*sp2&
         &**2*sp3 + 8*m2**2*sp3**2 - 8*m2*tscms*sp3**2 + 8*m2*se2*sp3**2 + 3*se2*sp2*sp3**2&
         &)
  den1 = sp2*(-s23 + sp2 + sp3)
  den6 = se3*sp2
  ifd16 = if16/den1/den6
  END FUNCTION


  FUNCTION IFD44(se2, sp2, se3, sp3, s23)
  real(kind=prec) :: s23, se2, se3, sp2, sp3
  real(kind=prec) :: ifd44,if44
  real(kind=prec) :: den4
  if44 = 2*(16*m2**3*tscms + 8*m2**2*tscms**2 + 16*m2**3*s23 + 16*m2**2*tscms*s23 + 8*m2**2*&
         &s23**2 - 16*m2**3*se2 - 24*m2**2*tscms*se2 - 24*m2**2*s23*se2 + 4*m2*tscms*s23*se2&
         & + 4*m2*s23**2*se2 + 12*m2**2*se2**2 - 2*m2*tscms*se2**2 - 4*m2*s23*se2**2 - 16*m&
         &2**3*se3 - 16*m2**2*tscms*se3 - 16*m2**2*s23*se3 + 24*m2**2*se2*se3 - 4*m2*s23*se&
         &2*se3 + 2*m2*se2**2*se3 + 8*m2**2*se3**2 - 16*m2**3*sp2 - 16*m2**2*tscms*sp2 - 16&
         &*m2**2*s23*sp2 + 24*m2**2*se2*sp2 - 4*m2*tscms*se2*sp2 - 8*m2*s23*se2*sp2 + 4*m2*&
         &se2**2*sp2 + 16*m2**2*se3*sp2 + 4*m2*se2*se3*sp2 + 8*m2**2*sp2**2 + 4*m2*se2*sp2&
         &**2 - 16*m2**3*sp3 - 24*m2**2*tscms*sp3 - 24*m2**2*s23*sp3 + 4*m2*tscms*s23*sp3 + &
         &4*m2*s23**2*sp3 + 40*m2**2*se2*sp3 + 6*m2*tscms*se2*sp3 - 6*m2*s23*se2*sp3 + tscms&
         &*s23*se2*sp3 + 2*s23**2*se2*sp3 - 2*m2*se2**2*sp3 - 2*s23*se2**2*sp3 + 24*m2**2*&
         &se3*sp3 - 4*m2*tscms*se3*sp3 - 8*m2*s23*se3*sp3 - 2*s23*se2*se3*sp3 + se2**2*se3*&
         &sp3 + 4*m2*se3**2*sp3 + 24*m2**2*sp2*sp3 - 4*m2*s23*sp2*sp3 - 2*s23*se2*sp2*sp3 &
         &+ 4*m2*se3*sp2*sp3 + se2*se3*sp2*sp3 + 12*m2**2*sp3**2 - 2*m2*tscms*sp3**2 - 4*m2&
         &*s23*sp3**2 - 2*m2*se2*sp3**2 - 2*s23*se2*sp3**2 + se2**2*sp3**2 + 4*m2*se3*sp3*&
         &*2 + 2*m2*sp2*sp3**2 + se2*sp2*sp3**2)
  den4 = se2*sp3
  ifd44 = if44/den4**2
  END FUNCTION


  FUNCTION IFD46(se2, sp2, se3, sp3, s23)
  real(kind=prec) :: s23, se2, se3, sp2, sp3
  real(kind=prec) :: ifd46,if46
  real(kind=prec) :: den4,den6
  if46 = 4*(16*m2**3*tscms - 8*m2**2*tscms**2 - 4*m2*tscms**3 + 2*tscms**4 + 16*m2**3*s23 + 8&
         &*m2**2*tscms*s23 - 6*m2*tscms**2*s23 + 2*tscms**3*s23 + 16*m2**2*s23**2 + 2*m2*s23*&
         &*3 - 16*m2**3*se2 + 4*m2**2*tscms*se2 + 10*m2*tscms**2*se2 - 4*tscms**3*se2 - 20*m2&
         &**2*s23*se2 + 8*m2*tscms*s23*se2 - 2*tscms**2*s23*se2 - 2*m2*s23**2*se2 + 4*m2**2*&
         &se2**2 - 6*m2*tscms*se2**2 + 2*tscms**2*se2**2 - 16*m2**3*se3 + 4*m2**2*tscms*se3 +&
         & 10*m2*tscms**2*se3 - 4*tscms**3*se3 - 20*m2**2*s23*se3 + 8*m2*tscms*s23*se3 - 2*tSc&
         &ms**2*s23*se3 - 2*m2*s23**2*se3 + 4*m2**2*se2*se3 - 19*m2*tscms*se2*se3 + 6*tscms*&
         &*2*se2*se3 - 7*m2*s23*se2*se3 + tscms*s23*se2*se3 + 7*m2*se2**2*se3 - 2*tscms*se2*&
         &*2*se3 + 4*m2**2*se3**2 - 6*m2*tscms*se3**2 + 2*tscms**2*se3**2 + 7*m2*se2*se3**2 &
         &- 2*tscms*se2*se3**2 - 16*m2**3*sp2 + 4*m2**2*tscms*sp2 + 10*m2*tscms**2*sp2 - 4*tSc&
         &ms**3*sp2 - 20*m2**2*s23*sp2 + 8*m2*tscms*s23*sp2 - 2*tscms**2*s23*sp2 - 2*m2*s23*&
         &*2*sp2 + 12*m2**2*se2*sp2 - 12*m2*tscms*se2*sp2 + 3*tscms**2*se2*sp2 + 8*m2**2*se3&
         &*sp2 - 16*m2*tscms*se3*sp2 + 6*tscms**2*se3*sp2 - 4*m2*s23*se3*sp2 + 2*tscms*s23*se&
         &3*sp2 + 11*m2*se2*se3*sp2 - 2*tscms*se2*se3*sp2 + 4*m2*se3**2*sp2 - 2*tscms*se3**2&
         &*sp2 + 4*m2**2*sp2**2 - 6*m2*tscms*sp2**2 + 2*tscms**2*sp2**2 + 4*m2*se3*sp2**2 - &
         &2*tscms*se3*sp2**2 - 16*m2**3*sp3 + 4*m2**2*tscms*sp3 + 10*m2*tscms**2*sp3 - 4*tscms&
         &**3*sp3 - 20*m2**2*s23*sp3 + 8*m2*tscms*s23*sp3 - 2*tscms**2*s23*sp3 - 2*m2*s23**2&
         &*sp3 + 8*m2**2*se2*sp3 - 16*m2*tscms*se2*sp3 + 6*tscms**2*se2*sp3 - 4*m2*s23*se2*s&
         &p3 + 2*tscms*s23*se2*sp3 + 4*m2*se2**2*sp3 - 2*tscms*se2**2*sp3 + 12*m2**2*se3*sp3&
         & - 12*m2*tscms*se3*sp3 + 3*tscms**2*se3*sp3 + 11*m2*se2*se3*sp3 - 2*tscms*se2*se3*s&
         &p3 + 4*m2**2*sp2*sp3 - 19*m2*tscms*sp2*sp3 + 6*tscms**2*sp2*sp3 - 7*m2*s23*sp2*sp3&
         & + tscms*s23*sp2*sp3 + 11*m2*se2*sp2*sp3 - 2*tscms*se2*sp2*sp3 + 11*m2*se3*sp2*sp3&
         & - 2*tscms*se3*sp2*sp3 + 7*m2*sp2**2*sp3 - 2*tscms*sp2**2*sp3 + 4*m2**2*sp3**2 - 6&
         &*m2*tscms*sp3**2 + 2*tscms**2*sp3**2 + 4*m2*se2*sp3**2 - 2*tscms*se2*sp3**2 + 7*m2*&
         &sp2*sp3**2 - 2*tscms*sp2*sp3**2)
  den4 = se2*sp3
  den6 = se3*sp2
  ifd46 = if46/den4/den6
  END FUNCTION



  FUNCTION EE2NNGGav(p1, p2,p3,p4, q2,q3)
    !! e+(p1) e-(p2) -> y*(p1+p2) -> nu(p3) nu(p4) g(q2) g(q3)
    !! e-(p1) e+(p2) -> y*(p1+p2) -> nu(p3) nu(p4) g(q2) g(q3)
    !! for massive electrons
    !! average over neutrino tensor taken
  real (kind=prec) :: p1(4), p2(4), q2(4), q3(4),p3(4),p4(4)
  real (kind=prec) :: se2, sp2, se3, sp3, s23
  real (kind=prec) :: ee2nnggav

  m2=sq(p1) ; tscms=sq(p1+p2)
  se2 = s(p1,q2) ; sp2 = s(p2,q2)
  se3 = s(p1,q3) ; sp3 = s(p2,q3)
  s23 = s(q2,q3)

  ee2nnggav = ifd11(se2,sp2,se3,sp3,s23) + ifd11(se3,sp3,se2,sp2,s23) + &
              &ifd11(sp2,se2,sp3,se3,s23) + ifd11(sp3,se3,sp2,se2,s23) + &
              &ifd12(se2,sp2,se3,sp3,s23) + ifd12(sp2,se2,sp3,se3,s23) + &
              &ifd13(se2,sp2,se3,sp3,s23) + ifd13(se3,sp3,se2,sp2,s23) + &
              &ifd14(se2,sp2,se3,sp3,s23) + ifd14(se3,sp3,se2,sp2,s23) + &
              &ifd14(sp2,se2,sp3,se3,s23) + ifd14(sp3,se3,sp2,se2,s23) + &
              &ifd15(se2,sp2,se3,sp3,s23) + ifd15(se3,sp3,se2,sp2,s23) + &
              &ifd16(se2,sp2,se3,sp3,s23) + ifd16(se3,sp3,se2,sp2,s23) + &
              &ifd16(sp2,se2,sp3,se3,s23) + ifd16(sp3,se3,sp2,se2,s23) + &
              &ifd44(se2,sp2,se3,sp3,s23) + ifd44(se3,sp3,se2,sp2,s23) + &
              &ifd46(se2,sp2,se3,sp3,s23)
  ee2nnggav = (256*alpha**2*ee2nnggav*GF**2*Pi**2)/3.
  END FUNCTION

                          !!!!!!!!!!!!!!!!!!!!!!
                           END MODULE misc_EE2NNGG
                          !!!!!!!!!!!!!!!!!!!!!!
