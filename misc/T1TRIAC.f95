
                          !!!!!!!!!!!!!!!!!!!!!!
                              MODULE rad_T1TRIAC
                          !!!!!!!!!!!!!!!!!!!!!!
  contains
      function T1TRIAC(EL,GF,MOU2,MIN2,p1p2,p1p4,p2p4,asym,np2,np3, & 
         np4,mu2)
!
      use global_def, only: pi, prec
      implicit none
!

    complex(kind=prec) A0, B0i
    complex(kind=prec) B0
    complex(kind=prec) DB0
    complex(kind=prec) C0, C0i
    complex(kind=prec) D0, D0i

    integer, parameter :: bb0  = 1
    integer, parameter :: bb1  = 4
    integer, parameter :: cc0  = 1
    integer, parameter :: cc1  = 4
    integer, parameter :: cc2  = 7
    integer, parameter :: cc00 = 10
    integer, parameter :: cc11 = 13
    integer, parameter :: cc12 = 16
    integer, parameter :: cc22 = 19
    integer, parameter :: dd0  = 1
    integer, parameter :: dd1  = 4
    integer, parameter :: dd2  = 7
    integer, parameter :: dd3  = 10
    integer, parameter :: dd00 = 13
    
    

    external A0, B0i
    external B0
    external DB0
    external C0, C0i
    external D0, D0i
    
    complex(kind=prec) :: Ccache(7)
    complex(kind=prec) :: Bcache(1)

!
      real(kind=prec) T1TRIAC,EL,GF,MOU2,MIN2, & 
         p1p2,p1p4,p2p4,asym, & 
         np2,np3,np4,mu2, & 
         aa,bb
!
      aa=0._prec
      bb=  & 
          -1._prec
!

!    
      call setlambda(  & 
          -1._prec)
      call setdelta(0._prec)
      call setmudim(mu2)
      call setminmass(0._prec*10._prec**(  & 
          -10._prec))
!     
    Bcache(1) = B0i(bb0,MOU2,0._prec,MOU2)
    Ccache(1) = C0i(cc0,MOU2,-MIN2+MOU2+2*p1p2-2*p2p4,MIN2-2*p1p4,0._prec,MOU2,MIN2)
    Ccache(2) = C0i(cc00,MOU2,-MIN2+MOU2+2*p1p2-2*p2p4,MIN2-2*p1p4,0._prec,MOU2,MIN2)
    Ccache(3) = C0i(cc1,MOU2,-MIN2+MOU2+2*p1p2-2*p2p4,MIN2-2*p1p4,0._prec,MOU2,MIN2)
    Ccache(4) = C0i(cc11,MOU2,-MIN2+MOU2+2*p1p2-2*p2p4,MIN2-2*p1p4,0._prec,MOU2,MIN2)
    Ccache(5) = C0i(cc12,MOU2,-MIN2+MOU2+2*p1p2-2*p2p4,MIN2-2*p1p4,0._prec,MOU2,MIN2)
    Ccache(6) = C0i(cc2,MOU2,-MIN2+MOU2+2*p1p2-2*p2p4,MIN2-2*p1p4,0._prec,MOU2,MIN2)
    Ccache(7) = C0i(cc22,MOU2,-MIN2+MOU2+2*p1p2-2*p2p4,MIN2-2*p1p4,0._prec,MOU2,MIN2)

!
      T1TRIAC=real((  & 
          -2*EL**4*GF**2*(2*MIN2**2*p1p2*p1p4   & 
          -  & 
           2*MIN2*MOU2*p1p2*p1p4   & 
          -  & 
           2*MIN2**1.5*np2*p1p2*p1p4   & 
          +  & 
           2*sqrt(MIN2)*MOU2*np2*p1p2*p1p4   & 
          -  & 
           2*MIN2*p1p2**2*p1p4   & 
          - 2*MOU2*p1p2**2*p1p4   & 
          -  & 
           3*MIN2**2*p1p4**2   & 
          + 4*MIN2*MOU2*p1p4**2   & 
          -  & 
           MOU2**2*p1p4**2   & 
          + 3*MIN2**1.5*np2*p1p4**2   & 
          -  & 
           3*sqrt(MIN2)*MOU2*np2*p1p4**2   & 
          +  & 
           2*MIN2*p1p2*p1p4**2   & 
          + 2*MOU2*p1p2*p1p4**2   & 
          +  & 
           2*MIN2*p1p4**3   & 
          - 2*MOU2*p1p4**3   & 
          -  & 
           2*sqrt(MIN2)*np2*p1p4**3   & 
          - 2*MIN2**3*p2p4   & 
          +  & 
           2*MIN2**2*MOU2*p2p4   & 
          + 2*MIN2**2.5*np2*p2p4   & 
          -  & 
           2*MIN2**1.5*MOU2*np2*p2p4   & 
          + 2*MIN2**2*p1p2*p2p4   & 
          +  & 
           2*MIN2*MOU2*p1p2*p2p4   & 
          + 3*MIN2**2*p1p4*p2p4   & 
          -  & 
           3*MIN2*MOU2*p1p4*p2p4   & 
          -  & 
           2*MIN2**1.5*np2*p1p4*p2p4   & 
          +  & 
           2*MOU2*p1p2*p1p4*p2p4   & 
          - 4*MIN2*p1p4**2*p2p4   & 
          -  & 
           2*MIN2**2*p2p4**2   & 
          - 2*MIN2*MOU2*p2p4**2   & 
          +  & 
           2*MIN2*p1p4*p2p4**2   & 
          - 2*MOU2*p1p4*p2p4**2   & 
          -  & 
           CMPLX(aa,bb)*asym*sqrt(MIN2)* & 
            (  & 
          -(MIN2*p1p4)   & 
          + MOU2*p1p4   & 
          + 4*p1p2*p1p4   & 
          -  & 
              2*p1p4**2   & 
          + 2*MIN2*p2p4   & 
          - 2*MOU2*p2p4   & 
          -  & 
              4*p1p2*p2p4   & 
          - 4*p1p4*p2p4   & 
          + 4*p2p4**2)   & 
          -  & 
           sqrt(MIN2)*np4* & 
            (  & 
          -(MIN2**2*p1p4)   & 
          + 2*MIN2*MOU2*p1p4   & 
          -  & 
              MOU2**2*p1p4   & 
          + MIN2*p1p2*p1p4   & 
          -  & 
              MOU2*p1p2*p1p4   & 
          + 2*MIN2*p1p4**2   & 
          -  & 
              2*MOU2*p1p4**2   & 
          - 2*p1p2*p1p4**2   & 
          +  & 
              2*MIN2**2*p2p4   & 
          - 2*MIN2*MOU2*p2p4   & 
          -  & 
              2*MIN2*p1p2*p2p4   & 
          + 2*MOU2*p1p2*p2p4   & 
          -  & 
              2*MIN2*p1p4*p2p4   & 
          + 2*MIN2*p2p4**2   & 
          -  & 
              2*MOU2*p2p4**2))*Bcache(1))/ & 
       (3.*p1p4**2*(p1p4   & 
          - p2p4)*Pi**2)   & 
          +  & 
      (2*EL**4*GF**2*(CMPLX(aa,bb)*asym*sqrt(MIN2)* & 
            (6*MIN2**2*p1p4   & 
          - 6*MIN2*MOU2*p1p4   & 
          -  & 
              14*MIN2*p1p2*p1p4   & 
          + 6*MOU2*p1p2*p1p4   & 
          +  & 
              8*p1p2**2*p1p4   & 
          - 8*MIN2*p1p4**2   & 
          +  & 
              4*MOU2*p1p4**2   & 
          + 16*p1p2*p1p4**2   & 
          - 4*p1p4**3   & 
          +  & 
              18*MIN2*p1p4*p2p4   & 
          - 10*MOU2*p1p4*p2p4   & 
          -  & 
              24*p1p2*p1p4*p2p4   & 
          - 16*p1p4**2*p2p4   & 
          +  & 
              16*p1p4*p2p4**2)   & 
          +  & 
           2*(2*MIN2**3*p1p2*p1p4   & 
          -  & 
              2*MIN2**2*MOU2*p1p2*p1p4   & 
          +  & 
              6*MIN2**2.5*np2*p1p2*p1p4   & 
          -  & 
              6*MIN2**1.5*MOU2*np2*p1p2*p1p4   & 
          -  & 
              12*MIN2**2*p1p2**2*p1p4   & 
          +  & 
              8*MIN2*MOU2*p1p2**2*p1p4   & 
          -  & 
              14*MIN2**1.5*np2*p1p2**2*p1p4   & 
          +  & 
              6*sqrt(MIN2)*MOU2*np2*p1p2**2*p1p4   & 
          +  & 
              18*MIN2*p1p2**3*p1p4   & 
          - 6*MOU2*p1p2**3*p1p4   & 
          +  & 
              8*sqrt(MIN2)*np2*p1p2**3*p1p4   & 
          -  & 
              8*p1p2**4*p1p4   & 
          - MIN2**3*p1p4**2   & 
          +  & 
              MIN2*MOU2**2*p1p4**2   & 
          -  & 
              3*MIN2**2.5*np2*p1p4**2   & 
          +  & 
              3*sqrt(MIN2)*MOU2**2*np2*p1p4**2   & 
          +  & 
              MIN2**2*p1p2*p1p4**2   & 
          +  & 
              9*MIN2*MOU2*p1p2*p1p4**2   & 
          -  & 
              6*MOU2**2*p1p2*p1p4**2   & 
          +  & 
              3*MIN2**1.5*np2*p1p2*p1p4**2   & 
          +  & 
              3*sqrt(MIN2)*MOU2*np2*p1p2*p1p4**2   & 
          +  & 
              8*MIN2*p1p2**2*p1p4**2   & 
          -  & 
              10*MOU2*p1p2**2*p1p4**2   & 
          +  & 
              4*sqrt(MIN2)*np2*p1p2**2*p1p4**2   & 
          -  & 
              8*p1p2**3*p1p4**2   & 
          - 4*MIN2**2*p1p4**3   & 
          +  & 
              6*MIN2*MOU2*p1p4**3   & 
          - 2*MOU2**2*p1p4**3   & 
          -  & 
              4*MIN2**1.5*np2*p1p4**3   & 
          +  & 
              6*sqrt(MIN2)*MOU2*np2*p1p4**3   & 
          +  & 
              18*MIN2*p1p2*p1p4**3   & 
          - 18*MOU2*p1p2*p1p4**3   & 
          +  & 
              4*sqrt(MIN2)*np2*p1p2*p1p4**3   & 
          -  & 
              16*p1p2**2*p1p4**3   & 
          + 2*MIN2*p1p4**4   & 
          -  & 
              2*MOU2*p1p4**4   & 
          + 2*sqrt(MIN2)*np2*p1p4**4   & 
          -  & 
              8*p1p2*p1p4**4   & 
          - 2*MIN2**4*p2p4   & 
          +  & 
              2*MIN2**3*MOU2*p2p4   & 
          - 6*MIN2**3.5*np2*p2p4   & 
          +  & 
              6*MIN2**2.5*MOU2*np2*p2p4   & 
          +  & 
              12*MIN2**3*p1p2*p2p4   & 
          -  & 
              8*MIN2**2*MOU2*p1p2*p2p4   & 
          +  & 
              14*MIN2**2.5*np2*p1p2*p2p4   & 
          -  & 
              6*MIN2**1.5*MOU2*np2*p1p2*p2p4   & 
          -  & 
              18*MIN2**2*p1p2**2*p2p4   & 
          +  & 
              6*MIN2*MOU2*p1p2**2*p2p4   & 
          -  & 
              8*MIN2**1.5*np2*p1p2**2*p2p4   & 
          +  & 
              8*MIN2*p1p2**3*p2p4   & 
          + 5*MIN2**3*p1p4*p2p4   & 
          -  & 
              8*MIN2**2*MOU2*p1p4*p2p4   & 
          +  & 
              3*MIN2*MOU2**2*p1p4*p2p4   & 
          +  & 
              4*MIN2**2.5*np2*p1p4*p2p4   & 
          -  & 
              2*MIN2**1.5*MOU2*np2*p1p4*p2p4   & 
          -  & 
              5*MIN2**2*p1p2*p1p4*p2p4   & 
          +  & 
              MIN2*MOU2*p1p2*p1p4*p2p4   & 
          +  & 
              6*MIN2**1.5*np2*p1p2*p1p4*p2p4   & 
          -  & 
              6*sqrt(MIN2)*MOU2*np2*p1p2*p1p4*p2p4   & 
          -  & 
              24*MIN2*p1p2**2*p1p4*p2p4   & 
          +  & 
              12*MOU2*p1p2**2*p1p4*p2p4   & 
          -  & 
              16*sqrt(MIN2)*np2*p1p2**2*p1p4*p2p4   & 
          +  & 
              24*p1p2**3*p1p4*p2p4   & 
          + MIN2**2*p1p4**2*p2p4   & 
          -  & 
              11*MIN2*MOU2*p1p4**2*p2p4   & 
          +  & 
              6*MOU2**2*p1p4**2*p2p4   & 
          -  & 
              MIN2**1.5*np2*p1p4**2*p2p4   & 
          -  & 
              5*sqrt(MIN2)*MOU2*np2*p1p4**2*p2p4   & 
          -  & 
              32*MIN2*p1p2*p1p4**2*p2p4   & 
          +  & 
              32*MOU2*p1p2*p1p4**2*p2p4   & 
          -  & 
              8*sqrt(MIN2)*np2*p1p2*p1p4**2*p2p4   & 
          +  & 
              40*p1p2**2*p1p4**2*p2p4   & 
          -  & 
              18*MIN2*p1p4**3*p2p4   & 
          + 20*MOU2*p1p4**3*p2p4   & 
          -  & 
              4*sqrt(MIN2)*np2*p1p4**3*p2p4   & 
          +  & 
              40*p1p2*p1p4**3*p2p4   & 
          + 8*p1p4**4*p2p4   & 
          -  & 
              12*MIN2**3*p2p4**2   & 
          + 8*MIN2**2*MOU2*p2p4**2   & 
          -  & 
              14*MIN2**2.5*np2*p2p4**2   & 
          +  & 
              6*MIN2**1.5*MOU2*np2*p2p4**2   & 
          +  & 
              36*MIN2**2*p1p2*p2p4**2   & 
          -  & 
              12*MIN2*MOU2*p1p2*p2p4**2   & 
          +  & 
              16*MIN2**1.5*np2*p1p2*p2p4**2   & 
          -  & 
              24*MIN2*p1p2**2*p2p4**2   & 
          +  & 
              15*MIN2**2*p1p4*p2p4**2   & 
          -  & 
              5*MIN2*MOU2*p1p4*p2p4**2   & 
          +  & 
              8*MIN2**1.5*np2*p1p4*p2p4**2   & 
          +  & 
              4*MIN2*p1p2*p1p4*p2p4**2   & 
          -  & 
              12*MOU2*p1p2*p1p4*p2p4**2   & 
          +  & 
              8*sqrt(MIN2)*np2*p1p2*p1p4*p2p4**2   & 
          -  & 
              32*p1p2**2*p1p4*p2p4**2   & 
          +  & 
              22*MIN2*p1p4**2*p2p4**2   & 
          -  & 
              20*MOU2*p1p4**2*p2p4**2   & 
          +  & 
              4*sqrt(MIN2)*np2*p1p4**2*p2p4**2   & 
          -  & 
              56*p1p2*p1p4**2*p2p4**2   & 
          - 24*p1p4**3*p2p4**2   & 
          -  & 
              18*MIN2**2*p2p4**3   & 
          + 6*MIN2*MOU2*p2p4**3   & 
          -  & 
              8*MIN2**1.5*np2*p2p4**3   & 
          +  & 
              24*MIN2*p1p2*p2p4**3   & 
          + 2*MIN2*p1p4*p2p4**3   & 
          +  & 
              6*MOU2*p1p4*p2p4**3   & 
          + 24*p1p2*p1p4*p2p4**3   & 
          +  & 
              24*p1p4**2*p2p4**3   & 
          - 8*MIN2*p2p4**4   & 
          -  & 
              8*p1p4*p2p4**4   & 
          +  & 
              sqrt(MIN2)*np4* & 
               (  & 
          -3*MIN2**3*p1p4   & 
          + 6*MIN2**2*MOU2*p1p4   & 
          -  & 
                 13*MIN2*MOU2*p1p2*p1p4   & 
          -  & 
                 11*MIN2*p1p2**2*p1p4   & 
          +  & 
                 7*MOU2*p1p2**2*p1p4   & 
          + 4*p1p2**3*p1p4   & 
          -  & 
                 8*MIN2*MOU2*p1p4**2   & 
          -  & 
                 18*MIN2*p1p2*p1p4**2   & 
          +  & 
                 12*MOU2*p1p2*p1p4**2   & 
          + 12*p1p2**2*p1p4**2   & 
          -  & 
                 2*MIN2*p1p4**3   & 
          + 2*MOU2*p1p4**3   & 
          +  & 
                 6*p1p2*p1p4**3   & 
          +  & 
                 MOU2**2*p1p4* & 
                  (  & 
          -3*MIN2   & 
          + 3*p1p2   & 
          + 2*p1p4   & 
          - 6*p2p4)   & 
          +  & 
                 6*MIN2**3*p2p4   & 
          - 6*MIN2**2*MOU2*p2p4   & 
          +  & 
                 12*MIN2*MOU2*p1p2*p2p4   & 
          +  & 
                 22*MIN2*p1p2**2*p2p4   & 
          -  & 
                 6*MOU2*p1p2**2*p2p4   & 
          - 8*p1p2**3*p2p4   & 
          +  & 
                 25*MIN2*MOU2*p1p4*p2p4   & 
          +  & 
                 53*MIN2*p1p2*p1p4*p2p4   & 
          -  & 
                 27*MOU2*p1p2*p1p4*p2p4   & 
          -  & 
                 32*p1p2**2*p1p4*p2p4   & 
          +  & 
                 24*MIN2*p1p4**2*p2p4   & 
          -  & 
                 20*MOU2*p1p4**2*p2p4   & 
          -  & 
                 36*p1p2*p1p4**2*p2p4   & 
          - 8*p1p4**3*p2p4   & 
          -  & 
                 12*MIN2*MOU2*p2p4**2   & 
          -  & 
                 44*MIN2*p1p2*p2p4**2   & 
          +  & 
                 12*MOU2*p1p2*p2p4**2   & 
          + 24*p1p2**2*p2p4**2   & 
          -  & 
                 42*MIN2*p1p4*p2p4**2   & 
          +  & 
                 20*MOU2*p1p4*p2p4**2   & 
          +  & 
                 52*p1p2*p1p4*p2p4**2   & 
          + 24*p1p4**2*p2p4**2   & 
          +  & 
                 22*MIN2*p2p4**3   & 
          - 6*MOU2*p2p4**3   & 
          -  & 
                 24*p1p2*p2p4**3   & 
          - 24*p1p4*p2p4**3   & 
          +  & 
                 8*p2p4**4   & 
          +  & 
                 MIN2**2* & 
                  (10*p1p2*p1p4   & 
          + 6*p1p4**2   & 
          - 20*p1p2*p2p4   & 
          -  & 
                    21*p1p4*p2p4   & 
          + 20*p2p4**2))))* & 
         Ccache(1))/ & 
       (3.*p1p4**2*(p1p4   & 
          - p2p4)*Pi**2)   & 
          -  & 
      (4*EL**4*GF**2*(  & 
          -6*MIN2**2*p1p2*p1p4   & 
          +  & 
           6*MIN2*MOU2*p1p2*p1p4   & 
          -  & 
           2*MIN2**1.5*np2*p1p2*p1p4   & 
          +  & 
           2*sqrt(MIN2)*MOU2*np2*p1p2*p1p4   & 
          +  & 
           14*MIN2*p1p2**2*p1p4   & 
          - 2*MOU2*p1p2**2*p1p4   & 
          +  & 
           8*sqrt(MIN2)*np2*p1p2**2*p1p4   & 
          - 8*p1p2**3*p1p4   & 
          +  & 
           5*MIN2**2*p1p4**2   & 
          - 4*MIN2*MOU2*p1p4**2   & 
          -  & 
           MOU2**2*p1p4**2   & 
          - MIN2**1.5*np2*p1p4**2   & 
          +  & 
           sqrt(MIN2)*MOU2*np2*p1p4**2   & 
          -  & 
           6*MIN2*p1p2*p1p4**2   & 
          - 6*MOU2*p1p2*p1p4**2   & 
          -  & 
           4*sqrt(MIN2)*np2*p1p2*p1p4**2   & 
          + 2*MIN2*p1p4**3   & 
          -  & 
           2*MOU2*p1p4**3   & 
          + 2*sqrt(MIN2)*np2*p1p4**3   & 
          -  & 
           8*p1p2*p1p4**3   & 
          + 6*MIN2**3*p2p4   & 
          -  & 
           6*MIN2**2*MOU2*p2p4   & 
          + 2*MIN2**2.5*np2*p2p4   & 
          -  & 
           2*MIN2**1.5*MOU2*np2*p2p4   & 
          -  & 
           14*MIN2**2*p1p2*p2p4   & 
          + 2*MIN2*MOU2*p1p2*p2p4   & 
          -  & 
           8*MIN2**1.5*np2*p1p2*p2p4   & 
          + 8*MIN2*p1p2**2*p2p4   & 
          -  & 
           5*MIN2**2*p1p4*p2p4   & 
          + 5*MIN2*MOU2*p1p4*p2p4   & 
          +  & 
           2*MIN2**1.5*np2*p1p4*p2p4   & 
          -  & 
           8*MIN2*p1p2*p1p4*p2p4   & 
          + 2*MOU2*p1p2*p1p4*p2p4   & 
          -  & 
           8*sqrt(MIN2)*np2*p1p2*p1p4*p2p4   & 
          +  & 
           16*p1p2**2*p1p4*p2p4   & 
          + 12*MOU2*p1p4**2*p2p4   & 
          +  & 
           4*sqrt(MIN2)*np2*p1p4**2*p2p4   & 
          +  & 
           16*p1p2*p1p4**2*p2p4   & 
          + 8*p1p4**3*p2p4   & 
          +  & 
           14*MIN2**2*p2p4**2   & 
          - 2*MIN2*MOU2*p2p4**2   & 
          +  & 
           8*MIN2**1.5*np2*p2p4**2   & 
          - 16*MIN2*p1p2*p2p4**2   & 
          -  & 
           2*MIN2*p1p4*p2p4**2   & 
          - 2*MOU2*p1p4*p2p4**2   & 
          -  & 
           16*p1p2*p1p4*p2p4**2   & 
          - 16*p1p4**2*p2p4**2   & 
          +  & 
           8*MIN2*p2p4**3   & 
          + 8*p1p4*p2p4**3   & 
          -  & 
           CMPLX(aa,bb)*asym*sqrt(MIN2)* & 
            (5*MIN2*p1p4   & 
          - 5*MOU2*p1p4   & 
          - 12*p1p2*p1p4   & 
          +  & 
              2*p1p4**2   & 
          - 4*MIN2*p2p4   & 
          + 4*MOU2*p2p4   & 
          +  & 
              8*p1p2*p2p4   & 
          + 12*p1p4*p2p4   & 
          - 8*p2p4**2)   & 
          -  & 
           sqrt(MIN2)*np4* & 
            (  & 
          -(MIN2**2*p1p4)   & 
          + 2*MIN2*MOU2*p1p4   & 
          -  & 
              MOU2**2*p1p4   & 
          + 5*MIN2*p1p2*p1p4   & 
          -  & 
              5*MOU2*p1p2*p1p4   & 
          - 4*p1p2**2*p1p4   & 
          +  & 
              2*MIN2*p1p4**2   & 
          - 2*MOU2*p1p4**2   & 
          -  & 
              6*p1p2*p1p4**2   & 
          + 2*MIN2**2*p2p4   & 
          -  & 
              2*MIN2*MOU2*p2p4   & 
          - 10*MIN2*p1p2*p2p4   & 
          +  & 
              2*MOU2*p1p2*p2p4   & 
          + 8*p1p2**2*p2p4   & 
          -  & 
              10*MIN2*p1p4*p2p4   & 
          + 12*MOU2*p1p4*p2p4   & 
          +  & 
              20*p1p2*p1p4*p2p4   & 
          + 8*p1p4**2*p2p4   & 
          +  & 
              10*MIN2*p2p4**2   & 
          - 2*MOU2*p2p4**2   & 
          -  & 
              16*p1p2*p2p4**2   & 
          - 16*p1p4*p2p4**2   & 
          + 8*p2p4**3)) & 
          *Ccache(2))/ & 
       (3.*p1p4**2*(p1p4   & 
          - p2p4)*Pi**2)   & 
          +  & 
      (2*EL**4*GF**2*(8*MIN2**3*p1p2*p1p4   & 
          -  & 
           8*MIN2**2*MOU2*p1p2*p1p4   & 
          +  & 
           8*MIN2**2.5*np2*p1p2*p1p4   & 
          -  & 
           2*MIN2**1.5*MOU2*np2*p1p2*p1p4   & 
          -  & 
           6*sqrt(MIN2)*MOU2**2*np2*p1p2*p1p4   & 
          -  & 
           32*MIN2**2*p1p2**2*p1p4   & 
          +  & 
           10*MIN2*MOU2*p1p2**2*p1p4   & 
          +  & 
           6*MOU2**2*p1p2**2*p1p4   & 
          -  & 
           24*MIN2**1.5*np2*p1p2**2*p1p4   & 
          -  & 
           4*sqrt(MIN2)*MOU2*np2*p1p2**2*p1p4   & 
          +  & 
           40*MIN2*p1p2**3*p1p4   & 
          + 4*MOU2*p1p2**3*p1p4   & 
          +  & 
           16*sqrt(MIN2)*np2*p1p2**3*p1p4   & 
          -  & 
           16*p1p2**4*p1p4   & 
          - 8*MIN2**3*p1p4**2   & 
          +  & 
           11*MIN2**2*MOU2*p1p4**2   & 
          -  & 
           6*MIN2*MOU2**2*p1p4**2   & 
          + 3*MOU2**3*p1p4**2   & 
          -  & 
           6*MIN2**1.5*MOU2*np2*p1p4**2   & 
          +  & 
           6*sqrt(MIN2)*MOU2**2*np2*p1p4**2   & 
          +  & 
           12*MIN2**2*p1p2*p1p4**2   & 
          +  & 
           6*MIN2*MOU2*p1p2*p1p4**2   & 
          -  & 
           2*MOU2**2*p1p2*p1p4**2   & 
          +  & 
           12*sqrt(MIN2)*MOU2*np2*p1p2*p1p4**2   & 
          +  & 
           12*MIN2*p1p2**2*p1p4**2   & 
          -  & 
           20*MOU2*p1p2**2*p1p4**2   & 
          +  & 
           8*sqrt(MIN2)*np2*p1p2**2*p1p4**2   & 
          -  & 
           16*p1p2**3*p1p4**2   & 
          - 4*MIN2**2*p1p4**3   & 
          +  & 
           4*MOU2**2*p1p4**3   & 
          - 12*MIN2**1.5*np2*p1p4**3   & 
          +  & 
           12*sqrt(MIN2)*MOU2*np2*p1p4**3   & 
          +  & 
           32*MIN2*p1p2*p1p4**3   & 
          - 16*MOU2*p1p2*p1p4**3   & 
          +  & 
           12*sqrt(MIN2)*np2*p1p2*p1p4**3   & 
          -  & 
           32*p1p2**2*p1p4**3   & 
          + 4*MIN2*p1p4**4   & 
          -  & 
           4*MOU2*p1p4**4   & 
          + 4*sqrt(MIN2)*np2*p1p4**4   & 
          -  & 
           16*p1p2*p1p4**4   & 
          - 8*MIN2**4*p2p4   & 
          +  & 
           8*MIN2**3*MOU2*p2p4   & 
          - 8*MIN2**3.5*np2*p2p4   & 
          +  & 
           2*MIN2**2.5*MOU2*np2*p2p4   & 
          +  & 
           6*MIN2**1.5*MOU2**2*np2*p2p4   & 
          +  & 
           32*MIN2**3*p1p2*p2p4   & 
          -  & 
           10*MIN2**2*MOU2*p1p2*p2p4   & 
          -  & 
           6*MIN2*MOU2**2*p1p2*p2p4   & 
          +  & 
           24*MIN2**2.5*np2*p1p2*p2p4   & 
          +  & 
           4*MIN2**1.5*MOU2*np2*p1p2*p2p4   & 
          -  & 
           40*MIN2**2*p1p2**2*p2p4   & 
          -  & 
           4*MIN2*MOU2*p1p2**2*p2p4   & 
          -  & 
           16*MIN2**1.5*np2*p1p2**2*p2p4   & 
          +  & 
           16*MIN2*p1p2**3*p2p4   & 
          + 16*MIN2**3*p1p4*p2p4   & 
          -  & 
           20*MIN2**2*MOU2*p1p4*p2p4   & 
          +  & 
           4*MIN2*MOU2**2*p1p4*p2p4   & 
          +  & 
           4*MIN2**2.5*np2*p1p4*p2p4   & 
          -  & 
           4*MIN2**1.5*MOU2*np2*p1p4*p2p4   & 
          -  & 
           12*MIN2**2*p1p2*p1p4*p2p4   & 
          +  & 
           10*MIN2*MOU2*p1p2*p1p4*p2p4   & 
          -  & 
           6*MOU2**2*p1p2*p1p4*p2p4   & 
          +  & 
           12*MIN2**1.5*np2*p1p2*p1p4*p2p4   & 
          +  & 
           4*sqrt(MIN2)*MOU2*np2*p1p2*p1p4*p2p4   & 
          -  & 
           52*MIN2*p1p2**2*p1p4*p2p4   & 
          -  & 
           8*MOU2*p1p2**2*p1p4*p2p4   & 
          -  & 
           32*sqrt(MIN2)*np2*p1p2**2*p1p4*p2p4   & 
          +  & 
           48*p1p2**3*p1p4*p2p4   & 
          - 12*MIN2**2*p1p4**2*p2p4   & 
          +  & 
           4*MIN2*MOU2*p1p4**2*p2p4   & 
          -  & 
           8*MOU2**2*p1p4**2*p2p4   & 
          +  & 
           4*MIN2**1.5*np2*p1p4**2*p2p4   & 
          -  & 
           12*sqrt(MIN2)*MOU2*np2*p1p4**2*p2p4   & 
          -  & 
           52*MIN2*p1p2*p1p4**2*p2p4   & 
          +  & 
           36*MOU2*p1p2*p1p4**2*p2p4   & 
          -  & 
           16*sqrt(MIN2)*np2*p1p2*p1p4**2*p2p4   & 
          +  & 
           80*p1p2**2*p1p4**2*p2p4   & 
          - 32*MIN2*p1p4**3*p2p4   & 
          +  & 
           24*MOU2*p1p4**3*p2p4   & 
          -  & 
           12*sqrt(MIN2)*np2*p1p4**3*p2p4   & 
          +  & 
           80*p1p2*p1p4**3*p2p4   & 
          + 16*p1p4**4*p2p4   & 
          -  & 
           32*MIN2**3*p2p4**2   & 
          + 10*MIN2**2*MOU2*p2p4**2   & 
          +  & 
           6*MIN2*MOU2**2*p2p4**2   & 
          -  & 
           24*MIN2**2.5*np2*p2p4**2   & 
          -  & 
           4*MIN2**1.5*MOU2*np2*p2p4**2   & 
          +  & 
           80*MIN2**2*p1p2*p2p4**2   & 
          +  & 
           8*MIN2*MOU2*p1p2*p2p4**2   & 
          +  & 
           32*MIN2**1.5*np2*p1p2*p2p4**2   & 
          -  & 
           48*MIN2*p1p2**2*p2p4**2   & 
          +  & 
           40*MIN2**2*p1p4*p2p4**2   & 
          -  & 
           22*MIN2*MOU2*p1p4*p2p4**2   & 
          +  & 
           6*MOU2**2*p1p4*p2p4**2   & 
          +  & 
           12*MIN2**1.5*np2*p1p4*p2p4**2   & 
          +  & 
           4*MIN2*p1p2*p1p4*p2p4**2   & 
          +  & 
           8*MOU2*p1p2*p1p4*p2p4**2   & 
          +  & 
           16*sqrt(MIN2)*np2*p1p2*p1p4*p2p4**2   & 
          -  & 
           64*p1p2**2*p1p4*p2p4**2   & 
          +  & 
           36*MIN2*p1p4**2*p2p4**2   & 
          -  & 
           16*MOU2*p1p4**2*p2p4**2   & 
          +  & 
           8*sqrt(MIN2)*np2*p1p4**2*p2p4**2   & 
          -  & 
           112*p1p2*p1p4**2*p2p4**2   & 
          - 48*p1p4**3*p2p4**2   & 
          -  & 
           40*MIN2**2*p2p4**3   & 
          - 4*MIN2*MOU2*p2p4**3   & 
          -  & 
           16*MIN2**1.5*np2*p2p4**3   & 
          + 48*MIN2*p1p2*p2p4**3   & 
          +  & 
           8*MIN2*p1p4*p2p4**3   & 
          - 4*MOU2*p1p4*p2p4**3   & 
          +  & 
           48*p1p2*p1p4*p2p4**3   & 
          + 48*p1p4**2*p2p4**3   & 
          -  & 
           16*MIN2*p2p4**4   & 
          - 16*p1p4*p2p4**4   & 
          +  & 
           CMPLX(aa,bb)*asym*sqrt(MIN2)* & 
            (8*MIN2**2*p1p4   & 
          - 4*MIN2*MOU2*p1p4   & 
          -  & 
              4*MOU2**2*p1p4   & 
          - 24*MIN2*p1p2*p1p4   & 
          +  & 
              16*p1p2**2*p1p4   & 
          - 4*MIN2*p1p4**2   & 
          +  & 
              4*MOU2*p1p4**2   & 
          + 12*p1p2*p1p4**2   & 
          - 4*p1p4**3   & 
          -  & 
              4*MIN2**2*p2p4   & 
          + 4*MIN2*MOU2*p2p4   & 
          +  & 
              12*MIN2*p1p2*p2p4   & 
          - 4*MOU2*p1p2*p2p4   & 
          -  & 
              8*p1p2**2*p2p4   & 
          + 28*MIN2*p1p4*p2p4   & 
          -  & 
              4*MOU2*p1p4*p2p4   & 
          - 40*p1p2*p1p4*p2p4   & 
          -  & 
              12*p1p4**2*p2p4   & 
          - 12*MIN2*p2p4**2   & 
          +  & 
              4*MOU2*p2p4**2   & 
          + 16*p1p2*p2p4**2   & 
          +  & 
              24*p1p4*p2p4**2   & 
          - 8*p2p4**3)   & 
          +  & 
           sqrt(MIN2)*np4* & 
            (5*MIN2**2*MOU2*p1p4   & 
          - 3*MOU2**3*p1p4   & 
          -  & 
              12*MIN2*MOU2*p1p2*p1p4   & 
          + 8*MOU2*p1p2**2*p1p4   & 
          -  & 
              4*MIN2*MOU2*p1p4**2   & 
          + 4*MOU2*p1p2*p1p4**2   & 
          +  & 
              4*MOU2*p1p4**3   & 
          - 2*MIN2**2*MOU2*p2p4   & 
          -  & 
              2*MIN2*MOU2*p1p2*p2p4   & 
          + 4*MOU2*p1p2**2*p2p4   & 
          +  & 
              28*MIN2*MOU2*p1p4*p2p4   & 
          -  & 
              24*MOU2*p1p2*p1p4*p2p4   & 
          -  & 
              24*MOU2*p1p4**2*p2p4   & 
          + 2*MIN2*MOU2*p2p4**2   & 
          -  & 
              8*MOU2*p1p2*p2p4**2   & 
          + 16*MOU2*p1p4*p2p4**2   & 
          +  & 
              4*MOU2*p2p4**3   & 
          +  & 
              2*MOU2**2* & 
               (MIN2*p1p4   & 
          - 2*p1p2*p1p4   & 
          - 2*p1p4**2   & 
          -  & 
                 3*MIN2*p2p4   & 
          + 3*p1p2*p2p4   & 
          + 4*p1p4*p2p4   & 
          -  & 
                 3*p2p4**2)   & 
          +  & 
              2*(MIN2   & 
          - p1p2   & 
          - p1p4   & 
          + p2p4)* & 
               (  & 
          -2*MIN2**2*p1p4   & 
          + 6*MIN2*p1p2*p1p4   & 
          -  & 
                 4*p1p2**2*p1p4   & 
          + 2*MIN2*p1p4**2   & 
          -  & 
                 6*p1p2*p1p4**2   & 
          + 4*MIN2**2*p2p4   & 
          -  & 
                 12*MIN2*p1p2*p2p4   & 
          + 8*p1p2**2*p2p4   & 
          -  & 
                 12*MIN2*p1p4*p2p4   & 
          + 20*p1p2*p1p4*p2p4   & 
          +  & 
                 8*p1p4**2*p2p4   & 
          + 12*MIN2*p2p4**2   & 
          -  & 
                 16*p1p2*p2p4**2   & 
          - 16*p1p4*p2p4**2   & 
          +  & 
                 8*p2p4**3)))* & 
         Ccache(3))/ & 
       (3.*p1p4**2*(p1p4   & 
          - p2p4)*Pi**2)   & 
          -  & 
      (4*EL**4*GF**2*(  & 
          -(CMPLX(aa,bb)*asym*sqrt(MIN2)*(p1p4   & 
          - p2p4)* & 
              (MIN2*MOU2   & 
          - MOU2**2   & 
          - 2*MOU2*p1p2   & 
          +  & 
                2*MOU2*p2p4))   & 
          -  & 
           MOU2*(4*MIN2**2*p1p2*p1p4   & 
          -  & 
              4*MIN2*MOU2*p1p2*p1p4   & 
          +  & 
              2*MIN2**1.5*np2*p1p2*p1p4   & 
          -  & 
              2*sqrt(MIN2)*MOU2*np2*p1p2*p1p4   & 
          -  & 
              10*MIN2*p1p2**2*p1p4   & 
          + 2*MOU2*p1p2**2*p1p4   & 
          -  & 
              6*sqrt(MIN2)*np2*p1p2**2*p1p4   & 
          +  & 
              6*p1p2**3*p1p4   & 
          - 3*MIN2**2*p1p4**2   & 
          +  & 
              2*MIN2*MOU2*p1p4**2   & 
          + MOU2**2*p1p4**2   & 
          +  & 
              3*MIN2*p1p2*p1p4**2   & 
          + 5*MOU2*p1p2*p1p4**2   & 
          +  & 
              4*sqrt(MIN2)*np2*p1p2*p1p4**2   & 
          +  & 
              2*p1p2**2*p1p4**2   & 
          - 3*MIN2*p1p4**3   & 
          +  & 
              3*MOU2*p1p4**3   & 
          - 2*sqrt(MIN2)*np2*p1p4**3   & 
          +  & 
              8*p1p2*p1p4**3   & 
          - 4*MIN2**3*p2p4   & 
          +  & 
              4*MIN2**2*MOU2*p2p4   & 
          - 2*MIN2**2.5*np2*p2p4   & 
          +  & 
              2*MIN2**1.5*MOU2*np2*p2p4   & 
          +  & 
              10*MIN2**2*p1p2*p2p4   & 
          - 2*MIN2*MOU2*p1p2*p2p4   & 
          +  & 
              6*MIN2**1.5*np2*p1p2*p2p4   & 
          -  & 
              6*MIN2*p1p2**2*p2p4   & 
          + 4*MIN2**2*p1p4*p2p4   & 
          -  & 
              4*MIN2*MOU2*p1p4*p2p4   & 
          -  & 
              2*MIN2**1.5*np2*p1p4*p2p4   & 
          +  & 
              4*MIN2*p1p2*p1p4*p2p4   & 
          -  & 
              2*MOU2*p1p2*p1p4*p2p4   & 
          +  & 
              6*sqrt(MIN2)*np2*p1p2*p1p4*p2p4   & 
          -  & 
              12*p1p2**2*p1p4*p2p4   & 
          + MIN2*p1p4**2*p2p4   & 
          -  & 
              9*MOU2*p1p4**2*p2p4   & 
          -  & 
              2*sqrt(MIN2)*np2*p1p4**2*p2p4   & 
          -  & 
              16*p1p2*p1p4**2*p2p4   & 
          - 6*p1p4**3*p2p4   & 
          -  & 
              10*MIN2**2*p2p4**2   & 
          + 2*MIN2*MOU2*p2p4**2   & 
          -  & 
              6*MIN2**1.5*np2*p2p4**2   & 
          +  & 
              12*MIN2*p1p2*p2p4**2   & 
          + 4*MIN2*p1p4*p2p4**2   & 
          +  & 
              2*MOU2*p1p4*p2p4**2   & 
          + 12*p1p2*p1p4*p2p4**2   & 
          +  & 
              12*p1p4**2*p2p4**2   & 
          - 6*MIN2*p2p4**3   & 
          -  & 
              6*p1p4*p2p4**3   & 
          +  & 
              sqrt(MIN2)*np4* & 
               (  & 
          -3*MIN2**2*p1p4   & 
          + 4*MIN2*MOU2*p1p4   & 
          -  & 
                 MOU2**2*p1p4   & 
          + 9*MIN2*p1p2*p1p4   & 
          -  & 
                 5*MOU2*p1p2*p1p4   & 
          - 6*p1p2**2*p1p4   & 
          +  & 
                 3*MIN2*p1p4**2   & 
          - 3*MOU2*p1p4**2   & 
          -  & 
                 6*p1p2*p1p4**2   & 
          + 2*MIN2**2*p2p4   & 
          -  & 
                 2*MIN2*MOU2*p2p4   & 
          - 8*MIN2*p1p2*p2p4   & 
          +  & 
                 2*MOU2*p1p2*p2p4   & 
          + 6*p1p2**2*p2p4   & 
          -  & 
                 11*MIN2*p1p4*p2p4   & 
          + 9*MOU2*p1p4*p2p4   & 
          +  & 
                 18*p1p2*p1p4*p2p4   & 
          + 6*p1p4**2*p2p4   & 
          +  & 
                 8*MIN2*p2p4**2   & 
          - 2*MOU2*p2p4**2   & 
          -  & 
                 12*p1p2*p2p4**2   & 
          - 12*p1p4*p2p4**2   & 
          +  & 
                 6*p2p4**3)))* & 
         Ccache(4))/ & 
       (3.*p1p4**2*(p1p4   & 
          - p2p4)*Pi**2)   & 
          -  & 
      (4*EL**4*GF**2*(  & 
          -6*MIN2**3*p1p2*p1p4   & 
          +  & 
           4*MIN2**2*MOU2*p1p2*p1p4   & 
          +  & 
           2*MIN2*MOU2**2*p1p2*p1p4   & 
          -  & 
           2*MIN2**2.5*np2*p1p2*p1p4   & 
          +  & 
           2*MIN2**1.5*MOU2*np2*p1p2*p1p4   & 
          +  & 
           20*MIN2**2*p1p2**2*p1p4   & 
          -  & 
           4*MIN2*MOU2*p1p2**2*p1p4   & 
          +  & 
           10*MIN2**1.5*np2*p1p2**2*p1p4   & 
          -  & 
           2*sqrt(MIN2)*MOU2*np2*p1p2**2*p1p4   & 
          -  & 
           22*MIN2*p1p2**3*p1p4   & 
          + 2*MOU2*p1p2**3*p1p4   & 
          -  & 
           8*sqrt(MIN2)*np2*p1p2**3*p1p4   & 
          + 8*p1p2**4*p1p4   & 
          +  & 
           5*MIN2**3*p1p4**2   & 
          - 2*MIN2**2*MOU2*p1p4**2   & 
          -  & 
           3*MIN2*MOU2**2*p1p4**2   & 
          - MIN2**2.5*np2*p1p4**2   & 
          +  & 
           sqrt(MIN2)*MOU2**2*np2*p1p4**2   & 
          -  & 
           5*MIN2**2*p1p2*p1p4**2   & 
          -  & 
           10*MIN2*MOU2*p1p2*p1p4**2   & 
          -  & 
           MOU2**2*p1p2*p1p4**2   & 
          -  & 
           3*MIN2**1.5*np2*p1p2*p1p4**2   & 
          -  & 
           sqrt(MIN2)*MOU2*np2*p1p2*p1p4**2   & 
          -  & 
           8*MIN2*p1p2**2*p1p4**2   & 
          + 4*MOU2*p1p2**2*p1p4**2   & 
          +  & 
           8*p1p2**3*p1p4**2   & 
          - MIN2**2*p1p4**3   & 
          +  & 
           2*MIN2*MOU2*p1p4**3   & 
          - MOU2**2*p1p4**3   & 
          +  & 
           5*MIN2**1.5*np2*p1p4**3   & 
          -  & 
           sqrt(MIN2)*MOU2*np2*p1p4**3   & 
          -  & 
           12*MIN2*p1p2*p1p4**3   & 
          + 8*MOU2*p1p2*p1p4**3   & 
          -  & 
           2*sqrt(MIN2)*np2*p1p2*p1p4**3   & 
          +  & 
           16*p1p2**2*p1p4**3   & 
          - 2*MIN2*p1p4**4   & 
          +  & 
           2*MOU2*p1p4**4   & 
          - 2*sqrt(MIN2)*np2*p1p4**4   & 
          +  & 
           8*p1p2*p1p4**4   & 
          + 6*MIN2**4*p2p4   & 
          -  & 
           4*MIN2**3*MOU2*p2p4   & 
          - 2*MIN2**2*MOU2**2*p2p4   & 
          +  & 
           2*MIN2**3.5*np2*p2p4   & 
          -  & 
           2*MIN2**2.5*MOU2*np2*p2p4   & 
          -  & 
           20*MIN2**3*p1p2*p2p4   & 
          + 4*MIN2**2*MOU2*p1p2*p2p4   & 
          -  & 
           10*MIN2**2.5*np2*p1p2*p2p4   & 
          +  & 
           2*MIN2**1.5*MOU2*np2*p1p2*p2p4   & 
          +  & 
           22*MIN2**2*p1p2**2*p2p4   & 
          -  & 
           2*MIN2*MOU2*p1p2**2*p2p4   & 
          +  & 
           8*MIN2**1.5*np2*p1p2**2*p2p4   & 
          -  & 
           8*MIN2*p1p2**3*p2p4   & 
          - 11*MIN2**3*p1p4*p2p4   & 
          +  & 
           8*MIN2**2*MOU2*p1p4*p2p4   & 
          +  & 
           3*MIN2*MOU2**2*p1p4*p2p4   & 
          +  & 
           2*MIN2**2.5*np2*p1p4*p2p4   & 
          +  & 
           2*MIN2**1.5*MOU2*np2*p1p4*p2p4   & 
          +  & 
           5*MIN2**2*p1p2*p1p4*p2p4   & 
          +  & 
           3*MIN2*MOU2*p1p2*p1p4*p2p4   & 
          -  & 
           8*MIN2**1.5*np2*p1p2*p1p4*p2p4   & 
          +  & 
           2*sqrt(MIN2)*MOU2*np2*p1p2*p1p4*p2p4   & 
          +  & 
           30*MIN2*p1p2**2*p1p4*p2p4   & 
          -  & 
           4*MOU2*p1p2**2*p1p4*p2p4   & 
          +  & 
           16*sqrt(MIN2)*np2*p1p2**2*p1p4*p2p4   & 
          -  & 
           24*p1p2**3*p1p4*p2p4   & 
          + 10*MIN2**2*p1p4**2*p2p4   & 
          +  & 
           5*MIN2*MOU2*p1p4**2*p2p4   & 
          + MOU2**2*p1p4**2*p2p4   & 
          -  & 
           MIN2**1.5*np2*p1p4**2*p2p4   & 
          -  & 
           sqrt(MIN2)*MOU2*np2*p1p4**2*p2p4   & 
          +  & 
           22*MIN2*p1p2*p1p4**2*p2p4   & 
          -  & 
           12*MOU2*p1p2*p1p4**2*p2p4   & 
          -  & 
           40*p1p2**2*p1p4**2*p2p4   & 
          + 14*MIN2*p1p4**3*p2p4   & 
          -  & 
           14*MOU2*p1p4**3*p2p4   & 
          +  & 
           2*sqrt(MIN2)*np2*p1p4**3*p2p4   & 
          -  & 
           40*p1p2*p1p4**3*p2p4   & 
          - 8*p1p4**4*p2p4   & 
          +  & 
           20*MIN2**3*p2p4**2   & 
          - 4*MIN2**2*MOU2*p2p4**2   & 
          +  & 
           10*MIN2**2.5*np2*p2p4**2   & 
          -  & 
           2*MIN2**1.5*MOU2*np2*p2p4**2   & 
          -  & 
           44*MIN2**2*p1p2*p2p4**2   & 
          +  & 
           4*MIN2*MOU2*p1p2*p2p4**2   & 
          -  & 
           16*MIN2**1.5*np2*p1p2*p2p4**2   & 
          +  & 
           24*MIN2*p1p2**2*p2p4**2   & 
          -  & 
           23*MIN2**2*p1p4*p2p4**2   & 
          -  & 
           MIN2*MOU2*p1p4*p2p4**2   & 
          -  & 
           2*MIN2**1.5*np2*p1p4*p2p4**2   & 
          -  & 
           2*MIN2*p1p2*p1p4*p2p4**2   & 
          +  & 
           4*MOU2*p1p2*p1p4*p2p4**2   & 
          -  & 
           8*sqrt(MIN2)*np2*p1p2*p1p4*p2p4**2   & 
          +  & 
           32*p1p2**2*p1p4*p2p4**2   & 
          -  & 
           14*MIN2*p1p4**2*p2p4**2   & 
          +  & 
           10*MOU2*p1p4**2*p2p4**2   & 
          +  & 
           56*p1p2*p1p4**2*p2p4**2   & 
          + 24*p1p4**3*p2p4**2   & 
          +  & 
           22*MIN2**2*p2p4**3   & 
          - 2*MIN2*MOU2*p2p4**3   & 
          +  & 
           8*MIN2**1.5*np2*p2p4**3   & 
          - 24*MIN2*p1p2*p2p4**3   & 
          -  & 
           6*MIN2*p1p4*p2p4**3   & 
          - 2*MOU2*p1p4*p2p4**3   & 
          -  & 
           24*p1p2*p1p4*p2p4**3   & 
          - 24*p1p4**2*p2p4**3   & 
          +  & 
           8*MIN2*p2p4**4   & 
          + 8*p1p4*p2p4**4   & 
          -  & 
           sqrt(MIN2)*np4* & 
            (2*MIN2**2*MOU2*p1p4   & 
          + 14*MIN2**2*p1p2*p1p4   & 
          -  & 
              6*MIN2*MOU2*p1p2*p1p4   & 
          - 19*MIN2*p1p2**2*p1p4   & 
          +  & 
              3*MOU2*p1p2**2*p1p4   & 
          + 8*p1p2**3*p1p4   & 
          +  & 
              5*MIN2**2*p1p4**2   & 
          - 4*MIN2*MOU2*p1p4**2   & 
          -  & 
              19*MIN2*p1p2*p1p4**2   & 
          + 7*MOU2*p1p2*p1p4**2   & 
          +  & 
              14*p1p2**2*p1p4**2   & 
          - 2*MIN2*p1p4**3   & 
          +  & 
              2*MOU2*p1p4**3   & 
          + 6*p1p2*p1p4**3   & 
          -  & 
              2*MIN2**2*MOU2*p2p4   & 
          - 12*MIN2**2*p1p2*p2p4   & 
          +  & 
              4*MIN2*MOU2*p1p2*p2p4   & 
          + 18*MIN2*p1p2**2*p2p4   & 
          -  & 
              2*MOU2*p1p2**2*p2p4   & 
          - 8*p1p2**3*p2p4   & 
          -  & 
              19*MIN2**2*p1p4*p2p4   & 
          +  & 
              14*MIN2*MOU2*p1p4*p2p4   & 
          +  & 
              59*MIN2*p1p2*p1p4*p2p4   & 
          -  & 
              13*MOU2*p1p2*p1p4*p2p4   & 
          -  & 
              40*p1p2**2*p1p4*p2p4   & 
          + 24*MIN2*p1p4**2*p2p4   & 
          -  & 
              14*MOU2*p1p4**2*p2p4   & 
          - 38*p1p2*p1p4**2*p2p4   & 
          -  & 
              8*p1p4**3*p2p4   & 
          + 12*MIN2**2*p2p4**2   & 
          -  & 
              4*MIN2*MOU2*p2p4**2   & 
          - 36*MIN2*p1p2*p2p4**2   & 
          +  & 
              4*MOU2*p1p2*p2p4**2   & 
          + 24*p1p2**2*p2p4**2   & 
          -  & 
              40*MIN2*p1p4*p2p4**2   & 
          + 10*MOU2*p1p4*p2p4**2   & 
          +  & 
              56*p1p2*p1p4*p2p4**2   & 
          + 24*p1p4**2*p2p4**2   & 
          +  & 
              18*MIN2*p2p4**3   & 
          - 2*MOU2*p2p4**3   & 
          -  & 
              24*p1p2*p2p4**3   & 
          - 24*p1p4*p2p4**3   & 
          +  & 
              8*p2p4**4   & 
          +  & 
              MOU2**2*p1p4*(MIN2   & 
          - p1p4   & 
          + p2p4)   & 
          +  & 
              MIN2**3*(  & 
          -3*p1p4   & 
          + 2*p2p4))   & 
          -  & 
           CMPLX(aa,bb)*asym*sqrt(MIN2)* & 
            (  & 
          -2*MIN2*MOU2*p1p4   & 
          + MOU2**2*p1p4   & 
          +  & 
              5*MOU2*p1p2*p1p4   & 
          - MOU2*p1p4**2   & 
          +  & 
              2*MIN2*MOU2*p2p4   & 
          - 2*MOU2*p1p2*p2p4   & 
          -  & 
              7*MOU2*p1p4*p2p4   & 
          + 2*MOU2*p2p4**2   & 
          -  & 
              (MIN2   & 
          - p1p2   & 
          - p1p4   & 
          + p2p4)* & 
               (  & 
          -(MIN2*p1p4)   & 
          + 4*p1p2*p1p4   & 
          - 2*p1p4**2   & 
          +  & 
                 2*MIN2*p2p4   & 
          - 4*p1p2*p2p4   & 
          - 4*p1p4*p2p4   & 
          +  & 
                 4*p2p4**2)))* & 
         Ccache(5))/ & 
       (3.*p1p4**2*(p1p4   & 
          - p2p4)*Pi**2)   & 
          +  & 
      (2*EL**4*GF**2*(14*MIN2**3*p1p2*p1p4   & 
          -  & 
           20*MIN2**2*MOU2*p1p2*p1p4   & 
          +  & 
           6*MIN2*MOU2**2*p1p2*p1p4   & 
          +  & 
           10*MIN2**2.5*np2*p1p2*p1p4   & 
          -  & 
           10*MIN2**1.5*MOU2*np2*p1p2*p1p4   & 
          -  & 
           46*MIN2**2*p1p2**2*p1p4   & 
          +  & 
           30*MIN2*MOU2*p1p2**2*p1p4   & 
          -  & 
           32*MIN2**1.5*np2*p1p2**2*p1p4   & 
          +  & 
           12*sqrt(MIN2)*MOU2*np2*p1p2**2*p1p4   & 
          +  & 
           48*MIN2*p1p2**3*p1p4   & 
          - 12*MOU2*p1p2**3*p1p4   & 
          +  & 
           16*sqrt(MIN2)*np2*p1p2**3*p1p4   & 
          -  & 
           16*p1p2**4*p1p4   & 
          - 13*MIN2**3*p1p4**2   & 
          +  & 
           18*MIN2**2*MOU2*p1p4**2   & 
          -  & 
           5*MIN2*MOU2**2*p1p4**2   & 
          + MIN2**2.5*np2*p1p4**2   & 
          -  & 
           4*MIN2**1.5*MOU2*np2*p1p4**2   & 
          +  & 
           3*sqrt(MIN2)*MOU2**2*np2*p1p4**2   & 
          +  & 
           6*MIN2**2*p1p2*p1p4**2   & 
          +  & 
           22*MIN2*MOU2*p1p2*p1p4**2   & 
          -  & 
           12*MOU2**2*p1p2*p1p4**2   & 
          +  & 
           4*MIN2**1.5*np2*p1p2*p1p4**2   & 
          +  & 
           40*MIN2*p1p2**2*p1p4**2   & 
          -  & 
           24*MOU2*p1p2**2*p1p4**2   & 
          +  & 
           16*sqrt(MIN2)*np2*p1p2**2*p1p4**2   & 
          -  & 
           32*p1p2**3*p1p4**2   & 
          + 4*MIN2*MOU2*p1p4**3   & 
          -  & 
           4*MOU2**2*p1p4**3   & 
          - 20*MIN2**1.5*np2*p1p4**3   & 
          +  & 
           20*sqrt(MIN2)*MOU2*np2*p1p4**3   & 
          +  & 
           44*MIN2*p1p2*p1p4**3   & 
          - 52*MOU2*p1p2*p1p4**3   & 
          +  & 
           12*sqrt(MIN2)*np2*p1p2*p1p4**3   & 
          -  & 
           48*p1p2**2*p1p4**3   & 
          + 8*MIN2*p1p4**4   & 
          -  & 
           8*MOU2*p1p4**4   & 
          + 8*sqrt(MIN2)*np2*p1p4**4   & 
          -  & 
           32*p1p2*p1p4**4   & 
          - 14*MIN2**4*p2p4   & 
          +  & 
           20*MIN2**3*MOU2*p2p4   & 
          - 6*MIN2**2*MOU2**2*p2p4   & 
          -  & 
           10*MIN2**3.5*np2*p2p4   & 
          +  & 
           10*MIN2**2.5*MOU2*np2*p2p4   & 
          +  & 
           46*MIN2**3*p1p2*p2p4   & 
          -  & 
           30*MIN2**2*MOU2*p1p2*p2p4   & 
          +  & 
           32*MIN2**2.5*np2*p1p2*p2p4   & 
          -  & 
           12*MIN2**1.5*MOU2*np2*p1p2*p2p4   & 
          -  & 
           48*MIN2**2*p1p2**2*p2p4   & 
          +  & 
           12*MIN2*MOU2*p1p2**2*p2p4   & 
          -  & 
           16*MIN2**1.5*np2*p1p2**2*p2p4   & 
          +  & 
           16*MIN2*p1p2**3*p2p4   & 
          + 33*MIN2**3*p1p4*p2p4   & 
          -  & 
           42*MIN2**2*MOU2*p1p4*p2p4   & 
          +  & 
           9*MIN2*MOU2**2*p1p4*p2p4   & 
          +  & 
           2*MIN2**2.5*np2*p1p4*p2p4   & 
          -  & 
           2*MIN2**1.5*MOU2*np2*p1p4*p2p4   & 
          -  & 
           32*MIN2**2*p1p2*p1p4*p2p4   & 
          +  & 
           12*MIN2**1.5*np2*p1p2*p1p4*p2p4   & 
          -  & 
           12*sqrt(MIN2)*MOU2*np2*p1p2*p1p4*p2p4   & 
          -  & 
           52*MIN2*p1p2**2*p1p4*p2p4   & 
          +  & 
           24*MOU2*p1p2**2*p1p4*p2p4   & 
          -  & 
           32*sqrt(MIN2)*np2*p1p2**2*p1p4*p2p4   & 
          +  & 
           48*p1p2**3*p1p4*p2p4   & 
          - 22*MIN2**2*p1p4**2*p2p4   & 
          -  & 
           6*MIN2*MOU2*p1p4**2*p2p4   & 
          +  & 
           12*MOU2**2*p1p4**2*p2p4   & 
          +  & 
           8*MIN2**1.5*np2*p1p4**2*p2p4   & 
          -  & 
           4*sqrt(MIN2)*MOU2*np2*p1p4**2*p2p4   & 
          -  & 
           92*MIN2*p1p2*p1p4**2*p2p4   & 
          +  & 
           72*MOU2*p1p2*p1p4**2*p2p4   & 
          -  & 
           32*sqrt(MIN2)*np2*p1p2*p1p4**2*p2p4   & 
          +  & 
           128*p1p2**2*p1p4**2*p2p4   & 
          - 48*MIN2*p1p4**3*p2p4   & 
          +  & 
           64*MOU2*p1p4**3*p2p4   & 
          -  & 
           12*sqrt(MIN2)*np2*p1p4**3*p2p4   & 
          +  & 
           128*p1p2*p1p4**3*p2p4   & 
          + 32*p1p4**4*p2p4   & 
          -  & 
           46*MIN2**3*p2p4**2   & 
          + 30*MIN2**2*MOU2*p2p4**2   & 
          -  & 
           32*MIN2**2.5*np2*p2p4**2   & 
          +  & 
           12*MIN2**1.5*MOU2*np2*p2p4**2   & 
          +  & 
           96*MIN2**2*p1p2*p2p4**2   & 
          -  & 
           24*MIN2*MOU2*p1p2*p2p4**2   & 
          +  & 
           32*MIN2**1.5*np2*p1p2*p2p4**2   & 
          -  & 
           48*MIN2*p1p2**2*p2p4**2   & 
          +  & 
           74*MIN2**2*p1p4*p2p4**2   & 
          -  & 
           26*MIN2*MOU2*p1p4*p2p4**2   & 
          +  & 
           20*MIN2**1.5*np2*p1p4*p2p4**2   & 
          -  & 
           20*MIN2*p1p2*p1p4*p2p4**2   & 
          -  & 
           24*MOU2*p1p2*p1p4*p2p4**2   & 
          +  & 
           16*sqrt(MIN2)*np2*p1p2*p1p4*p2p4**2   & 
          -  & 
           64*p1p2**2*p1p4*p2p4**2   & 
          +  & 
           48*MIN2*p1p4**2*p2p4**2   & 
          -  & 
           44*MOU2*p1p4**2*p2p4**2   & 
          +  & 
           16*sqrt(MIN2)*np2*p1p4**2*p2p4**2   & 
          -  & 
           160*p1p2*p1p4**2*p2p4**2   & 
          - 80*p1p4**3*p2p4**2   & 
          -  & 
           48*MIN2**2*p2p4**3   & 
          + 12*MIN2*MOU2*p2p4**3   & 
          -  & 
           16*MIN2**1.5*np2*p2p4**3   & 
          + 48*MIN2*p1p2*p2p4**3   & 
          +  & 
           24*MIN2*p1p4*p2p4**3   & 
          + 12*MOU2*p1p4*p2p4**3   & 
          +  & 
           48*p1p2*p1p4*p2p4**3   & 
          + 64*p1p4**2*p2p4**3   & 
          -  & 
           16*MIN2*p2p4**4   & 
          - 16*p1p4*p2p4**4   & 
          +  & 
           CMPLX(aa,bb)*asym*sqrt(MIN2)* & 
            (5*MIN2**2*p1p4   & 
          - 2*MIN2*MOU2*p1p4   & 
          -  & 
              3*MOU2**2*p1p4   & 
          - 12*MIN2*p1p2*p1p4   & 
          -  & 
              4*MOU2*p1p2*p1p4   & 
          - 4*MIN2*p1p4**2   & 
          +  & 
              4*MOU2*p1p4**2   & 
          + 20*p1p2*p1p4**2   & 
          - 8*p1p4**3   & 
          -  & 
              4*MIN2**2*p2p4   & 
          + 4*MIN2*MOU2*p2p4   & 
          +  & 
              8*MIN2*p1p2*p2p4   & 
          + 20*MIN2*p1p4*p2p4   & 
          -  & 
              4*MOU2*p1p4*p2p4   & 
          - 16*p1p2*p1p4*p2p4   & 
          -  & 
              20*p1p4**2*p2p4   & 
          - 8*MIN2*p2p4**2   & 
          +  & 
              16*p1p4*p2p4**2)   & 
          +  & 
           sqrt(MIN2)*np4* & 
            (  & 
          -9*MIN2**3*p1p4   & 
          + 18*MIN2**2*MOU2*p1p4   & 
          -  & 
              9*MIN2*MOU2**2*p1p4   & 
          - 46*MIN2*MOU2*p1p2*p1p4   & 
          +  & 
              9*MOU2**2*p1p2*p1p4   & 
          - 44*MIN2*p1p2**2*p1p4   & 
          +  & 
              24*MOU2*p1p2**2*p1p4   & 
          + 16*p1p2**3*p1p4   & 
          -  & 
              20*MIN2*MOU2*p1p4**2   & 
          + 4*MOU2**2*p1p4**2   & 
          -  & 
              56*MIN2*p1p2*p1p4**2   & 
          + 32*MOU2*p1p2*p1p4**2   & 
          +  & 
              36*p1p2**2*p1p4**2   & 
          - 8*MIN2*p1p4**3   & 
          +  & 
              8*MOU2*p1p4**3   & 
          + 24*p1p2*p1p4**3   & 
          +  & 
              10*MIN2**3*p2p4   & 
          - 10*MIN2**2*MOU2*p2p4   & 
          +  & 
              22*MIN2*MOU2*p1p2*p2p4   & 
          +  & 
              48*MIN2*p1p2**2*p2p4   & 
          - 12*MOU2*p1p2**2*p2p4   & 
          -  & 
              16*p1p2**3*p2p4   & 
          + 74*MIN2*MOU2*p1p4*p2p4   & 
          -  & 
              12*MOU2**2*p1p4*p2p4   & 
          +  & 
              168*MIN2*p1p2*p1p4*p2p4   & 
          -  & 
              68*MOU2*p1p2*p1p4*p2p4   & 
          -  & 
              96*p1p2**2*p1p4*p2p4   & 
          + 80*MIN2*p1p4**2*p2p4   & 
          -  & 
              64*MOU2*p1p4**2*p2p4   & 
          - 116*p1p2*p1p4**2*p2p4   & 
          -  & 
              32*p1p4**3*p2p4   & 
          - 22*MIN2*MOU2*p2p4**2   & 
          -  & 
              96*MIN2*p1p2*p2p4**2   & 
          + 24*MOU2*p1p2*p2p4**2   & 
          +  & 
              48*p1p2**2*p2p4**2   & 
          - 124*MIN2*p1p4*p2p4**2   & 
          +  & 
              44*MOU2*p1p4*p2p4**2   & 
          + 144*p1p2*p1p4*p2p4**2   & 
          +  & 
              80*p1p4**2*p2p4**2   & 
          + 48*MIN2*p2p4**3   & 
          -  & 
              12*MOU2*p2p4**3   & 
          - 48*p1p2*p2p4**3   & 
          -  & 
              64*p1p4*p2p4**3   & 
          + 16*p2p4**4   & 
          +  & 
              MIN2**2*(37*p1p2*p1p4   & 
          + 16*p1p4**2   & 
          -  & 
                 42*p1p2*p2p4   & 
          - 62*p1p4*p2p4   & 
          + 42*p2p4**2)))* & 
         Ccache(6))/ & 
       (3.*p1p4**2*(p1p4   & 
          - p2p4)*Pi**2)   & 
          -  & 
      (4*EL**4*GF**2*(  & 
          -4*MIN2**3*p1p2*p1p4   & 
          +  & 
           4*MIN2**2*MOU2*p1p2*p1p4   & 
          +  & 
           10*MIN2**2*p1p2**2*p1p4   & 
          -  & 
           2*MIN2*MOU2*p1p2**2*p1p4   & 
          +  & 
           2*MIN2**1.5*np2*p1p2**2*p1p4   & 
          -  & 
           6*MIN2*p1p2**3*p1p4   & 
          + 4*MIN2**3*p1p4**2   & 
          -  & 
           4*MIN2**2*MOU2*p1p4**2   & 
          -  & 
           2*MIN2**2.5*np2*p1p4**2   & 
          +  & 
           2*MIN2**1.5*MOU2*np2*p1p4**2   & 
          -  & 
           MIN2**2*p1p2*p1p4**2   & 
          - 7*MIN2*MOU2*p1p2*p1p4**2   & 
          +  & 
           MIN2**1.5*np2*p1p2*p1p4**2   & 
          -  & 
           sqrt(MIN2)*MOU2*np2*p1p2*p1p4**2   & 
          -  & 
           12*MIN2*p1p2**2*p1p4**2   & 
          +  & 
           2*MOU2*p1p2**2*p1p4**2   & 
          -  & 
           4*sqrt(MIN2)*np2*p1p2**2*p1p4**2   & 
          +  & 
           8*p1p2**3*p1p4**2   & 
          - 3*MIN2**2*p1p4**3   & 
          +  & 
           3*MIN2*MOU2*p1p4**3   & 
          + 5*MIN2**1.5*np2*p1p4**3   & 
          -  & 
           3*sqrt(MIN2)*MOU2*np2*p1p4**3   & 
          -  & 
           4*MIN2*p1p2*p1p4**3   & 
          + 8*MOU2*p1p2*p1p4**3   & 
          -  & 
           2*sqrt(MIN2)*np2*p1p2*p1p4**3   & 
          +  & 
           8*p1p2**2*p1p4**3   & 
          - 2*MIN2*p1p4**4   & 
          +  & 
           2*MOU2*p1p4**4   & 
          - 2*sqrt(MIN2)*np2*p1p4**4   & 
          +  & 
           8*p1p2*p1p4**4   & 
          + 4*MIN2**4*p2p4   & 
          -  & 
           4*MIN2**3*MOU2*p2p4   & 
          - 10*MIN2**3*p1p2*p2p4   & 
          +  & 
           2*MIN2**2*MOU2*p1p2*p2p4   & 
          -  & 
           2*MIN2**2.5*np2*p1p2*p2p4   & 
          +  & 
           6*MIN2**2*p1p2**2*p2p4   & 
          - 10*MIN2**3*p1p4*p2p4   & 
          +  & 
           10*MIN2**2*MOU2*p1p4*p2p4   & 
          +  & 
           2*MIN2**2.5*np2*p1p4*p2p4   & 
          +  & 
           11*MIN2**2*p1p2*p1p4*p2p4   & 
          -  & 
           MIN2*MOU2*p1p2*p1p4*p2p4   & 
          +  & 
           2*MIN2*p1p2**2*p1p4*p2p4   & 
          +  & 
           10*MIN2**2*p1p4**2*p2p4   & 
          -  & 
           2*MIN2*MOU2*p1p4**2*p2p4   & 
          -  & 
           5*MIN2**1.5*np2*p1p4**2*p2p4   & 
          +  & 
           sqrt(MIN2)*MOU2*np2*p1p4**2*p2p4   & 
          +  & 
           14*MIN2*p1p2*p1p4**2*p2p4   & 
          -  & 
           4*MOU2*p1p2*p1p4**2*p2p4   & 
          +  & 
           8*sqrt(MIN2)*np2*p1p2*p1p4**2*p2p4   & 
          -  & 
           24*p1p2**2*p1p4**2*p2p4   & 
          + 6*MIN2*p1p4**3*p2p4   & 
          -  & 
           12*MOU2*p1p4**3*p2p4   & 
          +  & 
           2*sqrt(MIN2)*np2*p1p4**3*p2p4   & 
          -  & 
           24*p1p2*p1p4**3*p2p4   & 
          - 8*p1p4**4*p2p4   & 
          +  & 
           10*MIN2**3*p2p4**2   & 
          - 2*MIN2**2*MOU2*p2p4**2   & 
          +  & 
           2*MIN2**2.5*np2*p2p4**2   & 
          -  & 
           12*MIN2**2*p1p2*p2p4**2   & 
          -  & 
           21*MIN2**2*p1p4*p2p4**2   & 
          +  & 
           3*MIN2*MOU2*p1p4*p2p4**2   & 
          -  & 
           2*MIN2**1.5*np2*p1p4*p2p4**2   & 
          +  & 
           14*MIN2*p1p2*p1p4*p2p4**2   & 
          -  & 
           2*MIN2*p1p4**2*p2p4**2   & 
          + 2*MOU2*p1p4**2*p2p4**2   & 
          -  & 
           4*sqrt(MIN2)*np2*p1p4**2*p2p4**2   & 
          +  & 
           24*p1p2*p1p4**2*p2p4**2   & 
          + 16*p1p4**3*p2p4**2   & 
          +  & 
           6*MIN2**2*p2p4**3   & 
          - 10*MIN2*p1p4*p2p4**3   & 
          -  & 
           8*p1p4**2*p2p4**3   & 
          -  & 
           CMPLX(aa,bb)*asym*sqrt(MIN2)* & 
            (  & 
          -(MIN2**2*p1p4)   & 
          + MIN2*MOU2*p1p4   & 
          +  & 
              3*MIN2*p1p2*p1p4   & 
          - MOU2*p1p2*p1p4   & 
          -  & 
              4*p1p2**2*p1p4   & 
          + MIN2*p1p4**2   & 
          - MOU2*p1p4**2   & 
          +  & 
              2*p1p2*p1p4**2   & 
          - 2*p1p4**3   & 
          - MIN2**2*p2p4   & 
          +  & 
              MIN2*MOU2*p2p4   & 
          + 2*MIN2*p1p2*p2p4   & 
          -  & 
              MIN2*p1p4*p2p4   & 
          - MOU2*p1p4*p2p4   & 
          +  & 
              4*p1p2*p1p4*p2p4   & 
          - 2*p1p4**2*p2p4   & 
          -  & 
              2*MIN2*p2p4**2)   & 
          -  & 
           sqrt(MIN2)*np4* & 
            (  & 
          -2*MIN2**3*p1p4   & 
          + 9*MIN2**2*p1p2*p1p4   & 
          -  & 
              11*MIN2*p1p2**2*p1p4   & 
          + 4*p1p2**3*p1p4   & 
          +  & 
              3*MIN2**2*p1p4**2   & 
          - 11*MIN2*p1p2*p1p4**2   & 
          +  & 
              6*p1p2**2*p1p4**2   & 
          - 2*MIN2*p1p4**3   & 
          +  & 
              6*p1p2*p1p4**3   & 
          - 2*MIN2**2*p1p2*p2p4   & 
          +  & 
              2*MIN2*p1p2**2*p2p4   & 
          - 11*MIN2**2*p1p4*p2p4   & 
          +  & 
              31*MIN2*p1p2*p1p4*p2p4   & 
          -  & 
              16*p1p2**2*p1p4*p2p4   & 
          + 16*MIN2*p1p4**2*p2p4   & 
          -  & 
              22*p1p2*p1p4**2*p2p4   & 
          - 8*p1p4**3*p2p4   & 
          +  & 
              2*MIN2**2*p2p4**2   & 
          - 4*MIN2*p1p2*p2p4**2   & 
          -  & 
              20*MIN2*p1p4*p2p4**2   & 
          + 20*p1p2*p1p4*p2p4**2   & 
          +  & 
              16*p1p4**2*p2p4**2   & 
          + 2*MIN2*p2p4**3   & 
          -  & 
              8*p1p4*p2p4**3   & 
          +  & 
              MOU2*p1p4* & 
               (2*MIN2**2   & 
          - 5*MIN2*p1p2   & 
          + p1p2**2   & 
          -  & 
                 3*MIN2*p1p4   & 
          + 5*p1p2*p1p4   & 
          + 2*p1p4**2   & 
          +  & 
                 9*MIN2*p2p4   & 
          - 3*p1p2*p2p4   & 
          - 12*p1p4*p2p4   & 
          +  & 
                 2*p2p4**2)))* & 
         Ccache(7))/ & 
       (3.*p1p4**2*(p1p4   & 
          - p2p4)*Pi**2))
!
!
      return
!
      end function

                          !!!!!!!!!!!!!!!!!!!!!!
                            END MODULE rad_T1TRIAC
                          !!!!!!!!!!!!!!!!!!!!!!
