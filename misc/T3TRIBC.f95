
                          !!!!!!!!!!!!!!!!!!!!!!
                              MODULE rad_T3TRIBC
                          !!!!!!!!!!!!!!!!!!!!!!
  contains
      function T3TRIBC(EL,GF,MOU2,MIN2,p1p2,p1p4,p2p4,asym,np2,np3, & 
         np4,mu2)
!
      use global_def, only: pi, prec
      implicit none
!

    complex(kind=prec) A0, B0i
    complex(kind=prec) B0
    complex(kind=prec) DB0
    complex(kind=prec) C0, C0i
    complex(kind=prec) D0, D0i

    integer, parameter :: bb0  = 1
    integer, parameter :: bb1  = 4
    integer, parameter :: cc0  = 1
    integer, parameter :: cc1  = 4
    integer, parameter :: cc2  = 7
    integer, parameter :: cc00 = 10
    integer, parameter :: cc11 = 13
    integer, parameter :: cc12 = 16
    integer, parameter :: cc22 = 19
    integer, parameter :: dd0  = 1
    integer, parameter :: dd1  = 4
    integer, parameter :: dd2  = 7
    integer, parameter :: dd3  = 10
    integer, parameter :: dd00 = 13
    
    

    external A0, B0i
    external B0
    external DB0
    external C0, C0i
    external D0, D0i
    
    complex(kind=prec) :: Ccache(7)
    complex(kind=prec) :: Bcache(1)

!
      real(kind=prec) T3TRIBC,EL,GF,MOU2,MIN2, & 
         p1p2,p1p4,p2p4,asym, & 
         np2,np3,np4,mu2, & 
         aa,bb
!
      aa=0._prec
      bb= & 
          -1._prec
!

!    
      call setlambda( & 
          -1._prec)
      call setdelta(0._prec)
      call setmudim(mu2)
      call setminmass(0._prec)
!     
    Bcache(1) = B0i(bb0,MIN2,0._prec,MIN2)
    Ccache(1) = C0i(cc0,MIN2,0._prec,MIN2-2*p1p4,0._prec,MIN2,MIN2)
    Ccache(2) = C0i(cc00,MIN2,0._prec,MIN2-2*p1p4,0._prec,MIN2,MIN2)
    Ccache(3) = C0i(cc1,MIN2,0._prec,MIN2-2*p1p4,0._prec,MIN2,MIN2)
    Ccache(4) = C0i(cc11,MIN2,0._prec,MIN2-2*p1p4,0._prec,MIN2,MIN2)
    Ccache(5) = C0i(cc12,MIN2,0._prec,MIN2-2*p1p4,0._prec,MIN2,MIN2)
    Ccache(6) = C0i(cc2,MIN2,0._prec,MIN2-2*p1p4,0._prec,MIN2,MIN2)
    Ccache(7) = C0i(cc22,MIN2,0._prec,MIN2-2*p1p4,0._prec,MIN2,MIN2)

!
      T3TRIBC=real((2*EL**4*GF**2*( & 
          -2*CMPLX(aa,bb)*asym*MIN2**1.5*p1p4  & 
          +  & 
           2*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p4  & 
          +  & 
           18*MIN2**2.5*np4*p1p4  & 
          -  & 
           36*MIN2**1.5*MOU2*np4*p1p4  & 
          +  & 
           18*sqrt(MIN2)*MOU2**2*np4*p1p4  & 
          -  & 
           3*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p1p4  & 
          -  & 
           20*MIN2**2*p1p2*p1p4  & 
          + 20*MIN2*MOU2*p1p2*p1p4  & 
          -  & 
           36*MIN2**1.5*np2*p1p2*p1p4  & 
          +  & 
           36*sqrt(MIN2)*MOU2*np2*p1p2*p1p4  & 
          -  & 
           46*MIN2**1.5*np4*p1p2*p1p4  & 
          +  & 
           46*sqrt(MIN2)*MOU2*np4*p1p2*p1p4  & 
          +  & 
           76*MIN2*p1p2**2*p1p4  & 
          - 36*MOU2*p1p2**2*p1p4  & 
          +  & 
           56*sqrt(MIN2)*np2*p1p2**2*p1p4  & 
          +  & 
           28*sqrt(MIN2)*np4*p1p2**2*p1p4  & 
          -  & 
           56*p1p2**3*p1p4  & 
          + 7*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4**2  & 
          +  & 
           2*MIN2**2*p1p4**2  & 
          + 16*MIN2*MOU2*p1p4**2  & 
          -  & 
           18*MOU2**2*p1p4**2  & 
          + 26*MIN2**1.5*np2*p1p4**2  & 
          -  & 
           26*sqrt(MIN2)*MOU2*np2*p1p4**2  & 
          -  & 
           20*MIN2**1.5*np4*p1p4**2  & 
          +  & 
           20*sqrt(MIN2)*MOU2*np4*p1p4**2  & 
          -  & 
           20*MIN2*p1p2*p1p4**2  & 
          - 20*MOU2*p1p2*p1p4**2  & 
          -  & 
           28*sqrt(MIN2)*np2*p1p2*p1p4**2  & 
          +  & 
           32*sqrt(MIN2)*np4*p1p2*p1p4**2  & 
          +  & 
           20*MIN2*p1p4**3  & 
          - 20*MOU2*p1p4**3  & 
          -  & 
           8*sqrt(MIN2)*np2*p1p4**3  & 
          - 24*p1p2*p1p4**3  & 
          -  & 
           9*CMPLX(aa,bb)*asym*MIN2**1.5*p2p4  & 
          + 20*MIN2**3*p2p4  & 
          +  & 
           9*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p2p4  & 
          -  & 
           20*MIN2**2*MOU2*p2p4  & 
          + 36*MIN2**2.5*np2*p2p4  & 
          -  & 
           36*MIN2**1.5*MOU2*np2*p2p4  & 
          -  & 
           36*MIN2**2.5*np4*p2p4  & 
          +  & 
           36*MIN2**1.5*MOU2*np4*p2p4  & 
          +  & 
           16*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p2p4  & 
          -  & 
           76*MIN2**2*p1p2*p2p4  & 
          + 36*MIN2*MOU2*p1p2*p2p4  & 
          -  & 
           56*MIN2**1.5*np2*p1p2*p2p4  & 
          +  & 
           92*MIN2**1.5*np4*p1p2*p2p4  & 
          -  & 
           36*sqrt(MIN2)*MOU2*np4*p1p2*p2p4  & 
          +  & 
           56*MIN2*p1p2**2*p2p4  & 
          -  & 
           56*sqrt(MIN2)*np4*p1p2**2*p2p4  & 
          +  & 
           3*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4*p2p4  & 
          -  & 
           2*MIN2**2*p1p4*p2p4  & 
          + 2*MIN2*MOU2*p1p4*p2p4  & 
          -  & 
           8*MIN2**1.5*np2*p1p4*p2p4  & 
          +  & 
           60*MIN2**1.5*np4*p1p4*p2p4  & 
          -  & 
           52*sqrt(MIN2)*MOU2*np4*p1p4*p2p4  & 
          -  & 
           56*MIN2*p1p2*p1p4*p2p4  & 
          + 36*MOU2*p1p2*p1p4*p2p4  & 
          -  & 
           56*sqrt(MIN2)*np2*p1p2*p1p4*p2p4  & 
          -  & 
           78*sqrt(MIN2)*np4*p1p2*p1p4*p2p4  & 
          +  & 
           112*p1p2**2*p1p4*p2p4  & 
          - 12*MIN2*p1p4**2*p2p4  & 
          +  & 
           52*MOU2*p1p4**2*p2p4  & 
          +  & 
           30*sqrt(MIN2)*np2*p1p4**2*p2p4  & 
          -  & 
           24*sqrt(MIN2)*np4*p1p4**2*p2p4  & 
          +  & 
           48*p1p2*p1p4**2*p2p4  & 
          + 24*p1p4**3*p2p4  & 
          -  & 
           16*CMPLX(aa,bb)*asym*sqrt(MIN2)*p2p4**2  & 
          + 76*MIN2**2*p2p4**2  & 
          -  & 
           36*MIN2*MOU2*p2p4**2  & 
          + 56*MIN2**1.5*np2*p2p4**2  & 
          -  & 
           74*MIN2**1.5*np4*p2p4**2  & 
          +  & 
           18*sqrt(MIN2)*MOU2*np4*p2p4**2  & 
          -  & 
           112*MIN2*p1p2*p2p4**2  & 
          +  & 
           80*sqrt(MIN2)*np4*p1p2*p2p4**2  & 
          -  & 
           8*MIN2*p1p4*p2p4**2  & 
          - 18*MOU2*p1p4*p2p4**2  & 
          +  & 
           48*sqrt(MIN2)*np4*p1p4*p2p4**2  & 
          -  & 
           80*p1p2*p1p4*p2p4**2  & 
          - 48*p1p4**2*p2p4**2  & 
          +  & 
           56*MIN2*p2p4**3  & 
          - 24*sqrt(MIN2)*np4*p2p4**3  & 
          +  & 
           24*p1p4*p2p4**3)*Bcache(1))/ & 
       (9.*p1p4**2*(p1p4  & 
          - p2p4)*Pi**2)  & 
          -  & 
      (2*EL**4*GF**2*(30*CMPLX(aa,bb)*asym*MIN2**2.5*p1p4  & 
          -  & 
           30*CMPLX(aa,bb)*asym*MIN2**1.5*MOU2*p1p4  & 
          -  & 
           18*MIN2**3.5*np4*p1p4  & 
          +  & 
           36*MIN2**2.5*MOU2*np4*p1p4  & 
          -  & 
           18*MIN2**1.5*MOU2**2*np4*p1p4  & 
          -  & 
           56*CMPLX(aa,bb)*asym*MIN2**1.5*p1p2*p1p4  & 
          +  & 
           28*MIN2**3*p1p2*p1p4  & 
          -  & 
           28*MIN2**2*MOU2*p1p2*p1p4  & 
          +  & 
           36*MIN2**2.5*np2*p1p2*p1p4  & 
          -  & 
           36*MIN2**1.5*MOU2*np2*p1p2*p1p4  & 
          +  & 
           50*MIN2**2.5*np4*p1p2*p1p4  & 
          -  & 
           50*MIN2**1.5*MOU2*np4*p1p2*p1p4  & 
          -  & 
           92*MIN2**2*p1p2**2*p1p4  & 
          +  & 
           36*MIN2*MOU2*p1p2**2*p1p4  & 
          -  & 
           64*MIN2**1.5*np2*p1p2**2*p1p4  & 
          -  & 
           32*MIN2**1.5*np4*p1p2**2*p1p4  & 
          +  & 
           64*MIN2*p1p2**3*p1p4  & 
          + 7*CMPLX(aa,bb)*asym*MIN2**1.5*p1p4**2  & 
          -  & 
           28*MIN2**3*p1p4**2  & 
          -  & 
           11*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p4**2  & 
          +  & 
           28*MIN2**2*MOU2*p1p4**2  & 
          -  & 
           36*MIN2**2.5*np2*p1p4**2  & 
          +  & 
           36*MIN2**1.5*MOU2*np2*p1p4**2  & 
          +  & 
           2*MIN2**2.5*np4*p1p4**2  & 
          +  & 
           16*MIN2**1.5*MOU2*np4*p1p4**2  & 
          -  & 
           18*sqrt(MIN2)*MOU2**2*np4*p1p4**2  & 
          +  & 
           13*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p1p4**2  & 
          +  & 
           122*MIN2**2*p1p2*p1p4**2  & 
          -  & 
           66*MIN2*MOU2*p1p2*p1p4**2  & 
          +  & 
           132*MIN2**1.5*np2*p1p2*p1p4**2  & 
          -  & 
           72*sqrt(MIN2)*MOU2*np2*p1p2*p1p4**2  & 
          +  & 
           21*MIN2**1.5*np4*p1p2*p1p4**2  & 
          -  & 
           57*sqrt(MIN2)*MOU2*np4*p1p2*p1p4**2  & 
          -  & 
           188*MIN2*p1p2**2*p1p4**2  & 
          +  & 
           72*MOU2*p1p2**2*p1p4**2  & 
          -  & 
           112*sqrt(MIN2)*np2*p1p2**2*p1p4**2  & 
          -  & 
           58*sqrt(MIN2)*np4*p1p2**2*p1p4**2  & 
          +  & 
           112*p1p2**3*p1p4**2  & 
          -  & 
           35*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4**3  & 
          - 42*MIN2**2*p1p4**3  & 
          +  & 
           24*MIN2*MOU2*p1p4**3  & 
          + 18*MOU2**2*p1p4**3  & 
          -  & 
           81*MIN2**1.5*np2*p1p4**3  & 
          +  & 
           85*sqrt(MIN2)*MOU2*np2*p1p4**3  & 
          +  & 
           8*MIN2**1.5*np4*p1p4**3  & 
          -  & 
           8*sqrt(MIN2)*MOU2*np4*p1p4**3  & 
          +  & 
           140*MIN2*p1p2*p1p4**3  & 
          - 28*MOU2*p1p2*p1p4**3  & 
          +  & 
           122*sqrt(MIN2)*np2*p1p2*p1p4**3  & 
          -  & 
           64*p1p2**2*p1p4**3  & 
          - 8*MIN2*p1p4**4  & 
          +  & 
           8*MOU2*p1p4**4  & 
          + 16*sqrt(MIN2)*np2*p1p4**4  & 
          -  & 
           16*p1p2*p1p4**4  & 
          - 24*CMPLX(aa,bb)*asym*MIN2**2.5*p2p4  & 
          -  & 
           28*MIN2**4*p2p4  & 
          + 24*CMPLX(aa,bb)*asym*MIN2**1.5*MOU2*p2p4  & 
          +  & 
           28*MIN2**3*MOU2*p2p4  & 
          - 36*MIN2**3.5*np2*p2p4  & 
          +  & 
           36*MIN2**2.5*MOU2*np2*p2p4  & 
          +  & 
           36*MIN2**3.5*np4*p2p4  & 
          -  & 
           36*MIN2**2.5*MOU2*np4*p2p4  & 
          +  & 
           48*CMPLX(aa,bb)*asym*MIN2**1.5*p1p2*p2p4  & 
          +  & 
           92*MIN2**3*p1p2*p2p4  & 
          -  & 
           36*MIN2**2*MOU2*p1p2*p2p4  & 
          +  & 
           64*MIN2**2.5*np2*p1p2*p2p4  & 
          -  & 
           100*MIN2**2.5*np4*p1p2*p2p4  & 
          +  & 
           36*MIN2**1.5*MOU2*np4*p1p2*p2p4  & 
          -  & 
           64*MIN2**2*p1p2**2*p2p4  & 
          +  & 
           64*MIN2**1.5*np4*p1p2**2*p2p4  & 
          +  & 
           71*CMPLX(aa,bb)*asym*MIN2**1.5*p1p4*p2p4  & 
          -  & 
           16*MIN2**3*p1p4*p2p4  & 
          -  & 
           21*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p4*p2p4  & 
          +  & 
           16*MIN2**2*MOU2*p1p4*p2p4  & 
          -  & 
           50*MIN2**2.5*np2*p1p4*p2p4  & 
          +  & 
           54*MIN2**1.5*MOU2*np2*p1p4*p2p4  & 
          -  & 
           67*MIN2**2.5*np4*p1p4*p2p4  & 
          +  & 
           63*MIN2**1.5*MOU2*np4*p1p4*p2p4  & 
          -  & 
           45*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p1p4*p2p4  & 
          +  & 
           170*MIN2**2*p1p2*p1p4*p2p4  & 
          -  & 
           90*MIN2*MOU2*p1p2*p1p4*p2p4  & 
          +  & 
           144*MIN2**1.5*np2*p1p2*p1p4*p2p4  & 
          +  & 
           41*MIN2**1.5*np4*p1p2*p1p4*p2p4  & 
          +  & 
           69*sqrt(MIN2)*MOU2*np4*p1p2*p1p4*p2p4  & 
          -  & 
           208*MIN2*p1p2**2*p1p4*p2p4  & 
          +  & 
           98*sqrt(MIN2)*np4*p1p2**2*p1p4*p2p4  & 
          +  & 
           6*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4**2*p2p4  & 
          -  & 
           34*MIN2**2*p1p4**2*p2p4  & 
          -  & 
           22*MIN2*MOU2*p1p4**2*p2p4  & 
          -  & 
           15*MIN2**1.5*np2*p1p4**2*p2p4  & 
          -  & 
           33*sqrt(MIN2)*MOU2*np2*p1p4**2*p2p4  & 
          +  & 
           48*MIN2**1.5*np4*p1p4**2*p2p4  & 
          -  & 
           16*sqrt(MIN2)*MOU2*np4*p1p4**2*p2p4  & 
          +  & 
           56*MIN2*p1p2*p1p4**2*p2p4  & 
          -  & 
           36*MOU2*p1p2*p1p4**2*p2p4  & 
          +  & 
           62*sqrt(MIN2)*np2*p1p2*p1p4**2*p2p4  & 
          -  & 
           2*sqrt(MIN2)*np4*p1p2*p1p4**2*p2p4  & 
          -  & 
           160*p1p2**2*p1p4**2*p2p4  & 
          -  & 
           112*MIN2*p1p4**3*p2p4  & 
          + 16*MOU2*p1p4**3*p2p4  & 
          -  & 
           142*sqrt(MIN2)*np2*p1p4**3*p2p4  & 
          -  & 
           16*sqrt(MIN2)*np4*p1p4**3*p2p4  & 
          +  & 
           144*p1p2*p1p4**3*p2p4  & 
          + 16*p1p4**4*p2p4  & 
          -  & 
           48*CMPLX(aa,bb)*asym*MIN2**1.5*p2p4**2  & 
          - 92*MIN2**3*p2p4**2  & 
          +  & 
           36*MIN2**2*MOU2*p2p4**2  & 
          -  & 
           64*MIN2**2.5*np2*p2p4**2  & 
          +  & 
           133*MIN2**2.5*np4*p2p4**2  & 
          -  & 
           69*MIN2**1.5*MOU2*np4*p2p4**2  & 
          +  & 
           128*MIN2**2*p1p2*p2p4**2  & 
          -  & 
           170*MIN2**1.5*np4*p1p2*p2p4**2  & 
          +  & 
           42*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4*p2p4**2  & 
          -  & 
           80*MIN2**2*p1p4*p2p4**2  & 
          +  & 
           72*MIN2*MOU2*p1p4*p2p4**2  & 
          -  & 
           80*MIN2**1.5*np2*p1p4*p2p4**2  & 
          -  & 
           126*MIN2**1.5*np4*p1p4*p2p4**2  & 
          +  & 
           36*sqrt(MIN2)*MOU2*np4*p1p4*p2p4**2  & 
          +  & 
           248*MIN2*p1p2*p1p4*p2p4**2  & 
          -  & 
           34*sqrt(MIN2)*np4*p1p2*p1p4*p2p4**2  & 
          +  & 
           112*MIN2*p1p4**2*p2p4**2  & 
          -  & 
           36*MOU2*p1p4**2*p2p4**2  & 
          +  & 
           50*sqrt(MIN2)*np2*p1p4**2*p2p4**2  & 
          +  & 
           80*sqrt(MIN2)*np4*p1p4**2*p2p4**2  & 
          -  & 
           16*p1p2*p1p4**2*p2p4**2  & 
          - 80*p1p4**3*p2p4**2  & 
          -  & 
           64*MIN2**2*p2p4**3  & 
          + 106*MIN2**1.5*np4*p2p4**3  & 
          -  & 
           104*MIN2*p1p4*p2p4**3  & 
          -  & 
           64*sqrt(MIN2)*np4*p1p4*p2p4**3  & 
          +  & 
           64*p1p4**2*p2p4**3)* & 
         Ccache(1))/ & 
       (9.*p1p4**2*(p1p4  & 
          - p2p4)*Pi**2)  & 
          +  & 
      (8*EL**4*GF**2*( & 
          -13*CMPLX(aa,bb)*asym*MIN2**1.5*p1p4  & 
          +  & 
           13*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p4  & 
          -  & 
           9*MIN2**2.5*np4*p1p4  & 
          +  & 
           18*MIN2**1.5*MOU2*np4*p1p4  & 
          -  & 
           9*sqrt(MIN2)*MOU2**2*np4*p1p4  & 
          +  & 
           26*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p1p4  & 
          +  & 
           10*MIN2**2*p1p2*p1p4  & 
          - 10*MIN2*MOU2*p1p2*p1p4  & 
          +  & 
           18*MIN2**1.5*np2*p1p2*p1p4  & 
          -  & 
           18*sqrt(MIN2)*MOU2*np2*p1p2*p1p4  & 
          +  & 
           23*MIN2**1.5*np4*p1p2*p1p4  & 
          -  & 
           23*sqrt(MIN2)*MOU2*np4*p1p2*p1p4  & 
          -  & 
           38*MIN2*p1p2**2*p1p4  & 
          + 18*MOU2*p1p2**2*p1p4  & 
          -  & 
           28*sqrt(MIN2)*np2*p1p2**2*p1p4  & 
          -  & 
           14*sqrt(MIN2)*np4*p1p2**2*p1p4  & 
          +  & 
           28*p1p2**3*p1p4  & 
          - MIN2**2*p1p4**2  & 
          -  & 
           8*MIN2*MOU2*p1p4**2  & 
          + 9*MOU2**2*p1p4**2  & 
          -  & 
           13*MIN2**1.5*np2*p1p4**2  & 
          +  & 
           13*sqrt(MIN2)*MOU2*np2*p1p4**2  & 
          -  & 
           4*MIN2**1.5*np4*p1p4**2  & 
          +  & 
           4*sqrt(MIN2)*MOU2*np4*p1p4**2  & 
          +  & 
           10*MIN2*p1p2*p1p4**2  & 
          + 10*MOU2*p1p2*p1p4**2  & 
          +  & 
           14*sqrt(MIN2)*np2*p1p2*p1p4**2  & 
          +  & 
           12*sqrt(MIN2)*np4*p1p2*p1p4**2  & 
          + 4*MIN2*p1p4**3  & 
          -  & 
           4*MOU2*p1p4**3  & 
          + 4*sqrt(MIN2)*np2*p1p4**3  & 
          -  & 
           16*p1p2*p1p4**3  & 
          + 15*CMPLX(aa,bb)*asym*MIN2**1.5*p2p4  & 
          -  & 
           10*MIN2**3*p2p4  & 
          - 15*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p2p4  & 
          +  & 
           10*MIN2**2*MOU2*p2p4  & 
          - 18*MIN2**2.5*np2*p2p4  & 
          +  & 
           18*MIN2**1.5*MOU2*np2*p2p4  & 
          +  & 
           18*MIN2**2.5*np4*p2p4  & 
          -  & 
           18*MIN2**1.5*MOU2*np4*p2p4  & 
          -  & 
           22*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p2p4  & 
          +  & 
           38*MIN2**2*p1p2*p2p4  & 
          - 18*MIN2*MOU2*p1p2*p2p4  & 
          +  & 
           28*MIN2**1.5*np2*p1p2*p2p4  & 
          -  & 
           46*MIN2**1.5*np4*p1p2*p2p4  & 
          +  & 
           18*sqrt(MIN2)*MOU2*np4*p1p2*p2p4  & 
          -  & 
           28*MIN2*p1p2**2*p2p4  & 
          +  & 
           28*sqrt(MIN2)*np4*p1p2**2*p2p4  & 
          -  & 
           26*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4*p2p4  & 
          +  & 
           MIN2**2*p1p4*p2p4  & 
          - MIN2*MOU2*p1p4*p2p4  & 
          +  & 
           4*MIN2**1.5*np2*p1p4*p2p4  & 
          +  & 
           4*MIN2**1.5*np4*p1p4*p2p4  & 
          -  & 
           8*sqrt(MIN2)*MOU2*np4*p1p4*p2p4  & 
          +  & 
           28*MIN2*p1p2*p1p4*p2p4  & 
          - 18*MOU2*p1p2*p1p4*p2p4  & 
          +  & 
           28*sqrt(MIN2)*np2*p1p2*p1p4*p2p4  & 
          -  & 
           22*sqrt(MIN2)*np4*p1p2*p1p4*p2p4  & 
          -  & 
           56*p1p2**2*p1p4*p2p4  & 
          - 28*MIN2*p1p4**2*p2p4  & 
          +  & 
           8*MOU2*p1p4**2*p2p4  & 
          -  & 
           22*sqrt(MIN2)*np2*p1p4**2*p2p4  & 
          -  & 
           16*sqrt(MIN2)*np4*p1p4**2*p2p4  & 
          +  & 
           44*p1p2*p1p4**2*p2p4  & 
          + 16*p1p4**3*p2p4  & 
          +  & 
           22*CMPLX(aa,bb)*asym*sqrt(MIN2)*p2p4**2  & 
          - 38*MIN2**2*p2p4**2  & 
          +  & 
           18*MIN2*MOU2*p2p4**2  & 
          - 28*MIN2**1.5*np2*p2p4**2  & 
          +  & 
           10*MIN2**1.5*np4*p2p4**2  & 
          +  & 
           18*sqrt(MIN2)*MOU2*np4*p2p4**2  & 
          +  & 
           56*MIN2*p1p2*p2p4**2  & 
          + 24*MIN2*p1p4*p2p4**2  & 
          -  & 
           18*MOU2*p1p4*p2p4**2  & 
          +  & 
           44*sqrt(MIN2)*np4*p1p4*p2p4**2  & 
          -  & 
           44*p1p4**2*p2p4**2  & 
          - 28*MIN2*p2p4**3  & 
          -  & 
           28*sqrt(MIN2)*np4*p2p4**3  & 
          + 28*p1p4*p2p4**3)* & 
         Ccache(2))/ & 
       (9.*p1p4**2*(p1p4  & 
          - p2p4)*Pi**2)  & 
          -  & 
      (2*EL**4*GF**2*(36*CMPLX(aa,bb)*asym*MIN2**2.5*p1p4  & 
          -  & 
           36*CMPLX(aa,bb)*asym*MIN2**1.5*MOU2*p1p4  & 
          -  & 
           70*CMPLX(aa,bb)*asym*MIN2**1.5*p1p2*p1p4  & 
          -  & 
           12*MIN2**3*p1p2*p1p4  & 
          -  & 
           6*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p2*p1p4  & 
          +  & 
           12*MIN2**2*MOU2*p1p2*p1p4  & 
          -  & 
           36*MIN2**2.5*np2*p1p2*p1p4  & 
          +  & 
           36*MIN2**1.5*MOU2*np2*p1p2*p1p4  & 
          +  & 
           26*MIN2**2.5*np4*p1p2*p1p4  & 
          -  & 
           26*MIN2**1.5*MOU2*np4*p1p2*p1p4  & 
          -  & 
           8*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2**2*p1p4  & 
          +  & 
           60*MIN2**2*p1p2**2*p1p4  & 
          -  & 
           36*MIN2*MOU2*p1p2**2*p1p4  & 
          +  & 
           48*MIN2**1.5*np2*p1p2**2*p1p4  & 
          -  & 
           58*MIN2**1.5*np4*p1p2**2*p1p4  & 
          +  & 
           18*sqrt(MIN2)*MOU2*np4*p1p2**2*p1p4  & 
          -  & 
           48*MIN2*p1p2**3*p1p4  & 
          +  & 
           32*sqrt(MIN2)*np4*p1p2**3*p1p4  & 
          +  & 
           37*CMPLX(aa,bb)*asym*MIN2**1.5*p1p4**2  & 
          + 12*MIN2**3*p1p4**2  & 
          -  & 
           33*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p4**2  & 
          -  & 
           12*MIN2**2*MOU2*p1p4**2  & 
          +  & 
           36*MIN2**2.5*np2*p1p4**2  & 
          -  & 
           36*MIN2**1.5*MOU2*np2*p1p4**2  & 
          -  & 
           8*MIN2**2.5*np4*p1p4**2  & 
          +  & 
           26*MIN2**1.5*MOU2*np4*p1p4**2  & 
          -  & 
           18*sqrt(MIN2)*MOU2**2*np4*p1p4**2  & 
          -  & 
           35*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p1p4**2  & 
          -  & 
           26*MIN2**2*p1p2*p1p4**2  & 
          +  & 
           2*MIN2*MOU2*p1p2*p1p4**2  & 
          +  & 
           54*MIN2**1.5*np2*p1p2*p1p4**2  & 
          -  & 
           90*sqrt(MIN2)*MOU2*np2*p1p2*p1p4**2  & 
          +  & 
           27*MIN2**1.5*np4*p1p2*p1p4**2  & 
          -  & 
           59*sqrt(MIN2)*MOU2*np4*p1p2*p1p4**2  & 
          -  & 
           116*MIN2*p1p2**2*p1p4**2  & 
          +  & 
           72*MOU2*p1p2**2*p1p4**2  & 
          -  & 
           144*sqrt(MIN2)*np2*p1p2**2*p1p4**2  & 
          -  & 
           58*sqrt(MIN2)*np4*p1p2**2*p1p4**2  & 
          +  & 
           112*p1p2**3*p1p4**2  & 
          -  & 
           35*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4**3  & 
          - 60*MIN2**2*p1p4**3  & 
          +  & 
           42*MIN2*MOU2*p1p4**3  & 
          + 18*MOU2**2*p1p4**3  & 
          -  & 
           99*MIN2**1.5*np2*p1p4**3  & 
          +  & 
           87*sqrt(MIN2)*MOU2*np2*p1p4**3  & 
          +  & 
           8*MIN2**1.5*np4*p1p4**3  & 
          -  & 
           8*sqrt(MIN2)*MOU2*np4*p1p4**3  & 
          +  & 
           208*MIN2*p1p2*p1p4**3  & 
          - 28*MOU2*p1p2*p1p4**3  & 
          +  & 
           122*sqrt(MIN2)*np2*p1p2*p1p4**3  & 
          -  & 
           64*p1p2**2*p1p4**3  & 
          - 8*MIN2*p1p4**4  & 
          +  & 
           8*MOU2*p1p4**4  & 
          + 16*sqrt(MIN2)*np2*p1p4**4  & 
          -  & 
           16*p1p2*p1p4**4  & 
          - 48*CMPLX(aa,bb)*asym*MIN2**2.5*p2p4  & 
          +  & 
           12*MIN2**4*p2p4  & 
          + 48*CMPLX(aa,bb)*asym*MIN2**1.5*MOU2*p2p4  & 
          -  & 
           12*MIN2**3*MOU2*p2p4  & 
          + 36*MIN2**3.5*np2*p2p4  & 
          -  & 
           36*MIN2**2.5*MOU2*np2*p2p4  & 
          -  & 
           36*MIN2**3.5*np4*p2p4  & 
          +  & 
           36*MIN2**2.5*MOU2*np4*p2p4  & 
          +  & 
           80*CMPLX(aa,bb)*asym*MIN2**1.5*p1p2*p2p4  & 
          -  & 
           60*MIN2**3*p1p2*p2p4  & 
          +  & 
           36*MIN2**2*MOU2*p1p2*p2p4  & 
          -  & 
           48*MIN2**2.5*np2*p1p2*p2p4  & 
          +  & 
           84*MIN2**2.5*np4*p1p2*p2p4  & 
          -  & 
           36*MIN2**1.5*MOU2*np4*p1p2*p2p4  & 
          +  & 
           48*MIN2**2*p1p2**2*p2p4  & 
          -  & 
           48*MIN2**1.5*np4*p1p2**2*p2p4  & 
          +  & 
           71*CMPLX(aa,bb)*asym*MIN2**1.5*p1p4*p2p4  & 
          -  & 
           40*MIN2**3*p1p4*p2p4  & 
          -  & 
           9*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p4*p2p4  & 
          +  & 
           40*MIN2**2*MOU2*p1p4*p2p4  & 
          -  & 
           120*MIN2**2.5*np2*p1p4*p2p4  & 
          +  & 
           108*MIN2**1.5*MOU2*np2*p1p4*p2p4  & 
          -  & 
           35*MIN2**2.5*np4*p1p4*p2p4  & 
          +  & 
           47*MIN2**1.5*MOU2*np4*p1p4*p2p4  & 
          -  & 
           13*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p1p4*p2p4  & 
          +  & 
           134*MIN2**2*p1p2*p1p4*p2p4  & 
          -  & 
           54*MIN2*MOU2*p1p2*p1p4*p2p4  & 
          +  & 
           120*MIN2**1.5*np2*p1p2*p1p4*p2p4  & 
          +  & 
           59*MIN2**1.5*np4*p1p2*p1p4*p2p4  & 
          +  & 
           69*sqrt(MIN2)*MOU2*np4*p1p2*p1p4*p2p4  & 
          -  & 
           40*MIN2*p1p2**2*p1p4*p2p4  & 
          +  & 
           66*sqrt(MIN2)*np4*p1p2**2*p1p4*p2p4  & 
          +  & 
           54*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4**2*p2p4  & 
          +  & 
           168*MIN2**2*p1p4**2*p2p4  & 
          -  & 
           144*MIN2*MOU2*p1p4**2*p2p4  & 
          +  & 
           115*MIN2**1.5*np2*p1p4**2*p2p4  & 
          -  & 
           33*sqrt(MIN2)*MOU2*np2*p1p4**2*p2p4  & 
          +  & 
           44*MIN2**1.5*np4*p1p4**2*p2p4  & 
          -  & 
           16*sqrt(MIN2)*MOU2*np4*p1p4**2*p2p4  & 
          -  & 
           228*MIN2*p1p2*p1p4**2*p2p4  & 
          -  & 
           36*MOU2*p1p2*p1p4**2*p2p4  & 
          +  & 
           94*sqrt(MIN2)*np2*p1p2*p1p4**2*p2p4  & 
          -  & 
           2*sqrt(MIN2)*np4*p1p2*p1p4**2*p2p4  & 
          -  & 
           160*p1p2**2*p1p4**2*p2p4  & 
          -  & 
           196*MIN2*p1p4**3*p2p4  & 
          + 16*MOU2*p1p4**3*p2p4  & 
          -  & 
           142*sqrt(MIN2)*np2*p1p4**3*p2p4  & 
          -  & 
           16*sqrt(MIN2)*np4*p1p4**3*p2p4  & 
          +  & 
           144*p1p2*p1p4**3*p2p4  & 
          + 16*p1p4**4*p2p4  & 
          -  & 
           80*CMPLX(aa,bb)*asym*MIN2**1.5*p2p4**2  & 
          + 60*MIN2**3*p2p4**2  & 
          -  & 
           36*MIN2**2*MOU2*p2p4**2  & 
          +  & 
           48*MIN2**2.5*np2*p2p4**2  & 
          +  & 
           39*MIN2**2.5*np4*p2p4**2  & 
          -  & 
           87*MIN2**1.5*MOU2*np4*p2p4**2  & 
          -  & 
           96*MIN2**2*p1p2*p2p4**2  & 
          -  & 
           90*MIN2**1.5*np4*p1p2*p2p4**2  & 
          +  & 
           18*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4*p2p4**2  & 
          -  & 
           248*MIN2**2*p1p4*p2p4**2  & 
          +  & 
           162*MIN2*MOU2*p1p4*p2p4**2  & 
          -  & 
           168*MIN2**1.5*np2*p1p4*p2p4**2  & 
          -  & 
           154*MIN2**1.5*np4*p1p4*p2p4**2  & 
          +  & 
           36*sqrt(MIN2)*MOU2*np4*p1p4*p2p4**2  & 
          +  & 
           336*MIN2*p1p2*p1p4*p2p4**2  & 
          -  & 
           34*sqrt(MIN2)*np4*p1p2*p1p4*p2p4**2  & 
          +  & 
           340*MIN2*p1p4**2*p2p4**2  & 
          -  & 
           36*MOU2*p1p4**2*p2p4**2  & 
          +  & 
           50*sqrt(MIN2)*np2*p1p4**2*p2p4**2  & 
          +  & 
           80*sqrt(MIN2)*np4*p1p4**2*p2p4**2  & 
          -  & 
           16*p1p2*p1p4**2*p2p4**2  & 
          - 80*p1p4**3*p2p4**2  & 
          +  & 
           48*MIN2**2*p2p4**3  & 
          + 138*MIN2**1.5*np4*p2p4**3  & 
          -  & 
           248*MIN2*p1p4*p2p4**3  & 
          -  & 
           64*sqrt(MIN2)*np4*p1p4*p2p4**3  & 
          +  & 
           64*p1p4**2*p2p4**3)* & 
         Ccache(3))/ & 
       (9.*p1p4**2*(p1p4  & 
          - p2p4)*Pi**2)  & 
          -  & 
      (8*EL**4*GF**2*sqrt(MIN2)* & 
         ( & 
          -4*CMPLX(aa,bb)*asym*MIN2**2*p1p4  & 
          + 4*CMPLX(aa,bb)*asym*MIN2*MOU2*p1p4  & 
          +  & 
           11*CMPLX(aa,bb)*asym*MIN2*p1p2*p1p4  & 
          - 10*MIN2**2.5*p1p2*p1p4  & 
          -  & 
           6*CMPLX(aa,bb)*asym*MOU2*p1p2*p1p4  & 
          +  & 
           10*MIN2**1.5*MOU2*p1p2*p1p4  & 
          -  & 
           18*MIN2**2*np2*p1p2*p1p4  & 
          +  & 
           18*MIN2*MOU2*np2*p1p2*p1p4  & 
          +  & 
           9*MIN2**2*np4*p1p2*p1p4  & 
          -  & 
           9*MIN2*MOU2*np4*p1p2*p1p4  & 
          - 8*CMPLX(aa,bb)*asym*p1p2**2*p1p4  & 
          +  & 
           38*MIN2**1.5*p1p2**2*p1p4  & 
          -  & 
           18*sqrt(MIN2)*MOU2*p1p2**2*p1p4  & 
          +  & 
           28*MIN2*np2*p1p2**2*p1p4  & 
          -  & 
           23*MIN2*np4*p1p2**2*p1p4  & 
          +  & 
           9*MOU2*np4*p1p2**2*p1p4  & 
          -  & 
           28*sqrt(MIN2)*p1p2**3*p1p4  & 
          +  & 
           14*np4*p1p2**3*p1p4  & 
          + CMPLX(aa,bb)*asym*MIN2*p1p4**2  & 
          +  & 
           10*MIN2**2.5*p1p4**2  & 
          + 2*CMPLX(aa,bb)*asym*MOU2*p1p4**2  & 
          -  & 
           10*MIN2**1.5*MOU2*p1p4**2  & 
          +  & 
           18*MIN2**2*np2*p1p4**2  & 
          -  & 
           18*MIN2*MOU2*np2*p1p4**2  & 
          -  & 
           5*MIN2**2*np4*p1p4**2  & 
          + 5*MIN2*MOU2*np4*p1p4**2  & 
          -  & 
           37*MIN2**1.5*p1p2*p1p4**2  & 
          +  & 
           17*sqrt(MIN2)*MOU2*p1p2*p1p4**2  & 
          -  & 
           15*MIN2*np2*p1p2*p1p4**2  & 
          -  & 
           9*MOU2*np2*p1p2*p1p4**2  & 
          +  & 
           11*MIN2*np4*p1p2*p1p4**2  & 
          -  & 
           5*MOU2*np4*p1p2*p1p4**2  & 
          +  & 
           18*sqrt(MIN2)*p1p2**2*p1p4**2  & 
          -  & 
           14*np2*p1p2**2*p1p4**2  & 
          - 6*np4*p1p2**2*p1p4**2  & 
          -  & 
           5*MIN2**1.5*p1p4**3  & 
          + 5*sqrt(MIN2)*MOU2*p1p4**3  & 
          -  & 
           9*MIN2*np2*p1p4**3  & 
          + 5*MOU2*np2*p1p4**3  & 
          +  & 
           18*sqrt(MIN2)*p1p2*p1p4**3  & 
          + 6*np2*p1p2*p1p4**3  & 
          -  & 
           3*CMPLX(aa,bb)*asym*MIN2**2*p2p4  & 
          + 10*MIN2**3.5*p2p4  & 
          +  & 
           3*CMPLX(aa,bb)*asym*MIN2*MOU2*p2p4  & 
          - 10*MIN2**2.5*MOU2*p2p4  & 
          +  & 
           18*MIN2**3*np2*p2p4  & 
          - 18*MIN2**2*MOU2*np2*p2p4  & 
          -  & 
           18*MIN2**3*np4*p2p4  & 
          + 18*MIN2**2*MOU2*np4*p2p4  & 
          +  & 
           4*CMPLX(aa,bb)*asym*MIN2*p1p2*p2p4  & 
          - 38*MIN2**2.5*p1p2*p2p4  & 
          +  & 
           18*MIN2**1.5*MOU2*p1p2*p2p4  & 
          -  & 
           28*MIN2**2*np2*p1p2*p2p4  & 
          +  & 
           46*MIN2**2*np4*p1p2*p2p4  & 
          -  & 
           18*MIN2*MOU2*np4*p1p2*p2p4  & 
          +  & 
           28*MIN2**1.5*p1p2**2*p2p4  & 
          -  & 
           28*MIN2*np4*p1p2**2*p2p4  & 
          -  & 
           7*CMPLX(aa,bb)*asym*MIN2*p1p4*p2p4  & 
          - 6*MIN2**2.5*p1p4*p2p4  & 
          +  & 
           6*MIN2**1.5*MOU2*p1p4*p2p4  & 
          -  & 
           22*MIN2**2*np2*p1p4*p2p4  & 
          +  & 
           18*MIN2*MOU2*np2*p1p4*p2p4  & 
          +  & 
           4*MIN2**2*np4*p1p4*p2p4  & 
          + 8*CMPLX(aa,bb)*asym*p1p2*p1p4*p2p4  & 
          -  & 
           9*MIN2**1.5*p1p2*p1p4*p2p4  & 
          +  & 
           9*sqrt(MIN2)*MOU2*p1p2*p1p4*p2p4  & 
          +  & 
           10*MIN2*np4*p1p2*p1p4*p2p4  & 
          +  & 
           42*sqrt(MIN2)*p1p2**2*p1p4*p2p4  & 
          -  & 
           14*np4*p1p2**2*p1p4*p2p4  & 
          +  & 
           47*MIN2**1.5*p1p4**2*p2p4  & 
          -  & 
           27*sqrt(MIN2)*MOU2*p1p4**2*p2p4  & 
          +  & 
           32*MIN2*np2*p1p4**2*p2p4  & 
          -  & 
           6*MIN2*np4*p1p4**2*p2p4  & 
          -  & 
           64*sqrt(MIN2)*p1p2*p1p4**2*p2p4  & 
          +  & 
           14*np2*p1p2*p1p4**2*p2p4  & 
          +  & 
           6*np4*p1p2*p1p4**2*p2p4  & 
          -  & 
           22*sqrt(MIN2)*p1p4**3*p2p4  & 
          - 6*np2*p1p4**3*p2p4  & 
          -  & 
           4*CMPLX(aa,bb)*asym*MIN2*p2p4**2  & 
          + 38*MIN2**2.5*p2p4**2  & 
          -  & 
           18*MIN2**1.5*MOU2*p2p4**2  & 
          +  & 
           28*MIN2**2*np2*p2p4**2  & 
          - 28*MIN2**2*np4*p2p4**2  & 
          -  & 
           56*MIN2**1.5*p1p2*p2p4**2  & 
          +  & 
           28*MIN2*np4*p1p2*p2p4**2  & 
          -  & 
           38*MIN2**1.5*p1p4*p2p4**2  & 
          +  & 
           18*sqrt(MIN2)*MOU2*p1p4*p2p4**2  & 
          -  & 
           28*MIN2*np2*p1p4*p2p4**2  & 
          +  & 
           14*sqrt(MIN2)*p1p2*p1p4*p2p4**2  & 
          +  & 
           50*sqrt(MIN2)*p1p4**2*p2p4**2  & 
          +  & 
           28*MIN2**1.5*p2p4**3  & 
          - 28*sqrt(MIN2)*p1p4*p2p4**3) & 
          *Ccache(4))/ & 
       (9.*p1p4**2*(p1p4  & 
          - p2p4)*Pi**2)  & 
          -  & 
      (4*EL**4*GF**2*( & 
          -16*CMPLX(aa,bb)*asym*MIN2**2.5*p1p4  & 
          +  & 
           16*CMPLX(aa,bb)*asym*MIN2**1.5*MOU2*p1p4  & 
          +  & 
           44*CMPLX(aa,bb)*asym*MIN2**1.5*p1p2*p1p4  & 
          -  & 
           40*MIN2**3*p1p2*p1p4  & 
          -  & 
           24*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p2*p1p4  & 
          +  & 
           40*MIN2**2*MOU2*p1p2*p1p4  & 
          -  & 
           72*MIN2**2.5*np2*p1p2*p1p4  & 
          +  & 
           72*MIN2**1.5*MOU2*np2*p1p2*p1p4  & 
          +  & 
           36*MIN2**2.5*np4*p1p2*p1p4  & 
          -  & 
           36*MIN2**1.5*MOU2*np4*p1p2*p1p4  & 
          -  & 
           32*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2**2*p1p4  & 
          +  & 
           152*MIN2**2*p1p2**2*p1p4  & 
          -  & 
           72*MIN2*MOU2*p1p2**2*p1p4  & 
          +  & 
           112*MIN2**1.5*np2*p1p2**2*p1p4  & 
          -  & 
           92*MIN2**1.5*np4*p1p2**2*p1p4  & 
          +  & 
           36*sqrt(MIN2)*MOU2*np4*p1p2**2*p1p4  & 
          -  & 
           112*MIN2*p1p2**3*p1p4  & 
          +  & 
           56*sqrt(MIN2)*np4*p1p2**3*p1p4  & 
          +  & 
           CMPLX(aa,bb)*asym*MIN2**1.5*p1p4**2  & 
          + 40*MIN2**3*p1p4**2  & 
          +  & 
           11*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p4**2  & 
          -  & 
           40*MIN2**2*MOU2*p1p4**2  & 
          +  & 
           72*MIN2**2.5*np2*p1p4**2  & 
          -  & 
           72*MIN2**1.5*MOU2*np2*p1p4**2  & 
          -  & 
           20*MIN2**2.5*np4*p1p4**2  & 
          +  & 
           20*MIN2**1.5*MOU2*np4*p1p4**2  & 
          +  & 
           10*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p1p4**2  & 
          -  & 
           128*MIN2**2*p1p2*p1p4**2  & 
          +  & 
           48*MIN2*MOU2*p1p2*p1p4**2  & 
          -  & 
           24*MIN2**1.5*np2*p1p2*p1p4**2  & 
          -  & 
           72*sqrt(MIN2)*MOU2*np2*p1p2*p1p4**2  & 
          +  & 
           36*MIN2**1.5*np4*p1p2*p1p4**2  & 
          -  & 
           12*sqrt(MIN2)*MOU2*np4*p1p2*p1p4**2  & 
          -  & 
           4*MIN2*p1p2**2*p1p4**2  & 
          +  & 
           36*MOU2*p1p2**2*p1p4**2  & 
          -  & 
           112*sqrt(MIN2)*np2*p1p2**2*p1p4**2  & 
          -  & 
           16*sqrt(MIN2)*np4*p1p2**2*p1p4**2  & 
          +  & 
           56*p1p2**3*p1p4**2  & 
          - 4*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4**3  & 
          -  & 
           40*MIN2**2*p1p4**3  & 
          + 40*MIN2*MOU2*p1p4**3  & 
          -  & 
           72*MIN2**1.5*np2*p1p4**3  & 
          +  & 
           56*sqrt(MIN2)*MOU2*np2*p1p4**3  & 
          -  & 
           8*MIN2**1.5*np4*p1p4**3  & 
          +  & 
           8*sqrt(MIN2)*MOU2*np4*p1p4**3  & 
          +  & 
           156*MIN2*p1p2*p1p4**3  & 
          - 44*MOU2*p1p2*p1p4**3  & 
          +  & 
           72*sqrt(MIN2)*np2*p1p2*p1p4**3  & 
          +  & 
           24*sqrt(MIN2)*np4*p1p2*p1p4**3  & 
          -  & 
           56*p1p2**2*p1p4**3  & 
          + 8*MIN2*p1p4**4  & 
          -  & 
           8*MOU2*p1p4**4  & 
          + 8*sqrt(MIN2)*np2*p1p4**4  & 
          -  & 
           32*p1p2*p1p4**4  & 
          - 12*CMPLX(aa,bb)*asym*MIN2**2.5*p2p4  & 
          +  & 
           40*MIN2**4*p2p4  & 
          + 12*CMPLX(aa,bb)*asym*MIN2**1.5*MOU2*p2p4  & 
          -  & 
           40*MIN2**3*MOU2*p2p4  & 
          + 72*MIN2**3.5*np2*p2p4  & 
          -  & 
           72*MIN2**2.5*MOU2*np2*p2p4  & 
          -  & 
           72*MIN2**3.5*np4*p2p4  & 
          +  & 
           72*MIN2**2.5*MOU2*np4*p2p4  & 
          +  & 
           16*CMPLX(aa,bb)*asym*MIN2**1.5*p1p2*p2p4  & 
          -  & 
           152*MIN2**3*p1p2*p2p4  & 
          +  & 
           72*MIN2**2*MOU2*p1p2*p2p4  & 
          -  & 
           112*MIN2**2.5*np2*p1p2*p2p4  & 
          +  & 
           184*MIN2**2.5*np4*p1p2*p2p4  & 
          -  & 
           72*MIN2**1.5*MOU2*np4*p1p2*p2p4  & 
          +  & 
           112*MIN2**2*p1p2**2*p2p4  & 
          -  & 
           112*MIN2**1.5*np4*p1p2**2*p2p4  & 
          -  & 
           25*CMPLX(aa,bb)*asym*MIN2**1.5*p1p4*p2p4  & 
          -  & 
           44*MIN2**3*p1p4*p2p4  & 
          -  & 
           3*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p4*p2p4  & 
          +  & 
           44*MIN2**2*MOU2*p1p4*p2p4  & 
          -  & 
           124*MIN2**2.5*np2*p1p4*p2p4  & 
          +  & 
           108*MIN2**1.5*MOU2*np2*p1p4*p2p4  & 
          +  & 
           42*MIN2**2.5*np4*p1p4*p2p4  & 
          -  & 
           26*MIN2**1.5*MOU2*np4*p1p4*p2p4  & 
          +  & 
           26*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p1p4*p2p4  & 
          +  & 
           40*MIN2**2*p1p2*p1p4*p2p4  & 
          +  & 
           56*MIN2**1.5*np2*p1p2*p1p4*p2p4  & 
          -  & 
           14*MIN2**1.5*np4*p1p2*p1p4*p2p4  & 
          +  & 
           18*sqrt(MIN2)*MOU2*np4*p1p2*p1p4*p2p4  & 
          +  & 
           112*MIN2*p1p2**2*p1p4*p2p4  & 
          -  & 
           28*sqrt(MIN2)*np4*p1p2**2*p1p4*p2p4  & 
          -  & 
           2*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4**2*p2p4  & 
          +  & 
           190*MIN2**2*p1p4**2*p2p4  & 
          -  & 
           110*MIN2*MOU2*p1p4**2*p2p4  & 
          +  & 
           154*MIN2**1.5*np2*p1p4**2*p2p4  & 
          -  & 
           18*sqrt(MIN2)*MOU2*np2*p1p4**2*p2p4  & 
          +  & 
           12*MIN2**1.5*np4*p1p4**2*p2p4  & 
          -  & 
           44*sqrt(MIN2)*MOU2*np4*p1p4**2*p2p4  & 
          -  & 
           236*MIN2*p1p2*p1p4**2*p2p4  & 
          +  & 
           84*sqrt(MIN2)*np2*p1p2*p1p4**2*p2p4  & 
          -  & 
           56*sqrt(MIN2)*np4*p1p2*p1p4**2*p2p4  & 
          -  & 
           56*p1p2**2*p1p4**2*p2p4  & 
          - 172*MIN2*p1p4**3*p2p4  & 
          +  & 
           44*MOU2*p1p4**3*p2p4  & 
          -  & 
           88*sqrt(MIN2)*np2*p1p4**3*p2p4  & 
          -  & 
           32*sqrt(MIN2)*np4*p1p4**3*p2p4  & 
          +  & 
           144*p1p2*p1p4**3*p2p4  & 
          + 32*p1p4**4*p2p4  & 
          -  & 
           16*CMPLX(aa,bb)*asym*MIN2**1.5*p2p4**2  & 
          + 152*MIN2**3*p2p4**2  & 
          -  & 
           72*MIN2**2*MOU2*p2p4**2  & 
          +  & 
           112*MIN2**2.5*np2*p2p4**2  & 
          -  & 
           112*MIN2**2.5*np4*p2p4**2  & 
          -  & 
           224*MIN2**2*p1p2*p2p4**2  & 
          +  & 
           112*MIN2**1.5*np4*p1p2*p2p4**2  & 
          +  & 
           6*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4*p2p4**2  & 
          -  & 
           210*MIN2**2*p1p4*p2p4**2  & 
          +  & 
           90*MIN2*MOU2*p1p4*p2p4**2  & 
          -  & 
           168*MIN2**1.5*np2*p1p4*p2p4**2  & 
          +  & 
           36*sqrt(MIN2)*MOU2*np4*p1p4*p2p4**2  & 
          +  & 
           140*MIN2*p1p2*p1p4*p2p4**2  & 
          +  & 
           28*sqrt(MIN2)*np4*p1p2*p1p4*p2p4**2  & 
          +  & 
           248*MIN2*p1p4**2*p2p4**2  & 
          -  & 
           36*MOU2*p1p4**2*p2p4**2  & 
          +  & 
           28*sqrt(MIN2)*np2*p1p4**2*p2p4**2  & 
          +  & 
           88*sqrt(MIN2)*np4*p1p4**2*p2p4**2  & 
          -  & 
           56*p1p2*p1p4**2*p2p4**2  & 
          - 88*p1p4**3*p2p4**2  & 
          +  & 
           112*MIN2**2*p2p4**3  & 
          - 140*MIN2*p1p4*p2p4**3  & 
          -  & 
           56*sqrt(MIN2)*np4*p1p4*p2p4**3  & 
          +  & 
           56*p1p4**2*p2p4**3)* & 
         Ccache(5))/ & 
       (9.*p1p4**2*(p1p4  & 
          - p2p4)*Pi**2)  & 
          -  & 
      (2*EL**4*GF**2*(36*CMPLX(aa,bb)*asym*MIN2**2.5*p1p4  & 
          -  & 
           36*CMPLX(aa,bb)*asym*MIN2**1.5*MOU2*p1p4  & 
          -  & 
           70*CMPLX(aa,bb)*asym*MIN2**1.5*p1p2*p1p4  & 
          -  & 
           12*MIN2**3*p1p2*p1p4  & 
          -  & 
           6*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p2*p1p4  & 
          +  & 
           12*MIN2**2*MOU2*p1p2*p1p4  & 
          -  & 
           36*MIN2**2.5*np2*p1p2*p1p4  & 
          +  & 
           36*MIN2**1.5*MOU2*np2*p1p2*p1p4  & 
          +  & 
           26*MIN2**2.5*np4*p1p2*p1p4  & 
          -  & 
           26*MIN2**1.5*MOU2*np4*p1p2*p1p4  & 
          -  & 
           8*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2**2*p1p4  & 
          +  & 
           60*MIN2**2*p1p2**2*p1p4  & 
          -  & 
           36*MIN2*MOU2*p1p2**2*p1p4  & 
          +  & 
           48*MIN2**1.5*np2*p1p2**2*p1p4  & 
          -  & 
           58*MIN2**1.5*np4*p1p2**2*p1p4  & 
          +  & 
           18*sqrt(MIN2)*MOU2*np4*p1p2**2*p1p4  & 
          -  & 
           48*MIN2*p1p2**3*p1p4  & 
          +  & 
           32*sqrt(MIN2)*np4*p1p2**3*p1p4  & 
          +  & 
           31*CMPLX(aa,bb)*asym*MIN2**1.5*p1p4**2  & 
          + 12*MIN2**3*p1p4**2  & 
          -  & 
           27*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p4**2  & 
          -  & 
           12*MIN2**2*MOU2*p1p4**2  & 
          +  & 
           36*MIN2**2.5*np2*p1p4**2  & 
          -  & 
           36*MIN2**1.5*MOU2*np2*p1p4**2  & 
          -  & 
           8*MIN2**2.5*np4*p1p4**2  & 
          +  & 
           26*MIN2**1.5*MOU2*np4*p1p4**2  & 
          -  & 
           18*sqrt(MIN2)*MOU2**2*np4*p1p4**2  & 
          -  & 
           15*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p1p4**2  & 
          +  & 
           14*MIN2**2*p1p2*p1p4**2  & 
          -  & 
           38*MIN2*MOU2*p1p2*p1p4**2  & 
          +  & 
           126*MIN2**1.5*np2*p1p2*p1p4**2  & 
          -  & 
           162*sqrt(MIN2)*MOU2*np2*p1p2*p1p4**2  & 
          +  & 
           11*MIN2**1.5*np4*p1p2*p1p4**2  & 
          -  & 
           43*sqrt(MIN2)*MOU2*np4*p1p2*p1p4**2  & 
          -  & 
           268*MIN2*p1p2**2*p1p4**2  & 
          +  & 
           144*MOU2*p1p2**2*p1p4**2  & 
          -  & 
           256*sqrt(MIN2)*np2*p1p2**2*p1p4**2  & 
          -  & 
           42*sqrt(MIN2)*np4*p1p2**2*p1p4**2  & 
          +  & 
           224*p1p2**3*p1p4**2  & 
          -  & 
           43*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4**3  & 
          -  & 
           100*MIN2**2*p1p4**3  & 
          + 82*MIN2*MOU2*p1p4**3  & 
          +  & 
           18*MOU2**2*p1p4**3  & 
          - 171*MIN2**1.5*np2*p1p4**3  & 
          +  & 
           159*sqrt(MIN2)*MOU2*np2*p1p4**3  & 
          -  & 
           8*MIN2**1.5*np4*p1p4**3  & 
          +  & 
           8*sqrt(MIN2)*MOU2*np4*p1p4**3  & 
          +  & 
           376*MIN2*p1p2*p1p4**3  & 
          - 116*MOU2*p1p2*p1p4**3  & 
          +  & 
           218*sqrt(MIN2)*np2*p1p2*p1p4**3  & 
          +  & 
           48*sqrt(MIN2)*np4*p1p2*p1p4**3  & 
          -  & 
           176*p1p2**2*p1p4**3  & 
          + 8*MIN2*p1p4**4  & 
          -  & 
           8*MOU2*p1p4**4  & 
          + 32*sqrt(MIN2)*np2*p1p4**4  & 
          -  & 
           80*p1p2*p1p4**4  & 
          - 48*CMPLX(aa,bb)*asym*MIN2**2.5*p2p4  & 
          +  & 
           12*MIN2**4*p2p4  & 
          + 48*CMPLX(aa,bb)*asym*MIN2**1.5*MOU2*p2p4  & 
          -  & 
           12*MIN2**3*MOU2*p2p4  & 
          + 36*MIN2**3.5*np2*p2p4  & 
          -  & 
           36*MIN2**2.5*MOU2*np2*p2p4  & 
          -  & 
           36*MIN2**3.5*np4*p2p4  & 
          +  & 
           36*MIN2**2.5*MOU2*np4*p2p4  & 
          +  & 
           80*CMPLX(aa,bb)*asym*MIN2**1.5*p1p2*p2p4  & 
          -  & 
           60*MIN2**3*p1p2*p2p4  & 
          +  & 
           36*MIN2**2*MOU2*p1p2*p2p4  & 
          -  & 
           48*MIN2**2.5*np2*p1p2*p2p4  & 
          +  & 
           84*MIN2**2.5*np4*p1p2*p2p4  & 
          -  & 
           36*MIN2**1.5*MOU2*np4*p1p2*p2p4  & 
          +  & 
           48*MIN2**2*p1p2**2*p2p4  & 
          -  & 
           48*MIN2**1.5*np4*p1p2**2*p2p4  & 
          +  & 
           77*CMPLX(aa,bb)*asym*MIN2**1.5*p1p4*p2p4  & 
          -  & 
           80*MIN2**3*p1p4*p2p4  & 
          -  & 
           15*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p4*p2p4  & 
          +  & 
           80*MIN2**2*MOU2*p1p4*p2p4  & 
          -  & 
           192*MIN2**2.5*np2*p1p4*p2p4  & 
          +  & 
           180*MIN2**1.5*MOU2*np2*p1p4*p2p4  & 
          +  & 
           17*MIN2**2.5*np4*p1p4*p2p4  & 
          -  & 
           5*MIN2**1.5*MOU2*np4*p1p4*p2p4  & 
          -  & 
           25*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p1p4*p2p4  & 
          +  & 
           286*MIN2**2*p1p2*p1p4*p2p4  & 
          -  & 
           126*MIN2*MOU2*p1p2*p1p4*p2p4  & 
          +  & 
           232*MIN2**1.5*np2*p1p2*p1p4*p2p4  & 
          -  & 
           49*MIN2**1.5*np4*p1p2*p1p4*p2p4  & 
          +  & 
           105*sqrt(MIN2)*MOU2*np4*p1p2*p1p4*p2p4  & 
          -  & 
           152*MIN2*p1p2**2*p1p4*p2p4  & 
          +  & 
           122*sqrt(MIN2)*np4*p1p2**2*p1p4*p2p4  & 
          +  & 
           50*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4**2*p2p4  & 
          +  & 
           172*MIN2**2*p1p4**2*p2p4  & 
          -  & 
           148*MIN2*MOU2*p1p4**2*p2p4  & 
          +  & 
           167*MIN2**1.5*np2*p1p4**2*p2p4  & 
          -  & 
           69*sqrt(MIN2)*MOU2*np2*p1p4**2*p2p4  & 
          +  & 
           116*MIN2**1.5*np4*p1p4**2*p2p4  & 
          -  & 
           104*sqrt(MIN2)*MOU2*np4*p1p4**2*p2p4  & 
          -  & 
           188*MIN2*p1p2*p1p4**2*p2p4  & 
          -  & 
           36*MOU2*p1p2*p1p4**2*p2p4  & 
          +  & 
           150*sqrt(MIN2)*np2*p1p2*p1p4**2*p2p4  & 
          -  & 
           162*sqrt(MIN2)*np4*p1p2*p1p4**2*p2p4  & 
          -  & 
           272*p1p2**2*p1p4**2*p2p4  & 
          -  & 
           364*MIN2*p1p4**3*p2p4  & 
          + 104*MOU2*p1p4**3*p2p4  & 
          -  & 
           270*sqrt(MIN2)*np2*p1p4**3*p2p4  & 
          -  & 
           80*sqrt(MIN2)*np4*p1p4**3*p2p4  & 
          +  & 
           432*p1p2*p1p4**3*p2p4  & 
          + 80*p1p4**4*p2p4  & 
          -  & 
           80*CMPLX(aa,bb)*asym*MIN2**1.5*p2p4**2  & 
          + 60*MIN2**3*p2p4**2  & 
          -  & 
           36*MIN2**2*MOU2*p2p4**2  & 
          +  & 
           48*MIN2**2.5*np2*p2p4**2  & 
          +  & 
           39*MIN2**2.5*np4*p2p4**2  & 
          -  & 
           87*MIN2**1.5*MOU2*np4*p2p4**2  & 
          -  & 
           96*MIN2**2*p1p2*p2p4**2  & 
          -  & 
           90*MIN2**1.5*np4*p1p2*p2p4**2  & 
          +  & 
           30*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4*p2p4**2  & 
          -  & 
           364*MIN2**2*p1p4*p2p4**2  & 
          +  & 
           198*MIN2*MOU2*p1p4*p2p4**2  & 
          -  & 
           280*MIN2**1.5*np2*p1p4*p2p4**2  & 
          -  & 
           154*MIN2**1.5*np4*p1p4*p2p4**2  & 
          +  & 
           108*sqrt(MIN2)*MOU2*np4*p1p4*p2p4**2  & 
          +  & 
           504*MIN2*p1p2*p1p4*p2p4**2  & 
          +  & 
           22*sqrt(MIN2)*np4*p1p2*p1p4*p2p4**2  & 
          +  & 
           436*MIN2*p1p4**2*p2p4**2  & 
          -  & 
           108*MOU2*p1p4**2*p2p4**2  & 
          +  & 
           106*sqrt(MIN2)*np2*p1p4**2*p2p4**2  & 
          +  & 
           256*sqrt(MIN2)*np4*p1p4**2*p2p4**2  & 
          -  & 
           128*p1p2*p1p4**2*p2p4**2  & 
          - 256*p1p4**3*p2p4**2  & 
          +  & 
           48*MIN2**2*p2p4**3  & 
          + 138*MIN2**1.5*np4*p2p4**3  & 
          -  & 
           304*MIN2*p1p4*p2p4**3  & 
          -  & 
           176*sqrt(MIN2)*np4*p1p4*p2p4**3  & 
          +  & 
           176*p1p4**2*p2p4**3)* & 
         Ccache(6))/ & 
       (9.*p1p4**2*(p1p4  & 
          - p2p4)*Pi**2)  & 
          -  & 
      (4*EL**4*GF**2*( & 
          -8*CMPLX(aa,bb)*asym*MIN2**2.5*p1p4  & 
          +  & 
           8*CMPLX(aa,bb)*asym*MIN2**1.5*MOU2*p1p4  & 
          +  & 
           22*CMPLX(aa,bb)*asym*MIN2**1.5*p1p2*p1p4  & 
          -  & 
           20*MIN2**3*p1p2*p1p4  & 
          -  & 
           12*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p2*p1p4  & 
          +  & 
           20*MIN2**2*MOU2*p1p2*p1p4  & 
          -  & 
           36*MIN2**2.5*np2*p1p2*p1p4  & 
          +  & 
           36*MIN2**1.5*MOU2*np2*p1p2*p1p4  & 
          +  & 
           18*MIN2**2.5*np4*p1p2*p1p4  & 
          -  & 
           18*MIN2**1.5*MOU2*np4*p1p2*p1p4  & 
          -  & 
           16*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2**2*p1p4  & 
          +  & 
           76*MIN2**2*p1p2**2*p1p4  & 
          -  & 
           36*MIN2*MOU2*p1p2**2*p1p4  & 
          +  & 
           56*MIN2**1.5*np2*p1p2**2*p1p4  & 
          -  & 
           46*MIN2**1.5*np4*p1p2**2*p1p4  & 
          +  & 
           18*sqrt(MIN2)*MOU2*np4*p1p2**2*p1p4  & 
          -  & 
           56*MIN2*p1p2**3*p1p4  & 
          +  & 
           28*sqrt(MIN2)*np4*p1p2**3*p1p4  & 
          -  & 
           CMPLX(aa,bb)*asym*MIN2**1.5*p1p4**2  & 
          + 20*MIN2**3*p1p4**2  & 
          +  & 
           7*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p4**2  & 
          -  & 
           20*MIN2**2*MOU2*p1p4**2  & 
          +  & 
           36*MIN2**2.5*np2*p1p4**2  & 
          -  & 
           36*MIN2**1.5*MOU2*np2*p1p4**2  & 
          -  & 
           10*MIN2**2.5*np4*p1p4**2  & 
          +  & 
           10*MIN2**1.5*MOU2*np4*p1p4**2  & 
          +  & 
           10*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p1p4**2  & 
          -  & 
           54*MIN2**2*p1p2*p1p4**2  & 
          +  & 
           14*MIN2*MOU2*p1p2*p1p4**2  & 
          +  & 
           6*MIN2**1.5*np2*p1p2*p1p4**2  & 
          -  & 
           54*sqrt(MIN2)*MOU2*np2*p1p2*p1p4**2  & 
          +  & 
           14*MIN2**1.5*np4*p1p2*p1p4**2  & 
          -  & 
           2*sqrt(MIN2)*MOU2*np4*p1p2*p1p4**2  & 
          -  & 
           40*MIN2*p1p2**2*p1p4**2  & 
          +  & 
           36*MOU2*p1p2**2*p1p4**2  & 
          -  & 
           84*sqrt(MIN2)*np2*p1p2**2*p1p4**2  & 
          -  & 
           4*sqrt(MIN2)*np4*p1p2**2*p1p4**2  & 
          +  & 
           56*p1p2**3*p1p4**2  & 
          - 4*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4**3  & 
          -  & 
           30*MIN2**2*p1p4**3  & 
          + 30*MIN2*MOU2*p1p4**3  & 
          -  & 
           54*MIN2**1.5*np2*p1p4**3  & 
          +  & 
           46*sqrt(MIN2)*MOU2*np2*p1p4**3  & 
          -  & 
           8*MIN2**1.5*np4*p1p4**3  & 
          +  & 
           8*sqrt(MIN2)*MOU2*np4*p1p4**3  & 
          +  & 
           120*MIN2*p1p2*p1p4**3  & 
          - 44*MOU2*p1p2*p1p4**3  & 
          +  & 
           60*sqrt(MIN2)*np2*p1p2*p1p4**3  & 
          +  & 
           24*sqrt(MIN2)*np4*p1p2*p1p4**3  & 
          -  & 
           56*p1p2**2*p1p4**3  & 
          + 8*MIN2*p1p4**4  & 
          -  & 
           8*MOU2*p1p4**4  & 
          + 8*sqrt(MIN2)*np2*p1p4**4  & 
          -  & 
           32*p1p2*p1p4**4  & 
          - 6*CMPLX(aa,bb)*asym*MIN2**2.5*p2p4  & 
          +  & 
           20*MIN2**4*p2p4  & 
          + 6*CMPLX(aa,bb)*asym*MIN2**1.5*MOU2*p2p4  & 
          -  & 
           20*MIN2**3*MOU2*p2p4  & 
          + 36*MIN2**3.5*np2*p2p4  & 
          -  & 
           36*MIN2**2.5*MOU2*np2*p2p4  & 
          -  & 
           36*MIN2**3.5*np4*p2p4  & 
          +  & 
           36*MIN2**2.5*MOU2*np4*p2p4  & 
          +  & 
           8*CMPLX(aa,bb)*asym*MIN2**1.5*p1p2*p2p4  & 
          -  & 
           76*MIN2**3*p1p2*p2p4  & 
          +  & 
           36*MIN2**2*MOU2*p1p2*p2p4  & 
          -  & 
           56*MIN2**2.5*np2*p1p2*p2p4  & 
          +  & 
           92*MIN2**2.5*np4*p1p2*p2p4  & 
          -  & 
           36*MIN2**1.5*MOU2*np4*p1p2*p2p4  & 
          +  & 
           56*MIN2**2*p1p2**2*p2p4  & 
          -  & 
           56*MIN2**1.5*np4*p1p2**2*p2p4  & 
          -  & 
           11*CMPLX(aa,bb)*asym*MIN2**1.5*p1p4*p2p4  & 
          -  & 
           32*MIN2**3*p1p4*p2p4  & 
          -  & 
           3*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p4*p2p4  & 
          +  & 
           32*MIN2**2*MOU2*p1p4*p2p4  & 
          -  & 
           80*MIN2**2.5*np2*p1p4*p2p4  & 
          +  & 
           72*MIN2**1.5*MOU2*np2*p1p4*p2p4  & 
          +  & 
           34*MIN2**2.5*np4*p1p4*p2p4  & 
          -  & 
           26*MIN2**1.5*MOU2*np4*p1p4*p2p4  & 
          +  & 
           10*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p1p4*p2p4  & 
          +  & 
           58*MIN2**2*p1p2*p1p4*p2p4  & 
          -  & 
           18*MIN2*MOU2*p1p2*p1p4*p2p4  & 
          +  & 
           56*MIN2**1.5*np2*p1p2*p1p4*p2p4  & 
          -  & 
           34*MIN2**1.5*np4*p1p2*p1p4*p2p4  & 
          +  & 
           18*sqrt(MIN2)*MOU2*np4*p1p2*p1p4*p2p4  & 
          +  & 
           28*MIN2*p1p2**2*p1p4*p2p4  & 
          -  & 
           2*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4**2*p2p4  & 
          +  & 
           96*MIN2**2*p1p4**2*p2p4  & 
          -  & 
           56*MIN2*MOU2*p1p4**2*p2p4  & 
          +  & 
           90*MIN2**1.5*np2*p1p4**2*p2p4  & 
          -  & 
           18*sqrt(MIN2)*MOU2*np2*p1p4**2*p2p4  & 
          +  & 
           24*MIN2**1.5*np4*p1p4**2*p2p4  & 
          -  & 
           44*sqrt(MIN2)*MOU2*np4*p1p4**2*p2p4  & 
          -  & 
           108*MIN2*p1p2*p1p4**2*p2p4  & 
          +  & 
           56*sqrt(MIN2)*np2*p1p2*p1p4**2*p2p4  & 
          -  & 
           68*sqrt(MIN2)*np4*p1p2*p1p4**2*p2p4  & 
          -  & 
           56*p1p2**2*p1p4**2*p2p4  & 
          - 128*MIN2*p1p4**3*p2p4  & 
          +  & 
           44*MOU2*p1p4**3*p2p4  & 
          -  & 
           76*sqrt(MIN2)*np2*p1p4**3*p2p4  & 
          -  & 
           32*sqrt(MIN2)*np4*p1p4**3*p2p4  & 
          +  & 
           144*p1p2*p1p4**3*p2p4  & 
          + 32*p1p4**4*p2p4  & 
          -  & 
           8*CMPLX(aa,bb)*asym*MIN2**1.5*p2p4**2  & 
          + 76*MIN2**3*p2p4**2  & 
          -  & 
           36*MIN2**2*MOU2*p2p4**2  & 
          +  & 
           56*MIN2**2.5*np2*p2p4**2  & 
          -  & 
           56*MIN2**2.5*np4*p2p4**2  & 
          -  & 
           112*MIN2**2*p1p2*p2p4**2  & 
          +  & 
           56*MIN2**1.5*np4*p1p2*p2p4**2  & 
          +  & 
           6*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4*p2p4**2  & 
          -  & 
           134*MIN2**2*p1p4*p2p4**2  & 
          +  & 
           54*MIN2*MOU2*p1p4*p2p4**2  & 
          -  & 
           112*MIN2**1.5*np2*p1p4*p2p4**2  & 
          +  & 
           36*sqrt(MIN2)*MOU2*np4*p1p4*p2p4**2  & 
          +  & 
           112*MIN2*p1p2*p1p4*p2p4**2  & 
          +  & 
           28*sqrt(MIN2)*np4*p1p2*p1p4*p2p4**2  & 
          +  & 
           148*MIN2*p1p4**2*p2p4**2  & 
          -  & 
           36*MOU2*p1p4**2*p2p4**2  & 
          +  & 
           28*sqrt(MIN2)*np2*p1p4**2*p2p4**2  & 
          +  & 
           88*sqrt(MIN2)*np4*p1p4**2*p2p4**2  & 
          -  & 
           56*p1p2*p1p4**2*p2p4**2  & 
          - 88*p1p4**3*p2p4**2  & 
          +  & 
           56*MIN2**2*p2p4**3  & 
          - 84*MIN2*p1p4*p2p4**3  & 
          -  & 
           56*sqrt(MIN2)*np4*p1p4*p2p4**3  & 
          +  & 
           56*p1p4**2*p2p4**3)* & 
         Ccache(7))/ & 
       (9.*p1p4**2*(p1p4  & 
          - p2p4)*Pi**2))
!
!
      return
!
      end function

                          !!!!!!!!!!!!!!!!!!!!!!
                            END MODULE rad_T3TRIBC
                          !!!!!!!!!!!!!!!!!!!!!!
