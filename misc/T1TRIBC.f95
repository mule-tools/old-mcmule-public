
                          !!!!!!!!!!!!!!!!!!!!!!
                              MODULE rad_T1TRIBC
                          !!!!!!!!!!!!!!!!!!!!!!
  contains
      function T1TRIBC(EL,GF,MOU2,MIN2,p1p2,p1p4,p2p4,asym,np2,np3, & 
         np4,mu2)
!
      use global_def, only: pi, prec
      implicit none
!

    complex(kind=prec) A0, B0i
    complex(kind=prec) B0
    complex(kind=prec) DB0
    complex(kind=prec) C0, C0i
    complex(kind=prec) D0, D0i

    integer, parameter :: bb0  = 1
    integer, parameter :: bb1  = 4
    integer, parameter :: cc0  = 1
    integer, parameter :: cc1  = 4
    integer, parameter :: cc2  = 7
    integer, parameter :: cc00 = 10
    integer, parameter :: cc11 = 13
    integer, parameter :: cc12 = 16
    integer, parameter :: cc22 = 19
    integer, parameter :: dd0  = 1
    integer, parameter :: dd1  = 4
    integer, parameter :: dd2  = 7
    integer, parameter :: dd3  = 10
    integer, parameter :: dd00 = 13
    
    

    external A0, B0i
    external B0
    external DB0
    external C0, C0i
    external D0, D0i
    
    complex(kind=prec) :: Ccache(7)
    complex(kind=prec) :: Bcache(1)

!
      real(kind=prec) T1TRIBC,EL,GF,MOU2,MIN2, & 
         p1p2,p1p4,p2p4,asym, & 
         np2,np3,np4,mu2, & 
         aa,bb
!
      aa=0._prec
      bb=  & 
          -1._prec
!

!    
      call setlambda(  & 
          -1._prec)
      call setdelta(0._prec)
      call setmudim(mu2)
      call setminmass(0._prec)
!     
    Bcache(1) = B0i(bb0,MOU2,0._prec,MOU2)
    Ccache(1) = C0i(cc0,MOU2,-MIN2+MOU2+2*p1p2-2*p2p4,MIN2-2*p1p4,0._prec,MOU2,MIN2)
    Ccache(2) = C0i(cc00,MOU2,-MIN2+MOU2+2*p1p2-2*p2p4,MIN2-2*p1p4,0._prec,MOU2,MIN2)
    Ccache(3) = C0i(cc1,MOU2,-MIN2+MOU2+2*p1p2-2*p2p4,MIN2-2*p1p4,0._prec,MOU2,MIN2)
    Ccache(4) = C0i(cc11,MOU2,-MIN2+MOU2+2*p1p2-2*p2p4,MIN2-2*p1p4,0._prec,MOU2,MIN2)
    Ccache(5) = C0i(cc12,MOU2,-MIN2+MOU2+2*p1p2-2*p2p4,MIN2-2*p1p4,0._prec,MOU2,MIN2)
    Ccache(6) = C0i(cc2,MOU2,-MIN2+MOU2+2*p1p2-2*p2p4,MIN2-2*p1p4,0._prec,MOU2,MIN2)
    Ccache(7) = C0i(cc22,MOU2,-MIN2+MOU2+2*p1p2-2*p2p4,MIN2-2*p1p4,0._prec,MOU2,MIN2)

!
      T1TRIBC=real((2*EL**4*GF**2*(  & 
          -2*CMPLX(aa,bb)*asym*MIN2**1.5*p1p4   & 
          +  & 
           2*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p4  & 
          +  & 
           10*MIN2**2.5*np4*p1p4   & 
          -  & 
           20*MIN2**1.5*MOU2*np4*p1p4   & 
          +  & 
           10*sqrt(MIN2)*MOU2**2*np4*p1p4   & 
          -  & 
           CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p1p4   & 
          - 4*MIN2**2*p1p2*p1p4   & 
          +  & 
           4*MIN2*MOU2*p1p2*p1p4   & 
          -  & 
           20*MIN2**1.5*np2*p1p2*p1p4   & 
          +  & 
           20*sqrt(MIN2)*MOU2*np2*p1p2*p1p4   & 
          -  & 
           22*MIN2**1.5*np4*p1p2*p1p4   & 
          +  & 
           22*sqrt(MIN2)*MOU2*np4*p1p2*p1p4   & 
          +  & 
           28*MIN2*p1p2**2*p1p4   & 
          - 20*MOU2*p1p2**2*p1p4   & 
          +  & 
           24*sqrt(MIN2)*np2*p1p2**2*p1p4   & 
          +  & 
           12*sqrt(MIN2)*np4*p1p2**2*p1p4   & 
          -  & 
           24*p1p2**3*p1p4   & 
          + 5*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4**2   & 
          -  & 
           6*MIN2**2*p1p4**2   & 
          + 16*MIN2*MOU2*p1p4**2   & 
          -  & 
           10*MOU2**2*p1p4**2   & 
          + 18*MIN2**1.5*np2*p1p4**2   & 
          -  & 
           18*sqrt(MIN2)*MOU2*np2*p1p4**2   & 
          -  & 
           20*MIN2**1.5*np4*p1p4**2   & 
          +  & 
           20*sqrt(MIN2)*MOU2*np4*p1p4**2   & 
          -  & 
           4*MIN2*p1p2*p1p4**2   & 
          - 4*MOU2*p1p2*p1p4**2   & 
          -  & 
           12*sqrt(MIN2)*np2*p1p2*p1p4**2   & 
          +  & 
           32*sqrt(MIN2)*np4*p1p2*p1p4**2   & 
          +  & 
           20*MIN2*p1p4**3   & 
          - 20*MOU2*p1p4**3   & 
          -  & 
           8*sqrt(MIN2)*np2*p1p4**3   & 
          - 24*p1p2*p1p4**3   & 
          -  & 
           8*CMPLX(aa,bb)*asym*MIN2**1.5*p2p4   & 
          + 4*MIN2**3*p2p4   & 
          +  & 
           8*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p2p4   & 
          -  & 
           4*MIN2**2*MOU2*p2p4   & 
          + 20*MIN2**2.5*np2*p2p4   & 
          -  & 
           20*MIN2**1.5*MOU2*np2*p2p4   & 
          -  & 
           20*MIN2**2.5*np4*p2p4   & 
          +  & 
           20*MIN2**1.5*MOU2*np4*p2p4   & 
          +  & 
           13*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p2p4   & 
          -  & 
           28*MIN2**2*p1p2*p2p4   & 
          + 20*MIN2*MOU2*p1p2*p2p4   & 
          -  & 
           24*MIN2**1.5*np2*p1p2*p2p4   & 
          +  & 
           44*MIN2**1.5*np4*p1p2*p2p4   & 
          -  & 
           20*sqrt(MIN2)*MOU2*np4*p1p2*p2p4   & 
          +  & 
           24*MIN2*p1p2**2*p2p4   & 
          -  & 
           24*sqrt(MIN2)*np4*p1p2**2*p2p4   & 
          +  & 
           7*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4*p2p4   & 
          +  & 
           6*MIN2**2*p1p4*p2p4   & 
          - 6*MIN2*MOU2*p1p4*p2p4   & 
          -  & 
           8*MIN2**1.5*np2*p1p4*p2p4   & 
          +  & 
           44*MIN2**1.5*np4*p1p4*p2p4   & 
          -  & 
           36*sqrt(MIN2)*MOU2*np4*p1p4*p2p4   & 
          -  & 
           24*MIN2*p1p2*p1p4*p2p4   & 
          + 20*MOU2*p1p2*p1p4*p2p4   & 
          -  & 
           24*sqrt(MIN2)*np2*p1p2*p1p4*p2p4   & 
          -  & 
           54*sqrt(MIN2)*np4*p1p2*p1p4*p2p4   & 
          +  & 
           48*p1p2**2*p1p4*p2p4   & 
          - 28*MIN2*p1p4**2*p2p4   & 
          +  & 
           36*MOU2*p1p4**2*p2p4   & 
          +  & 
           6*sqrt(MIN2)*np2*p1p4**2*p2p4   & 
          -  & 
           24*sqrt(MIN2)*np4*p1p4**2*p2p4   & 
          +  & 
           48*p1p2*p1p4**2*p2p4   & 
          + 24*p1p4**3*p2p4   & 
          -  & 
           16*CMPLX(aa,bb)*asym*sqrt(MIN2)*p2p4**2   & 
          + 28*MIN2**2*p2p4**2   & 
          -  & 
           20*MIN2*MOU2*p2p4**2   & 
          + 24*MIN2**1.5*np2*p2p4**2   & 
          -  & 
           50*MIN2**1.5*np4*p2p4**2   & 
          +  & 
           26*sqrt(MIN2)*MOU2*np4*p2p4**2   & 
          -  & 
           48*MIN2*p1p2*p2p4**2   & 
          +  & 
           48*sqrt(MIN2)*np4*p1p2*p2p4**2   & 
          +  & 
           8*MIN2*p1p4*p2p4**2   & 
          - 26*MOU2*p1p4*p2p4**2   & 
          +  & 
           48*sqrt(MIN2)*np4*p1p4*p2p4**2   & 
          -  & 
           48*p1p2*p1p4*p2p4**2   & 
          - 48*p1p4**2*p2p4**2   & 
          +  & 
           24*MIN2*p2p4**3   & 
          - 24*sqrt(MIN2)*np4*p2p4**3   & 
          +  & 
           24*p1p4*p2p4**3)*Bcache(1))/ & 
       (9.*p1p4**2*(p1p4   & 
          - p2p4)*Pi**2)   & 
          -  & 
      (2*EL**4*GF**2*(9*CMPLX(aa,bb)*asym*MIN2**2.5*p1p4   & 
          -  & 
           9*CMPLX(aa,bb)*asym*MIN2**1.5*MOU2*p1p4   & 
          -  & 
           18*MIN2**3.5*np4*p1p4   & 
          +  & 
           36*MIN2**2.5*MOU2*np4*p1p4   & 
          -  & 
           18*MIN2**1.5*MOU2**2*np4*p1p4   & 
          -  & 
           29*CMPLX(aa,bb)*asym*MIN2**1.5*p1p2*p1p4   & 
          +  & 
           28*MIN2**3*p1p2*p1p4   & 
          +  & 
           12*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p2*p1p4   & 
          -  & 
           28*MIN2**2*MOU2*p1p2*p1p4   & 
          +  & 
           36*MIN2**2.5*np2*p1p2*p1p4   & 
          -  & 
           36*MIN2**1.5*MOU2*np2*p1p2*p1p4   & 
          +  & 
           68*MIN2**2.5*np4*p1p2*p1p4   & 
          -  & 
           86*MIN2**1.5*MOU2*np4*p1p2*p1p4   & 
          +  & 
           18*sqrt(MIN2)*MOU2**2*np4*p1p2*p1p4   & 
          +  & 
           20*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2**2*p1p4   & 
          -  & 
           120*MIN2**2*p1p2**2*p1p4   & 
          +  & 
           64*MIN2*MOU2*p1p2**2*p1p4   & 
          -  & 
           100*MIN2**1.5*np2*p1p2**2*p1p4   & 
          +  & 
           36*sqrt(MIN2)*MOU2*np2*p1p2**2*p1p4   & 
          -  & 
           82*MIN2**1.5*np4*p1p2**2*p1p4   & 
          +  & 
           50*sqrt(MIN2)*MOU2*np4*p1p2**2*p1p4   & 
          +  & 
           156*MIN2*p1p2**3*p1p4   & 
          - 36*MOU2*p1p2**3*p1p4   & 
          +  & 
           64*sqrt(MIN2)*np2*p1p2**3*p1p4   & 
          +  & 
           32*sqrt(MIN2)*np4*p1p2**3*p1p4   & 
          -  & 
           64*p1p2**4*p1p4   & 
          - 5*CMPLX(aa,bb)*asym*MIN2**1.5*p1p4**2   & 
          -  & 
           14*MIN2**3*p1p4**2   & 
          +  & 
           4*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p4**2   & 
          +  & 
           14*MIN2*MOU2**2*p1p4**2   & 
          -  & 
           18*MIN2**2.5*np2*p1p4**2   & 
          +  & 
           18*sqrt(MIN2)*MOU2**2*np2*p1p4**2   & 
          +  & 
           30*MIN2**2.5*np4*p1p4**2   & 
          -  & 
           40*MIN2**1.5*MOU2*np4*p1p4**2   & 
          +  & 
           10*sqrt(MIN2)*MOU2**2*np4*p1p4**2   & 
          +  & 
           34*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p1p4**2   & 
          +  & 
           26*MIN2**2*p1p2*p1p4**2   & 
          +  & 
           66*MIN2*MOU2*p1p2*p1p4**2   & 
          -  & 
           36*MOU2**2*p1p2*p1p4**2   & 
          +  & 
           54*MIN2**1.5*np2*p1p2*p1p4**2   & 
          +  & 
           6*sqrt(MIN2)*MOU2*np2*p1p2*p1p4**2   & 
          -  & 
           102*MIN2**1.5*np4*p1p2*p1p4**2   & 
          +  & 
           66*sqrt(MIN2)*MOU2*np4*p1p2*p1p4**2   & 
          +  & 
           4*MIN2*p1p2**2*p1p4**2   & 
          -  & 
           56*MOU2*p1p2**2*p1p4**2   & 
          -  & 
           16*sqrt(MIN2)*np2*p1p2**2*p1p4**2   & 
          +  & 
           72*sqrt(MIN2)*np4*p1p2**2*p1p4**2   & 
          -  & 
           16*p1p2**3*p1p4**2   & 
          - 22*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4**3   & 
          -  & 
           38*MIN2**2*p1p4**3   & 
          + 48*MIN2*MOU2*p1p4**3   & 
          -  & 
           10*MOU2**2*p1p4**3   & 
          - 50*MIN2**1.5*np2*p1p4**3   & 
          +  & 
           54*sqrt(MIN2)*MOU2*np2*p1p4**3   & 
          +  & 
           8*MIN2**1.5*np4*p1p4**3   & 
          -  & 
           8*sqrt(MIN2)*MOU2*np4*p1p4**3   & 
          +  & 
           168*MIN2*p1p2*p1p4**3   & 
          - 120*MOU2*p1p2*p1p4**3   & 
          +  & 
           56*sqrt(MIN2)*np2*p1p2*p1p4**3   & 
          -  & 
           128*p1p2**2*p1p4**3   & 
          - 8*MIN2*p1p4**4   & 
          +  & 
           8*MOU2*p1p4**4   & 
          + 16*sqrt(MIN2)*np2*p1p4**4   & 
          -  & 
           16*p1p2*p1p4**4   & 
          - 15*CMPLX(aa,bb)*asym*MIN2**2.5*p2p4   & 
          -  & 
           28*MIN2**4*p2p4   & 
          + 15*CMPLX(aa,bb)*asym*MIN2**1.5*MOU2*p2p4   & 
          +  & 
           28*MIN2**3*MOU2*p2p4   & 
          - 36*MIN2**3.5*np2*p2p4   & 
          +  & 
           36*MIN2**2.5*MOU2*np2*p2p4   & 
          +  & 
           36*MIN2**3.5*np4*p2p4   & 
          -  & 
           36*MIN2**2.5*MOU2*np4*p2p4   & 
          +  & 
           39*CMPLX(aa,bb)*asym*MIN2**1.5*p1p2*p2p4   & 
          +  & 
           120*MIN2**3*p1p2*p2p4   & 
          -  & 
           12*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p2*p2p4   & 
          -  & 
           64*MIN2**2*MOU2*p1p2*p2p4   & 
          +  & 
           100*MIN2**2.5*np2*p1p2*p2p4   & 
          -  & 
           36*MIN2**1.5*MOU2*np2*p1p2*p2p4   & 
          -  & 
           136*MIN2**2.5*np4*p1p2*p2p4   & 
          +  & 
           72*MIN2**1.5*MOU2*np4*p1p2*p2p4   & 
          -  & 
           24*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2**2*p2p4   & 
          -  & 
           156*MIN2**2*p1p2**2*p2p4   & 
          +  & 
           36*MIN2*MOU2*p1p2**2*p2p4   & 
          -  & 
           64*MIN2**1.5*np2*p1p2**2*p2p4   & 
          +  & 
           164*MIN2**1.5*np4*p1p2**2*p2p4   & 
          -  & 
           36*sqrt(MIN2)*MOU2*np4*p1p2**2*p2p4   & 
          +  & 
           64*MIN2*p1p2**3*p2p4   & 
          -  & 
           64*sqrt(MIN2)*np4*p1p2**3*p2p4   & 
          +  & 
           84*CMPLX(aa,bb)*asym*MIN2**1.5*p1p4*p2p4   & 
          +  & 
           34*MIN2**3*p1p4*p2p4   & 
          -  & 
           64*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p4*p2p4   & 
          -  & 
           52*MIN2**2*MOU2*p1p4*p2p4   & 
          +  & 
           18*MIN2*MOU2**2*p1p4*p2p4   & 
          -  & 
           4*MIN2**2.5*np2*p1p4*p2p4   & 
          +  & 
           8*MIN2**1.5*MOU2*np2*p1p4*p2p4   & 
          -  & 
           150*MIN2**2.5*np4*p1p4*p2p4   & 
          +  & 
           200*MIN2**1.5*MOU2*np4*p1p4*p2p4   & 
          -  & 
           54*sqrt(MIN2)*MOU2**2*np4*p1p4*p2p4   & 
          -  & 
           132*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p1p4*p2p4   & 
          +  & 
           38*MIN2**2*p1p2*p1p4*p2p4   & 
          -  & 
           22*MIN2*MOU2*p1p2*p1p4*p2p4   & 
          +  & 
           84*MIN2**1.5*np2*p1p2*p1p4*p2p4   & 
          -  & 
           36*sqrt(MIN2)*MOU2*np2*p1p2*p1p4*p2p4   & 
          +  & 
           406*MIN2**1.5*np4*p1p2*p1p4*p2p4   & 
          -  & 
           234*sqrt(MIN2)*MOU2*np4*p1p2*p1p4*p2p4   & 
          -  & 
           264*MIN2*p1p2**2*p1p4*p2p4   & 
          +  & 
           72*MOU2*p1p2**2*p1p4*p2p4   & 
          -  & 
           128*sqrt(MIN2)*np2*p1p2**2*p1p4*p2p4   & 
          -  & 
           256*sqrt(MIN2)*np4*p1p2**2*p1p4*p2p4   & 
          +  & 
           192*p1p2**3*p1p4*p2p4   & 
          -  & 
           52*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4**2*p2p4   & 
          +  & 
           32*MIN2**2*p1p4**2*p2p4   & 
          -  & 
           142*MIN2*MOU2*p1p4**2*p2p4   & 
          +  & 
           54*MOU2**2*p1p4**2*p2p4   & 
          -  & 
           2*MIN2**1.5*np2*p1p4**2*p2p4   & 
          -  & 
           46*sqrt(MIN2)*MOU2*np2*p1p4**2*p2p4   & 
          +  & 
           144*MIN2**1.5*np4*p1p4**2*p2p4   & 
          -  & 
           112*sqrt(MIN2)*MOU2*np4*p1p4**2*p2p4   & 
          -  & 
           292*MIN2*p1p2*p1p4**2*p2p4   & 
          +  & 
           280*MOU2*p1p2*p1p4**2*p2p4   & 
          -  & 
           16*sqrt(MIN2)*np2*p1p2*p1p4**2*p2p4   & 
          -  & 
           228*sqrt(MIN2)*np4*p1p2*p1p4**2*p2p4   & 
          +  & 
           272*p1p2**2*p1p4**2*p2p4   & 
          -  & 
           144*MIN2*p1p4**3*p2p4   & 
          + 112*MOU2*p1p4**3*p2p4   & 
          -  & 
           44*sqrt(MIN2)*np2*p1p4**3*p2p4   & 
          -  & 
           16*sqrt(MIN2)*np4*p1p4**3*p2p4   & 
          +  & 
           272*p1p2*p1p4**3*p2p4   & 
          + 16*p1p4**4*p2p4   & 
          -  & 
           57*CMPLX(aa,bb)*asym*MIN2**1.5*p2p4**2   & 
          - 120*MIN2**3*p2p4**2   & 
          +  & 
           30*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p2p4**2   & 
          +  & 
           64*MIN2**2*MOU2*p2p4**2   & 
          -  & 
           100*MIN2**2.5*np2*p2p4**2   & 
          +  & 
           36*MIN2**1.5*MOU2*np2*p2p4**2   & 
          +  & 
           172*MIN2**2.5*np4*p2p4**2   & 
          -  & 
           108*MIN2**1.5*MOU2*np4*p2p4**2   & 
          +  & 
           72*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p2p4**2   & 
          +  & 
           312*MIN2**2*p1p2*p2p4**2   & 
          -  & 
           72*MIN2*MOU2*p1p2*p2p4**2   & 
          +  & 
           128*MIN2**1.5*np2*p1p2*p2p4**2   & 
          -  & 
           412*MIN2**1.5*np4*p1p2*p2p4**2   & 
          +  & 
           108*sqrt(MIN2)*MOU2*np4*p1p2*p2p4**2   & 
          -  & 
           192*MIN2*p1p2**2*p2p4**2   & 
          +  & 
           240*sqrt(MIN2)*np4*p1p2**2*p2p4**2   & 
          +  & 
           130*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4*p2p4**2   & 
          +  & 
           42*MIN2**2*p1p4*p2p4**2   & 
          +  & 
           14*MIN2*MOU2*p1p4*p2p4**2   & 
          +  & 
           16*MIN2**1.5*np2*p1p4*p2p4**2   & 
          -  & 
           348*MIN2**1.5*np4*p1p4*p2p4**2   & 
          +  & 
           196*sqrt(MIN2)*MOU2*np4*p1p4*p2p4**2   & 
          +  & 
           212*MIN2*p1p2*p1p4*p2p4**2   & 
          -  & 
           108*MOU2*p1p2*p1p4*p2p4**2   & 
          +  & 
           64*sqrt(MIN2)*np2*p1p2*p1p4*p2p4**2   & 
          +  & 
           464*sqrt(MIN2)*np4*p1p2*p1p4*p2p4**2   & 
          -  & 
           304*p1p2**2*p1p4*p2p4**2   & 
          +  & 
           272*MIN2*p1p4**2*p2p4**2   & 
          -  & 
           196*MOU2*p1p4**2*p2p4**2   & 
          +  & 
           32*sqrt(MIN2)*np2*p1p4**2*p2p4**2   & 
          +  & 
           144*sqrt(MIN2)*np4*p1p4**2*p2p4**2   & 
          -  & 
           496*p1p2*p1p4**2*p2p4**2   & 
          - 144*p1p4**3*p2p4**2   & 
          -  & 
           48*CMPLX(aa,bb)*asym*sqrt(MIN2)*p2p4**3   & 
          -  & 
           156*MIN2**2*p2p4**3   & 
          + 36*MIN2*MOU2*p2p4**3   & 
          -  & 
           64*MIN2**1.5*np2*p2p4**3   & 
          +  & 
           248*MIN2**1.5*np4*p2p4**3   & 
          -  & 
           72*sqrt(MIN2)*MOU2*np4*p2p4**3   & 
          +  & 
           192*MIN2*p1p2*p2p4**3   & 
          -  & 
           288*sqrt(MIN2)*np4*p1p2*p2p4**3   & 
          -  & 
           104*MIN2*p1p4*p2p4**3   & 
          + 72*MOU2*p1p4*p2p4**3   & 
          -  & 
           240*sqrt(MIN2)*np4*p1p4*p2p4**3   & 
          +  & 
           288*p1p2*p1p4*p2p4**3   & 
          + 240*p1p4**2*p2p4**3   & 
          -  & 
           64*MIN2*p2p4**4   & 
          + 112*sqrt(MIN2)*np4*p2p4**4   & 
          -  & 
           112*p1p4*p2p4**4)* & 
         Ccache(1))/ & 
       (9.*p1p4**2*(p1p4   & 
          - p2p4)*Pi**2)   & 
          +  & 
      (4*EL**4*GF**2*(  & 
          -26*CMPLX(aa,bb)*asym*MIN2**1.5*p1p4   & 
          +  & 
           26*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p4   & 
          +  & 
           4*MIN2**2.5*np4*p1p4   & 
          -  & 
           8*MIN2**1.5*MOU2*np4*p1p4   & 
          +  & 
           4*sqrt(MIN2)*MOU2**2*np4*p1p4   & 
          +  & 
           57*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p1p4   & 
          -  & 
           24*MIN2**2*p1p2*p1p4   & 
          + 24*MIN2*MOU2*p1p2*p1p4   & 
          -  & 
           8*MIN2**1.5*np2*p1p2*p1p4   & 
          +  & 
           8*sqrt(MIN2)*MOU2*np2*p1p2*p1p4   & 
          -  & 
           20*MIN2**1.5*np4*p1p2*p1p4   & 
          +  & 
           20*sqrt(MIN2)*MOU2*np4*p1p2*p1p4   & 
          +  & 
           56*MIN2*p1p2**2*p1p4   & 
          - 8*MOU2*p1p2**2*p1p4   & 
          +  & 
           32*sqrt(MIN2)*np2*p1p2**2*p1p4   & 
          +  & 
           16*sqrt(MIN2)*np4*p1p2**2*p1p4   & 
          -  & 
           32*p1p2**3*p1p4   & 
          - 5*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4**2   & 
          +  & 
           20*MIN2**2*p1p4**2   & 
          - 16*MIN2*MOU2*p1p4**2   & 
          -  & 
           4*MOU2**2*p1p4**2   & 
          - 4*MIN2**1.5*np2*p1p4**2   & 
          +  & 
           4*sqrt(MIN2)*MOU2*np2*p1p4**2   & 
          -  & 
           8*MIN2**1.5*np4*p1p4**2   & 
          +  & 
           8*sqrt(MIN2)*MOU2*np4*p1p4**2   & 
          -  & 
           24*MIN2*p1p2*p1p4**2   & 
          - 24*MOU2*p1p2*p1p4**2   & 
          -  & 
           16*sqrt(MIN2)*np2*p1p2*p1p4**2   & 
          +  & 
           24*sqrt(MIN2)*np4*p1p2*p1p4**2   & 
          + 8*MIN2*p1p4**3   & 
          -  & 
           8*MOU2*p1p4**3   & 
          + 8*sqrt(MIN2)*np2*p1p4**3   & 
          -  & 
           32*p1p2*p1p4**3   & 
          + 22*CMPLX(aa,bb)*asym*MIN2**1.5*p2p4   & 
          +  & 
           24*MIN2**3*p2p4   & 
          - 22*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p2p4   & 
          -  & 
           24*MIN2**2*MOU2*p2p4   & 
          + 8*MIN2**2.5*np2*p2p4   & 
          -  & 
           8*MIN2**1.5*MOU2*np2*p2p4   & 
          -  & 
           8*MIN2**2.5*np4*p2p4   & 
          +  & 
           8*MIN2**1.5*MOU2*np4*p2p4   & 
          -  & 
           41*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p2p4   & 
          -  & 
           56*MIN2**2*p1p2*p2p4   & 
          + 8*MIN2*MOU2*p1p2*p2p4   & 
          -  & 
           32*MIN2**1.5*np2*p1p2*p2p4   & 
          +  & 
           40*MIN2**1.5*np4*p1p2*p2p4   & 
          -  & 
           8*sqrt(MIN2)*MOU2*np4*p1p2*p2p4   & 
          +  & 
           32*MIN2*p1p2**2*p2p4   & 
          -  & 
           32*sqrt(MIN2)*np4*p1p2**2*p2p4   & 
          -  & 
           63*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4*p2p4   & 
          -  & 
           20*MIN2**2*p1p4*p2p4   & 
          + 20*MIN2*MOU2*p1p4*p2p4   & 
          +  & 
           8*MIN2**1.5*np2*p1p4*p2p4   & 
          +  & 
           52*MIN2**1.5*np4*p1p4*p2p4   & 
          -  & 
           60*sqrt(MIN2)*MOU2*np4*p1p4*p2p4   & 
          -  & 
           32*MIN2*p1p2*p1p4*p2p4   & 
          + 8*MOU2*p1p2*p1p4*p2p4   & 
          -  & 
           32*sqrt(MIN2)*np2*p1p2*p1p4*p2p4   & 
          -  & 
           110*sqrt(MIN2)*np4*p1p2*p1p4*p2p4   & 
          +  & 
           64*p1p2**2*p1p4*p2p4   & 
          - 12*MIN2*p1p4**2*p2p4   & 
          +  & 
           60*MOU2*p1p4**2*p2p4   & 
          +  & 
           22*sqrt(MIN2)*np2*p1p4**2*p2p4   & 
          -  & 
           32*sqrt(MIN2)*np4*p1p4**2*p2p4   & 
          +  & 
           88*p1p2*p1p4**2*p2p4   & 
          + 32*p1p4**3*p2p4   & 
          +  & 
           44*CMPLX(aa,bb)*asym*sqrt(MIN2)*p2p4**2   & 
          + 56*MIN2**2*p2p4**2   & 
          -  & 
           8*MIN2*MOU2*p2p4**2   & 
          + 32*MIN2**1.5*np2*p2p4**2   & 
          -  & 
           46*MIN2**1.5*np4*p2p4**2   & 
          +  & 
           14*sqrt(MIN2)*MOU2*np4*p2p4**2   & 
          -  & 
           64*MIN2*p1p2*p2p4**2   & 
          +  & 
           88*sqrt(MIN2)*np4*p1p2*p2p4**2   & 
          +  & 
           4*MIN2*p1p4*p2p4**2   & 
          - 14*MOU2*p1p4*p2p4**2   & 
          +  & 
           88*sqrt(MIN2)*np4*p1p4*p2p4**2   & 
          -  & 
           88*p1p2*p1p4*p2p4**2   & 
          - 88*p1p4**2*p2p4**2   & 
          +  & 
           32*MIN2*p2p4**3   & 
          - 56*sqrt(MIN2)*np4*p2p4**3   & 
          +  & 
           56*p1p4*p2p4**3)* & 
         Ccache(2))/ & 
       (9.*p1p4**2*(p1p4   & 
          - p2p4)*Pi**2)   & 
          -  & 
      (2*EL**4*GF**2*(  & 
          -4*CMPLX(aa,bb)*asym*MIN2**2.5*p1p4   & 
          +  & 
           26*CMPLX(aa,bb)*asym*MIN2**1.5*MOU2*p1p4   & 
          -  & 
           22*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2**2*p1p4   & 
          +  & 
           2*MIN2**3.5*np4*p1p4   & 
          -  & 
           13*MIN2**2.5*MOU2*np4*p1p4   & 
          +  & 
           20*MIN2**1.5*MOU2**2*np4*p1p4   & 
          -  & 
           9*sqrt(MIN2)*MOU2**3*np4*p1p4   & 
          +  & 
           6*CMPLX(aa,bb)*asym*MIN2**1.5*p1p2*p1p4   & 
          +  & 
           20*MIN2**3*p1p2*p1p4   & 
          -  & 
           48*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p2*p1p4   & 
          -  & 
           20*MIN2**2*MOU2*p1p2*p1p4   & 
          -  & 
           4*MIN2**2.5*np2*p1p2*p1p4   & 
          +  & 
           22*MIN2**1.5*MOU2*np2*p1p2*p1p4   & 
          -  & 
           18*sqrt(MIN2)*MOU2**2*np2*p1p2*p1p4   & 
          +  & 
           4*MIN2**2.5*np4*p1p2*p1p4   & 
          +  & 
           18*MIN2**1.5*MOU2*np4*p1p2*p1p4   & 
          -  & 
           22*sqrt(MIN2)*MOU2**2*np4*p1p2*p1p4   & 
          -  & 
           2*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2**2*p1p4   & 
          -  & 
           56*MIN2**2*p1p2**2*p1p4   & 
          -  & 
           2*MIN2*MOU2*p1p2**2*p1p4   & 
          +  & 
           18*MOU2**2*p1p2**2*p1p4   & 
          -  & 
           12*MIN2**1.5*np2*p1p2**2*p1p4   & 
          -  & 
           40*sqrt(MIN2)*MOU2*np2*p1p2**2*p1p4   & 
          -  & 
           14*MIN2**1.5*np4*p1p2**2*p1p4   & 
          +  & 
           2*sqrt(MIN2)*MOU2*np4*p1p2**2*p1p4   & 
          +  & 
           52*MIN2*p1p2**3*p1p4   & 
          + 40*MOU2*p1p2**3*p1p4   & 
          +  & 
           16*sqrt(MIN2)*np2*p1p2**3*p1p4   & 
          +  & 
           8*sqrt(MIN2)*np4*p1p2**3*p1p4   & 
          - 16*p1p2**4*p1p4   & 
          +  & 
           5*CMPLX(aa,bb)*asym*MIN2**1.5*p1p4**2   & 
          - 26*MIN2**3*p1p4**2   & 
          +  & 
           CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p4**2   & 
          +  & 
           41*MIN2**2*MOU2*p1p4**2   & 
          -  & 
           24*MIN2*MOU2**2*p1p4**2   & 
          + 9*MOU2**3*p1p4**2   & 
          +  & 
           18*MIN2**2.5*np2*p1p4**2   & 
          -  & 
           18*MIN2**1.5*MOU2*np2*p1p4**2   & 
          -  & 
           10*MIN2**2.5*np4*p1p4**2   & 
          +  & 
           44*MIN2**1.5*MOU2*np4*p1p4**2   & 
          -  & 
           34*sqrt(MIN2)*MOU2**2*np4*p1p4**2   & 
          +  & 
           18*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p1p4**2   & 
          +  & 
           30*MIN2**2*p1p2*p1p4**2   & 
          -  & 
           12*MIN2*MOU2*p1p2*p1p4**2   & 
          +  & 
           22*MOU2**2*p1p2*p1p4**2   & 
          -  & 
           6*MIN2**1.5*np2*p1p2*p1p4**2   & 
          +  & 
           6*sqrt(MIN2)*MOU2*np2*p1p2*p1p4**2   & 
          +  & 
           2*MIN2**1.5*np4*p1p2*p1p4**2   & 
          -  & 
           62*sqrt(MIN2)*MOU2*np4*p1p2*p1p4**2   & 
          +  & 
           12*MIN2*p1p2**2*p1p4**2   & 
          -  & 
           8*MOU2*p1p2**2*p1p4**2   & 
          +  & 
           8*sqrt(MIN2)*np2*p1p2**2*p1p4**2   & 
          +  & 
           8*sqrt(MIN2)*np4*p1p2**2*p1p4**2   & 
          -  & 
           16*p1p2**3*p1p4**2   & 
          - 22*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4**3   & 
          +  & 
           2*MIN2**2*p1p4**3   & 
          - 36*MIN2*MOU2*p1p4**3   & 
          +  & 
           34*MOU2**2*p1p4**3   & 
          - 66*MIN2**1.5*np2*p1p4**3   & 
          +  & 
           54*sqrt(MIN2)*MOU2*np2*p1p4**3   & 
          +  & 
           8*MIN2**1.5*np4*p1p4**3   & 
          -  & 
           8*sqrt(MIN2)*MOU2*np4*p1p4**3   & 
          +  & 
           80*MIN2*p1p2*p1p4**3   & 
          + 8*MOU2*p1p2*p1p4**3   & 
          +  & 
           72*sqrt(MIN2)*np2*p1p2*p1p4**3   & 
          -  & 
           80*p1p2**2*p1p4**3   & 
          - 8*MIN2*p1p4**4   & 
          +  & 
           8*MOU2*p1p4**4   & 
          + 16*sqrt(MIN2)*np2*p1p4**4   & 
          -  & 
           16*p1p2*p1p4**4   & 
          - 22*CMPLX(aa,bb)*asym*MIN2**2.5*p2p4   & 
          -  & 
           20*MIN2**4*p2p4   & 
          + 16*CMPLX(aa,bb)*asym*MIN2**1.5*MOU2*p2p4   & 
          +  & 
           20*MIN2**3*MOU2*p2p4   & 
          +  & 
           6*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2**2*p2p4   & 
          +  & 
           4*MIN2**3.5*np2*p2p4   & 
          -  & 
           22*MIN2**2.5*MOU2*np2*p2p4   & 
          +  & 
           18*MIN2**1.5*MOU2**2*np2*p2p4   & 
          -  & 
           4*MIN2**3.5*np4*p2p4   & 
          +  & 
           22*MIN2**2.5*MOU2*np4*p2p4   & 
          -  & 
           18*MIN2**1.5*MOU2**2*np4*p2p4   & 
          +  & 
           48*CMPLX(aa,bb)*asym*MIN2**1.5*p1p2*p2p4   & 
          +  & 
           56*MIN2**3*p1p2*p2p4   & 
          -  & 
           4*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p2*p2p4   & 
          +  & 
           2*MIN2**2*MOU2*p1p2*p2p4   & 
          -  & 
           18*MIN2*MOU2**2*p1p2*p2p4   & 
          +  & 
           12*MIN2**2.5*np2*p1p2*p2p4   & 
          +  & 
           40*MIN2**1.5*MOU2*np2*p1p2*p2p4   & 
          -  & 
           8*MIN2**2.5*np4*p1p2*p2p4   & 
          -  & 
           62*MIN2**1.5*MOU2*np4*p1p2*p2p4   & 
          +  & 
           18*sqrt(MIN2)*MOU2**2*np4*p1p2*p2p4   & 
          -  & 
           26*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2**2*p2p4   & 
          -  & 
           52*MIN2**2*p1p2**2*p2p4   & 
          -  & 
           40*MIN2*MOU2*p1p2**2*p2p4   & 
          -  & 
           16*MIN2**1.5*np2*p1p2**2*p2p4   & 
          +  & 
           28*MIN2**1.5*np4*p1p2**2*p2p4   & 
          +  & 
           40*sqrt(MIN2)*MOU2*np4*p1p2**2*p2p4   & 
          +  & 
           16*MIN2*p1p2**3*p2p4   & 
          -  & 
           16*sqrt(MIN2)*np4*p1p2**3*p2p4   & 
          +  & 
           46*CMPLX(aa,bb)*asym*MIN2**1.5*p1p4*p2p4   & 
          +  & 
           46*MIN2**3*p1p4*p2p4   & 
          +  & 
           2*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p4*p2p4   & 
          -  & 
           44*MIN2**2*MOU2*p1p4*p2p4   & 
          -  & 
           2*MIN2*MOU2**2*p1p4*p2p4   & 
          -  & 
           20*MIN2**2.5*np2*p1p4*p2p4   & 
          +  & 
           8*MIN2**1.5*MOU2*np2*p1p4*p2p4   & 
          -  & 
           42*MIN2**2.5*np4*p1p4*p2p4   & 
          -  & 
           8*MIN2**1.5*MOU2*np4*p1p4*p2p4   & 
          +  & 
           62*sqrt(MIN2)*MOU2**2*np4*p1p4*p2p4   & 
          -  & 
           91*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p1p4*p2p4   & 
          -  & 
           30*MIN2**2*p1p2*p1p4*p2p4   & 
          +  & 
           16*MIN2*MOU2*p1p2*p1p4*p2p4   & 
          -  & 
           18*MOU2**2*p1p2*p1p4*p2p4   & 
          +  & 
           12*MIN2**1.5*np2*p1p2*p1p4*p2p4   & 
          +  & 
           40*sqrt(MIN2)*MOU2*np2*p1p2*p1p4*p2p4   & 
          +  & 
           166*MIN2**1.5*np4*p1p2*p1p4*p2p4   & 
          +  & 
           42*sqrt(MIN2)*MOU2*np4*p1p2*p1p4*p2p4   & 
          -  & 
           64*MIN2*p1p2**2*p1p4*p2p4   & 
          -  & 
           80*MOU2*p1p2**2*p1p4*p2p4   & 
          -  & 
           32*sqrt(MIN2)*np2*p1p2**2*p1p4*p2p4   & 
          -  & 
           124*sqrt(MIN2)*np4*p1p2**2*p1p4*p2p4   & 
          +  & 
           48*p1p2**3*p1p4*p2p4   & 
          -  & 
           12*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4**2*p2p4   & 
          -  & 
           36*MIN2**2*p1p4**2*p2p4   & 
          +  & 
           58*MIN2*MOU2*p1p4**2*p2p4   & 
          -  & 
           62*MOU2**2*p1p4**2*p2p4   & 
          +  & 
           46*MIN2**1.5*np2*p1p4**2*p2p4   & 
          -  & 
           18*sqrt(MIN2)*MOU2*np2*p1p4**2*p2p4   & 
          +  & 
           56*MIN2**1.5*np4*p1p4**2*p2p4   & 
          -  & 
           12*sqrt(MIN2)*MOU2*np4*p1p4**2*p2p4   & 
          -  & 
           148*MIN2*p1p2*p1p4**2*p2p4   & 
          -  & 
           24*MOU2*p1p2*p1p4**2*p2p4   & 
          -  & 
           52*sqrt(MIN2)*np2*p1p2*p1p4**2*p2p4   & 
          -  & 
           116*sqrt(MIN2)*np4*p1p2*p1p4**2*p2p4   & 
          +  & 
           176*p1p2**2*p1p4**2*p2p4   & 
          - 56*MIN2*p1p4**3*p2p4   & 
          +  & 
           12*MOU2*p1p4**3*p2p4   & 
          -  & 
           60*sqrt(MIN2)*np2*p1p4**3*p2p4   & 
          -  & 
           16*sqrt(MIN2)*np4*p1p4**3*p2p4   & 
          +  & 
           176*p1p2*p1p4**3*p2p4   & 
          + 16*p1p4**4*p2p4   & 
          -  & 
           60*CMPLX(aa,bb)*asym*MIN2**1.5*p2p4**2   & 
          - 56*MIN2**3*p2p4**2   & 
          +  & 
           19*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p2p4**2   & 
          -  & 
           2*MIN2**2*MOU2*p2p4**2   & 
          +  & 
           18*MIN2*MOU2**2*p2p4**2   & 
          -  & 
           12*MIN2**2.5*np2*p2p4**2   & 
          -  & 
           40*MIN2**1.5*MOU2*np2*p2p4**2   & 
          +  & 
           32*MIN2**2.5*np4*p2p4**2   & 
          +  & 
           56*MIN2**1.5*MOU2*np4*p2p4**2   & 
          -  & 
           36*sqrt(MIN2)*MOU2**2*np4*p2p4**2   & 
          +  & 
           82*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p2p4**2   & 
          +  & 
           104*MIN2**2*p1p2*p2p4**2   & 
          +  & 
           80*MIN2*MOU2*p1p2*p2p4**2   & 
          +  & 
           32*MIN2**1.5*np2*p1p2*p2p4**2   & 
          -  & 
           128*MIN2**1.5*np4*p1p2*p2p4**2   & 
          -  & 
           92*sqrt(MIN2)*MOU2*np4*p1p2*p2p4**2   & 
          -  & 
           48*MIN2*p1p2**2*p2p4**2   & 
          +  & 
           96*sqrt(MIN2)*np4*p1p2**2*p2p4**2   & 
          +  & 
           90*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4*p2p4**2   & 
          +  & 
           70*MIN2**2*p1p4*p2p4**2   & 
          -  & 
           46*MIN2*MOU2*p1p4*p2p4**2   & 
          +  & 
           36*MOU2**2*p1p4*p2p4**2   & 
          -  & 
           164*MIN2**1.5*np4*p1p4*p2p4**2   & 
          -  & 
           32*sqrt(MIN2)*MOU2*np4*p1p4*p2p4**2   & 
          +  & 
           52*MIN2*p1p2*p1p4*p2p4**2   & 
          +  & 
           92*MOU2*p1p2*p1p4*p2p4**2   & 
          +  & 
           16*sqrt(MIN2)*np2*p1p2*p1p4*p2p4**2   & 
          +  & 
           260*sqrt(MIN2)*np4*p1p2*p1p4*p2p4**2   & 
          -  & 
           112*p1p2**2*p1p4*p2p4**2   & 
          +  & 
           120*MIN2*p1p4**2*p2p4**2   & 
          +  & 
           32*MOU2*p1p4**2*p2p4**2   & 
          +  & 
           44*sqrt(MIN2)*np2*p1p4**2*p2p4**2   & 
          +  & 
           96*sqrt(MIN2)*np4*p1p4**2*p2p4**2   & 
          -  & 
           304*p1p2*p1p4**2*p2p4**2   & 
          - 96*p1p4**3*p2p4**2   & 
          -  & 
           56*CMPLX(aa,bb)*asym*sqrt(MIN2)*p2p4**3   & 
          - 52*MIN2**2*p2p4**3   & 
          -  & 
           40*MIN2*MOU2*p2p4**3   & 
          - 16*MIN2**1.5*np2*p2p4**3   & 
          +  & 
           100*MIN2**1.5*np4*p2p4**3   & 
          +  & 
           52*sqrt(MIN2)*MOU2*np4*p2p4**3   & 
          +  & 
           48*MIN2*p1p2*p2p4**3   & 
          -  & 
           144*sqrt(MIN2)*np4*p1p2*p2p4**3   & 
          -  & 
           40*MIN2*p1p4*p2p4**3   & 
          - 52*MOU2*p1p4*p2p4**3   & 
          -  & 
           144*sqrt(MIN2)*np4*p1p4*p2p4**3   & 
          +  & 
           144*p1p2*p1p4*p2p4**3   & 
          + 144*p1p4**2*p2p4**3   & 
          -  & 
           16*MIN2*p2p4**4   & 
          + 64*sqrt(MIN2)*np4*p2p4**4   & 
          -  & 
           64*p1p4*p2p4**4)* & 
         Ccache(3))/ & 
       (9.*p1p4**2*(p1p4   & 
          - p2p4)*Pi**2)   & 
          -  & 
      (4*EL**4*GF**2*((  & 
          -(CMPLX(aa,bb)*asym*sqrt(MIN2)*(p1p4   & 
          - p2p4)* & 
                 (  & 
          -MIN2**2   & 
          + MIN2*MOU2   & 
          + 2*MIN2*p1p2   & 
          -  & 
                   MOU2*p1p2   & 
          - p1p2**2   & 
          + MIN2*p1p4   & 
          -  & 
                   p1p2*p1p4   & 
          - MIN2*p2p4   & 
          + p1p2*p2p4))   & 
          -  & 
              MOU2*(2*MIN2*p1p4**3   & 
          - 2*MOU2*p1p4**3   & 
          -  & 
                 4*p1p2*p1p4**3   & 
          - 4*MIN2*p1p4**2*p2p4   & 
          +  & 
                 4*MOU2*p1p4**2*p2p4   & 
          +  & 
                 10*p1p2*p1p4**2*p2p4   & 
          + 2*p1p4**3*p2p4   & 
          +  & 
                 2*MIN2*p1p4*p2p4**2   & 
          - 2*MOU2*p1p4*p2p4**2   & 
          -  & 
                 6*p1p2*p1p4*p2p4**2   & 
          - 8*p1p4**2*p2p4**2   & 
          +  & 
                 6*p1p4*p2p4**3   & 
          +  & 
                 sqrt(MIN2)*np4* & 
                  (  & 
          -2*MIN2*p1p4**2   & 
          + 2*MOU2*p1p4**2   & 
          +  & 
                    4*p1p2*p1p4**2   & 
          + 4*MIN2*p1p4*p2p4   & 
          -  & 
                    4*MOU2*p1p4*p2p4   & 
          - 10*p1p2*p1p4*p2p4   & 
          -  & 
                    2*p1p4**2*p2p4   & 
          - 2*MIN2*p2p4**2   & 
          +  & 
                    2*MOU2*p2p4**2   & 
          + 6*p1p2*p2p4**2   & 
          +  & 
                    8*p1p4*p2p4**2   & 
          - 6*p2p4**3)))/3.   & 
          +  & 
           (4*(CMPLX(aa,bb)*asym*sqrt(MIN2)*(p1p4   & 
          - p2p4)* & 
                 (MIN2*MOU2   & 
          - MOU2**2   & 
          - 2*MOU2*p1p2   & 
          +  & 
                   2*MOU2*p2p4)   & 
          +  & 
                MOU2*(4*MIN2**2*p1p2*p1p4   & 
          -  & 
                   4*MIN2*MOU2*p1p2*p1p4   & 
          +  & 
                   2*MIN2**1.5*np2*p1p2*p1p4   & 
          -  & 
                   2*sqrt(MIN2)*MOU2*np2*p1p2*p1p4   & 
          -  & 
                   10*MIN2*p1p2**2*p1p4   & 
          +  & 
                   2*MOU2*p1p2**2*p1p4   & 
          -  & 
                   6*sqrt(MIN2)*np2*p1p2**2*p1p4   & 
          +  & 
                   6*p1p2**3*p1p4   & 
          - 3*MIN2**2*p1p4**2   & 
          +  & 
                   2*MIN2*MOU2*p1p4**2   & 
          + MOU2**2*p1p4**2   & 
          +  & 
                   3*MIN2*p1p2*p1p4**2   & 
          +  & 
                   5*MOU2*p1p2*p1p4**2   & 
          +  & 
                   4*sqrt(MIN2)*np2*p1p2*p1p4**2   & 
          +  & 
                   2*p1p2**2*p1p4**2   & 
          - 3*MIN2*p1p4**3   & 
          +  & 
                   3*MOU2*p1p4**3   & 
          -  & 
                   2*sqrt(MIN2)*np2*p1p4**3   & 
          +  & 
                   8*p1p2*p1p4**3   & 
          - 4*MIN2**3*p2p4   & 
          +  & 
                   4*MIN2**2*MOU2*p2p4   & 
          -  & 
                   2*MIN2**2.5*np2*p2p4   & 
          +  & 
                   2*MIN2**1.5*MOU2*np2*p2p4   & 
          +  & 
                   10*MIN2**2*p1p2*p2p4   & 
          -  & 
                   2*MIN2*MOU2*p1p2*p2p4   & 
          +  & 
                   6*MIN2**1.5*np2*p1p2*p2p4   & 
          -  & 
                   6*MIN2*p1p2**2*p2p4   & 
          +  & 
                   4*MIN2**2*p1p4*p2p4   & 
          -  & 
                   4*MIN2*MOU2*p1p4*p2p4   & 
          -  & 
                   2*MIN2**1.5*np2*p1p4*p2p4   & 
          +  & 
                   4*MIN2*p1p2*p1p4*p2p4   & 
          -  & 
                   2*MOU2*p1p2*p1p4*p2p4   & 
          +  & 
                   6*sqrt(MIN2)*np2*p1p2*p1p4*p2p4   & 
          -  & 
                   12*p1p2**2*p1p4*p2p4   & 
          +  & 
                   MIN2*p1p4**2*p2p4   & 
          - 9*MOU2*p1p4**2*p2p4   & 
          -  & 
                   2*sqrt(MIN2)*np2*p1p4**2*p2p4   & 
          -  & 
                   16*p1p2*p1p4**2*p2p4   & 
          - 6*p1p4**3*p2p4   & 
          -  & 
                   10*MIN2**2*p2p4**2   & 
          +  & 
                   2*MIN2*MOU2*p2p4**2   & 
          -  & 
                   6*MIN2**1.5*np2*p2p4**2   & 
          +  & 
                   12*MIN2*p1p2*p2p4**2   & 
          +  & 
                   4*MIN2*p1p4*p2p4**2   & 
          +  & 
                   2*MOU2*p1p4*p2p4**2   & 
          +  & 
                   12*p1p2*p1p4*p2p4**2   & 
          +  & 
                   12*p1p4**2*p2p4**2   & 
          - 6*MIN2*p2p4**3   & 
          -  & 
                   6*p1p4*p2p4**3   & 
          +  & 
                   sqrt(MIN2)*np4* & 
                    (  & 
          -3*MIN2**2*p1p4   & 
          + 4*MIN2*MOU2*p1p4   & 
          -  & 
                      MOU2**2*p1p4   & 
          + 9*MIN2*p1p2*p1p4   & 
          -  & 
                      5*MOU2*p1p2*p1p4   & 
          - 6*p1p2**2*p1p4   & 
          +  & 
                      3*MIN2*p1p4**2   & 
          - 3*MOU2*p1p4**2   & 
          -  & 
                      6*p1p2*p1p4**2   & 
          + 2*MIN2**2*p2p4   & 
          -  & 
                      2*MIN2*MOU2*p2p4   & 
          - 8*MIN2*p1p2*p2p4   & 
          +  & 
                      2*MOU2*p1p2*p2p4   & 
          + 6*p1p2**2*p2p4   & 
          -  & 
                      11*MIN2*p1p4*p2p4   & 
          + 9*MOU2*p1p4*p2p4   & 
          +  & 
                      18*p1p2*p1p4*p2p4   & 
          + 6*p1p4**2*p2p4   & 
          +  & 
                      8*MIN2*p2p4**2   & 
          - 2*MOU2*p2p4**2   & 
          -  & 
                      12*p1p2*p2p4**2   & 
          - 12*p1p4*p2p4**2   & 
          +  & 
                      6*p2p4**3))))/9.)* & 
         Ccache(4))/ & 
       (p1p4**2*(p1p4   & 
          - p2p4)*Pi**2)   & 
          -  & 
      (4*EL**4*GF**2*(10*CMPLX(aa,bb)*asym*MIN2**2.5*p1p4   & 
          -  & 
           14*CMPLX(aa,bb)*asym*MIN2**1.5*MOU2*p1p4   & 
          +  & 
           4*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2**2*p1p4   & 
          -  & 
           12*MIN2**3.5*np4*p1p4   & 
          +  & 
           8*MIN2**2.5*MOU2*np4*p1p4   & 
          +  & 
           4*MIN2**1.5*MOU2**2*np4*p1p4   & 
          -  & 
           29*CMPLX(aa,bb)*asym*MIN2**1.5*p1p2*p1p4   & 
          +  & 
           24*MIN2**3*p1p2*p1p4   & 
          +  & 
           23*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p2*p1p4   & 
          -  & 
           16*MIN2**2*MOU2*p1p2*p1p4   & 
          -  & 
           8*MIN2*MOU2**2*p1p2*p1p4   & 
          +  & 
           8*MIN2**2.5*np2*p1p2*p1p4   & 
          -  & 
           8*MIN2**1.5*MOU2*np2*p1p2*p1p4   & 
          +  & 
           56*MIN2**2.5*np4*p1p2*p1p4   & 
          -  & 
           24*MIN2**1.5*MOU2*np4*p1p2*p1p4   & 
          +  & 
           19*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2**2*p1p4   & 
          -  & 
           80*MIN2**2*p1p2**2*p1p4   & 
          +  & 
           16*MIN2*MOU2*p1p2**2*p1p4   & 
          -  & 
           40*MIN2**1.5*np2*p1p2**2*p1p4   & 
          +  & 
           8*sqrt(MIN2)*MOU2*np2*p1p2**2*p1p4   & 
          -  & 
           76*MIN2**1.5*np4*p1p2**2*p1p4   & 
          +  & 
           12*sqrt(MIN2)*MOU2*np4*p1p2**2*p1p4   & 
          +  & 
           88*MIN2*p1p2**3*p1p4   & 
          - 8*MOU2*p1p2**3*p1p4   & 
          +  & 
           32*sqrt(MIN2)*np2*p1p2**3*p1p4   & 
          +  & 
           32*sqrt(MIN2)*np4*p1p2**3*p1p4   & 
          -  & 
           32*p1p2**4*p1p4   & 
          - 5*CMPLX(aa,bb)*asym*MIN2**1.5*p1p4**2   & 
          -  & 
           20*MIN2**3*p1p4**2   & 
          -  & 
           CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p4**2   & 
          +  & 
           8*MIN2**2*MOU2*p1p4**2   & 
          +  & 
           12*MIN2*MOU2**2*p1p4**2   & 
          +  & 
           4*MIN2**2.5*np2*p1p4**2   & 
          -  & 
           4*sqrt(MIN2)*MOU2**2*np2*p1p4**2   & 
          +  & 
           26*MIN2**2.5*np4*p1p4**2   & 
          -  & 
           16*MIN2**1.5*MOU2*np4*p1p4**2   & 
          -  & 
           10*sqrt(MIN2)*MOU2**2*np4*p1p4**2   & 
          +  & 
           14*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p1p4**2   & 
          +  & 
           20*MIN2**2*p1p2*p1p4**2   & 
          +  & 
           40*MIN2*MOU2*p1p2*p1p4**2   & 
          +  & 
           4*MOU2**2*p1p2*p1p4**2   & 
          +  & 
           12*MIN2**1.5*np2*p1p2*p1p4**2   & 
          +  & 
           4*sqrt(MIN2)*MOU2*np2*p1p2*p1p4**2   & 
          -  & 
           94*MIN2**1.5*np4*p1p2*p1p4**2   & 
          +  & 
           22*sqrt(MIN2)*MOU2*np4*p1p2*p1p4**2   & 
          +  & 
           32*MIN2*p1p2**2*p1p4**2   & 
          -  & 
           16*MOU2*p1p2**2*p1p4**2   & 
          +  & 
           68*sqrt(MIN2)*np4*p1p2**2*p1p4**2   & 
          -  & 
           32*p1p2**3*p1p4**2   & 
          - 5*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4**3   & 
          -  & 
           2*MIN2**2*p1p4**3   & 
          - 8*MIN2*MOU2*p1p4**3   & 
          +  & 
           10*MOU2**2*p1p4**3   & 
          - 26*MIN2**1.5*np2*p1p4**3   & 
          +  & 
           10*sqrt(MIN2)*MOU2*np2*p1p4**3   & 
          -  & 
           8*MIN2**1.5*np4*p1p4**3   & 
          +  & 
           8*sqrt(MIN2)*MOU2*np4*p1p4**3   & 
          +  & 
           72*MIN2*p1p2*p1p4**3   & 
          - 32*MOU2*p1p2*p1p4**3   & 
          +  & 
           20*sqrt(MIN2)*np2*p1p2*p1p4**3   & 
          +  & 
           24*sqrt(MIN2)*np4*p1p2*p1p4**3   & 
          -  & 
           88*p1p2**2*p1p4**3   & 
          + 8*MIN2*p1p4**4   & 
          -  & 
           8*MOU2*p1p4**4   & 
          + 8*sqrt(MIN2)*np2*p1p4**4   & 
          -  & 
           32*p1p2*p1p4**4   & 
          - 14*CMPLX(aa,bb)*asym*MIN2**2.5*p2p4   & 
          -  & 
           24*MIN2**4*p2p4   & 
          + 14*CMPLX(aa,bb)*asym*MIN2**1.5*MOU2*p2p4   & 
          +  & 
           16*MIN2**3*MOU2*p2p4   & 
          + 8*MIN2**2*MOU2**2*p2p4   & 
          -  & 
           8*MIN2**3.5*np2*p2p4   & 
          +  & 
           8*MIN2**2.5*MOU2*np2*p2p4   & 
          +  & 
           8*MIN2**3.5*np4*p2p4   & 
          -  & 
           8*MIN2**2.5*MOU2*np4*p2p4   & 
          +  & 
           33*CMPLX(aa,bb)*asym*MIN2**1.5*p1p2*p2p4   & 
          +  & 
           80*MIN2**3*p1p2*p2p4   & 
          -  & 
           11*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p2*p2p4   & 
          -  & 
           16*MIN2**2*MOU2*p1p2*p2p4   & 
          +  & 
           40*MIN2**2.5*np2*p1p2*p2p4   & 
          -  & 
           8*MIN2**1.5*MOU2*np2*p1p2*p2p4   & 
          -  & 
           48*MIN2**2.5*np4*p1p2*p2p4   & 
          +  & 
           16*MIN2**1.5*MOU2*np4*p1p2*p2p4   & 
          -  & 
           19*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2**2*p2p4   & 
          -  & 
           88*MIN2**2*p1p2**2*p2p4   & 
          +  & 
           8*MIN2*MOU2*p1p2**2*p2p4   & 
          -  & 
           32*MIN2**1.5*np2*p1p2**2*p2p4   & 
          +  & 
           72*MIN2**1.5*np4*p1p2**2*p2p4   & 
          -  & 
           8*sqrt(MIN2)*MOU2*np4*p1p2**2*p2p4   & 
          +  & 
           32*MIN2*p1p2**3*p2p4   & 
          -  & 
           32*sqrt(MIN2)*np4*p1p2**3*p2p4   & 
          +  & 
           43*CMPLX(aa,bb)*asym*MIN2**1.5*p1p4*p2p4   & 
          +  & 
           44*MIN2**3*p1p4*p2p4   & 
          -  & 
           31*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p4*p2p4   & 
          -  & 
           32*MIN2**2*MOU2*p1p4*p2p4   & 
          -  & 
           12*MIN2*MOU2**2*p1p4*p2p4   & 
          -  & 
           8*MIN2**2.5*np2*p1p4*p2p4   & 
          -  & 
           8*MIN2**1.5*MOU2*np2*p1p4*p2p4   & 
          -  & 
           94*MIN2**2.5*np4*p1p4*p2p4   & 
          +  & 
           68*MIN2**1.5*MOU2*np4*p1p4*p2p4   & 
          +  & 
           10*sqrt(MIN2)*MOU2**2*np4*p1p4*p2p4   & 
          -  & 
           57*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p1p4*p2p4   & 
          -  & 
           20*MIN2**2*p1p2*p1p4*p2p4   & 
          -  & 
           12*MIN2*MOU2*p1p2*p1p4*p2p4   & 
          +  & 
           32*MIN2**1.5*np2*p1p2*p1p4*p2p4   & 
          -  & 
           8*sqrt(MIN2)*MOU2*np2*p1p2*p1p4*p2p4   & 
          +  & 
           296*MIN2**1.5*np4*p1p2*p1p4*p2p4   & 
          -  & 
           52*sqrt(MIN2)*MOU2*np4*p1p2*p1p4*p2p4   & 
          -  & 
           120*MIN2*p1p2**2*p1p4*p2p4   & 
          +  & 
           16*MOU2*p1p2**2*p1p4*p2p4   & 
          -  & 
           64*sqrt(MIN2)*np2*p1p2**2*p1p4*p2p4   & 
          -  & 
           202*sqrt(MIN2)*np4*p1p2**2*p1p4*p2p4   & 
          +  & 
           96*p1p2**3*p1p4*p2p4   & 
          -  & 
           14*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4**2*p2p4   & 
          -  & 
           28*MIN2**2*p1p4**2*p2p4   & 
          -  & 
           26*MIN2*MOU2*p1p4**2*p2p4   & 
          -  & 
           10*MOU2**2*p1p4**2*p2p4   & 
          +  & 
           4*MIN2**1.5*np2*p1p4**2*p2p4   & 
          +  & 
           4*sqrt(MIN2)*MOU2*np2*p1p4**2*p2p4   & 
          +  & 
           120*MIN2**1.5*np4*p1p4**2*p2p4   & 
          -  & 
           68*sqrt(MIN2)*MOU2*np4*p1p4**2*p2p4   & 
          -  & 
           136*MIN2*p1p2*p1p4**2*p2p4   & 
          +  & 
           48*MOU2*p1p2*p1p4**2*p2p4   & 
          -  & 
           6*sqrt(MIN2)*np2*p1p2*p1p4**2*p2p4   & 
          -  & 
           194*sqrt(MIN2)*np4*p1p2*p1p4**2*p2p4   & 
          +  & 
           208*p1p2**2*p1p4**2*p2p4   & 
          - 80*MIN2*p1p4**3*p2p4   & 
          +  & 
           68*MOU2*p1p4**3*p2p4   & 
          -  & 
           14*sqrt(MIN2)*np2*p1p4**3*p2p4   & 
          -  & 
           32*sqrt(MIN2)*np4*p1p4**3*p2p4   & 
          +  & 
           208*p1p2*p1p4**3*p2p4   & 
          + 32*p1p4**4*p2p4   & 
          -  & 
           30*CMPLX(aa,bb)*asym*MIN2**1.5*p2p4**2   & 
          - 80*MIN2**3*p2p4**2   & 
          +  & 
           8*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p2p4**2   & 
          +  & 
           16*MIN2**2*MOU2*p2p4**2   & 
          -  & 
           40*MIN2**2.5*np2*p2p4**2   & 
          +  & 
           8*MIN2**1.5*MOU2*np2*p2p4**2   & 
          +  & 
           54*MIN2**2.5*np4*p2p4**2   & 
          -  & 
           22*MIN2**1.5*MOU2*np4*p2p4**2   & 
          +  & 
           35*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p2p4**2   & 
          +  & 
           176*MIN2**2*p1p2*p2p4**2   & 
          -  & 
           16*MIN2*MOU2*p1p2*p2p4**2   & 
          +  & 
           64*MIN2**1.5*np2*p1p2*p2p4**2   & 
          -  & 
           174*MIN2**1.5*np4*p1p2*p2p4**2   & 
          +  & 
           22*sqrt(MIN2)*MOU2*np4*p1p2*p2p4**2   & 
          -  & 
           96*MIN2*p1p2**2*p2p4**2   & 
          +  & 
           120*sqrt(MIN2)*np4*p1p2**2*p2p4**2   & 
          +  & 
           35*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4*p2p4**2   & 
          +  & 
           86*MIN2**2*p1p4*p2p4**2   & 
          +  & 
           10*MIN2*MOU2*p1p4*p2p4**2   & 
          +  & 
           8*MIN2**1.5*np2*p1p4*p2p4**2   & 
          -  & 
           214*MIN2**1.5*np4*p1p4*p2p4**2   & 
          +  & 
           46*sqrt(MIN2)*MOU2*np4*p1p4*p2p4**2   & 
          +  & 
           32*MIN2*p1p2*p1p4*p2p4**2   & 
          -  & 
           22*MOU2*p1p2*p1p4*p2p4**2   & 
          +  & 
           32*sqrt(MIN2)*np2*p1p2*p1p4*p2p4**2   & 
          +  & 
           314*sqrt(MIN2)*np4*p1p2*p1p4*p2p4**2   & 
          -  & 
           152*p1p2**2*p1p4*p2p4**2   & 
          +  & 
           104*MIN2*p1p4**2*p2p4**2   & 
          -  & 
           46*MOU2*p1p4**2*p2p4**2   & 
          +  & 
           6*sqrt(MIN2)*np2*p1p4**2*p2p4**2   & 
          +  & 
           120*sqrt(MIN2)*np4*p1p4**2*p2p4**2   & 
          -  & 
           320*p1p2*p1p4**2*p2p4**2   & 
          - 120*p1p4**3*p2p4**2   & 
          -  & 
           16*CMPLX(aa,bb)*asym*sqrt(MIN2)*p2p4**3   & 
          - 88*MIN2**2*p2p4**3   & 
          +  & 
           8*MIN2*MOU2*p2p4**3   & 
          - 32*MIN2**1.5*np2*p2p4**3   & 
          +  & 
           102*MIN2**1.5*np4*p2p4**3   & 
          -  & 
           14*sqrt(MIN2)*MOU2*np4*p2p4**3   & 
          +  & 
           96*MIN2*p1p2*p2p4**3   & 
          -  & 
           144*sqrt(MIN2)*np4*p1p2*p2p4**3   & 
          +  & 
           14*MOU2*p1p4*p2p4**3   & 
          -  & 
           144*sqrt(MIN2)*np4*p1p4*p2p4**3   & 
          +  & 
           144*p1p2*p1p4*p2p4**3   & 
          + 144*p1p4**2*p2p4**3   & 
          -  & 
           32*MIN2*p2p4**4   & 
          + 56*sqrt(MIN2)*np4*p2p4**4   & 
          -  & 
           56*p1p4*p2p4**4)* & 
         Ccache(5))/ & 
       (9.*p1p4**2*(p1p4   & 
          - p2p4)*Pi**2)   & 
          -  & 
      (2*EL**4*GF**2*(20*CMPLX(aa,bb)*asym*MIN2**2.5*p1p4   & 
          -  & 
           17*CMPLX(aa,bb)*asym*MIN2**1.5*MOU2*p1p4   & 
          -  & 
           3*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2**2*p1p4   & 
          -  & 
           9*MIN2**3.5*np4*p1p4   & 
          +  & 
           18*MIN2**2.5*MOU2*np4*p1p4   & 
          -  & 
           9*MIN2**1.5*MOU2**2*np4*p1p4   & 
          -  & 
           72*CMPLX(aa,bb)*asym*MIN2**1.5*p1p2*p1p4   & 
          +  & 
           38*MIN2**3*p1p2*p1p4   & 
          +  & 
           20*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p2*p1p4   & 
          -  & 
           56*MIN2**2*MOU2*p1p2*p1p4   & 
          +  & 
           18*MIN2*MOU2**2*p1p2*p1p4   & 
          +  & 
           10*MIN2**2.5*np2*p1p2*p1p4   & 
          -  & 
           10*MIN2**1.5*MOU2*np2*p1p2*p1p4   & 
          +  & 
           61*MIN2**2.5*np4*p1p2*p1p4   & 
          -  & 
           88*MIN2**1.5*MOU2*np4*p1p2*p1p4   & 
          +  & 
           27*sqrt(MIN2)*MOU2**2*np4*p1p2*p1p4   & 
          +  & 
           48*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2**2*p1p4   & 
          -  & 
           118*MIN2**2*p1p2**2*p1p4   & 
          +  & 
           78*MIN2*MOU2*p1p2**2*p1p4   & 
          -  & 
           80*MIN2**1.5*np2*p1p2**2*p1p4   & 
          +  & 
           36*sqrt(MIN2)*MOU2*np2*p1p2**2*p1p4   & 
          -  & 
           92*MIN2**1.5*np4*p1p2**2*p1p4   & 
          +  & 
           72*sqrt(MIN2)*MOU2*np4*p1p2**2*p1p4   & 
          +  & 
           144*MIN2*p1p2**3*p1p4   & 
          - 36*MOU2*p1p2**3*p1p4   & 
          +  & 
           64*sqrt(MIN2)*np2*p1p2**3*p1p4   & 
          +  & 
           40*sqrt(MIN2)*np4*p1p2**3*p1p4   & 
          -  & 
           64*p1p2**4*p1p4   & 
          + 23*CMPLX(aa,bb)*asym*MIN2**1.5*p1p4**2   & 
          -  & 
           31*MIN2**3*p1p4**2   & 
          -  & 
           17*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p4**2   & 
          +  & 
           42*MIN2**2*MOU2*p1p4**2   & 
          -  & 
           11*MIN2*MOU2**2*p1p4**2   & 
          +  & 
           31*MIN2**2.5*np2*p1p4**2   & 
          -  & 
           40*MIN2**1.5*MOU2*np2*p1p4**2   & 
          +  & 
           9*sqrt(MIN2)*MOU2**2*np2*p1p4**2   & 
          +  & 
           34*MIN2**2.5*np4*p1p4**2   & 
          -  & 
           44*MIN2**1.5*MOU2*np4*p1p4**2   & 
          +  & 
           10*sqrt(MIN2)*MOU2**2*np4*p1p4**2   & 
          +  & 
           2*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p1p4**2   & 
          -  & 
           24*MIN2**2*p1p2*p1p4**2   & 
          +  & 
           100*MIN2*MOU2*p1p2*p1p4**2   & 
          -  & 
           36*MOU2**2*p1p2*p1p4**2   & 
          -  & 
           8*MIN2**1.5*np2*p1p2*p1p4**2   & 
          -  & 
           173*MIN2**1.5*np4*p1p2*p1p4**2   & 
          +  & 
           113*sqrt(MIN2)*MOU2*np4*p1p2*p1p4**2   & 
          +  & 
           124*MIN2*p1p2**2*p1p4**2   & 
          -  & 
           72*MOU2*p1p2**2*p1p4**2   & 
          +  & 
           40*sqrt(MIN2)*np2*p1p2**2*p1p4**2   & 
          +  & 
           150*sqrt(MIN2)*np4*p1p2**2*p1p4**2   & 
          -  & 
           80*p1p2**3*p1p4**2   & 
          - 32*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4**3   & 
          -  & 
           18*MIN2**2*p1p4**3   & 
          + 28*MIN2*MOU2*p1p4**3   & 
          -  & 
           10*MOU2**2*p1p4**3   & 
          - 107*MIN2**1.5*np2*p1p4**3   & 
          +  & 
           95*sqrt(MIN2)*MOU2*np2*p1p4**3   & 
          -  & 
           8*MIN2**1.5*np4*p1p4**3   & 
          +  & 
           8*sqrt(MIN2)*MOU2*np4*p1p4**3   & 
          +  & 
           248*MIN2*p1p2*p1p4**3   & 
          - 208*MOU2*p1p2*p1p4**3   & 
          +  & 
           90*sqrt(MIN2)*np2*p1p2*p1p4**3   & 
          +  & 
           48*sqrt(MIN2)*np4*p1p2*p1p4**3   & 
          -  & 
           240*p1p2**2*p1p4**3   & 
          + 8*MIN2*p1p4**4   & 
          -  & 
           8*MOU2*p1p4**4   & 
          + 32*sqrt(MIN2)*np2*p1p4**4   & 
          -  & 
           80*p1p2*p1p4**4   & 
          - 31*CMPLX(aa,bb)*asym*MIN2**2.5*p2p4   & 
          -  & 
           38*MIN2**4*p2p4   & 
          + 31*CMPLX(aa,bb)*asym*MIN2**1.5*MOU2*p2p4   & 
          +  & 
           56*MIN2**3*MOU2*p2p4   & 
          - 18*MIN2**2*MOU2**2*p2p4   & 
          -  & 
           10*MIN2**3.5*np2*p2p4   & 
          +  & 
           10*MIN2**2.5*MOU2*np2*p2p4   & 
          +  & 
           13*MIN2**3.5*np4*p2p4   & 
          -  & 
           16*MIN2**2.5*MOU2*np4*p2p4   & 
          +  & 
           3*MIN2**1.5*MOU2**2*np4*p2p4   & 
          +  & 
           62*CMPLX(aa,bb)*asym*MIN2**1.5*p1p2*p2p4   & 
          +  & 
           118*MIN2**3*p1p2*p2p4   & 
          -  & 
           12*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p2*p2p4   & 
          -  & 
           78*MIN2**2*MOU2*p1p2*p2p4   & 
          +  & 
           80*MIN2**2.5*np2*p1p2*p2p4   & 
          -  & 
           36*MIN2**1.5*MOU2*np2*p1p2*p2p4   & 
          -  & 
           102*MIN2**2.5*np4*p1p2*p2p4   & 
          +  & 
           58*MIN2**1.5*MOU2*np4*p1p2*p2p4   & 
          -  & 
           24*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2**2*p2p4   & 
          -  & 
           144*MIN2**2*p1p2**2*p2p4   & 
          +  & 
           36*MIN2*MOU2*p1p2**2*p2p4   & 
          -  & 
           64*MIN2**1.5*np2*p1p2**2*p2p4   & 
          +  & 
           159*MIN2**1.5*np4*p1p2**2*p2p4   & 
          -  & 
           39*sqrt(MIN2)*MOU2*np4*p1p2**2*p2p4   & 
          +  & 
           64*MIN2*p1p2**3*p2p4   & 
          -  & 
           70*sqrt(MIN2)*np4*p1p2**3*p2p4   & 
          +  & 
           116*CMPLX(aa,bb)*asym*MIN2**1.5*p1p4*p2p4   & 
          +  & 
           99*MIN2**3*p1p4*p2p4   & 
          -  & 
           58*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p4*p2p4   & 
          -  & 
           126*MIN2**2*MOU2*p1p4*p2p4   & 
          +  & 
           27*MIN2*MOU2**2*p1p4*p2p4   & 
          -  & 
           28*MIN2**2.5*np2*p1p4*p2p4   & 
          +  & 
           16*MIN2**1.5*MOU2*np2*p1p4*p2p4   & 
          -  & 
           158*MIN2**2.5*np4*p1p4*p2p4   & 
          +  & 
           227*MIN2**1.5*MOU2*np4*p1p4*p2p4   & 
          -  & 
           57*sqrt(MIN2)*MOU2**2*np4*p1p4*p2p4   & 
          -  & 
           157*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p1p4*p2p4   & 
          -  & 
           74*MIN2**2*p1p2*p1p4*p2p4   & 
          -  & 
           6*MIN2*MOU2*p1p2*p1p4*p2p4   & 
          +  & 
           45*MIN2**1.5*np2*p1p2*p1p4*p2p4   & 
          -  & 
           33*sqrt(MIN2)*MOU2*np2*p1p2*p1p4*p2p4   & 
          +  & 
           552*MIN2**1.5*np4*p1p2*p1p4*p2p4   & 
          -  & 
           302*sqrt(MIN2)*MOU2*np4*p1p2*p1p4*p2p4   & 
          -  & 
           184*MIN2*p1p2**2*p1p4*p2p4   & 
          +  & 
           72*MOU2*p1p2**2*p1p4*p2p4   & 
          -  & 
           122*sqrt(MIN2)*np2*p1p2**2*p1p4*p2p4   & 
          -  & 
           384*sqrt(MIN2)*np4*p1p2**2*p1p4*p2p4   & 
          +  & 
           192*p1p2**3*p1p4*p2p4   & 
          -  & 
           8*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4**2*p2p4   & 
          +  & 
           8*MIN2**2*p1p4**2*p2p4   & 
          -  & 
           102*MIN2*MOU2*p1p4**2*p2p4   & 
          +  & 
           54*MOU2**2*p1p4**2*p2p4   & 
          +  & 
           86*MIN2**1.5*np2*p1p4**2*p2p4   & 
          -  & 
           34*sqrt(MIN2)*MOU2*np2*p1p4**2*p2p4   & 
          +  & 
           272*MIN2**1.5*np4*p1p4**2*p2p4   & 
          -  & 
           244*sqrt(MIN2)*MOU2*np4*p1p4**2*p2p4   & 
          -  & 
           488*MIN2*p1p2*p1p4**2*p2p4   & 
          +  & 
           324*MOU2*p1p2*p1p4**2*p2p4   & 
          -  & 
           140*sqrt(MIN2)*np2*p1p2*p1p4**2*p2p4   & 
          -  & 
           494*sqrt(MIN2)*np4*p1p2*p1p4**2*p2p4   & 
          +  & 
           512*p1p2**2*p1p4**2*p2p4   & 
          -  & 
           240*MIN2*p1p4**3*p2p4   & 
          + 244*MOU2*p1p4**3*p2p4   & 
          -  & 
           66*sqrt(MIN2)*np2*p1p4**3*p2p4   & 
          -  & 
           80*sqrt(MIN2)*np4*p1p4**3*p2p4   & 
          +  & 
           560*p1p2*p1p4**3*p2p4   & 
          + 80*p1p4**4*p2p4   & 
          -  & 
           59*CMPLX(aa,bb)*asym*MIN2**1.5*p2p4**2   & 
          - 118*MIN2**3*p2p4**2   & 
          +  & 
           12*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p2p4**2   & 
          +  & 
           78*MIN2**2*MOU2*p2p4**2   & 
          -  & 
           77*MIN2**2.5*np2*p2p4**2   & 
          +  & 
           33*MIN2**1.5*MOU2*np2*p2p4**2   & 
          +  & 
           126*MIN2**2.5*np4*p2p4**2   & 
          -  & 
           82*MIN2**1.5*MOU2*np4*p2p4**2   & 
          +  & 
           42*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p2p4**2   & 
          +  & 
           288*MIN2**2*p1p2*p2p4**2   & 
          -  & 
           72*MIN2*MOU2*p1p2*p2p4**2   & 
          +  & 
           122*MIN2**1.5*np2*p1p2*p2p4**2   & 
          -  & 
           399*MIN2**1.5*np4*p1p2*p2p4**2   & 
          +  & 
           111*sqrt(MIN2)*MOU2*np4*p1p2*p2p4**2   & 
          -  & 
           192*MIN2*p1p2**2*p2p4**2   & 
          +  & 
           252*sqrt(MIN2)*np4*p1p2**2*p2p4**2   & 
          +  & 
           112*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4*p2p4**2   & 
          +  & 
           158*MIN2**2*p1p4*p2p4**2   & 
          -  & 
           50*MIN2*MOU2*p1p4*p2p4**2   & 
          +  & 
           29*MIN2**1.5*np2*p1p4*p2p4**2   & 
          +  & 
           3*sqrt(MIN2)*MOU2*np2*p1p4*p2p4**2   & 
          -  & 
           460*MIN2**1.5*np4*p1p4*p2p4**2   & 
          +  & 
           230*sqrt(MIN2)*MOU2*np4*p1p4*p2p4**2   & 
          +  & 
           76*MIN2*p1p2*p1p4*p2p4**2   & 
          -  & 
           108*MOU2*p1p2*p1p4*p2p4**2   & 
          +  & 
           64*sqrt(MIN2)*np2*p1p2*p1p4*p2p4**2   & 
          +  & 
           696*sqrt(MIN2)*np4*p1p2*p1p4*p2p4**2   & 
          -  & 
           304*p1p2**2*p1p4*p2p4**2   & 
          +  & 
           348*MIN2*p1p4**2*p2p4**2   & 
          -  & 
           224*MOU2*p1p4**2*p2p4**2   & 
          +  & 
           100*sqrt(MIN2)*np2*p1p4**2*p2p4**2   & 
          +  & 
           320*sqrt(MIN2)*np4*p1p4**2*p2p4**2   & 
          -  & 
           784*p1p2*p1p4**2*p2p4**2   & 
          - 320*p1p4**3*p2p4**2   & 
          -  & 
           18*CMPLX(aa,bb)*asym*sqrt(MIN2)*p2p4**3   & 
          -  & 
           144*MIN2**2*p2p4**3   & 
          + 36*MIN2*MOU2*p2p4**3   & 
          -  & 
           58*MIN2**1.5*np2*p2p4**3   & 
          +  & 
           240*MIN2**1.5*np4*p2p4**3   & 
          -  & 
           72*sqrt(MIN2)*MOU2*np4*p2p4**3   & 
          +  & 
           192*MIN2*p1p2*p2p4**3   & 
          -  & 
           294*sqrt(MIN2)*np4*p1p2*p2p4**3   & 
          -  & 
           36*MIN2*p1p4*p2p4**3   & 
          + 72*MOU2*p1p4*p2p4**3   & 
          -  & 
           6*sqrt(MIN2)*np2*p1p4*p2p4**3   & 
          -  & 
           352*sqrt(MIN2)*np4*p1p4*p2p4**3   & 
          +  & 
           288*p1p2*p1p4*p2p4**3   & 
          + 352*p1p4**2*p2p4**3   & 
          -  & 
           64*MIN2*p2p4**4   & 
          + 112*sqrt(MIN2)*np4*p2p4**4   & 
          -  & 
           112*p1p4*p2p4**4)* & 
         Ccache(6))/ & 
       (9.*p1p4**2*(p1p4   & 
          - p2p4)*Pi**2)   & 
          -  & 
      (4*EL**4*GF**2*(  & 
          -(CMPLX(aa,bb)*asym*MIN2**2.5*p1p4)   & 
          +  & 
           CMPLX(aa,bb)*asym*MIN2**1.5*MOU2*p1p4   & 
          - 8*MIN2**3.5*np4*p1p4   & 
          +  & 
           8*MIN2**2.5*MOU2*np4*p1p4   & 
          +  & 
           9*CMPLX(aa,bb)*asym*MIN2**1.5*p1p2*p1p4   & 
          +  & 
           16*MIN2**3*p1p2*p1p4   & 
          -  & 
           4*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p2*p1p4   & 
          -  & 
           16*MIN2**2*MOU2*p1p2*p1p4   & 
          +  & 
           36*MIN2**2.5*np4*p1p2*p1p4   & 
          -  & 
           20*MIN2**1.5*MOU2*np4*p1p2*p1p4   & 
          -  & 
           16*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2**2*p1p4   & 
          -  & 
           40*MIN2**2*p1p2**2*p1p4   & 
          +  & 
           8*MIN2*MOU2*p1p2**2*p1p4   & 
          -  & 
           8*MIN2**1.5*np2*p1p2**2*p1p4   & 
          -  & 
           44*MIN2**1.5*np4*p1p2**2*p1p4   & 
          +  & 
           4*sqrt(MIN2)*MOU2*np4*p1p2**2*p1p4   & 
          +  & 
           24*MIN2*p1p2**3*p1p4   & 
          +  & 
           16*sqrt(MIN2)*np4*p1p2**3*p1p4   & 
          -  & 
           2*CMPLX(aa,bb)*asym*MIN2**1.5*p1p4**2   & 
          - 16*MIN2**3*p1p4**2   & 
          -  & 
           CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p4**2   & 
          +  & 
           16*MIN2**2*MOU2*p1p4**2   & 
          +  & 
           8*MIN2**2.5*np2*p1p4**2   & 
          -  & 
           8*MIN2**1.5*MOU2*np2*p1p4**2   & 
          +  & 
           18*MIN2**2.5*np4*p1p4**2   & 
          -  & 
           18*MIN2**1.5*MOU2*np4*p1p4**2   & 
          +  & 
           11*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p1p4**2   & 
          +  & 
           4*MIN2**2*p1p2*p1p4**2   & 
          +  & 
           28*MIN2*MOU2*p1p2*p1p4**2   & 
          -  & 
           4*MIN2**1.5*np2*p1p2*p1p4**2   & 
          +  & 
           4*sqrt(MIN2)*MOU2*np2*p1p2*p1p4**2   & 
          -  & 
           62*MIN2**1.5*np4*p1p2*p1p4**2   & 
          +  & 
           26*sqrt(MIN2)*MOU2*np4*p1p2*p1p4**2   & 
          +  & 
           48*MIN2*p1p2**2*p1p4**2   & 
          -  & 
           8*MOU2*p1p2**2*p1p4**2   & 
          +  & 
           16*sqrt(MIN2)*np2*p1p2**2*p1p4**2   & 
          +  & 
           36*sqrt(MIN2)*np4*p1p2**2*p1p4**2   & 
          -  & 
           32*p1p2**3*p1p4**2   & 
          - 5*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4**3   & 
          +  & 
           6*MIN2**2*p1p4**3   & 
          - 6*MIN2*MOU2*p1p4**3   & 
          -  & 
           26*MIN2**1.5*np2*p1p4**3   & 
          +  & 
           18*sqrt(MIN2)*MOU2*np2*p1p4**3   & 
          -  & 
           8*MIN2**1.5*np4*p1p4**3   & 
          +  & 
           8*sqrt(MIN2)*MOU2*np4*p1p4**3   & 
          +  & 
           40*MIN2*p1p2*p1p4**3   & 
          - 44*MOU2*p1p2*p1p4**3   & 
          +  & 
           20*sqrt(MIN2)*np2*p1p2*p1p4**3   & 
          +  & 
           24*sqrt(MIN2)*np4*p1p2*p1p4**3   & 
          -  & 
           56*p1p2**2*p1p4**3   & 
          + 8*MIN2*p1p4**4   & 
          -  & 
           8*MOU2*p1p4**4   & 
          + 8*sqrt(MIN2)*np2*p1p4**4   & 
          -  & 
           32*p1p2*p1p4**4   & 
          - 7*CMPLX(aa,bb)*asym*MIN2**2.5*p2p4   & 
          -  & 
           16*MIN2**4*p2p4   & 
          + 7*CMPLX(aa,bb)*asym*MIN2**1.5*MOU2*p2p4   & 
          +  & 
           16*MIN2**3*MOU2*p2p4   & 
          +  & 
           11*CMPLX(aa,bb)*asym*MIN2**1.5*p1p2*p2p4   & 
          +  & 
           40*MIN2**3*p1p2*p2p4   & 
          - 8*MIN2**2*MOU2*p1p2*p2p4   & 
          +  & 
           8*MIN2**2.5*np2*p1p2*p2p4   & 
          -  & 
           8*MIN2**2.5*np4*p1p2*p2p4   & 
          -  & 
           24*MIN2**2*p1p2**2*p2p4   & 
          +  & 
           8*MIN2**1.5*np4*p1p2**2*p2p4   & 
          +  & 
           5*CMPLX(aa,bb)*asym*MIN2**1.5*p1p4*p2p4   & 
          +  & 
           40*MIN2**3*p1p4*p2p4   & 
          -  & 
           7*CMPLX(aa,bb)*asym*sqrt(MIN2)*MOU2*p1p4*p2p4   & 
          -  & 
           40*MIN2**2*MOU2*p1p4*p2p4   & 
          -  & 
           8*MIN2**2.5*np2*p1p4*p2p4   & 
          -  & 
           56*MIN2**2.5*np4*p1p4*p2p4   & 
          +  & 
           48*MIN2**1.5*MOU2*np4*p1p4*p2p4   & 
          +  & 
           13*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p2*p1p4*p2p4   & 
          -  & 
           44*MIN2**2*p1p2*p1p4*p2p4   & 
          +  & 
           4*MIN2*MOU2*p1p2*p1p4*p2p4   & 
          +  & 
           160*MIN2**1.5*np4*p1p2*p1p4*p2p4   & 
          -  & 
           18*sqrt(MIN2)*MOU2*np4*p1p2*p1p4*p2p4   & 
          -  & 
           8*MIN2*p1p2**2*p1p4*p2p4   & 
          -  & 
           82*sqrt(MIN2)*np4*p1p2**2*p1p4*p2p4   & 
          -  & 
           14*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4**2*p2p4   & 
          -  & 
           34*MIN2**2*p1p4**2*p2p4   & 
          +  & 
           2*MIN2*MOU2*p1p4**2*p2p4   & 
          +  & 
           20*MIN2**1.5*np2*p1p4**2*p2p4   & 
          -  & 
           4*sqrt(MIN2)*MOU2*np2*p1p4**2*p2p4   & 
          +  & 
           88*MIN2**1.5*np4*p1p4**2*p2p4   & 
          -  & 
           66*sqrt(MIN2)*MOU2*np4*p1p4**2*p2p4   & 
          -  & 
           80*MIN2*p1p2*p1p4**2*p2p4   & 
          +  & 
           22*MOU2*p1p2*p1p4**2*p2p4   & 
          -  & 
           38*sqrt(MIN2)*np2*p1p2*p1p4**2*p2p4   & 
          -  & 
           130*sqrt(MIN2)*np4*p1p2*p1p4**2*p2p4   & 
          +  & 
           120*p1p2**2*p1p4**2*p2p4   & 
          - 48*MIN2*p1p4**3*p2p4   & 
          +  & 
           66*MOU2*p1p4**3*p2p4   & 
          -  & 
           14*sqrt(MIN2)*np2*p1p4**3*p2p4   & 
          -  & 
           32*sqrt(MIN2)*np4*p1p4**3*p2p4   & 
          +  & 
           144*p1p2*p1p4**3*p2p4   & 
          + 32*p1p4**4*p2p4   & 
          -  & 
           11*CMPLX(aa,bb)*asym*MIN2**1.5*p2p4**2   & 
          - 40*MIN2**3*p2p4**2   & 
          +  & 
           8*MIN2**2*MOU2*p2p4**2   & 
          -  & 
           8*MIN2**2.5*np2*p2p4**2   & 
          +  & 
           8*MIN2**2.5*np4*p2p4**2   & 
          +  & 
           48*MIN2**2*p1p2*p2p4**2   & 
          -  & 
           22*MIN2**1.5*np4*p1p2*p2p4**2   & 
          +  & 
           3*CMPLX(aa,bb)*asym*sqrt(MIN2)*p1p4*p2p4**2   & 
          +  & 
           84*MIN2**2*p1p4*p2p4**2   & 
          -  & 
           12*MIN2*MOU2*p1p4*p2p4**2   & 
          +  & 
           8*MIN2**1.5*np2*p1p4*p2p4**2   & 
          -  & 
           110*MIN2**1.5*np4*p1p4*p2p4**2   & 
          +  & 
           14*sqrt(MIN2)*MOU2*np4*p1p4*p2p4**2   & 
          -  & 
           56*MIN2*p1p2*p1p4*p2p4**2   & 
          +  & 
           122*sqrt(MIN2)*np4*p1p2*p1p4*p2p4**2   & 
          +  & 
           32*MIN2*p1p4**2*p2p4**2   & 
          -  & 
           14*MOU2*p1p4**2*p2p4**2   & 
          +  & 
           22*sqrt(MIN2)*np2*p1p4**2*p2p4**2   & 
          +  & 
           88*sqrt(MIN2)*np4*p1p4**2*p2p4**2   & 
          -  & 
           144*p1p2*p1p4**2*p2p4**2   & 
          - 88*p1p4**3*p2p4**2   & 
          -  & 
           24*MIN2**2*p2p4**3   & 
          + 14*MIN2**1.5*np4*p2p4**3   & 
          +  & 
           40*MIN2*p1p4*p2p4**3   & 
          -  & 
           56*sqrt(MIN2)*np4*p1p4*p2p4**3   & 
          +  & 
           56*p1p4**2*p2p4**3)* & 
         Ccache(7))/ & 
       (9.*p1p4**2*(p1p4   & 
          - p2p4)*Pi**2))
!
!
      return
!
      end function

                          !!!!!!!!!!!!!!!!!!!!!!
                            END MODULE rad_T1TRIBC
                          !!!!!!!!!!!!!!!!!!!!!!
