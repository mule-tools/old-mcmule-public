import subprocess
import hashlib
import sys


def build_hash(files):
    h = hashlib.sha1()
    for file in files:
        with open(file, 'rb') as fp:
            h.update(fp.read())

    return h.hexdigest()


def get_git():
    proc = subprocess.Popen(
        ['git', 'ls-files'],
        stdout=subprocess.PIPE, stderr=subprocess.PIPE
    )
    if proc.wait() != 0:
        return ('not-git', 'not-git')

    proc = subprocess.Popen(
        ['git', 'rev-parse', 'HEAD'],
        stdout=subprocess.PIPE
    )
    commit, _ = proc.communicate()

    proc = subprocess.Popen(
        ['git', 'rev-parse', '--abbrev-ref', 'HEAD'],
        stdout=subprocess.PIPE
    )
    branch, _ = proc.communicate()

    return (commit.strip(), branch.strip())


if __name__ == "__main__":
    commit, branch = get_git()
    print('struct{char f[40],g[40],b[40];}version_={.f = "%s", .g="%s",.b="%s"};' % (
        build_hash(sys.argv[1:]),
        commit.decode(), branch.decode()
    ))

