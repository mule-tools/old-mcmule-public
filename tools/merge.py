import struct
import sys


def parse_deps(fp):
    assert fp.read(12) == b'# ninjadeps\n'
    v, = struct.unpack('<I', fp.read(4))
    assert v == 4

    pathid = 0
    paths = []
    deps = []
    while True:
        h = fp.read(4)
        if len(h) != 4:
            break

        h, = struct.unpack('<I', h)
        is_dep = h >> 31 != 0
        size = h & 0x7FFFFFFF

        body = fp.read(size)

        if is_dep:
            pid, mtime = struct.unpack('<IQ', body[:12])
            extra = struct.unpack("<"+"I"*(len(body)//4-3), body[12:])
            deps.append((
                paths[pid],
                mtime,
                tuple(paths[i] for i in extra)
            ))
        else:
            paths.append(body[:-4].strip(b'\x00').decode())
            cks, = struct.unpack('<I', body[-4:])
            assert cks == 2**32 + ~pathid
            pathid += 1

    return deps


def write_deps(deps, fp):
    paths = {}
    def pid(path):
        nonlocal paths
        try:
            return paths[path]
        except KeyError:
            pid = len(paths)

            # write path
            body = path.encode()
            if len(path) % 4:
                body += b'\x00' * (4-len(path)%4)
            body += struct.pack('<I', 2**32 + ~pid)

            fp.write(struct.pack('<I', len(body)))
            fp.write(body)

            paths[path] = pid
            return pid

    fp.write(b'# ninjadeps\n')
    fp.write(struct.pack('<I', 4))
    for path, mtime, extra in deps:
        body = struct.pack('<IQ', pid(path), mtime)
        for i in extra:
            body += struct.pack('<I', pid(i))

        fp.write(struct.pack('<I', 0x80000000|len(body)))
        fp.write(body)


def merge_deps(ins, out):
    deps = set()
    for i in ins:
        with open(i, 'rb') as fp:
            deps = deps.union(parse_deps(fp))

    with open(out, 'wb') as fp:
        write_deps(deps, fp)


def merge_logs(ins, out):
    recs = []
    with open(out, 'w') as fo:
        fo.write("# ninja log v5\n")
        for i in ins:
            with open(i, 'r') as fp:
                for i in fp.read().split('\n')[1:]:
                    l = i.split('\t')
                    if len(l) < 4:
                        continue
                    if l[3] in recs:
                        continue
                    fo.write(i + '\n')
                    recs.append(l[3])


if __name__ == "__main__":
    merge_deps(
        [f'build/.ninja_deps_{i}' for i in sys.argv[1:]],
        'build/.ninja_deps'
    )
    merge_logs(
        [f'build/.ninja_log_{i}' for i in sys.argv[1:]],
        'build/.ninja_log'
    )
