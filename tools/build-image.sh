#!/bin/sh
set -e


function myecho {
    echo -e "\e[34m$@\e[0m"
    echo -n
}

myecho "+----------------------------------------------------------+"
myecho "|                   McMule image builder                   |"
myecho "+----------------------------------------------------------+"

if [ -z "$1" ]; then
    if [ -f ".git/config" ]; then
        if grep -q monte-carlo ".git/config"; then
            tag="registry.gitlab.com/mule-tools/monte-carlo"
            myecho " Internal build, using tag"
            myecho " $tag"
        else
            tag="registry.gitlab.com/mule-tools/mcmule"
            myecho " Public build, using tag"
            myecho " $tag"
        fi
    else
        tag="yulrich/mcmule"
        myecho " Unknown build, using tag"
        myecho " $tag"
    fi
else
    tag=$1
    myecho " Custom build, using tag"
    myecho " $tag"
fi

push=${2:-'yes'}

btag=`echo $tag | rev | cut -d":" -f1 | rev`
repo=`echo $tag | rev | cut -d":" -f2- | rev`

groups="misc mue mudec mudecrare ee"
myecho " This build uses groups $groups"

echo -e "\e[0Ksection_start:`date +%s`:pull\r\e[0KPulling cache"
myecho "+----------------------------------------------------------+"
myecho "| Pulling cache                                            |"
myecho "+----------------------------------------------------------+"
docker pull $repo:$btag || true
for i in pre $groups
do
    docker pull $repo:$btag-$i || true
done

echo -e "\e[0Ksection_end:`date +%s`:pull\r\e[0K"

myecho "+----------------------------------------------------------+"
myecho "| Execute build                                            |"
myecho "+----------------------------------------------------------+"

cachelist=""
for stage in pre $groups
do
    echo -e "\e[0Ksection_start:`date +%s`:stage$stage\r\e[0KBuilding stage $stage"
    myecho " Build stage $stage"
    myecho "+----------------------------------------------------------+"
    fulltag="$repo:$btag-$stage"
    cachelist="$cachelist --cache-from $fulltag"
    docker build $cachelist --target build$stage -t $fulltag .
    echo -e "\e[0Ksection_end:`date +%s`:stage$stage\r\e[0K"
done
echo -e "\e[0Ksection_start:`date +%s`:final\r\e[0KFinal image"
myecho " Build final image"
myecho "+----------------------------------------------------------+"
fulltag="$repo:$btag"
cachelist="$cachelist --cache-from $fulltag"
docker build $cachelist -t $fulltag .
echo -e "\e[0Ksection_end:`date +%s`:final\r\e[0K"

if [[ "$push" == "yes" ]]; then

myecho "+----------------------------------------------------------+"
myecho "| Pushing build                                            |"
myecho "+----------------------------------------------------------+"

docker push $fulltag

myecho "+----------------------------------------------------------+"
myecho "| Pushing cache                                            |"
myecho "+----------------------------------------------------------+"
for stage in pre $groups
do
    docker push "$repo:$btag-$stage"
done

fi
