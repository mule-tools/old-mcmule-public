#!/bin/sh

BASEDIR=$(dirname $0)

if [[ -f "$BASEDIR/../src/user.f95" ]] ; then
    cat "$BASEDIR/../src/user.f95"
else
    cat "$BASEDIR/user-default.f95"
fi
