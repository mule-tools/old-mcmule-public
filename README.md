# The McMule internal repository

McMule is a framework for fully differential higher-order QED
calculations of scattering and decay processes involving leptons. It
keeps finite lepton masses, which regularises collinear singularities.
Soft singularities are treated with dimensional regularisation and
using $`{\rm FKS}^\ell`$ subtraction

Please find the manual for McMule
[here](https://gitlab.com/mule-tools/manual) or download it here [as a
pdf](https://gitlab.com/mule-tools/manual/-/jobs/artifacts/master/raw/manual.pdf?job=build)

If you find McMule useful, please consider citing
[[2007.01654]](https://arxiv.org/abs/2007.01654)

>  QED at NNLO with McMule
>
>  P. Banerjee, T. Engel, A. Signer, Y. Ulrich

## Getting started
To obtain a copy of McMule we recommend the following approach
```bash
$ git clone --recursive https://gitlab.com/mule-tools/mcmule
````
To build McMule, a Fortran compiler such as `gfortran` and a python installation is needed.
McMule uses [meson](https://mesonbuild.com/) and [ninja](https://ninja-build.org) for building.
You can obtain these by running
```shell
$ pip install meson ninja
```
The main executable can be compiled by running
```bash
$ meson setup build
$ ninja -C build
```
McMule will then be compiled in the newly created subdirectory `build/`.
Developers might prefer to use the patched version of meson that is being shipped with pymule
```bash
$ pymule meson setup build
$ ninja -C build
```

Alternatively, we provide a Docker container for easy deployment and
legacy results. In multi-user environments, udocker can be used
instead. In either case, a pre-compiled copy of the code can be
obtained by calling
```bash
$ docker pull yulrich/mcmule  # requires Docker to be installed
$ udocker pull yulrich/mcmule # requires uDocker to be installed
```
or for the internal version (substitute the branch you want)
```bash
$ docker pull registry.gitlab.com/mule-tools/mcmule:master
```
Please refer to the manual on how to use McMule.
